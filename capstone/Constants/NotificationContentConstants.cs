﻿namespace capstone.Constants {
    public static class NotificationContentConstants {
        public const string USER_ROLE_CHANGE = "User Role Change";
        public const string USER_GROUP_ADD = "User Group Add";
        public const string USER_GROUP_REMOVE = "User Group Remove";
        public const string STUDENT_EVENT_OPENING = "Student Event Opening";
        public const string STUDENT_EVENT_REPORT = "Student Event Report";
        public const string STUDENT_EVENT_DRAFT = "Student Event Draft";
        public const string STUDENT_REPORT_REJECT = "Student Report Reject";
        public const string ORGANIZER_GROUP_LEADER = "Organizer Group Leader";
        public const string ORGANIZER_EVENT_REGISTER = "Organizer Event Register";
        public const string ORGANIZER_EVENT_FEEDBACK = "Organizer Event Feedback";
        public const string ORGANIZER_EVENT_ACCEPT = "Organizer Event Accept";
        public const string ORGANIZER_EVENT_REJECT = "Organizer Event Reject";
        public const string ORGANIZER_EVENT_REPORT = "Organizer Event Report";
        public const string ORGANIZER_EVENT_DRAFT = "Organizer Event Draft";
    }
}