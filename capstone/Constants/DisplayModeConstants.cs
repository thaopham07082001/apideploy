﻿namespace capstone.Constants {
    public static class DisplayModeConstants {
        public const string GRID = "Grid";
        public const string LIST = "List";
    }
}