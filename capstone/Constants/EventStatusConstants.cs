﻿namespace capstone.Constants {
    public static class EventStatusConstants {
        public const string DRAFT = "Draft";
        public const string PENDING = "Pending";
        public const string CANCELLED = "Cancelled";
        public const string OPENING = "Opening";
        public const string HAPPENING = "Happening";
        public const string CLOSED = "Closed";
        public const string REJECTED = "Rejected";
        public const string DELETED = "Deleted";
    }
}