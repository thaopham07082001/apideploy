﻿namespace capstone.Constants {
    public static class ObjectTypeConstants {
        public const string EVENT = "Event";
        public const string GROUP = "Group";
    }
}