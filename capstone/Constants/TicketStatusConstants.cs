﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace capstone.Constants
{
    public static class TicketStatusConstants
    {
        public const string Active = "Active";
        public const string Success = "Success";
        public const string Cancelled = "Cancelled";
        public const string Expired = "Expired";

    }
}