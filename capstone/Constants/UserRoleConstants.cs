﻿namespace capstone.Constants {
    public static class UserRoleConstants {
        public const string STUDENT = "Student";
        public const string ORGANIZER = "Organizer";
        public const string MANAGER = "Manager";
        public const string ADMIN = "Administrator";
    }
}