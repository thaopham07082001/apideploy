﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Helpers;

namespace capstone.Jobs {
    public class EventStartNotificationJob {
        public async Task NotifyEvent() {
            using (Database db = new Database()) {
                IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e) {
                    return e.EventStatus.Equals(EventStatusConstants.OPENING) && e.OpenDate <= DateTime.Now.AddDays(1) && e.OpenDate >= DateTime.Now.AddDays(1).AddMinutes(-1);
                });
                foreach (Event eventt in dbevents)
                    eventt.Registers.Select(r => r.User).AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.STUDENT_EVENT_OPENING);
                await db.SaveChangesAsync();
            }
        }
    }
}