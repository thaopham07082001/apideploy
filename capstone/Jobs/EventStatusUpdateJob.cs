﻿using System.Threading.Tasks;
using capstone.database.Database;

namespace capstone.Jobs {
    public class EventStatusUpdateJob {
        public async Task UpdateEventStatus() {
            using (Database db = new Database()) {
                await db.Database.ExecuteSqlCommandAsync("UPDATE [Event] SET EventStatus = 'Closed' WHERE EventStatus = 'Happening' AND CloseDate <= GETDATE()");
                await db.Database.ExecuteSqlCommandAsync("UPDATE [Event] SET EventStatus = 'Happening' WHERE EventStatus like 'Opening' AND OpenDate <= GETDATE()");
            }
        }
    }
}