﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace capstone.Api {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Api.ApiSoap")]
    public interface ApiSoap {
        
        // CODEGEN: Generating message contract since element name memberCode from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetStudentByMemberCode", ReplyAction="*")]
        capstone.Api.GetStudentByMemberCodeResponse GetStudentByMemberCode(capstone.Api.GetStudentByMemberCodeRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetStudentByMemberCode", ReplyAction="*")]
        System.Threading.Tasks.Task<capstone.Api.GetStudentByMemberCodeResponse> GetStudentByMemberCodeAsync(capstone.Api.GetStudentByMemberCodeRequest request);
        
        // CODEGEN: Generating message contract since element name key from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetChuyenNganh", ReplyAction="*")]
        capstone.Api.GetChuyenNganhResponse GetChuyenNganh(capstone.Api.GetChuyenNganhRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetChuyenNganh", ReplyAction="*")]
        System.Threading.Tasks.Task<capstone.Api.GetChuyenNganhResponse> GetChuyenNganhAsync(capstone.Api.GetChuyenNganhRequest request);
        
        // CODEGEN: Generating message contract since element name rollnumber from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetImage", ReplyAction="*")]
        capstone.Api.GetImageResponse GetImage(capstone.Api.GetImageRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetImage", ReplyAction="*")]
        System.Threading.Tasks.Task<capstone.Api.GetImageResponse> GetImageAsync(capstone.Api.GetImageRequest request);
        
        // CODEGEN: Generating message contract since element name memberCode from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetTimeTable", ReplyAction="*")]
        capstone.Api.GetTimeTableResponse GetTimeTable(capstone.Api.GetTimeTableRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetTimeTable", ReplyAction="*")]
        System.Threading.Tasks.Task<capstone.Api.GetTimeTableResponse> GetTimeTableAsync(capstone.Api.GetTimeTableRequest request);
        
        // CODEGEN: Generating message contract since element name memberCode from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetscheduleExam", ReplyAction="*")]
        capstone.Api.GetscheduleExamResponse GetscheduleExam(capstone.Api.GetscheduleExamRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetscheduleExam", ReplyAction="*")]
        System.Threading.Tasks.Task<capstone.Api.GetscheduleExamResponse> GetscheduleExamAsync(capstone.Api.GetscheduleExamRequest request);
        
        // CODEGEN: Generating message contract since element name key from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetTimeSlot", ReplyAction="*")]
        capstone.Api.GetTimeSlotResponse GetTimeSlot(capstone.Api.GetTimeSlotRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetTimeSlot", ReplyAction="*")]
        System.Threading.Tasks.Task<capstone.Api.GetTimeSlotResponse> GetTimeSlotAsync(capstone.Api.GetTimeSlotRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetStudentByMemberCodeRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetStudentByMemberCode", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetStudentByMemberCodeRequestBody Body;
        
        public GetStudentByMemberCodeRequest() {
        }
        
        public GetStudentByMemberCodeRequest(capstone.Api.GetStudentByMemberCodeRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetStudentByMemberCodeRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string memberCode;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string key;
        
        public GetStudentByMemberCodeRequestBody() {
        }
        
        public GetStudentByMemberCodeRequestBody(string memberCode, string key) {
            this.memberCode = memberCode;
            this.key = key;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetStudentByMemberCodeResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetStudentByMemberCodeResponse", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetStudentByMemberCodeResponseBody Body;
        
        public GetStudentByMemberCodeResponse() {
        }
        
        public GetStudentByMemberCodeResponse(capstone.Api.GetStudentByMemberCodeResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetStudentByMemberCodeResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetStudentByMemberCodeResult;
        
        public GetStudentByMemberCodeResponseBody() {
        }
        
        public GetStudentByMemberCodeResponseBody(string GetStudentByMemberCodeResult) {
            this.GetStudentByMemberCodeResult = GetStudentByMemberCodeResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetChuyenNganhRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetChuyenNganh", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetChuyenNganhRequestBody Body;
        
        public GetChuyenNganhRequest() {
        }
        
        public GetChuyenNganhRequest(capstone.Api.GetChuyenNganhRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetChuyenNganhRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string key;
        
        public GetChuyenNganhRequestBody() {
        }
        
        public GetChuyenNganhRequestBody(string key) {
            this.key = key;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetChuyenNganhResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetChuyenNganhResponse", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetChuyenNganhResponseBody Body;
        
        public GetChuyenNganhResponse() {
        }
        
        public GetChuyenNganhResponse(capstone.Api.GetChuyenNganhResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetChuyenNganhResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetChuyenNganhResult;
        
        public GetChuyenNganhResponseBody() {
        }
        
        public GetChuyenNganhResponseBody(string GetChuyenNganhResult) {
            this.GetChuyenNganhResult = GetChuyenNganhResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetImageRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetImage", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetImageRequestBody Body;
        
        public GetImageRequest() {
        }
        
        public GetImageRequest(capstone.Api.GetImageRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetImageRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string rollnumber;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string key;
        
        public GetImageRequestBody() {
        }
        
        public GetImageRequestBody(string rollnumber, string key) {
            this.rollnumber = rollnumber;
            this.key = key;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetImageResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetImageResponse", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetImageResponseBody Body;
        
        public GetImageResponse() {
        }
        
        public GetImageResponse(capstone.Api.GetImageResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetImageResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public byte[] GetImageResult;
        
        public GetImageResponseBody() {
        }
        
        public GetImageResponseBody(byte[] GetImageResult) {
            this.GetImageResult = GetImageResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetTimeTableRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetTimeTable", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetTimeTableRequestBody Body;
        
        public GetTimeTableRequest() {
        }
        
        public GetTimeTableRequest(capstone.Api.GetTimeTableRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetTimeTableRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string memberCode;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public System.DateTime SDate;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public System.DateTime eDate;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string key;
        
        public GetTimeTableRequestBody() {
        }
        
        public GetTimeTableRequestBody(string memberCode, System.DateTime SDate, System.DateTime eDate, string key) {
            this.memberCode = memberCode;
            this.SDate = SDate;
            this.eDate = eDate;
            this.key = key;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetTimeTableResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetTimeTableResponse", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetTimeTableResponseBody Body;
        
        public GetTimeTableResponse() {
        }
        
        public GetTimeTableResponse(capstone.Api.GetTimeTableResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetTimeTableResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetTimeTableResult;
        
        public GetTimeTableResponseBody() {
        }
        
        public GetTimeTableResponseBody(string GetTimeTableResult) {
            this.GetTimeTableResult = GetTimeTableResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetscheduleExamRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetscheduleExam", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetscheduleExamRequestBody Body;
        
        public GetscheduleExamRequest() {
        }
        
        public GetscheduleExamRequest(capstone.Api.GetscheduleExamRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetscheduleExamRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string memberCode;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public System.DateTime fdate;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public System.DateTime tDate;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string key;
        
        public GetscheduleExamRequestBody() {
        }
        
        public GetscheduleExamRequestBody(string memberCode, System.DateTime fdate, System.DateTime tDate, string key) {
            this.memberCode = memberCode;
            this.fdate = fdate;
            this.tDate = tDate;
            this.key = key;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetscheduleExamResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetscheduleExamResponse", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetscheduleExamResponseBody Body;
        
        public GetscheduleExamResponse() {
        }
        
        public GetscheduleExamResponse(capstone.Api.GetscheduleExamResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetscheduleExamResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetscheduleExamResult;
        
        public GetscheduleExamResponseBody() {
        }
        
        public GetscheduleExamResponseBody(string GetscheduleExamResult) {
            this.GetscheduleExamResult = GetscheduleExamResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetTimeSlotRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetTimeSlot", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetTimeSlotRequestBody Body;
        
        public GetTimeSlotRequest() {
        }
        
        public GetTimeSlotRequest(capstone.Api.GetTimeSlotRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetTimeSlotRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string key;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public byte slot;
        
        public GetTimeSlotRequestBody() {
        }
        
        public GetTimeSlotRequestBody(string key, byte slot) {
            this.key = key;
            this.slot = slot;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetTimeSlotResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetTimeSlotResponse", Namespace="http://tempuri.org/", Order=0)]
        public capstone.Api.GetTimeSlotResponseBody Body;
        
        public GetTimeSlotResponse() {
        }
        
        public GetTimeSlotResponse(capstone.Api.GetTimeSlotResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetTimeSlotResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetTimeSlotResult;
        
        public GetTimeSlotResponseBody() {
        }
        
        public GetTimeSlotResponseBody(string GetTimeSlotResult) {
            this.GetTimeSlotResult = GetTimeSlotResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ApiSoapChannel : capstone.Api.ApiSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ApiSoapClient : System.ServiceModel.ClientBase<capstone.Api.ApiSoap>, capstone.Api.ApiSoap {
        
        public ApiSoapClient() {
        }
        
        public ApiSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ApiSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ApiSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ApiSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        capstone.Api.GetStudentByMemberCodeResponse capstone.Api.ApiSoap.GetStudentByMemberCode(capstone.Api.GetStudentByMemberCodeRequest request) {
            return base.Channel.GetStudentByMemberCode(request);
        }
        
        public string GetStudentByMemberCode(string memberCode, string key) {
            capstone.Api.GetStudentByMemberCodeRequest inValue = new capstone.Api.GetStudentByMemberCodeRequest();
            inValue.Body = new capstone.Api.GetStudentByMemberCodeRequestBody();
            inValue.Body.memberCode = memberCode;
            inValue.Body.key = key;
            capstone.Api.GetStudentByMemberCodeResponse retVal = ((capstone.Api.ApiSoap)(this)).GetStudentByMemberCode(inValue);
            return retVal.Body.GetStudentByMemberCodeResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<capstone.Api.GetStudentByMemberCodeResponse> capstone.Api.ApiSoap.GetStudentByMemberCodeAsync(capstone.Api.GetStudentByMemberCodeRequest request) {
            return base.Channel.GetStudentByMemberCodeAsync(request);
        }
        
        public System.Threading.Tasks.Task<capstone.Api.GetStudentByMemberCodeResponse> GetStudentByMemberCodeAsync(string memberCode, string key) {
            capstone.Api.GetStudentByMemberCodeRequest inValue = new capstone.Api.GetStudentByMemberCodeRequest();
            inValue.Body = new capstone.Api.GetStudentByMemberCodeRequestBody();
            inValue.Body.memberCode = memberCode;
            inValue.Body.key = key;
            return ((capstone.Api.ApiSoap)(this)).GetStudentByMemberCodeAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        capstone.Api.GetChuyenNganhResponse capstone.Api.ApiSoap.GetChuyenNganh(capstone.Api.GetChuyenNganhRequest request) {
            return base.Channel.GetChuyenNganh(request);
        }
        
        public string GetChuyenNganh(string key) {
            capstone.Api.GetChuyenNganhRequest inValue = new capstone.Api.GetChuyenNganhRequest();
            inValue.Body = new capstone.Api.GetChuyenNganhRequestBody();
            inValue.Body.key = key;
            capstone.Api.GetChuyenNganhResponse retVal = ((capstone.Api.ApiSoap)(this)).GetChuyenNganh(inValue);
            return retVal.Body.GetChuyenNganhResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<capstone.Api.GetChuyenNganhResponse> capstone.Api.ApiSoap.GetChuyenNganhAsync(capstone.Api.GetChuyenNganhRequest request) {
            return base.Channel.GetChuyenNganhAsync(request);
        }
        
        public System.Threading.Tasks.Task<capstone.Api.GetChuyenNganhResponse> GetChuyenNganhAsync(string key) {
            capstone.Api.GetChuyenNganhRequest inValue = new capstone.Api.GetChuyenNganhRequest();
            inValue.Body = new capstone.Api.GetChuyenNganhRequestBody();
            inValue.Body.key = key;
            return ((capstone.Api.ApiSoap)(this)).GetChuyenNganhAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        capstone.Api.GetImageResponse capstone.Api.ApiSoap.GetImage(capstone.Api.GetImageRequest request) {
            return base.Channel.GetImage(request);
        }
        
        public byte[] GetImage(string rollnumber, string key) {
            capstone.Api.GetImageRequest inValue = new capstone.Api.GetImageRequest();
            inValue.Body = new capstone.Api.GetImageRequestBody();
            inValue.Body.rollnumber = rollnumber;
            inValue.Body.key = key;
            capstone.Api.GetImageResponse retVal = ((capstone.Api.ApiSoap)(this)).GetImage(inValue);
            return retVal.Body.GetImageResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<capstone.Api.GetImageResponse> capstone.Api.ApiSoap.GetImageAsync(capstone.Api.GetImageRequest request) {
            return base.Channel.GetImageAsync(request);
        }
        
        public System.Threading.Tasks.Task<capstone.Api.GetImageResponse> GetImageAsync(string rollnumber, string key) {
            capstone.Api.GetImageRequest inValue = new capstone.Api.GetImageRequest();
            inValue.Body = new capstone.Api.GetImageRequestBody();
            inValue.Body.rollnumber = rollnumber;
            inValue.Body.key = key;
            return ((capstone.Api.ApiSoap)(this)).GetImageAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        capstone.Api.GetTimeTableResponse capstone.Api.ApiSoap.GetTimeTable(capstone.Api.GetTimeTableRequest request) {
            return base.Channel.GetTimeTable(request);
        }
        
        public string GetTimeTable(string memberCode, System.DateTime SDate, System.DateTime eDate, string key) {
            capstone.Api.GetTimeTableRequest inValue = new capstone.Api.GetTimeTableRequest();
            inValue.Body = new capstone.Api.GetTimeTableRequestBody();
            inValue.Body.memberCode = memberCode;
            inValue.Body.SDate = SDate;
            inValue.Body.eDate = eDate;
            inValue.Body.key = key;
            capstone.Api.GetTimeTableResponse retVal = ((capstone.Api.ApiSoap)(this)).GetTimeTable(inValue);
            return retVal.Body.GetTimeTableResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<capstone.Api.GetTimeTableResponse> capstone.Api.ApiSoap.GetTimeTableAsync(capstone.Api.GetTimeTableRequest request) {
            return base.Channel.GetTimeTableAsync(request);
        }
        
        public System.Threading.Tasks.Task<capstone.Api.GetTimeTableResponse> GetTimeTableAsync(string memberCode, System.DateTime SDate, System.DateTime eDate, string key) {
            capstone.Api.GetTimeTableRequest inValue = new capstone.Api.GetTimeTableRequest();
            inValue.Body = new capstone.Api.GetTimeTableRequestBody();
            inValue.Body.memberCode = memberCode;
            inValue.Body.SDate = SDate;
            inValue.Body.eDate = eDate;
            inValue.Body.key = key;
            return ((capstone.Api.ApiSoap)(this)).GetTimeTableAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        capstone.Api.GetscheduleExamResponse capstone.Api.ApiSoap.GetscheduleExam(capstone.Api.GetscheduleExamRequest request) {
            return base.Channel.GetscheduleExam(request);
        }
        
        public string GetscheduleExam(string memberCode, System.DateTime fdate, System.DateTime tDate, string key) {
            capstone.Api.GetscheduleExamRequest inValue = new capstone.Api.GetscheduleExamRequest();
            inValue.Body = new capstone.Api.GetscheduleExamRequestBody();
            inValue.Body.memberCode = memberCode;
            inValue.Body.fdate = fdate;
            inValue.Body.tDate = tDate;
            inValue.Body.key = key;
            capstone.Api.GetscheduleExamResponse retVal = ((capstone.Api.ApiSoap)(this)).GetscheduleExam(inValue);
            return retVal.Body.GetscheduleExamResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<capstone.Api.GetscheduleExamResponse> capstone.Api.ApiSoap.GetscheduleExamAsync(capstone.Api.GetscheduleExamRequest request) {
            return base.Channel.GetscheduleExamAsync(request);
        }
        
        public System.Threading.Tasks.Task<capstone.Api.GetscheduleExamResponse> GetscheduleExamAsync(string memberCode, System.DateTime fdate, System.DateTime tDate, string key) {
            capstone.Api.GetscheduleExamRequest inValue = new capstone.Api.GetscheduleExamRequest();
            inValue.Body = new capstone.Api.GetscheduleExamRequestBody();
            inValue.Body.memberCode = memberCode;
            inValue.Body.fdate = fdate;
            inValue.Body.tDate = tDate;
            inValue.Body.key = key;
            return ((capstone.Api.ApiSoap)(this)).GetscheduleExamAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        capstone.Api.GetTimeSlotResponse capstone.Api.ApiSoap.GetTimeSlot(capstone.Api.GetTimeSlotRequest request) {
            return base.Channel.GetTimeSlot(request);
        }
        
        public string GetTimeSlot(string key, byte slot) {
            capstone.Api.GetTimeSlotRequest inValue = new capstone.Api.GetTimeSlotRequest();
            inValue.Body = new capstone.Api.GetTimeSlotRequestBody();
            inValue.Body.key = key;
            inValue.Body.slot = slot;
            capstone.Api.GetTimeSlotResponse retVal = ((capstone.Api.ApiSoap)(this)).GetTimeSlot(inValue);
            return retVal.Body.GetTimeSlotResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<capstone.Api.GetTimeSlotResponse> capstone.Api.ApiSoap.GetTimeSlotAsync(capstone.Api.GetTimeSlotRequest request) {
            return base.Channel.GetTimeSlotAsync(request);
        }
        
        public System.Threading.Tasks.Task<capstone.Api.GetTimeSlotResponse> GetTimeSlotAsync(string key, byte slot) {
            capstone.Api.GetTimeSlotRequest inValue = new capstone.Api.GetTimeSlotRequest();
            inValue.Body = new capstone.Api.GetTimeSlotRequestBody();
            inValue.Body.key = key;
            inValue.Body.slot = slot;
            return ((capstone.Api.ApiSoap)(this)).GetTimeSlotAsync(inValue);
        }
    }
}
