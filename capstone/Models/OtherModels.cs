﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace capstone.Models {
    public class ApplicationUser : IdentityUser {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager) {
            ClaimsIdentity userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
        public ApplicationDbContext() : base("Database", false) { }

        public static ApplicationDbContext Create() {
            return new ApplicationDbContext();
        }
    }

    public class ApplicationJobContext : DbContext {
        public ApplicationJobContext() : base("Database") {
            Database.SetInitializer<ApplicationJobContext>(null);
            Database.CreateIfNotExists();
        }
    }
}