﻿namespace capstone.Models
{
    public class EventDetailViewModel : EventViewModel
    {
        public int TotalAmountTicket { get; set; }
        public string EventType { get; set; }
    }
}