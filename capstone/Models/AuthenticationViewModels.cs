﻿using System.ComponentModel.DataAnnotations;

namespace capstone.Models {
    public class ExternalLoginListViewModel {
        public string ReturnUrl { get; set; }
    }

    public class LoginViewModel {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
