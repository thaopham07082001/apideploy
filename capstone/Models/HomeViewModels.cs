﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using capstone.Helpers;

namespace capstone.Models {
    public class CalendarModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Stack { get; set; }
        public bool IsPrivate { get; set; }
    }

    public class WeekModel {
        public int PreviousWeek { get; set; }
        public int PreviousYear { get; set; }
        public int NextWeek { get; set; }
        public int NextYear { get; set; }
    }

    public class EventModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CoverImage { get; set; }
        public bool IsPublic { get; set; }
        public bool IsFeatured { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string Place { get; set; }
        public UserModel Organizer { get; set; }
        public GroupModel Group { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime RegisterEndDate { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime CloseDate { get; set; }
        public bool IsRegistered { get; set; }
        public int RegisterCount { get; set; }
        public int? RegisterMax { get; set; }
        public bool IsOrganizerOnly { get; set; }
        public bool CanRegisterByGroup { get; set; }
    }

    public class UserModel {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileImage { get; set; }
    }

    public class GroupModel {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class NotificationModel {
        public UserModel Subject { get; set; }
        public ObjectModel Object { get; set; }
        public string Content { get; set; }
        public DateTime NotifiedDate { get; set; }
    }

    public class ObjectModel {
        public string Name { get; set; }
        public string Link { get; set; }
    }

    public class EventSearchFilterViewModel {
        public string Search { get; set; }
        public int? Category { get; set; }
        [Display(Name = "Organized by")]
        public int? OrganizerOrGroup { get; set; }
        public bool IsGroup { get; set; }
        public string Status { get; set; }
        public string Range { get; set; }
        [DataType(DataType.Date)]
        public string From { get; set; }
        [DataType(DataType.Date)]
        public string To { get; set; }
        public string Display { get; set; }
    }

    public class ManageYourProfileViewModel {
        public int ProfileId { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [ValidateImage]
        public HttpPostedFileBase Image { get; set; }
        public string ProfileImage { get; set; }

        [Required]
        [StringLength(11)]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone no.")]
        public string Phone { get; set; }
    }

    public class CategoryModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int CreatorId { get; set; }
    }

    public class FacebookModel {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
    }

    public class BookmarkViewModel
    {
        public int UserId { get; set; }
        public int EventId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}