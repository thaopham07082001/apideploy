﻿using System;
using System.ComponentModel.DataAnnotations;

namespace capstone.Models
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public bool IsStudent { get; set; }
        public bool IsOrganizer { get; set; }
        public bool IsManager { get; set; }
        public bool IsAdmin { get; set; }
    }

    public class AccountSearchFilterViewModel
    {
        [Display(Name = "Search")]
        public string Search { get; set; }
        [Display(Name = "Organizer")]
        public bool IsOrganizer { get; set; }
        [Display(Name = "Manager")]
        public bool IsManager { get; set; }
        [Display(Name = "Administrator")]
        public bool IsAdmin { get; set; }
    }

    public class CreateAccountViewModel
    {
        [Required]
        [EmailAddress]
        [StringLength(30)]
        public string Email { get; set; }
        [Display(Name = "Student")]
        public bool IsStudent { get; set; }
        [Display(Name = "Organizer")]
        public bool IsOrganizer { get; set; }
        [Display(Name = "Manager")]
        public bool IsManager { get; set; }
        [Display(Name = "Administrator")]
        public bool IsAdmin { get; set; }
    }


    public class EditAccountViewModel
    {
        [Required]
        public int Id { get; set; }
        public string Email { get; set; }
        [Display(Name = "Student")]
        public bool IsStudent { get; set; }
        [Display(Name = "Organizer")]
        public bool IsOrganizer { get; set; }
        [Display(Name = "Manager")]
        public bool IsManager { get; set; }
        [Display(Name = "Administrator")]
        public bool IsAdmin { get; set; }
    }

    public class CampusViewModel
    {
        [Required]
        public int CampusId { get; set; }
        [Required]
        [StringLength(100)]
        [Display(Name = "Campus")]
        public string CampusName { get; set; }
    }

    public class BuildingViewModel
    {
        [Required]
        public int BuildingId { get; set; }
        public string BuildingName { get; set; }
        public int CampusId { get; set; }
        public virtual CampusViewModel Campus { get; set; }
    }

    public class SmartTvViewModel
    {
        [Required]
        public int id { get; set; }
        public string TvCodeId { get; set; }
        public string TvName { get; set; }
        public int BuildingId { get; set; }
        public virtual BuildingViewModel Building { get; set; }
    }
}