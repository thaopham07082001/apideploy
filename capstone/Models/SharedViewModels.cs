﻿namespace capstone.Models {
    public class PageModel {
        public string Action { get; set; }
        public int Current { get; set; }
        public int Size { get; set; }
        public int Max { get; set; }
    }

    public class MetaModel {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }
}