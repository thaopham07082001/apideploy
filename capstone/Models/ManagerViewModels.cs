﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using capstone.Helpers;

namespace capstone.Models {
    public class RegisterModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredDate { get; set; }
    }

    public class CheckinModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime CheckedinDate { get; set; }
    }

    public class UserSearchViewModel {
        public string Search { get; set; }
    }

    public class SearchOrganizersModel {
        public int Id { get; set; }
        public string Email { get; set; }
    }

    public class CreateGroupModel {
        public int Id { get; set; }
        [Display(Name = "Group name")]
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [ValidateImage]
        [Display(Name = "Group image")]
        public HttpPostedFileBase Image { get; set; }
        public string GroupImage { get; set; }
        public string Description { get; set; }
        [EmailAddress]
        [StringLength(100)]
        [Display(Name = "Group mail")]
        public string Mail { get; set; }
        [Required]
        public string Leader { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Founded date")]
        public DateTime? FoundedDate { get; set; }
    }
}