﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using capstone.Helpers;

namespace capstone.Models
{
    public class CreateEventViewModel
    {
        public int EventId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [ValidateImage]
        [Display(Name = "Event cover image")]
        public HttpPostedFileBase Image { get; set; }
        public string CoverImage { get; set; }

        public bool PublicTv { get; set; }
        public bool Public { get; set; }

        [Display(Name = "For organizers only")]
        public bool OrganizerOnly { get; set; }
        [Display(Name = "Target participants")]
        public List<string> Participants { get; set; }
        public string OtherParticipants { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Event description")]
        public string DescriptionHtml { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1753-01-01", "9999-12-31", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event open date")]
        public DateTime OpenDate { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh-mm-tt}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event open time")]
        public DateTime OpenTime { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1753-01-01", "9999-12-31", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event close date")]
        public DateTime CloseDate { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh-mm-tt}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event close time")]
        public DateTime CloseTime { get; set; }
        public string FormLink { get; set; }
        public string FormRegister { get; set; }
        public string FormResult { get; set; }
        [Required]
        [StringLength(100)]
        public string Place { get; set; }
        [Required]
        [Range(0, 10000000000)]
        public decimal Fee { get; set; }
        [Required]
        [Display(Name = "Category")]
        public List<int> Category { get; set; }
        [Display(Name = "Event family")]
        public string EventFamily { get; set; }
        [Required]
        [Display(Name = "Group")]
        public int GroupId { get; set; }
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1753-01-01", "9999-12-31", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event register close date")]
        public DateTime? RegisterEndDate { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh-mm-tt}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event register close time")]
        public DateTime? RegisterEndTime { get; set; }
        [Range(0, 10000000000)]
        [Display(Name = "Maximum number of registers")]
        public int? RegisterNumber { get; set; }
        [Display(Name = "Target group")]
        public List<int> TargetGroup { get; set; }
        [Display(Name = "Campus")]
        public int Campus { get; set; }
        [Display(Name = "Link cho video")]
        public string UrlLink { get; set; }
        [Display(Name = "SmartTv")]
        public List<int> SmartTv { get; set; }
        [Display(Name = "StaffId")]
        public List<int> Staff { get; set; }
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1753-01-01", "9999-12-31", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event banner start date")]
        public DateTime StartDateBanner { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh-mm-tt}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event banner start time")]
        public DateTime StartTimeBanner { get; set; }
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1753-01-01", "9999-12-31", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event banner end date")]
        public DateTime EndDateBanner { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh-mm-tt}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event banner end time")]
        public DateTime EndTimeBanner { get; set; }
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1753-01-01", "9999-12-31", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event video start date")]
        public DateTime StartDateVideo { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh-mm-tt}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event video start time")]
        public DateTime StartTimeVideo { get; set; }
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1753-01-01", "9999-12-31", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event banner end date")]
        public DateTime EndDateVideo { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:hh-mm-tt}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event banner end time")]
        public DateTime EndTimeVideo { get; set; }
    }

    public class StaffModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class SmartTvModel
    {
        public int TvId { get; set; }
        public string TvName { get; set; }
    }

    public class BuildingModel
    {
        public int BuildingId { get; set; }
        public string BuildingName { get; set; }
    }

    public class FamilyModel
    {
        public int FamilyId { get; set; }
        public string FamilyName { get; set; }
    }

    public class RegisterCheckinModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public bool CheckedIn { get; set; }
    }

    public class MemberModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime JoinedDate { get; set; }
        public string Ticks { get; set; }
        public string strJoinedDate { get; set; }
        public string Role { get; set; }
    }
}