﻿using System;

namespace capstone.Models {
    public class FeedbackModel {
        public string Key { get; set; }
        public string Username { get; set; }
        public string UserImage { get; set; }
        public double? Value { get; set; }
        public string txtCreatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Content { get; set; }
        public double? AvgRating { get; set; }
        public int FeedbackCount { get; set; }
        public bool IsExternal { get; set; }
    }
    public class TicketDetailModels
    {
        public string event_name { get; set; }
        public DateTime opent_date { get; set; }
        public string location { get; set; }
        public DateTime booked_date { get; set; }
        public string qr_code { get; set; }
    }

    public class EventViewModel
    {
        public int EventId { get; set; }

        public string EventName { get; set; }

        public int? FamilyId { get; set; }

        public string CoverImage { get; set; }

        public bool IsPublic { get; set; }

        public bool IsFeatured { get; set; }

        public string EventStatus { get; set; }

        public string EventDescription { get; set; }

        public string EventDescriptionHtml { get; set; }

        public string EventPlace { get; set; }

        public decimal EventFee { get; set; }

        public int? OrganizerId { get; set; }

        public int? GroupId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ManagerId { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime? RegisterEndDate { get; set; }

        public DateTime OpenDate { get; set; }

        public DateTime CloseDate { get; set; }

        public int? RegisterMax { get; set; }

        public bool IsOrganizerOnly { get; set; }

        public string Participants { get; set; }

        public string TargetGroup { get; set; }

        public string UrlLink { get; set; }

        public DateTime? StartDateBanner { get; set; }

        public DateTime? EndDateBanner { get; set; }

        public DateTime? StartDateVideo { get; set; }

        public DateTime? EndDateVidep { get; set; }

        public string StandeeImage { get; set; }

        public int CampusId { get; set; }

        public string CampusName { get; set; }
    }
}