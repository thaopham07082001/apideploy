﻿using capstone.database.Database;
using capstone.Results;
using Google.Apis.Auth;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace capstone.API
{
    public class AuthController : ApiController
    {
        private readonly Database _db;

        public AuthController()
        {
            _db = new Database();
        }

        [Route("api/auth/testConnection")]
        [HttpGet]
        public IHttpActionResult TestConnection(string item)
        {
            if (_db == null)
                return Json(
                    new
                    {
                        status = "Error",
                        code = 500,
                        message = "Error while getting connection.",
                        data = "null"
                    });


            return Json(new
            {
                status = "Error",
                code = 200,
                message = "Connection Ready.",
                data = new
                {
                    item
                },
            });
        }

        [Route("api/auth/loginQr")]
        [HttpGet]
        public async Task<IHttpActionResult> LoginQrAsync(string idGoogleToken)
        {
            try
            {
                var payload = await GoogleJsonWebSignature.ValidateAsync(idGoogleToken);

                // Check if user authenticated
                var email = payload.Email;
                var domain = email.Split('@')[1];
                //if (!domain.Equals("fpt.edu.vn")) return BadRequest("Email is invalid");

                //TODO: Should change email domain here.
                if (!domain.Equals("fpt.edu.vn"))
                    throw new Exception("Invalid email while login with goo");

                //var user = new UserLoginViewModel() {Email=email, Name=payload.Name,Picture=payload.Picture };
                //if (domain.Equals("mail.com"))
                //Generate JWT token
                var token = await CreateTokenForQrLoginAsync(email);
                return Json(new ResponseResult
                {
                    status = "Success",
                    code = Results.StatusCode.Success,
                    message = "Authentication Success. Below is access token.",
                    data = token
                });
            }
            catch (Exception ex)
            {
                // return BadRequest("invalid google token");
                return Json(
                    new ResponseResult
                    {
                        status = "Error",
                        code = Results.StatusCode.InternalServerError,
                        message = $"Error: {ex}",
                        data = null
                    });
            }
            // string token = Request.Headers["Authorization"].ToString().Remove(0, 7);
            //// Validate username, password: not empty, not to short or long

            //return BadRequest("Username or password is incorrect");
        }

        private static Task<string> CreateTokenForQrLoginAsync(string email)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, email)
            };

            var key = ConfigurationManager.AppSettings["JwtKey"];
            var issuer = ConfigurationManager.AppSettings["JwtIssuer"];

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer,
                issuer,
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
            );

            return Task.FromResult(new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}