﻿using capstone.database.Database;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using capstone.Results;

namespace capstone.Api
{

    [Route("api/events")]
    public class EventsController : ApiController
    {
        private readonly Database _db;
        private int _code;
        private string _message;
        public EventsController()
        {
            _db = new Database();
            _code = 0;
            _message = string.Empty;
        }

        /// <summary>
        /// trả về list event người đó đang support
        /// </summary>
        /// <returns></returns>
        [Route("api/events/get")]
        [HttpGet]
        public IHttpActionResult GetEventByUser()
        {
            var email = GetEmail();
            if (string.IsNullOrEmpty(email))
            {
                return Json(
                    new ResponseResult
                    {
                        status = "Error",
                        code = Results.StatusCode.InternalServerError,
                        message = "Error while authorizing email.",
                        data = null
                    });
            }

            // User A
            var userId = _db.Users
                .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()))?.UserId;
            if (userId == null)
            {
                return Json(
                   new
                   {
                       status = "Error",
                       code = 400,
                       message = "User not found",
                       data = "null"
                   });
            }
            // SUPPORTER
            //  var userEventRoles = _db.EventRoles.Where(x => x.UserId == userId).ToList();
            var eventIds = _db.EventRoles.Where(x => x.UserId == userId).Select(x => x.EventId).ToList();
            var listEvents = new List<dynamic>();
            foreach (var item in eventIds)
            {
                // SUPPORTER

                // EVENT JS, EVENT AI
                var events = _db.Events
                    .Where(x => x.EventId == item)
                    .ToList();
                var ticketScanned = _db.Checkins.Count(x => x.EventId == item);
                var eventDetailViewModels = events
                    .Join(
                        _db.Registers,
                        e => e.EventId,
                        r => r.EventId,
                        (e, r) => new { e, r })
                    .GroupBy(t => new
                    {
                        t.e.EventId,
                        t.e.EventName,
                        t.e.CoverImage,
                        t.e.EventPlace,
                        t.e.OpenDate,
                        t.e.CloseDate,
                        t.e.EventDescription,
                        t.e.EventStatus
                    }, t => t.e)
                    .Select(gr => new
                    {
                        gr.Key.EventId,
                        gr.Key.EventName,
                        gr.Key.CoverImage,
                        gr.Key.EventPlace,
                        gr.Key.OpenDate,
                        gr.Key.CloseDate,
                        gr.Key.EventDescription,
                        EventType = GetEventTypeByEventStatus(gr.Key.EventStatus),
                        TotalAmountTicket = gr.Count(),
                        TicketScanned = ticketScanned
                    })
                    .OrderBy(x => x.OpenDate)
                    .ToList();

                eventDetailViewModels.ForEach(x =>
                    listEvents.Add(x));
                _code = 200;
                _message = "Get data successful";
            }

            return Json(new
            {
                status = "Success",
                code = _code,
                message = _message,
                data = listEvents
            });
        }

        [Route("api/events/getEventById")]
        [HttpGet]
        public IHttpActionResult GetEventByEventId(int? eventId)
        {
            if (eventId == null || eventId == 0)
            {
                return Json(
                    new ResponseResult
                    {
                        status = "Not Found",
                        code = Results.StatusCode.NotFound,
                        message = "EventID empty or null, please re-enter.",
                        data = null
                    });
            }

            // Get events
            var events = _db.Events
                .Where(x => x.EventId == eventId)
                .ToList();

            var ticketScanned = _db.Checkins.Count(x => x.EventId == eventId);
            var eventDetailViewModel = events
                .Join(
                    _db.Registers,
                    e => e.EventId,
                    r => r.EventId,
                    (e, r) => new { e, r })
                .GroupBy(t => new
                {
                    t.e.EventId,
                    t.e.EventName,
                    t.e.CoverImage,
                    t.e.EventPlace,
                    t.e.OpenDate,
                    t.e.CloseDate,
                    t.e.EventDescription,
                    t.e.EventStatus
                }, t => t.e)
                .Select(gr => new
                {
                    gr.Key.EventId,
                    gr.Key.EventName,
                    gr.Key.CoverImage,
                    gr.Key.EventPlace,
                    gr.Key.OpenDate,
                    gr.Key.CloseDate,
                    gr.Key.EventDescription,
                    EventType = GetEventTypeByEventStatus(gr.Key.EventStatus),
                    TotalAmountTicket = gr.Count(),
                    TicketScanned = ticketScanned
                })
                .OrderBy(x => x.OpenDate)
                .ToList();


            return Json(new
            {
                status = "Success",
                code = _code,
                message = _message,
                data = eventDetailViewModel
            });
        }

        private static string GetEventTypeByEventStatus(string eventStatus)
        {
            var output = string.Empty;

            switch (eventStatus)
            {
                case "Closed":
                    output = "ended";
                    break;
                case "Opening":
                    output = "incoming";
                    break;
                case "Happening":
                    output = "current";
                    break;
            }

            return output;
        }

        public string GetEmail()
        {
            var httpContext = HttpContext.Current;
            var handler = new JwtSecurityTokenHandler();
            var authHeader = httpContext.Request.Headers["Authorization"];
            authHeader = authHeader.Replace("Bearer ", "");
            //Bearer + token (getToken)
            //var jsonToken = handler.ReadToken(authHeader);
            if (!(handler.ReadToken(authHeader) is JwtSecurityToken tokenS)) return string.Empty;
            var email = tokenS.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
            return email;
        }
    }
}