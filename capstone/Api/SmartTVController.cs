﻿using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Results;
using Google.Apis.Auth;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace capstone.API
{
    [Route("api/smartTv")]
    public class SmartTvController : ApiController
    {
        private readonly Database _db;
        private StatusCode _statusCode;
        private readonly string _role;


        public SmartTvController()
        {
            _db = new Database();
            _role = "Admin";
        }

        [Route("api/auth/smartTv/getTvAccessToken")]
        [HttpGet]
        public async Task<IHttpActionResult> GetTvAccessTokenAsync(string idGoogleToken)
        {
            try
            {
                var payload = await GoogleJsonWebSignature.ValidateAsync(idGoogleToken);

                // Check if user authenticated
                var email = payload.Email;
                var domain = email.Split('@')[1];
                //if (!domain.Equals("fpt.edu.vn")) return BadRequest("Email is invalid");

                //TODO: Should change email domain here.
                if (!domain.Equals("fpt.edu.vn") || string.IsNullOrEmpty(email))
                {
                    _statusCode = Results.StatusCode.Unauthorized;
                    throw new Exception("Email is invalid");
                }

                // Get user
                var user = _db.Users.FirstOrDefault(x =>
                    x.Email.ToLower().Equals(email.ToLower()) && x.IsEnabled == true);
                if (user == null)
                {
                    _statusCode = Results.StatusCode.InternalServerError;
                    throw new Exception("User is invalid");
                }

                // Check admin roles
                var isAdmin = user.IsAdmin;
                if (!isAdmin)
                {
                    _statusCode = Results.StatusCode.Unauthorized;
                    throw new Exception("Unauthorized Account");
                }

                // Get userProfile
                var userProfile = user.UserProfile;
                if (userProfile == null)
                {
                    _statusCode = Results.StatusCode.Unauthorized;
                    throw new Exception("UserProfile is invalid");
                }

                // Get Campus
                var userCampusName = userProfile.Campus;
                var campusMapping = CampusMapping(userCampusName);
                var campus = _db.Campuses
                    .FirstOrDefault(x => x.CampusName.Equals(campusMapping));

                if (campus != null)
                {
                    var campusId = campus.CampusId;

                    // Get building
                    var buildings = _db.Buildings
                        .Where(x => x.CampusId.Equals(campusId))
                        .ToList();

                    //Generate JWT token
                    var token = await CreateTokenForTvLoginAsync(email, _role);


                    if (!string.IsNullOrEmpty(token))
                        return Json(new
                        {
                            status = "Success",
                            code = 200,
                            message = "Authentication Success. Below is access token.",
                            data = new
                            {
                                accessToken = token,
                                campus = userCampusName,
                                buildings = buildings.Select(b => b.BuildingName).ToList()
                            }
                        });
                }

                _statusCode = Results.StatusCode.InternalServerError;
                throw new Exception("Invalid Token");
            }
            catch (Exception ex)
            {
                // return BadRequest("invalid google token");
                return Json(
                    new ResponseResult
                    {
                        status = "Error",
                        code = _statusCode,
                        message = $"Error: {ex}",
                        data = null
                    });
            }
            // string token = Request.Headers["Authorization"].ToString().Remove(0, 7);
            //// Validate username, password: not empty, not to short or long

            //return BadRequest("Username or password is incorrect");
        }

        private static Task<string> CreateTokenForTvLoginAsync(
            string email, string role)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, email),
                new Claim(ClaimTypes.Role, role),
            };

            var key = ConfigurationManager.AppSettings["JwtKey"];
            var issuer = ConfigurationManager.AppSettings["JwtIssuer"];

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer,
                issuer,
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
            );

            return Task.FromResult(new JwtSecurityTokenHandler().WriteToken(token));
        }

        private static string CampusMapping(string campusName)
        {
            switch (campusName)
            {
                case "Hoa Lac":
                case "Hoà Lạc":
                case "FU-HL":
                    return "FU-HL";
                default:
                    return string.Empty; ;
            }
        }

        private static string BuildingMapping(string buildingName)
        {
            switch (buildingName)
            {
                case "AL":
                    return "ALPHA";
                case "BE":
                    return "BETA";
                case "GA":
                    return "GAMMA";
                case "DE":
                    return "DELTA";
                default:
                    return string.Empty;
            }
        }

        [Route("api/smartTv/insertTvCodeId")]
        [HttpPost]
        public IHttpActionResult InsertTvCodeId(
            string tvCodeId)
        {
            if (string.IsNullOrEmpty(tvCodeId))
            {
                return Json(new ResponseResult
                {
                    status = "Not Found",
                    code = Results.StatusCode.NotFound,
                    message = "Missing tvCodeId. Please Re-Check",
                    data = null
                });
            }

            var isAdmin = IsAdministrator();
            if (!isAdmin)
            {
                return Json(new ResponseResult
                {
                    status = "Unauthorized",
                    code = Results.StatusCode.Unauthorized,
                    message = "Invalid user while login",
                    data = null
                });
            }


            // HL_BE_UniqueId
            var tvCodeParts = tvCodeId.Split('_');

            //var campusPreCode = tvCodeParts[0];
            var buildingPreCode = tvCodeParts[1];
            var uniqueIdPreCode = tvCodeParts[tvCodeParts.Length - 1];

            var buildingMapping = BuildingMapping(buildingPreCode);
            var building = _db.Buildings.FirstOrDefault(x =>
                x.BuildingName.ToUpper().Equals(buildingMapping.ToUpper()));


            try
            {
                if (!string.IsNullOrEmpty(uniqueIdPreCode)
                    && building != null)
                {
                    var smartTv = _db.SmartTVs.FirstOrDefault(x =>
                        x.TvName.ToLower().Equals(uniqueIdPreCode.ToLower()));

                    // Check Tv existed?
                    if (smartTv == null)
                    {
                        // Case add new
                        _db.SmartTVs.Add(
                            new SmartTV
                            {
                                TvCodeId = tvCodeId,
                                TvName = uniqueIdPreCode,
                                Building = building,
                            });
                    }
                    else
                    {
                        if (!smartTv.TvCodeId.ToLower().Equals(tvCodeId.ToLower()))
                        {
                            // Case update existed
                            smartTv.TvCodeId = tvCodeId;
                            smartTv.Building = building;
                            _db.SmartTVs.AddOrUpdate(smartTv);
                        }
                        else
                        {
                            throw new Exception("This SmartTv already existed.");
                        }
                    }

                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception("Building or UniqueID null");
                }
            }
            catch (Exception e)
            {
                return Json(new ResponseResult
                {
                    status = "Error",
                    code = Results.StatusCode.InternalServerError,
                    message = $"Error: {e}",
                    data = null
                });
            }

            return Json(new ResponseResult
            {
                status = "Success",
                code = Results.StatusCode.CreateSuccess,
                message = "Insert smart tv information success.",
                data = null
            });
        }


        [Route("api/smartTv/getListEvent")]
        [HttpGet]
        public IHttpActionResult GetListEventByTvCodeId(string tvCodeId)
        {
            var isAdmin = IsAdministrator();
            if (!isAdmin)
            {
                return Json(new ResponseResult
                {
                    status = "Access Denied",
                    code = Results.StatusCode.Unauthorized,
                    message = "Access Denied. This function required higher permission.",
                    data = null
                });
            }

            var tv = _db.SmartTVs.FirstOrDefault(t => t.TvCodeId.ToLower().Equals(tvCodeId.ToLower()));
            if (tv == null)
            {
                return Json(new ResponseResult
                {
                    status = "Not Found",
                    code = Results.StatusCode.NotFound,
                    message = "SmartTv does not existed",
                    data = null
                });
            }

            var eventFilter = new List<string>
            {
                EventStatusConstants.DRAFT,
                EventStatusConstants.PENDING,
                EventStatusConstants.CANCELLED,
                EventStatusConstants.REJECTED,
                EventStatusConstants.DELETED,
            };

            var tvEvents = tv.Events.Where(e =>
                !eventFilter.Contains(e.EventStatus)).ToList();

            var currentUrl = HttpContext.Current.Request.Url.ToString();
            var uri = new Uri(currentUrl);
            var domain = uri.Host;
            const string eventDetailUrl = "/detail?id=";
            var events = tvEvents.Select(e => new
            {
                EventUrl = domain + eventDetailUrl + e.EventId,
                CoverImage = domain + e.CoverImage,
                e.StartDateBanner,
                e.EndDateBanner,
                e.UrlLink,
                e.StartDateVideo,
                e.EndDateVidep,
            }).ToList();

            return Json(
           new
           {
               status = "Success",
               code = 200,
               message = "Success",
               data = events
           });
        }

        private bool IsAdministrator()
        {
            var httpContext = HttpContext.Current;
            var handler = new JwtSecurityTokenHandler();
            var authHeader = httpContext.Request.Headers["Authorization"];
            if (authHeader == null)
            {
                return false;
            }
            authHeader = authHeader.Replace("Bearer ", "");
            //Bearer + token (getToken)
            //var jsonToken = handler.ReadToken(authHeader);
            if (!(handler.ReadToken(authHeader) is JwtSecurityToken tokenS)) return false;

            var role = tokenS.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Role)?.Value;

            return !string.IsNullOrEmpty(role) && role.ToLower().Equals(_role.ToLower());
        }
    }
}
