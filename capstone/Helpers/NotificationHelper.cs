﻿using System;
using System.Collections.Generic;
using capstone.database.Database;
using capstone.database.Entities;

namespace capstone.Helpers {
    public static class NotificationHelper {
        public static void AddNotification(this IEnumerable<User> users, int? subjectId, int? objectId, string objectType, string content) {
            using (Database db = new Database()) {
                foreach (User user in users) {
                    Notification notification = db.Notifications.Create();
                    notification.UserId = user.UserId;
                    notification.SubjectId = subjectId;
                    notification.ObjectId = objectId;
                    notification.ObjectType = objectType;
                    notification.NotificationContent = content;
                    notification.NotifiedDate = DateTime.Now;
                    db.Notifications.Add(notification);
                }
                db.SaveChanges();
            }
        }

        public static void AddNotification(this User user, int? subjectId, int? objectId, string objectType, string content) {
            using (Database db = new Database()) {
                Notification notification = db.Notifications.Create();
                notification.UserId = user.UserId;
                notification.SubjectId = subjectId;
                notification.ObjectId = objectId;
                notification.ObjectType = objectType;
                notification.NotificationContent = content;
                notification.NotifiedDate = DateTime.Now;
                db.Notifications.Add(notification);
                db.SaveChanges();
            }
        }
    }
}