﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace capstone.Helpers {
    public static class TextHelper {
        private static readonly Regex SIGNS_REGEX = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
        private static readonly Regex SPACES_REGEX = new Regex(@"\s+");

        public static string ToUnsignString(this string s) {
            if (string.IsNullOrEmpty(s))
                return string.Empty;
            return SPACES_REGEX.Replace(SIGNS_REGEX.Replace(s.Normalize(NormalizationForm.FormD), string.Empty), " ").Replace('đ', 'd').Replace('Đ', 'D');
        }

        public static bool ContainsIgnoreCase(this string s, string ss) {
            if (string.IsNullOrEmpty(s))
                return false;
            if (string.IsNullOrEmpty(ss))
                return true;
            return s.IndexOf(ss, StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}