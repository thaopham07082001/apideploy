﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace capstone.Helpers {
    public static class ImageHelper {
        public const int MAX_SIZE = 5242880;
        public static readonly ImageFormat[] FORMATS = new ImageFormat[] { ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif };

        public static string SaveImage(this HttpPostedFileBase file, string path) {
            path = HttpContext.Current.Server.MapPath(path);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            string image = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            Image img = Image.FromStream(file.InputStream);
            Bitmap bmp = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.DrawImage(img, 0, 0, img.Width, img.Height);
            graphics.Dispose();
            bmp.Save(path + image, img.RawFormat);
            return image;
        }
    }

    public class ValidateImageAttribute : ValidationAttribute {
        public ValidateImageAttribute() : base("Image must be of type JPG, PNG or GIF and smaller than 5MB") { }

        public override bool IsValid(object value) {
            if (!(value is HttpPostedFileBase file))
                return true;
            if (file.ContentLength > ImageHelper.MAX_SIZE)
                return false;
            try {
                using (Image img = Image.FromStream(file.InputStream))
                    return ImageHelper.FORMATS.Contains(img.RawFormat);
            }
            catch { }
            return false;
        }
    }
}