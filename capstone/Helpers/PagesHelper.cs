﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Routing;

namespace capstone.Helpers {
    public static class PagesHelper {
        public static RouteValueDictionary GetPage(int page, object obj) {
            RouteValueDictionary pages = new RouteValueDictionary {
                { "page", page }
            };
            if (obj != null) {
                IList<PropertyInfo> properties = new List<PropertyInfo>(obj.GetType().GetProperties());
                foreach (PropertyInfo property in properties) {
                    object value = property.GetValue(obj);
                    pages.Add(property.Name, property.GetValue(obj));
                }
            }
            return pages;
        }

        public static RouteValueDictionary GetWeek(int week, int year, object obj) {
            RouteValueDictionary pages = new RouteValueDictionary {
                { "week", week },
                { "year", year }
            };
            if (obj != null) {
                IList<PropertyInfo> properties = new List<PropertyInfo>(obj.GetType().GetProperties());
                foreach (PropertyInfo property in properties) {
                    object value = property.GetValue(obj);
                    pages.Add(property.Name, property.GetValue(obj));
                }
            }
            return pages;
        }
    }
}