﻿using System.Web.Mvc;
using capstone.Models;

namespace capstone.Helpers {
    public static class MetaHelper {
        public static void SetMeta(this Controller controller, string title, string url, string image, string description) {
            controller.TempData["Meta"] = new MetaModel() {
                Title = title,
                Url = url,
                Image = image,
                Description = description
            };
        }
    }
}