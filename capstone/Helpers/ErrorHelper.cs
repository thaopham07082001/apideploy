﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capstone.Helpers {
    public class ErrorLogFilter : IExceptionFilter {

        public void OnException(ExceptionContext filterContext) {
            string path = HttpContext.Current.Server.MapPath("~/Errors/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            path = Path.Combine(path, DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt");
            File.WriteAllLines(path, new string[] {
                "RouteData",
                string.Join(", ", filterContext.RouteData.Values.Select(rd => string.Format("{0}: {1}", rd.Key, rd.Value))),
                "Exception",
                filterContext.Exception.Message,
                filterContext.Exception.StackTrace
            });
        }
    }
}