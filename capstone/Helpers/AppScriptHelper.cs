﻿using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace capstone.Helpers {
    public static class AppScriptHelper {
        private static readonly string URL;
        private static readonly string TOKEN;

        static AppScriptHelper() {
            URL = ConfigurationManager.AppSettings["AppScriptApi"];
            TOKEN = ConfigurationManager.AppSettings["AppScriptToken"];
        }

        public static async Task<object> CreateForm(string email) {
            using (HttpClient client = new HttpClient()) {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string json = await new HttpClient().GetStringAsync(URL + "?token=" + TOKEN + "&action=CreateForm&email=" + email);
                return serializer.DeserializeObject(json);
            }
        }

        public static async Task<string> ExportEventStatistics(string email, object content) {
            using (HttpClient client = new HttpClient()) {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                HttpResponseMessage response = await client.PostAsync(URL + "?token=" + TOKEN + "&action=ExportEventStatistics&email=" + email, new StringContent(serializer.Serialize(content), Encoding.UTF8, "application/json"));
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}