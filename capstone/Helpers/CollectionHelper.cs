﻿using System;
using System.Collections.Generic;

namespace capstone.Helpers {
    public static class CollectionHelper {
        public static List<T> Shuffle<T>(this List<T> list) {
            List<T> randomList = new List<T>();
            Random r = new Random();
            int randomIndex = 0;
            while (list.Count > 0) {
                randomIndex = r.Next(0, list.Count);
                randomList.Add(list[randomIndex]);
                list.RemoveAt(randomIndex);
            }
            return randomList;
        }
    }
}