﻿using System.Web.Mvc;

namespace capstone.Helpers {
    public static class MessageHelper {
        public static void SetMessage(this Controller controller, string message) {
            controller.TempData["Message"] = message;
        }
    }
}