﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using capstone.api.Models;
using capstone.Api;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace capstone.Helpers {
    public static class ApiHelper {
        private const string DEFAULT_PROFILE_IMAGE = "Default.jpg";
        private static readonly string SECRET;
        private static readonly string[][] SLOTS;

        static ApiHelper() {
            ApiSoap client = new ApiSoapClient();
            SECRET = ConfigurationManager.AppSettings["FapToken"];
            SLOTS = new string[9][];
            for (byte i = 1; i <= 8; i++) {
                GetTimeSlotRequest request = new GetTimeSlotRequest() {
                    Body = new GetTimeSlotRequestBody(SECRET, i)
                };
                GetTimeSlotResponse response = client.GetTimeSlot(request);
                JToken slot = JsonConvert.DeserializeObject(response.Body.GetTimeSlotResult) as JToken;
                SLOTS[i] = new string[4];
                SLOTS[i][0] = slot["StartHour"].Value<string>();
                SLOTS[i][1] = slot["StartMinute"].Value<string>();
                SLOTS[i][2] = slot["EndHour"].Value<string>();
                SLOTS[i][3] = slot["EndMinute"].Value<string>();
            }
        }

        public static UserProfileApiModel GetProfile(string email) {
            string id = email.Split('@')[0];
            ApiSoap client = new ApiSoapClient();
            GetStudentByMemberCodeRequest request = new GetStudentByMemberCodeRequest() {
                Body = new GetStudentByMemberCodeRequestBody(id, SECRET)
            };
            GetStudentByMemberCodeResponse response = client.GetStudentByMemberCode(request);
            JToken profile = (JsonConvert.DeserializeObject(response.Body.GetStudentByMemberCodeResult) as JArray).First;
            if (profile == null)
                return null;
            GetImageRequest request2 = new GetImageRequest() {
                Body = new GetImageRequestBody(id, SECRET)
            };
            GetImageResponse response2 = client.GetImage(request2);
            string path = HttpContext.Current.Server.MapPath("~/Images/ProfileImage/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            string profileImage;
            if (response2.Body.GetImageResult != null) {
                profileImage = Guid.NewGuid().ToString() + ".jpg";
                File.WriteAllBytes(path + profileImage, response2.Body.GetImageResult);
            }
            else
                profileImage = DEFAULT_PROFILE_IMAGE;
            string city = profile["Address"].Value<string>();
            if (!string.IsNullOrEmpty(city)) {
                if (city.Contains(","))
                    city = Regex.Split(city, @"\s*,\s*").Last();
                else if (city.Contains("-"))
                    city = Regex.Split(city, @"\s*-\s*").Last();
                else
                    city = string.Join(" ", Regex.Split(city, @"\s+").Reverse().Take(2).Reverse());
                city = GetCity(city);
            }
            return new UserProfileApiModel() {
                RollNumber = profile["RollNumber"].Value<string>(),
                FirstName = profile["FirstName"].Value<string>(),
                LastName = profile["LastName"].Value<string>() + " " + profile["MiddleName"].Value<string>(),
                DOB = DateTime.Parse(profile["DateOfBirth"].Value<string>()),
                Gender = profile["Gender"].Value<bool>(),
                Email = profile["Email"].Value<string>(),
                Phone = profile["MobilePhone"].Value<string>(),
                City = city,
                Address = profile["Address"].Value<string>(),
                ProfileImage = "/Images/ProfileImage/" + profileImage,
                Campus = profile["CampusName"].Value<string>(),
                Major = profile["Major"].Value<string>(),
                Specialization = profile["Nganh"].Value<string>(),
                ParentName = profile["ParentName"].Value<string>(),
                ParentPhone = profile["ParentPhone"].Value<string>()
            };
        }

        public static List<CalendarApiModel> GetCalendars(string email, DateTime start, DateTime end) {
            string id = email.Split('@')[0];
            ApiSoap client = new ApiSoapClient();
            List<CalendarApiModel> calendars = new List<CalendarApiModel>();
            GetTimeTableRequest request = new GetTimeTableRequest() {
                Body = new GetTimeTableRequestBody(id, start, end, SECRET)
            };
            GetTimeTableResponse response = client.GetTimeTable(request);
            JArray studies = JsonConvert.DeserializeObject(response.Body.GetTimeTableResult) as JArray;
            foreach (JToken study in studies) {
                DateTime date = study["Date"].Value<DateTime>();
                byte slot = study["Slot"].Value<byte>();
                calendars.Add(new CalendarApiModel() {
                    Name = "Study " + study["SubjectCode"].Value<string>(),
                    StartDate = new DateTime(date.Year, date.Month, date.Day, int.Parse(SLOTS[slot][0]), int.Parse(SLOTS[slot][1]), 0),
                    EndDate = new DateTime(date.Year, date.Month, date.Day, int.Parse(SLOTS[slot][2]), int.Parse(SLOTS[slot][3]), 0)
                });
            }
            GetscheduleExamRequest request2 = new GetscheduleExamRequest() {
                Body = new GetscheduleExamRequestBody(id, start, end, SECRET)
            };
            GetscheduleExamResponse response2 = client.GetscheduleExam(request2);
            JArray exams = JsonConvert.DeserializeObject(response2.Body.GetscheduleExamResult) as JArray;
            foreach (JToken exam in exams) {
                DateTime date = exam["Date"].Value<DateTime>();
                string[] times = exam["GioThi"].Value<string>().Split('-', 'h');
                calendars.Add(new CalendarApiModel() {
                    Name = "Exam " + exam["kythi"].Value<string>() + " " + exam["SubjectCode"].Value<string>() + " in " + exam["RoomNo"].Value<string>(),
                    StartDate = new DateTime(date.Year, date.Month, date.Day, int.Parse(times[0]), int.Parse(times[1]), 0),
                    EndDate = new DateTime(date.Year, date.Month, date.Day, int.Parse(times[2]), int.Parse(times[3]), 0)
                });
            }
            return calendars;
        }

        public static List<string> GetMajors() {
            ApiSoap client = new ApiSoapClient();
            List<string> majors = new List<string>();
            GetChuyenNganhRequest request = new GetChuyenNganhRequest() {
                Body = new GetChuyenNganhRequestBody(SECRET)
            };
            GetChuyenNganhResponse response = client.GetChuyenNganh(request);
            JArray array = JsonConvert.DeserializeObject(response.Body.GetChuyenNganhResult) as JArray;
            foreach (JToken major in array)
                majors.Add(major["Nganh"].Value<string>());
            return majors;
        }

        private static string GetCity(string city) {
            city = city.Trim().ToLower();
            city = city.Replace("tp ", string.Empty);
            city = city.Replace("tp. ", string.Empty);
            city = city.Replace("thành phố ", string.Empty);
            city = city.Replace("tỉnh ", string.Empty);
            return string.Join(" ", Regex.Split(city, @"\s+").Select(s => char.ToUpper(s[0]) + s.Substring(1).ToLower()));
        }
    }
}