﻿using System;
using System.Globalization;

namespace capstone.Helpers {
    public static class CalendarHelper {
        public static bool CheckOverlap<T>(T a1, T a2, T b1, T b2) where T : IComparable {
            if (a1.CompareTo(b1) > 0) {
                T t = a1;
                a1 = b1;
                b1 = t;
                t = a2;
                a2 = b2;
                b2 = t;
            }
            return a2.CompareTo(b1) >= 0;
        }

        public static DateTime GetFirstDayOfWeek(int week, int year) {
            DateTime first = new DateTime(year, 1, 1);
            return first.AddDays((week - 1) * 7 - ((int)first.DayOfWeek + 6) % 7);
        }

        public static int GetWeekOfYear(this DateTime date) {
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
        }

        public static int GetNumberOfWeeksInYear(int year) {
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(new DateTime(year, 12, 31), CalendarWeekRule.FirstDay, DayOfWeek.Monday);
        }

        public static string ToStandardString(this DateTime date) {
            return date.ToString("yyyy-MM-dd");
        }

        public static string ToDateString(this DateTime date) {
            return date.ToString("dd/MM/yyyy");
        }

        public static string ToTimeString(this DateTime date) {
            return date.ToString("HH:mm");
        }

        public static string ToTimeString(this TimeSpan time) {
            return time.ToString("hh\\:mm");
        }

        public static string ToDateTimeString(this DateTime date) {
            return date.ToString("dd/MM/yyyy HH:mm");
        }

        public static string ToRelativeFromNowDateString(this DateTime date) {
            TimeSpan span = date - DateTime.Now;
            if (span.TotalMinutes < 1)
                return "about now";
            else if (span.TotalHours < 1) {
                int minutes = Convert.ToInt32(Math.Floor(span.TotalMinutes));
                return string.Format("in {0} minute{1}", minutes, minutes != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 1) {
                int hours = Convert.ToInt32(Math.Floor(span.TotalHours));
                return string.Format("in {0} hour{1}", hours, hours != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 7) {
                int days = Convert.ToInt32(Math.Floor(span.TotalDays));
                return string.Format("in {0} day{1}", days, days != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 30) {
                int weeks = Convert.ToInt32(Math.Floor(span.TotalDays / 7));
                return string.Format("in {0} week{1}", weeks, weeks != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 90) {
                int months = Convert.ToInt32(Math.Floor(span.TotalDays / 30));
                return string.Format("in {0} month{1}", months, months != 1 ? "s" : string.Empty);
            }
            else
                return string.Format("at {0} ", date.ToDateTimeString());
        }

        public static string ToRelativeToNowDateString(this DateTime date) {
            TimeSpan span = DateTime.Now - date;
            if (span.TotalMinutes < 1)
                return "just now";
            else if (span.TotalHours < 1) {
                int minutes = Convert.ToInt32(Math.Floor(span.TotalMinutes));
                return string.Format("{0} minute{1} ago", minutes, minutes != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 1) {
                int hours = Convert.ToInt32(Math.Floor(span.TotalHours));
                return string.Format("{0} hour{1} ago", hours, hours != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 7) {
                int days = Convert.ToInt32(Math.Floor(span.TotalDays));
                return string.Format("{0} day{1} ago", days, days != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 30) {
                int weeks = Convert.ToInt32(Math.Floor(span.TotalDays / 7));
                return string.Format("{0} week{1} ago", weeks, weeks != 1 ? "s" : string.Empty);
            }
            else if (span.TotalDays < 90) {
                int months = Convert.ToInt32(Math.Floor(span.TotalDays / 30));
                return string.Format("{0} month{1} ago", months, months != 1 ? "s" : string.Empty);
            }
            else
                return date.ToDateTimeString();
        }

        public static DateTime GetDateFromString(string s) {
            return DateTime.ParseExact(s, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        }

        public static DateTime GetDateTimeFromDateAndTime(DateTime date, DateTime time) {
            return date.Date.Add(time.TimeOfDay);
        }
    }
}