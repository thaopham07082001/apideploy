﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using capstone.api.Models;
using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace capstone.Helpers {
    public static class AccountHelper {
        private const string DEFAULT_LAST_NAME = "FPT";
        private const string DEFAULT_PROFILE_IMAGE = "/Images/ProfileImage/Default.jpg";

        private static bool IsRole(IPrincipal user, string role) {
            if (user.Identity.IsAuthenticated) {
                ApplicationDbContext context = new ApplicationDbContext();
                UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                if (UserManager.FindById(user.Identity.GetUserId()) == null) {
                    IAuthenticationManager authentication = HttpContext.Current.GetOwinContext().Authentication;
                    HttpContext.Current.Session.Abandon();
                    authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    return false;
                }
                return UserManager.GetRoles(user.Identity.GetUserId()).Contains(role);
            }
            return false;
        }

        public static bool IsStudent(this IPrincipal user) {
            return IsRole(user, UserRoleConstants.STUDENT);
        }

        public static bool IsOrganizer(this IPrincipal user) {
            return IsRole(user, UserRoleConstants.ORGANIZER);
        }

        public static bool IsManager(this IPrincipal user) {
            return IsRole(user, UserRoleConstants.MANAGER);
        }

        public static bool IsAdmin(this IPrincipal user) {
            return IsRole(user, UserRoleConstants.ADMIN);
        }

        public static int GetRolesCount(this IPrincipal user) {
            if (user.Identity.IsAuthenticated) {
                ApplicationDbContext context = new ApplicationDbContext();
                UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                if (UserManager.FindById(user.Identity.GetUserId()) == null) {
                    IAuthenticationManager authentication = HttpContext.Current.GetOwinContext().Authentication;
                    HttpContext.Current.Session.Abandon();
                    authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    return 0;
                }
                return UserManager.GetRoles(user.Identity.GetUserId()).Count;
            }
            return 0;
        }

        public static bool IsCurrentlyUnassigned(this IPrincipal user) {
            return HttpContext.Current.Session["Role"] == null;
        }

        public static bool IsCurrentlyStudent(this IPrincipal user) {
            return !user.IsCurrentlyUnassigned() && HttpContext.Current.Session["Role"].Equals(UserRoleConstants.STUDENT);
        }

        public static bool IsCurrentlyOrganizer(this IPrincipal user) {
            return !user.IsCurrentlyUnassigned() && HttpContext.Current.Session["Role"].Equals(UserRoleConstants.ORGANIZER);
        }

        public static bool IsCurrentlyManager(this IPrincipal user) {
            return !user.IsCurrentlyUnassigned() && HttpContext.Current.Session["Role"].Equals(UserRoleConstants.MANAGER);
        }

        public static bool IsCurrentlyAdmin(this IPrincipal user) {
            return !user.IsCurrentlyUnassigned() && HttpContext.Current.Session["Role"].Equals(UserRoleConstants.ADMIN);
        }

        public static string GetCurrentRole(this IPrincipal user) {
            return HttpContext.Current.Session["Role"] as string;
        }

        public static void SetToStudent(this IPrincipal user) {
            HttpContext.Current.Session["Role"] = UserRoleConstants.STUDENT;
        }

        public static void SetToOrganizer(this IPrincipal user) {
            HttpContext.Current.Session["Role"] = UserRoleConstants.ORGANIZER;
        }

        public static void SetToManager(this IPrincipal user) {
            HttpContext.Current.Session["Role"] = UserRoleConstants.MANAGER;
        }

        public static void SetToAdmin(this IPrincipal user) {
            HttpContext.Current.Session["Role"] = UserRoleConstants.ADMIN;
        }

        public static void SetUser(this IPrincipal user, ExternalLoginInfo login) {
            using (Database db = new Database()) {
                User user2 = db.Users.Where(u => u.Email.Equals(login.Email)).SingleOrDefault();
                UserManager<ApplicationUser> users = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                ApplicationUser user3 = users.FindByEmail(login.Email);
                if (user2 == null) {
                    user2 = db.Users.Create();
                    user2.Email = login.Email;
                    user2.IsStudent = true;
                    user2.IsEnabled = true;
                    user2.NotificationSeenDate = DateTime.Now;
                    db.Users.Add(user2);
                    users.AddToRole(user3.Id, UserRoleConstants.STUDENT);
                    db.SaveChanges();
                }
                else {
                    if (user2.IsStudent)
                        users.AddToRole(user3.Id, UserRoleConstants.STUDENT);
                    if (user2.IsOrganizer)
                        users.AddToRole(user3.Id, UserRoleConstants.ORGANIZER);
                    if (user2.IsManager)
                        users.AddToRole(user3.Id, UserRoleConstants.MANAGER);
                    if (user2.IsAdmin)
                        users.AddToRole(user3.Id, UserRoleConstants.ADMIN);
                }
                if (user2.UserProfile == null || user2.UserProfile.LastName.Equals(DEFAULT_LAST_NAME)) {
                    UserProfile profile;
                    if (user2.UserProfile == null) {
                        profile = db.UserProfiles.Create();
                        db.UserProfiles.Add(profile);
                    }
                    else
                        profile = user2.UserProfile;
                    UserProfileApiModel profile2 = ApiHelper.GetProfile(login.Email.ToLower());
                    if (profile2 != null) {
                        profile.RollNumber = profile2.RollNumber;
                        profile.FirstName = profile2.FirstName;
                        profile.LastName = profile2.LastName;
                        profile.DOB = profile2.DOB;
                        profile.Gender = profile2.Gender;
                        profile.Phone = profile2.Phone;
                        profile.City = profile2.City;
                        profile.Address = profile2.Address;
                        profile.ProfileImage = profile2.ProfileImage;
                        profile.Campus = profile2.Campus;
                        profile.Major = profile2.Major;
                        profile.Specialization = profile2.Specialization;
                        profile.ParentName = profile2.ParentName;
                        profile.ParentPhone = profile2.ParentPhone;
                    }
                    else if (login.ExternalIdentity != null) {
                        profile.FirstName = login.ExternalIdentity.Claims.Single(c => c.Type.Equals("FirstName")).Value.Substring(0, Math.Min(login.ExternalIdentity.Claims.Single(c => c.Type.Equals("FirstName")).Value.Length, 20));
                        profile.LastName = login.ExternalIdentity.Claims.Single(c => c.Type.Equals("LastName")).Value;
                        profile.ProfileImage = login.ExternalIdentity.Claims.Single(c => c.Type.Equals("ProfileImage")).Value;
                        profile.Gender = true;
                        profile.DOB = DateTime.Now;
                        profile.Campus = "FU-HL";
                    }
                    else {
                        profile.FirstName = login.Email.Split('@').First();
                        profile.LastName = DEFAULT_LAST_NAME;
                        profile.ProfileImage = DEFAULT_PROFILE_IMAGE;
                        profile.Gender = true;
                        profile.DOB = DateTime.Now;
                        profile.Campus = "FU-HL";
                    }
                    user2.UserProfile = profile;
                    db.SaveChanges();
                }
                HttpContext.Current.Session["User"] = user2;
            }
        }

        public static User GetUser(this IPrincipal user) {
            if (HttpContext.Current.Session["User"] == null)
                SetUser(user, new ExternalLoginInfo() {
                    Email = HttpContext.Current.User.Identity.Name
                });
            return HttpContext.Current.Session["User"] as User;
        }

        public static List<Group> GetGroups(this User user) {
            Database db = new Database();
            List<Group> groups = new List<Group>();
            groups.AddRange(db.Users.Where(u => u.UserId == user.UserId).Single().UserGroups.Select(ug => ug.Group).Where(ug => ug.IsEnabled == true));
            return groups;
        }
    }

    public class CheckUserRoleAttribute : FilterAttribute, IActionFilter {
        public void OnActionExecuted(ActionExecutedContext filterContext) { }

        public void OnActionExecuting(ActionExecutingContext filterContext) {
            string controller = filterContext.Controller.ControllerContext.RouteData.Values["controller"].ToString();
            string action = filterContext.Controller.ControllerContext.RouteData.Values["action"].ToString();
            if (controller.Equals("Home") && !action.Equals("Index") && HttpContext.Current.User.IsCurrentlyUnassigned())
                HttpContext.Current.User.SetToStudent();
            else if (controller.Equals("Student") && HttpContext.Current.User.IsStudent())
                HttpContext.Current.User.SetToStudent();
            else if (controller.Equals("Organizer") && HttpContext.Current.User.IsOrganizer())
                HttpContext.Current.User.SetToOrganizer();
            else if (controller.Equals("Manager") && HttpContext.Current.User.IsManager())
                HttpContext.Current.User.SetToManager();
            else if (controller.Equals("Admin") && HttpContext.Current.User.IsAdmin())
                HttpContext.Current.User.SetToAdmin();
        }
    }
}