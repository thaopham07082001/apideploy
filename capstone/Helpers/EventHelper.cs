﻿using System;
using System.Collections.Generic;
using System.Linq;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Models;

namespace capstone.Helpers {
    public static class EventHelper {
        public static double? GetRating(this Event eventt) {
            List<FeedbackModel> feedbacks = GetFeedbacks(eventt);
            if (feedbacks.Count() == 0)
                return 0;
            double? total = 0;
            foreach (FeedbackModel f in feedbacks)
                total += f.Value;
            return Math.Round(total.Value / feedbacks.Count() * 2) / 2;
        }

        public static List<FeedbackModel> GetFeedbacks(this Event eventt) {
            List<FeedbackModel> feedbacks = new List<FeedbackModel>();
            foreach (Feedback f in eventt.Feedbacks)
                feedbacks.Add(new FeedbackModel() {
                    Key = f.UserId.ToString(),
                    Username = f.User.UserProfile.LastName + " " + f.User.UserProfile.FirstName,
                    UserImage = f.User.UserProfile.ProfileImage,
                    Value = f.Value,
                    Content = f.FeedbackContent,
                    txtCreatedDate = f.CreatedDate.ToDateString(),
                    CreatedDate = f.CreatedDate,
                    IsExternal = false
                });
            foreach (FeedbackOuter f in eventt.FeedbackOuters)
                feedbacks.Add(new FeedbackModel() {
                    Key = f.Key,
                    Username = f.UserName,
                    UserImage = f.UserImage,
                    Value = f.Value,
                    Content = f.FeedbackContent,
                    txtCreatedDate = f.CreatedDate.ToDateString(),
                    CreatedDate = f.CreatedDate,
                    IsExternal = true
                });
            return feedbacks.OrderByDescending(f => f.CreatedDate).ToList();
        }

        public static string GetOtherParticipants(this Event eventt) {
            if (eventt.Participants == null || string.IsNullOrWhiteSpace(eventt.Participants))
                return string.Empty;
            string output = eventt.Participants.Split(new[] { ';' }, 2)[1];
            if (!string.IsNullOrWhiteSpace(output)) {
                output = output.Trim();
                output = output.Remove(output.Length - 1);
            }
            return output;
        }
        public static string GetParticipants(this Event eventt) {
            if (eventt.Participants == null || string.IsNullOrWhiteSpace(eventt.Participants))
                return string.Empty;
            string output = eventt.Participants.Split(new[] { ';' }, 2)[0];
            return output.Trim();
        }

        public static bool IsRegisteredByGroup(this Event eventt, ref List<Group> currGroups, User user) {
            if (eventt.TargetGroup == null || eventt.TargetGroup.Trim().Equals(""))
                return true;
            string[] tmpGroups = eventt.TargetGroup.Split(new[] { ", " }, StringSplitOptions.None);
            Database db = new Database();
            user = db.Users.Where(u => u.UserId == user.UserId).Single();
            foreach (string group in tmpGroups) {
                if (!group.Trim().Equals("")) {
                    int gid = int.Parse(System.Text.RegularExpressions.Regex.Match(group, @"\d+").Value);
                    if (db.Groups.Any(g => g.GroupId == gid && g.IsEnabled))
                        currGroups.Add(db.Groups.Where(g => g.GroupId == gid).Single());
                }
            }
            if (currGroups.Count == 0)
                return true;
            if (user == null)
                return false;
            foreach (Group g in currGroups) {
                if (user.UserGroups.Any(ug => ug.GroupId == g.GroupId))
                    return true;
            }
            return false;
        }
    }
}