﻿let tvList = [1,2,3];
$("body").on('keyup', "#txtCategorySearch", function () {
    var value = formatStrToLowerCase($(this).val());
    $("#listCategory li").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});


$("body").on('keyup', "#txtGroupSearch", function () {
    var value = formatStrToLowerCase($(this).val());
    $("#listGroup li").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});

$(document).on('click', '#listCategory li', function () {
    $(this).remove();
    var html = '<button type="button" class="btn btn-info m-1" value="' + $(this).val() + '">' + $(this).text() + ' <i class="fa fa-close"></i></button>';
    html += '<input type="hidden" name="Category" id="' + $(this).val() + '" value="' + $(this).val() + '" />';
    $('#CategoryArea').append(html);
});

$(document).on('click', '#listTv li', function () {
    $(this).remove();
    var html = '<button type="button" class="btn btn-info m-1" value="' + $(this).val() + '">' + $(this).text() + ' <i class="fa fa-close"></i></button>';
    html += '<input type="hidden" name="SmartTv" id="' + $(this).val() + '" value="' + $(this).val() + '" />';
    tvList.push('$(this).val()');
    $('#TvArea').append(html);
})

$(document).on('click', '#listGroup li', function () {
    $(this).remove();
    var html = '<button type="button" class="btn btn-info m-1" value="' + $(this).val() + '">' + $(this).text() + ' <i class="fa fa-close"></i></button>';
    html += '<input type="hidden" name="TargetGroup" id="' + $(this).val() + '" value="' + $(this).val() + '" />';
    $('#GroupArea').append(html);
});

$(document).on('click', '#listUser li', function () {
    $(this).remove();
    var html = '<button type="button" class="btn btn-info m-1" value="' + $(this).val() + '">' + $(this).text() + ' <i class="fa fa-close"></i></button>';
    html += '<input type="hidden" name="Staff" id="' + $(this).val() + '" value="' + $(this).val() + '" />';
    $('#StaffArea').append(html);
});

$(document).on('click', '#CategoryArea button', function () {
    $(this).remove();
    $("#" + $(this).val()).remove();
    $('#listCategory').append('<li class="list-group-item list-group-item-action p-2" value="' + $(this).val() + '">' + $(this).text() + '</li>')
});

$(document).on('click', '#TvArea button', function () {
    $(this).remove();
    $("#" + $(this).val()).remove();
    $('#listTv').append('<li class="list-group-item list-group-item-action p-2" value="' + $(this).val() + '">' + $(this).text() + '</li>')
});

$(document).on('click', '#GroupArea button', function () {
    $(this).remove();
    $("#" + $(this).val()).remove();
    $('#listGroup').append('<li class="list-group-item list-group-item-action p-2" value="' + $(this).val() + '">' + $(this).text() + '</li>')
});

$(document).on('click', '#StaffArea button', function () {
    $(this).remove();
    $("#" + $(this).val()).remove();
    $('#listUser').append('<li class="list-group-item list-group-item-action p-2" value="' + $(this).val() + '">' + $(this).text() + '</li>')
});

$(document).on('click', '#FamilyArea button', function () {
    var html = '<input type="text" id="txtFamily" class="form-control" />';
    html += '<ul class="list-group position-absolute w-100" id="listFamily" style="max-height: 200px; overflow: auto; cursor: pointer; z-index: 99;"></ul>';
    $('#FamilyArea').html(html);
});

$(document).on('click', '#listFamily li', function () {
    if (this.id != "null") {
        var html = '<button type="button" class="btn btn-info m-1 form-control"><span class="float-left">' + $(this).text() + '</span> <i class="fa fa-close float-right"></i></button>';
        html += '<input type="hidden" name="EventFamily" value="' + $(this).text() + '" />';
        $('#FamilyArea').html(html);
    }
});

$("body").on('focusout', "#txtFamily", function () {
    window.setTimeout(function () { $('#listFamily').hide(); }, 100);
});

$("body").on('focusin', "#txtFamily", function () {
    $('#listFamily').show();
});

$("body").on('keyup', "#txtFamily", function (event) {
    if ($(this).val() != "") {
        var data = {
            searchStr: formatStrToLowerCase($(this).val().trim())
        };
        $.ajax({
            url: "/Organizer/SearchFamily",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                $('#listFamily li').remove();
                if (result.length != 0) {
                    $('#listFamily').show();
                    $.each(result, function (index, value) {
                        $('#listFamily').append('<li class="list-group-item list-group-item-action p-2" value="1">' + value.FamilyName + '</li>');
                    });
                } else {
                    $('#listFamily').append('<li class="list-group-item list-group-item-action p-2" value="1" id="null">No items in result. Press enter to create a new one.</li>');
                }
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    } else {
        $('#listFamily').hide();
    }
});

$("body").on('keypress', "#txtFamily", function (event) {
    if ($(this).val() != "") {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var html = '<button type="button" class="btn btn-info m-1 form-control"><span class="float-left">' + $(this).val().trim() + '</span> <i class="fa fa-close float-right"></i></button>';
            html += '<input type="hidden" name="EventFamily" value="' + $(this).val().trim() + '" />';
            $('#FamilyArea').html(html);
        }
    }
});

function formatStrToLowerCase(input) {
    input = input.toLowerCase();
    input = input.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    input = input.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    input = input.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    input = input.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    input = input.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    input = input.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    input = input.replace(/đ/g, "d");
    input = input.replace(/ {1,}/g, " ");
    return input;
}

$(document).on('click', function (e) {
    /* bootstrap collapse js adds "in" class to your collapsible element*/
    var menu_opened = $('#collapseParticipants').hasClass('show');

    if (!$(e.target).closest('#collapseParticipants').length &&
        !$(e.target).is('#collapseParticipants') &&
        menu_opened === true) {
        $('#collapseParticipants').collapse('toggle');
    }
});

$(document).ready(function () {
    $("#listBuilding").change(function () {
        $.ajax({
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            url: "/Organizer/GetSmartTvByBuilingId",
            data: JSON.stringify({ "buildId": $("#listBuilding").val() }),
            success: function (result, status, xhr) {
                //$("#listBuilding").html(result);
                //$('#listFamily').show();
                console.log('đã có list tv');

                $('#listTv li').remove();
                var tvList = $('#listTv');
                if (result == null) {
                    tvList.append('<li class="list-group-item list-group-item-action p-2" value="">--Select a tv--</li>');
                }
                for (let ele of result) {
                    //if (listTv.filter(item => item === ele.TvId) != null) {

                    //} else {
                    //    tvList.append('<li class="list-group-item list-group-item-action p-2" value=' + ele.TvId + '>' + ele.TvName + '</li>');
                    //}
                    tvList.append('<li class="list-group-item list-group-item-action p-2" value=' + ele.TvId + '>' + ele.TvName + '</li>');
                    console.log(listTv);
                }
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    });
});
$(document).ready(function () {
        $.ajax({
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            url: "/Organizer/GetUserProfiles",
            success: function (result, status, xhr) {
                //$("#listBuilding").html(result);
                //$('#listFamily').show();
                console.log('đã có list user');

                $('#listUser li').remove();
                var tvList = $('#listUser');
                for (let ele of result) {
                    //if (listTv.filter(item => item === ele.TvId) != null) {

                    //} else {
                    //    tvList.append('<li class="list-group-item list-group-item-action p-2" value=' + ele.TvId + '>' + ele.TvName + '</li>');
                    //}
                    tvList.append('<li class="list-group-item list-group-item-action p-2" value=' + ele.UserId + '>' + ele.UserName + '</li>');
                    console.log(listTv);
                }
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
});


