﻿function addRow(section, initRow) {
    var newRow = initRow.clone().removeAttr('id').addClass('new').insertBefore(initRow),
        deleteRow = $('<a class="rowDelete"><img src="http://i.imgur.com/ZSoHl.png"></a>');

    newRow
        .append(deleteRow)
        .on('click', 'a.rowDelete', function () {
            removeRow(newRow);
        })
        .slideDown(300, function () {
            $(this)
                .find('input').focus();
        })
}

function removeRow(newRow) {
    newRow
        .slideUp(200, function () {
            $(this)
                .next('div:not(#initRow)')
                .find('input').focus()
                .end()
                .end()
                .remove();
        });
}
$(document).ready(function () {
    $('form').validate({
        ignore: []
    });
    var initRow = $('#initRow'),
        section = initRow.parent('section');

    initRow.on('keydown', 'input', function () {
        addRow(section, initRow);
    });
});

$("body").on('focusout', "#Leader", function () {
    window.setTimeout(function () { $('#listOrganizer').hide(); }, 100);
});

$("body").on('focusin', "#Leader", function () {
    $('#listOrganizer').show();
});

$("body").on('keyup', "#Leader", function () {
    if ($(this).val() != "") {
        var data = {
            searchStr: $(this).val().trim()
        };
        $.ajax({
            url: "/Manager/SearchOrganizers",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                $('#listOrganizer li').remove();
                if (result.length != 0) {
                    $('#listOrganizer').show();
                    $.each(result, function (index, value) {
                        $('#listOrganizer').append('<li class="list-group-item list-group-item-action p-2" value="' + value.Id + '">' + value.Email + '</li>');
                    });
                } else {
                    $('#listOrganizer').append('<li class="list-group-item list-group-item-action p-2" value="0" id="null">No items in result.</li>');
                }
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    } else {
        $('#listOrganizer').html("");
    }
});

$(document).on('click', '#listOrganizer li', function () {
    if ($(this).val() != 0) {
        $('#Leader').val($(this).text());
        $('#listOrganizer').html('');
    }
});