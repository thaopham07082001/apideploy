﻿//load rating function
function loadrating(result) {
    var avgRating = result.AvgRating;
    var count = result.FeedbackCount;
    var html = '';
    html += '<span class="font-weight-bold pt-1 mr-2">Rating: </span>';
    for (var i = 1; i <= 5; i++) {
        if (avgRating >= 1) {
            html += '<img src="/Images/Events/Star.png" style="margin-top: -0.3rem; margin-right: -0.2rem" height="15" />';
        } else if (avgRating == 0.5) {
            html += '<img src="/Images/Events/Star_half.png" style="margin-top: -0.3rem; margin-right: -0.2rem" height="15" />';
        } else {
            html += '<img src="/Images/Events/Star_empty.png" style="margin-top: -0.3rem; margin-right: -0.2rem" height="15" />';
        }
        avgRating -= 1;
    }
    html += '<span class="pt-1 ml-2">by ' + count + ' people</span>';
    $('#rating').html(html);
}

//load data function
function loaddata(result) {
    var html = '';
    html += '<div class="card mt-1">';
    html += '<div class="card-body" style="padding: 0.5rem">';
    html += '<div class="float-right">';
    html += '<button type="button" class="btn btn-danger float-right mr-1" data-toggle="modal" data-target="#deleteFeedbackModal">Delete</button>';
    html += '<button type="button" class="btn btn-info float-right mr-1" data-toggle="modal" data-target="#feedbackModal">Update</button>';
    html += '</div>';
    html += '<div name="header">';
    html += '<div name="img" class="float-left pt-1 pr-2">';
    html += '<a href="javascript:void(0);"><img src="' + result.UserImage + '" class="rounded-circle border" height="50" /></a>';
    html += '</div>';
    html += '<div name="text" class="overflow-hidden">';
    html += '<div><a href="javascript:void(0);">' + result.Username + '</a></div>';
    html += '<div><span class="font-weight-bold" style="color: rgb(255,215,0)">' + result.Value + '</span><img src="/Images/Events/Star.png" style="margin-top: -0.3rem" height="15" /> ' + result.txtCreatedDate + '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div name="body">';
    html += '<span style="white-space: pre-line"><br/>';
    html += result.Content;
    html += '</span>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    loadrating(result);
    $('#yourfeedback').html(html);
}
//Add Data Function
function Update() {
    var res = validate();
    if (res == false) {
        return false;
    }
    var feedbackObj = {
        Key: $('#UserId').val(),
        EventId: $('#EventId').val(),
        Value: $("input:radio[name ='Value']:checked").val(),
        CreatedDate: new Date(),
        FeedbackContent: $('#FeedbackContent').val()
    };
    $.ajax({
        url: "/Home/UpdateFeedbackAnonymous",
        data: JSON.stringify(feedbackObj),
        type: "POST",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: function (result) {
            loaddata(result);
            $('#feedbackModal').modal('hide');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

//load data after delete feedback function
function loaddata_del(result) {
    var html = '';
    html += '<div>';
    html += '<span>You haven\'t written anything yet, want to make some?</span><button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#feedbackModal">Send your feedback</button>';
    html += '</div>';
    $('#feedbackModal').find('form').trigger("reset");
    loadrating(result);
    $('#yourfeedback').html(html);
}
//Delete feedback Function
function Delete() {
    $('#deleteFeedbackModal').modal('toggle');
    var feedbackObj = {
        Key: $('#UserId').val(),
        EventId: $('#EventId').val()
    };
    $.ajax({
        url: "/Home/DeleteFeedbackAnonymous",
        data: JSON.stringify(feedbackObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loaddata_del(result);
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//Valdidation using jquery
function validate() {
    var isValid = true;
    if ($('#FeedbackContent').val().trim() == "") {
        $('#FeedbackContent').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#FeedbackContent').css('border-color', 'lightgrey');
    }
    return isValid;
}
//load more feedbacks
function loadFeedbacks() {
    var obj = {
        eventId: $('#EventId').val(),
        skip: $('#skipFeedback').val()
    };
    $.ajax({
        url: "/Home/GetFeedback",
        data: JSON.stringify(obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            result.forEach(function (item) {
                var html = '<div class="card mt-1">';
                html += '<div class="card-body" style="padding: 0.5rem">';
                html += '<div name="header">';
                html += '<div name="img" class="float-left pt-1 pr-2">';
                html += '<a href="javascript:void(0);"><div class="feedback-div rounded-circle border"><img src="' + item.UserImage + '" /></div></a>';
                html += '</div>';
                html += '<div name="text" class="overflow-hidden">';
                html += '<div><a href="javascript:void(0);">' + item.Username + '</a></div>';
                html += '<div><span class="font-weight-bold" style="color: rgb(255,215,0)">' + item.Value + '</span><img src="/Images/Events/Star.png" style="margin-top: -0.3rem" height="15" /> ' + item.txtCreatedDate + '</div>';
                html += '</div>';
                html += '</div>';
                html += '<div name="body">';
                html += '<span style="white-space: pre-line"></br>';
                html += item.Content;
                html += '</span>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                $('#d-otherFeedbacks').append(html);
            });
            $('#skipFeedback').val(parseInt($('#skipFeedback').val()) + 1);
            $('html, body').animate({ scrollTop: $("#btnViewMore").offset().top }, 1000);
            if (result.length < parseInt($('#NUMBER_FEEDBACKS_PER_LOAD').val())) {
                $('#btnViewMore').remove();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}