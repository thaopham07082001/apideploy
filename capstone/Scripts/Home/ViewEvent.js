﻿//load rating function
function loadrating(result) {
    var avgRating = result.AvgRating;
    var count = result.FeedbackCount;
    var html = '';
    html += '<span class="font-weight-bold pt-1 mr-2">Rating: </span>';
    for (var i = 1; i <= 5; i++) {
        if (avgRating >= 1) {
            html += '<img src="/Images/Events/Star.png" style="margin-top: -0.3rem; margin-right: -0.2rem" height="15" />';
        } else if (avgRating == 0.5) {
            html += '<img src="/Images/Events/Star_half.png" style="margin-top: -0.3rem; margin-right: -0.2rem" height="15" />';
        } else {
            html += '<img src="/Images/Events/Star_empty.png" style="margin-top: -0.3rem; margin-right: -0.2rem" height="15" />';
        }
        avgRating -= 1;
    }
    html += '<span class="pt-1 ml-2">by ' + count + ' people</span>';
    $('#rating').html(html);
}

//load data function
function loaddata(result) {
    var html = '';
    html += '<div class="card mt-1">';
    html += '<div class="card-body" style="padding: 0.5rem">';
    html += '<div class="float-right">';
    html += '<button type="button" class="btn btn-danger float-right mr-1" data-toggle="modal" data-target="#deleteFeedbackModal">Delete</button>';
    html += '<button type="button" class="btn btn-info float-right mr-1" data-toggle="modal" data-target="#feedbackModal">Update</button>';
    html += '</div>';
    html += '<div name="header">';
    html += '<div name="img" class="float-left pt-1 pr-2">';
    html += '<a href="/Home/ViewProfile/' + $('#UserId').val() + '"><div class="feedback-div rounded-circle border"><img src="' + result.UserImage + '" /></div></a>'
    html += '</div>';
    html += '<div name="text" class="overflow-hidden">';
    html += '<div><a href="/Home/ViewProfile/' + $('#UserId').val() + '">' + result.Username + '</a></div>';
    html += '<div><span class="font-weight-bold" style="color: rgb(255,215,0)">' + result.Value + '</span><img src="/Images/Events/Star.png" style="margin-top: -0.3rem" height="15" /> ' + result.txtCreatedDate + '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div name="body">';
    html += '<span style="white-space: pre-line"><br/>';
    html += result.Content;
    html += '</span>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    loadrating(result);
    $('#yourfeedback').html(html);
}
//Add Data Function
function Update() {
    var res = validate();
    if (res == false) {
        return false;
    }
    var feedbackObj = {
        UserId: $('#UserId').val(),
        EventId: $('#EventId').val(),
        Value: $("input:radio[name ='Value']:checked").val(),
        CreatedDate: new Date(),
        FeedbackContent: $('#FeedbackContent').val()
    };
    $.ajax({
        url: "/Student/UpdateFeedback",
        data: JSON.stringify(feedbackObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loaddata(result);
            $('#feedbackModal').modal('hide');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

//Send report function
function sendReport() {
    var reportObj = {
        eventId: $('#EventId').val(),
        repDes: $('#repDes').val()
    };
    $.ajax({
        url: "/Home/ReportEvent",
        data: JSON.stringify(reportObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#reportModal').modal('hide');
            $('#resultModal').modal('show');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

//load delete data function
function loaddata_del(result) {
    var html = '';
    html += '<div>';
    html += '<span>You haven\'t written anything yet, want to make some?</span><button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#feedbackModal">Send your feedback</button>';
    html += '</div>';
    $('#feedbackModal').find('form').trigger("reset");
    loadrating(result);
    $('#yourfeedback').html(html);
}
//Delete Data Function
function Delete() {
    $('#deleteFeedbackModal').modal('toggle');
    var feedbackObj = {
        UserId: $('#UserId').val(),
        EventId: $('#EventId').val()
    };
    $.ajax({
        url: "/Student/DeleteFeedback",
        data: JSON.stringify(feedbackObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loaddata_del(result);
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//Valdidation using jquery
function validate() {
    var isValid = true;
    if ($('#FeedbackContent').val().trim() == "") {
        $('#FeedbackContent').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#FeedbackContent').css('border-color', 'lightgrey');
    }
    return isValid;
}
//bookmark click
function Bookmark() {
    var bookmark = {
        UserId: $('#UserId').val(),
        EventId: $('#EventId').val()
    };
    $.ajax({
        url: "/Student/Bookmark",
        data: JSON.stringify(bookmark),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function () {
            $('#d-bookmark').html('<a href="javascript:void(0)" onclick="Unbookmark()"><i class="far fa-heart"></i> Not Interest</a>');
            setTimeout(function () {
                alert('Successfully bookmarked.');
            }, 100);
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//unbookmark click
function Unbookmark() {
    var bookmark = {
        UserId: $('#UserId').val(),
        EventId: $('#EventId').val()
    };
    $.ajax({
        url: "/Student/Unbookmark",
        data: JSON.stringify(bookmark),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function () {
            $('#d-bookmark').html('<a href="javascript:void(0)" onclick="Bookmark()"><i class="far fa-heart"> Interest</i></a>');
            setTimeout(function () {
                alert('Successfully unbookmarked.')
            }, 100);
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
//load more feedbacks
function loadFeedbacks() {
    var obj = {
        eventId: $('#EventId').val(),
        skip: $('#skipFeedback').val()
    };
    $.ajax({
        url: "/Home/GetFeedback",
        data: JSON.stringify(obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('html, body').animate({ scrollTop: $("#btnViewMore").offset().top - 100 }, 500);
            result.forEach(function (item) {
                var html = '<div class="card mt-1">';
                html += '<div class="card-body" style="padding: 0.5rem">';
                html += '<div name="header">';
                html += '<div name="img" class="float-left pt-1 pr-2">';
                if (item.IsExternal) {
                    html += '<a href="javascript:void(0);"><div class="feedback-div rounded-circle border"><img src="' + item.UserImage + '" /></div></a>';
                } else {
                    html += '<a href="/Home/ViewProfile/' + item.Key + '"><div class="feedback-div rounded-circle border"><img src="' + item.UserImage + '" /></div></a>';
                }
                html += '</div>';
                html += '<div name="text" class="overflow-hidden">';
                if (item.IsExternal) {
                    html += '<div><a href="javascript:void(0);">' + item.Username + '</a></div>';
                } else {
                    html += '<div><a href="/Home/ViewProfile/' + item.Key + '">' + item.Username + '</a></div>';
                }
                html += '<div><span class="font-weight-bold" style="color: rgb(255,215,0)">' + item.Value + '</span><img src="/Images/Events/Star.png" style="margin-top: -0.3rem" height="15" /> ' + item.txtCreatedDate + '</div>';
                html += '</div>';
                html += '</div>';
                html += '<div name="body">';
                html += '<span style="white-space: pre-line"></br>';
                html += item.Content;
                html += '</span>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                $('#d-otherFeedbacks').append(html);
            });
            $('#skipFeedback').val(parseInt($('#skipFeedback').val()) + 1);
            if (result.length < parseInt($('#NUMBER_FEEDBACKS_PER_LOAD').val())) {
                $('#btnViewMore').remove();
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}