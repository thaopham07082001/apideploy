$(document).ready(function () {
    $('.carousel').slick({
        slidesToShow: 1,
        dots: true,
        centerMode: true,
    });
});

// Slick version 1.5.8
// close the toggle menu if user clicks outside of the menu

$(document).click(function (event) {
    if (
        $('.toggle > input').is(':checked') &&
        !$(event.target).parents('.toggle').is('.toggle')
    ) {
        $('.toggle > input').prop('checked', false);
    }
});

// CHOOSE BANNER

const previewImage = (event) => {
    const imageFiles = event.target.files;
    const imageFilesLength = imageFiles.length;
    if (imageFilesLength > 0) {
        const imageSrc = URL.createObjectURL(imageFiles[0]);
        const imagePreviewElement = document.querySelector(
            '#preview-selected-image'
        );
        imagePreviewElement.src = imageSrc;

        imagePreviewElement.style.display = 'block';
    }
};
// SIDEBAR
(function ($) {
    'use strict';

    var fullHeight = function () {
        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function () {
            $('.js-fullheight').css('height', $(window).height());
        });
    };
    fullHeight();

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
})(jQuery);
