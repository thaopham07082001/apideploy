﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using capstone.api.Models;
using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Helpers;
using capstone.Models;

namespace capstone.Controllers {
    [Authorize]
    [CheckUserRole]
    public class HomeController : Controller {
        private const int VIEW_ALL_EVENTS_GRID_PAGE_SIZE = 10;
        private const int VIEW_ALL_EVENTS_LIST_PAGE_SIZE = 20;
        private const int GET_ALL_CATEGORIES_LIST_SIZE = 5;
        private const int GET_ALL_ORGANIZERS_AND_GROUPS_LIST_SIZE = 5;
        private const int GET_NOTIFICATIONS = 10;
        private const int NUMBER_FEEDBACKS_PER_LOAD = 5;
        private static readonly string[] PUBLIC_STATUSES = new string[] { EventStatusConstants.OPENING, EventStatusConstants.HAPPENING, EventStatusConstants.CLOSED };
        private static readonly string[] DEFAULT_STATUSES = new string[] { EventStatusConstants.OPENING, EventStatusConstants.HAPPENING };
        private const string ALL_CATEGORIES = "All categories";
        private const string ALL_ORGANIZERS_AND_GROUPS = "All organizers and groups";
        private const string DEFAULT_RANGE = "Within 2 months";
        private const string ALL_RANGE_FILTER = "All";
        private const string DEFAULT_RANGE_FILTER = "Default";
        private const string CUSTOM_RANGE_FILTER = "Custom";
        private const string ALL_STATUS_FILTER = "All";
        private const string DEFAULT_STATUS_FILTER = "Default";

        public ActionResult Index() {
            if ((User.IsCurrentlyUnassigned() && User.IsStudent()) || User.IsCurrentlyStudent())
                return RedirectToAction("Index", "Student");
            else if ((User.IsCurrentlyUnassigned() && User.IsOrganizer()) || User.IsCurrentlyOrganizer())
                return RedirectToAction("Index", "Organizer");
            else if ((User.IsCurrentlyUnassigned() && User.IsManager()) || User.IsCurrentlyManager())
                return RedirectToAction("Index", "Manager");
            else if ((User.IsCurrentlyUnassigned() && User.IsAdmin()) || User.IsCurrentlyAdmin())
                return RedirectToAction("Index", "Admin");
            return View("NotFound");
        }



        public ActionResult ViewUniversityEventCalendar(int? week, int? year, DateTime? date, EventSearchFilterViewModel model) {
            if (!ModelState.IsValid)
                return View("Error");
            if (date != null) {
                week = date.Value.GetWeekOfYear();
                year = date.Value.Year;
            }
            TimeSpan interval = new TimeSpan(1, 0, 0);
            List<DateTime> dates = new List<DateTime>();
            List<TimeSpan> times = new List<TimeSpan>();
            Dictionary<DateTime, List<CalendarModel>> calendarTime = new Dictionary<DateTime, List<CalendarModel>>();
            List<CalendarModel> calendarDay = new List<CalendarModel>();
            Dictionary<DateTime, int> stacksTime = new Dictionary<DateTime, int>();
            int stacksDay = 0;
            if (!year.HasValue)
                year = DateTime.Now.Year;
            if (!week.HasValue)
                week = DateTime.Now.GetWeekOfYear();
            DateTime startDate = CalendarHelper.GetFirstDayOfWeek(week.Value, year.Value);
            DateTime endDate = startDate.AddDays(7).AddSeconds(-1);
            int max = CalendarHelper.GetNumberOfWeeksInYear(year.Value);
            WeekModel weeks = new WeekModel() {
                PreviousWeek = week.Value > 1 ? week.Value - 1 : CalendarHelper.GetNumberOfWeeksInYear(year.Value - 1),
                PreviousYear = week.Value > 1 ? year.Value : year.Value - 1,
                NextWeek = week.Value < max ? week.Value + 1 : 1,
                NextYear = week.Value < max ? year.Value : year.Value + 1,
            };
            for (DateTime datee = startDate; datee <= endDate; datee = datee.AddDays(1)) {
                dates.Add(datee);
                calendarTime.Add(datee, new List<CalendarModel>());
                stacksTime.Add(datee, 0);
            }
            for (TimeSpan time = new TimeSpan(0, 0, 0); time < new TimeSpan(24, 00, 00); time = time.Add(interval))
                times.Add(time);
            IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e) {
                return PUBLIC_STATUSES.Contains(e.EventStatus) && CalendarHelper.CheckOverlap(e.OpenDate, e.CloseDate, startDate, endDate) && (model.Category == null || e.Categories.Count(c => c.CategoryId == model.Category.Value) > 0) && (model.OrganizerOrGroup == null || (!model.IsGroup && e.OrganizerId == model.OrganizerOrGroup) || (model.IsGroup && e.GroupId == model.OrganizerOrGroup));
            });
            foreach (Event eventt in dbevents)
                if (eventt.OpenDate.Date == eventt.CloseDate.Date) {
                    DateTime datee = eventt.OpenDate.Date;
                    int startTime = Convert.ToInt32(eventt.OpenDate.TimeOfDay.TotalMinutes / interval.TotalMinutes);
                    int endTime = Convert.ToInt32(eventt.CloseDate.TimeOfDay.TotalMinutes / interval.TotalMinutes);
                    CalendarModel newCalendarModel = new CalendarModel() {
                        Id = eventt.EventId,
                        Name = eventt.EventName,
                        Start = startTime,
                        End = endTime,
                        StartDate = eventt.OpenDate,
                        EndDate = eventt.CloseDate,
                        Stack = -1
                    };
                    for (int i = 1; i <= stacksTime[datee]; i++) {
                        bool overlap = false;
                        foreach (CalendarModel calendarModel in calendarTime[datee].Where((c, b) => c.Stack == i)) {
                            if (CalendarHelper.CheckOverlap(calendarModel.Start, calendarModel.End, newCalendarModel.Start, newCalendarModel.End)) {
                                overlap = true;
                                break;
                            }
                        }
                        if (!overlap) {
                            newCalendarModel.Stack = i;
                            break;
                        }
                    }
                    if (newCalendarModel.Stack < 0)
                        newCalendarModel.Stack = ++stacksTime[datee];
                    calendarTime[datee].Add(newCalendarModel);
                }
                else {
                    int startTime = 0;
                    int endTime = 6;
                    for (int i = 0; i < dates.Count; i++) {
                        if (eventt.OpenDate.Date == dates[i])
                            startTime = i;
                        if (eventt.CloseDate.Date == dates[i])
                            endTime = i;
                    }
                    CalendarModel newCalendarModel = new CalendarModel() {
                        Id = eventt.EventId,
                        Name = eventt.EventName,
                        Start = startTime,
                        End = endTime,
                        StartDate = eventt.OpenDate,
                        EndDate = eventt.CloseDate,
                        Stack = -1
                    };
                    for (int i = 1; i <= stacksDay; i++) {
                        bool overlap = false;
                        foreach (CalendarModel calendarModel in calendarDay.Where((c, b) => c.Stack == i)) {
                            if (CalendarHelper.CheckOverlap(calendarModel.Start, calendarModel.End, newCalendarModel.Start, newCalendarModel.End)) {
                                overlap = true;
                                break;
                            }
                        }
                        if (!overlap) {
                            newCalendarModel.Stack = i;
                            break;
                        }
                    }
                    if (newCalendarModel.Stack < 0)
                        newCalendarModel.Stack = ++stacksDay;
                    calendarDay.Add(newCalendarModel);
                }
            int current = Convert.ToInt32(new TimeSpan(6, 0, 0).TotalMinutes / interval.TotalMinutes);
            if (dbevents.Count() > 0 && dbevents.Where(e => e.OpenDate.Date == e.CloseDate.Date).ToList().Count != 0)
                current = Convert.ToInt32(dbevents.Where(e => e.OpenDate.Date == e.CloseDate.Date).Min(e => e.OpenDate.TimeOfDay).TotalMinutes / interval.TotalMinutes);
            string category = ALL_CATEGORIES;
            string organizerOrGroup = ALL_ORGANIZERS_AND_GROUPS;
            if (model.Category != null)
                category = db.Categories.Where(c => c.CategoryId == model.Category.Value).Single().CategoryName;
            if (model.OrganizerOrGroup != null) {
                if (!model.IsGroup)
                    organizerOrGroup = db.Users.Where(u => u.UserId == model.OrganizerOrGroup.Value && u.IsEnabled).Single().Email;
                else
                    organizerOrGroup = db.Groups.Where(g => g.GroupId == model.OrganizerOrGroup.Value && g.IsEnabled).Single().GroupName;
            }
            ViewBag.Dates = dates;
            ViewBag.Times = times;
            ViewBag.CalendarTime = calendarTime;
            ViewBag.CalendarDay = calendarDay;
            ViewBag.StacksTime = stacksTime;
            ViewBag.StacksDay = stacksDay;
            ViewBag.Current = current;
            ViewBag.Weeks = weeks;
            ViewBag.Category = category;
            ViewBag.OrganizerOrGroup = organizerOrGroup;
            return View(model);
        }

        public ActionResult ViewAllEvents(int? page, EventSearchFilterViewModel model) {
            if (string.IsNullOrEmpty(model.Status))
                model.Status = DEFAULT_STATUS_FILTER;
            if (model.Status != DEFAULT_STATUS_FILTER && model.Status != ALL_STATUS_FILTER && !PUBLIC_STATUSES.Contains(model.Status))
                ModelState.AddModelError("Status", "Status not public or not available");
            if (!ModelState.IsValid)
                return View("Error");
            if (string.IsNullOrEmpty(model.Range))
                model.Range = DEFAULT_RANGE_FILTER;
            if (model.Range.Equals(DEFAULT_RANGE_FILTER)) {
                model.From = DateTime.Now.ToStandardString();
                model.To = CalendarHelper.GetDateFromString(model.From).AddMonths(2).ToStandardString();
            }
            if (string.IsNullOrEmpty(model.Display))
                model.Display = DisplayModeConstants.GRID;
            int pagesize = 0;
            if (model.Display.Equals(DisplayModeConstants.GRID))
                pagesize = VIEW_ALL_EVENTS_GRID_PAGE_SIZE;
            else if (model.Display.Equals(DisplayModeConstants.LIST))
                pagesize = VIEW_ALL_EVENTS_LIST_PAGE_SIZE;
            else
                return View("Error");
            DateTime startDate;
            DateTime endDate;
            if (!model.Range.Equals(ALL_RANGE_FILTER)) {
                startDate = CalendarHelper.GetDateFromString(model.From);
                endDate = CalendarHelper.GetDateFromString(model.To);
            }
            else {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            model.Search = model.Search.ToUnsignString();
            IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e) {
                return (model.Range.Equals(ALL_RANGE_FILTER) || CalendarHelper.CheckOverlap(e.OpenDate, e.CloseDate, startDate, endDate)) && ((model.Status.Equals(ALL_STATUS_FILTER) && PUBLIC_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(ALL_STATUS_FILTER) && ((model.Status.Equals(DEFAULT_STATUS_FILTER) && DEFAULT_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(DEFAULT_STATUS_FILTER) && e.EventStatus.Equals(model.Status))))) && (string.IsNullOrEmpty(model.Search) || e.EventName.ToUnsignString().ContainsIgnoreCase(model.Search) || e.EventDescription.ToUnsignString().ContainsIgnoreCase(model.Search) || (e.EventFamily != null && e.EventFamily.FamilyName.ToUnsignString().ContainsIgnoreCase(model.Search)) || e.EventPlace.ToUnsignString().ContainsIgnoreCase(model.Search)) && (model.Category == null || e.Categories.Count(c => c.CategoryId == model.Category.Value) > 0) && (model.OrganizerOrGroup == null || (!model.IsGroup && e.OrganizerId == model.OrganizerOrGroup) || (model.IsGroup && e.GroupId == model.OrganizerOrGroup));
            });
            PageModel pages = new PageModel {
                Action = "ViewAllEvents",
                Size = pagesize,
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = Convert.ToInt32(Math.Ceiling(1.0 * dbevents.Count() / pagesize))
            };
            List<EventModel> events = new List<EventModel>();
            foreach (Event eventt in dbevents.OrderBy(u => u.OpenDate).Skip((pages.Current - 1) * pagesize).Take(pagesize)) {
                User user = db.Users.Where(u => u.UserId == eventt.OrganizerId).Single();
                Group group = db.Groups.Where(g => g.GroupId == eventt.GroupId && g.IsEnabled).SingleOrDefault();
                List<Group> currgroups = new List<Group>();
                bool canRegisterByGroup = true;
                try {
                    canRegisterByGroup = eventt.IsRegisteredByGroup(ref currgroups, User.GetUser());
                }
                catch {
                    currgroups = new List<Group>();
                    canRegisterByGroup = eventt.IsRegisteredByGroup(ref currgroups, null);
                }
                events.Add(new EventModel() {
                    Id = eventt.EventId,
                    Name = eventt.EventName,
                    CoverImage = eventt.CoverImage,
                    IsPublic = eventt.IsPublic,
                    IsFeatured = eventt.IsFeatured,
                    Status = eventt.EventStatus,
                    Description = eventt.EventDescription,
                    Place = eventt.EventPlace,
                    Organizer = new UserModel() {
                        Id = user.UserId,
                        FirstName = user.UserProfile.FirstName,
                        LastName = user.UserProfile.LastName,
                        ProfileImage = user.UserProfile.ProfileImage
                    },
                    Group = group != null ? new GroupModel() {
                        Id = group.GroupId,
                        Name = group.GroupName
                    } : null,
                    CreatedDate = eventt.CreatedDate,
                    OpenDate = eventt.OpenDate,
                    CloseDate = eventt.CloseDate,
                    RegisterEndDate = eventt.RegisterEndDate ?? eventt.OpenDate,
                    IsRegistered = eventt.Registers.Join(db.Users.Where(u => u.Email.Equals(User.Identity.Name)), r => r.UserId, u => u.UserId, (r, u) => u).Count() > 0,
                    RegisterCount = eventt.Registers.Count,
                    RegisterMax = eventt.RegisterMax,
                    IsOrganizerOnly = eventt.IsOrganizerOnly,
                    CanRegisterByGroup = canRegisterByGroup,
                });
            }
            string category = ALL_CATEGORIES;
            string organizerOrGroup = ALL_ORGANIZERS_AND_GROUPS;
            if (model.Category != null)
                category = db.Categories.Where(c => c.CategoryId == model.Category.Value).Single().CategoryName;
            if (model.OrganizerOrGroup != null) {
                if (!model.IsGroup)
                    organizerOrGroup = db.Users.Where(u => u.UserId == model.OrganizerOrGroup.Value && u.IsEnabled).Single().Email;
                else
                    organizerOrGroup = db.Groups.Where(g => g.GroupId == model.OrganizerOrGroup.Value && g.IsEnabled).Single().GroupName;
            }
            List<SelectListItem> statuses = new List<SelectListItem> {
                    new SelectListItem() {
                        Text = ALL_STATUS_FILTER,
                        Value = ALL_STATUS_FILTER
                    },
                    new SelectListItem() {
                        Text = string.Join(" and ", DEFAULT_STATUSES),
                        Value = DEFAULT_STATUS_FILTER
                    }
                };
            foreach (string status in PUBLIC_STATUSES)
                statuses.Add(new SelectListItem() {
                    Text = status,
                });
            List<SelectListItem> ranges = new List<SelectListItem> {
                    new SelectListItem() {
                        Text = ALL_RANGE_FILTER,
                        Value = ALL_RANGE_FILTER
                    },
                    new SelectListItem() {
                        Text = DEFAULT_RANGE,
                        Value = DEFAULT_RANGE_FILTER
                    },
                    new SelectListItem() {
                        Value = CUSTOM_RANGE_FILTER
                    }
                };
            if (model.Range == null)
                model.Range = DEFAULT_RANGE_FILTER;
            ViewBag.Events = events;
            ViewBag.Category = category;
            ViewBag.OrganizerOrGroup = organizerOrGroup;
            ViewBag.Statuses = statuses;
            ViewBag.Ranges = ranges;
            ViewBag.CustomRangeFilter = CUSTOM_RANGE_FILTER;
            ViewBag.Pages = pages;
            return View(model);
        }

        [HttpPost]
        public ActionResult GetAllCategories(string search) {
            search = search.ToUnsignString();
            List<SelectListItem> categories = db.Categories.Where(delegate (Category c) {
                return c.IsEnabled && c.CategoryName.ToUnsignString().ContainsIgnoreCase(search);
            }).Take(GET_ALL_CATEGORIES_LIST_SIZE).Select(c => new SelectListItem() {
                Text = c.CategoryName,
                Value = c.CategoryId.ToString()
            }).ToList();
            ViewBag.Categories = categories;
            return PartialView();
        }

        [HttpPost]
        public ActionResult GetAllOrganizersAndGroups(string search) {
            search = search.ToUnsignString();
            List<SelectListItem> organizers = db.Users.Where(delegate (User u) {
                return u.IsEnabled && u.IsOrganizer && u.Email.ToUnsignString().ContainsIgnoreCase(search);
            }).Take(GET_ALL_ORGANIZERS_AND_GROUPS_LIST_SIZE).Select(u => new SelectListItem() {
                Text = u.Email,
                Value = u.UserId.ToString()
            }).ToList();
            List<SelectListItem> groups = db.Groups.Where(delegate (Group g) {
                return g.IsEnabled && g.GroupName.ToUnsignString().ContainsIgnoreCase(search);
            }).Take(GET_ALL_ORGANIZERS_AND_GROUPS_LIST_SIZE).Select(g => new SelectListItem() {
                Text = g.GroupName,
                Value = g.GroupId.ToString()
            }).ToList();
            ViewBag.Organizers = organizers;
            ViewBag.Groups = groups;
            return PartialView();
        }

        [HttpPost]
        public ActionResult GetNotificationsNumber() {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).SingleOrDefault();
            ViewBag.Notifications = db.Notifications.Where(n => n.UserId == user.UserId && n.NotifiedDate > user.NotificationSeenDate).Count();
            return PartialView();
        }

        [HttpPost]
        public ActionResult GetNotifications(int skip, bool seen) {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).SingleOrDefault();
            IEnumerable<Notification> dbnotifications = Enumerable.Empty<Notification>();
            if (skip == 0)
                dbnotifications = db.Notifications.Where(n => n.UserId == user.UserId && n.NotifiedDate > user.NotificationSeenDate).OrderByDescending(n => n.NotifiedDate);
            if (skip != 0 || dbnotifications.Count() == 0)
                dbnotifications = db.Notifications.Where(n => n.UserId == user.UserId).OrderByDescending(n => n.NotifiedDate).Skip(skip).Take(GET_NOTIFICATIONS);
            List<NotificationModel> notifications = new List<NotificationModel>();
            foreach (Notification notification in dbnotifications) {
                NotificationModel notificationModel = new NotificationModel() {
                    NotifiedDate = notification.NotifiedDate,
                    Content = notification.NotificationContent
                };
                User subject = db.Users.Where(u => u.UserId == notification.SubjectId).SingleOrDefault();
                if (subject != null) {
                    notificationModel.Subject = new UserModel() {
                        Id = subject.UserId,
                        FirstName = subject.UserProfile != null ? subject.UserProfile.FirstName : subject.Email,
                        LastName = subject.UserProfile != null ? subject.UserProfile.LastName : string.Empty,
                        ProfileImage = subject.UserProfile != null ? subject.UserProfile.ProfileImage : string.Empty
                    };
                }
                switch (notification.ObjectType) {
                    case ObjectTypeConstants.EVENT: {
                        notificationModel.Object = db.Events.Where(e => e.EventId == notification.ObjectId).Select(e => new ObjectModel() {
                            Name = e.EventName,
                            Link = "/Home/ViewEvent/" + e.EventId
                        }).Single();
                        break;
                    }
                    case ObjectTypeConstants.GROUP: {
                        notificationModel.Object = db.Groups.Where(g => g.GroupId == notification.ObjectId).Select(e => new ObjectModel() {
                            Name = e.GroupName,
                            Link = "/Home/ViewGroup/" + e.GroupId
                        }).Single();
                        break;
                    }
                }
                notifications.Add(notificationModel);
            };
            if (seen) {
                user.NotificationSeenDate = DateTime.Now;
                db.SaveChanges();
            }
            ViewBag.Notifications = notifications;
            return PartialView();
        }


        [AllowAnonymous]
        public ActionResult ViewEvent(int id)
        {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null || eventt.EventStatus.Equals(EventStatusConstants.DELETED))
                return View("NotFound");
            else
                this.SetMeta(eventt.EventName, Request.Url.AbsoluteUri, Request.Url.Scheme + "://" + Request.Url.Host + eventt.CoverImage, eventt.EventDescription.Substring(0, Math.Min(eventt.EventDescription.Length - 1, 50)) + "...");
            List<Group> currGroups = new List<Group>();
            bool canRegisterByGroup = true;
            try
            {
                canRegisterByGroup = eventt.IsRegisteredByGroup(ref currGroups, User.GetUser());
            }
            catch
            {
                currGroups = new List<Group>();
                canRegisterByGroup = eventt.IsRegisteredByGroup(ref currGroups, null);
            }
            ViewBag.currGroups = currGroups;
            ViewBag.canRegisterByGroup = canRegisterByGroup;
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsCurrentlyStudent() && !PUBLIC_STATUSES.Contains(eventt.EventStatus))
                    return View("~/Views/Authentication/Login.cshtml", new ExternalLoginListViewModel() { ReturnUrl = Request.Url.AbsolutePath });
                else if (User.IsCurrentlyOrganizer() && !PUBLIC_STATUSES.Contains(eventt.EventStatus) && eventt.OrganizerId != User.GetUser().UserId)
                    return View("~/Views/Authentication/Login.cshtml", new ExternalLoginListViewModel() { ReturnUrl = Request.Url.AbsolutePath });
            }
            else
            {
                if (!PUBLIC_STATUSES.Contains(eventt.EventStatus) || !eventt.IsPublic)
                    return View("~/Views/Authentication/Login.cshtml", new ExternalLoginListViewModel() { ReturnUrl = Request.Url.AbsolutePath });
            }
            List<FeedbackModel> feedbackModels = eventt.GetFeedbacks();
            List<Event> relatedEvent = new List<Event>();
            foreach (Category c in eventt.Categories.Where(c => c.IsEnabled))
                relatedEvent.AddRange(c.Events.Where(e => Enumerable.Contains(DEFAULT_STATUSES, e.EventStatus)));
            List<Event> tempRelatedEvent = new List<Event>();
            foreach (Event e in relatedEvent)
                if (!tempRelatedEvent.Contains(e))
                    tempRelatedEvent.Add(e);
            tempRelatedEvent.Remove(eventt);
            relatedEvent = tempRelatedEvent;
            if (eventt.EventFamily != null)
            {
                foreach (Event e in eventt.EventFamily.Events)
                    if (relatedEvent.Any(e2 => e2.EventId == e.EventId))
                        relatedEvent.Remove(e);
                eventt.EventFamily.Events.Remove(eventt);
                eventt.EventFamily.Events = eventt.EventFamily.Events.Where(e => PUBLIC_STATUSES.Contains(e.EventStatus)).OrderBy(e => e.OpenDate).ToList();
            }
            ViewBag.AvgRating = eventt.GetRating();
            ViewBag.FeedbackCount = eventt.GetFeedbacks().Count;

            if (User.Identity.IsAuthenticated)
                goto LoggedinView;
            else
                goto AnomymousView;

            LoggedinView:
            relatedEvent = relatedEvent.Shuffle().Take(5).ToList();
            ViewBag.RelatedEvent = relatedEvent;
            User currUser = User.GetUser();
            ViewBag.Feedback = eventt.Feedbacks.Where(e => e.UserId == currUser.UserId).SingleOrDefault();
            feedbackModels.Remove(feedbackModels.Where(f => f.Key.Equals(currUser.UserId.ToString())).SingleOrDefault());
            ViewBag.Feedbacks = feedbackModels.Take(NUMBER_FEEDBACKS_PER_LOAD).ToList();
            string sameTimeEvent = "";
            List<Bookmark> bookmarks = db.Bookmarks.Where(b => b.UserId == currUser.UserId).ToList();
            foreach (Bookmark b in bookmarks)
                if (CalendarHelper.CheckOverlap(eventt.OpenDate, eventt.CloseDate, b.Event.OpenDate, b.Event.CloseDate))
                {
                    if (b.Event.EventId != eventt.EventId)
                    {
                        sameTimeEvent = b.Event.EventName;
                        break;
                    }
                }
            foreach (Register e in db.Registers.Where(r => r.UserId == currUser.UserId))
                if (CalendarHelper.CheckOverlap(eventt.OpenDate, eventt.CloseDate, e.Event.OpenDate, e.Event.CloseDate))
                {
                    if (e.Event.EventId != eventt.EventId)
                    {
                        sameTimeEvent = e.Event.EventName;
                        break;
                    }
                }
            IEnumerable<CalendarApiModel> calendarModels = ApiHelper.GetCalendars(User.Identity.Name, eventt.OpenDate, eventt.CloseDate);
            if (calendarModels.Count() != 0)
                sameTimeEvent = calendarModels.First().Name;
            ViewBag.SameTimeEvent = sameTimeEvent;
            ViewBag.NumberFeedbacksPerLoad = NUMBER_FEEDBACKS_PER_LOAD;
            return View(eventt);

        AnomymousView:
            relatedEvent = relatedEvent.Where(e => e.IsPublic).ToList().Shuffle().Take(5).ToList();
            ViewBag.RelatedEvent = relatedEvent;
            if (Session["fbUSer"] != null)
            {
                FacebookModel fbUser = (FacebookModel)Session["fbUSer"];
                ViewBag.Feedback = eventt.FeedbackOuters.Where(f => f.Key == fbUser.Id).SingleOrDefault();
                feedbackModels.Remove(feedbackModels.Where(f => f.Key.Equals(fbUser.Id)).SingleOrDefault());
            }
            ViewBag.Feedbacks = feedbackModels.Take(NUMBER_FEEDBACKS_PER_LOAD).ToList();
            ViewBag.NumberFeedbacksPerLoad = NUMBER_FEEDBACKS_PER_LOAD;
            return View("ViewEventAnonymous", eventt);
        }

        public ActionResult ViewEventAnonymous(Event eventt)
        {
            return View(eventt);
        }

        public ActionResult ViewAllGroups() {
            int currUserId = User.GetUser().UserId;
            User currUser = db.Users.Where(u => u.UserId == currUserId).SingleOrDefault();
            List<Group> groupList = db.Groups.Where(x => x.IsEnabled == true).ToList();

            if (User.IsCurrentlyOrganizer()) {
                List<Group> leaderGroup = new List<Group>();
                leaderGroup = currUser.Groups.Where(g => g.IsEnabled == true).ToList();
                ViewBag.LeaderGroup = leaderGroup;

                List<Group> memberGroup = new List<Group>();
                foreach (UserGroup ug in db.Users.Where(u => u.UserId == currUser.UserId).Single().UserGroups) {
                    Group tmpGroup = db.Groups.Where(g => g.GroupId == ug.GroupId && g.IsEnabled == true && g.LeaderId != currUserId).SingleOrDefault();
                    if (tmpGroup != null)
                        memberGroup.Add(tmpGroup);
                }
                ViewBag.MemberGroup = memberGroup;

                foreach (Group g in leaderGroup) {
                    groupList.Remove(g);
                }
                foreach (Group g in memberGroup) {
                    groupList.Remove(g);
                }
            }
            else if (User.IsCurrentlyStudent()) {
                List<Group> memberGroup = new List<Group>();
                foreach (UserGroup ug in db.Users.Where(u => u.UserId == currUser.UserId).Single().UserGroups) {
                    Group tmpGroup = db.Groups.Where(g => g.GroupId == ug.GroupId && g.IsEnabled == true).SingleOrDefault();
                    if (tmpGroup != null)
                        memberGroup.Add(tmpGroup);
                }
                ViewBag.MemberGroup = memberGroup;

                foreach (Group g in memberGroup) {
                    groupList.Remove(g);
                }
            }
            return View(groupList);
        }

        public ActionResult ViewGroup(int id) {
            Group grp = db.Groups.SingleOrDefault(x => x.GroupId == id);
            if (!grp.IsEnabled)
                return RedirectToAction("ViewAllGroups");
            List<UserGroup> userGroupList = db.UserGroups.Where(u => u.GroupId == grp.GroupId).OrderBy(u => u.ParticipatedDate).ToList();
            List<MemberModel> groupMemberList = new List<MemberModel>();
            foreach (UserGroup item in userGroupList)
                if (item.OrganizerId == grp.LeaderId)
                    groupMemberList.Insert(0, new MemberModel() {
                        Id = item.OrganizerId,
                        Name = item.User.UserProfile.LastName + " " + item.User.UserProfile.FirstName,
                        Email = item.User.Email,
                        JoinedDate = item.ParticipatedDate
                    });
                else
                    if (item.User.UserProfile == null) {
                    groupMemberList.Add(new MemberModel() {
                        Id = item.OrganizerId,
                        Name = "",
                        Email = item.User.Email,
                        JoinedDate = item.ParticipatedDate
                    });
                }
                else {
                    string tmpName = item.User.UserProfile.LastName ?? "";
                    tmpName += " ";
                    tmpName += item.User.UserProfile.FirstName ?? "";
                    groupMemberList.Add(new MemberModel() {
                        Id = item.OrganizerId,
                        Name = tmpName,
                        Email = item.User.Email,
                        JoinedDate = item.ParticipatedDate
                    });
                }

            List<Event> tmpEvents = db.Events.Where(e => e.GroupId == id && e.EventStatus == EventStatusConstants.CLOSED).OrderByDescending(e => e.OpenDate).ToList();
            List<Event> recentEvents = db.Events.Where(e => e.GroupId == id && DEFAULT_STATUSES.Contains(e.EventStatus)).OrderByDescending(e => e.OpenDate).Take(5).ToList();
            while (recentEvents.Count < 5) {
                if (tmpEvents.Count == 0)
                    break;
                recentEvents.Add(tmpEvents.First());
                tmpEvents.RemoveAt(0);
            }
            ViewBag.recentEvents = recentEvents.OrderByDescending(e => e.OpenDate).ToList();
            ViewBag.GroupMember = groupMemberList;
            return View(grp);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateFeedbackAnonymous(FeedbackOuter feedback) {
            FacebookModel fbUser = (FacebookModel)Session["fbUSer"];
            feedback.UserName = fbUser.Name;
            feedback.UserImage = fbUser.Picture;
            if (db.FeedbackOuters.Any(f => f.Key.Equals(feedback.Key) && f.EventId == feedback.EventId)) {
                FeedbackOuter feedbackDB = db.FeedbackOuters.Where(f => f.Key.Equals(feedback.Key) && f.EventId == feedback.EventId).SingleOrDefault();
                feedbackDB.Value = feedback.Value;
                feedbackDB.FeedbackContent = feedback.FeedbackContent;
                feedbackDB.CreatedDate = feedback.CreatedDate;
            }
            else
                db.FeedbackOuters.Add(feedback);
            db.SaveChanges();
            feedback = db.FeedbackOuters.Where(f => f.Key.Equals(feedback.Key) && f.EventId == feedback.EventId).SingleOrDefault();
            Event fbEvent = db.Events.Single(e => e.EventId == feedback.EventId);
            return Json(new FeedbackModel() {
                Key = feedback.Key,
                Username = feedback.UserName,
                UserImage = feedback.UserImage,
                Value = feedback.Value,
                CreatedDate = feedback.CreatedDate,
                txtCreatedDate = feedback.CreatedDate.ToDateTimeString(),
                Content = feedback.FeedbackContent,
                AvgRating = fbEvent.GetRating(),
                FeedbackCount = fbEvent.GetFeedbacks().Count
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteFeedbackAnonymous(FeedbackOuter feedback) {
            db.FeedbackOuters.Remove(db.FeedbackOuters.Where(f => f.Key.Equals(feedback.Key) && f.EventId == feedback.EventId).SingleOrDefault());
            db.SaveChanges();
            Event eventt = db.Events.Single(e => e.EventId == feedback.EventId);
            return Json(new FeedbackModel() {
                AvgRating = eventt.GetRating(),
                FeedbackCount = eventt.GetFeedbacks().Count
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewProfile(int id) {
            User user = db.Users.Where(u => u.UserId == id && u.UserProfile != null).SingleOrDefault();
            if (user == null)
                return View("NotFound");
            List<Event> events = null;
            if (user.IsOrganizer)
                events = db.Events.Where(e => e.OrganizerId == user.UserId && PUBLIC_STATUSES.Contains(e.EventStatus)).OrderByDescending(e => e.CreatedDate).Take(5).ToList();
            ViewBag.User = user;
            ViewBag.Profile = user.UserProfile;
            ViewBag.Events = events;
            return View();
        }

        public ActionResult ManageYourProfile() {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name) && u.UserProfile != null).SingleOrDefault();
            if (user == null)
                return View("NotFound");
            List<Event> events = null;
            if (user.IsOrganizer)
                events = db.Events.Where(e => e.OrganizerId == user.UserId && PUBLIC_STATUSES.Contains(e.EventStatus)).OrderByDescending(e => e.CreatedDate).Take(5).ToList();
            ManageYourProfileViewModel profile = new ManageYourProfileViewModel() {
                FirstName = user.UserProfile.FirstName,
                LastName = user.UserProfile.LastName,
                Phone = user.UserProfile.Phone,
                ProfileImage = user.UserProfile.ProfileImage,
                ProfileId = user.UserProfile.ProfileId
            };
            ViewBag.User = user;
            ViewBag.Profile = user.UserProfile;
            ViewBag.Events = events;
            return View(profile);
        }
        //New Version
        public ActionResult ManageProfile()
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name) && u.UserProfile != null).SingleOrDefault();
            if (user == null)
                return View("NotFound");
            List<Event> events = null;
            if (user.IsOrganizer)
                events = db.Events.Where(e => e.OrganizerId == user.UserId && PUBLIC_STATUSES.Contains(e.EventStatus)).OrderByDescending(e => e.CreatedDate).Take(5).ToList();
            ManageYourProfileViewModel profile = new ManageYourProfileViewModel()
            {
                FirstName = user.UserProfile.FirstName,
                LastName = user.UserProfile.LastName,
                Phone = user.UserProfile.Phone,
                ProfileImage = user.UserProfile.ProfileImage,
                ProfileId = user.UserProfile.ProfileId
            };
            ViewBag.User = user;
            ViewBag.Profile = user.UserProfile;
            ViewBag.Events = events;
            return View(profile);
        }

        [HttpPost]
        public ActionResult ManageYourProfile(ManageYourProfileViewModel model)
        {
            if (!ModelState.IsValid)
                return ManageYourProfile();
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            user.UserProfile.FirstName = model.FirstName;
            user.UserProfile.LastName = model.LastName;
            user.UserProfile.Phone = model.Phone;
            if (model.Image != null)
            {
                string image = model.Image.SaveImage("~/Images/ProfileImage/");
                user.UserProfile.ProfileImage = "/Images/ProfileImage/" + image;
            }
            db.SaveChanges();
            Session["User"] = user;
            this.SetMessage("Successfully edited your profile");
            return RedirectToAction("ManageProfile", new { id = user.UserId });
        }

        [HttpPost]
        public JsonResult ReportEvent(int eventId, string repDes) {
            int curUserId = User.GetUser().UserId;
            db.Reports.Add(new Report() {
                EventId = eventId,
                UserId = curUserId,
                CreatedDate = DateTime.Now,
                Description = repDes,
                IsDismissed = false
            });
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult GetFeedback(int eventId, int skip) {
            string key;
            if (User.Identity.IsAuthenticated)
                key = User.GetUser().UserId.ToString();
            else if (Session["fbUSer"] != null) {
                FacebookModel tmp = (FacebookModel)Session["fbUSer"];
                key = tmp.Id;
            }
            else
                key = "";
            List<FeedbackModel> feedbacks = db.Events.Where(e => e.EventId == eventId).Single().GetFeedbacks();
            feedbacks.Remove(feedbacks.SingleOrDefault(f => f.Key == key));
            return Json(feedbacks.Skip(skip * NUMBER_FEEDBACKS_PER_LOAD).Take(NUMBER_FEEDBACKS_PER_LOAD).ToList(), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ViewTeam() {
            return View();
        }

        private Database db;

        public HomeController() {
            db = new Database();
        }

        public HomeController(Database db) {
            this.db = db;
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}