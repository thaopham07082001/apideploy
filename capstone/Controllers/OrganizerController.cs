﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Helpers;
using capstone.Models;

namespace capstone.Controllers
{
    [Authorize(Roles = "Organizer")]
    [CheckUserRole]
    public class OrganizerController : Controller
    {
        private const int VIEW_ALL_YOUR_EVENTS_PAGE_SIZE = 20;
        private const int VIEW_YOUR_DRAFT_EVENTS_PAGE_SIZE = 20;
        private const int VIEW_YOUR_PENDING_EVENTS_PAGE_SIZE = 20;
        private const int VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE = 20;
        private const int VIEW_YOUR_CHECKEDIN_STUDENTS_PAGE_SIZE = 20;
        private static readonly string[] PUBLIC_STATUSES = new string[] { EventStatusConstants.OPENING, EventStatusConstants.HAPPENING, EventStatusConstants.CLOSED, EventStatusConstants.PENDING, EventStatusConstants.REJECTED };
        private static readonly string[] DEFAULT_STATUSES = new string[] { EventStatusConstants.OPENING, EventStatusConstants.HAPPENING, EventStatusConstants.CLOSED, EventStatusConstants.PENDING, EventStatusConstants.REJECTED };
        private static readonly string[] AFTER_STATUSES = new string[] { EventStatusConstants.HAPPENING, EventStatusConstants.CLOSED };
        private const string DEFAULT_STATUS_FILTER = "Default";
        private const string ALL_STATUS_FILTER = "All";

        [AllowAnonymous]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Authentication", new { ReturnUrl = "/Organizer/" });
            else if (!User.IsOrganizer())
                return View("Unauthorized");
            else
            {
                User user = User.GetUser();
                int drafts = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventStatus.Equals(EventStatusConstants.DRAFT)).Count();
                int rejected = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventStatus.Equals(EventStatusConstants.REJECTED)).Count();
                int pending = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventStatus.Equals(EventStatusConstants.PENDING)).Count();
                List<EventModel> happenings = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventStatus.Equals(EventStatusConstants.HAPPENING)).OrderBy(u => u.OpenDate).Select(e => new EventModel()
                {
                    Id = e.EventId,
                    Name = e.EventName,
                    CoverImage = e.CoverImage,
                    Description = e.EventDescription
                }).ToList();
                ViewBag.Drafts = drafts;
                ViewBag.Rejected = rejected;
                ViewBag.Pending = pending;
                ViewBag.Happenings = happenings;
            }
            return View();
        }

        public ActionResult ViewAllYourEvents2(int? page, EventSearchFilterViewModel model)
        {
            if (string.IsNullOrEmpty(model.Status))
                model.Status = DEFAULT_STATUS_FILTER;
            if (model.Status != DEFAULT_STATUS_FILTER && model.Status != ALL_STATUS_FILTER && !PUBLIC_STATUSES.Contains(model.Status))
                ModelState.AddModelError("Status", "Status not public or not available");
            if (!ModelState.IsValid)
                return View("Error");
            model.Search = model.Search.ToUnsignString();
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e)
            {
                return e.OrganizerId == user.UserId && ((model.Status.Equals(ALL_STATUS_FILTER) && PUBLIC_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(ALL_STATUS_FILTER) && ((model.Status.Equals(DEFAULT_STATUS_FILTER) && DEFAULT_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(DEFAULT_STATUS_FILTER) && e.EventStatus.Equals(model.Status))))) && (string.IsNullOrEmpty(model.Search) || e.EventName.ToUnsignString().ContainsIgnoreCase(model.Search) || e.EventDescription.ToUnsignString().ContainsIgnoreCase(model.Search) || (e.EventFamily != null && e.EventFamily.FamilyName.ToUnsignString().ContainsIgnoreCase(model.Search)) || e.EventPlace.ToUnsignString().ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel
            {
                Action = "ViewAllYourEvents",
                Size = VIEW_ALL_YOUR_EVENTS_PAGE_SIZE,
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = Convert.ToInt32(Math.Ceiling(1.0 * dbevents.Count() / VIEW_ALL_YOUR_EVENTS_PAGE_SIZE))
            };
            List<EventModel> events = new List<EventModel>();
            foreach (Event eventt in dbevents.OrderBy(u => u.OpenDate).Skip((pages.Current - 1) * VIEW_ALL_YOUR_EVENTS_PAGE_SIZE).Take(VIEW_ALL_YOUR_EVENTS_PAGE_SIZE))
            {
                Group group = db.Groups.Where(g => g.GroupId == eventt.GroupId && g.IsEnabled).SingleOrDefault();
                events.Add(new EventModel()
                {
                    Id = eventt.EventId,
                    Name = eventt.EventName,
                    Status = eventt.EventStatus,
                    Group = group != null ? new GroupModel()
                    {
                        Id = group.GroupId,
                        Name = group.GroupName
                    } : null,
                    CreatedDate = eventt.CreatedDate,
                    OpenDate = eventt.OpenDate,
                    CloseDate = eventt.CloseDate
                });
            }
            List<SelectListItem> statuses = new List<SelectListItem> {
                    new SelectListItem() {
                        Text = ALL_STATUS_FILTER,
                        Value = ALL_STATUS_FILTER
                    },
                    new SelectListItem() {
                        Text = string.Join(" and ", DEFAULT_STATUSES),
                        Value = DEFAULT_STATUS_FILTER
                    }
                };
            foreach (string status in PUBLIC_STATUSES)
                statuses.Add(new SelectListItem()
                {
                    Text = status,
                });
            ViewBag.Events = events;
            ViewBag.Statuses = statuses;
            ViewBag.Pages = pages;
            return View(model);
        }

        public ActionResult ViewYourEventStatistics(int id)
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventId == id).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            IEnumerable<Register> registers = eventt.Registers.Where(r => r.User.UserProfile != null);
            List<string> registerDateLabels = new List<string>();
            List<DateTime> registerDates = new List<DateTime>();
            List<string> registerDateData = new List<string>();
            if (eventt.Registers.Count > 0)
            {
                DateTime registerTo = DateTime.Now;
                if (eventt.RegisterEndDate.HasValue && eventt.RegisterEndDate < registerTo)
                    registerTo = eventt.RegisterEndDate.Value;
                for (DateTime date = eventt.ApprovedDate.Value; date <= registerTo; date = date.AddDays(1))
                {
                    registerDateLabels.Add(date.ToShortDateString());
                    registerDates.Add(date.Date);
                }
                foreach (DateTime date in registerDates)
                    registerDateData.Add(registers.Count(r => r.RegisteredDate.Date <= date).ToString());
            }
            int registerMale = 0;
            int registerFemale = 0;
            foreach (Register register in registers)
                if (register.User.UserProfile.Gender)
                    registerMale++;
                else
                    registerFemale++;
            List<string> registerCityLabels = registers.Select(r => r.User.UserProfile.City).Distinct().ToList();
            List<string> registerCityData = new List<string>();
            foreach (string city in registerCityLabels)
                registerCityData.Add(registers.Count(r => r.User.UserProfile.City.Equals(city)).ToString());
            List<string> registerCampusLabels = registers.Select(r => r.User.UserProfile.Campus).Distinct().ToList();
            List<string> registerCampusData = new List<string>();
            foreach (string campus in registerCampusLabels)
                registerCampusData.Add(registers.Count(r => r.User.UserProfile.Campus.Equals(campus)).ToString());
            List<string> registerMajorLabels = registers.Select(r => r.User.UserProfile.Major).Distinct().ToList();
            List<string> registerMajorData = new List<string>();
            foreach (string major in registerMajorLabels)
                registerMajorData.Add(registers.Count(r => r.User.UserProfile.Major.Equals(major)).ToString());
            List<string> registerSpecializationLabels = registers.Select(r => r.User.UserProfile.Specialization).Distinct().ToList();
            List<string> registerSpecializationData = new List<string>();
            foreach (string specialization in registerSpecializationLabels)
                registerSpecializationData.Add(registers.Count(r => r.User.UserProfile.Specialization.Equals(specialization)).ToString());
            if (!eventt.EventStatus.Equals(EventStatusConstants.OPENING))
            {
                IEnumerable<Checkin> checkins = eventt.Checkins.Where(c => c.User.UserProfile != null);
                List<string> checkinDateLabels = new List<string>();
                List<DateTime> checkinDates = new List<DateTime>();
                List<string> checkinDateData = new List<string>();
                if (eventt.Checkins.Count > 0)
                {
                    DateTime checkinTo = DateTime.Now;
                    for (DateTime date = eventt.OpenDate; date <= checkinTo; date = date.AddDays(1))
                    {
                        checkinDateLabels.Add(date.ToShortDateString());
                        checkinDates.Add(date.Date);
                    }
                    foreach (DateTime date in checkinDates)
                        checkinDateData.Add(checkins.Count(c => c.CheckedinDate.Date <= date).ToString());
                }
                int checkinMale = 0;
                int checkinFemale = 0;
                foreach (Checkin checkin in checkins)
                    if (checkin.User.UserProfile.Gender)
                        checkinMale++;
                    else
                        checkinFemale++;
                List<string> checkinCityLabels = checkins.Select(c => c.User.UserProfile.City).Distinct().ToList();
                List<string> checkinCityData = new List<string>();
                foreach (string city in checkinCityLabels)
                    checkinCityData.Add(checkins.Count(c => c.User.UserProfile.City.Equals(city)).ToString());
                List<string> checkinCampusLabels = checkins.Select(c => c.User.UserProfile.Campus).Distinct().ToList();
                List<string> checkinCampusData = new List<string>();
                foreach (string campus in checkinCampusLabels)
                    checkinCampusData.Add(checkins.Count(c => c.User.UserProfile.Campus.Equals(campus)).ToString());
                List<string> checkinMajorLabels = checkins.Select(c => c.User.UserProfile.Major).Distinct().ToList();
                List<string> checkinMajorData = new List<string>();
                foreach (string major in checkinMajorLabels)
                    checkinMajorData.Add(checkins.Count(c => c.User.UserProfile.Major.Equals(major)).ToString());
                List<string> checkinSpecializationLabels = checkins.Select(c => c.User.UserProfile.Specialization).Distinct().ToList();
                List<string> checkinSpecializationData = new List<string>();
                foreach (string specialization in checkinSpecializationLabels)
                    checkinSpecializationData.Add(checkins.Count(c => c.User.UserProfile.Specialization.Equals(specialization)).ToString());
                ViewBag.Checkin = checkins.Count();
                ViewBag.CheckinDateLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinDateLabels));
                ViewBag.CheckinDateData = string.Format("[{0}]", string.Join(", ", checkinDateData));
                ViewBag.CheckinGenderData = string.Format("[{0}, {1}]", checkinMale, checkinFemale);
                ViewBag.CheckinCityLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinCityLabels));
                ViewBag.CheckinCityData = string.Format("[{0}]", string.Join(", ", checkinCityData));
                ViewBag.CheckinCampusLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinCampusLabels));
                ViewBag.CheckinCampusData = string.Format("[{0}]", string.Join(", ", checkinCampusData));
                ViewBag.CheckinMajorLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinMajorLabels));
                ViewBag.CheckinMajorData = string.Format("[{0}]", string.Join(", ", checkinMajorData));
                ViewBag.CheckinSpecializationLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinSpecializationLabels));
                ViewBag.CheckinSpecializationData = string.Format("[{0}]", string.Join(", ", checkinSpecializationData));
            }
            else
            {
                ViewBag.Checkin = null;
                ViewBag.CheckinDateLabels = "[]";
                ViewBag.CheckinDateData = "[]";
                ViewBag.CheckinGenderData = "[]";
                ViewBag.CheckinCityLabels = "[]";
                ViewBag.CheckinCityData = "[]";
                ViewBag.CheckinCampusLabels = "[]";
                ViewBag.CheckinCampusData = "[]";
                ViewBag.CheckinMajorLabels = "[]";
                ViewBag.CheckinMajorData = "[]";
                ViewBag.CheckinSpecializationLabels = "[]";
                ViewBag.CheckinSpecializationData = "[]";
            }
            ViewBag.Event = eventt;
            ViewBag.Register = registers.Count();
            ViewBag.RegisterMax = eventt.RegisterMax;
            ViewBag.Rating = eventt.GetRating();
            ViewBag.Feedback = eventt.Feedbacks.Count;
            ViewBag.RegisterDateLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerDateLabels));
            ViewBag.RegisterDateData = string.Format("[{0}]", string.Join(", ", registerDateData));
            ViewBag.RegisterGenderData = string.Format("[{0}, {1}]", registerMale, registerFemale);
            ViewBag.RegisterCityLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerCityLabels));
            ViewBag.RegisterCityData = string.Format("[{0}]", string.Join(", ", registerCityData));
            ViewBag.RegisterCampusLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerCampusLabels));
            ViewBag.RegisterCampusData = string.Format("[{0}]", string.Join(", ", registerCampusData));
            ViewBag.RegisterMajorLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerMajorLabels));
            ViewBag.RegisterMajorData = string.Format("[{0}]", string.Join(", ", registerMajorData));
            ViewBag.RegisterSpecializationLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerSpecializationLabels));
            ViewBag.RegisterSpecializationData = string.Format("[{0}]", string.Join(", ", registerSpecializationData));
            if (eventt.Forms.Count > 0)
                ViewBag.FormResult = eventt.Forms.First().FormResult;
            return View();
        }

        public ActionResult ViewYourRegisteredStudents(int id, int? page, UserSearchViewModel model)
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventId == id).SingleOrDefault();
            if (eventt == null || !PUBLIC_STATUSES.Contains(eventt.EventStatus))
                return View("NotFound");
            model.Search = model.Search.ToUnsignString();
            IEnumerable<Register> dbregisters = db.Registers.Where(delegate (Register r)
            {
                return r.EventId == id && r.User.UserProfile != null && (string.IsNullOrEmpty(model.Search) || r.User.Email.ContainsIgnoreCase(model.Search) || string.Join(" ", r.User.UserProfile.FirstName.ToUnsignString(), r.User.UserProfile.LastName.ToUnsignString()).ContainsIgnoreCase(model.Search) || string.Join(" ", r.User.UserProfile.LastName.ToUnsignString(), r.User.UserProfile.FirstName.ToUnsignString()).ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel
            {
                Action = "ViewYourRegisteredStudents",
                Size = VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE,
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbregisters.Count() / VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE)
            };
            List<RegisterModel> registers = new List<RegisterModel>();
            foreach (Register register in dbregisters.OrderByDescending(r => r.User.Email).Skip((pages.Current - 1) * VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE).Take(VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE))
                registers.Add(new RegisterModel()
                {
                    Id = register.UserId,
                    Name = string.Join(" ", register.User.UserProfile.LastName, register.User.UserProfile.FirstName),
                    Email = register.User.Email,
                    RegisteredDate = register.RegisteredDate
                });
            ViewBag.EventId = id;
            ViewBag.Registers = registers;
            ViewBag.Pages = pages;
            return View(model);
        }

        public ActionResult ViewYourCheckedinStudents(int id, int? page, UserSearchViewModel model)
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventId == id).SingleOrDefault();
            if (eventt == null || (!AFTER_STATUSES.Contains(eventt.EventStatus)))
                return View("NotFound");
            model.Search = model.Search.ToUnsignString();
            IEnumerable<Checkin> dbcheckins = db.Checkins.Where(delegate (Checkin c)
            {
                return c.EventId == id && c.User.UserProfile != null && (string.IsNullOrEmpty(model.Search) || c.User.Email.ContainsIgnoreCase(model.Search) || string.Join(" ", c.User.UserProfile.FirstName.ToUnsignString(), c.User.UserProfile.LastName.ToUnsignString()).ContainsIgnoreCase(model.Search) || string.Join(" ", c.User.UserProfile.LastName.ToUnsignString(), c.User.UserProfile.FirstName.ToUnsignString()).ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel
            {
                Action = "ViewYourCheckedinStudents",
                Size = VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE,
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbcheckins.Count() / VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE)
            };
            List<CheckinModel> checkins = new List<CheckinModel>();
            foreach (Checkin checkin in dbcheckins.OrderByDescending(c => c.User.Email).Skip((pages.Current - 1) * VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE).Take(VIEW_YOUR_REGISTERED_STUDENTS_PAGE_SIZE))
                checkins.Add(new CheckinModel()
                {
                    Id = checkin.UserId,
                    Name = string.Join(" ", checkin.User.UserProfile.LastName, checkin.User.UserProfile.FirstName),
                    Email = checkin.User.Email,
                    CheckedinDate = checkin.CheckedinDate
                });
            ViewBag.EventId = id;
            ViewBag.Checkins = checkins;
            ViewBag.Pages = pages;
            return View(model);
        }

        public async Task<ActionResult> ExportYourEventStatistics(int id)
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventId == id).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            IEnumerable<Register> dbregisters = eventt.Registers.Where(r => r.User.UserProfile != null);
            IEnumerable<Checkin> dbcheckins = eventt.Checkins.Where(c => c.User.UserProfile != null);
            IEnumerable<Feedback> dbfeedbacks = eventt.Feedbacks.Where(f => f.User.UserProfile != null);
            IEnumerable<FeedbackOuter> dbfeedbackouters = eventt.FeedbackOuters;
            var info = new
            {
                name = eventt.EventName,
                registers = dbregisters.Count(),
                checkins = dbcheckins.Count(),
                feedbacks = dbfeedbacks.Count() + dbfeedbackouters.Count(),
                rating = eventt.GetRating()
            };
            var registers = dbregisters.Select(r => new { r.User.Email, r.User.UserProfile.FirstName, r.User.UserProfile.LastName, RegisteredDate = r.RegisteredDate.ToDateTimeString() }).ToList();
            var checkins = dbcheckins.Select(c => new { c.User.Email, c.User.UserProfile.FirstName, c.User.UserProfile.LastName, CheckedinDate = c.CheckedinDate.ToDateTimeString() }).ToList();
            var feedbacks = dbfeedbacks.Select(f => new { f.User.Email, f.User.UserProfile.FirstName, f.User.UserProfile.LastName, f.Value, f.FeedbackContent, CreatedDate = f.CreatedDate.ToDateTimeString() }).ToList();
            var feedbackOuter = dbfeedbackouters.Select(fo => new { fo.UserName, fo.Value, fo.FeedbackContent, CreatedDate = fo.CreatedDate.ToDateTimeString() }).ToList();
            string link = await AppScriptHelper.ExportEventStatistics(User.Identity.Name, new
            {
                info,
                registers,
                checkins,
                feedbacks,
                feedbackOuter,
                form = eventt.Forms.Count > 0 ? eventt.Forms.Single().FormLink : null
            });
            return Json(link, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEvent()
        {
            ViewBag.Categories = db.Categories.Where(c => c.IsEnabled).ToList();
            ViewBag.currCategories = new List<Category>();
            ViewBag.currSmartTv = new List<SmartTV>();
            ViewBag.Groups = db.Groups.Where(g => g.IsEnabled == true).ToList();
            ViewBag.currGroups = new List<Group>();
            string currFamily = "";
            ViewBag.currFamily = currFamily;
            ViewBag.currUserGroup = User.GetUser().GetGroups();
            ViewBag.selectedGroupId = 0;
            ViewBag.Campuses = db.Campuses.ToList();
            ViewBag.Majors = ApiHelper.GetMajors();
            ViewBag.currStaff = new List<EventRole>();
            ViewBag.Users = db.UserProfiles.ToList();
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEvent(CreateEventViewModel model, string submit)
        {
            string participants = "";
            if (model.Participants != null)
            {
                foreach (string major in model.Participants)
                    participants += major + ", ";
            }
            if (model.CloseDate < model.OpenDate)
                ModelState.AddModelError("CloseDate", "Event close date must be later than Event Open date.");
            if (model.CloseDate == model.OpenDate && model.CloseTime < model.OpenTime)
                ModelState.AddModelError("CloseTime", "Event close time must be later than Event Open time.");
            if (model.Image == null && model.CoverImage == null)
                ModelState.AddModelError("Image", "Must upload an image");
            if (model.RegisterEndDate > model.OpenDate || ((model.RegisterEndDate == model.OpenDate) && (model.RegisterEndTime > model.OpenTime)))
                ModelState.AddModelError("RegisterEndDate", "Event register close time must be sooner than Event open time.");
            if (model.RegisterEndDate != null && model.RegisterEndTime == null)
                ModelState.AddModelError("RegisterEndDate", "Register close time must be not null.");
            if (model.RegisterEndDate == null && model.RegisterEndTime != null)
                ModelState.AddModelError("RegisterEndDate", "Register close date must be not null.");
            if (model.Image != null && (!ModelState.ContainsKey("Image") || ModelState["Image"].Errors.Count == 0))
            {
                string image = model.Image.SaveImage("~/Images/Events/");
                model.CoverImage = "/Images/Events/" + image;
            }
            if (!ModelState.IsValid)
            {
                List<Category> currCategories = new List<Category>();
                if (model.Category != null)
                    foreach (int cId in model.Category)
                        currCategories.Add(db.Categories.Where(c => c.CategoryId == cId && c.IsEnabled).SingleOrDefault());
                ViewBag.currCategories = currCategories;
                List<Category> categories = db.Categories.Where(c => c.IsEnabled).ToList();
                foreach (Category category in currCategories)
                    categories.Remove(category);
                ViewBag.Categories = categories;
                ViewBag.currFamily = model.EventFamily;
                if (ViewBag.currFamily == null)
                    ViewBag.currFamily = string.Empty;
                ViewBag.currUserGroup = User.GetUser().GetGroups();
                ViewBag.Campuses = db.Campuses.ToList();
                ViewBag.selectedGroupId = model.GroupId;
                ViewBag.CurrentImage = model.CoverImage;
                List<Group> groups = db.Groups.Where(g => g.IsEnabled == true).ToList();
                ViewBag.Groups = db.Groups.Where(g => g.IsEnabled == true).ToList();
                if (model.TargetGroup != null)
                {
                    foreach (int gid in model.TargetGroup)
                    {
                        groups.Remove(db.Groups.Where(g => g.GroupId == gid).SingleOrDefault());
                    }
                    List<Group> currGroups = new List<Group>();
                    foreach (int gid in model.TargetGroup)
                        currGroups.Add(db.Groups.Where(g => g.GroupId == gid && g.IsEnabled).SingleOrDefault());
                    ViewBag.currGroups = currGroups;
                    ViewBag.Groups = groups;
                }
                ViewBag.Majors = ApiHelper.GetMajors();
                ViewBag.currMajors = participants;
                return View(model);
            }
            Event eventt = db.Events.Create();
            eventt.EventName = model.Name;
            eventt.CoverImage = model.CoverImage;
            eventt.IsPublic = model.Public;
            eventt.IsOrganizerOnly = model.OrganizerOnly;
            eventt.RegisterMax = model.RegisterNumber;
            eventt.CampusId = model.Campus;
            eventt.UrlLink = model.UrlLink;
            string targetGroup = "";
            if (model.TargetGroup != null)
            {
                foreach (int gid in model.TargetGroup)
                {
                    targetGroup += "$" + gid + "^, ";
                }
                eventt.TargetGroup = targetGroup;
            }
            if (string.IsNullOrEmpty(model.OtherParticipants))
                participants += "; " + "" + ", ";
            else
                participants += "; " + model.OtherParticipants.Trim() + ", ";
            eventt.Participants = participants;
            eventt.EventDescription = model.Description;
            eventt.EventDescriptionHtml = model.DescriptionHtml;
            eventt.OpenDate = CalendarHelper.GetDateTimeFromDateAndTime(model.OpenDate, model.OpenTime);
            eventt.CloseDate = CalendarHelper.GetDateTimeFromDateAndTime(model.CloseDate, model.CloseTime);
            if (model.RegisterEndDate == null || model.RegisterEndTime == null)
                eventt.RegisterEndDate = CalendarHelper.GetDateTimeFromDateAndTime(model.OpenDate, model.OpenTime);
            else
                eventt.RegisterEndDate = CalendarHelper.GetDateTimeFromDateAndTime(model.RegisterEndDate.Value, model.RegisterEndTime.Value);
            if (model.FormLink != null)
            {
                Form formCreate = db.Forms.Create();
                formCreate.FormLink = model.FormLink;
                formCreate.FormRegister = model.FormRegister;
                formCreate.FormResult = model.FormResult;
                formCreate.CreatedDate = DateTime.Now;
                formCreate.Event = eventt;
                db.Forms.Add(formCreate);
                eventt.Forms.Add(formCreate);
            }
            eventt.EventPlace = model.Place;
            eventt.EventFee = model.Fee;
            if (submit.Equals("Save draft"))
            {
                eventt.EventStatus = EventStatusConstants.DRAFT;
                this.SetMessage("Successfully save draft.");
            }
            else
            {
                eventt.EventStatus = EventStatusConstants.PENDING;
                this.SetMessage("Successfully create event. Please wait for approval.");
            }
            eventt.OrganizerId = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).SingleOrDefault().UserId;
            eventt.GroupId = model.GroupId;
            if (eventt.GroupId == 0)
                eventt.GroupId = null;
            eventt.CreatedDate = DateTime.Now;
            eventt.IsFeatured = false;
            foreach (int cId in model.Category)
                eventt.Categories.Add(db.Categories.Where(c => c.CategoryId == cId && c.IsEnabled).SingleOrDefault());

            if (model.PublicTv == true)
            {
                foreach (var tv in db.SmartTVs.ToList())
                {
                    eventt.SmartTVs.Add(tv);
                }
            }
            else
            {
                List<int> tvId = model.SmartTv.Distinct().ToList();
                foreach (int tId in tvId)
                    eventt.SmartTVs.Add(db.SmartTVs.Where(s => s.TvId == tId).SingleOrDefault());
            }

            foreach (int uId in model.Staff)
            {
                db.EventRoles.Add(new EventRole
                {
                    EventId = eventt.EventId,
                    UserId = uId,
                    Role = "Người soát vé"
                });
            }
            eventt.StartDateVideo = CalendarHelper.GetDateTimeFromDateAndTime(model.StartDateVideo, model.StartTimeVideo);
            eventt.StartDateBanner = CalendarHelper.GetDateTimeFromDateAndTime(model.StartDateBanner, model.StartDateBanner);
            eventt.EndDateVidep = CalendarHelper.GetDateTimeFromDateAndTime(model.EndDateVideo, model.EndTimeVideo);
            eventt.EndDateBanner = CalendarHelper.GetDateTimeFromDateAndTime(model.EndDateBanner, model.EndTimeBanner);
            if (model.EventFamily != null)
            {
                EventFamily family = null;
                if (!db.EventFamilies.Any(e => e.FamilyName.Equals(model.EventFamily)))
                {
                    family = db.EventFamilies.Create();
                    family.FamilyName = model.EventFamily;
                    db.EventFamilies.Add(family);
                }
                else
                    family = db.EventFamilies.Single(e => e.FamilyName.Equals(model.EventFamily));
                eventt.EventFamily = family;
            }
            else
                eventt.EventFamily = null;
            db.Events.Add(eventt);
            db.SaveChanges();
            return RedirectToAction("ViewEvent", "Home", new { id = eventt.EventId });
        }

        [HttpPost]
        public async Task<ActionResult> CreateForm()
        {
            return Json(await AppScriptHelper.CreateForm(User.Identity.Name));
        }

        public ActionResult ViewYourDraftEvents(int? page)
        {
            int currentUserId = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single().UserId;
            IEnumerable<Event> dbevents = db.Events.Where(e => e.EventStatus.Equals(EventStatusConstants.DRAFT) && e.OrganizerId == currentUserId);
            PageModel pages = new PageModel
            {
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbevents.Count() / VIEW_YOUR_DRAFT_EVENTS_PAGE_SIZE)
            };
            List<EventModel> drafts = new List<EventModel>();
            foreach (Event eventt in dbevents.OrderBy(e => e.CreatedDate).Skip((pages.Current - 1) * VIEW_YOUR_DRAFT_EVENTS_PAGE_SIZE).Take(VIEW_YOUR_DRAFT_EVENTS_PAGE_SIZE))
                drafts.Add(new EventModel()
                {
                    Id = eventt.EventId,
                    Name = eventt.EventName,
                    CreatedDate = eventt.CreatedDate
                });
            ViewBag.Drafts = drafts;
            ViewBag.Pages = pages;
            return View();
        }

        public ActionResult EditDraftEvent(string id)
        {
            if (id == null)
                return RedirectToAction("ViewYourDraftEvents", "Organizer");
            int eventId = int.Parse(id);
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventStatus.Equals(EventStatusConstants.DRAFT) && e.EventId == eventId).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            CreateEventViewModel draft = new CreateEventViewModel()
            {
                EventId = eventt.EventId,
                OpenDate = eventt.OpenDate,
                CloseDate = eventt.CloseDate,
                OpenTime = eventt.OpenDate,
                CloseTime = eventt.CloseDate,
                RegisterNumber = eventt.RegisterMax,
                RegisterEndDate = eventt.RegisterEndDate,
                RegisterEndTime = eventt.RegisterEndDate,
                Description = eventt.EventDescription,
                DescriptionHtml = eventt.EventDescriptionHtml,
                Fee = eventt.EventFee,
                Name = eventt.EventName,
                Place = eventt.EventPlace,
                Public = eventt.IsPublic,
                OtherParticipants = eventt.GetOtherParticipants(),
                OrganizerOnly = eventt.IsOrganizerOnly,
                FormLink = eventt.Forms.Count == 0 ? null : eventt.Forms.First().FormLink,
                FormRegister = eventt.Forms.Count == 0 ? null : eventt.Forms.First().FormRegister,
                FormResult = eventt.Forms.Count == 0 ? null : eventt.Forms.First().FormResult,
                Category = new List<int>(),
            };
            ViewBag.currMajors = eventt.GetParticipants();
            ViewBag.Majors = ApiHelper.GetMajors();
            if (eventt.EventFamily != null)
                draft.EventFamily = eventt.EventFamily.FamilyName;
            if (eventt.GroupId == null)
                draft.GroupId = 0;
            else
                draft.GroupId = (int)eventt.GroupId;
            foreach (Category c in eventt.Categories.Where(c => c.IsEnabled))
                draft.Category.Add(c.CategoryId);
            List<Category> currCategories = new List<Category>();
            if (draft.Category != null)
                foreach (int cId in draft.Category)
                    currCategories.Add(db.Categories.Where(c => c.CategoryId == cId && c.IsEnabled).Single());
            ViewBag.currCategories = currCategories;
            List<Category> categories = db.Categories.Where(c => c.IsEnabled).ToList();
            foreach (Category category in currCategories)
                categories.Remove(category);
            ViewBag.Categories = categories;
            ViewBag.currFamily = draft.EventFamily;
            if (ViewBag.currFamily == null)
                ViewBag.currFamily = string.Empty;
            ViewBag.currUserGroup = User.GetUser().GetGroups();
            ViewBag.Campuses = db.Campuses.ToList();
            ViewBag.selectedGroupId = draft.GroupId;
            ViewBag.CurrentImage = eventt.CoverImage;
            List<Group> groups = db.Groups.Where(g => g.IsEnabled == true).ToList();
            List<Group> currGroups = new List<Group>();
            if (eventt.TargetGroup != null)
            {
                string[] tmpGroups = eventt.TargetGroup.Split(new[] { ", " }, StringSplitOptions.None);
                foreach (string group in tmpGroups)
                {
                    if (group != "")
                    {
                        int gid = int.Parse(System.Text.RegularExpressions.Regex.Match(group, @"\d+").Value);
                        currGroups.Add(db.Groups.Where(g => g.GroupId == gid && g.IsEnabled).SingleOrDefault());
                    }
                }
            }
            foreach (Group group in currGroups)
                groups.Remove(group);
            ViewBag.Groups = groups;
            ViewBag.currGroups = currGroups;
            return View(draft);
        }

        [HttpPost]
        public ActionResult DeleteDraftEvent(int id)
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventId == id && e.EventStatus.Equals(EventStatusConstants.DRAFT)).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            eventt.EventStatus = EventStatusConstants.DELETED;
            db.SaveChanges();
            this.SetMessage("Successfully deleted event");
            return RedirectToAction("ViewYourDraftEvents");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditDraftEvent(CreateEventViewModel model, string submit)
        {
            string participants = "";
            if (model.Participants != null)
            {
                foreach (string major in model.Participants)
                    participants += major + ", ";
            }
            if (model.CloseDate < model.OpenDate)
                ModelState.AddModelError("CloseDate", "Event close date must be later than Event open date.");
            if (model.CloseDate == model.OpenDate && model.CloseTime < model.OpenTime)
                ModelState.AddModelError("CloseTime", "Event close time must be later than Event open time.");
            if (model.Image == null && model.CoverImage == null)
                ModelState.AddModelError("Image", "Must upload an image");
            if (model.RegisterEndDate > model.OpenDate || ((model.RegisterEndDate == model.OpenDate) && (model.RegisterEndTime > model.OpenTime)))
                ModelState.AddModelError("RegisterEndDate", "Event register close time must be sooner than Event open time.");
            if (model.RegisterEndDate != null && model.RegisterEndTime == null)
                ModelState.AddModelError("RegisterEndDate", "Register close time must be not null.");
            if (model.RegisterEndDate == null && model.RegisterEndTime != null)
                ModelState.AddModelError("RegisterEndDate", "Register close date must be not null.");
            if (model.Image != null && (!ModelState.ContainsKey("Image") || ModelState["Image"].Errors.Count == 0))
            {
                string image = model.Image.SaveImage("~/Images/Events/");
                model.CoverImage = "/Images/Events/" + image;
            }
            if (!ModelState.IsValid)
            {
                List<Category> currCategories = new List<Category>();
                if (model.Category != null)
                    foreach (int cId in model.Category)
                        currCategories.Add(db.Categories.Where(c => c.CategoryId == cId && c.IsEnabled).Single());
                ViewBag.currCategories = currCategories;
                List<Category> categories = db.Categories.Where(c => c.IsEnabled).ToList();
                foreach (Category category in currCategories)
                    categories.Remove(category);
                ViewBag.Categories = categories;
                ViewBag.currFamily = model.EventFamily;
                if (ViewBag.currFamily == null)
                    ViewBag.currFamily = string.Empty;
                ViewBag.currUserGroup = User.GetUser().GetGroups();
                ViewBag.selectedGroupId = model.GroupId;
                ViewBag.CurrentImage = model.CoverImage;
                List<Group> groups = db.Groups.Where(g => g.IsEnabled == true).ToList();
                ViewBag.Groups = db.Groups.Where(g => g.IsEnabled == true).ToList();
                if (model.TargetGroup != null)
                {
                    foreach (int gid in model.TargetGroup)
                    {
                        groups.Remove(db.Groups.Where(g => g.GroupId == gid).SingleOrDefault());
                    }
                    List<Group> currGroups = new List<Group>();
                    foreach (int gid in model.TargetGroup)
                        currGroups.Add(db.Groups.Where(g => g.GroupId == gid && g.IsEnabled).SingleOrDefault());
                    ViewBag.currGroups = currGroups;
                    ViewBag.Groups = groups;
                }
                ViewBag.Majors = ApiHelper.GetMajors();
                ViewBag.currMajors = participants;
                return View(model);
            }
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventStatus.Equals(EventStatusConstants.DRAFT) && e.EventId == model.EventId).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            if (eventt != null)
            {
                eventt.CreatedDate = DateTime.Now;
                eventt.CoverImage = model.CoverImage;
                eventt.EventName = model.Name;
                eventt.OpenDate = CalendarHelper.GetDateTimeFromDateAndTime(model.OpenDate, model.OpenTime);
                eventt.CloseDate = CalendarHelper.GetDateTimeFromDateAndTime(model.CloseDate, model.CloseTime);
                if (model.RegisterEndDate == null || model.RegisterEndTime == null)
                    eventt.RegisterEndDate = CalendarHelper.GetDateTimeFromDateAndTime(model.OpenDate, model.OpenTime);
                else
                    eventt.RegisterEndDate = CalendarHelper.GetDateTimeFromDateAndTime(model.RegisterEndDate.Value, model.RegisterEndTime.Value);
                eventt.IsPublic = model.Public;
                eventt.IsOrganizerOnly = model.OrganizerOnly;
                if (string.IsNullOrEmpty(model.OtherParticipants))
                    participants += "; " + "" + ", ";
                else
                    participants += "; " + model.OtherParticipants.Trim() + ", ";
                eventt.Participants = participants;
                eventt.EventDescription = model.Description;
                eventt.EventDescriptionHtml = model.DescriptionHtml;
                eventt.EventPlace = model.Place;
                eventt.EventFee = model.Fee;
                eventt.RegisterMax = model.RegisterNumber;
                if (submit.Equals("Save draft"))
                {
                    eventt.EventStatus = EventStatusConstants.DRAFT;
                    this.SetMessage("Successfully edit draft.");
                }
                else
                {
                    eventt.EventStatus = EventStatusConstants.PENDING;
                    this.SetMessage("Successfully create event. Please wait for approval.");
                }
                if (model.FormLink != null)
                {
                    Form form = db.Forms.Create();
                    form.FormLink = model.FormLink;
                    form.FormRegister = model.FormRegister;
                    form.FormResult = model.FormResult;
                    form.CreatedDate = DateTime.Now;
                    form.Event = eventt;
                    db.Forms.RemoveRange(eventt.Forms);
                    db.Forms.Add(form);
                }
                foreach (int cId in model.Category)
                    eventt.Categories.Add(db.Categories.Where(c => c.CategoryId == cId && c.IsEnabled).Single());
                if (model.EventFamily != null)
                {
                    EventFamily family = null;
                    if (!db.EventFamilies.Any(e => e.FamilyName.Equals(model.EventFamily)))
                    {
                        family = db.EventFamilies.Create();
                        family.FamilyName = model.EventFamily;
                        db.EventFamilies.Add(family);
                    }
                    else
                        family = db.EventFamilies.Single(e => e.FamilyName.Equals(model.EventFamily));
                    eventt.EventFamily = family;
                }
                else
                    eventt.EventFamily = null;
                eventt.GroupId = model.GroupId;
                if (eventt.GroupId == 0)
                    eventt.GroupId = null;
                string targetGroup = "";
                if (model.TargetGroup != null)
                    foreach (int gid in model.TargetGroup)
                    {
                        targetGroup += "$" + gid + "^, ";
                    }
                eventt.TargetGroup = targetGroup;
            }
            db.SaveChanges();
            return RedirectToAction("ViewEvent", "Home", new { id = model.EventId });
        }

        public ActionResult ViewYourPendingEvents(int? page)
        {
            int currentUserId = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single().UserId;
            IEnumerable<Event> dbevents = db.Events.Where(u => (u.EventStatus.Equals(EventStatusConstants.PENDING)
                || u.EventStatus.Equals(EventStatusConstants.REJECTED))
                && u.OrganizerId == currentUserId);
            PageModel pages = new PageModel
            {
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbevents.Count() / VIEW_YOUR_PENDING_EVENTS_PAGE_SIZE)
            };
            User user = db.Users.SingleOrDefault(u => u.Email.Equals(User.Identity.Name));
            int userId = user.UserId;
            List<EventModel> events = new List<EventModel>();
            foreach (Event eventt in dbevents
                    .OrderBy(e => e.CreatedDate)
                    .Skip((pages.Current - 1) * VIEW_YOUR_PENDING_EVENTS_PAGE_SIZE)
                    .Take(VIEW_YOUR_PENDING_EVENTS_PAGE_SIZE))
            {
                Group group = db.Groups.Where(g => g.GroupId == eventt.GroupId && g.IsEnabled).SingleOrDefault();
                events.Add(new EventModel()
                {
                    Id = eventt.EventId,
                    Name = eventt.EventName,
                    Status = eventt.EventStatus,
                    Group = group != null ? new GroupModel()
                    {
                        Id = group.GroupId,
                        Name = group.GroupName
                    } : null,
                    CreatedDate = eventt.CreatedDate
                });
            }
            ViewBag.Events = events;
            ViewBag.Pages = pages;
            return View(events);
        }

        public ActionResult CancelEvent(int id)
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventId == id).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            eventt.EventStatus = EventStatusConstants.DRAFT;
            eventt.CreatedDate = DateTime.Now;
            db.SaveChanges();
            this.SetMessage("Successfully canceled request");
            return RedirectToAction("ViewYourDraftEvents");
        }

        public ActionResult MoveToDraft(int id)
        {
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            Event eventt = db.Events.Where(e => e.OrganizerId == user.UserId && e.EventId == id).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            eventt.EventStatus = EventStatusConstants.DRAFT;
            eventt.CreatedDate = DateTime.Now;
            db.SaveChanges();
            this.SetMessage("Successfully moved event to draft");
            return RedirectToAction("ViewYourDraftEvents");
        }


        [HttpPost]
        public JsonResult GetBuildingByCampusId(int campId)
        {
            List<BuildingModel> buildings = new List<BuildingModel>();
            foreach (Building b in new Database().Buildings)
            {
                if (b.CampusId == campId)
                {
                    buildings.Add(new BuildingModel()
                    {
                        BuildingId = b.BuildingId,
                        BuildingName = b.BuildingName
                    });
                }
            }
            return Json(buildings, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSmartTvByBuilingId(int buildId)
        {
            List<SmartTvModel> smartTvs = new List<SmartTvModel>();
            foreach (SmartTV tv in new Database().SmartTVs)
            {
                if (tv.BuildingId == buildId)
                {
                    smartTvs.Add(new SmartTvModel()
                    {
                        TvId = tv.TvId,
                        TvName = tv.TvName
                    });
                }
            }
            return Json(smartTvs, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetUserProfiles()
        {
            List<StaffModel> staffs = new List<StaffModel>();
            foreach (UserProfile up in new Database().UserProfiles)
            {
                staffs.Add(new StaffModel
                {
                    UserId = up.ProfileId,
                    UserName = up.FirstName + " " + up.LastName
                });
            }
            return Json(staffs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SearchFamily(string searchStr)
        {
            List<FamilyModel> model = new List<FamilyModel>();
            foreach (EventFamily e in new Database().EventFamilies)
            {
                if (e.FamilyName.ToUnsignString().ContainsIgnoreCase(searchStr.ToUnsignString()))
                {
                    model.Add(new FamilyModel()
                    {
                        FamilyId = e.FamilyId,
                        FamilyName = e.FamilyName
                    });
                }
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ManageEventCheckins(int id)
        {
            User user = User.GetUser();
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null || eventt.OrganizerId != user.UserId || (!eventt.EventStatus.Equals(EventStatusConstants.HAPPENING) && (!eventt.EventStatus.Equals(EventStatusConstants.OPENING) || eventt.OpenDate > DateTime.Now.AddHours(1))))
                return View("NotFound");
            List<RegisterCheckinModel> registerCheckins = new List<RegisterCheckinModel>();
            foreach (Register register in db.Registers.Where(r => r.EventId == id && r.User.UserProfile != null))
                registerCheckins.Add(new RegisterCheckinModel()
                {
                    Id = register.User.UserId,
                    Email = register.User.Email,
                    Name = string.Join(" ", register.User.UserProfile.LastName, register.User.UserProfile.FirstName),
                    CheckedIn = db.Checkins.Count(c => c.EventId == id && c.UserId == register.User.UserId) > 0
                });
            ViewBag.EventId = id;
            ViewBag.RegisterCheckins = registerCheckins;
            return View();
        }

        public ActionResult CheckinEvent(int id, int studentId)
        {
            User user = User.GetUser();
            User student = db.Users.Where(u => u.UserId == studentId).SingleOrDefault();
            if (student == null || !student.IsStudent)
                return View("Error");
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null || eventt.OrganizerId != user.UserId || !eventt.EventStatus.Equals(EventStatusConstants.HAPPENING) || db.Checkins.Count(c => c.EventId == id && c.UserId == studentId) > 0)
                return View("NotFound");
            Checkin checkin = db.Checkins.Create();
            checkin.User = student;
            checkin.Event = eventt;
            checkin.CheckedinDate = DateTime.Now;
            db.Checkins.Add(checkin);
            db.SaveChanges();
            return PartialView();
        }

        public ActionResult EditGroup(int id)
        {
            Group group = db.Groups.SingleOrDefault(x => x.GroupId == id);
            if (!group.IsEnabled)
                return RedirectToAction("ViewAllGroups", "Home");
            CreateGroupModel model = new CreateGroupModel()
            {
                Id = group.GroupId,
                Name = group.GroupName,
                GroupImage = group.GroupImage,
                Description = group.GroupDescription,
                Leader = group.Leader.UserProfile.LastName + " " + group.Leader.UserProfile.FirstName + " (" + group.Leader.Email + ")",
                Mail = group.GroupMail,
                FoundedDate = group.FoundedYear
            };
            ViewBag.CurrentImage = group.GroupImage;
            return View(model);
        }

        [HttpPost]
        public ActionResult EditGroup(CreateGroupModel model)
        {
            if (model.FoundedDate.HasValue && (model.FoundedDate.Value < System.Data.SqlTypes.SqlDateTime.MinValue.Value || model.FoundedDate.Value > System.Data.SqlTypes.SqlDateTime.MaxValue.Value))
                ModelState.AddModelError("FoundedDate", "Founded date must be from " + System.Data.SqlTypes.SqlDateTime.MinValue.Value.ToString("MM/dd/yyyy") + " to " + System.Data.SqlTypes.SqlDateTime.MaxValue.Value.ToString("MM/dd/yyyy"));
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (model.Image != null && (!ModelState.ContainsKey("Image") || ModelState["Image"].Errors.Count == 0))
            {
                string image = model.Image.SaveImage("~/Images/Groups/");
                model.GroupImage = "/Images/Groups/" + image;
            }
            if (!ModelState.IsValid)
            {
                ViewBag.CurrentImage = model.GroupImage;
                return View(model);
            }
            Group group = db.Groups.Single(g => g.GroupId == model.Id);
            group.GroupDescription = model.Description;
            group.GroupMail = model.Mail;
            group.GroupImage = model.GroupImage;
            group.FoundedYear = model.FoundedDate;
            db.SaveChanges();
            this.SetMessage("Successfully edited group.");
            return RedirectToAction("ViewGroup/" + group.GroupId, "Home");
        }

        [HttpPost]
        public JsonResult RemoveMember(int groupId, int userId)
        {
            User user = db.Users.SingleOrDefault(u => u.UserId == userId);
            db.UserGroups.Remove(db.UserGroups.Where(ug => ug.OrganizerId == userId && ug.GroupId == groupId).Single());
            db.SaveChanges();
            user.AddNotification(null, groupId, ObjectTypeConstants.GROUP, NotificationContentConstants.USER_GROUP_REMOVE);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult AddMember(int groupId, string userEmail)
        {
            User user = db.Users.SingleOrDefault(u => u.Email.ToLower().Equals(userEmail.Trim().ToLower()));
            if (user == null)
                return Json(404, JsonRequestBehavior.AllowGet);
            if (db.UserGroups.SingleOrDefault(ug => ug.GroupId == groupId && ug.OrganizerId == user.UserId) != null)
                return Json(405, JsonRequestBehavior.AllowGet);
            else
            {
                UserGroup newMember = new UserGroup()
                {
                    GroupId = groupId,
                    OrganizerId = user.UserId,
                    ParticipatedDate = DateTime.Now
                };
                db.UserGroups.Add(newMember);
                db.SaveChanges();
                User addedUser = db.Users.Where(u => u.UserId == user.UserId).Single();
                MemberModel model;
                if (addedUser.UserProfile == null)
                {
                    model = new MemberModel()
                    {
                        Id = addedUser.UserId,
                        Name = "",
                        strJoinedDate = DateTime.Now.ToDateTimeString(),
                        Email = addedUser.Email,
                        Role = (db.Users.Single(u => u.UserId == addedUser.UserId).IsOrganizer) ? "Organizer" : "Member",
                    };
                }
                else
                {
                    string tmpName = addedUser.UserProfile.LastName ?? "";
                    tmpName += " ";
                    tmpName += addedUser.UserProfile.FirstName ?? "";
                    model = new MemberModel()
                    {
                        Id = addedUser.UserId,
                        Name = tmpName,
                        Ticks = DateTime.Now.Ticks.ToString(),
                        strJoinedDate = DateTime.Now.ToDateTimeString(),
                        Email = addedUser.Email,
                        Role = (db.Users.Single(u => u.UserId == addedUser.UserId).IsOrganizer) ? "Organizer" : "Member"
                    };
                }
                user.AddNotification(null, groupId, ObjectTypeConstants.GROUP, NotificationContentConstants.USER_GROUP_ADD);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SearchOrganizersNotMember(string searchStr, int groupId)
        {
            List<User> organizers = db.Users.Where(u => u.Email.ToUpper().Contains(searchStr.ToUpper())).ToList();
            Group grp = db.Groups.SingleOrDefault(x => x.GroupId == groupId);
            List<UserGroup> userGroupList = db.UserGroups.Where(u => u.GroupId == grp.GroupId).ToList();
            List<int> groupMemberIdList = new List<int> {
                grp.LeaderId
            };
            foreach (UserGroup item in userGroupList)
                groupMemberIdList.Add(item.OrganizerId);
            List<SearchOrganizersModel> model = new List<SearchOrganizersModel>();
            foreach (User u in organizers)
                if (!groupMemberIdList.Contains(u.UserId))
                    model.Add(new SearchOrganizersModel()
                    {
                        Id = u.UserId,
                        Email = u.Email
                    });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private Database db;

        public OrganizerController()
        {
            db = new Database();
        }

        public OrganizerController(Database db)
        {
            this.db = db;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult viewEvent2(int? page, EventSearchFilterViewModel model)
        {
            return View("~/Views/Organizer/ViewEventApproved.cshtml");
        }

        public ActionResult ViewAllYourEvents(int? page, EventSearchFilterViewModel model)
        {
            if (string.IsNullOrEmpty(model.Status))
                model.Status = DEFAULT_STATUS_FILTER;
            if (model.Status != DEFAULT_STATUS_FILTER && model.Status != ALL_STATUS_FILTER && !PUBLIC_STATUSES.Contains(model.Status))
                ModelState.AddModelError("Status", "Status not public or not available");
            if (!ModelState.IsValid)
                return View("Error");
            model.Search = model.Search.ToUnsignString();
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e)
            {
                return e.OrganizerId == user.UserId && ((model.Status.Equals(ALL_STATUS_FILTER) && PUBLIC_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(ALL_STATUS_FILTER) && ((model.Status.Equals(DEFAULT_STATUS_FILTER) && DEFAULT_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(DEFAULT_STATUS_FILTER) && e.EventStatus.Equals(model.Status))))) && (string.IsNullOrEmpty(model.Search) || e.EventName.ToUnsignString().ContainsIgnoreCase(model.Search) || e.EventDescription.ToUnsignString().ContainsIgnoreCase(model.Search) || (e.EventFamily != null && e.EventFamily.FamilyName.ToUnsignString().ContainsIgnoreCase(model.Search)) || e.EventPlace.ToUnsignString().ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel
            {
                Action = "ViewAllYourEvents",
                Size = VIEW_ALL_YOUR_EVENTS_PAGE_SIZE,
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = Convert.ToInt32(Math.Ceiling(1.0 * dbevents.Count() / VIEW_ALL_YOUR_EVENTS_PAGE_SIZE))
            };
            List<EventModel> events = new List<EventModel>();
            foreach (Event eventt in dbevents.OrderBy(u => u.OpenDate).Skip((pages.Current - 1) * VIEW_ALL_YOUR_EVENTS_PAGE_SIZE).Take(VIEW_ALL_YOUR_EVENTS_PAGE_SIZE))
            {
                Group group = db.Groups.Where(g => g.GroupId == eventt.GroupId && g.IsEnabled).SingleOrDefault();
                events.Add(new EventModel()
                {
                    Id = eventt.EventId,
                    Name = eventt.EventName,
                    Status = eventt.EventStatus,
                    Group = group != null ? new GroupModel()
                    {
                        Id = group.GroupId,
                        Name = group.GroupName
                    } : null,
                    CreatedDate = eventt.CreatedDate,
                    OpenDate = eventt.OpenDate,
                    CloseDate = eventt.CloseDate
                });
            }
            List<SelectListItem> statuses = new List<SelectListItem> {
                    new SelectListItem() {
                        Text = ALL_STATUS_FILTER,
                        Value = ALL_STATUS_FILTER
                    },
                    //new SelectListItem() {
                    //    Text = string.Join(" and ", DEFAULT_STATUSES),
                    //    Value = DEFAULT_STATUS_FILTER
                    //}
                };
            foreach (string status in PUBLIC_STATUSES)
                statuses.Add(new SelectListItem()
                {
                    Text = status,
                });
            ViewBag.Events = events;
            ViewBag.Statuses = statuses;
            ViewBag.Pages = pages;
            return View("~/Views/Organizer/ViewEventApproved.cshtml");
        }
    }
}
