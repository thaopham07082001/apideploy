﻿using System.Web.Mvc;

namespace capstone.Controllers {
    [AllowAnonymous]
    public class ErrorController : Controller {
        public ActionResult Index() {
            return View("Error");
        }

        public ActionResult NotFound() {
            return View("NotFound");
        }
    }
}