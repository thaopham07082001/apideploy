﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using capstone.api.Models;
using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Helpers;
using capstone.Models;

namespace capstone.Controllers
{
    [Authorize(Roles = "Student")]
    [CheckUserRole]
    public class StudentController : Controller
    {
        private const int VIEW_ALL_EVENTS_GRID_PAGE_SIZE = 10;
        private const int VIEW_ALL_EVENTS_LIST_PAGE_SIZE = 20;
        private const int GET_ALL_CATEGORIES_LIST_SIZE = 5;
        private const int GET_ALL_ORGANIZERS_AND_GROUPS_LIST_SIZE = 5;
        private const int GET_NOTIFICATIONS = 10;
        private const int NUMBER_FEEDBACKS_PER_LOAD = 5;
        private const int VIEW_YOUR_REGISTERED_EVENTS_PAGE_SIZE = 5;
        private static readonly string[] PUBLIC_STATUSES = new string[] { EventStatusConstants.OPENING, EventStatusConstants.HAPPENING, EventStatusConstants.CLOSED };
        private static readonly string[] DEFAULT_STATUSES = new string[] { EventStatusConstants.OPENING, EventStatusConstants.HAPPENING };
        private const string DEFAULT_STATUS_FILTER = "Default";
        private const string ALL_STATUS_FILTER = "All";
        private const string ALL_CATEGORIES = "All categories";
        private const string ALL_ORGANIZERS_AND_GROUPS = "All organizers and groups";
        private const string DEFAULT_RANGE = "Within 2 months";
        private const string ALL_RANGE_FILTER = "All";
        private const string DEFAULT_RANGE_FILTER = "Default";
        private const string CUSTOM_RANGE_FILTER = "Custom";
        


        [AllowAnonymous]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Authentication", new { ReturnUrl = "/Student/" });
            else if (!User.IsStudent())
                return View("Unauthorized");
            else
            {
                int abouttohappen = db.Registers.Where(r => r.User.Email.Equals(User.Identity.Name)).Join(db.Events, r => r.EventId, e => e.EventId, (r, e) => e).Where(delegate (Event e) {
                    return e.EventStatus.Equals(EventStatusConstants.OPENING) && e.OpenDate <= DateTime.Now.AddHours(1);
                }).Count();
                List<EventModel> featured = db.Events.Where(e => DEFAULT_STATUSES.Contains(e.EventStatus) && e.IsFeatured).Select(e => new EventModel()
                {
                    Id = e.EventId,
                    Name = e.EventName,
                    CoverImage = e.CoverImage,
                    Description = e.EventDescription
                }).ToList().Shuffle();
                ViewBag.AboutToHappen = abouttohappen;
                ViewBag.Featured = featured;
                List<EventModel> happening = db.Events.Where(e => e.EventStatus.Equals(EventStatusConstants.OPENING)).Select(e => new EventModel()
                {
                    Id = e.EventId,
                    Name = e.EventName,
                    CoverImage = e.CoverImage,
                    Description = e.EventDescription,
                    CreatedDate = e.CreatedDate
                }).ToList().Shuffle();
                ViewBag.Happening = happening;
                List<CategoryModel> categories = db.Categories.Select(c => new CategoryModel()
                {
                    Id = c.CategoryId,
                    Name = c.CategoryName
                }).ToList();
                ViewBag.Categories = categories;
                return View();
            }
        }

        public ActionResult ViewSearchEvent(int? categoryId, string status, int? page, EventSearchFilterViewModel model)
        {
            if (string.IsNullOrEmpty(model.Status))
                model.Status = DEFAULT_STATUS_FILTER;
            if (model.Status != DEFAULT_STATUS_FILTER && model.Status != ALL_STATUS_FILTER && !PUBLIC_STATUSES.Contains(model.Status))
                ModelState.AddModelError("Status", "Status not public or not available");
            if (!ModelState.IsValid)
                return View("Error");
            if (string.IsNullOrEmpty(model.Range))
                model.Range = DEFAULT_RANGE_FILTER;
            if (model.Range.Equals(DEFAULT_RANGE_FILTER))
            {
                model.From = DateTime.Now.ToStandardString();
                model.To = CalendarHelper.GetDateFromString(model.From).AddMonths(2).ToStandardString();
            }
            if (string.IsNullOrEmpty(model.Display))
                model.Display = DisplayModeConstants.GRID;
            int pagesize = 0;
            if (model.Display.Equals(DisplayModeConstants.GRID))
                pagesize = VIEW_ALL_EVENTS_GRID_PAGE_SIZE;
            else if (model.Display.Equals(DisplayModeConstants.LIST))
                pagesize = VIEW_ALL_EVENTS_LIST_PAGE_SIZE;
            else
                return View("Error");
            DateTime startDate;
            DateTime endDate;
            if (!model.Range.Equals(ALL_RANGE_FILTER))
            {
                startDate = CalendarHelper.GetDateFromString(model.From);
                endDate = CalendarHelper.GetDateFromString(model.To);
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Authentication", new { ReturnUrl = "/Student/" });
            else if (!User.IsStudent())
                return View("Unauthorized");
            else
            {
                int abouttohappen = db.Registers.Where(r => r.User.Email.Equals(User.Identity.Name)).Join(db.Events, r => r.EventId, e => e.EventId, (r, e) => e).Where(delegate (Event e)
                {
                    return e.EventStatus.Equals(EventStatusConstants.OPENING) && e.OpenDate <= DateTime.Now.AddHours(1);
                }).Count();
                List<EventModel> featured = db.Events.Where(e => DEFAULT_STATUSES.Contains(e.EventStatus) && e.IsFeatured).Select(e => new EventModel()
                {
                    Id = e.EventId,
                    Name = e.EventName,
                    CoverImage = e.CoverImage,
                    Description = e.EventDescription
                }).ToList().Shuffle();
                ViewBag.AboutToHappen = abouttohappen;
                ViewBag.Featured = featured;
                List<EventModel> happening = db.Events.Where(e => e.EventStatus.Equals(EventStatusConstants.OPENING)).Select(e => new EventModel()
                {
                    Id = e.EventId,
                    Name = e.EventName,
                    CoverImage = e.CoverImage,
                    Description = e.EventDescription,
                    CreatedDate = e.CreatedDate
                }).ToList().Shuffle();
                ViewBag.Happening = happening;
                model.Search = model.Search.ToUnsignString();
                IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e)
                {
                    return (model.Range.Equals(ALL_RANGE_FILTER) || CalendarHelper.CheckOverlap(e.OpenDate, e.CloseDate, startDate, endDate)) && ((model.Status.Equals(ALL_STATUS_FILTER) && PUBLIC_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(ALL_STATUS_FILTER) && ((model.Status.Equals(DEFAULT_STATUS_FILTER) && DEFAULT_STATUSES.Contains(e.EventStatus)) || (!model.Status.Equals(DEFAULT_STATUS_FILTER) && e.EventStatus.Equals(model.Status))))) && (string.IsNullOrEmpty(model.Search) || e.EventName.ToUnsignString().ContainsIgnoreCase(model.Search) || e.EventDescription.ToUnsignString().ContainsIgnoreCase(model.Search) || (e.EventFamily != null && e.EventFamily.FamilyName.ToUnsignString().ContainsIgnoreCase(model.Search)) || e.EventPlace.ToUnsignString().ContainsIgnoreCase(model.Search)) && (model.Category == null || e.Categories.Count(c => c.CategoryId == model.Category.Value) > 0) && (model.OrganizerOrGroup == null || (!model.IsGroup && e.OrganizerId == model.OrganizerOrGroup) || (model.IsGroup && e.GroupId == model.OrganizerOrGroup));
                });
                PageModel pages = new PageModel
                {
                    Action = "ViewAllEvents",
                    Size = pagesize,
                    Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                    Max = Convert.ToInt32(Math.Ceiling(1.0 * dbevents.Count() / pagesize))
                };
                List<SelectListItem> organizers = db.Users.Where(delegate (User u)
                {
                    return u.IsEnabled && u.IsOrganizer;
                }).Take(GET_ALL_ORGANIZERS_AND_GROUPS_LIST_SIZE).Select(u => new SelectListItem()
                {
                    Text = u.Email,
                    Value = u.UserId.ToString()
                }).ToList();
                List<SelectListItem> groups = db.Groups.Where(delegate (Group g)
                {
                    return g.IsEnabled;
                }).Take(GET_ALL_ORGANIZERS_AND_GROUPS_LIST_SIZE).Select(g => new SelectListItem()
                {
                    Text = g.GroupName,
                    Value = g.GroupId.ToString()
                }).ToList();
                ViewBag.Organizers = organizers;
                ViewBag.Groups = groups;
                List<SelectListItem> categories = db.Categories.Where(delegate (Category c)
                {
                    return c.IsEnabled;
                }).Take(GET_ALL_CATEGORIES_LIST_SIZE).Select(c => new SelectListItem()
                {
                    Text = c.CategoryName,
                    Value = c.CategoryId.ToString()
                }).ToList();
                ViewBag.Categories = categories;
                List<EventModel> events;

                if (categoryId != null)
                {
                    events = db.Categories.Where(a => a.CategoryId == categoryId).SelectMany(e => e.Events)

                        .Select(e => new EventModel()
                        {
                            Id = e.EventId,
                            Name = e.EventName,
                            CoverImage = e.CoverImage,
                            Description = e.EventDescription,
                            CreatedDate = e.CreatedDate,
                            Place = e.EventPlace
                        })
                    .ToList().Shuffle();
                }
                else
                {
                    events = new List<EventModel>();
                    foreach (Event eventt in dbevents.OrderBy(u => u.OpenDate).Skip((pages.Current - 1) * pagesize).Take(pagesize))
                    {
                        User user = db.Users.Where(u => u.UserId == eventt.OrganizerId).Single();
                        Group group = db.Groups.Where(g => g.GroupId == eventt.GroupId && g.IsEnabled).SingleOrDefault();
                        List<Group> currgroups = new List<Group>();
                        bool canRegisterByGroup = true;
                        try
                        {
                            canRegisterByGroup = eventt.IsRegisteredByGroup(ref currgroups, User.GetUser());
                        }
                        catch
                        {
                            currgroups = new List<Group>();
                            canRegisterByGroup = eventt.IsRegisteredByGroup(ref currgroups, null);
                        }
                        events.Add(new EventModel()
                        {
                            Id = eventt.EventId,
                            Name = eventt.EventName,
                            CoverImage = eventt.CoverImage,
                            IsPublic = eventt.IsPublic,
                            IsFeatured = eventt.IsFeatured,
                            Status = eventt.EventStatus,
                            Description = eventt.EventDescription,
                            Place = eventt.EventPlace,
                            Organizer = new UserModel()
                            {
                                Id = user.UserId,
                                FirstName = user.UserProfile.FirstName,
                                LastName = user.UserProfile.LastName,
                                ProfileImage = user.UserProfile.ProfileImage
                            },
                            Group = group != null ? new GroupModel()
                            {
                                Id = group.GroupId,
                                Name = group.GroupName
                            } : null,
                            CreatedDate = eventt.CreatedDate,
                            OpenDate = eventt.OpenDate,
                            CloseDate = eventt.CloseDate,
                            RegisterEndDate = eventt.RegisterEndDate ?? eventt.OpenDate,
                            IsRegistered = eventt.Registers.Join(db.Users.Where(u => u.Email.Equals(User.Identity.Name)), r => r.UserId, u => u.UserId, (r, u) => u).Count() > 0,
                            RegisterCount = eventt.Registers.Count,
                            RegisterMax = eventt.RegisterMax,
                            IsOrganizerOnly = eventt.IsOrganizerOnly,
                            CanRegisterByGroup = canRegisterByGroup,
                        });
                    }
                }
                string category = ALL_CATEGORIES;
                string organizerOrGroup = ALL_ORGANIZERS_AND_GROUPS;
                if (model.Category != null)
                    category = db.Categories.Where(c => c.CategoryId == model.Category.Value).Single().CategoryName;
                if (model.OrganizerOrGroup != null)
                {
                    if (!model.IsGroup)
                        organizerOrGroup = db.Users.Where(u => u.UserId == model.OrganizerOrGroup.Value && u.IsEnabled).Single().Email;
                    else
                        organizerOrGroup = db.Groups.Where(g => g.GroupId == model.OrganizerOrGroup.Value && g.IsEnabled).Single().GroupName;
                }
                List<SelectListItem> statuses = new List<SelectListItem> {
                    new SelectListItem() {
                        Text = ALL_STATUS_FILTER,
                        Value = ALL_STATUS_FILTER
                    },
                    new SelectListItem() {
                        Text = string.Join(" and ", DEFAULT_STATUSES),
                        Value = DEFAULT_STATUS_FILTER
                    }
                };
                foreach (string status1 in PUBLIC_STATUSES)
                    statuses.Add(new SelectListItem()
                    {
                        Text = status1,
                    });
                List<SelectListItem> ranges = new List<SelectListItem> {
                    new SelectListItem() {
                        Text = ALL_RANGE_FILTER,
                        Value = ALL_RANGE_FILTER
                    },
                    new SelectListItem() {
                        Text = DEFAULT_RANGE,
                        Value = DEFAULT_RANGE_FILTER
                    },
                    new SelectListItem() {
                        Value = CUSTOM_RANGE_FILTER
                    }
                };

                if (model.Range == null)
                    model.Range = DEFAULT_RANGE_FILTER;


                ViewBag.Events = events;
                ViewBag.Category = category;
                ViewBag.OrganizerOrGroup = organizerOrGroup;
                ViewBag.Statuses = statuses;
                ViewBag.Ranges = ranges;
                ViewBag.CustomRangeFilter = CUSTOM_RANGE_FILTER;
                if (categoryId != null)
                {
                    model.Status = ALL_STATUS_FILTER;
                    model.Range = ALL_RANGE_FILTER;
                    model.Category = categoryId;
                    ViewBag.Category = db.Categories.Where(c => c.CategoryId == categoryId).Single().CategoryName;
                }
                ViewBag.Pages = pages;
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult GetAllCategories(string search)
        {
            search = search.ToUnsignString();
            List<SelectListItem> categories = db.Categories.Where(delegate (Category c) {
                return c.IsEnabled;
            }).Take(GET_ALL_CATEGORIES_LIST_SIZE).Select(c => new SelectListItem()
            {
                Text = c.CategoryName,
                Value = c.CategoryId.ToString()
            }).ToList();
            ViewBag.Categories = categories;
            return PartialView();
        }
        [HttpPost]
        public ActionResult GetAllCategoriesOld(string search)
        {
            search = search.ToUnsignString();
            List<SelectListItem> categories = db.Categories.Where(delegate (Category c) {
                return c.IsEnabled && c.CategoryName.ToUnsignString().ContainsIgnoreCase(search);
            }).Take(GET_ALL_CATEGORIES_LIST_SIZE).Select(c => new SelectListItem()
            {
                Text = c.CategoryName,
                Value = c.CategoryId.ToString()
            }).ToList();
            ViewBag.Categories = categories;
            return PartialView();
        }

        public ActionResult ViewYourEventCalendar(int? week, int? year)
        {
            TimeSpan interval = new TimeSpan(1, 0, 0);
            List<DateTime> dates = new List<DateTime>();
            List<TimeSpan> times = new List<TimeSpan>();
            Dictionary<DateTime, List<CalendarModel>> calendarTime = new Dictionary<DateTime, List<CalendarModel>>();
            List<CalendarModel> calendarDay = new List<CalendarModel>();
            Dictionary<DateTime, int> stacksTime = new Dictionary<DateTime, int>();
            int stacksDay = 0;
            if (!year.HasValue)
                year = DateTime.Now.Year;
            if (!week.HasValue)
                week = DateTime.Now.GetWeekOfYear();
            DateTime startDate = CalendarHelper.GetFirstDayOfWeek(week.Value, year.Value);
            DateTime endDate = startDate.AddDays(7).AddSeconds(-1);
            int max = CalendarHelper.GetNumberOfWeeksInYear(year.Value);
            WeekModel weeks = new WeekModel()
            {
                PreviousWeek = week.Value > 1 ? week.Value - 1 : CalendarHelper.GetNumberOfWeeksInYear(year.Value - 1),
                PreviousYear = week.Value > 1 ? year.Value : year.Value - 1,
                NextWeek = week.Value < max ? week.Value + 1 : 1,
                NextYear = week.Value < max ? year.Value : year.Value + 1,
            };
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
            {
                dates.Add(date);
                calendarTime.Add(date, new List<CalendarModel>());
                stacksTime.Add(date, 0);
            }
            for (TimeSpan time = new TimeSpan(0, 0, 0); time < new TimeSpan(24, 00, 00); time = time.Add(interval))
                times.Add(time);
            User user = User.GetUser();
            IEnumerable<Event> dbevents = db.Events.Join(db.Bookmarks.Where(b => b.UserId == user.UserId), e => e.EventId, b => b.EventId, (e, b) => e).Where(delegate (Event e) {
                return PUBLIC_STATUSES.Contains(e.EventStatus) && CalendarHelper.CheckOverlap(e.OpenDate, e.CloseDate, startDate, endDate);
            }).Union(db.Events.Join(db.Registers.Where(r => r.UserId == user.UserId), e => e.EventId, r => r.EventId, (e, r) => e).Where(delegate (Event e) {
                return PUBLIC_STATUSES.Contains(e.EventStatus) && CalendarHelper.CheckOverlap(e.OpenDate, e.CloseDate, startDate, endDate);
            })).Distinct();
            IEnumerable<CalendarApiModel> calendarModels = ApiHelper.GetCalendars(User.Identity.Name, startDate, endDate);
            foreach (Event eventt in dbevents)
                if (eventt.OpenDate.Date == eventt.CloseDate.Date)
                {
                    DateTime date = eventt.OpenDate.Date;
                    int startTime = Convert.ToInt32(eventt.OpenDate.TimeOfDay.TotalMinutes / interval.TotalMinutes);
                    int endTime = Convert.ToInt32(eventt.CloseDate.TimeOfDay.TotalMinutes / interval.TotalMinutes);
                    CalendarModel newCalendarModel = new CalendarModel()
                    {
                        Id = eventt.EventId,
                        Name = eventt.EventName,
                        Start = startTime,
                        End = endTime,
                        StartDate = eventt.OpenDate,
                        EndDate = eventt.CloseDate,
                        Stack = -1
                    };
                    for (int i = 1; i <= stacksTime[date]; i++)
                    {
                        bool overlap = false;
                        foreach (CalendarModel calendarModel in calendarTime[date].Where((c, b) => c.Stack == i))
                        {
                            if (CalendarHelper.CheckOverlap(calendarModel.Start, calendarModel.End, newCalendarModel.Start, newCalendarModel.End))
                            {
                                overlap = true;
                                break;
                            }
                        }
                        if (!overlap)
                        {
                            newCalendarModel.Stack = i;
                            break;
                        }
                    }
                    if (newCalendarModel.Stack < 0)
                        newCalendarModel.Stack = ++stacksTime[date];
                    calendarTime[date].Add(newCalendarModel);
                }
                else
                {
                    int startTime = 0;
                    int endTime = 6;
                    for (int i = 0; i < dates.Count; i++)
                    {
                        if (eventt.OpenDate.Date == dates[i])
                            startTime = i;
                        if (eventt.CloseDate.Date == dates[i])
                            endTime = i;
                    }
                    CalendarModel newCalendarModel = new CalendarModel()
                    {
                        Id = eventt.EventId,
                        Name = eventt.EventName,
                        Start = startTime,
                        End = endTime,
                        StartDate = eventt.OpenDate,
                        EndDate = eventt.CloseDate,
                        Stack = -1
                    };
                    for (int i = 1; i <= stacksDay; i++)
                    {
                        bool overlap = false;
                        foreach (CalendarModel calendarModel in calendarDay.Where((c, b) => c.Stack == i))
                        {
                            if (CalendarHelper.CheckOverlap(calendarModel.Start, calendarModel.End, newCalendarModel.Start, newCalendarModel.End))
                            {
                                overlap = true;
                                break;
                            }
                        }
                        if (!overlap)
                        {
                            newCalendarModel.Stack = i;
                            break;
                        }
                    }
                    if (newCalendarModel.Stack < 0)
                        newCalendarModel.Stack = ++stacksDay;
                    calendarDay.Add(newCalendarModel);
                }
            foreach (CalendarApiModel ccalendar in calendarModels)
                if (ccalendar.StartDate.Date == ccalendar.EndDate.Date)
                {
                    DateTime date = ccalendar.StartDate.Date;
                    int startTime = Convert.ToInt32(ccalendar.StartDate.TimeOfDay.TotalMinutes / interval.TotalMinutes);
                    int endTime = Convert.ToInt32(ccalendar.EndDate.TimeOfDay.TotalMinutes / interval.TotalMinutes);
                    CalendarModel newCalendarModel = new CalendarModel()
                    {
                        Name = ccalendar.Name,
                        Start = startTime,
                        End = endTime,
                        StartDate = ccalendar.StartDate,
                        EndDate = ccalendar.EndDate,
                        IsPrivate = true,
                        Stack = -1
                    };
                    for (int i = 1; i <= stacksTime[date]; i++)
                    {
                        bool overlap = false;
                        foreach (CalendarModel calendarModel in calendarTime[date].Where((c, b) => c.Stack == i))
                        {
                            if (CalendarHelper.CheckOverlap(calendarModel.Start, calendarModel.End, newCalendarModel.Start, newCalendarModel.End))
                            {
                                overlap = true;
                                break;
                            }
                        }
                        if (!overlap)
                        {
                            newCalendarModel.Stack = i;
                            break;
                        }
                    }
                    if (newCalendarModel.Stack < 0)
                        newCalendarModel.Stack = ++stacksTime[date];
                    calendarTime[date].Add(newCalendarModel);
                }
                else
                {
                    int startTime = 0;
                    int endTime = 6;
                    for (int i = 0; i < dates.Count; i++)
                    {
                        if (ccalendar.StartDate.Date == dates[i])
                            startTime = i;
                        if (ccalendar.EndDate.Date == dates[i])
                            endTime = i;
                    }
                    CalendarModel newCalendarModel = new CalendarModel()
                    {
                        Name = ccalendar.Name,
                        Start = startTime,
                        End = endTime,
                        StartDate = ccalendar.StartDate,
                        EndDate = ccalendar.EndDate,
                        IsPrivate = true,
                        Stack = -1
                    };
                    for (int i = 1; i <= stacksDay; i++)
                    {
                        bool overlap = false;
                        foreach (CalendarModel calendarModel in calendarDay.Where((c, b) => c.Stack == i))
                        {
                            if (CalendarHelper.CheckOverlap(calendarModel.Start, calendarModel.End, newCalendarModel.Start, newCalendarModel.End))
                            {
                                overlap = true;
                                break;
                            }
                        }
                        if (!overlap)
                        {
                            newCalendarModel.Stack = i;
                            break;
                        }
                    }
                    if (newCalendarModel.Stack < 0)
                        newCalendarModel.Stack = ++stacksDay;
                    calendarDay.Add(newCalendarModel);
                }
            int current = Convert.ToInt32(new TimeSpan(6, 0, 0).TotalMinutes / interval.TotalMinutes);
            if (dbevents.Count() > 0 && dbevents.Where(e => e.OpenDate.Date == e.CloseDate.Date).ToList().Count != 0)
                current = Convert.ToInt32(dbevents.Where(e => e.OpenDate.Date == e.CloseDate.Date).Min(e => e.OpenDate.TimeOfDay).TotalMinutes / interval.TotalMinutes);
            if (calendarModels.Count() > 0)
                current = Math.Min(current, Convert.ToInt32(calendarModels.Min(c => c.StartDate.TimeOfDay).TotalMinutes / interval.TotalMinutes));
            ViewBag.Dates = dates;
            ViewBag.Times = times;
            ViewBag.CalendarTime = calendarTime;
            ViewBag.CalendarDay = calendarDay;
            ViewBag.StacksTime = stacksTime;
            ViewBag.StacksDay = stacksDay;
            ViewBag.Current = current;
            ViewBag.Weeks = weeks;
            return View();
        }

        public ActionResult ViewTicketDetail(int EventId, int UserId)
        {
            var eventt = db.Events.Where(e => e.EventId == EventId).SingleOrDefault();
            var eventData = db.Registers.Where(e => e.EventId == EventId && e.UserId == UserId).SingleOrDefault();
            TicketDetailModels ticketDetailModels = new TicketDetailModels
            {
                event_name = eventt.EventName,
                opent_date = eventt.OpenDate,
                location = eventt.EventPlace,
                booked_date = DateTime.Now,
                qr_code = eventData.QRCode
            };

            ViewData["MyModel"] = ticketDetailModels;

            return View("~/Views/Home/TicketDetail.cshtml", ticketDetailModels);
        }

        public ActionResult ViewYourEventCalendarByDate(DateTime? date)
        {
            if (date == null)
                date = DateTime.Now;
            return RedirectToAction("ViewYourEventCalendar", new { week = date.Value.GetWeekOfYear(), year = date.Value.Year });
        }

        public ActionResult RegisterEvent(int id, bool? submitted)
        {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null || !eventt.EventStatus.Equals(EventStatusConstants.OPENING) || (eventt.RegisterEndDate != null && eventt.RegisterEndDate < DateTime.Now))
                return View("NotFound");
            if (eventt.Forms.Count == 0 || (submitted ?? false))
            {
                User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
                var check_register = db.Registers.Where(c => c.EventId == id && c.UserId == user.UserId).FirstOrDefault();
                if (check_register != null)
                {
                    return RedirectToAction("ViewEvent", "Home", new { id });
                }
                Register register = db.Registers.Create();

                register.Event = eventt;
                register.User = user;
                register.RegisteredDate = DateTime.Now;
                QRCoder.QRCodeGenerator qrCode = new QRCoder.QRCodeGenerator();
                var qrData = qrCode.CreateQrCode(("eventID: " + eventt.EventId.ToString() + "\nuserID: " + user.UserId).ToString(), QRCoder.QRCodeGenerator.ECCLevel.H);
                var code = new QRCoder.QRCode(qrData);
                Bitmap image = code.GetGraphic(20);

                MemoryStream ms = new MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                var bytes = ms.ToArray();
                string base64String = Convert.ToBase64String(bytes);
                register.QRCode = base64String;
                register.Status = TicketStatusConstants.Active;
                db.Registers.Add(register);
                if (db.Bookmarks.Where(b => b.EventId == eventt.EventId && b.UserId == user.UserId).Count() == 0)
                {
                    Bookmark bookmark = db.Bookmarks.Create();
                    bookmark.Event = eventt;
                    bookmark.User = user;
                    bookmark.CreatedDate = DateTime.Now;
                    db.Bookmarks.Add(bookmark);
                }
                db.SaveChanges();

                eventt.Organizer.AddNotification(user.UserId, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.ORGANIZER_EVENT_REGISTER);
                this.SetMessage("Successfully registered to event");
                TicketDetailModels ticketDetailModels = new TicketDetailModels
                {
                    event_name = eventt.EventName,
                    opent_date = eventt.OpenDate,
                    location = eventt.EventPlace,
                    booked_date = DateTime.Now,
                    qr_code = base64String
                };

                ViewData["MyModel"] = ticketDetailModels;

                return RedirectToAction("ViewTicketDetail", "Student", new { eventt.EventId,user.UserId });
            }
            else
            {
                UserProfile profile = User.GetUser().UserProfile;
                ViewBag.EventId = eventt.EventId;
                ViewBag.FormRegister = eventt.Forms.Single().FormRegister.Replace("ThangLVSE04854", string.Join(" ", profile.LastName, profile.FirstName)).Replace("1997-01-13", profile.DOB.HasValue ? profile.DOB.Value.ToStandardString() : string.Empty).Replace("thanglvse04854@fpt.edu.vn", User.Identity.Name).Replace("0332554026", profile.Phone);
                return View();
            }
        }

        public ActionResult UnregisterEvent(int id)
        {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null || !eventt.EventStatus.Equals(EventStatusConstants.OPENING))
                return View("NotFound");
            User user = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single();
            db.Registers.RemoveRange(db.Registers.Where(r => r.EventId == eventt.EventId && r.UserId == user.UserId));
            db.Bookmarks.RemoveRange(db.Bookmarks.Where(b => b.EventId == eventt.EventId && b.UserId == user.UserId));
            db.SaveChanges();
            this.SetMessage("Successfully unregistered from event");
            return RedirectToAction("ViewEvent", "Home", new { id });
        }

        [HttpPost]
        public JsonResult UpdateFeedback(Feedback feedback)
        {
            if (db.Feedbacks.Any(f => f.UserId == feedback.UserId && f.EventId == feedback.EventId))
            {
                Feedback feedbackDB = db.Feedbacks.Where(f => f.UserId == feedback.UserId && f.EventId == feedback.EventId).SingleOrDefault();
                feedbackDB.Value = feedback.Value;
                feedbackDB.FeedbackContent = feedback.FeedbackContent;
                feedbackDB.CreatedDate = feedback.CreatedDate;
            }
            else
            {
                db.Feedbacks.Add(feedback);
                User user = db.Users.Where(u => u.UserId == db.Events.FirstOrDefault(e => e.EventId == feedback.EventId).OrganizerId).Single();
                user.AddNotification(feedback.UserId, feedback.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.ORGANIZER_EVENT_FEEDBACK);
            }
            db.SaveChanges();
            feedback = db.Feedbacks.Single(f => f.UserId == feedback.UserId && f.EventId == feedback.EventId);
            Event fbEvent = db.Events.Single(e => e.EventId == feedback.EventId);
            User fbUser = db.Users.Single(u => feedback.UserId == u.UserId);
            return Json(new FeedbackModel()
            {
                Key = feedback.UserId.ToString(),
                Username = fbUser.UserProfile.LastName + " " + fbUser.UserProfile.FirstName,
                UserImage = fbUser.UserProfile.ProfileImage,
                Value = feedback.Value,
                CreatedDate = feedback.CreatedDate,
                txtCreatedDate = feedback.CreatedDate.ToString("MMM dd, yyyy"),
                Content = feedback.FeedbackContent,
                AvgRating = fbEvent.GetRating(),
                FeedbackCount = fbEvent.GetFeedbacks().Count,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFeedback(Feedback feedback)
        {
            db.Feedbacks.Remove(db.Feedbacks.Where(f => f.UserId == feedback.UserId && f.EventId == feedback.EventId).SingleOrDefault());
            db.SaveChanges();
            Event eventt = db.Events.Single(e => e.EventId == feedback.EventId);
            return Json(new FeedbackModel()
            {
                AvgRating = eventt.GetRating(),
                FeedbackCount = eventt.GetFeedbacks().Count,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewRegisteredEvents(int? page, EventSearchFilterViewModel model)
        {
            if (string.IsNullOrEmpty(model.Status))
                model.Status = DEFAULT_STATUS_FILTER;
            if (model.Status != DEFAULT_STATUS_FILTER && model.Status != ALL_STATUS_FILTER && !PUBLIC_STATUSES.Contains(model.Status))
                ModelState.AddModelError("Status", "Status not public or not available");
            if (!ModelState.IsValid)
                return View("Error");
            model.Search = model.Search.ToUnsignString();
            List<Event> inforEvent = new List<Event>();
            User user = db.Users.SingleOrDefault(u => u.Email.Equals(User.Identity.Name));
            List<Register> eventList = db.Registers.Where(e => e.UserId == user.UserId).ToList();
            foreach (Register item in eventList)
            {
                Event eventTemp = db.Events.Where(e => e.EventId == item.EventId).SingleOrDefault();
                if (eventTemp != null)
                    inforEvent.Add(eventTemp);
            }
            List<Event> allRegisteredEvent = new List<Event>();
            foreach (Event e in inforEvent)
                if ((model.Status.Equals(ALL_STATUS_FILTER) && PUBLIC_STATUSES.Contains(e.EventStatus))
                        || (!model.Status.Equals(ALL_STATUS_FILTER)
                            && ((model.Status.Equals(DEFAULT_STATUS_FILTER) && DEFAULT_STATUSES.Contains(e.EventStatus))
                                || (!model.Status.Equals(DEFAULT_STATUS_FILTER) && e.EventStatus.Equals(model.Status))))
                    )
                    if (string.IsNullOrEmpty(model.Search)
                        || e.EventName.ToUnsignString().ContainsIgnoreCase(model.Search)
                        || e.EventDescription.ToUnsignString().ContainsIgnoreCase(model.Search)
                        || (e.EventFamily != null && e.EventFamily.FamilyName.ToUnsignString().ContainsIgnoreCase(model.Search))
                        || e.EventPlace.ToUnsignString().ContainsIgnoreCase(model.Search))
                        allRegisteredEvent.Add(e);
            PageModel pages = new PageModel
            {
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * allRegisteredEvent.Count() / VIEW_YOUR_REGISTERED_EVENTS_PAGE_SIZE)
            };
            List<Event> searchEvent = new List<Event>();
            foreach (Event e in allRegisteredEvent.Skip((pages.Current - 1) * VIEW_YOUR_REGISTERED_EVENTS_PAGE_SIZE).Take(VIEW_YOUR_REGISTERED_EVENTS_PAGE_SIZE))
                searchEvent.Add(e);
            List<SelectListItem> statuses = new List<SelectListItem> {
                new SelectListItem() {
                    Text = ALL_STATUS_FILTER,
                    Value = ALL_STATUS_FILTER
                },
                new SelectListItem() {
                    Text = string.Join(" and ", DEFAULT_STATUSES),
                    Value = DEFAULT_STATUS_FILTER
                }
            };
            foreach (string status in PUBLIC_STATUSES)
                statuses.Add(new SelectListItem()
                {
                    Text = status,
                });
            ViewBag.Pages = pages;
            ViewBag.Statuses = statuses;
            ViewBag.Events = searchEvent;
            ViewBag.UserId = user.UserId;
            return View(model);
        }

        [HttpPost]
        public JsonResult Bookmark(int EventId, int UserId)
        {
            db.Bookmarks.Add(new Bookmark()
            {
                UserId = UserId,
                EventId = EventId,
                CreatedDate = DateTime.Now
            });
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Unbookmark(int EventId, int UserId)
        {
            Bookmark bookmark = db.Bookmarks.Where(b => b.EventId == EventId && b.UserId == UserId).Single();
            db.Bookmarks.Remove(bookmark);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //start view bookmarked event - hieunqhe153633 - spring2023

        [HttpGet]
        public ActionResult ShowBookmarkedEvent()
        {
            var userId = 0;
            if (User.Identity.IsAuthenticated)
            {
                userId = User.GetUser().UserId;
            }
            var events = new List<Event>();
            if (userId != 0)
            {
                var bms = db.Bookmarks.Where(x => x.UserId == userId).ToList();
                if (bms.Any())
                {
                    foreach (Bookmark b in bms)
                    {
                        var e = db.Events.Include("Campus").Where(x => x.EventId == b.EventId).FirstOrDefault();
                        events.Add(e);
                    }
                }
            }
            var eventViews = new List<EventViewModel>();
            foreach (Event e in events)
            {
                var eventView = new EventViewModel { EventId = e.EventId, EventName = e.EventName, EventStatus = e.EventStatus, CampusName = e.Campus.CampusName, OpenDate = e.OpenDate, CoverImage = e.CoverImage };
                eventViews.Add(eventView);
            }

            ViewBag.bm = eventViews;
            return View();
        }


        public ActionResult UnBookmarked(int id)
        {
            var bookmarks = db.Bookmarks.Where(x => x.EventId == id).ToList();
            int userId = User.GetUser().UserId;
            foreach (Bookmark bookmark in bookmarks)
            {
                if (bookmark != null && bookmark.UserId == userId)
                {
                    db.Bookmarks.Remove(bookmark);
                }
            }
            db.SaveChanges();
            return RedirectToAction("ShowBookmarkedEvent");
        }
        //end view bookmarked event - hieunqhe153633 - spring2023

        private Database db;

        public StudentController()
        {
            db = new Database();
        }

        public StudentController(Database db)
        {
            this.db = db;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}