﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Helpers;
using capstone.Models;

namespace capstone.Controllers {
    [Authorize(Roles = "Manager")]
    [CheckUserRole]
    public class ManagerController : Controller {
        private const int MANAGE_PENDING_EVENTS_PAGE_SIZE = 20;
        private const int VIEW_REGISTERED_STUDENTS_PAGE_SIZE = 20;
        private const int VIEW_CHECKEDIN_STUDENTS_PAGE_SIZE = 20;
        private static readonly string[] PUBLIC_STATUSES = new string[] { EventStatusConstants.OPENING, EventStatusConstants.HAPPENING, EventStatusConstants.CLOSED };
        private static readonly string[] AFTER_STATUSES = new string[] { EventStatusConstants.HAPPENING, EventStatusConstants.CLOSED };

        [AllowAnonymous]
        public ActionResult Index() {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Authentication", new { ReturnUrl = "/Manager/" });
            else if (!User.IsManager())
                return View("Unauthorized");
            else {
                int pending = db.Events.Where(e => e.EventStatus.Equals(EventStatusConstants.PENDING)).Count();
                ViewBag.Pending = pending;
                ViewBag.Reports = db.Reports.Where(r => !r.IsDismissed && PUBLIC_STATUSES.Contains(r.Event.EventStatus)).Count();
                return View();
            }
        }

        public ActionResult ViewEventStatistics(int id) {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            IEnumerable<Register> registers = eventt.Registers.Where(r => r.User.UserProfile != null);
            List<string> registerDateLabels = new List<string>();
            List<DateTime> registerDates = new List<DateTime>();
            List<string> registerDateData = new List<string>();
            if (eventt.Registers.Count > 0) {
                DateTime registerTo = DateTime.Now;
                if (eventt.RegisterEndDate.HasValue && eventt.RegisterEndDate < registerTo)
                    registerTo = eventt.RegisterEndDate.Value;
                for (DateTime date = eventt.ApprovedDate.Value; date <= registerTo; date = date.AddDays(1)) {
                    registerDateLabels.Add(date.ToShortDateString());
                    registerDates.Add(date.Date);
                }
                foreach (DateTime date in registerDates)
                    registerDateData.Add(registers.Count(r => r.RegisteredDate.Date <= date).ToString());
            }
            int registerMale = 0;
            int registerFemale = 0;
            foreach (Register register in registers)
                if (register.User.UserProfile.Gender)
                    registerMale++;
                else
                    registerFemale++;
            List<string> registerCityLabels = registers.Select(r => r.User.UserProfile.City).Distinct().ToList();
            List<string> registerCityData = new List<string>();
            foreach (string city in registerCityLabels)
                registerCityData.Add(registers.Count(r => r.User.UserProfile.City.Equals(city)).ToString());
            List<string> registerCampusLabels = registers.Select(r => r.User.UserProfile.Campus).Distinct().ToList();
            List<string> registerCampusData = new List<string>();
            foreach (string campus in registerCampusLabels)
                registerCampusData.Add(registers.Count(r => r.User.UserProfile.Campus.Equals(campus)).ToString());
            List<string> registerMajorLabels = registers.Select(r => r.User.UserProfile.Major).Distinct().ToList();
            List<string> registerMajorData = new List<string>();
            foreach (string major in registerMajorLabels)
                registerMajorData.Add(registers.Count(r => r.User.UserProfile.Major.Equals(major)).ToString());
            List<string> registerSpecializationLabels = registers.Select(r => r.User.UserProfile.Specialization).Distinct().ToList();
            List<string> registerSpecializationData = new List<string>();
            foreach (string specialization in registerSpecializationLabels)
                registerSpecializationData.Add(registers.Count(r => r.User.UserProfile.Specialization.Equals(specialization)).ToString());
            if (!eventt.EventStatus.Equals(EventStatusConstants.OPENING)) {
                IEnumerable<Checkin> checkins = eventt.Checkins.Where(c => c.User.UserProfile != null);
                List<string> checkinDateLabels = new List<string>();
                List<DateTime> checkinDates = new List<DateTime>();
                List<string> checkinDateData = new List<string>();
                if (eventt.Checkins.Count > 0) {
                    DateTime checkinTo = DateTime.Now;
                    for (DateTime date = eventt.OpenDate; date <= checkinTo; date = date.AddDays(1)) {
                        checkinDateLabels.Add(date.ToShortDateString());
                        checkinDates.Add(date.Date);
                    }
                    foreach (DateTime date in checkinDates)
                        checkinDateData.Add(checkins.Count(c => c.CheckedinDate.Date <= date).ToString());
                }
                int checkinMale = 0;
                int checkinFemale = 0;
                foreach (Checkin checkin in checkins)
                    if (checkin.User.UserProfile.Gender)
                        checkinMale++;
                    else
                        checkinFemale++;
                List<string> checkinCityLabels = checkins.Select(c => c.User.UserProfile.City).Distinct().ToList();
                List<string> checkinCityData = new List<string>();
                foreach (string city in checkinCityLabels)
                    checkinCityData.Add(checkins.Count(c => c.User.UserProfile.City.Equals(city)).ToString());
                List<string> checkinCampusLabels = checkins.Select(c => c.User.UserProfile.Campus).Distinct().ToList();
                List<string> checkinCampusData = new List<string>();
                foreach (string campus in checkinCampusLabels)
                    checkinCampusData.Add(checkins.Count(c => c.User.UserProfile.Campus.Equals(campus)).ToString());
                List<string> checkinMajorLabels = checkins.Select(c => c.User.UserProfile.Major).Distinct().ToList();
                List<string> checkinMajorData = new List<string>();
                foreach (string major in checkinMajorLabels)
                    checkinMajorData.Add(checkins.Count(c => c.User.UserProfile.Major.Equals(major)).ToString());
                List<string> checkinSpecializationLabels = checkins.Select(c => c.User.UserProfile.Specialization).Distinct().ToList();
                List<string> checkinSpecializationData = new List<string>();
                foreach (string specialization in checkinSpecializationLabels)
                    checkinSpecializationData.Add(checkins.Count(c => c.User.UserProfile.Specialization.Equals(specialization)).ToString());
                ViewBag.Checkin = checkins.Count();
                ViewBag.CheckinDateLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinDateLabels));
                ViewBag.CheckinDateData = string.Format("[{0}]", string.Join(", ", checkinDateData));
                ViewBag.CheckinGenderData = string.Format("[{0}, {1}]", checkinMale, checkinFemale);
                ViewBag.CheckinCityLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinCityLabels));
                ViewBag.CheckinCityData = string.Format("[{0}]", string.Join(", ", checkinCityData));
                ViewBag.CheckinCampusLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinCampusLabels));
                ViewBag.CheckinCampusData = string.Format("[{0}]", string.Join(", ", checkinCampusData));
                ViewBag.CheckinMajorLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinMajorLabels));
                ViewBag.CheckinMajorData = string.Format("[{0}]", string.Join(", ", checkinMajorData));
                ViewBag.CheckinSpecializationLabels = string.Format("[\"{0}\"]", string.Join("\", \"", checkinSpecializationLabels));
                ViewBag.CheckinSpecializationData = string.Format("[{0}]", string.Join(", ", checkinSpecializationData));
            }
            else {
                ViewBag.Checkin = null;
                ViewBag.CheckinDateLabels = "[]";
                ViewBag.CheckinDateData = "[]";
                ViewBag.CheckinGenderData = "[]";
                ViewBag.CheckinCityLabels = "[]";
                ViewBag.CheckinCityData = "[]";
                ViewBag.CheckinCampusLabels = "[]";
                ViewBag.CheckinCampusData = "[]";
                ViewBag.CheckinMajorLabels = "[]";
                ViewBag.CheckinMajorData = "[]";
                ViewBag.CheckinSpecializationLabels = "[]";
                ViewBag.CheckinSpecializationData = "[]";
            }
            ViewBag.Event = eventt;
            ViewBag.Register = registers.Count();
            ViewBag.RegisterMax = eventt.RegisterMax;
            ViewBag.Rating = eventt.GetRating();
            ViewBag.Feedback = eventt.Feedbacks.Count + eventt.FeedbackOuters.Count;
            ViewBag.RegisterDateLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerDateLabels));
            ViewBag.RegisterDateData = string.Format("[{0}]", string.Join(", ", registerDateData));
            ViewBag.RegisterGenderData = string.Format("[{0}, {1}]", registerMale, registerFemale);
            ViewBag.RegisterCityLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerCityLabels));
            ViewBag.RegisterCityData = string.Format("[{0}]", string.Join(", ", registerCityData));
            ViewBag.RegisterCampusLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerCampusLabels));
            ViewBag.RegisterCampusData = string.Format("[{0}]", string.Join(", ", registerCampusData));
            ViewBag.RegisterMajorLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerMajorLabels));
            ViewBag.RegisterMajorData = string.Format("[{0}]", string.Join(", ", registerMajorData));
            ViewBag.RegisterSpecializationLabels = string.Format("[\"{0}\"]", string.Join("\", \"", registerSpecializationLabels));
            ViewBag.RegisterSpecializationData = string.Format("[{0}]", string.Join(", ", registerSpecializationData));
            if (eventt.Forms.Count > 0)
                ViewBag.FormResult = eventt.Forms.First().FormResult;
            return View();
        }

        public ActionResult ViewRegisteredStudents(int id, int? page, UserSearchViewModel model) {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null || !PUBLIC_STATUSES.Contains(eventt.EventStatus))
                return View("NotFound");
            model.Search = model.Search.ToUnsignString();
            IEnumerable<Register> dbregisters = db.Registers.Where(delegate (Register r) {
                return r.EventId == id && r.User.UserProfile != null && (string.IsNullOrEmpty(model.Search) || r.User.Email.ContainsIgnoreCase(model.Search) || string.Join(" ", r.User.UserProfile.FirstName.ToUnsignString(), r.User.UserProfile.LastName.ToUnsignString()).ContainsIgnoreCase(model.Search) || string.Join(" ", r.User.UserProfile.LastName.ToUnsignString(), r.User.UserProfile.FirstName.ToUnsignString()).ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel {
                Action = "ViewRegisteredStudents",
                Size = VIEW_REGISTERED_STUDENTS_PAGE_SIZE,
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbregisters.Count() / VIEW_REGISTERED_STUDENTS_PAGE_SIZE)
            };
            List<RegisterModel> registers = new List<RegisterModel>();
            foreach (Register register in dbregisters.OrderByDescending(r => r.User.Email).Skip((pages.Current - 1) * VIEW_REGISTERED_STUDENTS_PAGE_SIZE).Take(VIEW_REGISTERED_STUDENTS_PAGE_SIZE))
                registers.Add(new RegisterModel() {
                    Id = register.UserId,
                    Name = string.Join(" ", register.User.UserProfile.LastName, register.User.UserProfile.FirstName),
                    Email = register.User.Email,
                    RegisteredDate = register.RegisteredDate
                });
            ViewBag.EventId = id;
            ViewBag.Registers = registers;
            ViewBag.Pages = pages;
            return View(model);
        }

        public ActionResult ViewCheckedinStudents(int id, int? page, UserSearchViewModel model) {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null || (!AFTER_STATUSES.Contains(eventt.EventStatus)))
                return View("NotFound");
            model.Search = model.Search.ToUnsignString();
            IEnumerable<Checkin> dbcheckins = db.Checkins.Where(delegate (Checkin c) {
                return c.EventId == id && c.User.UserProfile != null && (string.IsNullOrEmpty(model.Search) || c.User.Email.ContainsIgnoreCase(model.Search) || string.Join(" ", c.User.UserProfile.FirstName.ToUnsignString(), c.User.UserProfile.LastName.ToUnsignString()).ContainsIgnoreCase(model.Search) || string.Join(" ", c.User.UserProfile.LastName.ToUnsignString(), c.User.UserProfile.FirstName.ToUnsignString()).ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel {
                Action = "ViewCheckedinStudents",
                Size = VIEW_REGISTERED_STUDENTS_PAGE_SIZE,
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbcheckins.Count() / VIEW_REGISTERED_STUDENTS_PAGE_SIZE)
            };
            List<CheckinModel> checkins = new List<CheckinModel>();
            foreach (Checkin checkin in dbcheckins.OrderByDescending(c => c.User.Email).Skip((pages.Current - 1) * VIEW_REGISTERED_STUDENTS_PAGE_SIZE).Take(VIEW_REGISTERED_STUDENTS_PAGE_SIZE))
                checkins.Add(new CheckinModel() {
                    Id = checkin.UserId,
                    Name = string.Join(" ", checkin.User.UserProfile.LastName, checkin.User.UserProfile.FirstName),
                    Email = checkin.User.Email,
                    CheckedinDate = checkin.CheckedinDate
                });
            ViewBag.EventId = id;
            ViewBag.Checkins = checkins;
            ViewBag.Pages = pages;
            return View(model);
        }

        public async Task<JsonResult> ExportEventStatistics(int id) {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null)
                return null;
            IEnumerable<Register> dbregisters = eventt.Registers.Where(r => r.User.UserProfile != null);
            IEnumerable<Checkin> dbcheckins = eventt.Checkins.Where(c => c.User.UserProfile != null);
            IEnumerable<Feedback> dbfeedbacks = eventt.Feedbacks.Where(f => f.User.UserProfile != null);
            IEnumerable<FeedbackOuter> dbfeedbackouters = eventt.FeedbackOuters;
            var info = new {
                name = eventt.EventName,
                registers = dbregisters.Count(),
                checkins = dbcheckins.Count(),
                feedbacks = dbfeedbacks.Count() + dbfeedbackouters.Count(),
                rating = eventt.GetRating()
            };
            var registers = dbregisters.Select(r => new { r.User.Email, r.User.UserProfile.FirstName, r.User.UserProfile.LastName, RegisteredDate = r.RegisteredDate.ToDateTimeString() }).ToList();
            var checkins = dbcheckins.Select(c => new { c.User.Email, c.User.UserProfile.FirstName, c.User.UserProfile.LastName, CheckedinDate = c.CheckedinDate.ToDateTimeString() }).ToList();
            var feedbacks = dbfeedbacks.Select(f => new { f.User.Email, f.User.UserProfile.FirstName, f.User.UserProfile.LastName, f.Value, f.FeedbackContent, CreatedDate = f.CreatedDate.ToDateTimeString() }).ToList();
            var feedbackOuter = dbfeedbackouters.Select(fo => new { fo.UserName, fo.Value, fo.FeedbackContent, CreatedDate = fo.CreatedDate.ToDateTimeString() }).ToList();
            string link = await AppScriptHelper.ExportEventStatistics(User.Identity.Name, new {
                info,
                registers,
                checkins,
                feedbacks,
                feedbackOuter,
                form = eventt.Forms.Count > 0 ? eventt.Forms.Single().FormLink : null
            });
            return Json(link, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ManageFeaturedEvents() {
            List<Event> events = db.Events.Where(x => x.EventStatus.Equals(EventStatusConstants.OPENING) || x.EventStatus.Equals(EventStatusConstants.HAPPENING)).OrderBy(x => x.CreatedDate).ToList();
            return View(events);
        }

        [HttpPost]
        public ActionResult ManageFeaturedEvents(FormCollection form) {
            string[] selectedItem = form["selectedItem"].Split(',');
            string[] eventID = form["eventID"].Split(',');
            int n = 0;
            for (int i = 0; i < selectedItem.Length; i++) {
                if (selectedItem[i].Equals("true")) {
                    int pos = i - n;
                    Event events = db.Events.Find(int.Parse(eventID[pos]));
                    events.IsFeatured = true;
                    n++;
                }
                else if (i >= 1 && selectedItem[i - 1].Equals("false")) {
                    int pos = i - n;
                    Event events = db.Events.Find(int.Parse(eventID[pos]));
                    events.IsFeatured = false;
                }
                else if (i == 0 && selectedItem[i].Equals("false")) {
                    Event events = db.Events.Find(int.Parse(eventID[0]));
                    events.IsFeatured = false;
                }
                db.SaveChanges();
            }
            List<Event> even = db.Events.Where(x => x.EventStatus.Equals(EventStatusConstants.OPENING) || x.EventStatus.Equals(EventStatusConstants.HAPPENING)).OrderBy(x => x.CreatedDate).ToList();
            this.SetMessage("Successfully updated feature events.");
            return View(even);
        }

        public ActionResult ViewAllCategories() {
            List<Category> category = db.Categories.Where(x => x.IsEnabled == true).ToList();
            return View(category);
        }

        public ActionResult CreateCategory() {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCategory(Category cat) {
            if (!ModelState.IsValid)
                return View();
            User user = db.Users.SingleOrDefault(u => u.Email.Equals(User.Identity.Name));
            cat.CreatorId = user.UserId;
            cat.CreatedDate = DateTime.Now;
            cat.IsEnabled = true;
            db.Categories.Add(cat);
            db.SaveChanges();
            this.SetMessage("Successfully created category");
            return RedirectToAction("ViewAllCategories");
        }

        public ActionResult EditCategory(int id) {
            Category category = db.Categories.SingleOrDefault(x => x.CategoryId == id);
            if (!category.IsEnabled)
                return RedirectToAction("ViewAllCategories");
            return View(category);
        }

        [HttpPost]
        public ActionResult EditCategory(Category cat) {
            if (!ModelState.IsValid)
                return View();
            if (!string.IsNullOrEmpty(cat.CategoryName)) {
                Category category = db.Categories.Find(cat.CategoryId);
                category.CategoryName = cat.CategoryName;
                category.CategoryDescription = cat.CategoryDescription;
                db.SaveChanges();
                this.SetMessage("Successfully editted category " + category.CategoryName);
                return RedirectToAction("ViewAllCategories");
            }
            return View(cat);
        }

        public ActionResult DeleteCategory(int id) {
            Category category = db.Categories.Find(id);
            category.IsEnabled = false;
            db.SaveChanges();
            this.SetMessage("Successfully deleted category " + category.CategoryName);
            return RedirectToAction("ViewAllCategories");
        }

        public ActionResult ManagePendingEvents2(int? page, EventSearchFilterViewModel model) {
            model.Search = model.Search.ToUnsignString();
            IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e) {
                return e.EventStatus.Equals(EventStatusConstants.PENDING) && (string.IsNullOrEmpty(model.Search) || e.EventName.ToUnsignString().ContainsIgnoreCase(model.Search) || e.EventDescription.ToUnsignString().ContainsIgnoreCase(model.Search) || (e.EventFamily != null && e.EventFamily.FamilyName.ToUnsignString().ContainsIgnoreCase(model.Search)) || e.EventPlace.ToUnsignString().ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel {
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbevents.Count() / MANAGE_PENDING_EVENTS_PAGE_SIZE)
            };
            List<EventModel> events = new List<EventModel>();
            foreach (Event eventt in dbevents.OrderBy(e => e.CreatedDate).Skip((pages.Current - 1) * MANAGE_PENDING_EVENTS_PAGE_SIZE).Take(MANAGE_PENDING_EVENTS_PAGE_SIZE)) {
                User user = db.Users.Where(u => u.UserId == eventt.OrganizerId).Single();
                Group group = db.Groups.Where(g => g.GroupId == eventt.GroupId && g.IsEnabled).SingleOrDefault();
                events.Add(new EventModel() {
                    Id = eventt.EventId,
                    Name = eventt.EventName,
                    CreatedDate = eventt.CreatedDate,
                    Organizer = new UserModel() {
                        Id = user.UserId,
                        FirstName = user.UserProfile.FirstName,
                        LastName = user.UserProfile.LastName
                    },
                    Group = group != null ? new GroupModel() {
                        Id = group.GroupId,
                        Name = group.GroupName
                    } : null
                });
            }
            ViewBag.Events = events;
            ViewBag.Pages = pages;
            return View(model);
        }

        public ActionResult AcceptEvent(int id) {
            Event eventt = db.Events.Find(id);
            if (eventt == null)
                return View("NotFound");
            eventt.ApprovedDate = DateTime.Now;
            eventt.ManagerId = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single().UserId;
            eventt.EventStatus = EventStatusConstants.OPENING;
            db.SaveChanges();
            eventt.Organizer.AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.ORGANIZER_EVENT_ACCEPT);
            this.SetMessage("Successfully accepted event");
            return RedirectToAction("ViewEvent", "Home", new { id = eventt.EventId });
        }

        public ActionResult RejectEvent(int id) {
            Event eventt = db.Events.Find(id);
            if (eventt == null)
                return View("NotFound");
            eventt.ApprovedDate = DateTime.Now;
            eventt.ManagerId = db.Users.Where(u => u.Email.Equals(User.Identity.Name)).Single().UserId;
            eventt.EventStatus = EventStatusConstants.REJECTED;
            db.SaveChanges();
            eventt.Organizer.AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.ORGANIZER_EVENT_REJECT);
            this.SetMessage("Successfully rejected event");
            return RedirectToAction("ManagePendingEvents");
        }

        public ActionResult DeleteGroup(int id) {
            Group grp = db.Groups.Find(id);
            grp.IsEnabled = false;
            db.SaveChanges();
            this.SetMessage("Successfully deleted group");
            return RedirectToAction("ViewAllGroups", "Home");
        }

        public ActionResult CreateGroup() {
            return View();
        }

        [HttpPost]
        public ActionResult CreateGroup(CreateGroupModel model) {
            if (model.FoundedDate.HasValue && (model.FoundedDate.Value < System.Data.SqlTypes.SqlDateTime.MinValue.Value || model.FoundedDate.Value > System.Data.SqlTypes.SqlDateTime.MaxValue.Value))
                ModelState.AddModelError("FoundedDate", "Founded date must be from " + System.Data.SqlTypes.SqlDateTime.MinValue.Value.ToString("MM/dd/yyyy") + " to " + System.Data.SqlTypes.SqlDateTime.MaxValue.Value.ToString("MM/dd/yyyy"));
            if (!ModelState.IsValid) {
                return View(model);
            }
            User leader = db.Users.SingleOrDefault(u => u.Email.ToLower().Equals(model.Leader.ToLower()));
            if (leader == null)
                ModelState.AddModelError("Leader", "Leader is not valid.");
            if (model.Image != null && (!ModelState.ContainsKey("Image") || ModelState["Image"].Errors.Count == 0)) {
                string image = model.Image.SaveImage("~/Images/Groups/");
                model.GroupImage = "/Images/Groups/" + image;
            }
            if (!ModelState.IsValid) {
                ViewBag.CurrentImage = model.GroupImage;
                return View(model);
            }
            Group group = new Group() {
                GroupName = model.Name,
                GroupDescription = model.Description,
                CreatedDate = DateTime.Now,
                IsEnabled = true,
                AssignedDate = DateTime.Now,
                ManagerId = User.GetUser().UserId,
                GroupMail = model.Mail,
                LeaderId = leader.UserId,
                GroupImage = model.GroupImage,
                FoundedYear = model.FoundedDate
            };
            db.Groups.Add(group);
            db.SaveChanges();
            int groupId = db.Groups.ToList().Last().GroupId;
            db.UserGroups.Add(new UserGroup() {
                OrganizerId = leader.UserId,
                GroupId = groupId,
                ParticipatedDate = DateTime.Now
            });
            db.SaveChanges();
            leader.AddNotification(null, group.GroupId, ObjectTypeConstants.GROUP, NotificationContentConstants.ORGANIZER_GROUP_LEADER);
            this.SetMessage("Successfully created group.");
            return RedirectToAction("ViewGroup/" + groupId, "Home");
        }

        [HttpPost]
        public JsonResult SearchOrganizers(string searchStr) {
            List<User> organizers = new Database().Users.Where(u => u.IsOrganizer == true && u.Email.ToUpper().Contains(searchStr.ToUpper())).ToList();
            List<SearchOrganizersModel> model = new List<SearchOrganizersModel>();
            foreach (User u in organizers) {
                model.Add(new SearchOrganizersModel() {
                    Id = u.UserId,
                    Email = u.Email
                });
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangeLeader(int groupId) {
            Group group = db.Groups.SingleOrDefault(g => g.GroupId == groupId && g.IsEnabled);
            if (group == null)
                return RedirectToAction("ViewAllGroups", "Home");
            List<MemberModel> groupMemberList = new List<MemberModel>();
            List<UserGroup> userGroupList = db.UserGroups.Where(u => u.GroupId == group.GroupId).ToList();
            foreach (UserGroup item in userGroupList) {
                if (item.User.IsOrganizer) {
                    groupMemberList.Add(new MemberModel() {
                        Id = item.OrganizerId,
                        Name = item.User.UserProfile.LastName + " " + item.User.UserProfile.FirstName,
                        Email = item.User.Email,
                        JoinedDate = item.ParticipatedDate
                    });
                }
            }
            groupMemberList.Remove(groupMemberList.Where(g => g.Id == group.LeaderId).SingleOrDefault());
            ViewBag.GroupMember = groupMemberList;
            return View(group);
        }

        [HttpPost]
        public ActionResult ChangeLeader(int groupId, int userId) {
            User user = db.Users.Where(u => u.UserId == userId).Single();
            Group targetGroup = db.Groups.Where(g => g.GroupId == groupId).Single();
            targetGroup.LeaderId = userId;
            db.SaveChanges();
            user.AddNotification(null, groupId, ObjectTypeConstants.GROUP, NotificationContentConstants.ORGANIZER_GROUP_LEADER);
            this.SetMessage("Successfully changed leader of" + targetGroup.GroupName + ".");
            return RedirectToAction("ViewGroup", "Home", new { id = groupId });
        }

        public ActionResult ViewAllReports() {
            List<Report> reports = db.Reports.Where(delegate (Report r) {
                return PUBLIC_STATUSES.Contains(r.Event.EventStatus) && (!r.IsDismissed || r.CreatedDate >= DateTime.Now.AddMonths(-1));
            }).OrderBy(r => r.IsDismissed).ToList();
            return View(reports);
        }

        [HttpPost]
        public ActionResult RejectReportedEvent(int eventId) {
            Event eventt = db.Events.Where(e => e.EventId == eventId).Single();
            eventt.EventStatus = EventStatusConstants.REJECTED;
            foreach (Report report in eventt.Reports)
                report.IsDismissed = true;
            db.SaveChanges();
            eventt.Organizer.AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.ORGANIZER_EVENT_REPORT);
            eventt.Registers.Select(r => r.User).AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.STUDENT_EVENT_REPORT);
            eventt.Reports.Where(r => r.EventId == eventt.EventId).Select(r => r.User).AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.STUDENT_REPORT_REJECT);
            this.SetMessage("Successfully rejected event");
            return RedirectToAction("ViewAllReports");
        }

        [HttpPost]
        public ActionResult DismissReport(int reportId) {
            Report report = db.Reports.Where(r => r.Id == reportId).Single();
            report.IsDismissed = true;
            db.SaveChanges();
            return RedirectToAction("ViewAllReports");
        }

        public ActionResult MoveToDraft(int id) {
            Event eventt = db.Events.Where(e => e.EventId == id).SingleOrDefault();
            if (eventt == null)
                return View("NotFound");
            eventt.EventStatus = EventStatusConstants.DRAFT;
            eventt.CreatedDate = DateTime.Now;
            db.SaveChanges();
            eventt.Organizer.AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.ORGANIZER_EVENT_REPORT);
            eventt.Registers.Select(r => r.User).AddNotification(null, eventt.EventId, ObjectTypeConstants.EVENT, NotificationContentConstants.STUDENT_EVENT_DRAFT);
            this.SetMessage("Successfully moved event to draft");
            return RedirectToAction("ViewEvent", "Home", new { id });
        }

        public ActionResult ManagePendingEvents(int? page, EventSearchFilterViewModel model)
        {
            model.Search = model.Search.ToUnsignString();
            IEnumerable<Event> dbevents = db.Events.Where(delegate (Event e) {
                return e.EventStatus.Equals(EventStatusConstants.PENDING) && (string.IsNullOrEmpty(model.Search)
                || e.EventName.ToUnsignString().ContainsIgnoreCase(model.Search)
                || e.EventDescription.ToUnsignString().ContainsIgnoreCase(model.Search)
                || (e.EventFamily != null && e.EventFamily.FamilyName.ToUnsignString().ContainsIgnoreCase(model.Search))
                || e.EventPlace.ToUnsignString().ContainsIgnoreCase(model.Search));
            });
            PageModel pages = new PageModel
            {
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Max = (int)Math.Ceiling(1.0 * dbevents.Count() / MANAGE_PENDING_EVENTS_PAGE_SIZE)
            };
            List<EventModel> events = new List<EventModel>();
            foreach (Event eventt in dbevents.OrderBy(e => e.CreatedDate)
                .Skip((pages.Current - 1) * MANAGE_PENDING_EVENTS_PAGE_SIZE)
                .Take(MANAGE_PENDING_EVENTS_PAGE_SIZE))
            {
                User user = db.Users.Where(u => u.UserId == eventt.OrganizerId).Single();
                Group group = db.Groups.Where(g => g.GroupId == eventt.GroupId && g.IsEnabled).SingleOrDefault();
                events.Add(new EventModel()
                {
                    Id = eventt.EventId,
                    Name = eventt.EventName,
                    CreatedDate = eventt.CreatedDate,
                    Organizer = new UserModel()
                    {
                        Id = user.UserId,
                        FirstName = user.UserProfile.FirstName,
                        LastName = user.UserProfile.LastName
                    },
                    Group = group != null ? new GroupModel()
                    {
                        Id = group.GroupId,
                        Name = group.GroupName
                    } : null
                });
            }
            ViewBag.Events = events;
            ViewBag.Pages = pages;
            return View("~/Views/Manager/ManagePendingEvents.cshtml", model);
        }

        private Database db;

        public ManagerController() {
            db = new Database();
        }

        public ManagerController(Database db) {
            this.db = db;
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}