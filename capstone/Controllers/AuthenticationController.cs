﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using capstone.Helpers;
using capstone.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace capstone.Controllers {
    [Authorize]
    public class AuthenticationController : Controller {
        private readonly string DOMAIN = "@fpt.edu.vn";
        private readonly string DEFAULT_PASSWORD = "swp493capstone";

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AuthenticationController() { }

        public AuthenticationController(ApplicationUserManager userManager, ApplicationSignInManager signInManager) {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager {
            get => _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            private set => _signInManager = value;
        }

        public ApplicationUserManager UserManager {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        [AllowAnonymous]
        public ActionResult Login(ExternalLoginListViewModel model) {
            Session["IsSignIn"] = true;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, ExternalLoginListViewModel model) {
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Authentication", model));
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(ExternalLoginListViewModel model) {
            ExternalLoginInfo loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
                return RedirectToAction("Login");
            else if (!loginInfo.Email.EndsWith(DOMAIN)) {
                TempData["IsOutside"] = true;
                return RedirectToAction("Login");
            }
            Session.Abandon();
            Session["IsSignIn"] = true;
            SignInStatus result = await SignInManager.ExternalSignInAsync(loginInfo, true);
            switch (result) {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.Failure:
                default:
                    if (ModelState.IsValid) {
                        ExternalLoginInfo info = await AuthenticationManager.GetExternalLoginInfoAsync();
                        if (info == null) {
                            return View("ExternalLoginFailure");
                        }
                        ApplicationUser user = await UserManager.FindByEmailAsync(loginInfo.Email);
                        if (user == null) {
                            user = new ApplicationUser { UserName = loginInfo.Email, Email = loginInfo.Email };
                            IdentityResult result2 = await UserManager.CreateAsync(user, DEFAULT_PASSWORD);
                            if (result2.Succeeded) {
                                result2 = await UserManager.AddLoginAsync(user.Id, info.Login);
                                if (result2.Succeeded) {
                                    User.SetUser(loginInfo);
                                    await SignInManager.SignInAsync(user, true, false);
                                    return RedirectToLocal(model.ReturnUrl);
                                }
                            }
                            AddErrors(result2);
                        }
                        else {
                            await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                            await SignInManager.SignInAsync(user, true, false);
                            return RedirectToLocal(model.ReturnUrl);
                        }
                    }
                    return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout() {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure() {
            return View();
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (_userManager != null) {
                    _userManager.Dispose();
                    _userManager = null;
                }
                if (_signInManager != null) {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }
            base.Dispose(disposing);
        }

        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private void AddErrors(IdentityResult result) {
            foreach (string error in result.Errors)
                ModelState.AddModelError("", error);
        }

        private ActionResult RedirectToLocal(string returnUrl) {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult FbLogin(string fbToken, FacebookModel fbUser) {
            Session["fbToken"] = fbToken;
            Session["fbUSer"] = fbUser;
            return Json(fbToken, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult FbLogout() {
            Session["fbToken"] = null;
            Session["fbUSer"] = null;
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult {
            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null) { }

            public ChallengeResult(string provider, string redirectUri, string userId) {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public override void ExecuteResult(ControllerContext context) {
                AuthenticationProperties properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                    properties.Dictionary[XsrfKey] = UserId;
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
    }
}