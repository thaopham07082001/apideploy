﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using capstone.Constants;
using capstone.database.Database;
using capstone.database.Entities;
using capstone.Helpers;
using capstone.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace capstone.Controllers
{
    [Authorize(Roles = "Administrator")]
    [CheckUserRole]
    public class AdminController : Controller
    {
        private const int VIEW_ALL_ACCOUNTS_PAGE_SIZE = 20;
        private readonly string LOGIN_AS_ACCOUNT_PASSWORD = "swp493capstone";

        [AllowAnonymous]
        public ActionResult Index()
        {

            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Authentication", new { ReturnUrl = "/Admin/" });
            else if (!User.IsAdmin())
                return View("Unauthorized");
            else
                return View();
        }

        public ActionResult ViewAllAccounts(int? page, AccountSearchFilterViewModel model)
        {
            if (!ModelState.IsValid)
                return View("Error");
            if (!model.IsOrganizer && !model.IsManager && !model.IsAdmin)
            {
                model.IsOrganizer = true;
                model.IsManager = true;
                model.IsAdmin = true;
            }
            model.Search = model.Search.ToUnsignString();
            IEnumerable<User> dbusers = db.Users.Where(delegate (User u)
            {
                return !u.Email.Equals(User.Identity.Name) && (string.IsNullOrEmpty(model.Search) || u.Email.ContainsIgnoreCase(model.Search) || (u.UserProfile != null && (string.Join(" ", u.UserProfile.FirstName.ToUnsignString(), u.UserProfile.LastName.ToUnsignString()).ContainsIgnoreCase(model.Search) || string.Join(" ", u.UserProfile.LastName.ToUnsignString(), u.UserProfile.FirstName.ToUnsignString()).ContainsIgnoreCase(model.Search)))) && ((model.IsOrganizer && u.IsOrganizer == true) || (model.IsManager && u.IsManager == true) || (model.IsAdmin && u.IsAdmin == true));
            });
            PageModel pages = new PageModel
            {
                Action = "ViewAllAccounts",
                Current = page.HasValue ? Math.Max(page.Value, 1) : 1,
                Size = VIEW_ALL_ACCOUNTS_PAGE_SIZE,
                Max = Convert.ToInt32(Math.Ceiling(1.0 * dbusers.Count() / VIEW_ALL_ACCOUNTS_PAGE_SIZE))
            };
            List<AccountModel> accounts = new List<AccountModel>();
            foreach (User user in dbusers.OrderBy(u => u.Email).Skip((pages.Current - 1) * VIEW_ALL_ACCOUNTS_PAGE_SIZE).Take(VIEW_ALL_ACCOUNTS_PAGE_SIZE))
                accounts.Add(new AccountModel()
                {
                    Id = user.UserId,
                    Email = user.Email,
                    Name = user.UserProfile != null ? string.Join(" ", user.UserProfile.LastName, user.UserProfile.FirstName) : user.Email,
                    IsStudent = user.IsStudent,
                    IsOrganizer = user.IsOrganizer,
                    IsManager = user.IsManager,
                    IsAdmin = user.IsAdmin
                });
            ViewBag.Accounts = accounts;
            ViewBag.Pages = pages;
            return View(model);
        }

        public ActionResult CreateAccount()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAccount(CreateAccountViewModel model)
        {
            if (string.IsNullOrEmpty(model.Email) || !model.Email.EndsWith("@fpt.edu.vn"))
                ModelState.AddModelError("Email", "You must enter @fpt.edu.vn email.");
            if (!model.IsStudent && !model.IsOrganizer && !model.IsManager && !model.IsAdmin)
                ModelState.AddModelError("Roles", "You must select at least 1 role.");
            if (!ModelState.IsValid)
                return View(model);
            User user = db.Users.Where(u => u.Email.ToLower().Equals(model.Email.ToLower())).SingleOrDefault();
            if (user == null)
            {
                user = db.Users.Create();
                user.Email = model.Email;
                user.IsEnabled = true;
                user.NotificationSeenDate = DateTime.Now;
                db.Users.Add(user);
            }
            else if (user.Email.Equals(User.Identity.Name))
            {
                ModelState.AddModelError("Email", "You cannot change your own role.");
                return View(model);
            }
            else if (user.IsOrganizer || user.IsManager || user.IsAdmin)
            {
                ModelState.AddModelError("Email", "An account with this email already existed.");
                return View(model);
            }
            user.IsStudent = model.IsStudent;
            user.IsOrganizer = model.IsOrganizer;
            user.IsManager = model.IsManager;
            user.IsAdmin = model.IsAdmin;
            UserManager<ApplicationUser> users = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user2 = users.FindByEmail(user.Email);
            if (user2 != null)
            {
                if (user.IsStudent)
                    users.AddToRole(user2.Id, UserRoleConstants.STUDENT);
                else
                    users.RemoveFromRole(user2.Id, UserRoleConstants.STUDENT);
                if (user.IsOrganizer)
                    users.AddToRole(user2.Id, UserRoleConstants.ORGANIZER);
                else
                    users.RemoveFromRole(user2.Id, UserRoleConstants.ORGANIZER);
                if (user.IsManager)
                    users.AddToRole(user2.Id, UserRoleConstants.MANAGER);
                else
                    users.RemoveFromRole(user2.Id, UserRoleConstants.MANAGER);
                if (user.IsAdmin)
                    users.AddToRole(user2.Id, UserRoleConstants.ADMIN);
                else
                    users.RemoveFromRole(user2.Id, UserRoleConstants.ADMIN);
            }
            db.SaveChanges();
            user.AddNotification(null, null, null, NotificationContentConstants.USER_ROLE_CHANGE);
            this.SetMessage("Successfully created account");
            return RedirectToAction("ViewAllAccounts");
        }

        public ActionResult EditAccount(int id)
        {
            EditAccountViewModel model = null;
            User user = db.Users.Where(u => u.UserId == id).SingleOrDefault();
            if (user == null)
                return View("NotFound");
            model = new EditAccountViewModel()
            {
                Id = user.UserId,
                Email = user.Email,
                IsStudent = user.IsStudent,
                IsOrganizer = user.IsOrganizer,
                IsManager = user.IsManager,
                IsAdmin = user.IsAdmin
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAccount(EditAccountViewModel model)
        {
            if (!model.IsStudent && !model.IsOrganizer && !model.IsManager && !model.IsAdmin)
                ModelState.AddModelError("Roles", "You must select at least 1 role");
            if (!ModelState.IsValid)
                return View(model);
            User user = db.Users.Where(u => u.UserId == model.Id).SingleOrDefault();
            if (user == null)
                return View("NotFound");
            user.IsStudent = model.IsStudent;
            user.IsOrganizer = model.IsOrganizer;
            user.IsManager = model.IsManager;
            if (model.IsAdmin == true || model.Id == User.GetUser().UserId)
                user.IsAdmin = model.IsAdmin;
            UserManager<ApplicationUser> users = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user2 = users.FindByEmail(user.Email);
            if (user2 != null)
            {
                if (user.IsStudent)
                    users.AddToRole(user2.Id, UserRoleConstants.STUDENT);
                else
                    users.RemoveFromRole(user2.Id, UserRoleConstants.STUDENT);
                if (user.IsOrganizer)
                    users.AddToRole(user2.Id, UserRoleConstants.ORGANIZER);
                else
                    users.RemoveFromRole(user2.Id, UserRoleConstants.ORGANIZER);
                if (user.IsManager)
                    users.AddToRole(user2.Id, UserRoleConstants.MANAGER);
                else
                    users.RemoveFromRole(user2.Id, UserRoleConstants.MANAGER);
            }
            db.SaveChanges();
            user.AddNotification(null, null, null, NotificationContentConstants.USER_ROLE_CHANGE);
            this.SetMessage("Successfully edited account");
            return RedirectToAction("ViewAllAccounts");
        }

        public ActionResult LoginAsAccount()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> LoginAsAccount(string email)
        {

            UserManager<ApplicationUser> users = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            SignInManager<ApplicationUser, string> signin = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            IAuthenticationManager authentication = HttpContext.GetOwinContext().Authentication;
            Session.Abandon();
            authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            ApplicationUser user = users.FindByEmail(email);
            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = email,
                    Email = email
                };
                users.Create(user, LOGIN_AS_ACCOUNT_PASSWORD);
            }
            User.SetUser(new ExternalLoginInfo()
            {
                Email = email
            });
            Session["IsSignIn"] = true;
            await signin.SignInAsync(user, false, false);
            return RedirectToAction("Index", "Home");
        }

        //begin Campus, Building and TV CRUD func - hieunqhe153633 - Spring 2023
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateCampus(string campusName)
        {
            var campuses = db.Campuses.ToList();
            foreach (var c in campuses)
            {
                if (campusName.ToLower().Equals(c.CampusName.ToLower()))
                {
                    this.SetMessage("Campus duplicate!");
                    return RedirectToAction("ViewAllCampus");
                }
            }
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                var campus = new Campus { CampusName = campusName };
                db.Campuses.Add(campus);
                await db.SaveChangesAsync();
                //set notification here

                this.SetMessage("Successfully created campus");
                return RedirectToAction("ViewAllCampus");
            }
        }

        [HttpGet]
        public ActionResult ViewAllCampus()
        {
            var campuses = db.Campuses.ToList();
            var campusesView = new List<CampusViewModel>();
            foreach (Campus campus in campuses)
            {
                CampusViewModel cam = new CampusViewModel { CampusId = campus.CampusId, CampusName = campus.CampusName };
                campusesView.Add(cam);
            }
            ViewBag.Campus = campusesView;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCampus(int campusId, string campusName)
        {
            var campuses = db.Campuses.ToList();
            foreach (var c in campuses)
            {
                if (campusName.ToLower().Equals(c.CampusName.ToLower()))
                {
                    this.SetMessage("Campus duplicate!");
                    return RedirectToAction("ViewAllCampus");
                }
            }
            if (!ModelState.IsValid)
            {
                return RedirectToAction("ViewAllCampus");
            }
            else
            {
                var campus = await db.Campuses.FindAsync(campusId);
                if (campus == null)
                {
                    this.SetMessage("Campus not found");
                    return RedirectToAction("ViewAllCampus");
                }
                else
                {
                    campus.CampusName = campusName;
                    await db.SaveChangesAsync();
                    //set notification here

                    this.SetMessage("Successfully update campus");
                    return RedirectToAction("ViewAllCampus");
                }
            }
        }

        //Hard delete, update soft delete later
        public ActionResult DeleteCampus(int id)
        {
            var campus = db.Campuses.Find(id);
            if (campus == null)
            {
                this.SetMessage("Campus not found");
                return RedirectToAction("ViewAllCampus");
            }
            else
            {
                db.Campuses.Remove(campus);
                db.SaveChanges();
                this.SetMessage("Successfully delete campus");
                return RedirectToAction("ViewAllCampus");
            }
        }

        [HttpGet]
        public ActionResult ViewAllBuilding()
        {
            var buidlings = db.Buildings.Include("Campus").ToList();
            var buildingsView = new List<BuildingViewModel>();
            foreach (Building bd in buidlings)
            {
                CampusViewModel campusViewModel = new CampusViewModel { CampusId = bd.Campus.CampusId, CampusName = bd.Campus.CampusName };
                BuildingViewModel cam = new BuildingViewModel { BuildingId = bd.BuildingId, BuildingName = bd.BuildingName, CampusId = bd.CampusId, Campus = campusViewModel };
                buildingsView.Add(cam);
            }
            ViewBag.building = buildingsView;
            var campuses = db.Campuses.ToList();
            var CampusViews = new List<CampusViewModel>();
            foreach (Campus c in campuses)
            {
                CampusViews.Add(new CampusViewModel { CampusId = c.CampusId, CampusName = c.CampusName });
            }
            ViewBag.campus = CampusViews;
            return View();
        }

        [HttpPost]
        public ActionResult ViewAllBuilding(int campusId)
        {
            var buidlings = new List<Building>();
            if (campusId == -1)
            {
                buidlings = db.Buildings.Include("Campus").ToList();
            }
            else
            {
                buidlings = db.Buildings.Include("Campus").Where(x => x.CampusId == campusId).ToList();
            }
            var buildingsView = new List<BuildingViewModel>();
            foreach (Building bd in buidlings)
            {
                CampusViewModel campusViewModel = new CampusViewModel { CampusId = bd.Campus.CampusId, CampusName = bd.Campus.CampusName };
                BuildingViewModel cam = new BuildingViewModel { BuildingId = bd.BuildingId, BuildingName = bd.BuildingName, CampusId = bd.CampusId, Campus = campusViewModel };
                buildingsView.Add(cam);
            }
            ViewBag.building = buildingsView;
            var campuses = db.Campuses.ToList();
            var CampusViews = new List<CampusViewModel>();
            foreach (Campus c in campuses)
            {
                CampusViews.Add(new CampusViewModel { CampusId = c.CampusId, CampusName = c.CampusName });
            }
            ViewBag.campus = CampusViews;
            return View();
        }

        //Hard delete, update soft delete later
        public ActionResult DeleteBuilding(int id)
        {
            var bd = db.Buildings.Find(id);
            if (bd == null)
            {
                this.SetMessage("Building not found");
                return RedirectToAction("ViewAllBuilding");
            }
            else
            {
                db.Buildings.Remove(bd);
                db.SaveChanges();
                this.SetMessage("Successfully delete building");
                return RedirectToAction("ViewAllBuilding");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateBuilding(int campusId, string building)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                var checkCampus = db.Campuses.Where(x => x.CampusId == campusId).FirstOrDefault();
                if (checkCampus == null || building == null || building == "")
                {
                    this.SetMessage("Please choose campus and enter campus name");
                    return RedirectToAction("ViewAllBuilding");
                }
                var buildings = db.Buildings.Where(x => x.CampusId == campusId).ToList();
                foreach (Building b in buildings)
                {
                    if (building.ToLower().Equals(b.BuildingName.ToLower()))
                    {
                        this.SetMessage("Campus has been existed");
                        return RedirectToAction("ViewAllBuilding");
                    }
                }
                var bd = new Building { BuildingName = building, CampusId = campusId };
                db.Buildings.Add(bd);
                await db.SaveChangesAsync();
                //set notification here

                this.SetMessage("Successfully created building");
                return RedirectToAction("ViewAllBuilding");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditBuilding(int buildingId, string txtbd, int campusId)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("ViewAllBuilding");
            }
            if (txtbd == "" || txtbd == null)
            {
                this.SetMessage("Please input building name");
                return RedirectToAction("ViewAllBuilding");
            }
            var bds = db.Buildings.Where(x => x.CampusId == campusId).ToList();
            foreach (var b in bds)
            {
                if (txtbd.ToLower().Equals(b.BuildingName.ToLower()))
                {
                    this.SetMessage("Duplicate building!");
                    return RedirectToAction("ViewAllBuilding");
                }
            }
            var bd = await db.Buildings.FindAsync(buildingId);
            if (bd == null)
            {
                this.SetMessage("Building not found");
                return RedirectToAction("ViewAllBuilding");
            }
            else
            {
                bd.BuildingName = txtbd;
                bd.CampusId = campusId;
                await db.SaveChangesAsync();
                //set notification here

                this.SetMessage("Successfully update building");
                return RedirectToAction("ViewAllBuilding");
            }
        }

        [HttpGet]
        public ActionResult ViewAllSmartTv()
        {
            var tvs = db.SmartTVs.ToList();
            var tvView = new List<SmartTvViewModel>();
            var campusViews = new List<CampusViewModel>();
            var buildingViews = new List<BuildingViewModel>();
            foreach (SmartTV tv in tvs)
            {
                var campus = new CampusViewModel { CampusId = tv.Building.Campus.CampusId, CampusName = tv.Building.Campus.CampusName };
                var bd = new BuildingViewModel { BuildingId = tv.BuildingId, BuildingName = tv.Building.BuildingName, Campus = campus, CampusId = tv.BuildingId };
                SmartTvViewModel cam = new SmartTvViewModel { id = tv.TvId, TvCodeId = tv.TvCodeId, TvName = tv.TvName, Building = bd, BuildingId = bd.BuildingId };
                tvView.Add(cam);
            }
            var campuses = db.Campuses.ToList();
            var buildings = db.Buildings.ToList();
            foreach (var c in campuses)
            {
                var campusView = new CampusViewModel { CampusId = c.CampusId, CampusName = c.CampusName };
                campusViews.Add(campusView);
            }
            foreach (var b in buildings)
            {
                var buildingView = new BuildingViewModel { BuildingId = b.BuildingId, BuildingName = b.BuildingName, CampusId = b.CampusId };
                buildingViews.Add(buildingView);
            }
            ViewBag.TV = tvView;
            ViewBag.campus = campusViews;
            ViewBag.building = buildingViews;
            return View();
        }

        [HttpPost]
        public ActionResult ViewAllSmartTv(int? searchCampus, string searchterm)
        {
            var tvs = db.SmartTVs.ToList();
            var tvView = new List<SmartTvViewModel>();
            var campusViews = new List<CampusViewModel>();
            var buildingViews = new List<BuildingViewModel>();
            foreach (SmartTV tv in tvs)
            {
                if (searchterm == null || searchterm == "")
                {
                    if (searchCampus == -1 || searchCampus == null)
                    {
                        var campus = new CampusViewModel { CampusId = tv.Building.Campus.CampusId, CampusName = tv.Building.Campus.CampusName };
                        var bd = new BuildingViewModel { BuildingId = tv.BuildingId, BuildingName = tv.Building.BuildingName, Campus = campus, CampusId = tv.BuildingId };
                        SmartTvViewModel cam = new SmartTvViewModel { id = tv.TvId, TvCodeId = tv.TvCodeId, TvName = tv.TvName, Building = bd, BuildingId = bd.BuildingId };
                        tvView.Add(cam);
                    }
                    else
                    {
                        if (tv.Building.CampusId == searchCampus)
                        {
                            var campus = new CampusViewModel { CampusId = tv.Building.Campus.CampusId, CampusName = tv.Building.Campus.CampusName };
                            var bd = new BuildingViewModel { BuildingId = tv.BuildingId, BuildingName = tv.Building.BuildingName, Campus = campus, CampusId = tv.BuildingId };
                            SmartTvViewModel cam = new SmartTvViewModel { id = tv.TvId, TvCodeId = tv.TvCodeId, TvName = tv.TvName, Building = bd, BuildingId = bd.BuildingId };
                            tvView.Add(cam);
                        }
                    }
                }
                else
                {
                    if (tv.TvCodeId.ToLower().Contains(searchterm.ToLower()))
                    {
                        var campus = new CampusViewModel { CampusId = tv.Building.Campus.CampusId, CampusName = tv.Building.Campus.CampusName };
                        var bd = new BuildingViewModel { BuildingId = tv.BuildingId, BuildingName = tv.Building.BuildingName, Campus = campus, CampusId = tv.BuildingId };
                        SmartTvViewModel cam = new SmartTvViewModel { id = tv.TvId, TvCodeId = tv.TvCodeId, TvName = tv.TvName, Building = bd, BuildingId = bd.BuildingId };
                        tvView.Add(cam);
                    }
                }

            }
            var campuses = db.Campuses.ToList();
            var buildings = db.Buildings.ToList();
            foreach (var c in campuses)
            {
                var campusView = new CampusViewModel { CampusId = c.CampusId, CampusName = c.CampusName };
                campusViews.Add(campusView);
            }
            foreach (var b in buildings)
            {
                var buildingView = new BuildingViewModel { BuildingId = b.BuildingId, BuildingName = b.BuildingName, CampusId = b.CampusId };
                buildingViews.Add(buildingView);
            }
            ViewBag.TV = tvView;
            ViewBag.campus = campusViews;
            ViewBag.building = buildingViews;
            return View();
        }

        //Method to real-time load building when choosing campus from a dropdown
        [HttpPost]
        public JsonResult GetBuildingByCampusId(int campId)
        {
            List<BuildingViewModel> buildings = new List<BuildingViewModel>();
            foreach (Building b in new Database().Buildings)
            {
                if (b.CampusId == campId)
                {
                    buildings.Add(new BuildingViewModel()
                    {
                        BuildingId = b.BuildingId,
                        BuildingName = b.BuildingName
                    });
                }
            }
            return Json(buildings, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateSmartTv(int campusId, int buildingId, string tvCode, string tvName)
        {
            //if (!ModelState.IsValid)
            //{
            //    return RedirectToAction("ViewAllSmartTv");
            //}
            //else
            //{
            if (tvCode == null || tvCode == "" || tvName == null || tvName == "" || buildingId == -1 || campusId == -1)
            {
                this.SetMessage("Please enter data");
                return RedirectToAction("ViewAllSmartTv");
            }
            var tvs = db.SmartTVs.Where(x => x.BuildingId == buildingId).ToList();
            foreach (var t in tvs)
            {
                if (t.TvCodeId.ToLower().Equals(tvCode.ToLower()))
                {
                    this.SetMessage("Duplicate TV!");
                    return RedirectToAction("ViewAllSmartTv");
                }
            }
            var tv = new SmartTV { TvCodeId = tvCode, TvName = tvName, BuildingId = buildingId };
            db.SmartTVs.Add(tv);
            await db.SaveChangesAsync();
            //set notification here

            this.SetMessage("Successfully created tv");
            return RedirectToAction("ViewAllSmartTv");
            // }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditSmartTv(int campusId, int buildingId, string tvCode, string tvName, int tvId)
        {
            //if (!ModelState.IsValid)
            //{
            //    return RedirectToAction("ViewAllSmartTv");
            //}
            //else
            //{
            if (tvCode == null || tvCode == "" || tvName == null || tvName == "" || buildingId == -1 || campusId == -1)
            {
                this.SetMessage("Please enter data");
                return RedirectToAction("ViewAllSmartTv");
            }
            var tv = await db.SmartTVs.FindAsync(tvId);
            if (tv == null)
            {
                this.SetMessage("Tv not found");
                return RedirectToAction("ViewAllSmartTv");
            }
            var tvs = db.SmartTVs.Where(x => x.BuildingId == buildingId).ToList();
            foreach (var t in tvs)
            {
                if (t.TvId != tvId)
                {
                    if (t.TvCodeId.ToLower().Equals(tvCode.ToLower()))
                    {
                        this.SetMessage("Duplicate TV!");
                        return RedirectToAction("ViewAllSmartTv");
                    }
                }
            }
            tv.TvCodeId = tvCode;
            tv.TvName = tvName;
            tv.BuildingId = buildingId;
            await db.SaveChangesAsync();
            //set notification here

            this.SetMessage("Successfully update TV");
            return RedirectToAction("ViewAllSmartTv");
            // }
        }

        public ActionResult DeleteSmartTv(int id)
        {
            var tv = db.SmartTVs.Find(id);
            if (tv == null)
            {
                this.SetMessage("TV not found");
                return RedirectToAction("ViewAllSmartTv");
            }
            else
            {
                db.SmartTVs.Remove(tv);
                db.SaveChanges();
                this.SetMessage("Successfully delete TV");
                return RedirectToAction("ViewAllSmartTv");
            }
        }

        //begin Campus, Building and TV CRUD func - hieunqhe153633 - Spring 2023

        private Database db;

        public AdminController()
        {
            db = new Database();
        }

        public AdminController(Database db)
        {
            this.db = db;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}