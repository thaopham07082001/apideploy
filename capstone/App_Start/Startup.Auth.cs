﻿using System;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;
using capstone.Constants;
using capstone.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Newtonsoft.Json.Linq;
using Owin;

namespace capstone {
    public partial class Startup {
        private const string DEFAULT_LAST_NAME = "FPT";

        public void ConfigureAuth(IAppBuilder app) {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Authentication/Login"),
                Provider = new CookieAuthenticationProvider {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(0),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions() {
                ClientId = ConfigurationManager.AppSettings["GoogleId"],
                ClientSecret = ConfigurationManager.AppSettings["GoogleSecret"],
                Provider = new GoogleOAuth2AuthenticationProvider() {
                    OnAuthenticated = delegate (GoogleOAuth2AuthenticatedContext context) {
                        context.Identity.AddClaim(new Claim("Email", context.User["email"].Value<string>()));
                        context.Identity.AddClaim(new Claim("FirstName", context.User["given_name"].Value<string>()));
                        context.Identity.AddClaim(new Claim("LastName", DEFAULT_LAST_NAME));
                        context.Identity.AddClaim(new Claim("ProfileImage", context.User["picture"].Value<string>()));
                        return Task.FromResult(0);
                    }
                }
            });
            ConfigureRoles();
        }

        private void ConfigureRoles() {
            ApplicationDbContext context = new ApplicationDbContext();
            RoleManager<IdentityRole> roles = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            UserManager<ApplicationUser> users = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            if (!roles.RoleExists(UserRoleConstants.STUDENT))
                roles.Create(new IdentityRole {
                    Name = UserRoleConstants.STUDENT
                });
            if (!roles.RoleExists(UserRoleConstants.ORGANIZER))
                roles.Create(new IdentityRole {
                    Name = UserRoleConstants.ORGANIZER
                });
            if (!roles.RoleExists(UserRoleConstants.MANAGER))
                roles.Create(new IdentityRole {
                    Name = UserRoleConstants.MANAGER
                });
            if (!roles.RoleExists(UserRoleConstants.ADMIN))
                roles.Create(new IdentityRole {
                    Name = UserRoleConstants.ADMIN
                });
        }
    }
}