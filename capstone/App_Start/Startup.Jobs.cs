﻿using capstone.Jobs;
using capstone.Models;
using Hangfire;
using Owin;

namespace capstone {
    public partial class Startup {
        public void ConfigureJobs(IAppBuilder app) {
            ApplicationJobContext db = new ApplicationJobContext();

            GlobalConfiguration.Configuration.UseSqlServerStorage(db.Database.Connection.ConnectionString);
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate<EventStatusUpdateJob>(
                "Event Status Update Job",
                x => x.UpdateEventStatus(),
                Cron.Minutely);
            RecurringJob.AddOrUpdate<EventStartNotificationJob>(
                "Event Start Notification Job",
                x => x.NotifyEvent(),
                Cron.Minutely);
        }
    }
}