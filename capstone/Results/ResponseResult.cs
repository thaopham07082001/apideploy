﻿namespace capstone.Results
{
    public class ResponseResult
    {
        public string status { get; set; }
        public StatusCode code { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }

    public enum StatusCode
    {
        Success = 200,
        CreateSuccess = 201,
        RequestTimeout = 408,
        BadRequest = 400,
        NotFound = 404,
        Unauthorized = 401,
        InternalServerError = 500,
    }
}