﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using capstone.database.Entities;
using capstone.Models;
using capstone.test.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace capstone.Controllers.Tests {
    [TestClass]
    public class HomeControllerTests {
        [TestMethod]
        public void ViewUniversityEventCalendarTest() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Draft",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",

                };
                //action
                HomeController controller = new HomeController(db);
                controller.ModelState.AddModelError("key", "error message");
                controller.ControllerContext = mockContext.Object;
                ViewResult result = (ViewResult)controller.ViewUniversityEventCalendar(2, 2019, null, input);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewUniversityEventCalendarTest1() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",

                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewUniversityEventCalendar(2, 2019, null, input);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewUniversityEventCalendarTest2() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",

                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewUniversityEventCalendar(2, 2019, new System.DateTime(2017, 1, 7), input);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewUniversityEventCalendarTest3() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",
                    Category = 1
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewUniversityEventCalendar(2, 2019, new System.DateTime(2017, 1, 7), input);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewUniversityEventCalendarTest4() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    IsEnabled = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8)
                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",
                    Category = 1,
                    IsGroup = false,
                    OrganizerOrGroup = 15
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewUniversityEventCalendar(2, 2019, new System.DateTime(2017, 1, 7), input);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewUniversityEventCalendarTest5() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",
                    IsGroup = true,
                    OrganizerOrGroup = 1
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewUniversityEventCalendar(2, 2019, new System.DateTime(2017, 1, 7), input);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewAllEventsTest() {

            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Draft"
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewAllEvents(1, input);
                Assert.AreEqual("Status not public or not available", result.ViewData.ModelState["Status"].Errors[0].ErrorMessage);
            }
        }

        [TestMethod]
        public void ViewAllEventsTest1() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Draft"
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewAllEvents(1, input);
                Assert.AreEqual("Status not public or not available", result.ViewData.ModelState["Status"].Errors[0].ErrorMessage);
            }
        }

        [TestMethod]
        public void ViewAllEventsTest2() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening"
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewAllEvents(1, input);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewAllEventsTest3() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",
                    Display = "Not"
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewAllEvents(1, input);
                //assert
                Assert.AreEqual("Error", result.ViewName);
            }
        }

        [TestMethod]
        public void ViewAllEventsTest4() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    IsEnabled = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",
                    OrganizerOrGroup = 1,
                    IsGroup = true
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewAllEvents(1, input);
                //assert
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void ViewAllEventsTest5() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    IsEnabled = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1" });
                Event event1 = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),

                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //input
                EventSearchFilterViewModel input = new EventSearchFilterViewModel() {
                    Status = "Opening",

                    OrganizerOrGroup = 15,
                    IsGroup = false
                };
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewAllEvents(1, input);
                //assert
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void GetAllCategoriesTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1", IsEnabled = true });
            db.Categories.Add(new Category() { CategoryId = 2, CategoryName = "abc", IsEnabled = true });
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllCategories("abc");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Categories"];
            Assert.AreEqual(1, data.Count);
        }

        [TestMethod]
        public void GetAllCategoriesTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1", IsEnabled = true });
            db.Categories.Add(new Category() { CategoryId = 2, CategoryName = "abc", IsEnabled = false });
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllCategories("abc");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Categories"];
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void GetAllCategoriesTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Categories.Add(new Category() { CategoryId = 1, CategoryName = "cat1", IsEnabled = true });
            db.Categories.Add(new Category() { CategoryId = 2, CategoryName = "abc", IsEnabled = false });
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllCategories("abcd");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Categories"];
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void GetAllOrganizersAndGroupsTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllOrganizersAndGroups("abcd");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Organizers"];
            Assert.AreEqual(0, data.Count);
            List<SelectListItem> data2 = (List<SelectListItem>)result.ViewData["Groups"];
            Assert.AreEqual(0, data2.Count);
        }

        [TestMethod]
        public void GetAllOrganizersAndGroupsTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennhabcd@gmail.com",
                IsOrganizer = true,
                IsEnabled = true
            };
            db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllOrganizersAndGroups("abcd");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Organizers"];
            Assert.AreEqual(1, data.Count);
            List<SelectListItem> data2 = (List<SelectListItem>)result.ViewData["Groups"];
            Assert.AreEqual(0, data2.Count);
        }

        [TestMethod]
        public void GetAllOrganizersAndGroupsTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennhabcd@gmail.com",
                IsOrganizer = true,
                IsEnabled = false
            };
            db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllOrganizersAndGroups("abcd");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Organizers"];
            Assert.AreEqual(0, data.Count);
            List<SelectListItem> data2 = (List<SelectListItem>)result.ViewData["Groups"];
            Assert.AreEqual(0, data2.Count);
        }

        [TestMethod]
        public void GetAllOrganizersAndGroupsTest3() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờabcd",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennhabcd@gmail.com",
                IsOrganizer = true,
                IsEnabled = false
            };
            db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllOrganizersAndGroups("abcd");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Organizers"];
            Assert.AreEqual(0, data.Count);
            List<SelectListItem> data2 = (List<SelectListItem>)result.ViewData["Groups"];
            Assert.AreEqual(1, data2.Count);
        }

        [TestMethod]
        public void GetAllOrganizersAndGroupsTest4() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờabcd",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = false
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennhabcd@gmail.com",
                IsOrganizer = true,
                IsEnabled = false
            };
            db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db);
            PartialViewResult result = (PartialViewResult)controller.GetAllOrganizersAndGroups("abcd");
            //assert
            List<SelectListItem> data = (List<SelectListItem>)result.ViewData["Organizers"];
            Assert.AreEqual(0, data.Count);
            List<SelectListItem> data2 = (List<SelectListItem>)result.ViewData["Groups"];
            Assert.AreEqual(0, data2.Count);
        }

        [TestMethod]
        public void GetNotificationsNumberTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                IsEnabled = false,
                NotificationSeenDate = new System.DateTime(2019, 1, 30)
            };
            db.Users.Add(user1);
            db.Notifications.Add(new Notification() { NotificationId = 1, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 1) });
            db.Notifications.Add(new Notification() { NotificationId = 2, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 2) });
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            PartialViewResult result = (PartialViewResult)controller.GetNotificationsNumber();
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetNotificationsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                IsEnabled = true,
                NotificationSeenDate = new System.DateTime(2019, 1, 30)
            };
            db.Users.Add(user1);
            db.Notifications.Add(new Notification() { NotificationId = 1, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 1) });
            db.Notifications.Add(new Notification() { NotificationId = 2, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 2) });
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            PartialViewResult result = (PartialViewResult)controller.GetNotifications(1, true);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetNotificationsTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                IsEnabled = true,
                NotificationSeenDate = new System.DateTime(2019, 1, 30)
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            db.Notifications.Add(new Notification() { NotificationId = 1, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 1), SubjectId = 15, ObjectType = "Event", ObjectId = 1 });
            db.Notifications.Add(new Notification() { NotificationId = 2, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 2), SubjectId = 15, ObjectType = "Event", ObjectId = 1 });
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            PartialViewResult result = (PartialViewResult)controller.GetNotifications(1, true);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetNotificationsTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                IsEnabled = true,
                NotificationSeenDate = new System.DateTime(2019, 1, 30)
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            db.Notifications.Add(new Notification() { NotificationId = 1, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 1), SubjectId = 15, ObjectType = "Group", ObjectId = 1 });
            db.Notifications.Add(new Notification() { NotificationId = 2, UserId = 15, NotifiedDate = new System.DateTime(2019, 2, 2), SubjectId = 15, ObjectType = "Group", ObjectId = 1 });
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            PartialViewResult result = (PartialViewResult)controller.GetNotifications(1, true);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ViewEventTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewEvent(12);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewEventTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Deleted",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewEvent(1);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewEventTest2() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                mockContext.SetupGet(p => p.HttpContext.Request.Url).Returns(new Uri("http://localhost:1301/Home/ViewEvent/3", UriKind.Absolute));
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event event1 = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),
                    CoverImage = "img/abc",
                    EventDescription = "Heeloo World"
                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.ViewEvent(1);
                //assert
                Assert.AreEqual("~/Views/Authentication/Login.cshtml", result.ViewName);
            }
        }

        [TestMethod]
        public void ViewEventTest3() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                HttpContext.Current.Session["fbUSer"] = null;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(null);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                mockContext.SetupGet(p => p.HttpContext.Request.Url).Returns(new Uri("http://localhost:1301/Home/ViewEvent/3", UriKind.Absolute));
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event event1 = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),
                    CoverImage = "img/abc",
                    EventDescription = "Heeloo World",
                    IsPublic = true
                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                //                controller.Session["fbUSer"] = null;
                ViewResult result = (ViewResult)controller.ViewEvent(1);
                //assert
                Assert.AreEqual("ViewEventAnonymous", result.ViewName);
            }
        }

        [TestMethod]
        public void ViewEventTest4() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                FacebookModel fb = new FacebookModel() { Id = "1" };
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(fb);
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                mockContext.SetupGet(p => p.HttpContext.Request.Url).Returns(new Uri("http://localhost:1301/Home/ViewEvent/3", UriKind.Absolute));
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                List<FeedbackOuter> fbOut = new List<FeedbackOuter>() { new FeedbackOuter() { Key = "1" } };
                Event event1 = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),
                    CoverImage = "img/abc",
                    EventDescription = "Heeloo World",
                    IsPublic = true,
                    FeedbackOuters = fbOut
                };
                db.Events.Add(event1);
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                db.Groups.Add(group1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                //controller.Session["fbUSer"] = fb;
                ViewResult result = (ViewResult)controller.ViewEvent(1);
                //assert
                Assert.AreEqual("ViewEventAnonymous", result.ViewName);
            }
        }

        [TestMethod]
        public void ViewEventAnonymousTest() {
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),
                CoverImage = "img/abc",
                EventDescription = "Heeloo World",
                IsPublic = true
            };
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            HomeController controller = new HomeController(db);
            //controller.Session["fbUSer"] = fb;
            ViewResult result = (ViewResult)controller.ViewEventAnonymous(event1);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ViewAllGroupsTest() {
            using (new FakeHttpContext.FakeHttpContext()) {
                List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1},
                    new UserGroup(){GroupId = 2}
                };
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                List<Group> groupList = new List<Group> {
                    group1
                };
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    UserGroups = ug,
                    Groups = groupList
                };
                HttpContext.Current.Session["User"] = user1;
                HttpContext.Current.Session["Role"] = "Organizer";

                TestCapstoneContext db = new TestCapstoneContext();

                db.Users.Add(user1);
                db.Groups.Add(group1);
                //action
                HomeController controller = new HomeController(db);
                ViewResult result = (ViewResult)controller.ViewAllGroups();
                List<Group> data = (List<Group>)result.ViewData["LeaderGroup"];
                Assert.AreEqual(1, data.Count);
                List<Group> data2 = (List<Group>)result.ViewData["MemberGroup"];
                Assert.AreEqual(1, data2.Count);
            }
        }

        [TestMethod]
        public void ViewAllGroupsTest1() {
            using (new FakeHttpContext.FakeHttpContext()) {
                List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1},
                    new UserGroup(){GroupId = 2}
                };
                Group group1 = new Group() {
                    GroupId = 1,
                    GroupName = "Câu lạc bộ Cờ",
                    GroupImage = "img",
                    GroupDescription = "Hello world",
                    Leader = new User() {
                        UserProfile = new UserProfile() {
                            LastName = "huong",
                            FirstName = "dinh"
                        },
                        Email = "huong@gm.com"
                    },
                    GroupMail = "co@gm.com",
                    IsEnabled = true
                };
                List<Group> groupList = new List<Group> {
                    group1
                };
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    UserGroups = ug,
                    Groups = groupList
                };
                HttpContext.Current.Session["User"] = user1;
                HttpContext.Current.Session["Role"] = "Student";

                TestCapstoneContext db = new TestCapstoneContext();

                db.Users.Add(user1);
                db.Groups.Add(group1);
                //action
                HomeController controller = new HomeController(db);
                ViewResult result = (ViewResult)controller.ViewAllGroups();
                List<Group> data2 = (List<Group>)result.ViewData["MemberGroup"];
                Assert.AreEqual(1, data2.Count);
            }
        }

        [TestMethod]
        public void ViewGroupTest() {
            List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1, OrganizerId = 15, ParticipatedDate = new DateTime(2019,1,2)}
                };
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = false
            };
            List<Group> groupList = new List<Group> {
                group1
            };
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserGroups = ug,
                Groups = groupList
            };
            TestCapstoneContext db = new TestCapstoneContext();

            db.Users.Add(user1);
            db.Groups.Add(group1);
            db.UserGroups.Add(new UserGroup() { GroupId = 1, OrganizerId = 15, ParticipatedDate = new DateTime(2019, 1, 2) });
            //action
            HomeController controller = new HomeController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.ViewGroup(1);
            Assert.AreEqual("ViewAllGroups", result.RouteValues["action"]);
        }

        [TestMethod]
        public void ViewGroupTest1() {
            List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1, OrganizerId = 15, ParticipatedDate = new DateTime(2019,1,2)}
                };
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                LeaderId = 15,
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            List<Group> groupList = new List<Group> {
                group1
            };
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserGroups = ug,
                Groups = groupList
            };
            TestCapstoneContext db = new TestCapstoneContext();

            db.UserGroups.Add(new UserGroup() {
                User = new User() { UserProfile = new UserProfile() { FirstName = "huong", ProfileId = 1 } }
                ,
                GroupId = 1,
                OrganizerId = 15,
                ParticipatedDate = new DateTime(2019, 1, 2)
            });
            db.Users.Add(user1);
            db.Groups.Add(group1);
            //action
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewGroup(1);
            List<MemberModel> data = (List<MemberModel>)result.ViewData["GroupMember"];
            Assert.AreEqual(1, data.Count);
        }

        [TestMethod]
        public void ViewGroupTest2() {
            List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1, OrganizerId = 15, ParticipatedDate = new DateTime(2019,1,2)}
                };
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                LeaderId = 150,
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            List<Group> groupList = new List<Group> {
                group1
            };
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserGroups = ug,
                Groups = groupList
            };
            TestCapstoneContext db = new TestCapstoneContext();

            db.UserGroups.Add(new UserGroup() {
                User = new User() { UserProfile = new UserProfile() { FirstName = "huong", ProfileId = 1 } }
                ,
                GroupId = 1,
                OrganizerId = 15,
                ParticipatedDate = new DateTime(2019, 1, 2)
            });
            db.Users.Add(user1);
            db.Groups.Add(group1);
            //action
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewGroup(1);
            List<MemberModel> data = (List<MemberModel>)result.ViewData["GroupMember"];
            Assert.AreEqual(1, data.Count);
        }

        [TestMethod]
        public void ViewGroupTest3() {
            List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1, OrganizerId = 15, User = new User(){ UserProfile = new UserProfile(){ ProfileId = 1020} } , ParticipatedDate = new DateTime(2019,1,2)}
                };
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                LeaderId = 150,
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            List<Group> groupList = new List<Group> {
                group1
            };
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserGroups = ug,
                Groups = groupList
            };
            TestCapstoneContext db = new TestCapstoneContext();

            db.UserGroups.Add(new UserGroup() {
                User = new User() { UserProfile = new UserProfile() { FirstName = "huong", ProfileId = 1 } }
                ,
                GroupId = 1,
                OrganizerId = 15,
                ParticipatedDate = new DateTime(2019, 1, 2)
            });
            db.Users.Add(user1);
            db.Groups.Add(group1);
            //action
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewGroup(1);
            List<MemberModel> data = (List<MemberModel>)result.ViewData["GroupMember"];
            Assert.AreEqual(1, data.Count);
        }

        [TestMethod]
        public void ViewGroupTest4() {
            List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1, OrganizerId = 15, User = new User(){ UserProfile = new UserProfile(){ ProfileId = 1020} } , ParticipatedDate = new DateTime(2019,1,2)}
                };
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                LeaderId = 150,
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            List<Group> groupList = new List<Group> {
                group1
            };
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserGroups = ug,
                Groups = groupList
            };
            TestCapstoneContext db = new TestCapstoneContext();

            db.UserGroups.Add(new UserGroup() {
                User = new User() { UserId = 1 }
                ,
                GroupId = 1,
                OrganizerId = 15,
                ParticipatedDate = new DateTime(2019, 1, 2)
            });
            db.Users.Add(user1);
            db.Groups.Add(group1);
            //action
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewGroup(1);
            List<MemberModel> data = (List<MemberModel>)result.ViewData["GroupMember"];
            Assert.AreEqual(1, data.Count);
        }

        [TestMethod]
        public void ViewGroupTest5() {
            List<UserGroup> ug = new List<UserGroup>() {
                    new UserGroup(){GroupId = 1, OrganizerId = 15, User = new User(){ UserProfile = new UserProfile(){ ProfileId = 1020} } , ParticipatedDate = new DateTime(2019,1,2)}
                };
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                LeaderId = 150,
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            List<Group> groupList = new List<Group> {
                group1
            };
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserGroups = ug,
                Groups = groupList
            };
            TestCapstoneContext db = new TestCapstoneContext();
            Event ev = new Event() {
                EventId = 1,
                OrganizerId = 15,
                GroupId = 1,
                EventStatus = "Opening"
            };
            db.Events.Add(ev);
            db.UserGroups.Add(new UserGroup() {
                User = new User() { UserProfile = new UserProfile() { FirstName = "huong", ProfileId = 1 } }
                ,
                GroupId = 1,
                OrganizerId = 15,
                ParticipatedDate = new DateTime(2019, 1, 2)
            });
            db.Users.Add(user1);
            db.Groups.Add(group1);
            //action
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewGroup(1);
            List<MemberModel> data = (List<MemberModel>)result.ViewData["GroupMember"];
            Assert.AreEqual(1, data.Count);
        }

        //[TestMethod]
        public void UpdateFeedbackAnonymousTest() {
            FacebookModel fb = new FacebookModel() { Id = "1", Name = "huong", Picture = "abc.jpg" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(fb);
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            mockContext.SetupGet(p => p.HttpContext.Request.Url).Returns(new Uri("http://localhost:1301/Home/ViewEvent/3", UriKind.Absolute));
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            FeedbackOuter fbOut = new FeedbackOuter() {
                EventId = 1,
                Key = "1"
            };
            JsonResult result = controller.UpdateFeedbackAnonymous(fbOut);
            Assert.IsNotNull(result);
        }
        //[TestMethod]
        public void UpdateFeedbackAnonymousTest1() {
            FacebookModel fb = new FacebookModel() { Id = "1", Name = "huong", Picture = "abc.jpg" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(fb);
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            mockContext.SetupGet(p => p.HttpContext.Request.Url).Returns(new Uri("http://localhost:1301/Home/ViewEvent/3", UriKind.Absolute));
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.FeedbackOuters.Add(new FeedbackOuter() { Key = "1", EventId = 1 });
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            FeedbackOuter fbOut = new FeedbackOuter() {
                EventId = 1,
                Key = "1"
            };
            JsonResult result = controller.UpdateFeedbackAnonymous(fbOut);
            Assert.IsNotNull(result);
        }

        //[TestMethod]
        public void DeleteFeedbackAnonymousTest() {
            FacebookModel fb = new FacebookModel() { Id = "1", Name = "huong", Picture = "abc.jpg" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(fb);
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            mockContext.SetupGet(p => p.HttpContext.Request.Url).Returns(new Uri("http://localhost:1301/Home/ViewEvent/3", UriKind.Absolute));
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.FeedbackOuters.Add(new FeedbackOuter() { Key = "1", EventId = 1 });
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            List<Feedback> listFB = new List<Feedback> {
                new Feedback() {
                    UserId = 1,
                    User = new User() { UserProfile = new UserProfile() { FirstName = "huong", ProfileId = 1 } }
                }
            };
            FeedbackOuter fbOut1 = new FeedbackOuter() {
                EventId = 1,
                Key = "1",
                Event = new Event() {
                    EventId = 1,
                    Feedbacks = listFB
                }
            };
            FeedbackOuter fbOut = new FeedbackOuter() {
                EventId = 1,
                Key = "1",
                Event = new Event() {
                    EventId = 1,
                    Feedbacks = listFB,
                    FeedbackOuters = new List<FeedbackOuter>() { fbOut1 }
                }
            };
            JsonResult result = controller.UpdateFeedbackAnonymous(fbOut);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ViewProfileTest() {
            TestCapstoneContext db = new TestCapstoneContext();
            User user2 = new User() {
                UserId = 11,
                Email = "linh@gmail.com"
            };
            db.Users.Add(user2);

            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewProfile(12);
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewProfileTest1() {
            TestCapstoneContext db = new TestCapstoneContext();
            User user2 = new User() {
                UserId = 11,
                Email = "linh@gmail.com"
            };
            db.Users.Add(user2);

            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewProfile(12);
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewProfileTest2() {
            TestCapstoneContext db = new TestCapstoneContext();
            User user2 = new User() {
                UserId = 11,
                Email = "linh@gmail.com"
            };
            db.Users.Add(user2);

            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewProfile(11);
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewProfileTest3() {
            TestCapstoneContext db = new TestCapstoneContext();
            User user2 = new User() {
                UserId = 11,
                Email = "linh@gmail.com",
                UserProfile = new UserProfile() {
                    ProfileId = 1,
                    FirstName = "linh"
                }
            };
            db.Users.Add(user2);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewProfile(11);

            User data = (User)result.ViewData["User"];
            Assert.AreEqual(11, data.UserId);
            UserProfile data2 = (UserProfile)result.ViewData["Profile"];
            Assert.AreEqual(1, data2.ProfileId);
        }

        [TestMethod]
        public void ViewProfileTest4() {
            TestCapstoneContext db = new TestCapstoneContext();
            User user2 = new User() {
                UserId = 11,
                Email = "linh@gmail.com",
                UserProfile = new UserProfile() {
                    ProfileId = 1,
                    FirstName = "linh"
                },
                IsOrganizer = true
            };
            db.Users.Add(user2);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 11,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewProfile(11);

            User data = (User)result.ViewData["User"];
            Assert.AreEqual(11, data.UserId);
            UserProfile data2 = (UserProfile)result.ViewData["Profile"];
            Assert.AreEqual(1, data2.ProfileId);
            List<Event> data3 = (List<Event>)result.ViewData["Events"];
            Assert.AreEqual(1, data3.Count);
        }

        [TestMethod]
        public void ManageYourProfileTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            //db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ManageYourProfile();
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ManageYourProfileTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = false,
                UserProfile = new UserProfile() {
                    ProfileId = 1,
                    FirstName = "huong"
                }
            };
            db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ManageYourProfile();
            User data = (User)result.ViewData["User"];
            Assert.AreEqual(15, data.UserId);
            UserProfile data2 = (UserProfile)result.ViewData["Profile"];
            Assert.AreEqual(1, data2.ProfileId);
        }

        [TestMethod]
        public void ManageYourProfileTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserProfile = new UserProfile() {
                    ProfileId = 1,
                    FirstName = "huong"
                }
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ManageYourProfile();
            User data = (User)result.ViewData["User"];
            Assert.AreEqual(15, data.UserId);
            UserProfile data2 = (UserProfile)result.ViewData["Profile"];
            Assert.AreEqual(1, data2.ProfileId);
        }

        [TestMethod]
        public void ManageYourProfileTest3() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            //db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            controller.ModelState.AddModelError("key", "error message");
            ManageYourProfileViewModel input = new ManageYourProfileViewModel() { ProfileId = 1 };
            ViewResult result = (ViewResult)controller.ManageYourProfile(input);
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ManageYourProfileTest4() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = false,
                UserProfile = new UserProfile() {
                    ProfileId = 1,
                    FirstName = "huong"
                }
            };
            db.Users.Add(user1);
            //action
            HomeController controller = new HomeController(db);
            controller.ModelState.AddModelError("key", "error message");
            controller.ControllerContext = mockContext.Object;
            ManageYourProfileViewModel input = new ManageYourProfileViewModel() { ProfileId = 1 };
            ViewResult result = (ViewResult)controller.ManageYourProfile(input);
            User data = (User)result.ViewData["User"];
            Assert.AreEqual(15, data.UserId);
            UserProfile data2 = (UserProfile)result.ViewData["Profile"];
            Assert.AreEqual(1, data2.ProfileId);
        }

        [TestMethod]
        public void ManageYourProfileTest5() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserProfile = new UserProfile() {
                    ProfileId = 1,
                    FirstName = "huong"
                }
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            HomeController controller = new HomeController(db);
            controller.ModelState.AddModelError("key", "error message");
            controller.ControllerContext = mockContext.Object;
            ManageYourProfileViewModel input = new ManageYourProfileViewModel() { ProfileId = 1 };
            ViewResult result = (ViewResult)controller.ManageYourProfile(input);
            User data = (User)result.ViewData["User"];
            Assert.AreEqual(15, data.UserId);
            UserProfile data2 = (UserProfile)result.ViewData["Profile"];
            Assert.AreEqual(1, data2.ProfileId);
        }

        [TestMethod]
        public void ManageYourProfileTest6() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true,
                UserProfile = new UserProfile() {
                    ProfileId = 1,
                    FirstName = "huong"
                }
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            HomeController controller = new HomeController(db) {
                ControllerContext = mockContext.Object
            };
            ManageYourProfileViewModel input = new ManageYourProfileViewModel() { ProfileId = 1, FirstName = "dn", LastName = "new", Phone = "2937837" };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.ManageYourProfile(input);
            Assert.AreEqual("ManageYourProfile", result.RouteValues["action"]);
        }

        [TestMethod]
        public void ReportEventTest() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                //action
                HomeController controller = new HomeController(db);
                JsonResult result = controller.ReportEvent(1, "Event khong tot");
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void GetFeedbackTest() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                HttpContext.Current.Session["fbUSer"] = null;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(null);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event event1 = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),
                    CoverImage = "img/abc",
                    EventDescription = "Heeloo World",
                    IsPublic = true
                };
                db.Events.Add(event1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                JsonResult result = controller.GetFeedback(1, 0);
                //assert
                Assert.IsNotNull(result);
            }

        }

        [TestMethod]
        public void GetFeedbackTest1() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;

                HttpContext.Current.Session["fbUSer"] = null;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(true);
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(null);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event event1 = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),
                    CoverImage = "img/abc",
                    EventDescription = "Heeloo World",
                    IsPublic = true
                };
                db.Events.Add(event1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                JsonResult result = controller.GetFeedback(1, 0);
                //assert
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void GetFeedbackTest2() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                FacebookModel fb = new FacebookModel() { Id = "1", Name = "huong", Picture = "abc.jpg" };
                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Session["fbUSer"]).Returns(fb);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event event1 = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),
                    CoverImage = "img/abc",
                    EventDescription = "Heeloo World",
                    IsPublic = true
                };
                db.Events.Add(event1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                HomeController controller = new HomeController(db) {
                    ControllerContext = mockContext.Object
                };
                JsonResult result = controller.GetFeedback(1, 0);
                //assert
                Assert.IsNotNull(result);
            }

        }

        [TestMethod]
        public void ViewTeamTest() {
            TestCapstoneContext db = new TestCapstoneContext();
            HomeController controller = new HomeController(db);
            ViewResult result = (ViewResult)controller.ViewTeam();
            Assert.AreEqual("", result.ViewName);
        }
    }
}