﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using capstone.database.Entities;
using capstone.Models;
using capstone.test.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace capstone.Controllers.Tests {
    [TestClass]
    public class StudentControllerTests {
        [TestMethod]
        public void IndexTest() {
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(false);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Index();
            //assert
            Assert.AreEqual("Authentication", result.RouteValues["controller"]);
            Assert.AreEqual("Login", result.RouteValues["action"]);
        }

        [TestMethod()]
        public void ViewYourEventCalendarTest() {
            //using (new FakeHttpContext.FakeHttpContext())
            //{
            //    User user1 = new User()
            //    {
            //        UserId = 15,
            //        Email = "duyennh@gmail.com",
            //        IsOrganizer = true
            //    };
            //    HttpContext.Current.Session["User"] = user1;

            //    // create mock principal
            //    var mocks = new MockRepository(MockBehavior.Default);
            //    Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            //    mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            //    mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            //    // create mock controller context
            //    var mockContext = new Mock<ControllerContext>();
            //    mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            //    mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //    //data
            //    TestCapstoneContext db = new TestCapstoneContext();
            //    db.Users.Add(user1);
            //    //action
            //    StudentController controller = new StudentController(db);
            //    controller.ControllerContext = mockContext.Object;
            //    ViewResult result = (ViewResult)controller.ViewYourEventCalendar(null, null);
            //    Assert.IsNotNull(result);
            //}
        }

        [TestMethod()]
        public void ViewYourEventCalendarTest1() {
            //using (new FakeHttpContext.FakeHttpContext())
            //{
            //    User user1 = new User()
            //    {
            //        UserId = 15,
            //        Email = "duyennh@gmail.com",
            //        IsOrganizer = true
            //    };
            //    HttpContext.Current.Session["User"] = user1;
            //    // create mock principal
            //    var mocks = new MockRepository(MockBehavior.Default);
            //    Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            //    mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            //    mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            //    // create mock controller context
            //    var mockContext = new Mock<ControllerContext>();
            //    mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            //    mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //    //data
            //    TestCapstoneContext db = new TestCapstoneContext();
            //    Event ev = new Event()
            //    {
            //        EventId = 123,
            //        EventName = "abc",
            //        OpenDate = new DateTime(2019, 1, 2, 10, 0, 0),
            //        CloseDate = new DateTime(2019, 1, 3, 10, 0, 0),
            //    };
            //    db.Events.Add(ev);
            //    db.Users.Add(user1);
            //    //action
            //    StudentController controller = new StudentController(db);
            //    controller.ControllerContext = mockContext.Object;
            //    ViewResult result = (ViewResult)controller.ViewYourEventCalendar(1, 2019);
            //    Assert.IsNotNull(result);
            //}
        }

        [TestMethod()]
        public void ViewYourEventCalendarTest2() {
            //using (new FakeHttpContext.FakeHttpContext())
            //{
            //    User user1 = new User()
            //    {
            //        UserId = 15,
            //        Email = "duyennh@gmail.com",
            //        IsOrganizer = true
            //    };
            //    HttpContext.Current.Session["User"] = user1;
            //    // create mock principal
            //    var mocks = new MockRepository(MockBehavior.Default);
            //    Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            //    mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            //    mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            //    // create mock controller context
            //    var mockContext = new Mock<ControllerContext>();
            //    mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            //    mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //    //data
            //    TestCapstoneContext db = new TestCapstoneContext();
            //    Event ev = new Event()
            //    {
            //        EventId = 123,
            //        EventName = "abc",
            //        OpenDate = new DateTime(2019, 1, 2, 10, 0, 0),
            //        CloseDate = new DateTime(2019, 1, 3, 10, 0, 0),
            //    };
            //    db.Events.Add(ev);
            //    db.Users.Add(user1);
            //    //action
            //    StudentController controller = new StudentController(db);
            //    controller.ControllerContext = mockContext.Object;
            //    ViewResult result = (ViewResult)controller.ViewYourEventCalendar(1, 2019);
            //    Assert.IsNotNull(result);
            //}

        }

        [TestMethod()]
        public void ViewYourEventCalendarTest3() {
            //using (new FakeHttpContext.FakeHttpContext())
            //{
            //    User user1 = new User()
            //    {
            //        UserId = 15,
            //        Email = "duyennh@gmail.com",
            //        IsOrganizer = true
            //    };
            //    HttpContext.Current.Session["User"] = user1;
            //    // create mock principal
            //    var mocks = new MockRepository(MockBehavior.Default);
            //    Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            //    mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            //    mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            //    // create mock controller context
            //    var mockContext = new Mock<ControllerContext>();
            //    mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            //    mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //    //data
            //    TestCapstoneContext db = new TestCapstoneContext();
            //    Event ev = new Event()
            //    {
            //        EventId = 123,
            //        EventName = "abc",
            //        OpenDate = new DateTime(2019, 1, 2, 10, 0, 0),
            //        CloseDate = new DateTime(2019, 1, 3, 10, 0, 0),
            //    };
            //    db.Events.Add(ev);
            //    db.Users.Add(user1);
            //    //action
            //    StudentController controller = new StudentController(db);
            //    controller.ControllerContext = mockContext.Object;
            //    ViewResult result = (ViewResult)controller.ViewYourEventCalendar(1, 2019);
            //    Assert.IsNotNull(result);
            //}

        }

        [TestMethod()]
        public void ViewYourEventCalendarTest4() {
            //using (new FakeHttpContext.FakeHttpContext())
            //{
            //    User user1 = new User()
            //    {
            //        UserId = 15,
            //        Email = "duyennh@gmail.com",
            //        IsOrganizer = true
            //    };
            //    HttpContext.Current.Session["User"] = user1;
            //    // create mock principal
            //    var mocks = new MockRepository(MockBehavior.Default);
            //    Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            //    mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            //    mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            //    // create mock controller context
            //    var mockContext = new Mock<ControllerContext>();
            //    mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            //    mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //    //data
            //    TestCapstoneContext db = new TestCapstoneContext();
            //    Event ev = new Event()
            //    {
            //        EventId = 123,
            //        EventName = "abc",
            //        OpenDate = new DateTime(2019, 1, 2, 10, 0, 0),
            //        CloseDate = new DateTime(2019, 1, 3, 10, 0, 0),
            //    };
            //    db.Events.Add(ev);
            //    db.Users.Add(user1);
            //    //action
            //    StudentController controller = new StudentController(db);
            //    controller.ControllerContext = mockContext.Object;
            //    ViewResult result = (ViewResult)controller.ViewYourEventCalendar(1, 2019);
            //    Assert.IsNotNull(result);
            //}

        }

        [TestMethod()]
        public void ViewYourEventCalendarTest5() {
            //using (new FakeHttpContext.FakeHttpContext())
            //{
            //    User user1 = new User()
            //    {
            //        UserId = 15,
            //        Email = "duyennh@gmail.com",
            //        IsOrganizer = true
            //    };
            //    HttpContext.Current.Session["User"] = user1;
            //    // create mock principal
            //    var mocks = new MockRepository(MockBehavior.Default);
            //    Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            //    mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            //    mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            //    // create mock controller context
            //    var mockContext = new Mock<ControllerContext>();
            //    mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            //    mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //    //data
            //    TestCapstoneContext db = new TestCapstoneContext();
            //    Event ev = new Event()
            //    {
            //        EventId = 123,
            //        EventName = "abc",
            //        OpenDate = new DateTime(2019, 1, 2, 10, 0, 0),
            //        CloseDate = new DateTime(2019, 1, 3, 10, 0, 0),
            //    };
            //    db.Events.Add(ev);
            //    db.Users.Add(user1);
            //    //action
            //    StudentController controller = new StudentController(db);
            //    controller.ControllerContext = mockContext.Object;
            //    ViewResult result = (ViewResult)controller.ViewYourEventCalendar(1, 2019);
            //    Assert.IsNotNull(result);
            //}

        }

        [TestMethod]
        public void ViewYourEventCalendarByDateTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            StudentController controller = new StudentController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.ViewYourEventCalendarByDate(null);
            //assert
            Assert.AreEqual("ViewYourEventCalendar", result.RouteValues["action"]);
        }

        [TestMethod]
        public void ViewYourEventCalendarByDateTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            StudentController controller = new StudentController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.ViewYourEventCalendarByDate(new DateTime(2019, 1, 2));
            //assert
            Assert.AreEqual("ViewYourEventCalendar", result.RouteValues["action"]);
        }

        [TestMethod()]
        public void ViewYourEventCalendarByDateTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            StudentController controller = new StudentController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.ViewYourEventCalendarByDate(new DateTime(2019, 4, 2));
            //assert
            Assert.AreEqual("ViewYourEventCalendar", result.RouteValues["action"]);
        }

        [TestMethod]
        public void RegisterEventTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            StudentController controller = new StudentController(db);
            ViewResult result = (ViewResult)controller.RegisterEvent(12, true);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void RegisterEventTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            StudentController controller = new StudentController(db);
            ViewResult result = (ViewResult)controller.RegisterEvent(1, true);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void RegisterEventTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),
                RegisterEndDate = DateTime.Now.AddDays(-1)
            };
            db.Events.Add(event1);
            //action
            StudentController controller = new StudentController(db);
            ViewResult result = (ViewResult)controller.RegisterEvent(1, true);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void RegisterEventTest3() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event event1 = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    EventStatus = "Opening",
                    EventName = "Event 1",
                    OpenDate = new System.DateTime(2017, 1, 7),
                    CloseDate = new System.DateTime(2017, 1, 8),
                    RegisterEndDate = DateTime.Now.AddDays(1),
                    Forms = new List<Form>()
                {
                    new Form(){FormRegister = "abc"}
                }
                };
                db.Users.Add(user1);
                db.Events.Add(event1);
                //action
                StudentController controller = new StudentController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.RegisterEvent(1, false);
                //assert
                Assert.AreEqual("", result.ViewName);
            }
        }

        [TestMethod()]
        public void UnregisterEventTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            StudentController controller = new StudentController(db);
            ViewResult result = (ViewResult)controller.UnregisterEvent(12);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod()]
        public void UnregisterEventTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            StudentController controller = new StudentController(db);
            ViewResult result = (ViewResult)controller.UnregisterEvent(1);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod()]
        public void UnregisterEventTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8)
            };
            db.Events.Add(event1);
            db.Registers.Add(new Register() { EventId = 1, UserId = 15 });
            db.Bookmarks.Add(new Bookmark() { EventId = 1, UserId = 15 });
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.UnregisterEvent(1);
            //assert
            Assert.AreEqual("ViewEvent", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        //[TestMethod()]
        public void UpdateFeedbackTest() {
            Feedback input = new Feedback() {
                EventId = 1,
                UserId = 15,
                User = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    UserProfile = new UserProfile() {
                        ProfileId = 1,
                        FirstName = "duyen",
                        LastName = "my",
                        ProfileImage = "abc.jpg"
                    }
                },
                CreatedDate = new DateTime(2019, 2, 13),
                Event = new Event() {
                    Feedbacks = new List<Feedback>() {
                        new Feedback(){EventId = 1, Value = 1}
                    },
                    FeedbackOuters = new List<FeedbackOuter>() {
                        new FeedbackOuter(){EventId = 1, Value = 1}
                    }
                }
            };

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Feedbacks.Add(input);
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8)
            };
            db.Events.Add(event1);
            db.Registers.Add(new Register() { EventId = 1, UserId = 15 });
            db.Bookmarks.Add(new Bookmark() { EventId = 1, UserId = 15 });
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.UpdateFeedback(input);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void DeleteFeedbackTest() {
            Feedback input = new Feedback() {
                EventId = 1,
                UserId = 15,
                User = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    UserProfile = new UserProfile() {
                        ProfileId = 1,
                        FirstName = "duyen",
                        LastName = "my",
                        ProfileImage = "abc.jpg"
                    }
                },
                CreatedDate = new DateTime(2019, 2, 13),
                Event = new Event() {
                    Feedbacks = new List<Feedback>() {
                        new Feedback(){
                            EventId = 1,
                            Value = 1,
                            User = new User()
                                {
                                    UserId = 15,
                                    Email = "duyennh@gmail.com",
                                    IsOrganizer = true,
                                    UserProfile = new UserProfile()
                                    {
                                        ProfileId = 1,
                                        FirstName = "duyen",
                                        LastName = "my",
                                        ProfileImage = "abc.jpg"
                                    }
                                }
                        }
                    },
                    FeedbackOuters = new List<FeedbackOuter>() {
                        new FeedbackOuter(){EventId = 1, Value = 1}
                    }
                }
            };

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Feedbacks.Add(input);
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8)
            };
            db.Events.Add(event1);
            db.Registers.Add(new Register() { EventId = 1, UserId = 15 });
            db.Bookmarks.Add(new Bookmark() { EventId = 1, UserId = 15 });
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.DeleteFeedback(input);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void DeleteFeedbackTest2() {
            Feedback input = new Feedback() {
                EventId = 1,
                UserId = 15,
                User = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    UserProfile = new UserProfile() {
                        ProfileId = 1,
                        FirstName = "duyen",
                        LastName = "my",
                        ProfileImage = "abc.jpg"
                    }
                },
                CreatedDate = new DateTime(2019, 2, 13),
                Event = new Event() {
                    Feedbacks = new List<Feedback>() {
                        new Feedback(){
                            EventId = 1,
                            Value = 1,
                            User = new User()
                                {
                                    UserId = 15,
                                    Email = "duyennh@gmail.com",
                                    IsOrganizer = true,
                                    UserProfile = new UserProfile()
                                    {
                                        ProfileId = 1,
                                        FirstName = "duyen",
                                        LastName = "my",
                                        ProfileImage = "abc.jpg"
                                    }
                                }
                        }
                    },
                    FeedbackOuters = new List<FeedbackOuter>() {
                        new FeedbackOuter(){EventId = 1, Value = 1}
                    }
                }
            };

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Feedbacks.Add(input);
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8)
            };
            db.Events.Add(event1);
            db.Registers.Add(new Register() { EventId = 1, UserId = 15 });
            db.Bookmarks.Add(new Bookmark() { EventId = 1, UserId = 15 });
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.DeleteFeedback(input);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void DeleteFeedbackTest1() {
            Feedback input = new Feedback() {
                EventId = 1,
                UserId = 15,
                User = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    UserProfile = new UserProfile() {
                        ProfileId = 1,
                        FirstName = "duyen",
                        LastName = "my",
                        ProfileImage = "abc.jpg"
                    }
                },
                CreatedDate = new DateTime(2019, 2, 13),
                Event = new Event() {
                    Feedbacks = new List<Feedback>() {
                        new Feedback(){
                            EventId = 1,
                            Value = 1,
                            User = new User()
                                {
                                    UserId = 15,
                                    Email = "duyennh@gmail.com",
                                    IsOrganizer = true,
                                    UserProfile = new UserProfile()
                                    {
                                        ProfileId = 1,
                                        FirstName = "duyen",
                                        LastName = "my",
                                        ProfileImage = "abc.jpg"
                                    }
                                }
                        }
                    },
                    FeedbackOuters = new List<FeedbackOuter>() {
                        new FeedbackOuter(){EventId = 1, Value = 1}
                    }
                }
            };

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8)
            };
            db.Events.Add(event1);
            db.Registers.Add(new Register() { EventId = 1, UserId = 15 });
            db.Bookmarks.Add(new Bookmark() { EventId = 1, UserId = 15 });
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.DeleteFeedback(input);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void ViewRegisteredEventsTest() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "Draft" };
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            StudentController controller = new StudentController(db);
            ViewResult result = (ViewResult)controller.ViewRegisteredEvents(1, input);
            Assert.AreEqual("Error", result.ViewName);
        }

        [TestMethod()]
        public void ViewRegisteredEventsTest1() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "Opening" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);

            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewRegisteredEvents(1, input);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void ViewRegisteredEventsTest2() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "Opening", Search = "" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);

            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewRegisteredEvents(1, input);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void ViewRegisteredEventsTest3() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "Opening", Search = "" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);

            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewRegisteredEvents(1, input);
            //assert
            Assert.IsNotNull(result);

        }

        [TestMethod()]
        public void ViewRegisteredEventsTest4() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "Opening", Search = "abc" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1 abc",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);

            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewRegisteredEvents(1, input);
            //assert
            Assert.IsNotNull(result);

        }

        [TestMethod()]
        public void ViewRegisteredEventsTest5() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "", Search = "abc" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1 abc",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);

            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewRegisteredEvents(1, input);
            //assert
            Assert.IsNotNull(result);

        }

        [TestMethod()]
        public void ViewRegisteredEventsTest6() {

            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "Opening", Search = "abc" };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 15, User = new User() { UserId = 15, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1 abc",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);

            //action
            StudentController controller = new StudentController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewRegisteredEvents(1, input);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void BookmarkTest() {
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.Bookmark(2, 15);
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void UnbookmarkTest() {
            TestCapstoneContext db = new TestCapstoneContext();
            Bookmark bm = new Bookmark() { EventId = 1, UserId = 15 };
            db.Bookmarks.Add(bm);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.Unbookmark(1, 15);
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void UnbookmarkTest1() {
            TestCapstoneContext db = new TestCapstoneContext();
            Bookmark bm = new Bookmark() { EventId = 1, UserId = 10 };
            db.Bookmarks.Add(bm);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.Unbookmark(1, 10);
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void UnbookmarkTest2() {
            TestCapstoneContext db = new TestCapstoneContext();
            Bookmark bm = new Bookmark() { EventId = 2, UserId = 15 };
            db.Bookmarks.Add(bm);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            StudentController controller = new StudentController(db);
            JsonResult result = controller.Unbookmark(2, 15);
            Assert.IsNotNull(result);
        }

    }
}