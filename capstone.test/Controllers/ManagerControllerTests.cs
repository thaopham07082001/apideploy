﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;
using capstone.database.Entities;
using capstone.Models;
using capstone.test.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace capstone.Controllers.Tests {
    [TestClass]
    public class ManagerControllerTests {
        [TestMethod]
        public void IndexTest() {
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(false);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            db.Users.Add(user1);
            //action
            ManagerController controller = new ManagerController(db) {
                ControllerContext = mockContext.Object
            };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Index();
            //assert
            Assert.AreEqual("Authentication", result.RouteValues["controller"]);
            Assert.AreEqual("Login", result.RouteValues["action"]);

        }

        [TestMethod]
        public void ViewEventStatisticsTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewEventStatistics(12);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewEventStatisticsTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewEventStatistics(123);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ViewEventStatisticsTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewEventStatistics(123);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ViewEventStatisticsTest3() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins,
                RegisterEndDate = new DateTime(2019, 1, 1)
            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewEventStatistics(123);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ViewRegisteredStudentsTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ViewRegisteredStudents(12, 1, model);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewRegisteredStudentsTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ViewRegisteredStudents(1, 1, model);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewRegisteredStudentsTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event2 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins,
                RegisterEndDate = new DateTime(2019, 1, 1)
            };
            db.Events.Add(event2);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ViewRegisteredStudents(123, 1, model);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ViewCheckedinStudentsTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ViewCheckedinStudents(12, 1, model);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewCheckedinStudentsTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ViewCheckedinStudents(1, 1, model);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewCheckedinStudentsTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event2 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins,
                RegisterEndDate = new DateTime(2019, 1, 1)
            };
            db.Events.Add(event2);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ViewCheckedinStudents(123, 1, model);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ExportEventStatisticsTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            Task<JsonResult> result = controller.ExportEventStatistics(12);
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageFeaturedEventsTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            Event event2 = new Event() {
                EventId = 2,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event2);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ManageFeaturedEvents();
            List<Event> data = (List<Event>)result.ViewData.Model;
            //assert
            Assert.AreEqual(2, data.Count);
        }

        [TestMethod]
        public void ManageFeaturedEventsTest1() {
            FormCollection inut = new FormCollection();
            inut.Set("selectedItem", "true,false");
            inut.Set("eventID", "1,2");

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            Event event2 = new Event() {
                EventId = 2,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event2);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ManageFeaturedEvents(inut);
            List<Event> data = (List<Event>)result.ViewData.Model;
            //assert
            Assert.AreEqual(2, data.Count);
            Assert.AreEqual(1, data[0].EventId);
            Assert.AreEqual(true, data[0].IsFeatured);
            Assert.AreEqual(2, data[1].EventId);
            Assert.AreEqual(false, data[1].IsFeatured);
        }

        [TestMethod]
        public void ViewAllCategoriesTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true
            };
            Category cat1 = new Category() {
                CategoryId = 2,
                IsEnabled = true
            };
            db.Categories.Add(cat1);
            db.Categories.Add(cat2);
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.ViewAllCategories();
            List<Category> data = (List<Category>)result.ViewData.Model;
            //assert
            Assert.AreEqual(2, data.Count);
        }

        [TestMethod]
        public void CreateCategoryTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.CreateCategory();
            //assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void CreateCategoryTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true
            };
            Category cat1 = new Category() {
                CategoryId = 2,
                IsEnabled = true
            };
            db.Categories.Add(cat1);
            //action
            ManagerController controller = new ManagerController(db);
            controller.ModelState.AddModelError("key", "error message");
            controller.ControllerContext = mockContext.Object;
            ViewResult result = (ViewResult)controller.CreateCategory(cat2);
            //assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void CreateCategoryTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true
            };
            Category cat1 = new Category() {
                CategoryId = 2,
                IsEnabled = true
            };
            db.Categories.Add(cat1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            ManagerController controller = new ManagerController(db) {
                ControllerContext = mockContext.Object
            };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.CreateCategory(cat2);
            //assert
            Assert.AreEqual("ViewAllCategories", result.RouteValues["action"]);
        }

        [TestMethod]
        public void EditCategoryTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true
            };
            Category cat1 = new Category() {
                CategoryId = 2,
                IsEnabled = true
            };
            db.Categories.Add(cat2);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            ManagerController controller = new ManagerController(db) {
                ControllerContext = mockContext.Object
            };
            //assert
            ViewResult result = (ViewResult)controller.EditCategory(cat2);
            //assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void EditCategoryTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true,
                CategoryName = "abc"
            };
            Category cat1 = new Category() {
                CategoryId = 2,
                IsEnabled = true
            };
            db.Categories.Add(cat1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            ManagerController controller = new ManagerController(db) {
                ControllerContext = mockContext.Object
            };
            controller.ModelState.AddModelError("key", "error message");
            //assert
            ViewResult result = (ViewResult)controller.EditCategory(cat2);
            //assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void EditCategoryTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true,
                CategoryName = "abc"
            };
            Category cat1 = new Category() {
                CategoryId = 2,
                IsEnabled = true
            };
            db.Categories.Add(cat2);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            ManagerController controller = new ManagerController(db) {
                ControllerContext = mockContext.Object
            };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.EditCategory(cat2);
            //assert
            Assert.AreEqual("ViewAllCategories", result.RouteValues["action"]);
        }

        [TestMethod]
        public void EditCategoryTest3() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true,
                CategoryName = "abc"
            };
            db.Categories.Add(cat2);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            ManagerController controller = new ManagerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.EditCategory(1);
            Category data = (Category)result.ViewData.Model;
            //assert
            Assert.AreEqual(1, data.CategoryId);
        }

        [TestMethod]
        public void DeleteCategoryTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Category cat2 = new Category() {
                CategoryId = 1,
                IsEnabled = true,
                CategoryName = "abc"
            };
            db.Categories.Add(cat2);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            //action
            ManagerController controller = new ManagerController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.DeleteCategory(1);
            //assert
            //assert
            Assert.AreEqual("ViewAllCategories", result.RouteValues["action"]);
        }

        [TestMethod]
        public void ManagePendingEventsTest() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "abc", Search = "abc" };

            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            TestCapstoneContext db = new TestCapstoneContext();
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);

            //action
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ManagePendingEvents(1, input);
            //assert
            //assert
            List<EventModel> ev = (List<EventModel>)result.ViewData["Events"];
            Assert.AreEqual(0, ev.Count);
        }

        [TestMethod]
        public void ManagePendingEventsTest1() {
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Search = "" };

            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Pending",
                EventName = "Event 1abc",
                GroupId = 1
            };
            TestCapstoneContext db = new TestCapstoneContext();
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserProfile = new UserProfile() {
                    LastName = "huong",
                    FirstName = "dinh"
                },
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);

            //action
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ManagePendingEvents(1, input);
            //assert
            //assert
            List<EventModel> ev = (List<EventModel>)result.ViewData["Events"];
            Assert.AreEqual(1, ev.Count);
        }

        [TestMethod]
        public void AcceptEventTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8),

            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.AcceptEvent(12);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }
        // [TestMethod]
        public void AcceptEventTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 1,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                OpenDate = new System.DateTime(2017, 1, 7),
                CloseDate = new System.DateTime(2017, 1, 8)
            };
            db.Events.Add(event1);
            //action
            ManagerController controller = new ManagerController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.AcceptEvent(1);
            //assert
            Assert.AreEqual("ViewEvent/1", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);

        }

        [TestMethod]
        public void DeleteGroupTest() {
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            //action
            ManagerController controller = new ManagerController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.DeleteGroup(1);
            //assert
            Assert.AreEqual("ViewAllGroups", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void CreateGroupTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.CreateGroup();
            //assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void CreateGroupTest1() {
            CreateGroupModel input = new CreateGroupModel() {
                Leader = "huong@gm.com"
            };
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            ManagerController controller = new ManagerController(db);
            UserSearchViewModel model = new UserSearchViewModel() { };
            ViewResult result = (ViewResult)controller.CreateGroup(input);
            //assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void ChangeLeaderTest() {//data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            ManagerController controller = new ManagerController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.ChangeLeader(15);
            //assert
            Assert.AreEqual("Home", result.RouteValues["controller"]);
            Assert.AreEqual("ViewAllGroups", result.RouteValues["action"]);
        }

        [TestMethod]
        public void ChangeLeaderTest2() {//data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = false
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            ManagerController controller = new ManagerController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.ChangeLeader(1);
            //assert
            Assert.AreEqual("Home", result.RouteValues["controller"]);
            Assert.AreEqual("ViewAllGroups", result.RouteValues["action"]);
        }

        [TestMethod]
        public void ChangeLeaderTest1() {//data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 1,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            UserGroup ug1 = new UserGroup() {
                GroupId = 1,
                OrganizerId = 1,
                User = new User() {
                    UserId = 1,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true,
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    }
                }
            };
            db.UserGroups.Add(ug1);
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ChangeLeader(1);
            Group data = (Group)result.ViewData.Model;
            //assert
            Assert.AreEqual(1, data.GroupId);
        }

        [TestMethod]
        public void ViewAllReportsTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            Report report = new Report() {
                IsDismissed = false
            };

            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewAllReports();
            List<Report> data = (List<Report>)result.ViewData.Model;
            //assert
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void ViewAllReportsTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            Report report = new Report() {
                IsDismissed = false,
                Event = event1
            };
            db.Reports.Add(report);

            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewAllReports();
            List<Report> data = (List<Report>)result.ViewData.Model;
            //assert
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void ViewAllReportsTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1",
                CreatedDate = DateTime.Now.AddDays(-1)
            };
            db.Events.Add(event1);
            Report report = new Report() {
                IsDismissed = true,
                Event = event1
            };

            db.Reports.Add(report);
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewAllReports();
            List<Report> data = (List<Report>)result.ViewData.Model;
            //assert
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void ViewAllReportsTest3() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1",
                CreatedDate = DateTime.Now.AddMonths(-2)
            };
            db.Events.Add(event1);
            Report report = new Report() {
                IsDismissed = true,
                Event = event1
            };

            db.Reports.Add(report);
            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewAllReports();
            List<Report> data = (List<Report>)result.ViewData.Model;
            //assert
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void ViewAllReportsTest4() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1",
                CreatedDate = DateTime.Now.AddDays(-1)
            };
            db.Events.Add(event1);
            Report report = new Report() {
                IsDismissed = true,
                Event = event1
            };
            db.Reports.Add(report);

            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewAllReports();
            List<Report> data = (List<Report>)result.ViewData.Model;
            //assert
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void ViewAllReportsTest5() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1",
                CreatedDate = DateTime.Now.AddMonths(-2)
            };
            db.Events.Add(event1);
            Report report = new Report() {
                IsDismissed = true,
                Event = event1
            };
            db.Reports.Add(report);

            ManagerController controller = new ManagerController(db);
            ViewResult result = (ViewResult)controller.ViewAllReports();
            List<Report> data = (List<Report>)result.ViewData.Model;
            //assert
            Assert.AreEqual(0, data.Count);
        }

        [TestMethod]
        public void DismissReportTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 1",
                CreatedDate = DateTime.Now.AddMonths(-2)
            };
            db.Events.Add(event1);
            Report report = new Report() {
                Id = 1,
                IsDismissed = false,
                Event = event1
            };
            db.Reports.Add(report);
            ManagerController controller = new ManagerController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.DismissReport(1);
            //assert
            Assert.AreEqual("ViewAllReports", result.RouteValues["action"]);
        }
    }
}