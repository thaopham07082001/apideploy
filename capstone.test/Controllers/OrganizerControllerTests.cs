﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using capstone.database.Entities;
using capstone.Models;
using capstone.test.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace capstone.Controllers.Tests {
    [TestClass]
    public class OrganizerControllerTests {
        [TestMethod]
        public void CheckinEventTest() {
            using (new FakeHttpContext.FakeHttpContext()) {
                HttpContext.Current.Session["User"] = "duyennh@gmail.com";
                // mock 
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.CheckinEvent(123, 1);
                //assert
                Assert.AreEqual("Error", result.ViewName);
            }
        }

        [TestMethod]
        public void EditGroupTest() {
            //set data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img/123.jpg",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gmail.com"
                },
                GroupMail = "co@gmail.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            OrganizerController controller = new OrganizerController(db);
            //action
            ViewResult result = controller.EditGroup(1) as ViewResult;
            CreateGroupModel group = (CreateGroupModel)result.ViewData.Model;
            //assert
            Assert.AreEqual(group.Id, 1);
            Assert.AreEqual(group.Name, "Câu lạc bộ Cờ");
            Assert.AreEqual(group.Leader, "huong dinh (huong@gmail.com)");
        }

        [TestMethod]
        public void EditGroupTest1() {
            //set data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = false
            };
            db.Groups.Add(group1);
            OrganizerController controller = new OrganizerController(db);
            //action
            RedirectToRouteResult result = (RedirectToRouteResult)controller.EditGroup(1);
            //assert
            Assert.AreEqual("ViewAllGroups", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void EditGroupTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            //input
            CreateGroupModel input = new CreateGroupModel() {
                Id = 1,
                Name = "CLB Hoa Sen",
                Description = "Hello world",
                GroupImage = "img/Hoa sen",
                Leader = "Lan Huong",
                Mail = "huongdl@gmail.com"
            };
            //action
            OrganizerController controller = new OrganizerController(db);
            RedirectToRouteResult result = (RedirectToRouteResult)controller.EditGroup(input);
            //assert
            Assert.AreEqual("ViewGroup/1", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void EditGroupTest3() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            //input
            CreateGroupModel input = new CreateGroupModel() {
                Id = 1,
                Name = "CLB Hoa Sen",
                Description = "Hello world",
                GroupImage = "img/Hoa sen",
                Leader = "Lan Huong",
                Mail = "huongdl@gmail.com"
            };
            //action
            OrganizerController controller = new OrganizerController(db);
            controller.ModelState.AddModelError("key", "error message");
            ViewResult result = (ViewResult)controller.EditGroup(input);
            //assert
            Assert.AreEqual(input.Id, 1);
            Assert.AreEqual(input.Name, "CLB Hoa Sen");
        }

        //[TestMethod]
        public void RemoveMemberTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            UserGroup ug1 = new UserGroup() {
                GroupId = 1,
                OrganizerId = 1
            };
            db.UserGroups.Add(ug1);
            //action
            OrganizerController controller = new OrganizerController(db);
            controller.ModelState.AddModelError("key", "error message");
            JsonResult result = controller.RemoveMember(1, 1);
            //assert
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void AddMemberTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 1,
                Email = "huong"
            };
            db.Users.Add(user1);
            //action
            OrganizerController controller = new OrganizerController(db);
            JsonResult result = controller.AddMember(1, "Duyen");
            //assert
            Assert.AreEqual(404, result.Data);
        }

        [TestMethod]
        public void AddMemberTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 1,
                Email = "huong"
            };
            db.Users.Add(user1);
            UserGroup ug1 = new UserGroup() {
                GroupId = 1,
                OrganizerId = 1
            };
            db.UserGroups.Add(ug1);
            //action
            OrganizerController controller = new OrganizerController(db);
            JsonResult result = controller.AddMember(1, "huong");
            //assert
            Assert.AreEqual(405, result.Data);
        }

        //[TestMethod]
        public void AddMemberTest2() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "linh"
            };
            db.Users.Add(user1);
            UserGroup ug1 = new UserGroup() {
                GroupId = 1,
                OrganizerId = 1
            };
            db.UserGroups.Add(ug1);
            //action
            OrganizerController controller = new OrganizerController(db);
            JsonResult result = controller.AddMember(1, "linh");
            //assert
            MemberModel resultData = (MemberModel)result.Data;
            Assert.AreEqual("linh", resultData.Email);
            Assert.AreEqual(15, resultData.Id);
        }

        [TestMethod]
        public void SearchOrganizersNotMemberTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Group group1 = new Group() {
                GroupId = 1,
                LeaderId = 1
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            OrganizerController controller = new OrganizerController(db);
            JsonResult result = controller.SearchOrganizersNotMember("duyen", 1);
            //assert
            List<SearchOrganizersModel> resultData = (List<SearchOrganizersModel>)result.Data;
            Assert.AreEqual("duyennh@gmail.com", resultData[0].Email);
            Assert.AreEqual(15, resultData[0].Id);
        }

        [TestMethod]
        public void CheckinEventTest1() {
            using (new FakeHttpContext.FakeHttpContext()) {
                HttpContext.Current.Session["User"] = "duyennh@gmail.com";
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event ev = new Event() {
                    EventId = 123
                };
                db.Events.Add(ev);
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsStudent = true
                };
                db.Users.Add(user1);
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.CheckinEvent(125, 15);
                //assert
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }

        [TestMethod]
        public void CheckinEventTest2() {
            using (new FakeHttpContext.FakeHttpContext()) {
                HttpContext.Current.Session["User"] = "duyennh@gmail.com";
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event ev = new Event() {
                    EventId = 123
                };
                db.Events.Add(ev);
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsStudent = false
                };
                db.Users.Add(user1);
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.CheckinEvent(123, 15);
                //assert
                Assert.AreEqual("Error", result.ViewName);
            }
        }

        [TestMethod]
        public void CheckinEventTest3() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event ev = new Event() {
                    EventId = 123,
                    OrganizerId = 1000,
                    EventStatus = "Happening"
                };
                db.Events.Add(ev);
                db.Users.Add(user1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "huong@gmail.com",
                    IsStudent = true
                };
                db.Users.Add(user2);
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.CheckinEvent(123, 11);
                //assert
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }

        [TestMethod]
        public void CheckinEventTest4() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event ev = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Draft"
                };
                db.Events.Add(ev);
                db.Users.Add(user1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "huong@gmail.com",
                    IsStudent = true
                };
                db.Users.Add(user2);
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.CheckinEvent(123, 11);
                //assert
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }

        [TestMethod]
        public void CheckinEventTest5() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event ev = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Happening"
                };
                db.Events.Add(ev);
                db.Users.Add(user1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "huong@gmail.com",
                    IsStudent = true
                };
                db.Users.Add(user2);
                Checkin checkIn = new Checkin() { EventId = 123, UserId = 11 };
                db.Checkins.Add(checkIn);
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.CheckinEvent(123, 11);
                //assert
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }

        [TestMethod]
        public void CheckinEventTest6() {
            using (new FakeHttpContext.FakeHttpContext()) {
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                Event ev = new Event() {
                    EventId = 123,
                    OrganizerId = 15,
                    EventStatus = "Happening"
                };
                db.Events.Add(ev);
                db.Users.Add(user1);
                User user2 = new User() {
                    UserId = 11,
                    Email = "huong@gmail.com",
                    IsStudent = true
                };
                db.Users.Add(user2);
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                PartialViewResult result = (PartialViewResult)controller.CheckinEvent(123, 11);
                //assert
                Assert.IsNotNull(result);
            }
        }


        [TestMethod]
        public void MoveToDraftTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event ev = new Event() {
                EventId = 1
            };
            db.Events.Add(ev);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            ViewResult result = (ViewResult)controller.MoveToDraft(1);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void MoveToDraftTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event ev = new Event() {
                EventId = 1,
                OrganizerId = 15
            };
            db.Events.Add(ev);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            RedirectToRouteResult result = (RedirectToRouteResult)controller.MoveToDraft(1);
            //assert
            Assert.AreEqual("ViewYourDraftEvents", result.RouteValues["action"]);
        }

        [TestMethod]
        public void CancelEventTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event ev = new Event() {
                EventId = 1
            };
            db.Events.Add(ev);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            ViewResult result = (ViewResult)controller.CancelEvent(1);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void CancelEventTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event ev = new Event() {
                EventId = 1,
                OrganizerId = 15
            };
            db.Events.Add(ev);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            RedirectToRouteResult result = (RedirectToRouteResult)controller.CancelEvent(1);
            //assert
            Assert.AreEqual("ViewYourDraftEvents", result.RouteValues["action"]);
        }

        [TestMethod]
        public void ViewYourPendingEventsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event ev = new Event() {
                EventId = 1,
                OrganizerId = 15,
                GroupId = 1,
                EventName = "Toi la ai",
                EventStatus = "Pending"
            };
            db.Events.Add(ev);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            ViewResult result = (ViewResult)controller.ViewYourPendingEvents(1);
            List<EventModel> resultData = (List<EventModel>)result.ViewData.Model;
            //assert
            Assert.AreEqual("Toi la ai", resultData[0].Name);
            Assert.AreEqual("Pending", resultData[0].Status);
        }

        [TestMethod]
        public void EditDraftEventTest() {
            using (new FakeHttpContext.FakeHttpContext()) {
                HttpContext.Current.Session["User"] = "duyennh@gmail.com";
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                User duyen = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com"
                };
                db.Users.Add(duyen);
                db.UserGroups.Add(new UserGroup() { GroupId = 1, OrganizerId = 15 });
                db.Groups.Add(new Group() { GroupId = 1, GroupName = "CLB Cờ" });
                //input
                CreateEventViewModel input = new CreateEventViewModel() {
                    EventId = 1,
                    Name = "CLB Hoa Sen",
                    Description = "Hello world",
                    CoverImage = "img/Hoa sen",
                    OpenDate = new System.DateTime(2019, 2, 17),
                    CloseDate = new System.DateTime(2019, 2, 18),
                    OpenTime = new System.DateTime(2019, 2, 17, 10, 30, 0),
                    CloseTime = new System.DateTime(2019, 2, 17, 11, 30, 0),
                    GroupId = 1
                };
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.EditDraftEvent(input, "Save draft");
                //assert
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }

        [TestMethod]
        public void EditDraftEventTest1() {
            using (new FakeHttpContext.FakeHttpContext()) {
                HttpContext.Current.Session["User"] = "duyennh@gmail.com";
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                User duyen = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com"
                };
                db.Users.Add(duyen);
                db.UserGroups.Add(new UserGroup() { GroupId = 1, OrganizerId = 15 });
                db.Groups.Add(new Group() { GroupId = 1, GroupName = "CLB Cờ" });
                Event ev = new Event() {
                    EventId = 1,
                    OrganizerId = 15,
                    GroupId = 1,
                    EventName = "Toi la ai",
                    EventStatus = "Pending"
                };
                db.Events.Add(ev);
                //input
                CreateEventViewModel input = new CreateEventViewModel() {
                    EventId = 1,
                    Name = "CLB Hoa Sen",
                    Description = "Hello world",
                    CoverImage = "img/Hoa sen",
                    OpenDate = new System.DateTime(2019, 2, 17),
                    CloseDate = new System.DateTime(2019, 2, 18),
                    OpenTime = new System.DateTime(2019, 2, 17, 10, 30, 0),
                    CloseTime = new System.DateTime(2019, 2, 17, 11, 30, 0),
                    GroupId = 1
                };
                //action
                OrganizerController controller = new OrganizerController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.EditDraftEvent(input, "Save draft");
                //assert
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }

        [TestMethod]
        public void DeleteDraftEventTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User duyen = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(duyen);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.DeleteDraftEvent(1);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void DeleteDraftEventTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User duyen = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(duyen);
            db.UserGroups.Add(new UserGroup() { GroupId = 1, OrganizerId = 15 });
            db.Groups.Add(new Group() { GroupId = 1, GroupName = "CLB Cờ" });
            Event ev = new Event() {
                EventId = 1,
                OrganizerId = 15,
                GroupId = 1,
                EventName = "Toi la ai",
                EventStatus = "Draft"
            };
            db.Events.Add(ev);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            RedirectToRouteResult result = (RedirectToRouteResult)controller.DeleteDraftEvent(1);
            //assert
            Assert.AreEqual("ViewYourDraftEvents", result.RouteValues["action"]);
        }

        [TestMethod]
        public void EditDraftEventTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            RedirectToRouteResult result = (RedirectToRouteResult)controller.EditDraftEvent(null);
            //assert
            Assert.AreEqual("ViewYourDraftEvents", result.RouteValues["action"]);
            Assert.AreEqual("Organizer", result.RouteValues["controller"]);
        }

        //[TestMethod]
        public void CreateFormTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            //action
            Task<ActionResult> result = controller.CreateForm() as Task<ActionResult>;
            Assert.IsNotNull(result.Result);
        }

        [TestMethod]
        public void ViewYourDraftEventsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            Event event2 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Happening",
                EventName = "Event 2"
            };
            db.Events.Add(event2);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourDraftEvents(1);
            List<EventModel> resultData = (List<EventModel>)result.ViewData["Drafts"];

            Assert.AreEqual(1, resultData.Count);
            Assert.AreEqual("Event 1", resultData[0].Name);

        }

        [TestMethod]
        public void ExportYourEventStatisticsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 11,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            Task<ActionResult> result = controller.ExportYourEventStatistics(123);
            ViewResult resultData = (ViewResult)result.Result;
            //assert
            Assert.AreEqual("NotFound", resultData.ViewName);
        }

        [TestMethod]
        public void ViewYourCheckedinStudentsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 11,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            //input 
            UserSearchViewModel input = new UserSearchViewModel() { Search = "abc" };
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourCheckedinStudents(123, 1, input);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewYourCheckedinStudentsTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Open",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            //input 
            UserSearchViewModel input = new UserSearchViewModel() { Search = "abc" };
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourCheckedinStudents(123, 1, input);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewYourCheckedinStudentsTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Closed",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            db.Checkins.Add(new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong" } } });
            //input 
            UserSearchViewModel input = new UserSearchViewModel() { Search = "abc" };
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourCheckedinStudents(123, 1, input);
            //assert
            Assert.AreEqual(123, result.ViewData["EventId"]);
            Assert.IsNotNull(result.ViewData["Checkins"]);
        }

        [TestMethod]
        public void ViewYourRegisteredStudentsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 11,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            //input 
            UserSearchViewModel input = new UserSearchViewModel() { Search = "abc" };
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourRegisteredStudents(123, 1, input);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewYourRegisteredStudentsTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Open",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            //input 
            UserSearchViewModel input = new UserSearchViewModel() { Search = "abc" };
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourRegisteredStudents(123, 1, input);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewYourRegisteredStudentsTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Closed",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            db.Registers.Add(new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong" } } });
            //input 
            UserSearchViewModel input = new UserSearchViewModel() { Search = "abc" };
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourRegisteredStudents(123, 1, input);
            //assert
            Assert.AreEqual(123, result.ViewData["EventId"]);
            Assert.IsNotNull(result.ViewData["Registers"]);
        }

        [TestMethod]
        public void ViewYourEventStatisticsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 11,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourEventStatistics(123);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewYourEventStatisticsTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourEventStatistics(1);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void ViewYourEventStatisticsTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourEventStatistics(123);
            Assert.IsNotNull(result.ViewData);
        }

        [TestMethod]
        public void ViewYourEventStatisticsTest3() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);

            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user1);
            List<Register> registers = new List<Register>();
            Register register1 = new Register() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            registers.Add(register1);
            List<Checkin> checkins = new List<Checkin>();
            Checkin checkin = new Checkin() { EventId = 123, UserId = 11, User = new User() { UserId = 11, UserProfile = new UserProfile() { ProfileId = 1, LastName = "Huong", City = "Ha noi", Campus = "Hoa lac", Major = "IT", Specialization = "JS", Gender = true } } };
            checkins.Add(checkin);
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Opening",
                EventName = "Event 1",
                ApprovedDate = new System.DateTime(2019, 4, 12),
                Registers = registers,
                Checkins = checkins
            };
            db.Events.Add(event1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewYourEventStatistics(123);
            Assert.IsNotNull(result.ViewData);
            Assert.AreEqual(null, result.ViewData["Checkin"]);
            Assert.AreEqual("[]", result.ViewData["CheckinDateLabels"]);
            Assert.AreEqual("[]", result.ViewData["CheckinDateData"]);
            Assert.AreEqual("[]", result.ViewData["CheckinGenderData"]);
            Assert.AreEqual("[]", result.ViewData["CheckinCityLabels"]);
            Assert.AreEqual("[]", result.ViewData["CheckinCityData"]);
            Assert.AreEqual("[]", result.ViewData["CheckinCampusLabels"]);
            Assert.AreEqual("[]", result.ViewData["CheckinCampusData"]);
            Assert.AreEqual("[]", result.ViewData["CheckinMajorLabels"]);
            Assert.AreEqual("[]", result.ViewData["CheckinMajorData"]);
            Assert.AreEqual("[]", result.ViewData["CheckinSpecializationLabels"]);
            Assert.AreEqual("[]", result.ViewData["CheckinSpecializationData"]);
        }

        [TestMethod]
        public void ViewAllYourEventsTest() {
            //input 
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = "abc", Search = "abc" };
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            OrganizerController controller = new OrganizerController(db);
            ViewResult result = (ViewResult)controller.ViewAllYourEvents(1, input);
            //assert
            Assert.AreEqual("Error", result.ViewName);
        }

        [TestMethod]
        public void ViewAllYourEventsTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            Event event1 = new Event() {
                EventId = 123,
                OrganizerId = 15,
                EventStatus = "Draft",
                EventName = "Event 1"
            };
            db.Events.Add(event1);
            Group group1 = new Group() {
                GroupId = 1,
                GroupName = "Câu lạc bộ Cờ",
                GroupImage = "img",
                GroupDescription = "Hello world",
                Leader = new User() {
                    UserProfile = new UserProfile() {
                        LastName = "huong",
                        FirstName = "dinh"
                    },
                    Email = "huong@gm.com"
                },
                GroupMail = "co@gm.com",
                IsEnabled = true
            };
            db.Groups.Add(group1);
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //input 
            EventSearchFilterViewModel input = new EventSearchFilterViewModel() { Status = null, Search = "abc" };
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            ViewResult result = (ViewResult)controller.ViewAllYourEvents(1, input);
            //assert
            Assert.IsNotNull(result.ViewData["Events"]);
        }

        [TestMethod]
        public void IndexTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            OrganizerController controller = new OrganizerController(db) {
                ControllerContext = mockContext.Object
            };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Index();
            //assert
            Assert.AreEqual("Login", result.RouteValues["action"]);
            Assert.AreEqual("Authentication", result.RouteValues["controller"]);
        }
    }
}