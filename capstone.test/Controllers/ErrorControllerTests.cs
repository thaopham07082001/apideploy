﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace capstone.Controllers.Tests {
    [TestClass]
    public class ErrorControllerTests {
        [TestMethod]
        public void IndexTest() {
            ErrorController controller = new ErrorController();
            ViewResult result = (ViewResult)controller.Index();
            Assert.AreEqual("Error", result.ViewName);
        }

        [TestMethod]
        public void NotFoundTest() {
            ErrorController controller = new ErrorController();
            ViewResult result = (ViewResult)controller.NotFound();
            Assert.AreEqual("NotFound", result.ViewName);
        }
    }
}