﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using capstone.database.Entities;
using capstone.Models;
using capstone.test.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace capstone.Controllers.Tests {
    [TestClass]
    public class AdminControllerTests {
        [TestMethod]
        public void IndexTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            AdminController controller = new AdminController(db) {
                ControllerContext = mockContext.Object
            };
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Index();
            //assert
            Assert.AreEqual("Login", result.RouteValues["action"]);
            Assert.AreEqual("Authentication", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void ViewAllAccountsTest() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            AdminController controller = new AdminController(db) {
                ControllerContext = mockContext.Object
            };
            controller.ModelState.AddModelError("key", "error message");
            AccountSearchFilterViewModel input = new AccountSearchFilterViewModel() { };
            ViewResult result = (ViewResult)controller.ViewAllAccounts(1, input);
            //assert
            Assert.AreEqual("Error", result.ViewName);
        }

        [TestMethod]
        public void ViewAllAccountsTest1() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            User user2 = new User() {
                UserId = 11,
                Email = "linh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user2);
            //action
            AdminController controller = new AdminController(db) {
                ControllerContext = mockContext.Object
            };
            AccountSearchFilterViewModel input = new AccountSearchFilterViewModel() {
                IsAdmin = false,
                IsManager = false,
                IsOrganizer = false,
                Search = ""
            };
            ViewResult result = (ViewResult)controller.ViewAllAccounts(1, input);
            List<AccountModel> data = (List<AccountModel>)result.ViewData["Accounts"];
            //assert
            Assert.AreEqual(1, data.Count);
        }

        [TestMethod]
        public void ViewAllAccountsTest2() {
            // create mock principal
            MockRepository mocks = new MockRepository(MockBehavior.Default);
            Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
            mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
            mockPrincipal.SetupGet(p => p.Identity.IsAuthenticated).Returns(false);
            mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

            // create mock controller context
            Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
            mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
            mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            AdminController controller = new AdminController(db) {
                ControllerContext = mockContext.Object
            };
            controller.ModelState.AddModelError("key", "error message");
            AccountSearchFilterViewModel input = new AccountSearchFilterViewModel() { };
            ViewResult result = (ViewResult)controller.ViewAllAccounts(1, input);
            //assert
            Assert.AreEqual("Error", result.ViewName);
        }

        [TestMethod]
        public void CreateAccountTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.CreateAccount();
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void CreateAccountTest1() {
            //input 
            CreateAccountViewModel input = new CreateAccountViewModel() {
                Email = null,
                IsOrganizer = false,
                IsAdmin = false,
                IsManager = false,
                IsStudent = true
            };
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.CreateAccount(input);
            Assert.AreEqual("You must enter @fpt.edu.vn email.", result.ViewData.ModelState["Email"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void CreateAccountTest2() {
            //input 
            CreateAccountViewModel input = new CreateAccountViewModel() {
                Email = "huong@gmail.vn",
                IsOrganizer = false,
                IsAdmin = false,
                IsManager = false,
                IsStudent = true
            };
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.CreateAccount(input);
            Assert.AreEqual("You must enter @fpt.edu.vn email.", result.ViewData.ModelState["Email"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void CreateAccountTest3() {
            //input 
            CreateAccountViewModel input = new CreateAccountViewModel() {
                Email = "huong@fpt.edu.vn",
                IsOrganizer = false,
                IsAdmin = false,
                IsManager = false,
                IsStudent = false
            };
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.CreateAccount(input);
            Assert.AreEqual("You must select at least 1 role.", result.ViewData.ModelState["Roles"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void CreateAccountTest4() {
            //input 
            CreateAccountViewModel input = new CreateAccountViewModel() {
                Email = "huong@gmail.vn",
                IsOrganizer = false,
                IsAdmin = false,
                IsManager = false,
                IsStudent = true
            };
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.CreateAccount(input);
            Assert.AreEqual("You must enter @fpt.edu.vn email.", result.ViewData.ModelState["Email"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void LoginAsAccountTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.LoginAsAccount();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void EditAccountTest() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.EditAccount(11);
            //assert
            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void EditAccountTest1() {
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            User user1 = new User() {
                UserId = 15,
                Email = "duyennh@gmail.com"
            };
            db.Users.Add(user1);
            User user2 = new User() {
                UserId = 11,
                Email = "linh@gmail.com",
                IsOrganizer = true
            };
            db.Users.Add(user2);
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.EditAccount(15);
            EditAccountViewModel data = (EditAccountViewModel)result.ViewData.Model;
            //assert
            Assert.AreEqual(15, data.Id);
            Assert.AreEqual("duyennh@gmail.com", data.Email);
        }

        [TestMethod]
        public void EditAccountTest2() {
            //input 
            EditAccountViewModel input = new EditAccountViewModel() {
                Email = "huong@fpt.edu.vn",
                IsOrganizer = false,
                IsAdmin = false,
                IsManager = false,
                IsStudent = false
            };
            //data
            TestCapstoneContext db = new TestCapstoneContext();
            //action
            AdminController controller = new AdminController(db);
            ViewResult result = (ViewResult)controller.EditAccount(input);
            Assert.AreEqual("You must select at least 1 role", result.ViewData.ModelState["Roles"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EditAccountTest3() {
            using (new FakeHttpContext.FakeHttpContext()) {
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                //input 
                EditAccountViewModel input = new EditAccountViewModel() {
                    Email = "huong@fpt.edu.vn",
                    IsOrganizer = false,
                    IsAdmin = false,
                    IsManager = false,
                    IsStudent = true,
                    Id = 10
                };
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                User user2 = new User() {
                    UserId = 12,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                AdminController controller = new AdminController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.EditAccount(input);
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }

        [TestMethod]
        public void EditAccountTest4() {
            using (new FakeHttpContext.FakeHttpContext()) {
                // create mock principal
                MockRepository mocks = new MockRepository(MockBehavior.Default);
                Mock<IPrincipal> mockPrincipal = mocks.Create<IPrincipal>();
                mockPrincipal.SetupGet(p => p.Identity.Name).Returns("duyennh@gmail.com");
                mockPrincipal.Setup(p => p.IsInRole("User")).Returns(true);

                // create mock controller context
                Mock<ControllerContext> mockContext = new Mock<ControllerContext>();
                mockContext.SetupGet(p => p.HttpContext.User).Returns(mockPrincipal.Object);
                mockContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
                User user1 = new User() {
                    UserId = 15,
                    Email = "duyennh@gmail.com",
                    IsOrganizer = true
                };
                HttpContext.Current.Session["User"] = user1;
                //input 
                EditAccountViewModel input = new EditAccountViewModel() {
                    Email = "huong@fpt.edu.vn",
                    IsOrganizer = false,
                    IsAdmin = false,
                    IsManager = false,
                    IsStudent = true,
                    Id = 10
                };
                //data
                TestCapstoneContext db = new TestCapstoneContext();
                User user2 = new User() {
                    UserId = 15,
                    Email = "linh@gmail.com"
                };
                db.Users.Add(user1);
                db.Users.Add(user2);
                //action
                AdminController controller = new AdminController(db) {
                    ControllerContext = mockContext.Object
                };
                ViewResult result = (ViewResult)controller.EditAccount(input);
                Assert.AreEqual("NotFound", result.ViewName);
            }
        }
    }
}