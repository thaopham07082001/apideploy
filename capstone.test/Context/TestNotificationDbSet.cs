﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestNotificationDbSet : TestDbSet<Notification> {
        public override Notification Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.NotificationId == (int)keyValues.Single());
        }
    }
}