﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestUserGroupDbSet : TestDbSet<UserGroup> {
        public override UserGroup Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.GroupId == (int)keyValues.Single());
        }
    }
}