﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestReportDbSet : TestDbSet<Report> {
        public override Report Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.Id == (int)keyValues.Single());
        }
    }
}