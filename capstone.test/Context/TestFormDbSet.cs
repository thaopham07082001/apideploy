﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestFormDbSet : TestDbSet<Form> {
        public override Form Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.FormId == (int)keyValues.Single());
        }
    }
}