﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestFeedbackOuterDbSet : TestDbSet<FeedbackOuter> {
        public override FeedbackOuter Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.Key.Equals(keyValues.Single().ToString()));
        }
    }
}