﻿using capstone.database.Database;
using capstone.database.Entities;

namespace capstone.test.Context {
    public class TestCapstoneContext : Database {
        public TestCapstoneContext() {
            Groups = new TestGroupDbSet();
            Bookmarks = new TestBookmarkDbSet();
            Forms = new TestFormDbSet();
            Events = new TestEventDbSet();
            Categories = new TestCategoryDbSet();
            UserGroups = new TestUserGroupDbSet();
            Users = new TestUserDbSet();
            Checkins = new TestCheckinDbSet();
            Registers = new TestRegisterDbSet();
            Notifications = new TestNotificationDbSet();
            FeedbackOuters = new TestFeedbackOuterDbSet();
            Reports = new TestReportDbSet();
            Feedbacks = new TestFeedbackDbSet();
        }

        public void MarkAsModified(Group item) { }
    }
}
