﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestBookmarkDbSet : TestDbSet<Bookmark> {
        public override Bookmark Find(params object[] keyValues) {
            return this.SingleOrDefault(bm => bm.EventId == (int)keyValues.Single());
        }
    }
}
