﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestFeedbackDbSet : TestDbSet<Feedback> {
        public override Feedback Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.EventId == (int)keyValues.Single());
        }
    }
}