﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestUserDbSet : TestDbSet<User> {
        public override User Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.UserId == (int)keyValues.Single());
        }
    }
}