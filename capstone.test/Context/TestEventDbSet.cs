﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestEventDbSet : TestDbSet<Event> {
        public override Event Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.EventId == (int)keyValues.Single());
        }
    }
}