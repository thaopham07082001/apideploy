﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestCategoryDbSet : TestDbSet<Category> {
        public override Category Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.CategoryId == (int)keyValues.Single());
        }
    }
}