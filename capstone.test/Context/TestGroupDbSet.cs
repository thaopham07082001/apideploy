﻿using System.Linq;
using capstone.database.Entities;
using capstone.Tests.Context;

namespace capstone.test.Context {
    public class TestGroupDbSet : TestDbSet<Group> {
        public override Group Find(params object[] keyValues) {
            return this.SingleOrDefault(gr => gr.GroupId == (int)keyValues.Single());
        }
    }
}