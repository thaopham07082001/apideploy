﻿using System;

namespace capstone.api.Models {
    public class CalendarApiModel {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}