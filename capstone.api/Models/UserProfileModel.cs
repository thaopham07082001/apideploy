﻿using System;

namespace capstone.api.Models {
    public class UserProfileApiModel {
        public string RollNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public bool Gender { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string ProfileImage { get; set; }
        public string Campus { get; set; }
        public string Major { get; set; }
        public string Specialization { get; set; }
        public string ParentName { get; set; }
        public string ParentPhone { get; set; }
    }
}