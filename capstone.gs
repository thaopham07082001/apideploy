var token = "swp493capstone";

function doGet(e) {
  if (e.parameter.token != token)
    return null;
  if (e.parameter.action == "CreateForm") {
    var form = FormApp.create("Form");
    var sheet = SpreadsheetApp.create("Sheet");
    var response = form.createResponse();
    var phone = FormApp.createTextValidation().setHelpText("You must enter phone number").requireNumber().build();
    var email = FormApp.createTextValidation().setHelpText("You must enter email address").requireTextIsEmail().build();
    form.addTextItem().setTitle("Name").setRequired(true);
    form.addDateItem().setTitle("Date of Birth").setRequired(true);
    form.addTextItem().setTitle("Email").setRequired(true).setValidation(email);
    form.addTextItem().setTitle("Phone").setRequired(true).setValidation(phone);
    form.setShowLinkToRespondAgain(false);
    form.addEditor(e.parameter.email);
    form.setPublishingSummary(true);
    form.setDestination(FormApp.DestinationType.SPREADSHEET, sheet.getId());
    sheet.addViewer(e.parameter.email);
    var items = form.getItems();
    response.withItemResponse(items[0].asTextItem().createResponse("ThangLVSE04854"));
    response.withItemResponse(items[1].asDateItem().createResponse(new Date("1997-01-13")));
    response.withItemResponse(items[2].asTextItem().createResponse("thanglvse04854@fpt.edu.vn"));
    response.withItemResponse(items[3].asTextItem().createResponse("0332554026"));
    var event = {
      formLink: form.getEditUrl(),
      formResult: form.getSummaryUrl()+"?embedded=true",
      formRegister: response.toPrefilledUrl()+"&embedded=true"
    };
    return ContentService.createTextOutput(JSON.stringify(event)).setMimeType(ContentService.MimeType.JSON);
  }
  return null;
}

function doPost(e) {
  if (e.parameter.token != token)
    return null;
  if (e.parameter.action == "ExportEventStatistics") {
    var contents = JSON.parse(e.postData.contents);
    var sheet = SpreadsheetApp.create(contents.info.name);
    var page;
    page = sheet.getSheets()[0];
    page.setName("Event Info");
    page.getRange(1, 1).setValue("Event name");
    page.getRange(1, 2).setValue(contents.info.name);
    page.getRange(2, 1).setValue("Registered Students");
    page.getRange(2, 2).setValue(contents.info.registers);
    page.getRange(3, 1).setValue("Checkedin Students");
    page.getRange(3, 2).setValue(contents.info.checkins);
    page.getRange(4, 1).setValue("Feedbacks");
    page.getRange(4, 2).setValue(contents.info.feedbacks);
    page.getRange(5, 1).setValue("Rating");
    page.getRange(5, 2).setValue(contents.info.rating);
    page.autoResizeColumn(1);
    page.autoResizeColumn(2);
    sheet.insertSheet();
    page = sheet.getSheets()[1];
    page.setName("Registered Students");
    page.getRange(1, 1).setValue("Registered Students");
    page.getRange(2, 1).setValue("Email");
    page.getRange(2, 2).setValue("First name");
    page.getRange(2, 3).setValue("Last name");
    page.getRange(2, 4).setValue("Registered date");
    for (var i = 0; i < contents.registers.length; i++) {
      page.getRange(i+3, 1).setValue(contents.registers[i].Email);
      page.getRange(i+3, 2).setValue(contents.registers[i].FirstName);
      page.getRange(i+3, 3).setValue(contents.registers[i].LastName);
      page.getRange(i+3, 4).setValue(contents.registers[i].RegisteredDate);
    }
    page.autoResizeColumn(1);
    page.autoResizeColumn(2);
    page.autoResizeColumn(3);
    page.autoResizeColumn(4);
    sheet.insertSheet();
    page = sheet.getSheets()[2];
    page.setName("Checkedin Students");
    page.getRange(1, 1).setValue("Checkedin Students");
    page.getRange(2, 1).setValue("Email");
    page.getRange(2, 2).setValue("First name");
    page.getRange(2, 3).setValue("Last name");
    page.getRange(2, 4).setValue("Checkedin date");
    for (var i = 0; i < contents.checkins.length; i++) {
      page.getRange(i+3, 1).setValue(contents.checkins[i].Email);
      page.getRange(i+3, 2).setValue(contents.checkins[i].FirstName);
      page.getRange(i+3, 3).setValue(contents.checkins[i].LastName);
      page.getRange(i+3, 4).setValue(contents.checkins[i].CheckedinDate);
    }
    page.autoResizeColumn(1);
    page.autoResizeColumn(2);
    page.autoResizeColumn(3);
    page.autoResizeColumn(4);
    sheet.insertSheet();
    page = sheet.getSheets()[3];
    page.setName("Feedbacks");
    page.getRange(1, 1).setValue("Feedbacks within FPT University");
    page.getRange(2, 1).setValue("Email");
    page.getRange(2, 2).setValue("First name");
    page.getRange(2, 3).setValue("Last name");
    page.getRange(2, 4).setValue("Stars");
    page.getRange(2, 5).setValue("Feedback");
    page.getRange(2, 6).setValue("Created date");
    for (var i = 0; i < contents.feedbacks.length; i++) {
      page.getRange(i+3, 1).setValue(contents.feedbacks[i].Email);
      page.getRange(i+3, 2).setValue(contents.feedbacks[i].FirstName);
      page.getRange(i+3, 3).setValue(contents.feedbacks[i].LastName);
      page.getRange(i+3, 4).setValue(contents.feedbacks[i].Value);
      page.getRange(i+3, 5).setValue(contents.feedbacks[i].FeedbackContent);
      page.getRange(i+3, 6).setValue(contents.feedbacks[i].CreatedDate);
    }
    page.getRange(contents.feedbacks.length+4, 1).setValue("Feedbacks outside FPT University");
    for (var i = 0; i < contents.feedbackOuter.length; i++) {
      page.getRange(i+contents.feedbacks.length+5, 2).setValue(contents.feedbackOuter[i].UserName);
      page.getRange(i+contents.feedbacks.length+5, 4).setValue(contents.feedbackOuter[i].Value);
      page.getRange(i+contents.feedbacks.length+5, 5).setValue(contents.feedbackOuter[i].FeedbackContent);
      page.getRange(i+contents.feedbacks.length+5, 6).setValue(contents.feedbackOuter[i].CreatedDate);
    }
    page.autoResizeColumn(1);
    page.autoResizeColumn(2);
    page.autoResizeColumn(3);
    page.autoResizeColumn(4);
    page.autoResizeColumn(5);
    page.autoResizeColumn(6);
    sheet.addEditor(e.parameter.email);
    if (contents.form != null) {
      var form = FormApp.openByUrl(contents.form);
      var stats = SpreadsheetApp.openById(form.getDestinationId());
      stats.getSheets()[0].copyTo(sheet).setName("Form Statistics");
    }
    var statistics = sheet.getUrl();
    return ContentService.createTextOutput(statistics).setMimeType(ContentService.MimeType.TEXT);
  }
  return null;
}
