USE master
GO
IF EXISTS(select * from sys.databases where name='SWP493_CAPSTONE')
	DROP DATABASE SWP493_CAPSTONE
GO
CREATE DATABASE SWP493_CAPSTONE
GO
USE SWP493_CAPSTONE
GO

CREATE TABLE UserProfile (
	ProfileId int primary key IDENTITY(1,1),
	RollNumber nvarchar(10),
	FirstName nvarchar(20) NOT NULL,
	LastName nvarchar(50) NOT NULL,
	DOB date,
	Gender bit NOT NULL, -- 1 is Male
	Phone nvarchar(20),
	City nvarchar(100),
	[Address] nvarchar(max),
	ProfileImage nvarchar(max),
	Campus nvarchar(100) NOT NULL,
	Major nvarchar(100),
	Specialization nvarchar(100),
	ParentName nvarchar(100),
	ParentPhone nvarchar(50)
)
GO

CREATE TABLE [User] (
	UserId int primary key IDENTITY(1,1),
	ProfileId int references UserProfile(ProfileId),
	Email nvarchar(30) NOT NULL,
	IsStudent bit NOT NULL DEFAULT 1,
	IsOrganizer bit NOT NULL DEFAULT 0,
	IsManager bit NOT NULL DEFAULT 0,
	IsAdmin bit NOT NULL DEFAULT 0,
	IsEnabled bit NOT NULL DEFAULT 1,
	NotificationSeenDate datetime NOT NULL DEFAULT GETDATE()
)
GO

CREATE TABLE Category (
	CategoryId int primary key IDENTITY(1,1),
	CategoryName nvarchar(50) NOT NULL,
	CategoryDescription nvarchar(max),
	IsEnabled bit NOT NULL DEFAULT 1,
	CreatorId int NOT NULL references [User](UserId),
	CreatedDate datetime NOT NULL DEFAULT GETDATE()
)
GO

CREATE TABLE EventFamily (
	FamilyId int primary key IDENTITY(1,1),
	FamilyName nvarchar(100) NOT NULL
)
GO

CREATE TABLE [Group] (
	GroupId int primary key IDENTITY(1,1),
	GroupName nvarchar(50) NOT NULL,
	GroupImage nvarchar(max),
	GroupDescription nvarchar(max),
	GroupMail nvarchar(100) NULL,
	FoundedYear date NULL,
	IsEnabled bit NOT NULL DEFAULT 1,
	ManagerId int NOT NULL references [User](UserId),
	CreatedDate datetime NOT NULL DEFAULT GETDATE(),
	LeaderId int NOT NULL references [User](UserId),
	AssignedDate datetime NOT NULL DEFAULT GETDATE()
)
GO

CREATE TABLE [Event] (
	EventId int primary key IDENTITY(1,1),
	EventName nvarchar(100) NOT NULL,
	FamilyId int references EventFamily(FamilyId),
	CoverImage nvarchar(max) NOT NULL,
	IsPublic bit NOT NULL DEFAULT 0, --if a event set IsPublic = 1, everyone can see it thought they dont need to log into the system 
	IsFeatured bit NOT NULL DEFAULT 0,
	EventStatus nvarchar(10) NOT NULL DEFAULT 'Pending' CHECK (EventStatus IN ('Draft', 'Pending', 'Cancelled', 'Opening', 'Happening', 'Closed', 'Rejected', 'Deleted')),
	EventDescription nvarchar(max) NOT NULL,
	EventDescriptionHtml nvarchar(max) NOT NULL,
	EventPlace nvarchar(100) NOT NULL,
	EventFee money NOT NULL,
	OrganizerId int references [User](UserId),
	GroupId int references [Group](GroupId),
	CreatedDate datetime NOT NULL DEFAULT GETDATE(),
	ManagerId int references [User](UserId) NULL,
	ApprovedDate datetime,
	RegisterEndDate datetime,
	OpenDate datetime NOT NULL,
	CloseDate datetime NOT NULL,
	RegisterMax int,
	IsOrganizerOnly bit not null default 0,
	Participants nvarchar(max),
	TargetGroup nvarchar(max)
)
GO

CREATE TABLE EventCategory (
	EventId int references [Event](EventId),
	CategoryId int references Category(CategoryId),
	PRIMARY KEY (EventId, CategoryId)
)
GO

CREATE TABLE Form (
	FormId int primary key IDENTITY(1,1),
	EventId int references [Event](EventId),
	FormLink nvarchar(max) NOT NULL,
	FormRegister nvarchar(max) NOT NULL,
	FormResult nvarchar(max) NOT NULL,
	CreatedDate datetime NOT NULL DEFAULT GETDATE()
)
GO

CREATE TABLE Register (
	UserId int references [User](UserId),
	EventId int references [Event](EventId),
	RegisteredDate datetime NOT NULL DEFAULT GETDATE(),
	PRIMARY KEY (UserId, EventId)
)
GO

CREATE TABLE Checkin (
	UserId int references [User](UserId),
	EventId int references [Event](EventId),
	CheckedinDate datetime NOT NULL DEFAULT GETDATE(),
	PRIMARY KEY (UserId, EventId)
)
GO

CREATE TABLE UserGroup (
	OrganizerId int references [User](UserId),
	GroupId int references [Group](GroupId),
	ParticipatedDate datetime NOT NULL DEFAULT GETDATE(),
	PRIMARY KEY (GroupId, OrganizerId)
)
GO

CREATE TABLE Feedback (
	UserId int references [User](UserId),
	EventId int references [Event](EventId),
	[Value] float NOT NULL,
	CreatedDate datetime NOT NULL DEFAULT GETDATE(),
	FeedbackContent nvarchar(max),
	PRIMARY KEY (UserId, EventId)
)
GO

CREATE TABLE FeedbackOuter (
	UserName nvarchar(max),
	UserImage nvarchar(max),
	[Key] varchar(200) not null,
	EventId int references [Event](EventId) not null,
	[Value] float NOT NULL,
	CreatedDate datetime NOT NULL DEFAULT GETDATE(),
	FeedbackContent nvarchar(max),
	PRIMARY KEY ([Key], EventId)
)
GO

CREATE TABLE Bookmark (
	UserId int references [User](UserId),
	EventId int references [Event](EventId),
	CreatedDate datetime NOT NULL DEFAULT GETDATE(),
	PRIMARY KEY (UserId, EventId)
)
GO

CREATE TABLE Report (
	Id int primary key IDENTITY(1,1),
	UserId int references [User](UserId) not null,
	EventId int references [Event](EventId) not null,
	CreatedDate datetime NOT NULL DEFAULT GETDATE(),
	[Description] nvarchar(max),
	IsDismissed bit not null default 0
)
GO

CREATE TABLE [Notification] (
	NotificationId int primary key IDENTITY(1,1),
	UserId int references [User](UserId) not null,
	SubjectId int references [User](UserId),
	ObjectId int,
	ObjectType nvarchar(10),
	NotificationContent nvarchar(max),
	NotifiedDate datetime not null DEFAULT GETDATE()
)
GO

INSERT INTO UserProfile  (RollNumber, DOB,  Gender, FirstName, LastName, ProfileImage, Major, Campus, ParentName, ParentPhone, Phone, City, Address, Specialization)
VALUES ('SE04823', '1997-05-26', 0, N'Yên', N'Luyện Thị', '/Images/Students/YenLTSE04823.jpg', N'Software Engineering', N'Hòa Lạc', N'Luyện Quang Tuyển','0906663717','0835713075', N'Hà Tĩnh', N'17 Chùa Cao', 'Japanese Software');
INSERT INTO UserProfile  (RollNumber, DOB, Gender, FirstName, LastName, ProfileImage, Major, Campus, ParentName, ParentPhone, Phone, City, Address, Specialization)
VALUES ('SE04854', '1997-01-13', 1, N'Thắng', N'Lê Việt', '/Images/Students/ThangLVSE04854.jpg', N'Software Engineering', N'Hòa Lạc', N'Lê Tuấn Kiệt','0911523717','0835333075', N'Hà Nội', N'22 Chu Văn An', 'Japanese Software');
INSERT INTO UserProfile  (RollNumber, DOB, Gender, FirstName, LastName, ProfileImage, Major, Campus, ParentName, ParentPhone, Phone, City, Address, Specialization)
VALUES ('SE05113', '1997-08-31', 1, N'Phương', N'Nguyễn Minh', '/Images/Students/PhuongNMSE05113.jpg', N'Software Engineering', N'Hòa Lạc', N'Nguyễn Minh Đức','0906523717','0892513075', N'Hà Nội', N'37 Chùa Thấp', 'Japanese Software');
INSERT INTO UserProfile  (RollNumber, DOB, Gender, FirstName, LastName, ProfileImage, Major, Campus, ParentName, ParentPhone, Phone, City, Address, Specialization)
VALUES ('SE05123', '1997-01-27', 0, N'Hương', N'Đinh Lan', '/Images/Students/HuongDLSE05123.jpg', N'Software Engineering', N'Hòa Lạc', N'Đinh Bộ Lĩnh','0906523717','0115713075', N'Hà Nội', N'12 Nguyễn Huệ', 'Japanese Software');
INSERT INTO UserProfile (RollNumber, DOB, Gender, FirstName, LastName, ProfileImage, Major, Campus, ParentName, ParentPhone, Phone, City, Address, Specialization)
VALUES ('SE05164', '1997-02-20', 0, N'Duyên', N'Nguyễn Thị Mỹ', '/Images/Students/DuyenNTMSE05164.jpg', N'Software Engineering', N'Hòa Lạc', N'Nguyễn Thị Ngọc Lan','0906523717','0835713075', N'Huế', N'66 Nguyễn Phúc Nguyên', 'Japanese Software');

INSERT INTO [User](Email, ProfileId, IsEnabled, IsStudent, IsOrganizer, IsManager, IsAdmin) VALUES ('yenltse04823@fpt.edu.vn', 1, 1, 1, 1, 1, 1);
INSERT INTO [User](Email, ProfileId, IsEnabled, IsStudent, IsOrganizer, IsManager, IsAdmin) VALUES ('thanglvse04854@fpt.edu.vn', 2, 1, 1, 1, 1, 1);
INSERT INTO [User](Email, ProfileId, IsEnabled, IsStudent, IsOrganizer, IsManager, IsAdmin) VALUES ('phuongnmse05113@fpt.edu.vn', 3, 1, 1, 1, 1, 1);
INSERT INTO [User](Email, ProfileId, IsEnabled, IsStudent, IsOrganizer, IsManager, IsAdmin) VALUES ('huongdlse05123@fpt.edu.vn', 4, 1, 1, 1, 1, 1);
INSERT INTO [User](Email, ProfileId, IsEnabled, IsStudent, IsOrganizer, IsManager, IsAdmin) VALUES ('duyenntmse05164@fpt.edu.vn', 5, 1, 1, 1, 1, 1);

INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Softskill workshop',NULL,1, GETDATE());
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Recruitment workshop',NULL,1, GETDATE());
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Orientation workshop',NULL,1, GETDATE());
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Learning workshop',NULL,1, GETDATE());
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Competition',NULL,1, GETDATE());
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Music',NULL,1, GETDATE());
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Field trip',NULL,1, GETDATE());
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Traditional Festival',NULL,1, GETDATE())
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Learning exchange',NULL,1, GETDATE())
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Recruitement',NULL,1, GETDATE())
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Internal culture',NULL,1, GETDATE())
INSERT INTO Category(CategoryName,CategoryDescription,CreatorId,CreatedDate)
VALUES(N'Teambuilding',NULL,1, GETDATE())

INSERT INTO EventFamily(FamilyName) VALUES (N'Miss FPTU')
INSERT INTO EventFamily(FamilyName) VALUES (N'Chess Workshop- Basics Chess')
INSERT INTO EventFamily(FamilyName) VALUES (N'Tết dân gian')
INSERT INTO EventFamily(FamilyName) VALUES (N'Night Concert')
INSERT INTO EventFamily(FamilyName) VALUES (N'Học kỳ Nhật bản')


INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Câu lạc bộ Cờ',
N'FU Chess Club - CLB Cờ
Chính thức thành lập vào ngày 30/07/2012 bởi chủ nhiệm đời đầu là anh Trần Hoàng Vy (cựu sinh viên K7), CLB Cờ (FCC) đã bắt đầu sinh hoạt với 30 thành viên đầu tiên. Từ lúc bắt đầu đi vào hoạt động chính thức, CLB đã trải qua rất nhiều khó khăn khi những buổi sinh hoạt đầu tiên mọi người phải sinh hoạt ở thư viện và thiếu nhiều cơ sở vật chất để các thành viên có thể giao lưu, sinh hoạt cùng nhau. Nhưng với ý tưởng tìm những người có chung sở thích và để nâng cao khả năng của chính mình, mọi người đã đến với nhau và đã xây dựng nên một cộng đồng người yêu cờ ở FU.
Thế rồi trải qua gần 4 năm phát triển, CLB Cờ đã từng bước được chuyên môn hóa và phát triển hơn, đã có thời gian số thành viên của CLB lên đến hơn 100 người, sinh hoạt nhiều nhất gần 50 người (buổi họp), quỹ CLB có lúc đạt ngưỡng 6 triệu và có nhiều học kỳ CLB đạt được danh hiệu Xuất sắc của trường trong đó có 2 kỳ Spring và Summer 2013, kỳ Spring 2015, và Spring 2016 . FCC đạt được thành tích là 1 trong 5 CLB Xuất sắc của trường đánh dấu những bước tiến vững chắc của CLB.
Ngoài ra, CLB Cờ là nơi tập trung và nung đúc những tài năng trẻ, đưa những kỳ thủ trẻ đi cọ sát và giành những giải thưởng lớn khắp miền Bắc. Trong đó, nổi bật là kiện tướng quốc gia Đồng Bảo Nghĩa (cựu sinh viên) , anh đã giành rất nhiều giải thưởng lớn có quy mô toàn quốc và châu lục trong màu áo của đội tuyển Bắc Giang hay đội tuyển Việt Nam. Đối với trường ĐH FPT, gần đây nhất, anh đã giành huy chương Vàng tại Đại hội Thể dục thể thao sinh viên toàn quốc 2015 được tổ chức tại Bắc Ninh. Ngoài ra, ta không thể không kể đến những cái tên như Phạm Minh Đức (cựu sinh viên K7), Nguyễn Huy Hoàng (sinh viên K8), Lê Hà Phan (sinh viên K10), Mỹ Duyên (K12)… Đấy là những cái tên có tiếng nổi lên ở các giải đấu cờ không chuyên có quy mô lớn ở Hà Nội. 
Ở trường ĐH FPT, hằng năm CLB Cờ chính là đơn vị tổ chức 2 giải Cờ chính, trong đó một giải cờ thường tổ chức vào kỳ Fall là giải Chess Tournament và một giải được tổ chức vào kỳ Spring, được gọi là giải Khai Xuân Kỳ Hội. Đây là môi trường mà các kỳ thủ của trường ĐH FPT và Fschool có thể tranh tài với nhau và tạo điều kiện cho chính FCC tìm kiếm ra những tài năng trẻ. Nội dung của các giải đấu đều rất đa dạng khi có các loại cờ khác nhau từ những môn Cờ rất cổ điển, cần suy nghĩ cho đến những môn Cờ mang tính giải trí cao như: cờ Vua, cờ Tướng, cờ Caro, cờ Domino, cờ Cá Ngựa,… Tất cả tạo nên những điều đặc biệt trong các giải đấu mở rộng này và tạo sự thu hút lớn cho cộng đồng trường ĐH FPT, Hòa Lạc với bình quân có từ 100-150 kỳ thủ tham gia các giải đấu. Trong tương lai, CLB Cờ sẽ nâng cao chất lượng giải đấu hơn với hi vọng thu hút được nhiều kỳ thủ tham gia hơn cũng như tạo ra một sân chơi trí tuệ và vô cùng bổ ích cho sinh viên, học sinh sau những giờ học căng thẳng.'
,'/Images/Groups/FCC.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Câu lạc bộ No Shy',NULL,'/Images/Groups/NSC.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Phòng phát triển cá nhân', NULL, '/Images/Groups/PDP.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Phòng công tác sinh viên SRO', NULL, '/Images/Groups/SRO.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Câu lạc bộ FTIC', NULL, '/Images/Groups/FTIC.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Câu lạc bộ Cóc Đọc và Những người bạn', NULL, '/Images/Groups/CocDoc.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Câu lạc bộ Vì Cộng Đồng iGo', NULL, '/Images/Groups/iGo.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Câu lạc bộ Kỹ sư Cầu nối Nhật Bản - JS', NULL, '/Images/Groups/JS.jpg', 1, 3)
INSERT INTO [Group](GroupName,GroupDescription,GroupImage, ManagerId, LeaderId)
VALUES(N'Câu lạc bộ Guitar - FGC', NULL, '/Images/Groups/FGC.jpg', 1, 3)

INSERT INTO [Event](ManagerId, FamilyId, EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1, 2, 0, N'Tết Dân Gian 2019','/Images/Events/SPRING2019_TetDanGian.jpg', 0, 'Closed', N'[TẾT DÂN GIAN 2019]
Vậy là một năm nữa lại sắp qua. Cái lạnh của những ngày cuối năm chẳng thể làm giảm đi sự ồn ào, tấp nập của đường phố, sự rạng rỡ của các loài hoa đua nhau khoe sắc, và sự háo hức, phấn khởi chờ đón một năm mới đầy sung túc, hy vọng...
Hoà chung với không khí đó chúng mình có một món quà nhỏ, dành cho tất cả cư dân xứ sở Hola đây:
CHƯƠNG TRÌNH TẾT DÂN GIAN – SẮC XUÂN 2019
Hãy cùng tham gia Sắc Xuân 2019 cùng chúng mình để: 
+ Hoà mình vào không khí náo nhiệt Tết đến Xuân về
+ Cùng trải nghiệm những trò chơi thú vị như đập niêu, bắt vịt, limbo...và nhận những phần quà đặc biệt từ BTC
+ Thưởng thức những món ăn phong phú, đa dạng đến từ các gian hàng đặc sắc của các thần dân vô cùng tài năng xứ sở Hola 
+ Cùng nhau (học) gói bánh chưng - một phong tục không thể thiếu trong ngày Tết Việt
+ Và điều thú vị hơn, đến với Sắc Xuân 2019 các thần dân sẽ có cơ hội “khi đi thì lẻ bóng khi về sẽ có đôi” đó nhé.
Còn đợi chờ gì nữa, các bạn đã sẵn sàng đón Tết trên vương quốc HoLa của chúng mình chưa nào???!
--------------------
THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:
Thời gian: Thứ Sáu - ngày 18/1/2019
Địa điểm: Khuôn viên đại học FPT

#Tếtdângian
#Sắcxuân 
#FPTU
#ICPDP 
#CoreTeamk14', N'[TẾT DÂN GIAN 2019]
Vậy là một năm nữa lại sắp qua. Cái lạnh của những ngày cuối năm chẳng thể làm giảm đi sự ồn ào, tấp nập của đường phố, sự rạng rỡ của các loài hoa đua nhau khoe sắc, và sự háo hức, phấn khởi chờ đón một năm mới đầy sung túc, hy vọng...
Hoà chung với không khí đó chúng mình có một món quà nhỏ, dành cho tất cả cư dân xứ sở Hola đây:
CHƯƠNG TRÌNH TẾT DÂN GIAN – SẮC XUÂN 2019
Hãy cùng tham gia Sắc Xuân 2019 cùng chúng mình để: 
+ Hoà mình vào không khí náo nhiệt Tết đến Xuân về
+ Cùng trải nghiệm những trò chơi thú vị như đập niêu, bắt vịt, limbo...và nhận những phần quà đặc biệt từ BTC
+ Thưởng thức những món ăn phong phú, đa dạng đến từ các gian hàng đặc sắc của các thần dân vô cùng tài năng xứ sở Hola 
+ Cùng nhau (học) gói bánh chưng - một phong tục không thể thiếu trong ngày Tết Việt
+ Và điều thú vị hơn, đến với Sắc Xuân 2019 các thần dân sẽ có cơ hội “khi đi thì lẻ bóng khi về sẽ có đôi” đó nhé.
Còn đợi chờ gì nữa, các bạn đã sẵn sàng đón Tết trên vương quốc HoLa của chúng mình chưa nào???!
--------------------
THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:
Thời gian: Thứ Sáu - ngày 18/1/2019
Địa điểm: Khuôn viên đại học FPT

#Tếtdângian
#Sắcxuân 
#FPTU
#ICPDP 
#CoreTeamk14', N'Khuôn viên Đại học FPT, Hòa Lạc', '2019-01-01 19:30:00',5,'2019-01-18 8:00:00','2019-01-18 18:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(1,6)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(1,8)

INSERT INTO Event(ManagerId, FamilyId, EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 3, 1, N'Spring Concert Night - Khúc Ca Xuân','/Images/Events/SPRING2019_SpringConcertNight.jpg', 0, 'Closed', 
N'[SPRING CONCERT] KHÚC CA XUÂN
SPRING CONCERT là một món quà đặc biệt chào đón Xuân Kỷ Hợi 2019 của phòng Hợp tác Quốc tế và Phát triển cá nhân IC-PDP dành tặng cho tất cả các sinh viên của đại học FPT.
Mang một màu sắc âm nhạc mới lạ thông qua sự kết hợp của các nhạc cụ cổ điển, là sự giao thoa giữa các nền văn hoá khác nhau, KHÚC CA XUÂN hứa hẹn sẽ mang đến cho các bạn sinh viên một trải nghiệm vô cùng lý thú và nhiều màu sắc. 
- Thời gian: 19h30 ngày 28/02/2019. 
- Địa điểm: Sảnh Tầng 1 - toà nhà Beta
Hãy cùng chờ đón đêm nhạc có 1-0-2 tại Hola này các bạn nha!',
N'[SPRING CONCERT] KHÚC CA XUÂN
SPRING CONCERT là một món quà đặc biệt chào đón Xuân Kỷ Hợi 2019 của phòng Hợp tác Quốc tế và Phát triển cá nhân IC-PDP dành tặng cho tất cả các sinh viên của đại học FPT.
Mang một màu sắc âm nhạc mới lạ thông qua sự kết hợp của các nhạc cụ cổ điển, là sự giao thoa giữa các nền văn hoá khác nhau, KHÚC CA XUÂN hứa hẹn sẽ mang đến cho các bạn sinh viên một trải nghiệm vô cùng lý thú và nhiều màu sắc. 
- Thời gian: 19h30 ngày 28/02/2019. 
- Địa điểm: Sảnh Tầng 1 - toà nhà Beta
Hãy cùng chờ đón đêm nhạc có 1-0-2 tại Hola này các bạn nha!', N'Sảnh tầng 1 - tòa nhà Beta', '2019-02-20',5,'2019-02-28 19:30:00','2019-02-28 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(2,6)

INSERT INTO Event(ManagerId,FamilyId, EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1,4,100000000, N'Học kì tiếng Nhật tại Nhật Bản','/Images/Events/FALL2018_JapaneseCourseAtJapan.jpg', 0, 'Happening', 
N'[BÍ MẬT ĐỂ TRAU DỒI KHẢ NĂNG BẮN TIẾNG NHẬT NHƯ GIÓ CHO SINH VIÊN TRƯỜNG F]
Bạn là sinh viên trường F?
Bạn yêu thích ngôn ngữ và nên văn hóa Nhật Bản ?
Bạn mong muốn được làm việc trong công ty Nhật sau khi tốt nghiệp?
Vậy thì chương trình « Học tiếng Nhật tại Nhật Bản – Japanese in Japan » chính là một cơ hội tuyệt vời dành cho bạn. Chương trình được thiết kế với mục tiêu giúp tăng cường khả năng giao tiếp, trau dồi các kỹ năng nhằm giúp sinh viên có khả năng cạnh tranh tốt trong thị trường nhân lực quốc tế, đặc biệt là thị trường Nhật Bản.
Xuyên suốt chương trình học, bên cạnh các giờ học tiếng Nhật vô cùng lý thú, sinh viên còn được tham gia học môn học bổ trợ cho việc tìm hiểu văn hóa, ngôn ngữ Nhật như : Tiếng Nhật qua văn hóa, Tiếng Nhật qua Anime, Cắm hoa, Trà Đạo…rất hấp dẫn nữa đó nhé.
Đến với chương trình Japanese in Japan lần này, các bạn sẽ có cơ hội được học tập tại một trong 3 trường đại học đó là Đại học Shinshu, Đại học Rissho, và Đại học ngoại ngữ Kyoto.
Thông tin cụ thể về chương trình như sau : 
Thời gian : dự kiến từ tháng 04/2019 đến tháng 08/2019
Địa điểm : ĐH Shinshu Matsumoto Campus, Đại học Rissho - Kumagaya Campus, Đại học ngoại ngữ Kyoto.
Chi phí : Xem cụ thể trong link đăng ký
Đối tượng tham gia: 
Sinh viên chuyên ngành Nhật và các ngành khác, ưu tiên sinh viên có trình độ tiếng Nhật N3 trở lên.
Lưu ý: 
Sinh viên chưa có chứng chỉ N3 sẽ được tham gia vào kỳ thi sát hạch để xếp lớp.
Sinh viên đăng ký trường ĐH Rissho yêu cầu bắt buộc có chứng chỉ N3 trở lên.
Quy định chuyển đổi tương đương:
Kết quả học tập tại Nhật Bản của sinh viên sẽ được xem xét công nhận tương đương & chuyển đổi kết quả sang các môn học tương ứng của chương trình của FPTU. Công nhận tối đa 4 môn trong các môn sau: JPD111, JPD121, JPD131, JPD141, JPD151, JPS212, JPS222, JPD222, JPD223, JPD322, JPD323, JPD 324, JPD 325.
Ngoài ra, sinh viên có nguyện vọng có thể xin đăng kí công nhận và chuyển đổi tương đương cho kì OJT, tương đương 10 tín chỉ, thay cho các môn tiếng Nhật chuyên ngành kể trên.
Thời hạn đăng ký và nộp cọc:
- Link đăng ký: https://goo.gl/forms/dFzbOC2L6g0u1ynf2
- Đăng ký và nộp phí giữ chỗ trước 17:00 ngày 25/11/2018
- Phí giữ chỗ: 20.000.000VNĐ chuyển vào tài khoản trường ĐH FPT theo thông tin:
Số TK : 00006969002
Tên TK: Trường Đại học FPT
Tại NH TMCP Tiên Phong- CN Hoàn Kiếm
Nội dung ghi rõ: ICPDP – Phí tham gia chương trình học tiếng Nhật tại Nhật Bản năm 2018 – Họ và tên, MSSV/CMND

Mọi thông tin chi tiết về chương trình vui lòng liên hệ:
Phòng Hợp tác Quốc tế & Phát triển cá nhân, điện thoại: 024 6680 5910/ 090 172 9173(Gặp C.Trinh) hoặc Email trinhnm3@fe.edu.vn',
N'[BÍ MẬT ĐỂ TRAU DỒI KHẢ NĂNG BẮN TIẾNG NHẬT NHƯ GIÓ CHO SINH VIÊN TRƯỜNG F]
Bạn là sinh viên trường F?
Bạn yêu thích ngôn ngữ và nên văn hóa Nhật Bản ?
Bạn mong muốn được làm việc trong công ty Nhật sau khi tốt nghiệp?
Vậy thì chương trình « Học tiếng Nhật tại Nhật Bản – Japanese in Japan » chính là một cơ hội tuyệt vời dành cho bạn. Chương trình được thiết kế với mục tiêu giúp tăng cường khả năng giao tiếp, trau dồi các kỹ năng nhằm giúp sinh viên có khả năng cạnh tranh tốt trong thị trường nhân lực quốc tế, đặc biệt là thị trường Nhật Bản.
Xuyên suốt chương trình học, bên cạnh các giờ học tiếng Nhật vô cùng lý thú, sinh viên còn được tham gia học môn học bổ trợ cho việc tìm hiểu văn hóa, ngôn ngữ Nhật như : Tiếng Nhật qua văn hóa, Tiếng Nhật qua Anime, Cắm hoa, Trà Đạo…rất hấp dẫn nữa đó nhé.
Đến với chương trình Japanese in Japan lần này, các bạn sẽ có cơ hội được học tập tại một trong 3 trường đại học đó là Đại học Shinshu, Đại học Rissho, và Đại học ngoại ngữ Kyoto.
Thông tin cụ thể về chương trình như sau : 
Thời gian : dự kiến từ tháng 04/2019 đến tháng 08/2019
Địa điểm : ĐH Shinshu Matsumoto Campus, Đại học Rissho - Kumagaya Campus, Đại học ngoại ngữ Kyoto.
Chi phí : Xem cụ thể trong link đăng ký
Đối tượng tham gia: 
Sinh viên chuyên ngành Nhật và các ngành khác, ưu tiên sinh viên có trình độ tiếng Nhật N3 trở lên.
Lưu ý: 
Sinh viên chưa có chứng chỉ N3 sẽ được tham gia vào kỳ thi sát hạch để xếp lớp.
Sinh viên đăng ký trường ĐH Rissho yêu cầu bắt buộc có chứng chỉ N3 trở lên.
Quy định chuyển đổi tương đương:
Kết quả học tập tại Nhật Bản của sinh viên sẽ được xem xét công nhận tương đương & chuyển đổi kết quả sang các môn học tương ứng của chương trình của FPTU. Công nhận tối đa 4 môn trong các môn sau: JPD111, JPD121, JPD131, JPD141, JPD151, JPS212, JPS222, JPD222, JPD223, JPD322, JPD323, JPD 324, JPD 325.
Ngoài ra, sinh viên có nguyện vọng có thể xin đăng kí công nhận và chuyển đổi tương đương cho kì OJT, tương đương 10 tín chỉ, thay cho các môn tiếng Nhật chuyên ngành kể trên.
Thời hạn đăng ký và nộp cọc:
- Link đăng ký: <a href="https://goo.gl/forms/dFzbOC2L6g0u1ynf2">https://goo.gl/forms/dFzbOC2L6g0u1ynf2</a>
- Đăng ký và nộp phí giữ chỗ trước 17:00 ngày 25/11/2018
- Phí giữ chỗ: 20.000.000VNĐ chuyển vào tài khoản trường ĐH FPT theo thông tin:
Số TK : 00006969002
Tên TK: Trường Đại học FPT
Tại NH TMCP Tiên Phong- CN Hoàn Kiếm
Nội dung ghi rõ: ICPDP – Phí tham gia chương trình học tiếng Nhật tại Nhật Bản năm 2018 – Họ và tên, MSSV/CMND

Mọi thông tin chi tiết về chương trình vui lòng liên hệ:
Phòng Hợp tác Quốc tế & Phát triển cá nhân, điện thoại: 024 6680 5910/ 090 172 9173(Gặp C.Trinh) hoặc Email trinhnm3@fe.edu.vn',
N'Nhật Bản','2018-11-15',4,'2019-04-01','2019-08-01')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(3,7)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(3,9)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(3,'2018-11-15','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(5,1,'2018-11-22')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(3,1,'2018-11-23')

INSERT INTO EventFamily(FamilyName) VALUES (N'Newbie trip')
INSERT INTO Event(ManagerId,FamilyId, EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2,5, 450000, N'Newbie trip - Jump into the word','/Images/Events/FALL2018_NewbieTrip.jpg', 0, 'Closed', 
N'[NEWBIE TRIP - JUMP INTO THE WORLD]
Step 1: Beyond borders"" - Món quà đặc biệt dành cho K14

Sau thời gian “chốt” trường đầy cam go và tốn não, các bạn tân sinh viên trường F đã sẵn sàng cho hành trình sắp tới của mình tại FPTU chưa nào???

Như đã hứa hẹn, vào với FPTU các bạn sẽ bắt đầu chuỗi hành trình NEWBIE TRIP - JUMP INTO THE WORLD, một chuỗi hành trình trải nghiệm học tập, văn hóa tại nước ngoài dành cho các bạn sinh viên K14 nhằm giúp các bạn không chỉ trau dồi kiến thức, trải nghiệm văn hóa mà còn cải thiện các kỹ năng của bản thân. Và để mở màn cho chuỗi hành trình này, IC-PDP xin gửi tới các bạn chương trình “Step 1: Beyond borders”- một món quà đầy bất ngờ nhằm giúp các bạn cóc con xả stress sau thời gian căng não “chốt” trường để chuẩn bị sẵn sàng cho hành trình sắp tới.

Đến với chương trình “Step 1: Beyond border” lần này, các bạn cóc con K14 sẽ có cơ hội được tìm hiểu, trải nghiệm văn hóa và cuộc sống tại Sapa, Lào Cào cũng như tìm hiểu những nét văn hóa đặc sắc của nhân dân nước bạn tại thị trấn Hà Khẩu, Vân Nam, Trung Quốc.

Còn chần chừ gì nữa xách balo lên và bắt đầu hành trình JUMP INTO THE WORLD nào các bạn tân sinh viên trường F ơi.
---------------------------------------------------------------
Thông tin chi tiết chương trình: 
Đối tượng tham dự: Sinh viên Đại học FPT
Số lượng: 35 sinh viên
THỜI GIAN DIỄN RA CHƯƠNG TRÌNH : 25/08/2018 – 27/08/2018
Ưu tiên: tân sinh viên K14.
Link đăng ký: https://goo.gl/forms/ww9TPhog40nHEQfe2
HẠN ĐĂNG KÝ VÀ ĐẶT CỌC: trước 23:00 ngày 21/08/2018
Chi phí: 450.000 VNĐ ( đã bao gồm chi phí ăn, ở, di chuyển và chi phí xuất cảnh sang Trung Quốc)
Nộp trực tiếp tại phòng HB104L hoặc chuyển khoản qua thông tin tài khoản: 
TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_Step1: Beyond borders
THÔNG TIN LIÊN HỆ: Email: anhtp7@fe.edu.vn / Phone: 0978882396 (Phương Anh)',
N'[NEWBIE TRIP - JUMP INTO THE WORLD]
Step 1: Beyond borders"" - Món quà đặc biệt dành cho K14

Sau thời gian “chốt” trường đầy cam go và tốn não, các bạn tân sinh viên trường F đã sẵn sàng cho hành trình sắp tới của mình tại FPTU chưa nào???

Như đã hứa hẹn, vào với FPTU các bạn sẽ bắt đầu chuỗi hành trình NEWBIE TRIP - JUMP INTO THE WORLD, một chuỗi hành trình trải nghiệm học tập, văn hóa tại nước ngoài dành cho các bạn sinh viên K14 nhằm giúp các bạn không chỉ trau dồi kiến thức, trải nghiệm văn hóa mà còn cải thiện các kỹ năng của bản thân. Và để mở màn cho chuỗi hành trình này, IC-PDP xin gửi tới các bạn chương trình “Step 1: Beyond borders”- một món quà đầy bất ngờ nhằm giúp các bạn cóc con xả stress sau thời gian căng não “chốt” trường để chuẩn bị sẵn sàng cho hành trình sắp tới.

Đến với chương trình “Step 1: Beyond border” lần này, các bạn cóc con K14 sẽ có cơ hội được tìm hiểu, trải nghiệm văn hóa và cuộc sống tại Sapa, Lào Cào cũng như tìm hiểu những nét văn hóa đặc sắc của nhân dân nước bạn tại thị trấn Hà Khẩu, Vân Nam, Trung Quốc.

Còn chần chừ gì nữa xách balo lên và bắt đầu hành trình JUMP INTO THE WORLD nào các bạn tân sinh viên trường F ơi.
---------------------------------------------------------------
Thông tin chi tiết chương trình: 
Đối tượng tham dự: Sinh viên Đại học FPT
Số lượng: 35 sinh viên
THỜI GIAN DIỄN RA CHƯƠNG TRÌNH : 25/08/2018 – 27/08/2018
Ưu tiên: tân sinh viên K14.
Link đăng ký: <a href="https://goo.gl/forms/ww9TPhog40nHEQfe2">https://goo.gl/forms/ww9TPhog40nHEQfe2</a>
HẠN ĐĂNG KÝ VÀ ĐẶT CỌC: trước 23:00 ngày 21/08/2018
Chi phí: 450.000 VNĐ ( đã bao gồm chi phí ăn, ở, di chuyển và chi phí xuất cảnh sang Trung Quốc)
Nộp trực tiếp tại phòng HB104L hoặc chuyển khoản qua thông tin tài khoản: 
TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_Step1: Beyond borders
THÔNG TIN LIÊN HỆ: Email: anhtp7@fe.edu.vn / Phone: 0978882396 (Phương Anh)', 
N'Sapa, Lào Cai','2018-08-13',4,'2018-08-25','2018-08-27')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(4,7)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(4,9)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(4,'2018-08-21','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(5,2,'2018-11-22')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(3,2,'2018-11-23')

INSERT INTO EventFamily(FamilyName) VALUES (N'Amazing race')
INSERT INTO Event(ManagerId,FamilyId, EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (3,6, 8000000, N'Cuộc đua kì thú','/Images/Events/FALL2018_AmazingRace.jpg', 0, 'Closed', 
N'[LET’S PHỔ đi Tân Gia Ba and Mã Lệ Tây Á Ở FUHL này thì chỉ có mỗi Phổ là chiều các bạn sinh viên nhất thôi
FU-er nhà mình đã có kế hoạch tránh nóng mà lại học được vô vàn điều bổ ích trong thời gian nghỉ hè chưa?
Nếu bạn cảm thấy khó khăn để vượt qua cái nắng mùa hè. 😘Nếu bạn vẫn đang băn khoăn vì hè này đi đâu cũng chán.
Đừng lo đã có IC-PDP giải nhiệt giúp bạn. Phòng hợp tác quốc tế và phát triền cá nhân IC-PDP xin giới thiệu tới các bạn sinh viên chương trình trải nghiệm du hý Amazing Race Summer 2018.
Thời gian: dự kiến từ ngày 30/06 đến ngày 04/07 (5 ngày 4 đêm).
Đặc biệt, đây là chương trình Amazing Race đầu tiên mà bạn đăng ký tham gia không cần phỏng vấn, miễn là bạn được nghỉ hè bạn sẽ được đi. 
Chỉ tiêu: 25 bạn.
Nhận đơn đăng ký và đặt cọc từ 09/05 đến khi đủ số lượng.
Chi phí tham gia:
+ 10 bạn đặt cọc đầu : 7.500.000 vnđ
+ 15 bạn đặt cọc tiếp theo: 8.000.000 vnđ
Phí đặt cọc: 5.000.000 vnđ
(lệ phí trên bao gồm tiền vé máy bay khứ hồi, tiền phòng và hỗ trợ di chuyển)
Hình thức đăng ký:
B1: Đăng ký tại link: https://goo.gl/forms/GXHMj0cVopK80dHh1
B2: Tiến hành đặt cọc theo 2 cách:
- Nộp tiền mặt cho anh Phạm Anh tại phòng HB104L.
- Chuyển khoản cho chị Phương Anh theo số tài khoản sau:
Chuyển khoản tới số TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_AMR Summer 2018
Mọi thắc mắc liên hệ: 
Phạm Anh (0127 810 0588)
Email: anhpt69@gmail.com
Chậm 1 phút quyền lợi mất đi, còn chần chừ gì nữa mà không đăng ký chứ!
Ghi chú: Thời gian nghỉ hè: từ 30/06 đến ngày 08/07',
N'[LET’S PHỔ đi Tân Gia Ba and Mã Lệ Tây Á Ở FUHL này thì chỉ có mỗi Phổ là chiều các bạn sinh viên nhất thôi
FU-er nhà mình đã có kế hoạch tránh nóng mà lại học được vô vàn điều bổ ích trong thời gian nghỉ hè chưa?
Nếu bạn cảm thấy khó khăn để vượt qua cái nắng mùa hè. 😘Nếu bạn vẫn đang băn khoăn vì hè này đi đâu cũng chán.
Đừng lo đã có IC-PDP giải nhiệt giúp bạn. Phòng hợp tác quốc tế và phát triền cá nhân IC-PDP xin giới thiệu tới các bạn sinh viên chương trình trải nghiệm du hý Amazing Race Summer 2018.
Thời gian: dự kiến từ ngày 30/06 đến ngày 04/07 (5 ngày 4 đêm).
Đặc biệt, đây là chương trình Amazing Race đầu tiên mà bạn đăng ký tham gia không cần phỏng vấn, miễn là bạn được nghỉ hè bạn sẽ được đi. 
Chỉ tiêu: 25 bạn.
Nhận đơn đăng ký và đặt cọc từ 09/05 đến khi đủ số lượng.
Chi phí tham gia:
+ 10 bạn đặt cọc đầu : 7.500.000 vnđ
+ 15 bạn đặt cọc tiếp theo: 8.000.000 vnđ
Phí đặt cọc: 5.000.000 vnđ
(lệ phí trên bao gồm tiền vé máy bay khứ hồi, tiền phòng và hỗ trợ di chuyển)
Hình thức đăng ký:
B1: Đăng ký tại link: <a href="https://goo.gl/forms/GXHMj0cVopK80dHh1">https://goo.gl/forms/GXHMj0cVopK80dHh1</a>
B2: Tiến hành đặt cọc theo 2 cách:
- Nộp tiền mặt cho anh Phạm Anh tại phòng HB104L.
- Chuyển khoản cho chị Phương Anh theo số tài khoản sau:
Chuyển khoản tới số TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_AMR Summer 2018
Mọi thắc mắc liên hệ: 
Phạm Anh (0127 810 0588)
Email: anhpt69@gmail.com
Chậm 1 phút quyền lợi mất đi, còn chần chừ gì nữa mà không đăng ký chứ!
Ghi chú: Thời gian nghỉ hè: từ 30/06 đến ngày 08/07',
N'Tân Gia Ba and Mã Lệ Tây Á','2018-04-30',5,'2018-06-30','2018-07-04')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(5,7)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(5,'2018-05-09','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(5,3,'2018-05-09')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,3,'2018-05-09')

INSERT INTO EventFamily(FamilyName) VALUES (N'Tuyển thành viên No Shy')
INSERT INTO Event(ManagerId,FamilyId, EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2,7, 0, N'Đợt tuyển thành viên CLB No Shy','/Images/Events/SPRING2019_NoShyRecruitement.jpg', 1, 'Closed', 
N'[NO SHY CLUB TUYỂN THÀNH VIÊN KÌ SPRING 2019 ]
‘’Be yourself because an original is worth more than a copy’’ (Tạm dịch: Hãy là chính bạn bởi vì một bản gốc sẽ có giá trị hơn một bản sao). Đã bao giờ trong cuộc sống này, các bạn cảm thấy chán nản vì không được sống là chính mình? Mình tin chắc rằng có một phần lớn trong số chúng ta đã và đang phải trải qua điều đó . Chúng ta đều không sống là chính mình ở nhiều thời điểm để phù hợp với môi trường, với từng trường hợp, với định kiến, và cả vì một quy chuẩn nào đó của xã hội... Để rồi một lúc nào đó bạn vô tình đánh mất chính mình lúc nào không hay, có lẽ vì đã quá lâu bạn không được sống là chính mình. :/

‘’Let your styles shine with us’’ chính là thông điệp mà No Shy Club muốn nhắn gửi tới các bạn trong đợt “Member Recruitment 2019” lần này. Bạn biết đó, cuộc sống này được tạo nên bởi những mảng màu sắc riêng biệt và cũng không kém phần nổi bật. Dù đó có là mảng màu trầm hay sáng, nóng hay lạnh, dù bạn có lỡ đánh rơi nó dọc đường hay có khi đơn giản là bạn vẫn chưa dám để con người bạn được tỏa sáng theo cách mà nó vốn có. Vậy thì, chúng mình sẽ giúp bạn tỏa sáng theo cách bạn muốn nhé. Dù bằng cách nào đi nữa, thì chúng mình cũng sẽ tìm ra và đánh thức con người bạn! Để rồi, hãy cháy hết mình với những mảng màu đó và tỏa sáng theo một cách riêng. Người No Shy chúng mình, thường truyền nhau câu nói: ""Be what you wanna be"", bởi chúng mình tin vào một điều rằng mỗi cá nhân đều có những phẩm chất đáng quý riêng biệt, chúng chỉ thực sự được tỏa sáng khi bạn tự tin là chính mình. Đừng tự ti! Đừng lẩn tránh khiếm khuyết của bản thân! Hay đừng ngại ngùng thể hiện chất riêng của mình. Điều quan trọng bạn cần làm bây giờ, đó chính là tham gia ngay vào gia đình No Shy, chúng mình đang đợi các bạn đó ! <3 <3 <3
-------------------------------------------------------------------------------------------------------------------------------------------------CÁCH THỨC ĐĂNG KÍ 
Link đăng kí:
https://goo.gl/forms/QaOOCYR2AqxSPCTq1
Thời hạn đăng kí: 21h ngày 8/1 – 23h59’ ngày 12/1 
Thời gian phỏng vấn: 15/1 - 18/1

THÔNG TIN LIÊN HỆ 
Facebook: www.facebook.com/NoShyClub 
Email: noshy.fpt@gmail.com
Hotline: 0968 279 588 (Ms Linh)
#NSC
#MemberRecruitment2019
#LetYourStylesShineWithUs
#BeWhatYouWannaBe',
N'[NO SHY CLUB TUYỂN THÀNH VIÊN KÌ SPRING 2019 ]
‘’Be yourself because an original is worth more than a copy’’ (Tạm dịch: Hãy là chính bạn bởi vì một bản gốc sẽ có giá trị hơn một bản sao). Đã bao giờ trong cuộc sống này, các bạn cảm thấy chán nản vì không được sống là chính mình? Mình tin chắc rằng có một phần lớn trong số chúng ta đã và đang phải trải qua điều đó . Chúng ta đều không sống là chính mình ở nhiều thời điểm để phù hợp với môi trường, với từng trường hợp, với định kiến, và cả vì một quy chuẩn nào đó của xã hội... Để rồi một lúc nào đó bạn vô tình đánh mất chính mình lúc nào không hay, có lẽ vì đã quá lâu bạn không được sống là chính mình. :/

‘’Let your styles shine with us’’ chính là thông điệp mà No Shy Club muốn nhắn gửi tới các bạn trong đợt “Member Recruitment 2019” lần này. Bạn biết đó, cuộc sống này được tạo nên bởi những mảng màu sắc riêng biệt và cũng không kém phần nổi bật. Dù đó có là mảng màu trầm hay sáng, nóng hay lạnh, dù bạn có lỡ đánh rơi nó dọc đường hay có khi đơn giản là bạn vẫn chưa dám để con người bạn được tỏa sáng theo cách mà nó vốn có. Vậy thì, chúng mình sẽ giúp bạn tỏa sáng theo cách bạn muốn nhé. Dù bằng cách nào đi nữa, thì chúng mình cũng sẽ tìm ra và đánh thức con người bạn! Để rồi, hãy cháy hết mình với những mảng màu đó và tỏa sáng theo một cách riêng. Người No Shy chúng mình, thường truyền nhau câu nói: ""Be what you wanna be"", bởi chúng mình tin vào một điều rằng mỗi cá nhân đều có những phẩm chất đáng quý riêng biệt, chúng chỉ thực sự được tỏa sáng khi bạn tự tin là chính mình. Đừng tự ti! Đừng lẩn tránh khiếm khuyết của bản thân! Hay đừng ngại ngùng thể hiện chất riêng của mình. Điều quan trọng bạn cần làm bây giờ, đó chính là tham gia ngay vào gia đình No Shy, chúng mình đang đợi các bạn đó ! <3 <3 <3
-------------------------------------------------------------------------------------------------------------------------------------------------CÁCH THỨC ĐĂNG KÍ 
Link đăng kí:
<a href="https://goo.gl/forms/QaOOCYR2AqxSPCTq1">https://goo.gl/forms/QaOOCYR2AqxSPCTq1</a>
Thời hạn đăng kí: 21h ngày 8/1 – 23h59’ ngày 12/1 
Thời gian phỏng vấn: 15/1 - 18/1

THÔNG TIN LIÊN HỆ 
Facebook: www.facebook.com/NoShyClub 
Email: noshy.fpt@gmail.com
Hotline: 0968 279 588 (Ms Linh)
#NSC
#MemberRecruitment2019
#LetYourStylesShineWithUs
#BeWhatYouWannaBe', 
N'Hòa Lạc','2019-01-07',5,'2019-01-15','2018-01-18')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(6,10)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(6,'2019-01-07','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,4,'2019-01-12')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,4,'2019-01-12')

INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1, 1, 0, N'Miss FPTU Hà Nội','/Images/Events/FALL2018_MissFPTUHanoi.jpg', 0, 'Closed', 
N'MISS FPTU HANOI 2018 -“BEAUTY & BEYOND” CHÍNH THỨC TRỞ LẠI 
Cuộc thi Miss FPTU Hà Nội được tổ chức hai năm một lần bởi Hội Sinh viên trường Đại học FPT dưới sự đồng hành và cố vấn của phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP nhằm tìm kiếm, tôn vinh vẻ đẹp, trí tuệ và tài năng của nữ sinh Đại học FPT Hà Nội. 
Miss FPTU Hà Nội 2018 trở lại với chủ đề “Beauty and beyond”, chúng tôi mong muốn trở thành người đồng hành cùng bạn trên hành trình khám phá, trải nghiệm và tỏa sáng vẻ đẹp của chính mình. 
Song hành cùng cuộc thi là sự kiện bên lề Nam sinh đồng hành - nhằm tìm kiếm những gương mặt nam sinh nổi bật và tài năng nhất Đại học FPT. Đây cũng là trải nghiệm tuyệt vời dành cho những chàng trai trên chặng đường khẳng định bản lĩnh của nam nhân trường F.

MISS FPTU HÀ NỘI 2018 -“BEAUTY & BEYOND”

#MissFPTUHanoi
#Beauty_and_beyond
------------------------------
Tiến trình cuộc thi:

Vòng đơn: 11/10/2018 tới 25/10/2018
Sơ khảo Miss: 29,30 và 31/10/2018
Sơ khảo Nam sinh đồng hành: 29,30 và 31/10/2018
Chung kết: 02/12/2018
------------------------------
Đơn đăng ký:

დ Đăng ký Miss: http://bit.ly/missFPTUhanoi2018
დ Đăng ký Nam sinh đồng hành: http://bit.ly/manhuntFPTUhanoi2018

-------------------------------
MISS FPTU HANOI 2018 -“BEAUTY & BEYOND”
Email: missfptu@gmail.com
Page: https://www.facebook.com/missfptu/
Hotline: (+84) 393349954 - Nguyễn Đức Giang',
N'MISS FPTU HANOI 2018 -“BEAUTY & BEYOND” CHÍNH THỨC TRỞ LẠI 
Cuộc thi Miss FPTU Hà Nội được tổ chức hai năm một lần bởi Hội Sinh viên trường Đại học FPT dưới sự đồng hành và cố vấn của phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP nhằm tìm kiếm, tôn vinh vẻ đẹp, trí tuệ và tài năng của nữ sinh Đại học FPT Hà Nội. 
Miss FPTU Hà Nội 2018 trở lại với chủ đề “Beauty and beyond”, chúng tôi mong muốn trở thành người đồng hành cùng bạn trên hành trình khám phá, trải nghiệm và tỏa sáng vẻ đẹp của chính mình. 
Song hành cùng cuộc thi là sự kiện bên lề Nam sinh đồng hành - nhằm tìm kiếm những gương mặt nam sinh nổi bật và tài năng nhất Đại học FPT. Đây cũng là trải nghiệm tuyệt vời dành cho những chàng trai trên chặng đường khẳng định bản lĩnh của nam nhân trường F.

MISS FPTU HÀ NỘI 2018 -“BEAUTY & BEYOND”

#MissFPTUHanoi
#Beauty_and_beyond
------------------------------
Tiến trình cuộc thi:

Vòng đơn: 11/10/2018 tới 25/10/2018
Sơ khảo Miss: 29,30 và 31/10/2018
Sơ khảo Nam sinh đồng hành: 29,30 và 31/10/2018
Chung kết: 02/12/2018
------------------------------
Đơn đăng ký:

დ Đăng ký Miss: <a href="http://bit.ly/missFPTUhanoi2018">http://bit.ly/missFPTUhanoi2018</a>
დ Đăng ký Nam sinh đồng hành: <a href="http://bit.ly/manhuntFPTUhanoi2018">http://bit.ly/manhuntFPTUhanoi2018</a>

-------------------------------
MISS FPTU HANOI 2018 -“BEAUTY & BEYOND”
Email: missfptu@gmail.com
Page: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393349954 - Nguyễn Đức Giang',
N'Hòa Lạc','2018-10-10',5,'2018-10-11 12:00:00','2018-12-02 22:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(7,5)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(7,'2018-10-25','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,5,'2018-10-25')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,5,'2018-10-25')


INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1, 1, 0, N'Miss FPTU Hà Nội - Vòng Chung kết','/Images/Events/FALL2018_MissFPTUHanoi_Chungket.jpg', 0, 'Closed', 
N'CHUNG KẾT MISS FPTU HANOI 2018 
- Beauty and Beyond -

✨ Một trong những sự kiện được mong chờ nhất suốt 3 năm vừa qua
✨ Sân khấu ngoài trời tại bãi biển nhân tạo lớn nhất Hà Nội
✨ Được đầu tư kĩ lưỡng về nội dung cũng như quy mô
✨ Đêm hội tụ sắc đẹp và tài năng của 15 nữ sinh xuất sắc nhất đại học FPT
✨ Sự xuất hiện của rất nhiều những vị khách mời đặc biệt và những nhà tài trợ có uy tín

Hãy đến tham dự và trở thành một phần của Đêm Chung kết để biết:

💥 Các thí sinh sẽ biến hoá mình như thế nào?
💥 Ai sẽ là chủ nhân của chiếc vương miện cao quý?
💥 Còn những điều gì vẫn đang chờ đón các bạn...?

Đừng quên click vào link đăng kí và sở hữu cho mình tấm vé tham dự Đêm Chung kết vào ngày 02/12/2018 tại Baaraland với chúng mình để có được lời giải đáp cho những câu hỏi trên nhé 😍

Link đăng ký tham dự Chung kết: http://bit.ly/MissFPTU_Dangky

----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: https://www.facebook.com/missfptu/
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com',
N'CHUNG KẾT MISS FPTU HANOI 2018 
- Beauty and Beyond -

✨ Một trong những sự kiện được mong chờ nhất suốt 3 năm vừa qua
✨ Sân khấu ngoài trời tại bãi biển nhân tạo lớn nhất Hà Nội
✨ Được đầu tư kĩ lưỡng về nội dung cũng như quy mô
✨ Đêm hội tụ sắc đẹp và tài năng của 15 nữ sinh xuất sắc nhất đại học FPT
✨ Sự xuất hiện của rất nhiều những vị khách mời đặc biệt và những nhà tài trợ có uy tín

Hãy đến tham dự và trở thành một phần của Đêm Chung kết để biết:

💥 Các thí sinh sẽ biến hoá mình như thế nào?
💥 Ai sẽ là chủ nhân của chiếc vương miện cao quý?
💥 Còn những điều gì vẫn đang chờ đón các bạn...?

Đừng quên click vào link đăng kí và sở hữu cho mình tấm vé tham dự Đêm Chung kết vào ngày 02/12/2018 tại Baaraland với chúng mình để có được lời giải đáp cho những câu hỏi trên nhé 😍

Link đăng ký tham dự Chung kết: <a href="http://bit.ly/MissFPTU_Dangky">http://bit.ly/MissFPTU_Dangky</a>

----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com',
N'Hòa Lạc','2018-10-10',5,'2018-12-02 19:30:00','2018-12-02 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(8,5)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(8,'2018-10-25','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,6,'2018-10-25')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,6,'2018-10-25')

INSERT INTO EventFamily(FamilyName) VALUES (N'F-Talent Code 2019')
INSERT INTO Event(ManagerId,FamilyId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (4,8, 0, N'Cuộc thi F Talent Code 2019','/Images/Events/SPRING2019_FTalentCode2019.jpg', 0, 1, 'Happening', 
N'[CUỘC THI F TALENT CODE 2019]
Giải F Talent Code mùa thứ hai đã chính thức mở màn
Các cánh tay của những #Coder đâu rồi nhỉ???
Hãy đến và click tham gia ngay cuộc thi F Talent Code 2019 cực #hot ngay thôi.
-----------------------------------------------------------------------------------------------------------
F TALENT CODE 2019 sẽ diễn ra vào ngày 16/03/2019
Vậy Cuộc thi có điều gì hấp dẫn:
(y) Bạn sẽ có cơ hội Đại diện Trường Đại học FPT tham gia cuộc thi ACM/ICPC quốc tế.
(y) Đặc biệt với nhiều Giải thưởng tiền mặt: 
- 1 giải nhất: tiền mặt 3,000,000 VND
- 2 giải nhì: mỗi giải tiền mặt 1,500,000 VND
- 3 giải ba: mỗi giải tiền mặt 1,000,000 VND
- 5 giải khuyến khích: mỗi giải tiền mặt 500,000 VND
- 7 giải cho cá nhân giải quyết được problem đầu tiên: 300,000 VND
Vậy trước đó các bạn cần chuẩn bị những gì, hãy chú ý những điều sau đây nhé:
1. Đối tương dự thi: 
Sinh viên Trường Đại học FPT các cơ sở: Hà Nội, Đà Nẵng, TP. Hồ Chí Minh, Cần Thơ.
2. Hình thức thi: Thi cá nhân
3. Thể lệ cuộc thi:
- Cơ cấu đề thi: Đề thi gồm 20 bài, giải trong 5 giờ.
- Trong suốt quá trình cuộc thi diễn ra, các thi sinh không được phép rời khỏi khu vực dự thi. Các trường hợp còn lại đều phải có ý kiến của BTC.
- Thí sinh có hành động sao chép hoặc gian lận sẽ lập tức bị loại khỏi cuộc thi.
- Quyết định của BTC là quyết định cuối cùng.
- Hạn cuối đăng kí danh sách 12h00, 12/03/2019
 Link đăng ký: https://goo.gl/forms/E3AxerRqozKdWkzD3
=============================================================
- Tên chương trình: F TALENT CODE 2019
- Địa điểm tổ chức : Các cơ sở Đại học FPT trên toàn quốc.
- Thời gian: 7h30 ngày 16/03/2019
Đơn vị tổ chức: Phòng Công tác sinh viên -Trường Đại học FPT cơ sở Hà Nội , Bộ môn An ninh an toàn thông tin, Ban Đào tạo – Trường Đại học FPT cơ sở Hà Nội, Phòng Đào tạo, Phòng Công tác sinh viên các cơ sở Hồ Chí Minh, Cần Thơ, Đà Nẵng.
Liên hệ: 02466805915',
N'[CUỘC THI F TALENT CODE 2019]
Giải F Talent Code mùa thứ hai đã chính thức mở màn
Các cánh tay của những #Coder đâu rồi nhỉ???
Hãy đến và click tham gia ngay cuộc thi F Talent Code 2019 cực #hot ngay thôi.
-----------------------------------------------------------------------------------------------------------
F TALENT CODE 2019 sẽ diễn ra vào ngày 16/03/2019
Vậy Cuộc thi có điều gì hấp dẫn:
(y) Bạn sẽ có cơ hội Đại diện Trường Đại học FPT tham gia cuộc thi ACM/ICPC quốc tế.
(y) Đặc biệt với nhiều Giải thưởng tiền mặt: 
- 1 giải nhất: tiền mặt 3,000,000 VND
- 2 giải nhì: mỗi giải tiền mặt 1,500,000 VND
- 3 giải ba: mỗi giải tiền mặt 1,000,000 VND
- 5 giải khuyến khích: mỗi giải tiền mặt 500,000 VND
- 7 giải cho cá nhân giải quyết được problem đầu tiên: 300,000 VND
Vậy trước đó các bạn cần chuẩn bị những gì, hãy chú ý những điều sau đây nhé:
1. Đối tương dự thi: 
Sinh viên Trường Đại học FPT các cơ sở: Hà Nội, Đà Nẵng, TP. Hồ Chí Minh, Cần Thơ.
2. Hình thức thi: Thi cá nhân
3. Thể lệ cuộc thi:
- Cơ cấu đề thi: Đề thi gồm 20 bài, giải trong 5 giờ.
- Trong suốt quá trình cuộc thi diễn ra, các thi sinh không được phép rời khỏi khu vực dự thi. Các trường hợp còn lại đều phải có ý kiến của BTC.
- Thí sinh có hành động sao chép hoặc gian lận sẽ lập tức bị loại khỏi cuộc thi.
- Quyết định của BTC là quyết định cuối cùng.
- Hạn cuối đăng kí danh sách 12h00, 12/03/2019
 Link đăng ký: <a href="https://goo.gl/forms/E3AxerRqozKdWkzD3">https://goo.gl/forms/E3AxerRqozKdWkzD3</a>
=============================================================
- Tên chương trình: F TALENT CODE 2019
- Địa điểm tổ chức : Các cơ sở Đại học FPT trên toàn quốc.
- Thời gian: 7h30 ngày 16/03/2019
Đơn vị tổ chức: Phòng Công tác sinh viên -Trường Đại học FPT cơ sở Hà Nội , Bộ môn An ninh an toàn thông tin, Ban Đào tạo – Trường Đại học FPT cơ sở Hà Nội, Phòng Đào tạo, Phòng Công tác sinh viên các cơ sở Hồ Chí Minh, Cần Thơ, Đà Nẵng.
Liên hệ: 02466805915', 
N'Các cơ sở Đại học FPT trên toàn quốc','2019-03-03',5,'2019-03-16 8:00:00','2019-03-16 19:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(9,5)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(9,'2019-03-14 23:59:00','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,7,'2019-03-03')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,7,'2019-03-03')

INSERT INTO EventFamily(FamilyName) VALUES (N'Khai Xuân Kì Hội 2019')
INSERT INTO Event(ManagerId,FamilyId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2,10, 0, N'Khai Xuân Kì Hội 2019','/Images/Events/SPRING2019_ChessTournament.jpg', 1, 0, 'Closed', 
N'Mọi người ơi ..mọi người ơi..
Đến thi đấu ở giải cờ Khai Xuân Kì Hội của chúng tớ nè
❤ Đến hẹn lại lên, CLB Cờ và CLB Cờ Vây, Phòng hợp tác và phát triển quốc tế cá nhân (IC-PDP) kết hợp cùng nhóm WIG EZP lớp SE1405 tổ chức một giải đấu vô cùng quan trọng đó chính là Khai Xuân Kì Hội 2019
❤ Khai xuân kì hội là một giải cờ thường niên của chúng tớ, để tận hưởng không khí của mùa xuân trong veo mát mẻ ~~
❤ Là nơi gặp gỡ, hội tụ của các cao nhân cờ lão luyện trong thiên hạ và cả những tay mơ đẹp trai xinh gái 
❤ Tổng giải thưởng lên đến 3.500.000. 
- Giải cờ lần này chúng mình bao gồm 5 bộ môn: Cờ Tướng, Cờ Vua, Cờ Vây, Cờ Caro và Cờ Cá Ngựa 
- Còn chần chừ gì nữa, hãy nhanh tay đăng kí nào: 
Link đăng kí: https://goo.gl/forms/PAX9FryQtuWg9Hw02
Thời gian, địa điểm
⏰⏰⏰Thời gian thi đấu: Thứ 3, 05/03/2019
+ 18h00-18h15: Đón tiếp kỳ thủ, hướng dẫn kỳ thủ vào vị trí thi đấu
+ 18h15-18h30: Khai mạc Giải
+ 18h30-18h50: Ván 1
+ 19h00-19h20: Ván 2
+ 19h30-19h50: Ván 3
+ 20h00-20h20: Ván 4
+ 20h30-20h50: Ván 5
+ 21h00-21h20: Ván 6
+ 21h30-21h50: Ván 7
+ 22h00: Bế mạc trao giải thưởng
Lệ phí thi đấu: 20.000 VNĐ/ 1 bộ môn
- Mọi thông tin chi tiết xin vui lòng liên hệ: 
*Hotline
Mr.Tuan: 0365153859
Ms.Thao: 0914171669
Hoặc địa chỉ email: khaixuankyhoi2019@gmail.com
#ChessandGo
#KhaiXuanKyHoi2019',
N'Mọi người ơi ..mọi người ơi..
Đến thi đấu ở giải cờ Khai Xuân Kì Hội của chúng tớ nè
❤ Đến hẹn lại lên, CLB Cờ và CLB Cờ Vây, Phòng hợp tác và phát triển quốc tế cá nhân (IC-PDP) kết hợp cùng nhóm WIG EZP lớp SE1405 tổ chức một giải đấu vô cùng quan trọng đó chính là Khai Xuân Kì Hội 2019
❤ Khai xuân kì hội là một giải cờ thường niên của chúng tớ, để tận hưởng không khí của mùa xuân trong veo mát mẻ ~~
❤ Là nơi gặp gỡ, hội tụ của các cao nhân cờ lão luyện trong thiên hạ và cả những tay mơ đẹp trai xinh gái 
❤ Tổng giải thưởng lên đến 3.500.000. 
- Giải cờ lần này chúng mình bao gồm 5 bộ môn: Cờ Tướng, Cờ Vua, Cờ Vây, Cờ Caro và Cờ Cá Ngựa 
- Còn chần chừ gì nữa, hãy nhanh tay đăng kí nào: 
Link đăng kí: <a href="https://goo.gl/forms/PAX9FryQtuWg9Hw02">https://goo.gl/forms/PAX9FryQtuWg9Hw02</a>
Thời gian, địa điểm
⏰⏰⏰Thời gian thi đấu: Thứ 3, 05/03/2019
+ 18h00-18h15: Đón tiếp kỳ thủ, hướng dẫn kỳ thủ vào vị trí thi đấu
+ 18h15-18h30: Khai mạc Giải
+ 18h30-18h50: Ván 1
+ 19h00-19h20: Ván 2
+ 19h30-19h50: Ván 3
+ 20h00-20h20: Ván 4
+ 20h30-20h50: Ván 5
+ 21h00-21h20: Ván 6
+ 21h30-21h50: Ván 7
+ 22h00: Bế mạc trao giải thưởng
Lệ phí thi đấu: 20.000 VNĐ/ 1 bộ môn
- Mọi thông tin chi tiết xin vui lòng liên hệ: 
*Hotline
Mr.Tuan: 0365153859
Ms.Thao: 0914171669
Hoặc địa chỉ email: khaixuankyhoi2019@gmail.com
#ChessandGo
#KhaiXuanKyHoi2019', 
N'Sảnh cờ vua tòa nhà Beta','2019-02-28',5,'2019-03-05 18:00:00','2019-03-05 22:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(10,5)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(10,'2019-03-05','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,8,'2019-03-03')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,8,'2019-03-03')

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Khóa học cờ vua cơ bản','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 1, 'Closed', 
N'[CHESS BASICS]
🌈 🌈 Có những lĩnh vực, tuổi tác không thể quyết thể quyết định được năng lực. Một đứa trẻ 10 tuổi có thể chiến thắng một cụ lão 70 tuổi, một người chơi hơn 1,2 tháng có thể thắng một người đã chơi 3,4 năm. Nếu gọi nó là sở thích, thì sở thích ấy không có biên giới, không giới hạn độ tuổi, không phân biệt giới tính, vùng miền,.... Và đặc biệt, không bao giờ là quá muộn để bắt đầu.
❓ ❓ Vậy bạn biết đó là gì và sự khác biệt ở đâu không?
💡 💡 💡 Đó là CỜ💡 💡 💡 
Và cách nhanh nhất để chơi giỏi là HỌC CÁCH CHƠI
👦 👧 Chúng ta đều là sinh viên đại học rồi. Ở cấp bậc đại học, chúng ta nên hiểu biết về cờ một cách nghiêm túc. Bạn đã từng nghe đến các khái niệm như khai cuộc, tàn cuộc, trung cuộc,...chưa? Có thể bạn đã đánh thắng nhiều rồi, nhưng để nâng cao khả năng đánh cờ của mình, chắc chắn những kiến thức cơ bản ấy là không thể không có. Chúng mình cũng hiểu, không phải ai cũng có cơ hội được học những điều ấy.
➡️ ➡️ Với mục đích để mọi người có thể hiểu sâu hơn về Cờ, CLB Cờ Đại học FPT (FCC) dưới sự hỗ trợ của Phòng hợp tác và phát triển cá nhân (IC-PDP) tổ chức khóa học Cơ bản về Cờ Vua.
✅ Khóa học hướng đến việc trang bị cho học viên những kiến thức từ cơ bản cho đến phức tạp trong bộ môn cờ vua, từ các bước khai cuộc cho đến tàn cuộc. 
✅ Ngoài ra các bạn còn được trải nghiệm mình ở một vị trí kì thủ trong những trận đấu cờ được sắp xếp vào cuối khóa học.
❌ ❌ ❌ Vậy nên chúng mình xin gửi những lời này để gửi lời mời đến bạn, bạn của bạn và cả crush của bạn cùng tham gia vào khóa học của chúng mình. Chúng mình rất hân hạnh chào đón các bạn đến và tham dự <3
Bạn chỉ cần yêu thích Cờ Vua thôi, việc còn lại cứ để FCC lo <3
▶ Thời gian: 19h30-21h vào các ngày thứ 2, thứ 3, thứ 5 từ ngày 14/11 – 14/2
▶ Lệ phí: 50k
▶ LINK ĐĂNG KÝ KHÓA HỌC: https://goo.gl/forms/olYGsC9kB0526dsu2
▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS]
🌈 🌈 Có những lĩnh vực, tuổi tác không thể quyết thể quyết định được năng lực. Một đứa trẻ 10 tuổi có thể chiến thắng một cụ lão 70 tuổi, một người chơi hơn 1,2 tháng có thể thắng một người đã chơi 3,4 năm. Nếu gọi nó là sở thích, thì sở thích ấy không có biên giới, không giới hạn độ tuổi, không phân biệt giới tính, vùng miền,.... Và đặc biệt, không bao giờ là quá muộn để bắt đầu.
❓ ❓ Vậy bạn biết đó là gì và sự khác biệt ở đâu không?
💡 💡 💡 Đó là CỜ💡 💡 💡 
Và cách nhanh nhất để chơi giỏi là HỌC CÁCH CHƠI
👦 👧 Chúng ta đều là sinh viên đại học rồi. Ở cấp bậc đại học, chúng ta nên hiểu biết về cờ một cách nghiêm túc. Bạn đã từng nghe đến các khái niệm như khai cuộc, tàn cuộc, trung cuộc,...chưa? Có thể bạn đã đánh thắng nhiều rồi, nhưng để nâng cao khả năng đánh cờ của mình, chắc chắn những kiến thức cơ bản ấy là không thể không có. Chúng mình cũng hiểu, không phải ai cũng có cơ hội được học những điều ấy.
➡️ ➡️ Với mục đích để mọi người có thể hiểu sâu hơn về Cờ, CLB Cờ Đại học FPT (FCC) dưới sự hỗ trợ của Phòng hợp tác và phát triển cá nhân (IC-PDP) tổ chức khóa học Cơ bản về Cờ Vua.
✅ Khóa học hướng đến việc trang bị cho học viên những kiến thức từ cơ bản cho đến phức tạp trong bộ môn cờ vua, từ các bước khai cuộc cho đến tàn cuộc. 
✅ Ngoài ra các bạn còn được trải nghiệm mình ở một vị trí kì thủ trong những trận đấu cờ được sắp xếp vào cuối khóa học.
❌ ❌ ❌ Vậy nên chúng mình xin gửi những lời này để gửi lời mời đến bạn, bạn của bạn và cả crush của bạn cùng tham gia vào khóa học của chúng mình. Chúng mình rất hân hạnh chào đón các bạn đến và tham dự <3
Bạn chỉ cần yêu thích Cờ Vua thôi, việc còn lại cứ để FCC lo <3
▶ Thời gian: 19h30-21h vào các ngày thứ 2, thứ 3, thứ 5 từ ngày 14/11 – 14/2
▶ Lệ phí: 50k
▶ LINK ĐĂNG KÝ KHÓA HỌC: <a href="https://goo.gl/forms/olYGsC9kB0526dsu2">https://goo.gl/forms/olYGsC9kB0526dsu2</a>
▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-01-14','2019-02-21')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(11,4)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(11,'2019-01-12','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,9,'2019-01-09')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,9,'2019-01-09')

INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1, 1, 0, N'Miss FPTU Hà Nội - Vòng sơ khảo','/Images/Events/FALL2018_MissFPTUHanoi_Sokhao.jpg', 0, 'Closed', 
N'❤️Thân gửi các cô gái của đại học FPT, những ngày vừa qua BTC chúng tớ đã cố gắng để có thể chuyển những bức thư xinh đẹp này tới các bạn. Nhưng chúng tớ vẫn chưa thể gặp vào trao tận tay tới tất cả mọi người. Vì vậy chúng tớ muốn 1 lần nữa gửi gắm tâm tình của mình lên đây, với hy vọng tất cả các cậu có thể đọc và cảm nhận tấm lòng của BTC bọn tớ.
💃Cô gái nào muốn cầm tận tay chiếc thư mời xinh xắn này làm kỷ niệm hay check in thì hãy cmt hoặc nhắn tin trực tiếp cho page nha, chúng mình sẽ gửi tận tay các cậu.
------------------------------------------------------------------
Gửi các cô gái
Khi đọc tấm thư này cũng có nghĩa là bạn đang ở độ tuổi đẹp nhất của một cuộc đời. Là lúc những nụ hoa e ấp hé nở những cánh đầu tiên, rạng ngời và kiêu hãnh trong nắng sớm. Là khi bạn dám dấn thân mà không sợ vấp ngã, dám ước mơ, dám khát khao, dám yêu và dám chấp nhận đau khổ... Đó là thanh xuân.

Có bao giờ bạn tự hỏi tuổi thanh xuân sẽ kéo dài bao lâu? Một năm, hai năm, hay mười năm nữa? Và dù bạn có để ý hay không thì thanh xuân ấy cũng đang trôi qua vội vã trong từng nhịp thở. Chúng tôi - Miss FPTU ở đây với sứ mệnh giúp bạn lưu lại những khoảnh khắc đẹp nhất của tuổi trẻ. Để bạn không phải hối tiếc khi chợt nhận ra mình đã vô tình làm vuột mất thanh xuân.

Vì thế, hãy một lần bước ra khỏi những lắng lo để đến với Miss FPTU, dù đối với bạn đây có vẻ là quyết định điên rồ nhất, thì chúng tôi cũng nguyện làm điều điên rồ ấy cùng bạn. Hãy tự tin mang vẻ đẹp duy nhất của riêng bạn tỏa sáng rạng ngời dưới ánh hào quang của Miss FPTU. Chúng tôi tin chắc rằng những ký ức tại Miss FPTU sẽ làm cho bạn mỉm cười hạnh phúc khi nhìn lại cuốn album của cuộc đời mình.
Chúng tôi chờ thấy tên bạn trong dòng đăng ký thân thương của Miss FPTU Hà Nội 2018 ❤ 
Nhớ nhé, chúng ta có một cuộc hẹn ở Chương trình Miss FPTU, hỡi cô gái tuổi thanh xuân!

BAN TỔ CHỨC
-------------------------------------------------------------------
დ Đăng ký Miss: http://bit.ly/missFPTUhanoi2018
დ Đăng ký Nam sinh đồng hành: http://bit.ly/manhuntFPTUhanoi2018', 
N'❤️Thân gửi các cô gái của đại học FPT, những ngày vừa qua BTC chúng tớ đã cố gắng để có thể chuyển những bức thư xinh đẹp này tới các bạn. Nhưng chúng tớ vẫn chưa thể gặp vào trao tận tay tới tất cả mọi người. Vì vậy chúng tớ muốn 1 lần nữa gửi gắm tâm tình của mình lên đây, với hy vọng tất cả các cậu có thể đọc và cảm nhận tấm lòng của BTC bọn tớ.
💃Cô gái nào muốn cầm tận tay chiếc thư mời xinh xắn này làm kỷ niệm hay check in thì hãy cmt hoặc nhắn tin trực tiếp cho page nha, chúng mình sẽ gửi tận tay các cậu.
------------------------------------------------------------------
Gửi các cô gái
Khi đọc tấm thư này cũng có nghĩa là bạn đang ở độ tuổi đẹp nhất của một cuộc đời. Là lúc những nụ hoa e ấp hé nở những cánh đầu tiên, rạng ngời và kiêu hãnh trong nắng sớm. Là khi bạn dám dấn thân mà không sợ vấp ngã, dám ước mơ, dám khát khao, dám yêu và dám chấp nhận đau khổ... Đó là thanh xuân.

Có bao giờ bạn tự hỏi tuổi thanh xuân sẽ kéo dài bao lâu? Một năm, hai năm, hay mười năm nữa? Và dù bạn có để ý hay không thì thanh xuân ấy cũng đang trôi qua vội vã trong từng nhịp thở. Chúng tôi - Miss FPTU ở đây với sứ mệnh giúp bạn lưu lại những khoảnh khắc đẹp nhất của tuổi trẻ. Để bạn không phải hối tiếc khi chợt nhận ra mình đã vô tình làm vuột mất thanh xuân.

Vì thế, hãy một lần bước ra khỏi những lắng lo để đến với Miss FPTU, dù đối với bạn đây có vẻ là quyết định điên rồ nhất, thì chúng tôi cũng nguyện làm điều điên rồ ấy cùng bạn. Hãy tự tin mang vẻ đẹp duy nhất của riêng bạn tỏa sáng rạng ngời dưới ánh hào quang của Miss FPTU. Chúng tôi tin chắc rằng những ký ức tại Miss FPTU sẽ làm cho bạn mỉm cười hạnh phúc khi nhìn lại cuốn album của cuộc đời mình.
Chúng tôi chờ thấy tên bạn trong dòng đăng ký thân thương của Miss FPTU Hà Nội 2018 ❤ 
Nhớ nhé, chúng ta có một cuộc hẹn ở Chương trình Miss FPTU, hỡi cô gái tuổi thanh xuân!

BAN TỔ CHỨC
-------------------------------------------------------------------
დ Đăng ký Miss: <a href="http://bit.ly/missFPTUhanoi2018">http://bit.ly/missFPTUhanoi2018</a>
დ Đăng ký Nam sinh đồng hành: <a href="http://bit.ly/manhuntFPTUhanoi2018">http://bit.ly/manhuntFPTUhanoi2018</a>',
N'Hòa Lạc','2018-10-10',5,'2018-10-11','2018-10-31')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(12,5)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(12,'2018-10-25','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,10,'2018-10-25')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,10,'2018-10-25')


INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1, 1, 0, N'Miss FPTU Hà Nội - First Meeting','/Images/Events/FALL2018_MissFPTUHanoi_FirstMeeting.jpg', 0, 'Closed', 
N'
Thứ hai vừa rồi chúng mình đã có buổi gặp mặt đầu tiên giữa các miss, nam sinh đồng hành và ban tổ chức. 
Các bạn hãy cùng chúng mình xem lại một số hình ảnh vào tối hôm ấy nhé ^^. 
Hi vọng các bạn sẽ ủng hộ sự thể hiện của 15 miss cũng như nam sinh đồng hành trong những vòng thi sắp tới. 
Cảm ơn các bạn ❤ 

Cuối cùng, BTC xin gửi lời cảm ơn sâu sắc tới các nhà tài trợ đã tài trợ cho sự kiện MISS FPTU
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: https://www.facebook.com/missfptu/
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', 
N'
Thứ hai vừa rồi chúng mình đã có buổi gặp mặt đầu tiên giữa các miss, nam sinh đồng hành và ban tổ chức. 
Các bạn hãy cùng chúng mình xem lại một số hình ảnh vào tối hôm ấy nhé ^^. 
Hi vọng các bạn sẽ ủng hộ sự thể hiện của 15 miss cũng như nam sinh đồng hành trong những vòng thi sắp tới. 
Cảm ơn các bạn ❤ 

Cuối cùng, BTC xin gửi lời cảm ơn sâu sắc tới các nhà tài trợ đã tài trợ cho sự kiện MISS FPTU
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com',
N'Hòa Lạc','2018-10-10',5,'2018-11-05 19:30:00','2018-11-05 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(13,5)



INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1, 1, 0, N'Miss FPTU Hà Nội - Lotus In City','/Images/Events/FALL2018_MissFPTUHanoi_LotusInCity.jpg', 0, 'Closed', 
N'
CÓ GÌ TRONG MỘT BUỔI LIC? 

LiC - LOTUS in City: Thử thách trải nghiệm thực tế: Đổi kẹp giấy ra vật phẩm có giá trị lớn nhất có thể. 

Mục đích: Khám phá và trau dồi kỹ năng thuyết phục và nói trước đám đông cho những người tham gia. 

Hãy cùng xem các Miss đã thu được "chiến lợi phẩm" gì trong gần 2 giờ đồng hồ tham gia LiC nhé!!!
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: https://www.facebook.com/missfptu/
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com',
N'
CÓ GÌ TRONG MỘT BUỔI LIC? 

LiC - LOTUS in City: Thử thách trải nghiệm thực tế: Đổi kẹp giấy ra vật phẩm có giá trị lớn nhất có thể. 

Mục đích: Khám phá và trau dồi kỹ năng thuyết phục và nói trước đám đông cho những người tham gia. 

Hãy cùng xem các Miss đã thu được "chiến lợi phẩm" gì trong gần 2 giờ đồng hồ tham gia LiC nhé!!!
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', 
N'Hòa Lạc','2018-10-10',5,'2018-11-11 19:30:00','2018-11-11 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(14,5)




INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (1, 1, 0, N'Miss FPTU Hà Nội - Ngày Hiến Chương Nhà Giáo Việt Nam 2018','/Images/Events/FALL2018_MissFPTUHanoi_NgayHienChuongNhaGiaoVietNam.jpg', 0, 'Closed', 
N'
"Nhân ngày nhà giáo Việt Nam, chúng em chúc thầy cô luôn tràn đầy hạnh phúc. Một lời cảm ơn thôi vẫn chưa đủ, cảm ơn thầy cô đã tận tâm cống hiến, dạy dỗ chúng em nên người như ngày hôm nay." 

"Nhân ngày 20/11, chúng em xin kính chúc thầy cô sức khoẻ dồi dào, hạnh phúc trên con đường sự nghiệp của mình." 

Đó là lời nhắn trên những tấm thiệp do chính tay các Miss, Nam sinh đồng hành và toàn thể Ban tổ chức chuẩn bị. Và tất cả đã được các bạn gửi tới các thầy cô giáo vào ngày 16/11 vừa rồi. Một lần nữa, chúng em xin gửi lời cảm ơn chân thành nhất tới toàn thể thầy cô giáo của trường đại học FPT nói riêng và toàn thể những người làm nghề giáo nói chung. Cảm ơn những bài học chuyên ngành, cảm ơn những bài học cuộc sống, cảm ơn những cơ hội và thách thức thầy cô đem đến cho chúng em. Chúng em yêu thầy cô ❤️
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: https://www.facebook.com/missfptu/
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com',
N'
"Nhân ngày nhà giáo Việt Nam, chúng em chúc thầy cô luôn tràn đầy hạnh phúc. Một lời cảm ơn thôi vẫn chưa đủ, cảm ơn thầy cô đã tận tâm cống hiến, dạy dỗ chúng em nên người như ngày hôm nay." 

"Nhân ngày 20/11, chúng em xin kính chúc thầy cô sức khoẻ dồi dào, hạnh phúc trên con đường sự nghiệp của mình." 

Đó là lời nhắn trên những tấm thiệp do chính tay các Miss, Nam sinh đồng hành và toàn thể Ban tổ chức chuẩn bị. Và tất cả đã được các bạn gửi tới các thầy cô giáo vào ngày 16/11 vừa rồi. Một lần nữa, chúng em xin gửi lời cảm ơn chân thành nhất tới toàn thể thầy cô giáo của trường đại học FPT nói riêng và toàn thể những người làm nghề giáo nói chung. Cảm ơn những bài học chuyên ngành, cảm ơn những bài học cuộc sống, cảm ơn những cơ hội và thách thức thầy cô đem đến cho chúng em. Chúng em yêu thầy cô ❤️
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', 
N'Hòa Lạc','2018-10-10',5,'2018-11-20 07:00:00','2018-11-20 08:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(15,5)


INSERT INTO EventFamily(FamilyName) VALUES (N'Estrella')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES ( 11,1, 0, N'Estrella','/Images/Events/Estrella.jpg', 1, 'Opening', 
N'[ESTRELLA]

Vậy là sự kiện được mong chờ nhất trong năm học đã bắt đầu được khởi động. Khác với đêm Prom "Lumos" mà chúng ta đã được thưởng thức, buổi tiệc đêm mang phong cách huyền ảo, hiện đại, bí ẩn và hào nhoáng thì năm nay, BTC lựa chọn bộ film "A Star is born" là chủ đề chính cho đêm Prom 2019 năm nay, mang ánh sáng đặc trưng của những vì sao với mong muốn khi đến với sự kiện lần này ai trong chúng ta đều có thể trở thành phiên bản "tỏa sáng" nhất của chính mình.

Khung cảnh lung linh hiện ra và cùng với đó những vì sao rực sáng trong đêm, không những vậy âm nhạc được cất lên là kèm theo những vũ điệu đầy mê hoặc giữa giải ngân hà. Đó cũng chính là nguồn cảm hứng để Prom No Shy 2019 năm nay khoác trên mình cái tên vô cùng độc đáo: “ESTRELLA” - Bản giao hưởng ánh sáng.

“ESTRELLA” - Prom No Shy 2019 với không gian gần gũi hứa hẹn sẽ là một đêm tiệc ngập tràn màu sắc lung linh của ánh sáng, những giai điệu nhẹ nhàng và thêm vào đó là cả những màn slowdance cuốn hút.

Hãy cùng chúng mình đón chờ điều bất ngờ tiếp theo từ “ESTRELLA” nhé!

#NSC
#Promnight2019
#ESTRELLA
---------------------------------------
',
N'[ESTRELLA]

Vậy là sự kiện được mong chờ nhất trong năm học đã bắt đầu được khởi động. Khác với đêm Prom "Lumos" mà chúng ta đã được thưởng thức, buổi tiệc đêm mang phong cách huyền ảo, hiện đại, bí ẩn và hào nhoáng thì năm nay, BTC lựa chọn bộ film "A Star is born" là chủ đề chính cho đêm Prom 2019 năm nay, mang ánh sáng đặc trưng của những vì sao với mong muốn khi đến với sự kiện lần này ai trong chúng ta đều có thể trở thành phiên bản "tỏa sáng" nhất của chính mình.

Khung cảnh lung linh hiện ra và cùng với đó những vì sao rực sáng trong đêm, không những vậy âm nhạc được cất lên là kèm theo những vũ điệu đầy mê hoặc giữa giải ngân hà. Đó cũng chính là nguồn cảm hứng để Prom No Shy 2019 năm nay khoác trên mình cái tên vô cùng độc đáo: “ESTRELLA” - Bản giao hưởng ánh sáng.

“ESTRELLA” - Prom No Shy 2019 với không gian gần gũi hứa hẹn sẽ là một đêm tiệc ngập tràn màu sắc lung linh của ánh sáng, những giai điệu nhẹ nhàng và thêm vào đó là cả những màn slowdance cuốn hút.

Hãy cùng chúng mình đón chờ điều bất ngờ tiếp theo từ “ESTRELLA” nhé!

#NSC
#Promnight2019
#ESTRELLA
---------------------------------------
', 
N'Guest House of Vietnam National University, Hanoi','2019-03-12 21:00:21',5,'2019-03-16 19:00:00','2019-03-16 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(16,11)

INSERT INTO EventFamily(FamilyName) VALUES (N'Orientation & Coaching 2019')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (12, 1, 0, N'Orientation & Coaching','/Images/Events/SPRING2019_Orientation&Coaching.jpg', 0, 'Opening', 
N'[Orientation & Coaching]
Orentation & Coaching là một trong số những sự kiện do Phòng Công tác sinh viên tổ chức dành cho các bạn sinh viên chuẩn bị bước vào giai đoạn thực tập. Với mong muốn giúp cho các bạn sinh viên chuẩn bị những hành trang tốt nhất bước vào kỳ OJT, kỳ này, #SRO gửi đến các bạn rất nhiều thông tin hữu ích và sự tư vấn đặc biệt đến từ các khách mời #Coaching của chúng ta.
🔥Bạn sẽ nhận được gì tại buổi Orentation & Coaching này?🔥
- Những điều cần biết khi đi OJT trong buổi Orentation.
- Không đơn giản là biết cách viết một CV.
- Không đơn giản là biết cách thuyết phục các nhà tuyển dụng.
- Hơn thế bạn còn được các khách mời chia sẻ nhiều bí kíp sống còn trong những tháng ngày OJT.
- Đặc biệt sẽ có nhiều câu chuyện thú vị là những bài học kinh nghiệm các khách mời sẽ gửi đến các bạn.
Sự cần thiết của kỳ #OJT sẽ được giải đáp trong sự kiện này.
Hãy click đăng ký ngay hôm nay để bạn có thêm những hành trang hữu ích nhé. ;)
====================================================================================
💃💃💃Link đăng ký: https://goo.gl/forms/I5WV8QpONUTcPMKm2
• Hạn đăng ký: 12h, 13/03/2019
• Tên chương trình: Orientation & Coaching
Nội dung chương trình:
1. Orientation for OJT Students:
- Thời gian: 8:00 - 9:00 và 13:30 - 14:00 ngày 13/3/2019
- Địa điểm: Phòng 402R, tòa nhà Alpha, Đại học FPT
2. Coaching
- Thời gian: 9:00 - 11:30 và 14:00 - 16:00 ngày 13/3/2019
- Địa điểm: Phòng 102R, 104R, 107R, 402R tòa nhà Alpha, Trường Đại học FPT
• Khách mời coaching: anh Nguyễn Đăng Hiếu, chị Phạm Tuyết Hạnh Hà, chị Trần Thị Thu Hiền, anh Tô Vũ An.
• Liên hệ: 02466805915
',
N'[Orientation & Coaching]
Orentation & Coaching là một trong số những sự kiện do Phòng Công tác sinh viên tổ chức dành cho các bạn sinh viên chuẩn bị bước vào giai đoạn thực tập. Với mong muốn giúp cho các bạn sinh viên chuẩn bị những hành trang tốt nhất bước vào kỳ OJT, kỳ này, #SRO gửi đến các bạn rất nhiều thông tin hữu ích và sự tư vấn đặc biệt đến từ các khách mời #Coaching của chúng ta.
🔥Bạn sẽ nhận được gì tại buổi Orentation & Coaching này?🔥
- Những điều cần biết khi đi OJT trong buổi Orentation.
- Không đơn giản là biết cách viết một CV.
- Không đơn giản là biết cách thuyết phục các nhà tuyển dụng.
- Hơn thế bạn còn được các khách mời chia sẻ nhiều bí kíp sống còn trong những tháng ngày OJT.
- Đặc biệt sẽ có nhiều câu chuyện thú vị là những bài học kinh nghiệm các khách mời sẽ gửi đến các bạn.
Sự cần thiết của kỳ #OJT sẽ được giải đáp trong sự kiện này.
Hãy click đăng ký ngay hôm nay để bạn có thêm những hành trang hữu ích nhé. ;)
====================================================================================
💃💃💃Link đăng ký: <a href="https://goo.gl/forms/I5WV8QpONUTcPMKm2">https://goo.gl/forms/I5WV8QpONUTcPMKm2</a>
• Hạn đăng ký: 12h, 13/03/2019
• Tên chương trình: Orientation & Coaching
Nội dung chương trình:
1. Orientation for OJT Students:
- Thời gian: 8:00 - 9:00 và 13:30 - 14:00 ngày 13/3/2019
- Địa điểm: Phòng 402R, tòa nhà Alpha, Đại học FPT
2. Coaching
- Thời gian: 9:00 - 11:30 và 14:00 - 16:00 ngày 13/3/2019
- Địa điểm: Phòng 102R, 104R, 107R, 402R tòa nhà Alpha, Trường Đại học FPT
• Khách mời coaching: anh Nguyễn Đăng Hiếu, chị Phạm Tuyết Hạnh Hà, chị Trần Thị Thu Hiền, anh Tô Vũ An.
• Liên hệ: 02466805915
', 
N'Phòng 102R, 104R, 107R, 402R tòa nhà Alpha, Trường Đại học FPT','2019-03-12 21:00:21',5,'2019-03-13 08:00:00','2019-03-13 14:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(17,3)

INSERT INTO EventFamily(FamilyName) VALUES (N'Company Tour')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (12, 1, 0, N'VTI Tour Company','/Images/Events/SPRING2019_VTITOUR.jpg', 0, 'Opening', 
N'[COMPANYTOUR VTI]
Bạn còn nhớ:
🔥 VTI - nhà tài trợ đồng hành cùng Coding Inspiration của FPTU?
🔥VTI - doanh nghiệp cùng đến tham gia Japan day vào năm 2018?
🔥Đặc biệt bạn có biết, VTI còn là nhà tài trợ cho cuộc thi F Talent Code 2019.
🔥VTI - công ty outsourcing các sản phẩm công nghệ phần mềm cho thị trường Nhật.
🔥Rất nhiều cựu sinh viên FPTU hiện đang làm việc tại VTI.
Vậy bạn có muốn trải nghiệm văn hóa, môi trường tại VTI thì hãy đăng ký ngay nhé.
Nguồn ảnh: JS Club
==========================================
COMPANY TOUR VTI 
🔥🔥🔥Link đăng ký: https://goo.gl/forms/atXXAY3zLz8F23I13
- Hạn đăng ký: 13/03/2019
- Đối tượng tham dự: Sinh viên ngành CNTT, ngành Ngôn ngữ Nhật (ưu tiên các bạn kỳ 5 trước OJT, kỳ 8-9 trước khi tốt nghiệp).
- Thời gian: 14h00 - 16h30 chiều Thứ 6 ngày 15/03/2019.
- Địa điểm: Công ty VTI, Tòa nhà AC, 78 Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội.
- Phương tiện di chuyển: Có xe đưa đón của BTC tại sảnh Alpha
- Liên hệ: 02466805915.
',
N'[COMPANYTOUR VTI]
Bạn còn nhớ:
🔥 VTI - nhà tài trợ đồng hành cùng Coding Inspiration của FPTU?
🔥VTI - doanh nghiệp cùng đến tham gia Japan day vào năm 2018?
🔥Đặc biệt bạn có biết, VTI còn là nhà tài trợ cho cuộc thi F Talent Code 2019.
🔥VTI - công ty outsourcing các sản phẩm công nghệ phần mềm cho thị trường Nhật.
🔥Rất nhiều cựu sinh viên FPTU hiện đang làm việc tại VTI.
Vậy bạn có muốn trải nghiệm văn hóa, môi trường tại VTI thì hãy đăng ký ngay nhé.
Nguồn ảnh: JS Club
==========================================
COMPANY TOUR VTI 
🔥🔥🔥Link đăng ký: <a href="https://goo.gl/forms/atXXAY3zLz8F23I13">https://goo.gl/forms/atXXAY3zLz8F23I13</a>
- Hạn đăng ký: 13/03/2019
- Đối tượng tham dự: Sinh viên ngành CNTT, ngành Ngôn ngữ Nhật (ưu tiên các bạn kỳ 5 trước OJT, kỳ 8-9 trước khi tốt nghiệp).
- Thời gian: 14h00 - 16h30 chiều Thứ 6 ngày 15/03/2019.
- Địa điểm: Công ty VTI, Tòa nhà AC, 78 Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội.
- Phương tiện di chuyển: Có xe đưa đón của BTC tại sảnh Alpha
- Liên hệ: 02466805915.
', 
N'VTI Company','2019-03-08 12:00:21',5,'2019-03-15 14:00:00','2019-03-15 16:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(18,7)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(18,10)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(18,02)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(18,'2019-03-13','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,11,'2019-03-09')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,11,'2019-03-09')

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 1','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 1
Tại buổi 1, sinh viên sẽ lần đầu làm quen với các kí hiệu của cờ vua, các bạn sẽ được học cách chơi cờ vua mà làm quen với những lối đánh đơn giản.
Nội dung của buổi học gồm có:
- Học cách di chuyển quân và một số nước đi đặc biệt như bắt Tốt qua đường, phong cấp, hòa PAT, chiếu MAT
- Học kí hiệu bàn cờ vua và cách viết biên bản bàn cờ
- Học cách chiếu hết

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS] Buổi 1
Tại buổi 1, sinh viên sẽ lần đầu làm quen với các kí hiệu của cờ vua, các bạn sẽ được học cách chơi cờ vua mà làm quen với những lối đánh đơn giản.
Nội dung của buổi học gồm có:
- Học cách di chuyển quân và một số nước đi đặc biệt như bắt Tốt qua đường, phong cấp, hòa PAT, chiếu MAT
- Học kí hiệu bàn cờ vua và cách viết biên bản bàn cờ
- Học cách chiếu hết

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-01-14 19:30:00','2019-01-14 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(19,4)

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 2','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 2
Tại buổi 2, sinh viên sẽ được mở rộng hiểu biết về cờ khi được tiếp xúc với các ván cờ nổi tiếng và có cái nhìn toàn cảnh hơn về một ván cờ vua.
Các bạn sẽ được trả lời các câu hỏi:
- Một ván cờ gồm bao nhiêu giai đoạn
- Làm thế nào để phát huy khả năng của bản thân trong từng gian đoạn của một ván cờ
- Tiếp xúc với các ván cờ của các nhà vô địch thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'[CHESS BASICS] Buổi 2
Tại buổi 2, sinh viên sẽ được mở rộng hiểu biết về cờ khi được tiếp xúc với các ván cờ nổi tiếng và có cái nhìn toàn cảnh hơn về một ván cờ vua.
Các bạn sẽ được trả lời các câu hỏi:
- Một ván cờ gồm bao nhiêu giai đoạn
- Làm thế nào để phát huy khả năng của bản thân trong từng gian đoạn của một ván cờ
- Tiếp xúc với các ván cờ của các nhà vô địch thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-01-17 19:30:00','2019-01-17 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(20,4)

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 3','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 3
Tại buổi 3, sinh viên sẽ được giới thiệu về cách chơi khai cuộc
Các bạn sẽ được học:
- Các nguyên tắc cơ bản khi chơi khai cuộc
- Các sai lầm thường gặp khi đánh khai cuộc
- Giải thích cách chơi khai cuộc của một số ván cờ nổi tiếng của các Đại kiến tướng trên thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS] Buổi 3
Tại buổi 3, sinh viên sẽ được giới thiệu về cách chơi khai cuộc
Các bạn sẽ được học:
- Các nguyên tắc cơ bản khi chơi khai cuộc
- Các sai lầm thường gặp khi đánh khai cuộc
- Giải thích cách chơi khai cuộc của một số ván cờ nổi tiếng của các Đại kiến tướng trên thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-01-21 19:30:00','2019-01-21 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(21,4)

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 4','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 4
Tại buổi 4, sinh viên sẽ được đấu tập về khai cuộc.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS] Buổi 4
Tại buổi 4, sinh viên sẽ được đấu tập về khai cuộc.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-01-28 19:30:00','2019-01-28 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(22,4)

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 5','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 5
Tại buổi 5, sinh viên sẽ học cách chơi trung cuộc.
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của trung cuộc
- Chiến lược và chiến thuật trong một ván cờ vua là gì
- Học cách chơi trung cuộc như một Đại kiện tướng

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS] Buổi 5
Tại buổi 5, sinh viên sẽ học cách chơi trung cuộc.
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của trung cuộc
- Chiến lược và chiến thuật trong một ván cờ vua là gì
- Học cách chơi trung cuộc như một Đại kiện tướng

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-02-11 19:30:00','2019-02-11 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(23,4)

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 6','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 6
Tại buổi 5, sinh viên sẽ học cách chơi tàn cuộc
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của tàn cuộc
- Cách chiếu hết bằng Hậu, Xe
- Thế nào là đối Vua

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS] Buổi 6
Tại buổi 5, sinh viên sẽ học cách chơi tàn cuộc
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của tàn cuộc
- Cách chiếu hết bằng Hậu, Xe
- Thế nào là đối Vua

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-02-14 19:30:00','2019-02-14 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(24,4)

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 7','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 7
Buổi 7 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS] Buổi 7
Buổi 7 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-02-18 19:30:00','2019-02-18 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(25,4)

INSERT INTO [Event](FamilyId, ManagerId, EventFee, EventName, CoverImage, IsPublic, IsFeatured, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (2, 1, 50000, N'Chess Basics - Buổi 8','/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, 'Closed', 
N'[CHESS BASICS] Buổi 8
Buổi 8 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass',
N'[CHESS BASICS] Buổi 8
Buổi 8 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', 
N'205HB','2019-01-08',5,'2019-02-21 19:30:00','2019-02-21 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(26,4)

INSERT INTO EventFamily(FamilyName) VALUES (N'Tuyển phóng viên cóc đọc')
INSERT INTO Event(RegisterEndDate, FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES ('2019-04-01 23:59:59',13, 2, 0, N'Tuyển phóng viên Cóc Đọc','/Images/Events/SPRING2019_TuyenCocDoc.jpg', 1, 'Opening', 
N'🎉A đây rồi! Công việc hot nhất trong năm🎉
CÓC ĐỌC VÀ NHỮNG NGƯỜI BẠN TUYỂN PHÓNG VIÊN 
❄️Yêu cầu: Biết đọc, biết viết, yêu cái đẹp, biết quay phim chụp ảnh, hài hước, dễ thương, thích nhận nhuận bút

❄️Ưu tiên: Có nhan sắc và sức khỏe là một lợi thế

❄️Cách thức ứng tuyển: Post status 300- 400 chữ để chế độ công khai viết về lí do Cóc Đọc và Những người bạn nên tuyển bạn. Tag Cóc Đọc và Những người bạn và để hashtag: #Coc_Doc_Tuyen_minh_di



👉🏻Hạn cuối: Ngày Cá thế giới – 1/4/2019
👉🏻Truy cập website: http://cocdoc.fpt.edu.vn để biết thêm chi tiết.',
N'🎉A đây rồi! Công việc hot nhất trong năm🎉
CÓC ĐỌC VÀ NHỮNG NGƯỜI BẠN TUYỂN PHÓNG VIÊN 
❄️Yêu cầu: Biết đọc, biết viết, yêu cái đẹp, biết quay phim chụp ảnh, hài hước, dễ thương, thích nhận nhuận bút

❄️Ưu tiên: Có nhan sắc và sức khỏe là một lợi thế

❄️Cách thức ứng tuyển: Post status 300- 400 chữ để chế độ công khai viết về lí do Cóc Đọc và Những người bạn nên tuyển bạn. Tag Cóc Đọc và Những người bạn và để hashtag: #Coc_Doc_Tuyen_minh_di

✍🏻Link đăng ký: <a href="http://bit.ly/tuyenquancocdoc2019">http://bit.ly/tuyenquancocdoc2019</a>

👉🏻Hạn cuối: Ngày Cá thế giới – 1/4/2019
👉🏻Truy cập website: <a href="http://cocdoc.fpt.edu.vn">http://cocdoc.fpt.edu.vn</a> để biết thêm chi tiết.', 
N'Hòa Lạc','2019-03-12 10:00:00',5,'2019-03-12 14:00:00','2019-04-01 23:59:59')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(27,10)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(27,'2019-04-01 23:59:59','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,12,'2019-03-13')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,12,'2019-03-13')

INSERT INTO EventFamily(FamilyName) VALUES (N'Xuân yêu thương')
INSERT INTO Event( FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (14, 2, 0, N'Xuân yêu thương','/Images/Events/SPRING2019_XuanYeuThuong.jpg', 1, 'Closed', 
N'🌸🌸🌸[XUÂN YÊU THƯƠNG 2019]🌸🌸🌸
“Gió xuân phơ phất thổi trong cành
Lớp lớp bên đường bóng lá xanh…”
🎋🎋🎋 Mùa Xuân, khi đất trời giao thoa, cây cối đua nhau đâm trồi nảy lộc, là mùa tượng trưng cho sức sống mãnh liệt, cho hi vọng và ước mơ 🌺 Tết Nguyên Đán đã qua đi, thế nhưng không khí vui tươi của mùa xuân ấy vẫn chưa dịu lại, hãy để FTIC một lần nữa thổi lên ngọn gió mùa xuân ấy trong lòng các bạn nhé🌬🌬
🍀🍀🍀Xuân Yêu Thương – một sự kiện được ấp ủ và tổ chức bởi cộng tác viên CLB FTIC, đồng hành cùng sự kiện lần này còn có sự hợp tác của Fire Team lớp WIG BA1402. Là một món quà đầu xuân mà chúng mình muốn gửi gắm đến tất cả mọi người. Đây là lúc chúng ta ngồi lại bên nhau, tâm sự, kể về những điều tuyệt vời ta đã làm trong năm cũ, những chặng đường mà FTIC đã cùng nhau đi qua. Từ đó cùng nỗ lực, sát cánh bên nhau trên chặng đường dài phía trước 🌈🌈
😮🤭😤Hòa cùng không khí sắc xuân này, các bạn không chỉ được thưởng thức tiếng nhạc cụ hết sức nhịp nhàng và ấm áp mà còn có cơ hội rinh ngay những phần quà vô cùng giá trị về nhà nữa đấy!!!🎊🎊
Còn chần chừ gì nữa khi mà chúng tớ với chiếc hẹn chan chứa yêu thương đang chờ đợi các cậu! Cùng chúng tớ lắng nghe lời thì thầm của mùa xuân nhé❤️❤️❤️
---------------------------------------------------
⏰Thời gian: 19H30- Thứ năm, ngày 7/3/2019
⛩Địa điểm: Sảnh chính tòa Beta
☎️MỌI CHI TIẾT XIN LIÊN HỆ☎️
Trưởng BTC: Nguyễn Thành Đạt - 0936704211
Phó BTC: Nguyễn Văn Bách - 0974156308 
---------------------------------------------------------------
#XuânYêuThương2019
#CLBNhạcCụTruyềnThống
#DoWhatYouLove - #LoveWhatYouDo
#CTVMangMùaXuânĐến
#FireTeam',
N'🌸🌸🌸[XUÂN YÊU THƯƠNG 2019]🌸🌸🌸
“Gió xuân phơ phất thổi trong cành
Lớp lớp bên đường bóng lá xanh…”
🎋🎋🎋 Mùa Xuân, khi đất trời giao thoa, cây cối đua nhau đâm trồi nảy lộc, là mùa tượng trưng cho sức sống mãnh liệt, cho hi vọng và ước mơ 🌺 Tết Nguyên Đán đã qua đi, thế nhưng không khí vui tươi của mùa xuân ấy vẫn chưa dịu lại, hãy để FTIC một lần nữa thổi lên ngọn gió mùa xuân ấy trong lòng các bạn nhé🌬🌬
🍀🍀🍀Xuân Yêu Thương – một sự kiện được ấp ủ và tổ chức bởi cộng tác viên CLB FTIC, đồng hành cùng sự kiện lần này còn có sự hợp tác của Fire Team lớp WIG BA1402. Là một món quà đầu xuân mà chúng mình muốn gửi gắm đến tất cả mọi người. Đây là lúc chúng ta ngồi lại bên nhau, tâm sự, kể về những điều tuyệt vời ta đã làm trong năm cũ, những chặng đường mà FTIC đã cùng nhau đi qua. Từ đó cùng nỗ lực, sát cánh bên nhau trên chặng đường dài phía trước 🌈🌈
😮🤭😤Hòa cùng không khí sắc xuân này, các bạn không chỉ được thưởng thức tiếng nhạc cụ hết sức nhịp nhàng và ấm áp mà còn có cơ hội rinh ngay những phần quà vô cùng giá trị về nhà nữa đấy!!!🎊🎊
Còn chần chừ gì nữa khi mà chúng tớ với chiếc hẹn chan chứa yêu thương đang chờ đợi các cậu! Cùng chúng tớ lắng nghe lời thì thầm của mùa xuân nhé❤️❤️❤️
---------------------------------------------------
⏰Thời gian: 19H30- Thứ năm, ngày 7/3/2019
⛩Địa điểm: Sảnh chính tòa Beta
☎️MỌI CHI TIẾT XIN LIÊN HỆ☎️
Trưởng BTC: Nguyễn Thành Đạt - 0936704211
Phó BTC: Nguyễn Văn Bách - 0974156308 
---------------------------------------------------------------
#XuânYêuThương2019
#CLBNhạcCụTruyềnThống
#DoWhatYouLove - #LoveWhatYouDo
#CTVMangMùaXuânĐến
#FireTeam', 
N'Sảnh chính tòa Beta','2019-03-01 10:00:00',5,'2019-03-07 19:30:00','2019-03-07 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(28,11)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(28,6)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(28,8)

INSERT INTO EventFamily(FamilyName) VALUES (N'Save your soul')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (15, 2, 0, N'Save Your Soul','/Images/Events/SPRING2019_SaveYourSoul.png', 1, 'Closed', 
N'[SPRING TEAM BUILDING : SAVE YOUR SOUL]
---------------------------------------------------------------------
Sau những cuộc nhậu xuân xuyên đêm cùng với bạn bè .
💁🏼‍♀️Những lần nhảy dù cùng với đồng đội
Những trận combat ác liệt trên đấu trường công lý.
💁🏼‍♀️Hay những kèo bài khiến ví của bạn trở nên ốm yếu.........

⏰⏰DỪNG LẠI TRƯỚC KHI QUÁ MUỘN.
HÃY CỨU LẤY LINH HỒN CỦA BẠN !
----------------------------------------------------------------

✒️Linh hồn của bạn đang bị vấy bẩn bởi những cuộc vui chơi quá đà trong nhưng ngày tết, hãy đến với TEAM BUILDING lần này để gột rửa linh hồn, trở về thánh đường. 
🤼‍♀️Team Building lần này mang đến cho các bạn những trải nghiệm thú vị với những game mang đậm dấu ấn của một game show nổi tiếng trên truyền hình "AMAZING RACE" với nhiều thử thách, mật thư hóc búa đầy khó khăn,...Vậy tại sao bạn lại không trở thành một trong những nhà thám hiểm? Hãy tham gia ngay nàooooooo !
🌋Ngoài ra Team Building còn mang đến đêm lửa trại hoành tráng, ấm áp bên ánh lửa bập bùng kỳ ảo 💃💃, và sẽ có một bí mật bất ngờ cho những nhân vật đặc biệt. Không chỉ vậy, bạn còn có cơ hội:
🍻Ăn uống thỏa thích, với nguồn thức ăn vô tận và địa điểm đầy lí thú bạn có thể ăn mãi không hết .
💣Tăng Exp sau Team building
🤝 Hâm nóng tình cảm anh em.
💑Cải thiện các mối quan hệ vẫn còn mập mờ.
🕵🏼‍♀️Khám phá những điều mới lạ, bổ ích.
👼🏻👼🏻👼🏻👼🏻Và nhất là để linh hồn bạn là thánh đường.

Vậy còn chần chờ gì nữa mà không nhanh tay đặt ngay 1 slot cho chuyến đi lần này nào !!!!!!!!!
--••----••----••----••----••----••----••-
🕰 Thời gian : 8/3/2019 đến 10/3/2019
⛰ địa điểm : đồi 79 Mùa Xuân - Mê Linh - Hà Nội.',
N'[SPRING TEAM BUILDING : SAVE YOUR SOUL]
---------------------------------------------------------------------
Sau những cuộc nhậu xuân xuyên đêm cùng với bạn bè .
💁🏼‍♀️Những lần nhảy dù cùng với đồng đội
Những trận combat ác liệt trên đấu trường công lý.
💁🏼‍♀️Hay những kèo bài khiến ví của bạn trở nên ốm yếu.........

⏰⏰DỪNG LẠI TRƯỚC KHI QUÁ MUỘN.
HÃY CỨU LẤY LINH HỒN CỦA BẠN !
----------------------------------------------------------------

✒️Linh hồn của bạn đang bị vấy bẩn bởi những cuộc vui chơi quá đà trong nhưng ngày tết, hãy đến với TEAM BUILDING lần này để gột rửa linh hồn, trở về thánh đường. 
🤼‍♀️Team Building lần này mang đến cho các bạn những trải nghiệm thú vị với những game mang đậm dấu ấn của một game show nổi tiếng trên truyền hình "AMAZING RACE" với nhiều thử thách, mật thư hóc búa đầy khó khăn,...Vậy tại sao bạn lại không trở thành một trong những nhà thám hiểm? Hãy tham gia ngay nàooooooo !
🌋Ngoài ra Team Building còn mang đến đêm lửa trại hoành tráng, ấm áp bên ánh lửa bập bùng kỳ ảo 💃💃, và sẽ có một bí mật bất ngờ cho những nhân vật đặc biệt. Không chỉ vậy, bạn còn có cơ hội:
🍻Ăn uống thỏa thích, với nguồn thức ăn vô tận và địa điểm đầy lí thú bạn có thể ăn mãi không hết .
💣Tăng Exp sau Team building
🤝 Hâm nóng tình cảm anh em.
💑Cải thiện các mối quan hệ vẫn còn mập mờ.
🕵🏼‍♀️Khám phá những điều mới lạ, bổ ích.
👼🏻👼🏻👼🏻👼🏻Và nhất là để linh hồn bạn là thánh đường.

Vậy còn chần chờ gì nữa mà không nhanh tay đặt ngay 1 slot cho chuyến đi lần này nào !!!!!!!!!
--••----••----••----••----••----••----••-
🕰 Thời gian : 8/3/2019 đến 10/3/2019
⛰ địa điểm : đồi 79 Mùa Xuân - Mê Linh - Hà Nội.', 
N'Đồi 79 Mùa Xuân - Mê Linh - Hà Nội','2019-03-01 10:00:00',5,'2019-03-08 8:00:00','2019-03-10 21:00:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(29,11)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(29,7)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(29,12)

INSERT INTO EventFamily(FamilyName) VALUES (N'JS Rose Day')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (16, 2, 0, N'JS Rose Day','/Images/Events/SPRING2019_JSRoseDay.jpg', 1, 'Closed', 
N'🌹 🌹 🌹 JS Roses’s Day🌹🌹🌹
💕 “Roses” là “Hoa hồng”! Một loại hoa tượng trưng cho tình yêu của cánh mày râu dành tặng cho chị em phụ nữ. Nhất là trong các ngày lễ, loài hoa này được sử dụng rất nhiều cùng với những lời chúc tốt đẹp mọi người muốn dành cho nhau. Và trong ngày “Quốc tế Phụ Nữ” sắp tới, những chàng trai trong đại gia đình JS sẽ dành tặng cho các bạn nữ trong CLB hoa hồng đỏ, nhưng đó không phải là những bó hoa hồng đỏ đơn thuần mà sẽ là một buổi lễ “Hoa Hồng Đỏ” rất đặc biệt mà mọi cô gái khác phải ghen tị vì không có được!!!
💕 Sẽ là một buổi lễ không quá cầu kì, phức tạp nhưng nó chứa đựng những tình cảm💓, những lời cảm ơn mà các chàng trai trong ngôi nhà JS muốn dành tặng đến các cô gái xinh đẹp của chúng ta. Và trong buổi lễ không thể thiếu được những mini game, những phần quà hấp dẫn 🎁, một bữa tiệc nhỏ xinh 🍰 và đặc biệt là những điều rất rất bất ngờ mà các “soái ca 🤵 ” của CLB chúng ta dành đến các bạn nữ!!
💕 Đây là sự kiện đầu tiên sau khi những CTV chính thức trở thành thành viên của gia đình JS thân thương, vì vậy rất mong mọi người sẽ bỏ chút thời gian quý báu của mình để đến tham gia, đặc biệt là những cô gái xinh đẹp, dễ thương và không kém phần tài giỏi của chúng ta hãy có mặt đầy đủ để không bỏ lỡ một đêm tuyệt vời mà các chàng trai đem lại!!!! 💗 💗 💗
📅 Thời gian: 19:00 ngày 7/3/2019
📍 Địa điểm: Phòng 401R, Tòa nhà Alpha

#JSRoses’sDay
#Women’sDay
#JSClub',
N'🌹 🌹 🌹 JS Roses’s Day🌹🌹🌹
💕 “Roses” là “Hoa hồng”! Một loại hoa tượng trưng cho tình yêu của cánh mày râu dành tặng cho chị em phụ nữ. Nhất là trong các ngày lễ, loài hoa này được sử dụng rất nhiều cùng với những lời chúc tốt đẹp mọi người muốn dành cho nhau. Và trong ngày “Quốc tế Phụ Nữ” sắp tới, những chàng trai trong đại gia đình JS sẽ dành tặng cho các bạn nữ trong CLB hoa hồng đỏ, nhưng đó không phải là những bó hoa hồng đỏ đơn thuần mà sẽ là một buổi lễ “Hoa Hồng Đỏ” rất đặc biệt mà mọi cô gái khác phải ghen tị vì không có được!!!
💕 Sẽ là một buổi lễ không quá cầu kì, phức tạp nhưng nó chứa đựng những tình cảm💓, những lời cảm ơn mà các chàng trai trong ngôi nhà JS muốn dành tặng đến các cô gái xinh đẹp của chúng ta. Và trong buổi lễ không thể thiếu được những mini game, những phần quà hấp dẫn 🎁, một bữa tiệc nhỏ xinh 🍰 và đặc biệt là những điều rất rất bất ngờ mà các “soái ca 🤵 ” của CLB chúng ta dành đến các bạn nữ!!
💕 Đây là sự kiện đầu tiên sau khi những CTV chính thức trở thành thành viên của gia đình JS thân thương, vì vậy rất mong mọi người sẽ bỏ chút thời gian quý báu của mình để đến tham gia, đặc biệt là những cô gái xinh đẹp, dễ thương và không kém phần tài giỏi của chúng ta hãy có mặt đầy đủ để không bỏ lỡ một đêm tuyệt vời mà các chàng trai đem lại!!!! 💗 💗 💗
📅 Thời gian: 19:00 ngày 7/3/2019
📍 Địa điểm: Phòng 401R, Tòa nhà Alpha

#JSRoses’sDay
#Women’sDay
#JSClub', 
N'Phòng 401R, Tòa nhà Alpha','2019-03-01 10:00:00',5,'2019-03-07 19:00:00','2019-03-07 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(30,11)

INSERT INTO EventFamily(FamilyName) VALUES (N'Day of Chess Club')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (17, 2, 0, N'Happy Women Day of Chess Club','/Images/Events/SPRING2019_FCCWomenDay.jpg', 1, 'Closed', 
N'
😍Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhen 😍
Nhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. 🌸🍰
Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé 💞
Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này 🌏
⚡Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ.
⚡Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan 😚 Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em 🎉',
N'
😍Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhen 😍
Nhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. 🌸🍰
Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé 💞
Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này 🌏
⚡Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ.
⚡Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan 😚 Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em 🎉', 
N'Phòng 205R, Tòa nhà Alpha','2019-03-01 10:00:00',5,'2019-03-07 19:00:00','2019-03-07 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(31,11)

INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (17, 2, 0, N'Happy Man Day of Chess Club','/Images/Events/SPRING2019_FCCWomenDay.jpg', 1, 'Draft', 
N'
😍Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhen 😍
Nhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. 🌸🍰
Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé 💞
Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này 🌏
⚡Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ.
⚡Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan 😚 Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em 🎉',
N'
😍Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhen 😍
Nhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. 🌸🍰
Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé 💞
Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này 🌏
⚡Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ.
⚡Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan 😚 Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em 🎉', 
N'Phòng 205R, Tòa nhà Alpha','2019-03-01 10:00:00',5,'2019-03-07 19:00:00','2019-03-07 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(32,11)

INSERT INTO EventFamily(FamilyName) VALUES (N'Guitar Girls Plaza')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (18, 2, 0, N'Guitar Girls Plaza','/Images/Events/SPRING2019_GIRLPLAZA.jpg', 1, 'Closed', 
N'🎶დ Bọn anh vào đây đơn giản chỉ vì mê tiếng đàn, nhưng sau cùng thứ bọn anh say nhất lại là “em” დ🎶

💐[G-Girls Plaza 2019] GUITAR GIRLS PLAZA💐

👑FROM: Các soái ca FGC cùng tình yêu bất diệt
🕌TO: Tất cả các NỮ THÀNH VIÊN xinh đẹp của chúng tôi

🌼 Thực sự mà nói, bí ẩn về ngày 8/3 của FGC Girls luôn là một trong những câu hỏi lớn và khó trả lời nhất trong lòng chúng tớ. Nối tiếp những event đi trước, năm nay với sự đổi mới hơn cả về kinh nghiệm lẫn chất lượng, hơn hết là quy mô "khủng" nhất từi trước đến nay, hứa hẹn sẽ đem tới các đóa hồng một đêm thật lãng mạng và hạnh phúc hơn bao giờ hết

🏩 Chương trình năm nay được mang tên Guitar Girls Plaza. Tại đây chị em có thể thỏa sức hoàn toàn "free" vui chơi hết mình trong khu trung tâm cực kì hoành tráng này. Có thể kể đến như: Kinh đô thời trang, check in nhận quà, Guitar và những người bạn, phim mà hội trai đẹp diễn, nhảy Kpop và đặc biệt hơn hết là đắm chìm trong tiệc rượu

🍧 Ngày Quốc tế Phụ nữ ở xứ sở Guitar đã sẵn sàng, các nàng hãy cùng chúng tớ khai phá nhé

🚇 Thời gian: 19h00- Thứ 4, ngày 06/03/2019
💒 Địa điểm: Phòng 404L, Tòa nhà Alpha 
💫 Đối tượng tham gia: Nữ thành viên câu lạc bộ Guitar

#GGP
#FGCGirls
#FGCMen',
N'🎶დ Bọn anh vào đây đơn giản chỉ vì mê tiếng đàn, nhưng sau cùng thứ bọn anh say nhất lại là “em” დ🎶

💐[G-Girls Plaza 2019] GUITAR GIRLS PLAZA💐

👑FROM: Các soái ca FGC cùng tình yêu bất diệt
🕌TO: Tất cả các NỮ THÀNH VIÊN xinh đẹp của chúng tôi

🌼 Thực sự mà nói, bí ẩn về ngày 8/3 của FGC Girls luôn là một trong những câu hỏi lớn và khó trả lời nhất trong lòng chúng tớ. Nối tiếp những event đi trước, năm nay với sự đổi mới hơn cả về kinh nghiệm lẫn chất lượng, hơn hết là quy mô "khủng" nhất từi trước đến nay, hứa hẹn sẽ đem tới các đóa hồng một đêm thật lãng mạng và hạnh phúc hơn bao giờ hết

🏩 Chương trình năm nay được mang tên Guitar Girls Plaza. Tại đây chị em có thể thỏa sức hoàn toàn "free" vui chơi hết mình trong khu trung tâm cực kì hoành tráng này. Có thể kể đến như: Kinh đô thời trang, check in nhận quà, Guitar và những người bạn, phim mà hội trai đẹp diễn, nhảy Kpop và đặc biệt hơn hết là đắm chìm trong tiệc rượu

🍧 Ngày Quốc tế Phụ nữ ở xứ sở Guitar đã sẵn sàng, các nàng hãy cùng chúng tớ khai phá nhé

🚇 Thời gian: 19h00- Thứ 4, ngày 06/03/2019
💒 Địa điểm: Phòng 404L, Tòa nhà Alpha 
💫 Đối tượng tham gia: Nữ thành viên câu lạc bộ Guitar

#GGP
#FGCGirls
#FGCMen'

, N'Phòng 404L, Tòa nhà Alpha','2019-03-01 10:00:00',5,'2019-03-06 19:00:00','2019-03-06 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(33,11)

INSERT INTO EventFamily(FamilyName) VALUES (N'FGC X-Factor')
INSERT INTO Event(FamilyId, ManagerId,EventFee, EventName, CoverImage, IsPublic, EventStatus, EventDescription, EventDescriptionHtml, EventPlace, CreatedDate,OrganizerId,OpenDate,CloseDate)
VALUES (19, 2, 0, N'FGC X-Factor','/Images/Events/SPRING2019_XFACTOR.jpg', 1, 'Closed', 
N'🔈🔈🔈 [FGC X-FACTOR SEASON 3 2019] 

🎉 Tiếp nối sự thành công của show Nhạc Kịch đầu tiên được FU Guitar Club tổ chức thì FGC X-FACTOR là sự kiện tiếp theo được câu lạc bộ đầu tư cả về chất lượng sự kiện lẫn kĩ thuật chuyên môn. Không chỉ vậy, FGC X Factor cũng chính là sân chơi lớn tạo cơ hội cho tất cả thành viên cũng như cộng tác viên được sát lại gần nhau hơn. 
👉 Bạn có niềm đam mê với âm nhạc? Bạn có tài năng ca hát, chơi guitar, cajon,..? Bạn có mong muốn được thể hiện bản thân trên sân khấu? Hãy đến với FXF để được hòa mình vào những bản nhạc du dương đầy tâm trạng, được quẩy thật sung cùng những bài hát sôi động mang đầy nhiệt huyết tuổi trẻ. 
💃 Sau 2 năm vắng bóng sự kiện FGC X FACTOR Season 3 (FXF 2019) đã chính thức quay trở lại. Hứa hẹn đây sẽ là 1 cuộc thi đầy cam go và hấp dẫn. 
👏 Mọi người hãy cùng tham gia và theo dõi những điều đặc biệt mà FXF sẽ mang đến cho chúng ta nhé ❤️❤️❤️
---------------------------------------------------

⏰ Thời gian: 19h00 ngày 04 tháng 03 năm 2019
🏫 Địa điểm: Sảnh giảng đường Beta – Đại học FPT khu công nghệ cao Hoà Lạc
👉Thông tin liên hệ: 
- Trưởng BTC: Nguyễn Thị Thùy Linh - 0387050787
- Phó BTC: Nguyễn Việt Hoàng - 0363908833

#FGC
#XFactor
#FXF2019
#TalentOffensive',
N'🔈🔈🔈 [FGC X-FACTOR SEASON 3 2019] 

🎉 Tiếp nối sự thành công của show Nhạc Kịch đầu tiên được FU Guitar Club tổ chức thì FGC X-FACTOR là sự kiện tiếp theo được câu lạc bộ đầu tư cả về chất lượng sự kiện lẫn kĩ thuật chuyên môn. Không chỉ vậy, FGC X Factor cũng chính là sân chơi lớn tạo cơ hội cho tất cả thành viên cũng như cộng tác viên được sát lại gần nhau hơn. 
👉 Bạn có niềm đam mê với âm nhạc? Bạn có tài năng ca hát, chơi guitar, cajon,..? Bạn có mong muốn được thể hiện bản thân trên sân khấu? Hãy đến với FXF để được hòa mình vào những bản nhạc du dương đầy tâm trạng, được quẩy thật sung cùng những bài hát sôi động mang đầy nhiệt huyết tuổi trẻ. 
💃 Sau 2 năm vắng bóng sự kiện FGC X FACTOR Season 3 (FXF 2019) đã chính thức quay trở lại. Hứa hẹn đây sẽ là 1 cuộc thi đầy cam go và hấp dẫn. 
👏 Mọi người hãy cùng tham gia và theo dõi những điều đặc biệt mà FXF sẽ mang đến cho chúng ta nhé ❤️❤️❤️
---------------------------------------------------

⏰ Thời gian: 19h00 ngày 04 tháng 03 năm 2019
🏫 Địa điểm: Sảnh giảng đường Beta – Đại học FPT khu công nghệ cao Hoà Lạc
👉Thông tin liên hệ: 
- Trưởng BTC: Nguyễn Thị Thùy Linh - 0387050787
- Phó BTC: Nguyễn Việt Hoàng - 0363908833

#FGC
#XFactor
#FXF2019
#TalentOffensive'

, N'Sảnh giảng đường Beta','2019-03-01 10:00:00',5,'2019-03-04 19:00:00','2019-03-04 21:30:00')
INSERT INTO EventCategory(EventId,CategoryId) VALUES(34,11)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(34,5)
INSERT INTO EventCategory(EventId,CategoryId) VALUES(34,6)
INSERT INTO Form(EventId,CreatedDate,FormLink,FormRegister,FormResult)
VALUES(34,'2019-03-04 23:59:59','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2','https://goo.gl/forms/dFzbOC2L6g0u1ynf2')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(2,13,'2019-03-01')
INSERT INTO Register(UserId,EventId,RegisteredDate)
VALUES(4,13,'2019-03-01')

Insert into Feedback (UserId, EventId, Value, CreatedDate, FeedbackContent) values (2, 8, 5, '2019-01-16', N'Nhóm mình giành được giải nhất nè, hihi')
Insert into Feedback (UserId, EventId, Value, CreatedDate, FeedbackContent) values (1, 8, 2.5, '2019-02-28', N'Ban tổ chức không chọn nhóm mình :<
cuộc thi thất bại!')
Insert into Feedback (UserId, EventId, Value, CreatedDate, FeedbackContent) values (5, 8, 4.5, '2019-01-18', N'Giải thưởng lớn quá, tiếc là mình không tham dự được :(')
Insert into Feedback (UserId, EventId, Value, CreatedDate, FeedbackContent) values (4, 8, 4, '2019-01-16', N'Chúc mừng nhóm Batah team nha!!!
Ban tổ chức cũng xuất sắc lắm!!')

-- Exported
SET IDENTITY_INSERT [dbo].[Event] ON

INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], 
[ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-04-20 23:59:59', 38, N'Khóa học Public Speaking 15 ngày tại Malaysia và Singapore', NULL, N'/Images/Events/448b0575-d19d-4c90-92ed-22e8012fae40.jpg', 0, 0, N'Opening', N'[SPEAK TRIP] Khóa học Public Speaking 15 ngày tại Malaysia và SingaporeSpeak Trip là một format chương trình hoàn toàn mới do IC-PDP thiết kế, nhằm nâng cao khả năng giao tiếp bằng tiếng Anh.Hành trình Speak Trip đầu tiên với chủ đề Public Speaking sẽ tập trung vào trau dồi kỹ năng nói trước đám đông, khả năng vận dụng tiếng Anh trong tư duy và biểu đạt suy nghĩ, quan điểm bằng ngôn từ một cách hiệu quả nhất – đây là một trong những kỹ năng không thể thiếu cho những công dân trong thời đại toàn cầu hóa. Trong thời gian 2 tuần, các bạn sẽ không những được trau dồi kiến thức và kỹ năng về Public Speaking tại Đại học KDU, Malaysia mà còn được thực hành và áp dụng những kiến thức đã học vào cuộc sống hàng ngày qua những hoạt động trải nghiệm văn hóa đặc sắc tại đảo quốc Sư Tử - Singapore. ------------------THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:- Thời gian: 27/04-12/05/2019 ( 15 ngày bao gồm 3 ngày ở Singapore) - Chi phí: 7.850.000 VNĐ ( Đã bao gồm học phí, chỗ ở, phí di chuyển tới Singapore)- Đối tượng: sinh viên Đại học FPT
Mọi thắc mắc vui lòng liên hệ phòng Hợp tác Quốc tế và Phát triển Cá nhân qua fanpage hoặc SĐT: 024 6680 5910/0903.217.995 (A.Phú)', N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">[SPEAK TRIP] Khóa học Public Speaking 15 ngày tại Malaysia và Singapore</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Speak Trip là một format chương trình hoàn toàn mới do IC-PDP thiết kế, nhằm nâng cao khả năng giao tiếp bằng tiếng Anh.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Hành trình Speak Trip đầu tiên với chủ đề Public Speaking sẽ tập trung vào trau dồi kỹ năng nói trước đám đông, khả năng vận dụng tiếng Anh trong tư duy và biểu đạt suy nghĩ, quan điểm bằng ngôn từ một các</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">h hiệu quả nhất – đây là một trong những kỹ năng không thể thiếu cho những công dân trong thời đại toàn cầu hóa.<span> </span><br><br>Trong thời gian 2 tuần, các bạn sẽ không những được trau dồi kiến thức và kỹ năng về Public Speaking tại Đại học KDU, Malaysia mà còn được thực hành và áp dụng những kiến thức đã học vào cuộc sống hàng ngày qua những hoạt động trải nghiệm văn hóa đặc sắc tại đảo quốc Sư Tử - Singapore.<span> </span><br>------------------<br>THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:<br>- Thời gian: 27/04-12/05/2019 ( 15 ngày bao gồm 3 ngày ở Singapore)<span> </span><br>- Chi phí: 7.850.000 VNĐ ( Đã bao gồm học phí, chỗ ở, phí di chuyển tới Singapore)<br>- Đối tượng: sinh viên Đại học FPT<br>- Đăng ký:<span> https://docs.google.com/forms/d/1xXaWR404LOse-YWeHQbBEgA8xffH0Ztws1GlAbkRZBA/edit</span><br><br>Mọi thắc mắc vui lòng liên hệ phòng Hợp tác Quốc tế và Phát triển Cá nhân qua fanpage hoặc SĐT: 024 6680 5910/0903.217.995 (A.Phú)</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Malaysia, Singapore', 7850000.0000, 5, NULL, CAST(N'2019-04-14T12:22:09.877' AS DateTime), 5, CAST(N'2019-04-14T12:35:29.593' AS DateTime), CAST(N'2019-04-27T08:00:00.000' AS DateTime), CAST(N'2019-05-12T21:30:00.000' AS DateTime))
GO
INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate],
 [ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-04-13 11:59:59', 39, N'[HÀNH TRÌNH THỦ LĨNH] Outsmart Outlast Survival Camp 2019', NULL, N'/Images/Events/506b0790-9765-48fb-9d05-41ef00d2d64e.jpg', 0, 0, N'Opening', N'💥💥[HÀNH TRÌNH THỦ LĨNH] 💥💥 Outsmart Outlast Survival Camp 2019Học kỳ SPRING 2019 này, phòng Hợp tác Quốc tế và Phát triển Cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn BAN CHỦ NHIỆM đến với những trải nghiệm mới, cùng khám phá và mang đến những thử thách mang tính chất sinh tồn để thách thức kỹ năng của mỗi leader. Ngoài ra, đây cũng là một chương trình tập huấn kỹ năng đặc biệt của phòng IC-PDP đến tất cả các bạn BCN cho sự cố gắng, cống hiến và nỗ lực hết mình trong suốt thời gian qua <3Tạm xa Hà Nội náo nhiệt, khoác ba-lô lên và chúng ta sẽ có một chuyến trải nghiệm khó quên nhé các leader <3 <3--------------------------------------------------------------------------⏰Thời gian: Từ ngày 20/04 đến 21/04/2019.📌 Đối tượng: dành riêng cho Ban chủ nhiệm của các CLB, hội nhóm tại trường ĐH FPT.💵 Phí tham gia: 150.000 vnđ/Sinh viên 🧨 Thời hạn đăng ký và đóng tiền: 12h00 ngày 13/04/2019.☎️ Mọi thắc mắc xin vui lòng liên hệ: Anh Nguyễn Bá Khiêm - 0967 002 027', N'<p><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png");''>💥</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png");''>💥</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">[HÀNH TRÌNH THỦ LĨNH]<span> </span></span><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png");''>💥</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png");''>💥</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span> </span>Outsmart Outlast Survival Camp 2019</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Học kỳ SPRING 2019 này, phòng Hợp tác Quốc tế và Phát triển Cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn BAN CHỦ NHIỆM đến với những trải nghiệm mới, cùng khám phá và mang đến những thử thách mang tính chất sinh tồn để thách thức kỹ năng của mỗi leader. Ngoài ra, đây cũng là một chương trình tập huấn kỹ năng đặc biệt của phòn</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">g IC-PDP đến tất cả các bạn BCN cho sự cố gắng, cống hiến và nỗ lực hết mình trong suốt thời gian qua<span> </span><span title="Biểu tượng cảm xúc heart" class="_47e3 _5mfr" style="line-height: 0; vertical-align: middle; margin: 0px 1px; font-family: inherit;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 0px; width: 0px; font-family: inherit;"><3</span></span><br>Tạm xa Hà Nội náo nhiệt, khoác ba-lô lên và chúng ta sẽ có một chuyến trải nghiệm khó quên nhé các leader<span> </span><span title="Biểu tượng cảm xúc heart" class="_47e3 _5mfr" style="line-height: 0; vertical-align: middle; margin: 0px 1px; font-family: inherit;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 0px; width: 0px; font-family: inherit;"><3</span></span><span> </span><span title="Biểu tượng cảm xúc heart" class="_47e3 _5mfr" style="line-height: 0; vertical-align: middle; margin: 0px 1px; font-family: inherit;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 0px; width: 0px; font-family: inherit;"><3</span></span><br><br><span style="font-family: inherit;">--------------------------</span><wbr><span class="word_break" style="display: inline-block; font-family: inherit;"></span><span style="font-family: inherit;">--------------------------</span><wbr><span class="word_break" style="display: inline-block; font-family: inherit;"></span>----------------------<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t34/1/16/23f0.png");''>⏰</span></span>Thời gian: Từ ngày 20/04 đến 21/04/2019.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tac/1/16/1f4cc.png");''>📌</span></span><span> </span>Đối tượng: dành riêng cho Ban chủ nhiệm của các CLB, hội nhóm tại trường ĐH FPT.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tdf/1/16/1f4b5.png");''>💵</span></span><span> </span>Phí tham gia: 150.000 vnđ/Sinh viên<span> </span><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t84/1/16/1f9e8.png");''>🧨</span></span><span> </span>Thời hạn đăng ký và đóng tiền: 12h00 ngày 13/04/2019.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t22/1/16/260e.png");''>☎️</span></span><span> </span>Mọi thắc mắc xin vui lòng liên hệ: Anh Nguyễn Bá Khiêm - 0967 002 027</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Sơn Tinh Camp', 150000.0000, 5, NULL, CAST(N'2019-04-14T12:26:43.627' AS DateTime), 5, CAST(N'2019-04-14T12:35:36.703' AS DateTime), CAST(N'2019-04-20T07:30:00.000' AS DateTime), CAST(N'2019-04-21T19:30:00.000' AS DateTime))
GO
INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], 
[ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-04-09 23:59:59', 40, N'CHƯƠNG TRÌNH TRẢI NGHIỆM ĐẶC BIỆT KẾT HỢP GIỮA ĐẠI HỌC FPT VÀ ĐẠI HỌC INJE, HÀN QUỐC', NULL, N'/Images/Events/8b187cbb-d19f-4ad5-9892-29228a80d295.jpg', 0, 0, N'Opening', 
N'CHƯƠNG TRÌNH TRẢI NGHIỆM ĐẶC BIỆT KẾT HỢP GIỮA ĐẠI HỌC FPT VÀ ĐẠI HỌC INJE, HÀN QUỐC.

Mùa hè rồi sẽ nóng nhưng chương trình này còn nóng hơn cả mùa hè. STUDY TOUR IN KOREA chương trình được rất nhiều sinh viên chờ đợi đã chính thức ra lò.
STUDY TOUR IN KOREA sẽ mang đến cho các bạn: TRẢI NGHIỆM VĂN HOÁ, TÌM HIỂU CON NGƯỜI & CHIÊM NGƯỠNG CẢNH VẬT, ẨM THỰC TẠI ĐẤT NƯỚC HÀN QUỐC!CAMPUS TOUR và chương trình học tập, giao lưu TẠI ĐẠI HỌC INJE.VẬN DỤNG HẾT “CÔNG SUẤT” & SỰ KHÉO LÉO ĐỂ XỬ LÝ TÌNH HUỐNG ĐỜI SỐNG THỰC TẾ KHI TỚI HÀN QUỐC & CÙNG LÚC THI ĐUA VỚI CÁC ĐỘI NHÓM KHÁC! Hành trình kéo dài 7 ngày 6 đêm tại Busan và Seoul, Hàn Quốc lần này sẽ đưa các bạn tới thăm rất nhiều những địa điểm hấp dẫn đó nhé.🤝 Đối tượng tham gia: Sinh viên trường Đại học FPT🤝 Số lượng tham dự: tối đa 20 sinh viên.🤝 Yêu cầu:+ Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các quy định của chuyến đi.+ Bắt buộc sinh viên đăng ký tham gia phải có hộ chiếu. BTC sẽ không nhận đăng ký những bạn chưa có hộ chiếu.🤝Thời gian: 01/07/2019 - 07/ 07/2019 (7 ngày 6 đêm)🤝Phí tham dự: 9.500.000 VNĐ (Đã bao gồm vé máy bay, visa, tiền ở và hỗ trợ ăn uống, đi lại tại điểm)🤝Hạn chót đăng ký và đặt cọc: trước 17h00 thứ Ba ngày 09/04/2019.
LỊCH PHỎNG VẤN: sẽ thông báo cụ thể tới những bạn đăng kí qua emailSINH VIÊN ĐÓNG PHÍ ĐẶT CỌC 5.500.000 VNĐ trực tiếp hoặc chuyển khoản qua thông tin tài khoản:Chuyển khoản tới số TK: 01427436001Chủ TK: Trịnh Phương AnhNgân hàng: Tiên Phong BankNội dung CK: Họ tên_MSV_trai nghiem Han QuocLưu ý: Đơn đăng ký chỉ được tính là thành công khi các bạn hoàn thành tiền đặt cọc.📧THÔNG TIN LIÊN HỆ: Ms. Phương Anh/ Email: anhtp7@fe.edu.vn / Phone: 0978882396', N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">CHƯƠNG TRÌNH TRẢI NGHIỆM ĐẶC BIỆT KẾT HỢP GIỮA ĐẠI HỌC FPT VÀ ĐẠI HỌC INJE, HÀN QUỐC</span><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t21/1/16/1f1f0_1f1f7.png");''>🇰🇷</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Mùa hè rồi sẽ nóng nhưng chương trình này còn nóng hơn cả mùa hè. STUDY TOUR IN KOREA chương trình được rất nhiều sinh viên chờ đợi đã chính thức ra lò.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">STUDY TOUR IN KOREA sẽ mang đến cho các bạn:<span> </span></span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br><br>TRẢI NGHIỆM VĂN HOÁ, TÌM HIỂU CON NGƯỜI & CHIÊM NGƯỠNG CẢNH VẬT, ẨM THỰC TẠI ĐẤT NƯỚC HÀN QUỐC!<br><br>CAMPUS TOUR và chương trình học tập, giao lưu TẠI ĐẠI HỌC INJE.<br><br>VẬN DỤNG HẾT “CÔNG SUẤT” & SỰ KHÉO LÉO ĐỂ XỬ LÝ TÌNH HUỐNG ĐỜI SỐNG THỰC TẾ KHI TỚI HÀN QUỐC & CÙNG LÚC THI ĐUA VỚI CÁC ĐỘI NHÓM KHÁC!<span> </span><br><br>Hành trình kéo dài 7 ngày 6 đêm tại Busan và Seoul, Hàn Quốc lần này sẽ đưa các bạn tới thăm rất nhiều những địa điểm hấp dẫn đó nhé.<br><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span><span> </span>Đối tượng tham gia: Sinh viên trường Đại học FPT<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span><span> </span>Số lượng tham dự: tối đa 20 sinh viên.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span><span> </span>Yêu cầu:<br>+ Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các quy định của chuyến đi.<br>+ Bắt buộc sinh viên đăng ký tham gia phải có hộ chiếu. BTC sẽ không nhận đăng ký những bạn chưa có hộ chiếu.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span>Thời gian: 01/07/2019 - 07/ 07/2019 (7 ngày 6 đêm)<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span>Phí tham dự: 9.500.000 VNĐ (Đã bao gồm vé máy bay, visa, tiền ở và hỗ trợ ăn uống, đi lại tại điểm)<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span>Hạn chót đăng ký và đặt cọc: trước 17h00 thứ Ba ngày 09/04/2019.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span>Link đăng ký:<span> https://docs.google.com/forms/d/1i-FuOSmJHNXmw0dWWsdDVf-9-X4WmdVWiY6Q_CJA9Gc/edit</span><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png");''>🤝</span></span>LỊCH PHỎNG VẤN: sẽ thông báo cụ thể tới những bạn đăng kí qua email<br><br>SINH VIÊN ĐÓNG PHÍ ĐẶT CỌC 5.500.000 VNĐ trực tiếp hoặc chuyển khoản qua thông tin tài khoản:<br>Chuyển khoản tới số TK: 01427436001<br>Chủ TK: Trịnh Phương Anh<br>Ngân hàng: Tiên Phong Bank<br>Nội dung CK: Họ tên_MSV_trai nghiem Han Quoc<br><br>Lưu ý: Đơn đăng ký chỉ được tính là thành công khi các bạn hoàn thành tiền đặt cọc.<br><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tbe/1/16/1f4e7.png");''>📧</span></span>THÔNG TIN LIÊN HỆ: Ms. Phương Anh/ Email: anhtp7@fe.edu.vn / Phone: 0978882396</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Busan, Hàn Quốc', 9500000.0000, 5, NULL, CAST(N'2019-04-14T12:31:42.140' AS DateTime), 5, CAST(N'2019-04-14T12:35:48.377' AS DateTime), CAST(N'2019-07-01T07:30:00.000' AS DateTime), CAST(N'2019-07-07T19:30:00.000' AS DateTime))
GO
INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], 
[ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-04-12 16:59:59', 41, N'Asean Cutural trip tại Bangkok, Thái Lan', NULL, N'/Images/Events/820d3f75-35ab-482d-a17d-afa5dc33921e.jpg', 0, 0, N'Opening',
N'Dạo này IC-PDP liên tục nhận inbox của các bạn sinh viên đang học Little UK than thở rằng sao bao nhiêu chương trình mà toàn nhằm trúng lịch học. 
Để xoa dịu nỗi lòng của các bạn, hôm nay IC-PDP quay trở lại với một chương trình rất thú vị nhằm trúng thời gian các bạn được nghỉ tại Little UK đó nhé. 
THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:
- ĐỐI TƯỢNG: Sinh viên Đại học FPT.
- SỐ LƯỢNG THAM DỰ: Tối đa 30 suất.
- YÊU CẦU: Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các yêu cầu và quy định của chuyến đi.
- THỜI GIAN: 07/05/2019 – 11/05/2019
- PHÍ THAM DỰ: 3.900.000 VNĐ (Chi phí tham dự bao gồm vé máy bay, đi lại, bảo hiểm du lịch và ở tại Thái Lan, sinh viên tự túc các chi phí chi tiêu cá nhân khác).
- HẠN ĐĂNG KÍ & ĐẶT CỌC: 17:00 NGÀY 12/04/2019
- LỊCH PHỎNG VẤN: sẽ thông báo cụ thể tới những bạn đăng ký qua email.
- SINH VIÊN ĐÓNG PHÍ ĐẶT CỌC 2.500.000VNĐ trực tiếp tại 104L hoặc chuyển khoản qua thông tin tài khoản: 
	Chuyển khoản tới số TK: 01427436001
	Chủ TK: Trịnh Phương AnhNgân hàng: Tiên Phong BankNội dung CK: Trai nghiem Thai Lan_Ho va Ten_MSSV
THÔNG TIN LIÊN HỆ: MS. Linh 
SĐT: 0973.153.740 hoặc 024 6680 5910 
Để biết Bangkok có gì hot, hãy nhanh tay đăng ký và tham gia cùng IC-PDP cùng chúng mình nhé. ', N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Dạo này IC-PDP liên tục nhận inbox của các bạn sinh viên đang học Little UK than thở rằng sao bao nhiêu chương trình mà toàn nhằm trúng lịch học. Để xoa dịu nỗi lòng của các bạn, hôm nay IC-PDP quay trở lại với một chương trình rất thú vị nhằm trúng thời gian các bạn được nghỉ tại Little UK đó nhé.<span> </span></span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br>- ĐỐI TƯỢNG: Sinh viên Đại học FPT.<br>- SỐ LƯỢNG THAM DỰ: Tối đa 30 suất.<br>- YÊU CẦU: Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các yêu cầu và quy định của chuyến đi.<br>- THỜI GIAN: 07/05/2019 – 11/05/2019<br>- PHÍ THAM DỰ: 3.900.000 VNĐ (Chi phí tham dự bao gồm vé máy bay, đi lại, bảo hiểm du lịch và ở tại Thái Lan, sinh viên tự túc các chi phí chi tiêu cá nhân khác).<br>- HẠN ĐĂNG KÍ & ĐẶT CỌC: 17:00 NGÀY 12/04/2019<br>- ĐĂNG KÍ THEO FORM:<span> https://docs.google.com/forms/u/1/d/1wjuuSI1_HcSyWgg4XnlvnxnttVPAjoaQbbC_0H5Tze4/edit</span><br>- LỊCH PHỎNG VẤN: sẽ thông báo cụ thể tới những bạn đăng ký qua email.<br>- SINH VIÊN ĐÓNG PHÍ ĐẶT CỌC 2.500.000VNĐ trực tiếp tại 104L hoặc chuyển khoản qua thông tin tài khoản:<span> </span><br>Chuyển khoản tới số TK: 01427436001<br>Chủ TK: Trịnh Phương Anh<br>Ngân hàng: Tiên Phong Bank<br>Nội dung CK: Trai nghiem Thai Lan_Ho va Ten_MSSV<br>THÔNG TIN LIÊN HỆ: MS. Linh SĐT: 0973.153.740 hoặc 024 6680 5910<span> </span><br><br>Để biết Bangkok có gì hot, hãy nhanh tay đăng ký và tham gia cùng IC-PDP cùng chúng mình nhé.<span> </span></span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Bangkok, Thailand', 3900000.0000, 5, NULL, CAST(N'2019-04-14T12:35:04.907' AS DateTime), 5, CAST(N'2019-04-14T12:35:43.453' AS DateTime), CAST(N'2019-05-07T07:30:00.000' AS DateTime), CAST(N'2019-05-11T19:30:00.000' AS DateTime))
GO
INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], 
[ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-04-10 23:59:59', 42, N'Cổ vũ bán kết cuộc đua số', NULL, N'/Images/Events/5f9afcc4-4ca9-4e15-8455-41b52464a883.jpg', 0, 0, N'Closed', N'
Bán kết cuộc đua số

💃Thời gian: 18h30-21h30, 11/04/2019
💃Địa điểm: Nhà thi đấu Cầu Giấy, 35 Trần Quý Kiên, Dịch Vọng, Cầu Giấy, Hà Nội.
💃Phương tiện đi lại: Có xe đưa đón của Ban tổ chức hoàn toàn miễn phí tại sảnh Penrose tòa nha Alpha, 11/04/2019.
🔥 Địa điểm nhận vé mời tham dự Cổ vũ Bán kết Cuộc đua số: Quầy 8, 102L tòa nhà Alpha trong giờ hành chính (8h30-12h, 13h30-17h00 các ngày trong tuần từ 01/04/2019 - 12h, 09/04/2019)
☎️Liên hệ: 02466805915', 

N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">[BÁN KẾT CUỘC ĐUA SỐ 2019]</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t79/1/16/1f68c.png");''>🚌</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"></span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tc7/1/16/1f483.png");''>💃</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Thời gian: 18h30-21h30, 11/04/2019</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tc7/1/16/1f483.png");''>💃</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Địa điểm: Nhà thi đấu Cầu Giấy, 35 Trần Quý Kiên, Dịch Vọng, Cầu Giấy, Hà Nội.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tc7/1/16/1f483.png");''>💃</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Phương tiện đi lại: Có xe đưa đón của Ban tổ chức hoàn toàn miễn phí tại sảnh Penrose tòa nha Alpha, 11/04/2019.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t50/1/16/1f525.png");''>🔥</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span> </span>Địa điểm nhận vé mời tham dự Cổ vũ Bán kết Cuộc đua số: Quầy 8, 102L tòa nhà Alpha trong giờ hành chính (8h30-12h, 13h30-17h00 các ngày trong tuần từ 01/04/2019 - 12h, 09/04/2019)</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t22/1/16/260e.png");''>☎️</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Liên hệ: 02466805915</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Nhà thi đấu Cầu Giấy, 35 Trần Quý Kiên, Dịch Vọng, Cầu Giấy, Hà Nội', 0.0000, 5, NULL, CAST(N'2019-04-14T12:39:04.983' AS DateTime), 5, CAST(N'2019-04-14T12:39:46.453' AS DateTime), CAST(N'2019-04-11T18:30:00.000' AS DateTime), CAST(N'2019-04-11T21:30:00.000' AS DateTime))
GO
INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], 
[ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-04-08 23:59:59', 43, N'Talk show impossible is nothing - finnish ecosystem & innovation', NULL, N'/Images/Events/7929b4ce-281c-42dd-a83e-923a366735f8.jpg', 0, 0, N'Closed', N'😍Bạn có muốn gặp gỡ người đã sáng tạo ra game Angry Bird?🤩Bạn muốn khởi nghiệp nhưng chưa biết bắt đầu từ đâu?💃Bạn muốn nghe các chuyên gia dẫn dắt và bổ sung thêm kinh nghiệm cho mình?😊Hay đơn giản bạn muốn có môi trường được giao tiếp và phản biện tiếng anh?Hãy đến với:🔥🔥TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM & INNOVATION 🔥🔥Talkshow với sự phối hợp của Phòng Công tác sinh viên cùng Văn phòng đại diện tư vấn Phần Lan (WCF) tổ chức với diễn giả Peter Vesterbacka – Nhà phát triển game Angry Bird.Tại #Talkshow bạn sẽ có cơ hội:🍄Tìm hiểu về SLUSH, sự kiện công nghệ lớn nhất hành tinh.🍄Làm thế nào để Angry Birds trở thành thương hiệu toàn cầu.🍄Cùng trò chuyện với nhà phát triển game Angry Bird: Peter Vesterbacka về chủ đề khởi nghiệp.Còn ngại ngần gì mà không đăng ký ngay 1 chiếc ghế xinh xắn trong hội trường vào ngày hôm đó. ;)============================================- Tên chương trình: TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM & INNOVATION- Diễn giả: Peter Vesterbacka – Nhà phát triển game Angry Bird.- MC: Lucy Hoàng- Thời gian: 13:00 – 14:30 ngày thứ Ba, 09/4/2019- Địa điểm: Phòng 102R – 104R nhà Alpha, Đại học FPT.- Liên hệ: 02466805915', N'<p><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t2/1/16/1f60d.png");''>😍</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Bạn có muốn gặp gỡ người đã sáng tạo ra game Angry Bird?</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t58/1/16/1f929.png");''>🤩</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Bạn muốn khởi nghiệp nhưng chưa biết bắt đầu từ đâu?</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tc7/1/16/1f483.png");''>💃</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Bạn muốn nghe các chuyên gia dẫn dắt và bổ sung thêm kinh nghiệm cho mình?</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t7f/1/16/1f60a.png");''>😊</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Hay đơn giản bạn muốn có môi trường được giao tiếp và phản biện tiếng anh?</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Hãy đến với:</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t50/1/16/1f525.png");''>🔥</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t50/1/16/1f525.png");''>🔥</span></span>TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM & INNOVATION<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t50/1/16/1f525.png");''>🔥</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t50/1/16/1f525.png");''>🔥</span></span><br>Talkshow với sự phối hợp của Phòng Công tác sinh viên cùng Văn phòng đại diện tư vấn Phần Lan (WCF) tổ chức với diễn giả Peter Vesterbacka – Nhà phát triển game Angry Bird.<br>Tại<span> </span><a class="_58cn" style="color: rgb(54, 88, 153); cursor: pointer; text-decoration: none; font-family: inherit;" href="https://www.facebook.com/hashtag/talkshow?epa=HASHTAG" data-ft=''{"type":104,"tn":"*N"}''>#Talkshow</a><span> </span>bạn sẽ có cơ hội:<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t8b/1/16/1f344.png");''>🍄</span></span>Tìm hiểu về SLUSH, sự kiện công nghệ lớn nhất hành tinh.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t8b/1/16/1f344.png");''>🍄</span></span>Làm thế nào để Angry Birds trở thành thương hiệu toàn cầu.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t8b/1/16/1f344.png");''>🍄</span></span>Cùng trò chuyện với nhà phát triển game Angry Bird: Peter Vesterbacka về chủ đề khởi nghiệp.<br>Còn ngại ngần gì mà không đăng ký ngay 1 chiếc ghế xinh xắn trong hội trường vào ngày hôm đó.<span> </span><span title="Biểu tượng cảm xúc wink" class="_47e3 _5mfr" style="line-height: 0; vertical-align: middle; margin: 0px 1px; font-family: inherit;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t57/1/16/1f609.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 0px; width: 0px; font-family: inherit;">;)</span></span><br><span style="font-family: inherit;">==========================</span><wbr><span class="word_break" style="display: inline-block; font-family: inherit;"></span>==================<br>Hãy đăng kí ngay tại:<span> https://docs.google.com/forms/u/1/d/1M2hcA0MQ1ubrjo18rSqBk2MNmQyRPRGdbOPCreYM2sc/edit</span><br>- Tên chương trình: TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM & INNOVATION<br>- Diễn giả: Peter Vesterbacka – Nhà phát triển game Angry Bird.<br>- MC: Lucy Hoàng<br>- Thời gian: 13:00 – 14:30 ngày thứ Ba, 09/4/2019<br>- Địa điểm: Phòng 102R – 104R nhà Alpha, Đại học FPT.<br>- Liên hệ: 02466805915</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Phòng 102R – 104R nhà Alpha, Đại học FPT', 0.0000, 5, NULL, CAST(N'2019-04-14T12:43:55.220' AS DateTime), 5, CAST(N'2019-04-14T12:44:08.187' AS DateTime), CAST(N'2019-04-09T13:00:00.000' AS DateTime), CAST(N'2019-04-09T14:30:00.000' AS DateTime))
GO
INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], 
[ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-03-26 23:59:59', 44, N'The dialogue: "Transforming industry to Blockchain: Utop & Akachain story " ', NULL, N'/Images/Events/9e86086a-45a7-42a7-b937-50ff2b831ba5.jpg', 0, 0, N'Closed', N'Sinh viên ĐH FPT có cơ hội đối thoại về công nghệ BlockchainChủ Nhật, ngày 24 tháng 03 năm 2019Ngày 27/3 những bạn trẻ yêu công nghệ Đại học FPT có cơ hội đối thoại cùng diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain xoay quanh chủ đề “Transforming industry to Blockchain: Utop & Akachain story “.Trong làn sóng cách mạng công nghiệp 4.0, Blockchain được xem là một công nghệ “chìa khóa” cho chuyển đổi số và xây dựng nền tảng công nghệ thông tin tương lai.Với khả năng chia sẻ thông tin dữ liệu minh bạch theo thời gian thực, tiết kiệm không gian lưu trữ và bảo mật cao, công nghệ blockchain (chuỗi khối) là một trong những xu hướng công nghệ đột phá, có khả năng ứng dụng rộng rãi ở nhiều ngành nghề, lĩnh vực.Với mong muốn mang tới cho sinh viên những kiên thức bổ ích về công nghệ tiên tiến này, phòng Công tác sinh viên tổ chức sự kiện “The dialogue: “Transforming industry to Blockchain: Utop & Akachain story’ vào ngày 27/3 tại phòng 102R – 104R Alpha. Diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain sẽ giúp các bạn có cái nhìn sâu sắc hơn về làn sóng công nghệ này.Anh Trần Hoàng Giang đã có 10 năm kinh nghiệm trong lĩnh vực lập trình, robot và các thiết bị điều khiển tự động hoá; 6 năm kinh nghiệm trong việc quản lý dự án; 2 năm kinh nghiệm làm quản lý trong bộ phận liên quan đến sản phẩm về Blockchain. Ngoài ra, anh Trần Hoàng Giang còn tham gia vào rất nhiều hoạt động phát triển cộng đồng khác như: Dự án Akachain, Blockchain Empire- Cộng đồng kết nối các kĩ sư phát triển và những người muốn tìm hiểu về công nghệ blockchain, …Đặc biệt, anh Giang chính là cựu sinh viên Đại học FPT và ra nhập FPT Software, đảm nhiệm nhiều vị trí quan trọng tại FPT Software như lập trình viên, quản trị dự án, kiến trúc sư giải pháp, quản trị chương trình Blockchain của FPT Nhật Bản. Trong năm 2019 vừa qua, anh Trần Hoàng Giang vinh dự nằm trong danh sách FPT Under 35 và hiện anh là thành viên tích cực của Ban điều hành Mạng lưới khởi nghiệp Blockchain Việt Nam.Với kinh nghiệm phong phú và đa dạng trong cả lĩnh vực Blockchain lẫn quản lý dự án, buổi đối thoại hứa hẹn sẽ mang tới cho các bạn nhiều câu chuyện thú vị về chuyển đổi số cũng như về cơ hội nghề nghiệp trong ngành này dành cho các bạn sinh viên Đại học FPT.', N'<header class="entry-header" style=''box-sizing: inherit; display: block; color: rgb(64, 64, 64); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif; font-size: 14.4px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;''><h1 class="entry-title" style="box-sizing: inherit; font-size: 2em; margin: 0px; clear: both; line-height: 1.2;">Sinh viên ĐH FPT có cơ hội đối thoại về công nghệ Blockchain</h1><span class="posted-on" style="box-sizing: inherit;"><time class="entry-date published" style="box-sizing: inherit; color: rgb(136, 136, 136); line-height: 1; display: block; margin-top: 1em; font-style: italic;" datetime="2019-03-24T08:53:26+00:00">Chủ Nhật, ngày 24 tháng 03 năm 2019</time></span></header><div class="entry-content" style=''box-sizing: inherit; margin: 1.5em 0px 0px; color: rgb(64, 64, 64); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif; font-size: 14.4px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;''><p style="box-sizing: inherit; margin: 0px 0px 1.5em;"><strong style="box-sizing: inherit; font-weight: bold;">Ngày 27/3 những bạn trẻ yêu công nghệ Đại học FPT có cơ hội đối thoại cùng diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain xoay quanh chủ đề “Transforming industry to Blockchain: Utop & Akachain story “.</strong></p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;"><img width="550" height="733" class="aligncenter wp-image-19952" style="box-sizing: inherit; border: 0px; height: auto; max-width: 100%; clear: both; display: block; margin-left: auto; margin-right: auto;" alt="" src="https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-244x325.jpg" srcset="https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-244x325.jpg 244w, https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-768x1024.jpg 768w, https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-910x1213.jpg 910w, https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o.jpg 1296w" sizes="(max-width: 550px) 100vw, 550px"></p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Trong làn sóng cách mạng công nghiệp 4.0, Blockchain được xem là một công nghệ “chìa khóa” cho chuyển đổi số và xây dựng nền tảng công nghệ thông tin tương lai.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Với khả năng chia sẻ thông tin dữ liệu minh bạch theo thời gian thực, tiết kiệm không gian lưu trữ và bảo mật cao, công nghệ blockchain (chuỗi khối) là một trong những xu hướng công nghệ đột phá, có khả năng ứng dụng rộng rãi ở nhiều ngành nghề, lĩnh vực.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Với mong muốn mang tới cho sinh viên những kiên thức bổ ích về công nghệ tiên tiến này, phòng Công tác sinh viên tổ chức sự kiện “The dialogue: “Transforming industry to Blockchain: Utop & Akachain story’ vào ngày 27/3 tại phòng 102R – 104R Alpha. Diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain sẽ giúp các bạn có cái nhìn sâu sắc hơn về làn sóng công nghệ này.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Anh Trần Hoàng Giang đã có 10 năm kinh nghiệm trong lĩnh vực lập trình, robot và các thiết bị điều khiển tự động hoá; 6 năm kinh nghiệm trong việc quản lý dự án; 2 năm kinh nghiệm làm quản lý trong bộ phận liên quan đến sản phẩm về Blockchain. Ngoài ra, anh Trần Hoàng Giang còn tham gia vào rất nhiều hoạt động phát triển cộng đồng khác như: Dự án Akachain, Blockchain Empire- Cộng đồng kết nối các kĩ sư phát triển và những người muốn tìm hiểu về công nghệ blockchain, …</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;"><img width="550" height="309" class="aligncenter wp-image-19953" style="box-sizing: inherit; border: 0px; height: auto; max-width: 100%; clear: both; display: block; margin-left: auto; margin-right: auto;" alt="" src="https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-578x325.jpg" srcset="https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-578x325.jpg 578w, https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-768x432.jpg 768w, https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-910x512.jpg 910w" sizes="(max-width: 550px) 100vw, 550px"></p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Đặc biệt, anh Giang chính là cựu sinh viên Đại học FPT và ra nhập FPT Software, đảm nhiệm nhiều vị trí quan trọng tại FPT Software như lập trình viên, quản trị dự án, kiến trúc sư giải pháp, quản trị chương trình Blockchain của FPT Nhật Bản. Trong năm 2019 vừa qua, anh Trần Hoàng Giang vinh dự nằm trong danh sách FPT Under 35 và hiện anh là thành viên tích cực của Ban điều hành Mạng lưới khởi nghiệp Blockchain Việt Nam.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Với kinh nghiệm phong phú và đa dạng trong cả lĩnh vực Blockchain lẫn quản lý dự án, buổi đối thoại hứa hẹn sẽ mang tới cho các bạn nhiều câu chuyện thú vị về chuyển đổi số cũng như về cơ hội nghề nghiệp trong ngành này dành cho các bạn sinh viên Đại học FPT.</p></div><p><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Phòng 102R – 104R Alpha', 0.0000, 5, NULL, CAST(N'2019-04-14T12:50:09.093' AS DateTime), 5, CAST(N'2019-04-14T12:50:16.627' AS DateTime), CAST(N'2019-03-27T14:00:00.000' AS DateTime), CAST(N'2019-03-27T16:00:00.000' AS DateTime))
GO
INSERT [dbo].[Event] (RegisterEndDate, [EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], 
[ManagerId], [ApprovedDate], [OpenDate], [CloseDate]) VALUES ('2019-04-20 23:59:59', 45, N'Học Kỳ Trao Đổi Tại Đại Học Kanto Gakuin - Nhật Bản', NULL, N'/Images/Events/74e98206-fec3-4eb1-989d-b9c1f463b890.jpg', 0, 0, N'Opening', N'🎉 HỌC KỲ TRAO ĐỔI TẠI ĐẠI HỌC KANTO GAKUIN - NHẬT BẢN 🇯🇵Tọa lạc tại thành phố cảng Yokohama ⚓️, thủ phủ tỉnh Kanagawa, trường Đại học Kanto Gakuin được biết đến như một trong những trường đại học tư thục tốt nhất của tỉnh Kanagawa nói riêng và của đất nước Nhật Bản nói chung trong suốt hơn 100 năm hình thành và phát triển 💯. Ngôi trường chất lượng này cũng chính là nơi sẽ diễn ra học kỳ trao đổi dành cho các bạn sinh viên FPTU HCM trong kỳ Fall 2019 sắp tới. 🤩🤩🤩Với chương trình Japanese Language and Culture Program, Đại học Kanto Gakuin dành ra 3 suất học trao đổi dành cho sinh viên tham gia học Ngôn ngữ và Văn hoá Nhật Bản. ✨✨✨🔶 Đối tượng: Sinh viên ngành Ngôn ngữ Nhật và khối ngành Kỹ thuật phần mềm. 🙋🏻‍♂️🙋🏻‍♂️🔶 Chương trình sẽ được diễn ra từ 27/08/2019 đến 24/12/2019.🔶 Thông tin chi tiết về chương trình các bạn có thể xem tại đường link: https://bitly.vn/frf🔶Để đăng ký, các bạn vui lòng hoàn thành các bước sau đây 👇:✅ Điền đầy đủ thông tin vào link đăng ký, phòng IC sẽ xem xét và chọn ra các bạn sinh viên đủ tiêu chuẩn, những bạn được chọn sẽ tiếp tục hoàn tất các mẫu form bắt buộc từ phía Đại học Kanto Gakuin. ✅ Sau khi hoàn thành các bước trên, sinh viên sẽ được hỗ trợ để hoàn thành tất cả các thủ tục cuối cùng.‼️Hạn chót đăng ký: 10/04/2019.⁉️ Mọi ý kiến thắc mắc về chương trình, các bạn vui lòng liên hệ fanpage Study Overseas hoặc đến phòng IC (203) nhé!', N'<p style="margin: 1em 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t8c/1/16/1f389.png");''>🎉</span></span><span> </span>HỌC KỲ TRAO ĐỔI TẠI ĐẠI HỌC KANTO GAKUIN - NHẬT BẢN<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t76/1/16/1f1ef_1f1f5.png");''>🇯🇵</span></span></p><p style="margin: 1em 0px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Tọa lạc tại thành phố cảng Yokohama<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t87/1/16/2693.png");''>⚓️</span></span>, thủ phủ tỉnh Kanagawa, trường Đại học Kanto Gakuin được biết đế<span class="text_exposed_show" style="display: inline; font-family: inherit;">n như một trong những trường đại học tư thục tốt nhất của tỉnh Kanagawa nói riêng và của đất nước Nhật Bản nói chung trong suốt hơn 100 năm hình thành và phát triển<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tf1/1/16/1f4af.png");''>💯</span></span>. Ngôi trường chất lượng này cũng chính là nơi sẽ diễn ra học kỳ trao đổi dành cho các bạn sinh viên FPTU HCM trong kỳ Fall 2019 sắp tới.<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t58/1/16/1f929.png");''>🤩</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t58/1/16/1f929.png");''>🤩</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t58/1/16/1f929.png");''>🤩</span></span></span></p><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 1em 0px; font-family: inherit;">Với chương trình Japanese Language and Culture Program, Đại học Kanto Gakuin dành ra 3 suất học trao đổi dành cho sinh viên tham gia học Ngôn ngữ và Văn hoá Nhật Bản.<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tf4/1/16/2728.png");''>✨</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tf4/1/16/2728.png");''>✨</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tf4/1/16/2728.png");''>✨</span></span></p><p style="margin: 1em 0px; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t70/1/16/1f536.png");''>🔶</span></span><span> </span>Đối tượng:<span> </span><br>Sinh viên ngành Ngôn ngữ Nhật và khối ngành Kỹ thuật phần mềm.<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t67/1/16/1f64b_1f3fb_200d_2642.png");''>🙋🏻‍♂️</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t67/1/16/1f64b_1f3fb_200d_2642.png");''>🙋🏻‍♂️</span></span></p><p style="margin: 1em 0px; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t70/1/16/1f536.png");''>🔶</span></span><span> </span>Chương trình sẽ được diễn ra từ 27/08/2019 đến 24/12/2019.</p><p style="margin: 1em 0px; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t70/1/16/1f536.png");''>🔶</span></span><span> </span>Thông tin chi tiết về chương trình các bạn có thể xem tại đường link:<span> </span><a style="color: rgb(54, 88, 153); cursor: pointer; text-decoration: none; font-family: inherit;" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fbitly.vn%2Ffrf%3Ffbclid%3DIwAR2-vEvLUkiSme_6goj2R3mVjxyQfJRWBRJge5Ozu53UJXlPyor2Fee_EiU&h=AT11UrW4tlYlfDtGdtmdqRFht_FTLLMMhlhCcjOPYvFrjzTvrU9pGK4P7IvMeHgzx3jyWXI1OsychH9XThi9lOiqS3nmbcoKkGZmrdOW02uoTy1RTzWcBhFfTGP7dQlzytnpiW4QsJwAwKOIv65zSDvYJvvmmwqpqrXMtmFJZX2VMv9G7I1KRz6OJYFsRsmk4jlQChGs4NWk0GHRResk2P9A8Pdzu9rgRh768SveRT9acqxv0UbfTHnJ5H0CyiSTk1jyiaWE9w5oFAfKKDzFJZMvUO0fxuZ3WoQAekouHZdePu9lwfUF2_dwQ1wyP_hxettHMFN_YZu6Dmb0IYHWNhl01B5v_M-M7siZmjwP6JaZ7eNOsfcaRG3BtOQEXhLI2db1aJ-HeXb_ZAtQMujdB9MdtWtHKmrLAS_OiMfJ4EmXgdJtsOI_COT9ISg6MoZ1EnLkLISYy2lGgBNKSJPcVG4gShRgABFVdWBlxAaUh3xhCXZ8PqDYC6YpXz-tVz0iKKsX3D-iWhlZ8q1xAcASBakWJYAfnThJftKVaRXhVLUwnXNNvopWhEgaqa9Q-zWQ76ZaU_DvnskUb55CVKBmV0_t5ueODDbjWVX8WzE9MDtw9rZmnMWjGp3Xql6RtHTj4QoNYZgKssb8Qk8SeXccRfhy4PnWhMe-nS_4ASFFOsURXejIaKxT6TnQMbtxBhOqDLtAW6YIhi_QJPtrRph6ttUJFI5212NliepwmETYH_sX20DK2CG9paEUFsiKBHfs7uC5msaHXEoOif8M0-kPjjS4t1D_WiBXg9bLxjWdaVY40vz8wstMAG012DTNn8UihxNWecZavHZEmVv9UcQaLubVP9PJtEzmV7u3tBmQqChEVFPcGw" target="_blank" rel="noopener nofollow" data-ft=''{"tn":"-U"}'' data-lynx-mode="async">https://bitly.vn/frf</a></p><p style="margin: 1em 0px; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t70/1/16/1f536.png");''>🔶</span></span>Để đăng ký, các bạn vui lòng hoàn thành các bước sau đây<span> </span><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t4f/1/16/1f447.png");''>👇</span></span>:</p><p style="margin: 1em 0px; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t33/1/16/2705.png");''>✅</span></span><span> </span>Điền đầy đủ thông tin vào link đăng ký:<span> https://docs.google.com/forms/d/1eiEqmFaAPdEEO191aUOya-woCBzCCnB6RHaYWsjCpv0/edit</span><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t33/1/16/2705.png");''>✅</span></span><span> </span>Phòng IC sẽ xem xét và chọn ra các bạn sinh viên đủ tiêu chuẩn, những bạn được chọn sẽ tiếp tục hoàn tất các mẫu form bắt buộc từ phía Đại học Kanto Gakuin.<span> </span><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t33/1/16/2705.png");''>✅</span></span><span> </span>Sau khi hoàn thành các bước trên, sinh viên sẽ được hỗ trợ để hoàn thành tất cả các thủ tục cuối cùng.</p><p style="margin: 1em 0px; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t77/1/16/203c.png");''>‼️</span></span>Hạn chót đăng ký: 10/04/2019.</p><p style="margin: 1em 0px 0px; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style=''background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tec/1/16/2049.png");''>⁉️</span></span><span> </span>Mọi ý kiến thắc mắc về chương trình, các bạn vui lòng liên hệ fanpage Study Overseas hoặc đến phòng IC (203) nhé!</p></div><p style="margin: 1em 0px 0px; font-family: inherit;"><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><img alt="Trong hÃ¬nh áº£nh cÃ³ thá» cÃ³: báº§u trá»i, ÄÃ¡m mÃ¢y, ngoÃ i trá»i vÃ  nÆ°á»c" src="https://scontent.fhan3-3.fna.fbcdn.net/v/t1.0-9/51623680_2251740125041355_1552057698801942528_n.jpg?_nc_cat=100&_nc_oc=AQkuql7qzGFpasNSJli5i4gtV3KqadqAVsVkppeL7nTvAWuaJpygVCDxyOJx5q7r7E6FiOSQqL-5BR5N4ZcPu8n7&_nc_ht=scontent.fhan3-3.fna&oh=c89c8d9e5934c8ea809f1970aa2b7a11&oe=5D44BD9E"></div><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br></div><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br></div></p><div class="text_exposed_show" style="color: rgb(102, 102, 102); text-transform: none; text-indent: 0px; letter-spacing: normal; font-family: Helvetica,Arial,sans-serif; font-size: 12px; word-spacing: 0px; display: inline; white-space: normal; orphans: 2; widows: 2; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);"><img style="width: 599.12px; height: 449.34px;" alt="Trong hÃ¬nh áº£nh cÃ³ thá» cÃ³: báº§u trá»i, cÃ¢y vÃ  ngoÃ i trá»i" src="https://scontent.fhan3-3.fna.fbcdn.net/v/t1.0-9/51899496_2251740145041353_173572397369131008_n.jpg?_nc_cat=106&_nc_oc=AQns65QLvWwdBTvtWqstZb39-9bm8JK-EZy4krd_9bRwE4uR4zHMrG9jceC_nvhprJAwFCE8wGnZkX27rpFjKUil&_nc_ht=scontent.fhan3-3.fna&oh=d4b8cc877ece3eed27beeb87fde35888&oe=5D4DD43B"></div><br>', N'Kanto, Japan', 73600000.0000, 5, NULL, CAST(N'2019-04-14T12:59:16.313' AS DateTime), 5, CAST(N'2019-04-14T12:59:24.470' AS DateTime), CAST(N'2019-08-27T05:59:00.000' AS DateTime), CAST(N'2019-12-24T05:59:00.000' AS DateTime))
GO

SET IDENTITY_INSERT [dbo].[Event] OFF

GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (38, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (38, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (38, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (39, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (39, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (40, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (40, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (41, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (41, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (41, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (42, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (43, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (43, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (44, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (45, 9)
GO

INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 38, 2.5, CAST(N'2019-04-14T14:32:24.677' AS DateTime), N'楽しみ')
GO
INSERT [dbo].[FeedbackOuter] ([Key], [EventId], [Value], [CreatedDate], [FeedbackContent], [UserName], [UserImage]) VALUES (N'1758724840893851', 10, 4.5, CAST(N'2019-04-12T00:54:31.587' AS DateTime), N'Test nè', N'Phương Nguyễn', N'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1758724840893851&height=200&width=200&ext=1557597263&hash=AeScQIK59ZU9CQgb')
GO
INSERT [dbo].[FeedbackOuter] ([Key], [EventId], [Value], [CreatedDate], [FeedbackContent], [UserName], [UserImage]) VALUES (N'2138011532980792', 10, 0.5, CAST(N'2019-04-12T00:30:18.657' AS DateTime), N'aaa', N'Nguyễn Hùng Tiến', N'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2138011532980792&height=200&width=200&ext=1557595789&hash=AeT3Z8Abh-r2gbg2')
GO
SET IDENTITY_INSERT [dbo].[Form] ON 
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (14, 38, N'https://docs.google.com/forms/d/1xXaWR404LOse-YWeHQbBEgA8xffH0Ztws1GlAbkRZBA/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSehQSfcwuQvyMNMu7FNE5ad8GrF9tpHdDCMuSui4GGrfzg5FA/viewform?usp=pp_url&entry.2136228084=ThangLVSE04854&entry.1256107218=1997-01-13&entry.108773365=thanglvse04854@fpt.edu.vn&entry.235571913=0332554026&embedded=true', N'https://docs.google.com/forms/d/1xXaWR404LOse-YWeHQbBEgA8xffH0Ztws1GlAbkRZBA/viewanalytics?embedded=true', CAST(N'2019-04-14T12:22:09.860' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (15, 40, N'https://docs.google.com/forms/d/1i-FuOSmJHNXmw0dWWsdDVf-9-X4WmdVWiY6Q_CJA9Gc/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSedeHrr4Ce8hxTSZaphqFwqLwtF9etfqbZ0rsDdEKrAxfiL-g/viewform?usp=pp_url&entry.1460515575=ThangLVSE04854&entry.1218073800=1997-01-13&entry.585447260=thanglvse04854@fpt.edu.vn&entry.172889523=0332554026&embedded=true', N'https://docs.google.com/forms/d/1i-FuOSmJHNXmw0dWWsdDVf-9-X4WmdVWiY6Q_CJA9Gc/viewanalytics?embedded=true', CAST(N'2019-04-14T12:31:42.127' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (16, 41, N'https://docs.google.com/forms/d/1wjuuSI1_HcSyWgg4XnlvnxnttVPAjoaQbbC_0H5Tze4/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSc0BKdET99hf4OrqmLJy4g3ff4JMvT7g6EeM5wMqssg6qhkYw/viewform?usp=pp_url&entry.547838405=ThangLVSE04854&entry.1591795084=1997-01-13&entry.2130651168=thanglvse04854@fpt.edu.vn&entry.60036003=0332554026&embedded=true', N'https://docs.google.com/forms/d/1wjuuSI1_HcSyWgg4XnlvnxnttVPAjoaQbbC_0H5Tze4/viewanalytics?embedded=true', CAST(N'2019-04-14T12:35:04.907' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (17, 42, N'https://docs.google.com/forms/d/1hFyTrD16U69PIv3C33FZefiIEr2pCewq0CxY4-Cd4OA/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSfNOJolCVpDxuiBUbN_mluspLbERB0Oc8l50EZlC-uJVkkfNA/viewform?usp=pp_url&entry.84598528=ThangLVSE04854&entry.1784985863=1997-01-13&entry.1719326975=thanglvse04854@fpt.edu.vn&entry.858957936=0332554026&embedded=true', N'https://docs.google.com/forms/d/1hFyTrD16U69PIv3C33FZefiIEr2pCewq0CxY4-Cd4OA/viewanalytics?embedded=true', CAST(N'2019-04-14T12:39:04.983' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (18, 43, N'https://docs.google.com/forms/d/1M2hcA0MQ1ubrjo18rSqBk2MNmQyRPRGdbOPCreYM2sc/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSe4WgGhTeOF2NO2x8jB3Hw-eimqjJTRDE0zGFPdNbYVjUpJbw/viewform?usp=pp_url&entry.1429928612=ThangLVSE04854&entry.1805681664=1997-01-13&entry.1832696572=thanglvse04854@fpt.edu.vn&entry.458959013=0332554026&embedded=true', N'https://docs.google.com/forms/d/1M2hcA0MQ1ubrjo18rSqBk2MNmQyRPRGdbOPCreYM2sc/viewanalytics?embedded=true', CAST(N'2019-04-14T12:43:55.220' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (19, 44, N'https://docs.google.com/forms/d/1eiEqmFaAPdEEO191aUOya-woCBzCCnB6RHaYWsjCpv0/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSde5r6nnZ7jmulaLbYqQ_UDsAueUeuuhitqPzL8neKvVHYryA/viewform?usp=pp_url&entry.1862883321=ThangLVSE04854&entry.1529378116=1997-01-13&entry.495271899=thanglvse04854@fpt.edu.vn&entry.2118770351=0332554026&embedded=true', N'https://docs.google.com/forms/d/1eiEqmFaAPdEEO191aUOya-woCBzCCnB6RHaYWsjCpv0/viewanalytics?embedded=true', CAST(N'2019-04-14T12:50:09.093' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (20, 45, N'https://docs.google.com/forms/d/1-NSuZtqii3haN6YMIObxUM48ov6dXQgyzUM_4vb-74M/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSd8kIik2FkS5zbkNetV-TtmlwzsymzJ5ARNDsX4_F_12_riZw/viewform?usp=pp_url&entry.1501009126=ThangLVSE04854&entry.1644490591=1997-01-13&entry.714784524=thanglvse04854@fpt.edu.vn&entry.1047446744=0332554026&embedded=true', N'https://docs.google.com/forms/d/1-NSuZtqii3haN6YMIObxUM48ov6dXQgyzUM_4vb-74M/viewanalytics?embedded=true', CAST(N'2019-04-14T12:59:16.313' AS DateTime))
GO

SET IDENTITY_INSERT [dbo].[Form] OFF
GO
SET IDENTITY_INSERT [dbo].[Group] ON 
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (10, N'JS1102 Class', N'/Images/Groups/DSC04839.jpg', N'Đây là lớp JS1102', NULL, NULL, 1, 5, CAST(N'2019-04-11T21:35:01.220' AS DateTime), 1, CAST(N'2019-04-11T21:35:01.220' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Group] OFF 
GO
SET IDENTITY_INSERT [dbo].[UserProfile] ON 
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (9, N'SE04976', N'Tiến', N'Nguyễn Hùng', CAST(N'1997-01-10' AS Date), 1, N'0344216717', N'Số 63 ngách 33 văn chương 2 Đống Đa Hà Nội', N'Số 63 ngách 33 văn chương 2 Đống Đa Hà Nội', N'/Images/ProfileImage/5c196851-cb3b-4989-82f3-c75176af0974.jpg', N'Hòa Lạc', N'BSE', N'JS', N'Nguyễn Hùng Khanh', N'0904121911-0979695554')
GO
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled]) VALUES (8, NULL, N'baopnse04414@fpt.edu.vn', 1, 0, 0, 0, 0)
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled]) VALUES (21, 9, N'tiennhse04976@fpt.edu.vn', 1, 0, 0, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 10, CAST(N'2019-04-15T01:12:42.720' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (4, 10, CAST(N'2019-04-11T21:35:37.390' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 10, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 1, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 2, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 3, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 4, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 5, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 6, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 7, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 8, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 9, CAST(N'2019-04-14T21:27:22.767' AS DateTime))
GO