namespace capstone.database.Database {
    using System.Data.Entity;
    using capstone.database.Entities;

    public partial class Database : DbContext {
        public Database()
            : base("name=Database") {
        }

        public virtual DbSet<Bookmark> Bookmarks { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Checkin> Checkins { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<EventFamily> EventFamilies { get; set; }
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<FeedbackOuter> FeedbackOuters { get; set; }
        public virtual DbSet<Form> Forms { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Register> Registers { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<Campus> Campuses { get; set; }
        public virtual DbSet<Building> Buildings { get; set; }
        public virtual DbSet<SmartTV> SmartTVs { get; set; }
        public virtual DbSet<EventRole> EventRoles { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.Events)
                .WithMany(e => e.Categories)
                .Map(m => m.ToTable("EventCategory").MapLeftKey("CategoryId").MapRightKey("EventId"));

            modelBuilder.Entity<Event>()
                .Property(e => e.EventFee)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.Bookmarks)
                .WithRequired(e => e.Event)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.Checkins)
                .WithRequired(e => e.Event)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.Feedbacks)
                .WithRequired(e => e.Event)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.FeedbackOuters)
                .WithRequired(e => e.Event)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.Registers)
                .WithRequired(e => e.Event)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.EventRoles)
                .WithRequired(e => e.Event)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.Reports)
                .WithRequired(e => e.Event)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SmartTV>()
               .HasMany(e => e.Events)
               .WithMany(e => e.SmartTVs)
               .Map(m => m.ToTable("EventTv").MapLeftKey("TvId").MapRightKey("EventId"));

            modelBuilder.Entity<Campus>()
                .HasMany(c => c.Events)
                .WithRequired(c => c.Campus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Campus>()
                .HasMany(c => c.Buildings)
                .WithRequired(c => c.Campus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Building>()
                .HasMany(b => b.SmartTVs)
                .WithRequired(b => b.Building)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FeedbackOuter>()
                .Property(e => e.Key)
                .IsUnicode(false);

            modelBuilder.Entity<Group>()
                .HasMany(e => e.UserGroups)
                .WithRequired(e => e.Group)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Bookmarks)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Categories)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Checkins)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Events)
                .WithOptional(e => e.Manager)
                .HasForeignKey(e => e.ManagerId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.OrganizedEvents)
                .WithOptional(e => e.Organizer)
                .HasForeignKey(e => e.OrganizerId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Feedbacks)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Groups)
                .WithRequired(e => e.Leader)
                .HasForeignKey(e => e.LeaderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ApprovedGroups)
                .WithRequired(e => e.Manager)
                .HasForeignKey(e => e.ManagerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SubjectedNotifications)
                .WithOptional(e => e.Subject)
                .HasForeignKey(e => e.SubjectId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Notifications)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Registers)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
            .HasMany(e => e.EventRoles)
            .WithRequired(e => e.User)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Reports)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserGroups)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.OrganizerId)
                .WillCascadeOnDelete(false);
        }
    }
}
