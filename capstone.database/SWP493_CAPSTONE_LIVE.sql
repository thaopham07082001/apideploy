USE master
GO
IF EXISTS(select * from sys.databases where name='SWP493_CAPSTONE')
	DROP DATABASE [SWP493_CAPSTONE]
	GO
CREATE DATABASE SWP493_CAPSTONE
GO
USE [SWP493_CAPSTONE]

GO
/****** Object:  Table [dbo].[Bookmark]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bookmark](
	[UserId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
	[CategoryDescription] [nvarchar](max) NULL,
	[IsEnabled] [bit] NOT NULL,
	[CreatorId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Checkin]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Checkin](
	[UserId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[CheckedinDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[EventName] [nvarchar](100) NOT NULL,
	[FamilyId] [int] NULL,
	[CoverImage] [nvarchar](max) NOT NULL,
	[IsPublic] [bit] NOT NULL,
	[IsFeatured] [bit] NOT NULL,
	[EventStatus] [nvarchar](10) NOT NULL,
	[EventDescription] [nvarchar](max) NOT NULL,
	[EventDescriptionHtml] [nvarchar](max) NOT NULL,
	[EventPlace] [nvarchar](100) NOT NULL,
	[EventFee] [money] NOT NULL,
	[OrganizerId] [int] NULL,
	[GroupId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ManagerId] [int] NULL,
	[ApprovedDate] [datetime] NULL,
	[OpenDate] [datetime] NOT NULL,
	[CloseDate] [datetime] NOT NULL,
	[RegisterEndDate] [datetime] NULL,
	[IsOrganizerOnly] [bit] NOT NULL,
	[Participants] [nvarchar](max) NULL,
	[RegisterMax] [int] NULL,
	[TargetGroup] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventCategory]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventCategory](
	[EventId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC,
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventFamily]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventFamily](
	[FamilyId] [int] IDENTITY(1,1) NOT NULL,
	[FamilyName] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FamilyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[UserId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[FeedbackContent] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeedbackOuter]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeedbackOuter](
	[Key] [varchar](200) NOT NULL,
	[EventId] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[FeedbackContent] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[UserImage] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Form]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Form](
	[FormId] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[FormLink] [nvarchar](max) NOT NULL,
	[FormRegister] [nvarchar](max) NOT NULL,
	[FormResult] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[GroupImage] [nvarchar](max) NULL,
	[GroupDescription] [nvarchar](max) NULL,
	[GroupMail] [nvarchar](100) NULL,
	[FoundedYear] [date] NULL,
	[IsEnabled] [bit] NOT NULL,
	[ManagerId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LeaderId] [int] NOT NULL,
	[AssignedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ObjectId] [int] NULL,
	[ObjectType] [nvarchar](10) NULL,
	[NotificationContent] [nvarchar](max) NULL,
	[NotifiedDate] [datetime] NOT NULL,
	[SubjectId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Register]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Register](
	[UserId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[RegisteredDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Report]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Report](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[IsDismissed] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NULL,
	[Email] [nvarchar](30) NOT NULL,
	[IsStudent] [bit] NOT NULL,
	[IsOrganizer] [bit] NOT NULL,
	[IsManager] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[NotificationSeenDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[OrganizerId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[ParticipatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[OrganizerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 5/2/2019 21:45:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[ProfileId] [int] IDENTITY(1,1) NOT NULL,
	[RollNumber] [nvarchar](10) NULL,
	[FirstName] [nvarchar](20) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[DOB] [date] NULL,
	[Gender] [bit] NOT NULL,
	[Phone] [nvarchar](20) NULL,
	[City] [nvarchar](100) NULL,
	[Address] [nvarchar](max) NULL,
	[ProfileImage] [nvarchar](max) NULL,
	[Campus] [nvarchar](100) NOT NULL,
	[Major] [nvarchar](100) NULL,
	[Specialization] [nvarchar](100) NULL,
	[ParentName] [nvarchar](100) NULL,
	[ParentPhone] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (1, 39, CAST(N'2019-04-21T20:08:07.203' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (1, 40, CAST(N'2019-02-20T01:22:06.500' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (1, 51, CAST(N'2019-04-21T23:57:06.063' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (1, 56, CAST(N'2019-04-17T17:12:52.830' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (2, 39, CAST(N'2019-04-21T20:08:33.157' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (2, 53, CAST(N'2019-04-16T15:48:35.233' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (2, 56, CAST(N'2019-04-24T21:24:58.813' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (2, 57, CAST(N'2019-04-20T16:21:16.377' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (2, 70, CAST(N'2019-04-24T21:57:01.907' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (3, 38, CAST(N'2019-04-16T16:21:43.077' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (3, 39, CAST(N'2019-04-21T20:07:15.797' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (3, 63, CAST(N'2019-04-20T15:05:29.280' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (4, 4, CAST(N'2019-04-24T12:46:23.593' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (4, 39, CAST(N'2019-04-21T20:07:42.343' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (4, 50, CAST(N'2019-04-23T16:22:00.423' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (4, 57, CAST(N'2019-04-20T15:10:24.577' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 3, CAST(N'2019-04-15T23:36:56.063' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 10, CAST(N'2019-04-15T23:37:36.127' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 15, CAST(N'2019-04-28T04:34:17.907' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 16, CAST(N'2019-04-29T01:15:53.750' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 29, CAST(N'2019-04-21T14:31:08.673' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 31, CAST(N'2019-04-15T23:37:34.343' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 34, CAST(N'2019-04-15T23:37:37.877' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 38, CAST(N'2019-04-20T17:39:28.890' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 39, CAST(N'2019-04-21T20:05:46.673' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 40, CAST(N'2019-04-15T23:37:05.657' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 41, CAST(N'2019-04-28T10:30:11.933' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 45, CAST(N'2019-04-28T10:39:06.673' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 48, CAST(N'2019-04-15T23:16:40.593' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 51, CAST(N'2019-04-21T02:55:29.157' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 54, CAST(N'2019-04-17T16:16:07.673' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 55, CAST(N'2019-04-17T16:30:52.080' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 56, CAST(N'2019-04-28T10:29:45.223' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 57, CAST(N'2019-04-29T20:17:21.657' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 68, CAST(N'2019-04-22T05:37:19.047' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (5, 69, CAST(N'2019-04-25T01:37:08.780' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (8, 39, CAST(N'2019-04-15T22:30:26.390' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (23, 38, CAST(N'2019-04-16T16:28:13.267' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (24, 38, CAST(N'2019-04-16T18:13:31.737' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (25, 38, CAST(N'2019-04-16T20:01:15.360' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (26, 38, CAST(N'2019-04-20T17:26:55.157' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (26, 57, CAST(N'2019-04-20T16:03:24.983' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (28, 38, CAST(N'2019-04-16T21:05:21.157' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (29, 38, CAST(N'2019-04-20T17:48:15.280' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (30, 38, CAST(N'2019-04-17T10:12:40.517' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (30, 54, CAST(N'2019-02-20T01:27:12.500' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (31, 38, CAST(N'2019-04-20T17:56:39.923' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (31, 39, CAST(N'2019-04-21T20:09:26.970' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (32, 38, CAST(N'2019-04-20T17:55:33.407' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (32, 39, CAST(N'2019-04-21T20:08:49.827' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (35, 41, CAST(N'2019-04-23T22:35:17.483' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (35, 56, CAST(N'2019-04-29T20:32:59.343' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (35, 57, CAST(N'2019-04-20T15:08:49.297' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (35, 68, CAST(N'2019-05-01T19:19:14.797' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (36, 38, CAST(N'2019-04-20T17:40:26.860' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (37, 38, CAST(N'2019-04-20T17:39:53.437' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (37, 57, CAST(N'2019-04-20T15:56:37.093' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (39, 38, CAST(N'2019-04-20T17:28:31.673' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (40, 38, CAST(N'2019-04-20T17:41:32.063' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (42, 38, CAST(N'2019-04-20T17:50:09.077' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (43, 38, CAST(N'2019-04-20T17:51:50.970' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (44, 38, CAST(N'2019-04-20T17:53:07.767' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (45, 38, CAST(N'2019-04-20T17:59:24.140' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (46, 38, CAST(N'2019-04-20T18:04:19.470' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (47, 38, CAST(N'2019-04-20T18:06:49.890' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (48, 38, CAST(N'2019-04-20T18:11:27.797' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (49, 38, CAST(N'2019-04-20T18:12:58.577' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (57, 38, CAST(N'2019-04-21T01:06:32.047' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (58, 38, CAST(N'2019-04-21T11:47:11.937' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (59, 38, CAST(N'2019-04-21T11:48:37.407' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (60, 38, CAST(N'2019-04-21T11:49:22.530' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (61, 38, CAST(N'2019-04-21T11:50:28.157' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (71, 51, CAST(N'2019-04-22T00:23:04.890' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (71, 57, CAST(N'2019-04-22T00:12:17.720' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (93, 69, CAST(N'2019-04-25T22:36:26.923' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (100, 68, CAST(N'2019-05-02T20:22:44.813' AS DateTime))
GO
INSERT [dbo].[Bookmark] ([UserId], [EventId], [CreatedDate]) VALUES (101, 38, CAST(N'2019-02-20T02:14:38.483' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Category] ON 
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (1, N'Softskill workshop', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (2, N'Recruitment workshop', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (3, N'Orientation workshop', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (4, N'Learning workshop', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (5, N'Competition', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (6, N'Music', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (7, N'Field trip', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (8, N'Traditional Festival', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (9, N'Learning exchange', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (10, N'Recruitement', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (11, N'Internal culture', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryDescription], [IsEnabled], [CreatorId], [CreatedDate]) VALUES (12, N'Teambuilding', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.437' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (1, 38, CAST(N'2019-04-30T02:30:09.407' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (1, 39, CAST(N'2019-04-21T20:13:03.030' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (2, 39, CAST(N'2019-04-21T20:13:04.000' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (3, 39, CAST(N'2019-04-21T20:13:04.530' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (3, 63, CAST(N'2019-04-20T15:15:31.203' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (5, 38, CAST(N'2019-04-29T20:28:10.093' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (5, 39, CAST(N'2019-04-21T20:13:06.110' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (31, 39, CAST(N'2019-04-21T20:13:06.827' AS DateTime))
GO
INSERT [dbo].[Checkin] ([UserId], [EventId], [CheckedinDate]) VALUES (48, 38, CAST(N'2019-04-30T02:30:03.313' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Event] ON 
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) 
VALUES (1, N'Tết Dân Gian 2019', NULL, N'/Images/Events/SPRING2019_TetDanGian.jpg', 0, 0, N'Closed', N'[TẾT DÂN GIAN 2019]
Vậy là một năm nữa lại sắp qua. Cái lạnh của những ngày cuối năm chẳng thể làm giảm đi sự ồn ào, tấp nập của đường phố, sự rạng rỡ của các loài hoa đua nhau khoe sắc, và sự háo hức, phấn khởi chờ đón một năm mới đầy sung túc, hy vọng...
Hoà chung với không khí đó chúng mình có một món quà nhỏ, dành cho tất cả cư dân xứ sở Hola đây:
CHƯƠNG TRÌNH TẾT DÂN GIAN – SẮC XUÂN 2019
Hãy cùng tham gia Sắc Xuân 2019 cùng chúng mình để: 
+ Hoà mình vào không khí náo nhiệt Tết đến Xuân về
+ Cùng trải nghiệm những trò chơi thú vị như đập niêu, bắt vịt, limbo...và nhận những phần quà đặc biệt từ BTC
+ Thưởng thức những món ăn phong phú, đa dạng đến từ các gian hàng đặc sắc của các thần dân vô cùng tài năng xứ sở Hola 
+ Cùng nhau (học) gói bánh chưng - một phong tục không thể thiếu trong ngày Tết Việt
+ Và điều thú vị hơn, đến với Sắc Xuân 2019 các thần dân sẽ có cơ hội “khi đi thì lẻ bóng khi về sẽ có đôi” đó nhé.
Còn đợi chờ gì nữa, các bạn đã sẵn sàng đón Tết trên vương quốc HoLa của chúng mình chưa nào???!
--------------------
THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:
Thời gian: Thứ Sáu - ngày 18/1/2019
Địa điểm: Khuôn viên đại học FPT

#Tếtdângian
#Sắcxuân 
#FPTU
#ICPDP 
#CoreTeamk14', N'[TẾT DÂN GIAN 2019]
Vậy là một năm nữa lại sắp qua. Cái lạnh của những ngày cuối năm chẳng thể làm giảm đi sự ồn ào, tấp nập của đường phố, sự rạng rỡ của các loài hoa đua nhau khoe sắc, và sự háo hức, phấn khởi chờ đón một năm mới đầy sung túc, hy vọng...
Hoà chung với không khí đó chúng mình có một món quà nhỏ, dành cho tất cả cư dân xứ sở Hola đây:
CHƯƠNG TRÌNH TẾT DÂN GIAN – SẮC XUÂN 2019
Hãy cùng tham gia Sắc Xuân 2019 cùng chúng mình để: 
+ Hoà mình vào không khí náo nhiệt Tết đến Xuân về
+ Cùng trải nghiệm những trò chơi thú vị như đập niêu, bắt vịt, limbo...và nhận những phần quà đặc biệt từ BTC
+ Thưởng thức những món ăn phong phú, đa dạng đến từ các gian hàng đặc sắc của các thần dân vô cùng tài năng xứ sở Hola 
+ Cùng nhau (học) gói bánh chưng - một phong tục không thể thiếu trong ngày Tết Việt
+ Và điều thú vị hơn, đến với Sắc Xuân 2019 các thần dân sẽ có cơ hội “khi đi thì lẻ bóng khi về sẽ có đôi” đó nhé.
Còn đợi chờ gì nữa, các bạn đã sẵn sàng đón Tết trên vương quốc HoLa của chúng mình chưa nào???!
--------------------
THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:
Thời gian: Thứ Sáu - ngày 18/1/2019
Địa điểm: Khuôn viên đại học FPT

#Tếtdângian
#Sắcxuân 
#FPTU
#ICPDP 
#CoreTeamk14', N'Khuôn viên Đại học FPT, Hòa Lạc', 0.0000, 5, 29, CAST(N'2019-01-01T12:57:14.063' AS DateTime), 5, CAST(N'2019-01-01T12:57:20.720' AS DateTime), CAST(N'2019-01-18T08:00:00.000' AS DateTime), CAST(N'2019-01-18T18:00:00.000' AS DateTime), CAST(N'2019-01-18T08:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (2, N'Spring Concert Night - Khúc Ca Xuân', 3, N'/Images/Events/SPRING2019_SpringConcertNight.jpg', 0, 0, N'Closed', N'[SPRING CONCERT] KHÚC CA XUÂN
SPRING CONCERT là một món quà đặc biệt chào đón Xuân Kỷ Hợi 2019 của phòng Hợp tác Quốc tế và Phát triển cá nhân IC-PDP dành tặng cho tất cả các sinh viên của đại học FPT.
Mang một màu sắc âm nhạc mới lạ thông qua sự kết hợp của các nhạc cụ cổ điển, là sự giao thoa giữa các nền văn hoá khác nhau, KHÚC CA XUÂN hứa hẹn sẽ mang đến cho các bạn sinh viên một trải nghiệm vô cùng lý thú và nhiều màu sắc. 
- Thời gian: 19h30 ngày 28/02/2019. 
- Địa điểm: Sảnh Tầng 1 - toà nhà Beta
Hãy cùng chờ đón đêm nhạc có 1-0-2 tại Hola này các bạn nha!', N'[SPRING CONCERT] KHÚC CA XUÂN
SPRING CONCERT là một món quà đặc biệt chào đón Xuân Kỷ Hợi 2019 của phòng Hợp tác Quốc tế và Phát triển cá nhân IC-PDP dành tặng cho tất cả các sinh viên của đại học FPT.
Mang một màu sắc âm nhạc mới lạ thông qua sự kết hợp của các nhạc cụ cổ điển, là sự giao thoa giữa các nền văn hoá khác nhau, KHÚC CA XUÂN hứa hẹn sẽ mang đến cho các bạn sinh viên một trải nghiệm vô cùng lý thú và nhiều màu sắc. 
- Thời gian: 19h30 ngày 28/02/2019. 
- Địa điểm: Sảnh Tầng 1 - toà nhà Beta
Hãy cùng chờ đón đêm nhạc có 1-0-2 tại Hola này các bạn nha!', N'Sảnh tầng 1 - tòa nhà Beta', 1.0000, 5, NULL, CAST(N'2019-02-20T00:00:00.000' AS DateTime), 2, NULL, CAST(N'2019-02-28T19:30:00.000' AS DateTime), CAST(N'2019-02-28T21:30:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (3, N'Học kì tiếng Nhật tại Nhật Bản', 4, N'/Images/Events/FALL2018_JapaneseCourseAtJapan.jpg', 0, 0, N'Draft', N'[BÍ MẬT ĐỂ TRAU DỒI KHẢ NĂNG BẮN TIẾNG NHẬT NHƯ GIÓ CHO SINH VIÊN TRƯỜNG F]
Bạn là sinh viên trường F?
Bạn yêu thích ngôn ngữ và nên văn hóa Nhật Bản ?
Bạn mong muốn được làm việc trong công ty Nhật sau khi tốt nghiệp?
Vậy thì chương trình « Học tiếng Nhật tại Nhật Bản – Japanese in Japan » chính là một cơ hội tuyệt vời dành cho bạn. Chương trình được thiết kế với mục tiêu giúp tăng cường khả năng giao tiếp, trau dồi các kỹ năng nhằm giúp sinh viên có khả năng cạnh tranh tốt trong thị trường nhân lực quốc tế, đặc biệt là thị trường Nhật Bản.
Xuyên suốt chương trình học, bên cạnh các giờ học tiếng Nhật vô cùng lý thú, sinh viên còn được tham gia học môn học bổ trợ cho việc tìm hiểu văn hóa, ngôn ngữ Nhật như : Tiếng Nhật qua văn hóa, Tiếng Nhật qua Anime, Cắm hoa, Trà Đạo…rất hấp dẫn nữa đó nhé.
Đến với chương trình Japanese in Japan lần này, các bạn sẽ có cơ hội được học tập tại một trong 3 trường đại học đó là Đại học Shinshu, Đại học Rissho, và Đại học ngoại ngữ Kyoto.
Thông tin cụ thể về chương trình như sau : 
Thời gian : dự kiến từ tháng 04/2019 đến tháng 08/2019
Địa điểm : ĐH Shinshu Matsumoto Campus, Đại học Rissho - Kumagaya Campus, Đại học ngoại ngữ Kyoto.
Chi phí : Xem cụ thể trong link đăng ký
Đối tượng tham gia: 
Sinh viên chuyên ngành Nhật và các ngành khác, ưu tiên sinh viên có trình độ tiếng Nhật N3 trở lên.
Lưu ý: 
Sinh viên chưa có chứng chỉ N3 sẽ được tham gia vào kỳ thi sát hạch để xếp lớp.
Sinh viên đăng ký trường ĐH Rissho yêu cầu bắt buộc có chứng chỉ N3 trở lên.
Quy định chuyển đổi tương đương:
Kết quả học tập tại Nhật Bản của sinh viên sẽ được xem xét công nhận tương đương & chuyển đổi kết quả sang các môn học tương ứng của chương trình của FPTU. Công nhận tối đa 4 môn trong các môn sau: JPD111, JPD121, JPD131, JPD141, JPD151, JPS212, JPS222, JPD222, JPD223, JPD322, JPD323, JPD 324, JPD 325.
Ngoài ra, sinh viên có nguyện vọng có thể xin đăng kí công nhận và chuyển đổi tương đương cho kì OJT, tương đương 10 tín chỉ, thay cho các môn tiếng Nhật chuyên ngành kể trên.
Thời hạn đăng ký và nộp cọc:
- Link đăng ký: https://goo.gl/forms/dFzbOC2L6g0u1ynf2
- Đăng ký và nộp phí giữ chỗ trước 17:00 ngày 25/11/2018
- Phí giữ chỗ: 20.000.000VNĐ chuyển vào tài khoản trường ĐH FPT theo thông tin:
Số TK : 00006969002
Tên TK: Trường Đại học FPT
Tại NH TMCP Tiên Phong- CN Hoàn Kiếm
Nội dung ghi rõ: ICPDP – Phí tham gia chương trình học tiếng Nhật tại Nhật Bản năm 2018 – Họ và tên, MSSV/CMND

Mọi thông tin chi tiết về chương trình vui lòng liên hệ:
Phòng Hợp tác Quốc tế & Phát triển cá nhân, điện thoại: 024 6680 5910/ 090 172 9173(Gặp C.Trinh) hoặc Email trinhnm3@fe.edu.vn', N'[BÍ MẬT ĐỂ TRAU DỒI KHẢ NĂNG BẮN TIẾNG NHẬT NHƯ GIÓ CHO SINH VIÊN TRƯỜNG F]
Bạn là sinh viên trường F?
Bạn yêu thích ngôn ngữ và nên văn hóa Nhật Bản ?
Bạn mong muốn được làm việc trong công ty Nhật sau khi tốt nghiệp?
Vậy thì chương trình « Học tiếng Nhật tại Nhật Bản – Japanese in Japan » chính là một cơ hội tuyệt vời dành cho bạn. Chương trình được thiết kế với mục tiêu giúp tăng cường khả năng giao tiếp, trau dồi các kỹ năng nhằm giúp sinh viên có khả năng cạnh tranh tốt trong thị trường nhân lực quốc tế, đặc biệt là thị trường Nhật Bản.
Xuyên suốt chương trình học, bên cạnh các giờ học tiếng Nhật vô cùng lý thú, sinh viên còn được tham gia học môn học bổ trợ cho việc tìm hiểu văn hóa, ngôn ngữ Nhật như : Tiếng Nhật qua văn hóa, Tiếng Nhật qua Anime, Cắm hoa, Trà Đạo…rất hấp dẫn nữa đó nhé.
Đến với chương trình Japanese in Japan lần này, các bạn sẽ có cơ hội được học tập tại một trong 3 trường đại học đó là Đại học Shinshu, Đại học Rissho, và Đại học ngoại ngữ Kyoto.
Thông tin cụ thể về chương trình như sau : 
Thời gian : dự kiến từ tháng 04/2019 đến tháng 08/2019
Địa điểm : ĐH Shinshu Matsumoto Campus, Đại học Rissho - Kumagaya Campus, Đại học ngoại ngữ Kyoto.
Chi phí : Xem cụ thể trong link đăng ký
Đối tượng tham gia: 
Sinh viên chuyên ngành Nhật và các ngành khác, ưu tiên sinh viên có trình độ tiếng Nhật N3 trở lên.
Lưu ý: 
Sinh viên chưa có chứng chỉ N3 sẽ được tham gia vào kỳ thi sát hạch để xếp lớp.
Sinh viên đăng ký trường ĐH Rissho yêu cầu bắt buộc có chứng chỉ N3 trở lên.
Quy định chuyển đổi tương đương:
Kết quả học tập tại Nhật Bản của sinh viên sẽ được xem xét công nhận tương đương & chuyển đổi kết quả sang các môn học tương ứng của chương trình của FPTU. Công nhận tối đa 4 môn trong các môn sau: JPD111, JPD121, JPD131, JPD141, JPD151, JPS212, JPS222, JPD222, JPD223, JPD322, JPD323, JPD 324, JPD 325.
Ngoài ra, sinh viên có nguyện vọng có thể xin đăng kí công nhận và chuyển đổi tương đương cho kì OJT, tương đương 10 tín chỉ, thay cho các môn tiếng Nhật chuyên ngành kể trên.
Thời hạn đăng ký và nộp cọc:
- Link đăng ký: <a href="https://goo.gl/forms/dFzbOC2L6g0u1ynf2">https://goo.gl/forms/dFzbOC2L6g0u1ynf2</a>
- Đăng ký và nộp phí giữ chỗ trước 17:00 ngày 25/11/2018
- Phí giữ chỗ: 20.000.000VNĐ chuyển vào tài khoản trường ĐH FPT theo thông tin:
Số TK : 00006969002
Tên TK: Trường Đại học FPT
Tại NH TMCP Tiên Phong- CN Hoàn Kiếm
Nội dung ghi rõ: ICPDP – Phí tham gia chương trình học tiếng Nhật tại Nhật Bản năm 2018 – Họ và tên, MSSV/CMND

Mọi thông tin chi tiết về chương trình vui lòng liên hệ:
Phòng Hợp tác Quốc tế & Phát triển cá nhân, điện thoại: 024 6680 5910/ 090 172 9173(Gặp C.Trinh) hoặc Email trinhnm3@fe.edu.vn', N'Nhật Bản', 100000000.0000, 4, NULL, CAST(N'2019-01-01T03:39:30.577' AS DateTime), 4, CAST(N'2019-02-02T23:16:36.610' AS DateTime), CAST(N'2019-04-23T00:00:00.000' AS DateTime), CAST(N'2019-08-25T00:00:00.000' AS DateTime), CAST(N'2019-04-23T00:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (4, N'Newbie trip - Jump into the word', 5, N'/Images/Events/FALL2018_NewbieTrip.jpg', 0, 0, N'Closed', N'[NEWBIE TRIP - JUMP INTO THE WORLD]
Step 1: Beyond borders"" - Món quà đặc biệt dành cho K14

Sau thời gian “chốt” trường đầy cam go và tốn não, các bạn tân sinh viên trường F đã sẵn sàng cho hành trình sắp tới của mình tại FPTU chưa nào???

Như đã hứa hẹn, vào với FPTU các bạn sẽ bắt đầu chuỗi hành trình NEWBIE TRIP - JUMP INTO THE WORLD, một chuỗi hành trình trải nghiệm học tập, văn hóa tại nước ngoài dành cho các bạn sinh viên K14 nhằm giúp các bạn không chỉ trau dồi kiến thức, trải nghiệm văn hóa mà còn cải thiện các kỹ năng của bản thân. Và để mở màn cho chuỗi hành trình này, IC-PDP xin gửi tới các bạn chương trình “Step 1: Beyond borders”- một món quà đầy bất ngờ nhằm giúp các bạn cóc con xả stress sau thời gian căng não “chốt” trường để chuẩn bị sẵn sàng cho hành trình sắp tới.

Đến với chương trình “Step 1: Beyond border” lần này, các bạn cóc con K14 sẽ có cơ hội được tìm hiểu, trải nghiệm văn hóa và cuộc sống tại Sapa, Lào Cào cũng như tìm hiểu những nét văn hóa đặc sắc của nhân dân nước bạn tại thị trấn Hà Khẩu, Vân Nam, Trung Quốc.

Còn chần chừ gì nữa xách balo lên và bắt đầu hành trình JUMP INTO THE WORLD nào các bạn tân sinh viên trường F ơi.
---------------------------------------------------------------
Thông tin chi tiết chương trình: 
Đối tượng tham dự: Sinh viên Đại học FPT
Số lượng: 35 sinh viên
THỜI GIAN DIỄN RA CHƯƠNG TRÌNH : 25/08/2018 – 27/08/2018
Ưu tiên: tân sinh viên K14.
Link đăng ký: https://goo.gl/forms/ww9TPhog40nHEQfe2
HẠN ĐĂNG KÝ VÀ ĐẶT CỌC: trước 23:00 ngày 21/08/2018
Chi phí: 450.000 VNĐ ( đã bao gồm chi phí ăn, ở, di chuyển và chi phí xuất cảnh sang Trung Quốc)
Nộp trực tiếp tại phòng HB104L hoặc chuyển khoản qua thông tin tài khoản: 
TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_Step1: Beyond borders
THÔNG TIN LIÊN HỆ: Email: anhtp7@fe.edu.vn / Phone: 0978882396 (Phương Anh)', N'[NEWBIE TRIP - JUMP INTO THE WORLD]
Step 1: Beyond borders"" - Món quà đặc biệt dành cho K14

Sau thời gian “chốt” trường đầy cam go và tốn não, các bạn tân sinh viên trường F đã sẵn sàng cho hành trình sắp tới của mình tại FPTU chưa nào???

Như đã hứa hẹn, vào với FPTU các bạn sẽ bắt đầu chuỗi hành trình NEWBIE TRIP - JUMP INTO THE WORLD, một chuỗi hành trình trải nghiệm học tập, văn hóa tại nước ngoài dành cho các bạn sinh viên K14 nhằm giúp các bạn không chỉ trau dồi kiến thức, trải nghiệm văn hóa mà còn cải thiện các kỹ năng của bản thân. Và để mở màn cho chuỗi hành trình này, IC-PDP xin gửi tới các bạn chương trình “Step 1: Beyond borders”- một món quà đầy bất ngờ nhằm giúp các bạn cóc con xả stress sau thời gian căng não “chốt” trường để chuẩn bị sẵn sàng cho hành trình sắp tới.

Đến với chương trình “Step 1: Beyond border” lần này, các bạn cóc con K14 sẽ có cơ hội được tìm hiểu, trải nghiệm văn hóa và cuộc sống tại Sapa, Lào Cào cũng như tìm hiểu những nét văn hóa đặc sắc của nhân dân nước bạn tại thị trấn Hà Khẩu, Vân Nam, Trung Quốc.

Còn chần chừ gì nữa xách balo lên và bắt đầu hành trình JUMP INTO THE WORLD nào các bạn tân sinh viên trường F ơi.
---------------------------------------------------------------
Thông tin chi tiết chương trình: 
Đối tượng tham dự: Sinh viên Đại học FPT
Số lượng: 35 sinh viên
THỜI GIAN DIỄN RA CHƯƠNG TRÌNH : 25/08/2018 – 27/08/2018
Ưu tiên: tân sinh viên K14.
Link đăng ký: <a href="https://goo.gl/forms/ww9TPhog40nHEQfe2">https://goo.gl/forms/ww9TPhog40nHEQfe2</a>
HẠN ĐĂNG KÝ VÀ ĐẶT CỌC: trước 23:00 ngày 21/08/2018
Chi phí: 450.000 VNĐ ( đã bao gồm chi phí ăn, ở, di chuyển và chi phí xuất cảnh sang Trung Quốc)
Nộp trực tiếp tại phòng HB104L hoặc chuyển khoản qua thông tin tài khoản: 
TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_Step1: Beyond borders
THÔNG TIN LIÊN HỆ: Email: anhtp7@fe.edu.vn / Phone: 0978882396 (Phương Anh)', N'Sapa, Lào Cai', 450000.0000, 4, NULL, CAST(N'2018-08-13T00:00:00.000' AS DateTime), 2, NULL, CAST(N'2018-08-25T00:00:00.000' AS DateTime), CAST(N'2018-08-27T00:00:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (5, N'Cuộc đua kì thú', 6, N'/Images/Events/FALL2018_AmazingRace.jpg', 0, 0, N'Closed', N'[LET’S PHỔ đi Tân Gia Ba and Mã Lệ Tây Á Ở FUHL này thì chỉ có mỗi Phổ là chiều các bạn sinh viên nhất thôi
FU-er nhà mình đã có kế hoạch tránh nóng mà lại học được vô vàn điều bổ ích trong thời gian nghỉ hè chưa?
Nếu bạn cảm thấy khó khăn để vượt qua cái nắng mùa hè. 😘Nếu bạn vẫn đang băn khoăn vì hè này đi đâu cũng chán.
Đừng lo đã có IC-PDP giải nhiệt giúp bạn. Phòng hợp tác quốc tế và phát triền cá nhân IC-PDP xin giới thiệu tới các bạn sinh viên chương trình trải nghiệm du hý Amazing Race Summer 2018.
Thời gian: dự kiến từ ngày 30/06 đến ngày 04/07 (5 ngày 4 đêm).
Đặc biệt, đây là chương trình Amazing Race đầu tiên mà bạn đăng ký tham gia không cần phỏng vấn, miễn là bạn được nghỉ hè bạn sẽ được đi. 
Chỉ tiêu: 25 bạn.
Nhận đơn đăng ký và đặt cọc từ 09/05 đến khi đủ số lượng.
Chi phí tham gia:
+ 10 bạn đặt cọc đầu : 7.500.000 vnđ
+ 15 bạn đặt cọc tiếp theo: 8.000.000 vnđ
Phí đặt cọc: 5.000.000 vnđ
(lệ phí trên bao gồm tiền vé máy bay khứ hồi, tiền phòng và hỗ trợ di chuyển)
Hình thức đăng ký:
B1: Đăng ký tại link: https://goo.gl/forms/GXHMj0cVopK80dHh1
B2: Tiến hành đặt cọc theo 2 cách:
- Nộp tiền mặt cho anh Phạm Anh tại phòng HB104L.
- Chuyển khoản cho chị Phương Anh theo số tài khoản sau:
Chuyển khoản tới số TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_AMR Summer 2018
Mọi thắc mắc liên hệ: 
Phạm Anh (0127 810 0588)
Email: anhpt69@gmail.com
Chậm 1 phút quyền lợi mất đi, còn chần chừ gì nữa mà không đăng ký chứ!
Ghi chú: Thời gian nghỉ hè: từ 30/06 đến ngày 08/07', N'[LET’S PHỔ đi Tân Gia Ba and Mã Lệ Tây Á Ở FUHL này thì chỉ có mỗi Phổ là chiều các bạn sinh viên nhất thôi
FU-er nhà mình đã có kế hoạch tránh nóng mà lại học được vô vàn điều bổ ích trong thời gian nghỉ hè chưa?
Nếu bạn cảm thấy khó khăn để vượt qua cái nắng mùa hè. 😘Nếu bạn vẫn đang băn khoăn vì hè này đi đâu cũng chán.
Đừng lo đã có IC-PDP giải nhiệt giúp bạn. Phòng hợp tác quốc tế và phát triền cá nhân IC-PDP xin giới thiệu tới các bạn sinh viên chương trình trải nghiệm du hý Amazing Race Summer 2018.
Thời gian: dự kiến từ ngày 30/06 đến ngày 04/07 (5 ngày 4 đêm).
Đặc biệt, đây là chương trình Amazing Race đầu tiên mà bạn đăng ký tham gia không cần phỏng vấn, miễn là bạn được nghỉ hè bạn sẽ được đi. 
Chỉ tiêu: 25 bạn.
Nhận đơn đăng ký và đặt cọc từ 09/05 đến khi đủ số lượng.
Chi phí tham gia:
+ 10 bạn đặt cọc đầu : 7.500.000 vnđ
+ 15 bạn đặt cọc tiếp theo: 8.000.000 vnđ
Phí đặt cọc: 5.000.000 vnđ
(lệ phí trên bao gồm tiền vé máy bay khứ hồi, tiền phòng và hỗ trợ di chuyển)
Hình thức đăng ký:
B1: Đăng ký tại link: <a href="https://goo.gl/forms/GXHMj0cVopK80dHh1">https://goo.gl/forms/GXHMj0cVopK80dHh1</a>
B2: Tiến hành đặt cọc theo 2 cách:
- Nộp tiền mặt cho anh Phạm Anh tại phòng HB104L.
- Chuyển khoản cho chị Phương Anh theo số tài khoản sau:
Chuyển khoản tới số TK: 01427436001
Chủ TK: Trịnh Phương Anh
Ngân hàng: Tiên Phong Bank
Nội dung CK: Họ tên_MSV_AMR Summer 2018
Mọi thắc mắc liên hệ: 
Phạm Anh (0127 810 0588)
Email: anhpt69@gmail.com
Chậm 1 phút quyền lợi mất đi, còn chần chừ gì nữa mà không đăng ký chứ!
Ghi chú: Thời gian nghỉ hè: từ 30/06 đến ngày 08/07', N'Tân Gia Ba and Mã Lệ Tây Á', 8000000.0000, 5, NULL, CAST(N'2018-04-30T00:00:00.000' AS DateTime), 3, NULL, CAST(N'2018-06-30T00:00:00.000' AS DateTime), CAST(N'2018-07-04T00:00:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (6, N'Đợt tuyển thành viên CLB No Shy - Let your styles shine with us', NULL, N'/Images/Events/SPRING2019_NoShyRecruitement.jpg', 1, 0, N'Closed', N'[NO SHY CLUB TUYỂN THÀNH VIÊN KÌ SPRING 2019 ]
‘’Be yourself because an original is worth more than a copy’’ (Tạm dịch: Hãy là chính bạn bởi vì một bản gốc sẽ có giá trị hơn một bản sao). Đã bao giờ trong cuộc sống này, các bạn cảm thấy chán nản vì không được sống là chính mình? Mình tin chắc rằng có một phần lớn trong số chúng ta đã và đang phải trải qua điều đó . Chúng ta đều không sống là chính mình ở nhiều thời điểm để phù hợp với môi trường, với từng trường hợp, với định kiến, và cả vì một quy chuẩn nào đó của xã hội... Để rồi một lúc nào đó bạn vô tình đánh mất chính mình lúc nào không hay, có lẽ vì đã quá lâu bạn không được sống là chính mình. ‘’Let your styles shine with us’’ chính là thông điệp mà No Shy Club muốn nhắn gửi tới các bạn trong đợt “Member Recruitment 2019” lần này. Bạn biết đó, cuộc sống này được tạo nên bởi những mảng màu sắc riêng biệt và cũng không kém phần nổi bật. Dù đó có là mảng màu trầm hay sáng, nóng hay lạnh, dù bạn có lỡ đánh rơi nó dọc đường hay có khi đơn giản là bạn vẫn chưa dám để con người bạn được tỏa sáng theo cách mà nó vốn có. Vậy thì, chúng mình sẽ giúp bạn tỏa sáng theo cách bạn muốn nhé. Dù bằng cách nào đi nữa, thì chúng mình cũng sẽ tìm ra và đánh thức con người bạn! Để rồi, hãy cháy hết mình với những mảng màu đó và tỏa sáng theo một cách riêng. Người No Shy chúng mình, thường truyền nhau câu nói: ""Be what you wanna be"", bởi chúng mình tin vào một điều rằng mỗi cá nhân đều có những phẩm chất đáng quý riêng biệt, chúng chỉ thực sự được tỏa sáng khi bạn tự tin là chính mình. Đừng tự ti! Đừng lẩn tránh khiếm khuyết của bản thân! Hay đừng ngại ngùng thể hiện chất riêng của mình. Điều quan trọng bạn cần làm bây giờ, đó chính là tham gia ngay vào gia đình No Shy, chúng mình đang đợi các bạn đó !CÁCH THỨC ĐĂNG KÍ 
Thời hạn đăng kí: 21h ngày 8/1 – 23h59’ ngày 12/1 
Thời gian phỏng vấn: 15/1 - 18/1

THÔNG TIN LIÊN HỆ Facebook: www.facebook.com/NoShyClub 
Email: noshy.fpt@gmail.com
Hotline: 0968 279 588 (Ms Linh)', N'<p style="text-align: justify; "><b>[NO SHY CLUB TUYỂN THÀNH VIÊN KÌ SPRING 2019 ]
</b></p><p style="text-align: justify; "><i>‘’Be yourself because an original is worth more than a copy’’</i> (Tạm dịch: Hãy là chính bạn bởi vì một bản gốc sẽ có giá trị hơn một bản sao).</p><p style="text-align: justify;"> Đã bao giờ trong cuộc sống này, các bạn cảm thấy chán nản vì không được sống là chính mình? Mình tin chắc rằng có một phần lớn trong số chúng ta đã và đang phải trải qua điều đó . Chúng ta đều không sống là chính mình ở nhiều thời điểm để phù hợp với môi trường, với từng trường hợp, với định kiến, và cả vì một quy chuẩn nào đó của xã hội... Để rồi một lúc nào đó bạn vô tình đánh mất chính mình lúc nào không hay, có lẽ vì đã quá lâu bạn không được sống là chính mình.</p><p style="text-align: justify;"> ‘’Let your styles shine with us’’ chính là thông điệp mà No Shy Club muốn nhắn gửi tới các bạn trong đợt “Member Recruitment 2019” lần này. Bạn biết đó, cuộc sống này được tạo nên bởi những mảng màu sắc riêng biệt và cũng không kém phần nổi bật. Dù đó có là mảng màu trầm hay sáng, nóng hay lạnh, dù bạn có lỡ đánh rơi nó dọc đường hay có khi đơn giản là bạn vẫn chưa dám để con người bạn được tỏa sáng theo cách mà nó vốn có. Vậy thì, chúng mình sẽ giúp bạn tỏa sáng theo cách bạn muốn nhé. Dù bằng cách nào đi nữa, thì chúng mình cũng sẽ tìm ra và đánh thức con người bạn! Để rồi, hãy cháy hết mình với những mảng màu đó và tỏa sáng theo một cách riêng. </p><p style="text-align: justify;">Người No Shy chúng mình, thường truyền nhau câu nói: ""Be what you wanna be"", bởi chúng mình tin vào một điều rằng mỗi cá nhân đều có những phẩm chất đáng quý riêng biệt, chúng chỉ thực sự được tỏa sáng khi bạn tự tin là chính mình. Đừng tự ti! Đừng lẩn tránh khiếm khuyết của bản thân! Hay đừng ngại ngùng thể hiện chất riêng của mình. Điều quan trọng bạn cần làm bây giờ, đó chính là tham gia ngay vào gia đình No Shy, chúng mình đang đợi các bạn đó !</p><p style="text-align: justify; "><b>CÁCH THỨC ĐĂNG KÍ 
</b></p><p style="text-align: justify; "><i>Thời hạn đăng kí: </i>21h ngày 8/1 – 23h59’ ngày 12/1 </p><p style="text-align: justify; "><i>
Thời gian phỏng vấn: </i>15/1 - 18/1
</p><p style="text-align: justify;"><br></p><p style="text-align: justify;"><b>
THÔNG TIN LIÊN HỆ </b></p><p style="text-align: justify; "><i>Facebook: </i>www.facebook.com/NoShyClub 
Email: noshy.fpt@gmail.com
</p><p style="text-align: justify; "><i>Hotline: </i>0968 279 588 (Ms Linh)</p>', N'Hòa Lạc', 0.0000, 5, 2, CAST(N'2019-01-10T02:19:02.577' AS DateTime), 5, CAST(N'2019-01-11T03:07:58.530' AS DateTime), CAST(N'2019-01-15T00:00:00.000' AS DateTime), CAST(N'2019-01-18T00:00:00.000' AS DateTime), CAST(N'2019-01-15T00:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (7, N'Miss FPTU Hà Nội', 1, N'/Images/Events/FALL2018_MissFPTUHanoi.jpg', 0, 0, N'Closed', N'MISS FPTU HANOI 2018 -“BEAUTY & BEYOND” CHÍNH THỨC TRỞ LẠI 
Cuộc thi Miss FPTU Hà Nội được tổ chức hai năm một lần bởi Hội Sinh viên trường Đại học FPT dưới sự đồng hành và cố vấn của phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP nhằm tìm kiếm, tôn vinh vẻ đẹp, trí tuệ và tài năng của nữ sinh Đại học FPT Hà Nội. 
Miss FPTU Hà Nội 2018 trở lại với chủ đề “Beauty and beyond”, chúng tôi mong muốn trở thành người đồng hành cùng bạn trên hành trình khám phá, trải nghiệm và tỏa sáng vẻ đẹp của chính mình. 
Song hành cùng cuộc thi là sự kiện bên lề Nam sinh đồng hành - nhằm tìm kiếm những gương mặt nam sinh nổi bật và tài năng nhất Đại học FPT. Đây cũng là trải nghiệm tuyệt vời dành cho những chàng trai trên chặng đường khẳng định bản lĩnh của nam nhân trường F.

MISS FPTU HÀ NỘI 2018 -“BEAUTY & BEYOND”

#MissFPTUHanoi
#Beauty_and_beyond
------------------------------
Tiến trình cuộc thi:

Vòng đơn: 11/10/2018 tới 25/10/2018
Sơ khảo Miss: 29,30 và 31/10/2018
Sơ khảo Nam sinh đồng hành: 29,30 và 31/10/2018
Chung kết: 02/12/2018
------------------------------
Đơn đăng ký:

დ Đăng ký Miss: http://bit.ly/missFPTUhanoi2018
დ Đăng ký Nam sinh đồng hành: http://bit.ly/manhuntFPTUhanoi2018

-------------------------------
MISS FPTU HANOI 2018 -“BEAUTY & BEYOND”
Email: missfptu@gmail.com
Page: https://www.facebook.com/missfptu/
Hotline: (+84) 393349954 - Nguyễn Đức Giang', N'MISS FPTU HANOI 2018 -“BEAUTY & BEYOND” CHÍNH THỨC TRỞ LẠI 
Cuộc thi Miss FPTU Hà Nội được tổ chức hai năm một lần bởi Hội Sinh viên trường Đại học FPT dưới sự đồng hành và cố vấn của phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP nhằm tìm kiếm, tôn vinh vẻ đẹp, trí tuệ và tài năng của nữ sinh Đại học FPT Hà Nội. 
Miss FPTU Hà Nội 2018 trở lại với chủ đề “Beauty and beyond”, chúng tôi mong muốn trở thành người đồng hành cùng bạn trên hành trình khám phá, trải nghiệm và tỏa sáng vẻ đẹp của chính mình. 
Song hành cùng cuộc thi là sự kiện bên lề Nam sinh đồng hành - nhằm tìm kiếm những gương mặt nam sinh nổi bật và tài năng nhất Đại học FPT. Đây cũng là trải nghiệm tuyệt vời dành cho những chàng trai trên chặng đường khẳng định bản lĩnh của nam nhân trường F.

MISS FPTU HÀ NỘI 2018 -“BEAUTY & BEYOND”

#MissFPTUHanoi
#Beauty_and_beyond
------------------------------
Tiến trình cuộc thi:

Vòng đơn: 11/10/2018 tới 25/10/2018
Sơ khảo Miss: 29,30 và 31/10/2018
Sơ khảo Nam sinh đồng hành: 29,30 và 31/10/2018
Chung kết: 02/12/2018
------------------------------
Đơn đăng ký:

დ Đăng ký Miss: <a href="http://bit.ly/missFPTUhanoi2018">http://bit.ly/missFPTUhanoi2018</a>
დ Đăng ký Nam sinh đồng hành: <a href="http://bit.ly/manhuntFPTUhanoi2018">http://bit.ly/manhuntFPTUhanoi2018</a>

-------------------------------
MISS FPTU HANOI 2018 -“BEAUTY & BEYOND”
Email: missfptu@gmail.com
Page: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393349954 - Nguyễn Đức Giang', N'Hòa Lạc', 0.0000, 5, NULL, CAST(N'2018-10-10T00:00:00.000' AS DateTime), 1, NULL, CAST(N'2018-10-11T12:00:00.000' AS DateTime), CAST(N'2018-12-02T22:00:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (8, N'Miss FPTU Hà Nội - Vòng Chung kết', 1, N'/Images/Events/FALL2018_MissFPTUHanoi_Chungket.jpg', 1, 1, N'Closed', N'CHUNG KẾT MISS FPTU HANOI 2018 
- Beauty and BeyondMột trong những sự kiện được mong chờ nhất suốt 3 năm vừa qua
Sân khấu ngoài trời tại bãi biển nhân tạo lớn nhất Hà Nội
Được đầu tư kĩ lưỡng về nội dung cũng như quy mô
Đêm hội tụ sắc đẹp và tài năng của 15 nữ sinh xuất sắc nhất đại học FPT
Sự xuất hiện của rất nhiều những vị khách mời đặc biệt và những nhà tài trợ có uy tín

Hãy đến tham dự và trở thành một phần của Đêm Chung kết để biết:

Các thí sinh sẽ biến hoá mình như thế nào?
Ai sẽ là chủ nhân của chiếc vương miện cao quý?
Còn những điều gì vẫn đang chờ đón các bạn...?

Đừng quên click vào link đăng kí và sở hữu cho mình tấm vé tham dự Đêm Chung kết vào ngày 02/12/2018 tại Baaraland với chúng mình để có được lời giải đáp cho những câu hỏi trên nhé!!Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', N'<div style="text-align: justify;"><span style="font-size: 1rem;"><b>CHUNG KẾT MISS FPTU HANOI 2018 
- Beauty and Beyond</b></span></div><div style="text-align: justify;"><span style="font-size: 1rem;"><br></span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Một trong những sự kiện được mong chờ nhất suốt 3 năm vừa qua
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Sân khấu ngoài trời tại bãi biển nhân tạo lớn nhất Hà Nội
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Được đầu tư kĩ lưỡng về nội dung cũng như quy mô
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Đêm hội tụ sắc đẹp và tài năng của 15 nữ sinh xuất sắc nhất đại học FPT</span></div><div style="text-align: justify;"><span style="font-size: 1rem;"><br></span></div><div style="text-align: justify;"><span style="font-size: 1rem;">
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Sự xuất hiện của rất nhiều những vị khách mời đặc biệt và những nhà tài trợ có uy tín

Hãy đến tham dự và trở thành một phần của Đêm Chung kết để biết:

</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Các thí sinh sẽ biến hoá mình như thế nào?
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Ai sẽ là chủ nhân của chiếc vương miện cao quý?
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Còn những điều gì vẫn đang chờ đón các bạn...?
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;"><br></span></div><div style="text-align: justify;"><span style="font-size: 1rem;">
Đừng quên click vào link đăng kí và sở hữu cho mình tấm vé tham dự Đêm Chung kết vào ngày 02/12/2018 tại Baaraland với chúng mình để có được lời giải đáp cho những câu hỏi trên nhé!!</span></div><div style="text-align: justify;"><span style="font-size: 1rem;"><br></span><span style="font-size: 1rem;">Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com</span></div>', N'Hòa Lạc', 50000.0000, 5, 4, CAST(N'2018-11-26T02:22:12.470' AS DateTime), 5, CAST(N'2018-11-26T04:08:04.280' AS DateTime), CAST(N'2018-12-02T19:30:00.000' AS DateTime), CAST(N'2018-12-02T21:30:00.000' AS DateTime), CAST(N'2018-12-02T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (9, N'Cuộc thi F Talent Code 2019', NULL, N'/Images/Events/SPRING2019_FTalentCode2019.jpg', 0, 1, N'Closed', N'[CUỘC THI F TALENT CODE 2019]
Giải F Talent Code mùa thứ hai đã chính thức mở màn
Các cánh tay của những #Coder đâu rồi nhỉ???
Hãy đến và click tham gia ngay cuộc thi F Talent Code 2019 cực #hot ngay thôi.
-----------------------------------------------------------------------------------------------------------
F TALENT CODE 2019 sẽ diễn ra vào ngày 16/03/2019
Vậy Cuộc thi có điều gì hấp dẫn:
(y) Bạn sẽ có cơ hội Đại diện Trường Đại học FPT tham gia cuộc thi ACM/ICPC quốc tế.
(y) Đặc biệt với nhiều Giải thưởng tiền mặt: 
- 1 giải nhất: tiền mặt 3,000,000 VND
- 2 giải nhì: mỗi giải tiền mặt 1,500,000 VND
- 3 giải ba: mỗi giải tiền mặt 1,000,000 VND
- 5 giải khuyến khích: mỗi giải tiền mặt 500,000 VND
- 7 giải cho cá nhân giải quyết được problem đầu tiên: 300,000 VND
Vậy trước đó các bạn cần chuẩn bị những gì, hãy chú ý những điều sau đây nhé:
1. Đối tương dự thi: 
Sinh viên Trường Đại học FPT các cơ sở: Hà Nội, Đà Nẵng, TP. Hồ Chí Minh, Cần Thơ.
2. Hình thức thi: Thi cá nhân
3. Thể lệ cuộc thi:
- Cơ cấu đề thi: Đề thi gồm 20 bài, giải trong 5 giờ.
- Trong suốt quá trình cuộc thi diễn ra, các thi sinh không được phép rời khỏi khu vực dự thi. Các trường hợp còn lại đều phải có ý kiến của BTC.
- Thí sinh có hành động sao chép hoặc gian lận sẽ lập tức bị loại khỏi cuộc thi.
- Quyết định của BTC là quyết định cuối cùng.- Hạn cuối đăng kí danh sách 12h00, 12/03/2019
=============================================================
- Tên chương trình: F TALENT CODE 2019
- Địa điểm tổ chức : Các cơ sở Đại học FPT trên toàn quốc.
- Thời gian: 7h30 ngày 16/03/2019
Đơn vị tổ chức: Phòng Công tác sinh viên -Trường Đại học FPT cơ sở Hà Nội , Bộ môn An ninh an toàn thông tin,Ban Đào tạo – Trường Đại học FPT cơ sở Hà Nội, Phòng Đào tạo, Phòng Công tác sinh viên các cơ sở Hồ Chí Minh, Cần Thơ, Đà Nẵng.
Liên hệ: 02466805915', N'<p>[CUỘC THI F TALENT CODE 2019]
Giải F Talent Code mùa thứ hai đã chính thức mở màn
Các cánh tay của những </p><p>#Coder đâu rồi nhỉ???
Hãy đến và click tham gia ngay cuộc thi F Talent Code 2019 cực #hot ngay thôi.
</p><p>-----------------------------------------------------------------------------------------------------------</p><p>
F TALENT CODE 2019 sẽ diễn ra vào ngày 16/03/2019
</p><p>Vậy Cuộc thi có điều gì hấp dẫn:
</p><p>(<span style="font-size: 1rem;">y) Bạn sẽ có cơ hội Đại diện Trường Đại học FPT tham gia cuộc thi ACM/ICPC quốc tế.</span></p><p><span style="font-size: 1rem;">
(y) Đặc biệt với nhiều Giải thưởng tiền mặt: </span></p><p><span style="font-size: 1rem;">
- 1 giải nhất: tiền mặt 3,000,000 VND
</span></p><p><span style="font-size: 1rem;">- 2 giải nhì: mỗi giải tiền mặt 1,500,000 VND
</span></p><p><span style="font-size: 1rem;">- 3 giải ba: mỗi giải tiền mặt 1,000,000 VND
</span></p><p><span style="font-size: 1rem;">- 5 giải khuyến khích: mỗi giải tiền mặt 500,000 VND
</span></p><p><span style="font-size: 1rem;">- 7 giải cho cá nhân giải quyết được problem đầu tiên: 300,000 VND
</span></p><p><span style="font-size: 1rem;"><br></span></p><p><span style="font-size: 1rem;">Vậy trước đó các bạn cần chuẩn bị những gì, hãy chú ý những điều sau đây nhé:
</span></p><p><span style="font-size: 1rem;">1. Đối tương dự thi: 
Sinh viên Trường Đại học FPT các cơ sở: Hà Nội, Đà Nẵng, TP. Hồ Chí Minh, Cần Thơ.</span></p><p><span style="font-size: 1rem;">
2. Hình thức thi: Thi cá nhân</span></p><p><span style="font-size: 1rem;">
3. Thể lệ cuộc thi:
</span></p><p><span style="font-size: 1rem;">- Cơ cấu đề thi: Đề thi gồm 20 bài, giải trong 5 giờ.
</span></p><p><span style="font-size: 1rem;">- Trong suốt quá trình cuộc thi diễn ra, các thi sinh không được phép rời khỏi khu vực dự thi. Các trường hợp còn lại đều phải có ý kiến của BTC.
</span></p><p><span style="font-size: 1rem;">- Thí sinh có hành động sao chép hoặc gian lận sẽ lập tức bị loại khỏi cuộc thi.</span></p><p><span style="font-size: 1rem;">
- Quyết định của BTC là quyết định cuối cùng.</span></p><p>- Hạn cuối đăng kí danh sách 12h00, 12/03/2019</p><p>
=============================================================</p><p>
- Tên chương trình: F TALENT CODE 2019
</p><p>- Địa điểm tổ chức : Các cơ sở Đại học FPT trên toàn quốc.</p><p>
- Thời gian: 7h30 ngày 16/03/2019
</p><p>Đơn vị tổ chức: Phòng Công tác sinh viên </p><p>-Trường Đại học FPT cơ sở Hà Nội , Bộ môn An ninh an toàn thông tin,<span style="font-size: 1rem;">Ban Đào tạo </span></p><p><span style="font-size: 1rem;">– Trường Đại học FPT cơ sở Hà Nội, Phòng Đào tạo, Phòng Công tác sinh viên các cơ sở Hồ Chí Minh, Cần Thơ, Đà Nẵng.
Liên hệ: 02466805915</span></p>', N'Các cơ sở Đại học FPT trên toàn quốc', 0.0000, 5, 4, CAST(N'2019-03-10T18:23:11.453' AS DateTime), 5, CAST(N'2019-03-11T18:23:45.797' AS DateTime), CAST(N'2019-03-16T08:00:00.000' AS DateTime), CAST(N'2019-03-16T19:00:00.000' AS DateTime), CAST(N'2019-03-12T23:59:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (10, N'Khai Xuân Kì Hội 2019', 10, N'/Images/Events/SPRING2019_ChessTournament.jpg', 1, 0, N'Closed', N'Mọi người ơi ..mọi người ơi..
Đến thi đấu ở giải cờ Khai Xuân Kì Hội của chúng tớ nè
 Đến hẹn lại lên, CLB Cờ và CLB Cờ Vây, Phòng hợp tác và phát triển quốc tế cá nhân (IC-PDP) kết hợp cùng nhóm WIG EZP lớp SE1405 tổ chức một giải đấu vô cùng quan trọng đó chính là Khai Xuân Kì Hội 2019
Khai xuân kì hội là một giải cờ thường niên của chúng tớ, để tận hưởng không khí của mùa xuân trong veo mát mẻ ~~
Là nơi gặp gỡ, hội tụ của các cao nhân cờ lão luyện trong thiên hạ và cả những tay mơ đẹp trai xinh gái Tổng giải thưởng lên đến 3.500.000. Giải cờ lần này chúng mình bao gồm 5 bộ môn: Cờ Tướng, Cờ Vua, Cờ Vây, Cờ Caro và Cờ Cá Ngựa Thời gian, địa điểm
Thời gian thi đấu: Thứ 3, 05/03/2019
+ 18h00-18h15: Đón tiếp kỳ thủ, hướng dẫn kỳ thủ vào vị trí thi đấu
+ 18h15-18h30: Khai mạc Giải
+ 18h30-18h50: Ván 1
+ 19h00-19h20: Ván 2
+ 19h30-19h50: Ván 3
+ 20h00-20h20: Ván 4
+ 20h30-20h50: Ván 5
+ 21h00-21h20: Ván 6
+ 21h30-21h50: Ván 7
+ 22h00: Bế mạc trao giải thưởng
Lệ phí thi đấu: 20.000 VNĐ/ 1 bộ môn
- Mọi thông tin chi tiết xin vui lòng liên hệ: Hotline:Mr.Tuan: 0365153859
Ms.Thao: 0914171669
Hoặc địa chỉ email: khaixuankyhoi2019@gmail.com ', N'<p style="text-align: justify; ">Mọi người ơi ..mọi người ơi..
Đến thi đấu ở giải cờ Khai Xuân Kì Hội của chúng tớ nè
</p><p style="text-align: justify;"> Đến hẹn lại lên, CLB Cờ và CLB Cờ Vây, Phòng hợp tác và phát triển quốc tế cá nhân (IC-PDP) kết hợp cùng nhóm WIG EZP lớp SE1405 tổ chức một giải đấu vô cùng quan trọng đó chính là Khai Xuân Kì Hội 2019
</p><p style="text-align: justify;">Khai xuân kì hội là một giải cờ thường niên của chúng tớ, để tận hưởng không khí của mùa xuân trong veo mát mẻ ~~
</p><p style="text-align: justify;">Là nơi gặp gỡ, hội tụ của các cao nhân cờ lão luyện trong thiên hạ và cả những tay mơ đẹp trai xinh gái </p><p style="text-align: justify;"><b><span style="font-size: 18px;">Tổng giải thưởng lên đến 3.500.000. </span></b></p><p><div style="text-align: justify;"><span style="font-size: 18px;"><b><br></b></span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Giải cờ lần này chúng mình bao gồm 5 bộ môn: Cờ Tướng, Cờ Vua, Cờ Vây, Cờ Caro và Cờ Cá Ngựa&nbsp;</span></div></p><p style="text-align: justify;"><b>Thời gian, địa điểm</b>
</p><p style="text-align: justify;">Thời gian thi đấu: Thứ 3, 05/03/2019
</p><p style="text-align: justify;">+ 18h00-18h15: Đón tiếp kỳ thủ, hướng dẫn kỳ thủ vào vị trí thi đấu
</p><p style="text-align: justify;">+ 18h15-18h30: Khai mạc Giải
</p><p style="text-align: justify;">+ 18h30-18h50: Ván 1
</p><p style="text-align: justify;">+ 19h00-19h20: Ván 2
</p><p style="text-align: justify;">+ 19h30-19h50: Ván 3
</p><p style="text-align: justify;">+ 20h00-20h20: Ván 4
</p><p style="text-align: justify;">+ 20h30-20h50: Ván 5
</p><p style="text-align: justify;">+ 21h00-21h20: Ván 6
</p><p style="text-align: justify;">+ 21h30-21h50: Ván 7
</p><p style="text-align: justify;">+ 22h00: Bế mạc trao giải thưởng
</p><p style="text-align: justify;"><b>Lệ phí thi đấu: </b>20.000 VNĐ/ 1 bộ môn
</p><p style="text-align: justify;">- Mọi thông tin chi tiết xin vui lòng liên hệ: </p><p style="text-align: justify;"><b>Hotline:</b></p><p style="text-align: justify;">Mr.Tuan: 0365153859
</p><p style="text-align: justify;">Ms.Thao: 0914171669
</p><p style="text-align: justify;">Hoặc địa chỉ email: khaixuankyhoi2019@gmail.com&nbsp;</p>', N'Sảnh cờ vua tòa nhà Beta', 0.0000, 5, 34, CAST(N'2019-03-01T04:47:41.470' AS DateTime), 5, CAST(N'2019-03-01T04:49:28.983' AS DateTime), CAST(N'2019-03-05T18:00:00.000' AS DateTime), CAST(N'2019-03-05T22:00:00.000' AS DateTime), CAST(N'2019-03-05T18:00:00.000' AS DateTime), 0, NULL, 150, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (11, N'Chess Basics - Khóa học cờ vua cơ bản', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 1, N'Closed', N'[CHESS BASICS]
Có những lĩnh vực, tuổi tác không thể quyết thể quyết định được năng lực. Một đứa trẻ 10 tuổi có thể chiến thắng một cụ lão 70 tuổi, một người chơi hơn 1,2 tháng có thể thắng một người đã chơi 3,4 năm. Nếu gọi nó là sở thích, thì sở thích ấy không có biên giới, không giới hạn độ tuổi, không phân biệt giới tính, vùng miền,.... Và đặc biệt, không bao giờ là quá muộn để bắt đầu.
Vậy bạn biết đó là gì và sự khác biệt ở đâu không?
Đó là CỜ
Và cách nhanh nhất để chơi giỏi là HỌC CÁCH CHƠI
Chúng ta đều là sinh viên đại học rồi. Ở cấp bậc đại học, chúng ta nên hiểu biết về cờ một cách nghiêm túc. Bạn đã từng nghe đến các khái niệm như khai cuộc, tàn cuộc, trung cuộc,...chưa? Có thể bạn đã đánh thắng nhiều rồi, nhưng để nâng cao khả năng đánh cờ của mình, chắc chắn những kiến thức cơ bản ấy là không thể không có. Chúng mình cũng hiểu, không phải ai cũng có cơ hội được học những điều ấy.
Với mục đích để mọi người có thể hiểu sâu hơn về Cờ, CLB Cờ Đại học FPT (FCC) dưới sự hỗ trợ của Phòng hợp tác và phát triển cá nhân (IC-PDP) tổ chức khóa học Cơ bản về Cờ Vua.
Khóa học hướng đến việc trang bị cho học viên những kiến thức từ cơ bản cho đến phức tạp trong bộ môn cờ vua, từ các bước khai cuộc cho đến tàn cuộc. 
Ngoài ra các bạn còn được trải nghiệm mình ở một vị trí kì thủ trong những trận đấu cờ được sắp xếp vào cuối khóa học.
Vậy nên chúng mình xin gửi những lời này để gửi lời mời đến bạn, bạn của bạn và cả crush của bạn cùng tham gia vào khóa học của chúng mình. Chúng mình rất hân hạnh chào đón các bạn đến và tham dựBạn chỉ cần yêu thích Cờ Vua thôi, việc còn lại cứ để FCC lo Thời gian: 19h30-21h vào các ngày thứ 2, thứ 3, thứ 5 từ ngày 14/11 – 14/2Lệ phí: 50k
Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà) ', N'<p style="text-align: justify;"><b>[CHESS BASICS]
</b></p><p style="text-align: justify;">Có những lĩnh vực, tuổi tác không thể quyết thể quyết định được năng lực. Một đứa trẻ 10 tuổi có thể chiến thắng một cụ lão 70 tuổi, một người chơi hơn 1,2 tháng có thể thắng một người đã chơi 3,4 năm. Nếu gọi nó là sở thích, thì sở thích ấy không có biên giới, không giới hạn độ tuổi, không phân biệt giới tính, vùng miền,.... Và đặc biệt, không bao giờ là quá muộn để bắt đầu.
</p><p style="text-align: justify;">Vậy bạn biết đó là gì và sự khác biệt ở đâu không?
</p><p style="text-align: justify;"><b>Đó là CỜ</b></p><p style="text-align: justify;">
Và cách nhanh nhất để chơi giỏi là HỌC CÁCH CHƠI
</p><p style="text-align: justify;">Chúng ta đều là sinh viên đại học rồi. Ở cấp bậc đại học, chúng ta nên hiểu biết về cờ một cách nghiêm túc. Bạn đã từng nghe đến các khái niệm như khai cuộc, tàn cuộc, trung cuộc,...chưa? Có thể bạn đã đánh thắng nhiều rồi, nhưng để nâng cao khả năng đánh cờ của mình, chắc chắn những kiến thức cơ bản ấy là không thể không có. Chúng mình cũng hiểu, không phải ai cũng có cơ hội được học những điều ấy.
</p><p style="text-align: justify;">Với mục đích để mọi người có thể hiểu sâu hơn về Cờ, CLB Cờ Đại học FPT (FCC) dưới sự hỗ trợ của Phòng hợp tác và phát triển cá nhân (IC-PDP) tổ chức khóa học Cơ bản về Cờ Vua.
</p><p style="text-align: justify;">Khóa học hướng đến việc trang bị cho học viên những kiến thức từ cơ bản cho đến phức tạp trong bộ môn cờ vua, từ các bước khai cuộc cho đến tàn cuộc. 
</p><p style="text-align: justify;">Ngoài ra các bạn còn được trải nghiệm mình ở một vị trí kì thủ trong những trận đấu cờ được sắp xếp vào cuối khóa học.
</p><p style="text-align: justify;"><span style="font-size: 1rem;">Vậy nên chúng mình xin gửi những lời này để gửi lời mời đến bạn, bạn của bạn và cả crush của bạn cùng tham gia vào khóa học của chúng mình. Chúng mình rất hân hạnh chào đón các bạn đến và tham dự</span><br></p><p style="text-align: justify;">Bạn chỉ cần yêu thích Cờ Vua thôi, việc còn lại cứ để FCC lo </p><p style="text-align: justify;"><b>Thời gian: </b>19h30-21h vào các ngày thứ 2, thứ 3, thứ 5 từ ngày 14/11 – 14/2</p><p style="text-align: justify;"><b>Lệ phí: </b>50k
</p><p style="text-align: justify;"><span style="font-size: 1rem;"><b>Thắc mắc xin liên hệ:</b> 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)&nbsp;</span><br></p>', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-05T04:47:26.140' AS DateTime), 5, CAST(N'2019-01-05T04:49:34.610' AS DateTime), CAST(N'2019-01-14T00:00:00.000' AS DateTime), CAST(N'2019-02-21T00:00:00.000' AS DateTime), CAST(N'2019-01-14T00:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (12, N'Miss FPTU Hà Nội - Vòng sơ khảo', 1, N'/Images/Events/FALL2018_MissFPTUHanoi_Sokhao.jpg', 0, 0, N'Closed', N'❤️Thân gửi các cô gái của đại học FPT, những ngày vừa qua BTC chúng tớ đã cố gắng để có thể chuyển những bức thư xinh đẹp này tới các bạn. Nhưng chúng tớ vẫn chưa thể gặp vào trao tận tay tới tất cả mọi người. Vì vậy chúng tớ muốn 1 lần nữa gửi gắm tâm tình của mình lên đây, với hy vọng tất cả các cậu có thể đọc và cảm nhận tấm lòng của BTC bọn tớ.
💃Cô gái nào muốn cầm tận tay chiếc thư mời xinh xắn này làm kỷ niệm hay check in thì hãy cmt hoặc nhắn tin trực tiếp cho page nha, chúng mình sẽ gửi tận tay các cậu.
------------------------------------------------------------------
Gửi các cô gái
Khi đọc tấm thư này cũng có nghĩa là bạn đang ở độ tuổi đẹp nhất của một cuộc đời. Là lúc những nụ hoa e ấp hé nở những cánh đầu tiên, rạng ngời và kiêu hãnh trong nắng sớm. Là khi bạn dám dấn thân mà không sợ vấp ngã, dám ước mơ, dám khát khao, dám yêu và dám chấp nhận đau khổ... Đó là thanh xuân.

Có bao giờ bạn tự hỏi tuổi thanh xuân sẽ kéo dài bao lâu? Một năm, hai năm, hay mười năm nữa? Và dù bạn có để ý hay không thì thanh xuân ấy cũng đang trôi qua vội vã trong từng nhịp thở. Chúng tôi - Miss FPTU ở đây với sứ mệnh giúp bạn lưu lại những khoảnh khắc đẹp nhất của tuổi trẻ. Để bạn không phải hối tiếc khi chợt nhận ra mình đã vô tình làm vuột mất thanh xuân.

Vì thế, hãy một lần bước ra khỏi những lắng lo để đến với Miss FPTU, dù đối với bạn đây có vẻ là quyết định điên rồ nhất, thì chúng tôi cũng nguyện làm điều điên rồ ấy cùng bạn. Hãy tự tin mang vẻ đẹp duy nhất của riêng bạn tỏa sáng rạng ngời dưới ánh hào quang của Miss FPTU. Chúng tôi tin chắc rằng những ký ức tại Miss FPTU sẽ làm cho bạn mỉm cười hạnh phúc khi nhìn lại cuốn album của cuộc đời mình.
Chúng tôi chờ thấy tên bạn trong dòng đăng ký thân thương của Miss FPTU Hà Nội 2018 ❤ 
Nhớ nhé, chúng ta có một cuộc hẹn ở Chương trình Miss FPTU, hỡi cô gái tuổi thanh xuân!

BAN TỔ CHỨC
-------------------------------------------------------------------
დ Đăng ký Miss: http://bit.ly/missFPTUhanoi2018
დ Đăng ký Nam sinh đồng hành: http://bit.ly/manhuntFPTUhanoi2018', N'❤️Thân gửi các cô gái của đại học FPT, những ngày vừa qua BTC chúng tớ đã cố gắng để có thể chuyển những bức thư xinh đẹp này tới các bạn. Nhưng chúng tớ vẫn chưa thể gặp vào trao tận tay tới tất cả mọi người. Vì vậy chúng tớ muốn 1 lần nữa gửi gắm tâm tình của mình lên đây, với hy vọng tất cả các cậu có thể đọc và cảm nhận tấm lòng của BTC bọn tớ.
💃Cô gái nào muốn cầm tận tay chiếc thư mời xinh xắn này làm kỷ niệm hay check in thì hãy cmt hoặc nhắn tin trực tiếp cho page nha, chúng mình sẽ gửi tận tay các cậu.
------------------------------------------------------------------
Gửi các cô gái
Khi đọc tấm thư này cũng có nghĩa là bạn đang ở độ tuổi đẹp nhất của một cuộc đời. Là lúc những nụ hoa e ấp hé nở những cánh đầu tiên, rạng ngời và kiêu hãnh trong nắng sớm. Là khi bạn dám dấn thân mà không sợ vấp ngã, dám ước mơ, dám khát khao, dám yêu và dám chấp nhận đau khổ... Đó là thanh xuân.

Có bao giờ bạn tự hỏi tuổi thanh xuân sẽ kéo dài bao lâu? Một năm, hai năm, hay mười năm nữa? Và dù bạn có để ý hay không thì thanh xuân ấy cũng đang trôi qua vội vã trong từng nhịp thở. Chúng tôi - Miss FPTU ở đây với sứ mệnh giúp bạn lưu lại những khoảnh khắc đẹp nhất của tuổi trẻ. Để bạn không phải hối tiếc khi chợt nhận ra mình đã vô tình làm vuột mất thanh xuân.

Vì thế, hãy một lần bước ra khỏi những lắng lo để đến với Miss FPTU, dù đối với bạn đây có vẻ là quyết định điên rồ nhất, thì chúng tôi cũng nguyện làm điều điên rồ ấy cùng bạn. Hãy tự tin mang vẻ đẹp duy nhất của riêng bạn tỏa sáng rạng ngời dưới ánh hào quang của Miss FPTU. Chúng tôi tin chắc rằng những ký ức tại Miss FPTU sẽ làm cho bạn mỉm cười hạnh phúc khi nhìn lại cuốn album của cuộc đời mình.
Chúng tôi chờ thấy tên bạn trong dòng đăng ký thân thương của Miss FPTU Hà Nội 2018 ❤ 
Nhớ nhé, chúng ta có một cuộc hẹn ở Chương trình Miss FPTU, hỡi cô gái tuổi thanh xuân!

BAN TỔ CHỨC
-------------------------------------------------------------------
დ Đăng ký Miss: <a href="http://bit.ly/missFPTUhanoi2018">http://bit.ly/missFPTUhanoi2018</a>
დ Đăng ký Nam sinh đồng hành: <a href="http://bit.ly/manhuntFPTUhanoi2018">http://bit.ly/manhuntFPTUhanoi2018</a>', N'Hòa Lạc', 0.0000, 5, NULL, CAST(N'2018-10-10T00:00:00.000' AS DateTime), 1, NULL, CAST(N'2018-10-11T00:00:00.000' AS DateTime), CAST(N'2018-10-31T00:00:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (13, N'Miss FPTU Hà Nội - First Meeting', 1, N'/Images/Events/FALL2018_MissFPTUHanoi_FirstMeeting.jpg', 0, 0, N'Closed', N'
Thứ hai vừa rồi chúng mình đã có buổi gặp mặt đầu tiên giữa các miss, nam sinh đồng hành và ban tổ chức. 
Các bạn hãy cùng chúng mình xem lại một số hình ảnh vào tối hôm ấy nhé ^^. 
Hi vọng các bạn sẽ ủng hộ sự thể hiện của 15 miss cũng như nam sinh đồng hành trong những vòng thi sắp tới. 
Cảm ơn các bạn ❤ 

Cuối cùng, BTC xin gửi lời cảm ơn sâu sắc tới các nhà tài trợ đã tài trợ cho sự kiện MISS FPTU
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: https://www.facebook.com/missfptu/
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', N'
Thứ hai vừa rồi chúng mình đã có buổi gặp mặt đầu tiên giữa các miss, nam sinh đồng hành và ban tổ chức. 
Các bạn hãy cùng chúng mình xem lại một số hình ảnh vào tối hôm ấy nhé ^^. 
Hi vọng các bạn sẽ ủng hộ sự thể hiện của 15 miss cũng như nam sinh đồng hành trong những vòng thi sắp tới. 
Cảm ơn các bạn ❤ 

Cuối cùng, BTC xin gửi lời cảm ơn sâu sắc tới các nhà tài trợ đã tài trợ cho sự kiện MISS FPTU
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', N'Hòa Lạc', 0.0000, 5, NULL, CAST(N'2018-10-10T00:00:00.000' AS DateTime), 1, NULL, CAST(N'2018-11-05T19:30:00.000' AS DateTime), CAST(N'2018-11-05T21:00:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (14, N'Miss FPTU Hà Nội - Lotus In City', 1, N'/Images/Events/FALL2018_MissFPTUHanoi_LotusInCity.jpg', 0, 0, N'Closed', N'
CÓ GÌ TRONG MỘT BUỔI LIC? 

LiC - LOTUS in City: Thử thách trải nghiệm thực tế: Đổi kẹp giấy ra vật phẩm có giá trị lớn nhất có thể. 

Mục đích: Khám phá và trau dồi kỹ năng thuyết phục và nói trước đám đông cho những người tham gia. 

Hãy cùng xem các Miss đã thu được "chiến lợi phẩm" gì trong gần 2 giờ đồng hồ tham gia LiC nhé!!!
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: https://www.facebook.com/missfptu/
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', N'
CÓ GÌ TRONG MỘT BUỔI LIC? 

LiC - LOTUS in City: Thử thách trải nghiệm thực tế: Đổi kẹp giấy ra vật phẩm có giá trị lớn nhất có thể. 

Mục đích: Khám phá và trau dồi kỹ năng thuyết phục và nói trước đám đông cho những người tham gia. 

Hãy cùng xem các Miss đã thu được "chiến lợi phẩm" gì trong gần 2 giờ đồng hồ tham gia LiC nhé!!!
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', N'Hòa Lạc', 0.0000, 5, NULL, CAST(N'2018-10-10T00:00:00.000' AS DateTime), 1, NULL, CAST(N'2018-11-11T19:30:00.000' AS DateTime), CAST(N'2018-11-11T21:00:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (15, N'Miss FPTU Hà Nội - Ngày Hiến Chương Nhà Giáo Việt Nam 2018', 1, N'/Images/Events/FALL2018_MissFPTUHanoi_NgayHienChuongNhaGiaoVietNam.jpg', 0, 1, N'Closed', N'
"Nhân ngày nhà giáo Việt Nam, chúng em chúc thầy cô luôn tràn đầy hạnh phúc. Một lời cảm ơn thôi vẫn chưa đủ, cảm ơn thầy cô đã tận tâm cống hiến, dạy dỗ chúng em nên người như ngày hôm nay." 

"Nhân ngày 20/11, chúng em xin kính chúc thầy cô sức khoẻ dồi dào, hạnh phúc trên con đường sự nghiệp của mình." 

Đó là lời nhắn trên những tấm thiệp do chính tay các Miss, Nam sinh đồng hành và toàn thể Ban tổ chức chuẩn bị. Và tất cả đã được các bạn gửi tới các thầy cô giáo vào ngày 16/11 vừa rồi. Một lần nữa, chúng em xin gửi lời cảm ơn chân thành nhất tới toàn thể thầy cô giáo của trường đại học FPT nói riêng và toàn thể những người làm nghề giáo nói chung. Cảm ơn những bài học chuyên ngành, cảm ơn những bài học cuộc sống, cảm ơn những cơ hội và thách thức thầy cô đem đến cho chúng em. Chúng em yêu thầy cô ❤️
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: https://www.facebook.com/missfptu/
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', N'
"Nhân ngày nhà giáo Việt Nam, chúng em chúc thầy cô luôn tràn đầy hạnh phúc. Một lời cảm ơn thôi vẫn chưa đủ, cảm ơn thầy cô đã tận tâm cống hiến, dạy dỗ chúng em nên người như ngày hôm nay." 

"Nhân ngày 20/11, chúng em xin kính chúc thầy cô sức khoẻ dồi dào, hạnh phúc trên con đường sự nghiệp của mình." 

Đó là lời nhắn trên những tấm thiệp do chính tay các Miss, Nam sinh đồng hành và toàn thể Ban tổ chức chuẩn bị. Và tất cả đã được các bạn gửi tới các thầy cô giáo vào ngày 16/11 vừa rồi. Một lần nữa, chúng em xin gửi lời cảm ơn chân thành nhất tới toàn thể thầy cô giáo của trường đại học FPT nói riêng và toàn thể những người làm nghề giáo nói chung. Cảm ơn những bài học chuyên ngành, cảm ơn những bài học cuộc sống, cảm ơn những cơ hội và thách thức thầy cô đem đến cho chúng em. Chúng em yêu thầy cô ❤️
----------------------
Thông tin chi tiết vui lòng liên hệ:
Fanpage: <a href="https://www.facebook.com/missfptu/">https://www.facebook.com/missfptu/</a>
Hotline: (+84) 393 349 954 - Nguyễn Đức Giang
Email: missfptu@gmail.com', N'Hòa Lạc', 0.0000, 5, NULL, CAST(N'2018-10-10T00:00:00.000' AS DateTime), 1, NULL, CAST(N'2018-11-20T07:00:00.000' AS DateTime), CAST(N'2018-11-20T08:30:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (16, N'Estrella', 11, N'/Images/Events/Estrella.jpg', 1, 1, N'Opening', N'[ESTRELLA]

Vậy là sự kiện được mong chờ nhất trong năm học đã bắt đầu được khởi động. Khác với đêm Prom "Lumos" mà chúng ta đã được thưởng thức, buổi tiệc đêm mang phong cách huyền ảo, hiện đại, bí ẩn và hào nhoáng thì năm nay, BTC lựa chọn bộ film "A Star is born" là chủ đề chính cho đêm Prom 2019 năm nay, mang ánh sáng đặc trưng của những vì sao với mong muốn khi đến với sự kiện lần này ai trong chúng ta đều có thể trở thành phiên bản "tỏa sáng" nhất của chính mình.

Khung cảnh lung linh hiện ra và cùng với đó những vì sao rực sáng trong đêm, không những vậy âm nhạc được cất lên là kèm theo những vũ điệu đầy mê hoặc giữa giải ngân hà. Đó cũng chính là nguồn cảm hứng để Prom No Shy 2019 năm nay khoác trên mình cái tên vô cùng độc đáo: “ESTRELLA” - Bản giao hưởng ánh sáng.

“ESTRELLA” - Prom No Shy 2019 với không gian gần gũi hứa hẹn sẽ là một đêm tiệc ngập tràn màu sắc lung linh của ánh sáng, những giai điệu nhẹ nhàng và thêm vào đó là cả những màn slowdance cuốn hút.

Hãy cùng chúng mình đón chờ điều bất ngờ tiếp theo từ “ESTRELLA” nhé!', N'<div style="text-align: justify;"><span style="font-size: 1rem;"><b>[ESTRELLA]
</b></span></div><div style="text-align: justify;"><span style="font-size: 1rem;"><b><br></b>
Vậy là sự kiện được mong chờ nhất trong năm học đã bắt đầu được khởi động. Khác với đêm Prom "Lumos" mà chúng ta đã được thưởng thức, buổi tiệc đêm mang phong cách huyền ảo, hiện đại, bí ẩn và hào nhoáng thì năm nay, BTC lựa chọn bộ film "A Star is born" là chủ đề chính cho đêm Prom 2019 năm nay, mang ánh sáng đặc trưng của những vì sao với mong muốn khi đến với sự kiện lần này ai trong chúng ta đều có thể trở thành phiên bản "tỏa sáng" nhất của chính mình.

Khung cảnh lung linh hiện ra và cùng với đó những vì sao rực sáng trong đêm, không những vậy âm nhạc được cất lên là kèm theo những vũ điệu đầy mê hoặc giữa giải ngân hà. </span></div><div style="text-align: justify;"><span style="font-size: 1rem;">Đó cũng chính là nguồn cảm hứng để Prom No Shy 2019 năm nay khoác trên mình cái tên vô cùng độc đáo: “ESTRELLA” - Bản giao hưởng ánh sáng.
</span></div><div style="text-align: justify;"><span style="font-size: 1rem;">
“ESTRELLA” - Prom No Shy 2019 với không gian gần gũi hứa hẹn sẽ là một đêm tiệc ngập tràn màu sắc lung linh của ánh sáng, những giai điệu nhẹ nhàng và thêm vào đó là cả những màn slowdance cuốn hút.

Hãy cùng chúng mình đón chờ điều bất ngờ tiếp theo từ “ESTRELLA” nhé!</span></div>', N'Guest House of Vietnam National University, Hanoi', 0.0000, 5, 2, CAST(N'2019-02-20T01:51:08.970' AS DateTime), 5, CAST(N'2019-02-20T01:51:37.907' AS DateTime), CAST(N'2020-03-16T19:00:00.000' AS DateTime), CAST(N'2020-03-16T21:30:00.000' AS DateTime), CAST(N'2020-03-16T19:00:00.000' AS DateTime), 0, NULL, NULL, N'$2^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (17, N'Orientation & Coaching', 12, N'/Images/Events/SPRING2019_Orientation&Coaching.jpg', 0, 0, N'Closed', N'[Orientation & Coaching]
Orentation & Coaching là một trong số những sự kiện do Phòng Công tác sinh viên tổ chức dành cho các bạn sinh viên chuẩn bị bước vào giai đoạn thực tập. Với mong muốn giúp cho các bạn sinh viên chuẩn bị những hành trang tốt nhất bước vào kỳ OJT, kỳ này, #SRO gửi đến các bạn rất nhiều thông tin hữu ích và sự tư vấn đặc biệt đến từ các khách mời #Coaching của chúng ta. Bạn sẽ nhận được gì tại buổi Orentation & Coaching này?- Những điều cần biết khi đi OJT trong buổi Orentation.
- Không đơn giản là biết cách viết một CV.
- Không đơn giản là biết cách thuyết phục các nhà tuyển dụng.
- Hơn thế bạn còn được các khách mời chia sẻ nhiều bí kíp sống còn trong những tháng ngày OJT.
- Đặc biệt sẽ có nhiều câu chuyện thú vị là những bài học kinh nghiệm các khách mời sẽ gửi đến các bạn.
Sự cần thiết của kỳ #OJT sẽ được giải đáp trong sự kiện này. ', N'<p style="line-height: 1.5;"><b>[Orientation &amp; Coaching]</b></p><p style="line-height: 1.5;">
Orentation &amp; Coaching là một trong số những sự kiện do Phòng Công tác sinh viên tổ chức dành cho các bạn sinh viên chuẩn bị bước vào giai đoạn thực tập. Với mong muốn giúp cho các bạn sinh viên chuẩn bị những hành trang tốt nhất bước vào kỳ OJT, kỳ này, #SRO gửi đến các bạn rất nhiều thông tin hữu ích và sự tư vấn đặc biệt đến từ các khách mời #Coaching của chúng ta. </p><p style="line-height: 1.5;">Bạn sẽ nhận được gì tại buổi Orentation &amp; Coaching này?</p><p style="line-height: 1.5;">- Những điều cần biết khi đi OJT trong buổi Orentation.
</p><p style="line-height: 1.5;">- Không đơn giản là biết cách viết một CV.
</p><p style="line-height: 1.5;">- Không đơn giản là biết cách thuyết phục các nhà tuyển dụng.
</p><p style="line-height: 1.5;">- Hơn thế bạn còn được các khách mời chia sẻ nhiều bí kíp sống còn trong những tháng ngày OJT.
</p><p style="line-height: 1.5;">- Đặc biệt sẽ có nhiều câu chuyện thú vị là những bài học kinh nghiệm các khách mời sẽ gửi đến các bạn.
Sự cần thiết của kỳ #OJT sẽ được giải đáp trong sự kiện này.&nbsp;</p>', N'Phòng 102R, 104R, 107R, 402R tòa nhà Alpha, Trường Đại học FPT', 0.0000, 5, 4, CAST(N'2019-03-10T02:00:26.813' AS DateTime), 5, CAST(N'2019-03-10T03:07:39.203' AS DateTime), CAST(N'2019-03-13T08:00:00.000' AS DateTime), CAST(N'2019-03-13T14:00:00.000' AS DateTime), CAST(N'2019-03-13T08:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (18, N'VTI Tour Company', 12, N'/Images/Events/SPRING2019_VTITOUR.jpg', 0, 0, N'Closed', N'[COMPANYTOUR VTI]
Bạn còn nhớ:
🔥 VTI - nhà tài trợ đồng hành cùng Coding Inspiration của FPTU?
🔥VTI - doanh nghiệp cùng đến tham gia Japan day vào năm 2018?
🔥Đặc biệt bạn có biết, VTI còn là nhà tài trợ cho cuộc thi F Talent Code 2019.
🔥VTI - công ty outsourcing các sản phẩm công nghệ phần mềm cho thị trường Nhật.
🔥Rất nhiều cựu sinh viên FPTU hiện đang làm việc tại VTI.
Vậy bạn có muốn trải nghiệm văn hóa, môi trường tại VTI thì hãy đăng ký ngay nhé.
Nguồn ảnh: JS Club
==========================================
COMPANY TOUR VTI 
🔥🔥🔥Link đăng ký: https://goo.gl/forms/atXXAY3zLz8F23I13
- Hạn đăng ký: 13/03/2019
- Đối tượng tham dự: Sinh viên ngành CNTT, ngành Ngôn ngữ Nhật (ưu tiên các bạn kỳ 5 trước OJT, kỳ 8-9 trước khi tốt nghiệp).
- Thời gian: 14h00 - 16h30 chiều Thứ 6 ngày 15/03/2019.
- Địa điểm: Công ty VTI, Tòa nhà AC, 78 Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội.
- Phương tiện di chuyển: Có xe đưa đón của BTC tại sảnh Alpha
- Liên hệ: 02466805915.
', N'[COMPANYTOUR VTI]
Bạn còn nhớ:
🔥 VTI - nhà tài trợ đồng hành cùng Coding Inspiration của FPTU?
🔥VTI - doanh nghiệp cùng đến tham gia Japan day vào năm 2018?
🔥Đặc biệt bạn có biết, VTI còn là nhà tài trợ cho cuộc thi F Talent Code 2019.
🔥VTI - công ty outsourcing các sản phẩm công nghệ phần mềm cho thị trường Nhật.
🔥Rất nhiều cựu sinh viên FPTU hiện đang làm việc tại VTI.
Vậy bạn có muốn trải nghiệm văn hóa, môi trường tại VTI thì hãy đăng ký ngay nhé.
Nguồn ảnh: JS Club
==========================================
COMPANY TOUR VTI 
🔥🔥🔥Link đăng ký: <a href="https://goo.gl/forms/atXXAY3zLz8F23I13">https://goo.gl/forms/atXXAY3zLz8F23I13</a>
- Hạn đăng ký: 13/03/2019
- Đối tượng tham dự: Sinh viên ngành CNTT, ngành Ngôn ngữ Nhật (ưu tiên các bạn kỳ 5 trước OJT, kỳ 8-9 trước khi tốt nghiệp).
- Thời gian: 14h00 - 16h30 chiều Thứ 6 ngày 15/03/2019.
- Địa điểm: Công ty VTI, Tòa nhà AC, 78 Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội.
- Phương tiện di chuyển: Có xe đưa đón của BTC tại sảnh Alpha
- Liên hệ: 02466805915.
', N'VTI Company', 0.0000, 5, 4, CAST(N'2019-02-21T12:47:01.657' AS DateTime), 5, CAST(N'2019-02-21T12:51:48.423' AS DateTime), CAST(N'2019-03-15T14:00:00.000' AS DateTime), CAST(N'2019-03-15T16:30:00.000' AS DateTime), CAST(N'2019-03-15T14:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (19, N'Chess Basics - Buổi 1', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 1
Tại buổi 1, sinh viên sẽ lần đầu làm quen với các kí hiệu của cờ vua, các bạn sẽ được học cách chơi cờ vua mà làm quen với những lối đánh đơn giản.
Nội dung của buổi học gồm có:
- Học cách di chuyển quân và một số nước đi đặc biệt như bắt Tốt qua đường, phong cấp, hòa PAT, chiếu MAT
- Học kí hiệu bàn cờ vua và cách viết biên bản bàn cờ
- Học cách chiếu hết

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 1
Tại buổi 1, sinh viên sẽ lần đầu làm quen với các kí hiệu của cờ vua, các bạn sẽ được học cách chơi cờ vua mà làm quen với những lối đánh đơn giản.
Nội dung của buổi học gồm có:
- Học cách di chuyển quân và một số nước đi đặc biệt như bắt Tốt qua đường, phong cấp, hòa PAT, chiếu MAT
- Học kí hiệu bàn cờ vua và cách viết biên bản bàn cờ
- Học cách chiếu hết

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:56:13.017' AS DateTime), 5, CAST(N'2019-01-01T12:56:39.563' AS DateTime), CAST(N'2019-01-14T19:30:00.000' AS DateTime), CAST(N'2019-01-14T21:00:00.000' AS DateTime), CAST(N'2019-01-14T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (20, N'Chess Basics - Buổi 2', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 2
Tại buổi 2, sinh viên sẽ được mở rộng hiểu biết về cờ khi được tiếp xúc với các ván cờ nổi tiếng và có cái nhìn toàn cảnh hơn về một ván cờ vua.
Các bạn sẽ được trả lời các câu hỏi:
- Một ván cờ gồm bao nhiêu giai đoạn
- Làm thế nào để phát huy khả năng của bản thân trong từng gian đoạn của một ván cờ
- Tiếp xúc với các ván cờ của các nhà vô địch thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 2
Tại buổi 2, sinh viên sẽ được mở rộng hiểu biết về cờ khi được tiếp xúc với các ván cờ nổi tiếng và có cái nhìn toàn cảnh hơn về một ván cờ vua.
Các bạn sẽ được trả lời các câu hỏi:
- Một ván cờ gồm bao nhiêu giai đoạn
- Làm thế nào để phát huy khả năng của bản thân trong từng gian đoạn của một ván cờ
- Tiếp xúc với các ván cờ của các nhà vô địch thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:56:03.780' AS DateTime), 5, CAST(N'2019-01-01T12:56:42.517' AS DateTime), CAST(N'2019-01-17T19:30:00.000' AS DateTime), CAST(N'2019-01-17T21:00:00.000' AS DateTime), CAST(N'2019-01-17T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (21, N'Chess Basics - Buổi 3', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 3
Tại buổi 3, sinh viên sẽ được giới thiệu về cách chơi khai cuộc
Các bạn sẽ được học:
- Các nguyên tắc cơ bản khi chơi khai cuộc
- Các sai lầm thường gặp khi đánh khai cuộc
- Giải thích cách chơi khai cuộc của một số ván cờ nổi tiếng của các Đại kiến tướng trên thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 3
Tại buổi 3, sinh viên sẽ được giới thiệu về cách chơi khai cuộc
Các bạn sẽ được học:
- Các nguyên tắc cơ bản khi chơi khai cuộc
- Các sai lầm thường gặp khi đánh khai cuộc
- Giải thích cách chơi khai cuộc của một số ván cờ nổi tiếng của các Đại kiến tướng trên thế giới

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:55:52.437' AS DateTime), 5, CAST(N'2019-01-01T12:56:46.797' AS DateTime), CAST(N'2019-01-21T19:30:00.000' AS DateTime), CAST(N'2019-01-21T21:00:00.000' AS DateTime), CAST(N'2019-01-21T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (22, N'Chess Basics - Buổi 4', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 4
Tại buổi 4, sinh viên sẽ được đấu tập về khai cuộc.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 4
Tại buổi 4, sinh viên sẽ được đấu tập về khai cuộc.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:55:39.110' AS DateTime), 5, CAST(N'2019-01-01T12:56:50.280' AS DateTime), CAST(N'2019-01-28T19:30:00.000' AS DateTime), CAST(N'2019-01-28T21:00:00.000' AS DateTime), CAST(N'2019-01-28T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (23, N'Chess Basics - Buổi 5', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 5
Tại buổi 5, sinh viên sẽ học cách chơi trung cuộc.
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của trung cuộc
- Chiến lược và chiến thuật trong một ván cờ vua là gì
- Học cách chơi trung cuộc như một Đại kiện tướng

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 5
Tại buổi 5, sinh viên sẽ học cách chơi trung cuộc.
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của trung cuộc
- Chiến lược và chiến thuật trong một ván cờ vua là gì
- Học cách chơi trung cuộc như một Đại kiện tướng

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:55:30.297' AS DateTime), 5, CAST(N'2019-01-01T12:56:35.877' AS DateTime), CAST(N'2019-02-11T19:30:00.000' AS DateTime), CAST(N'2019-02-11T21:00:00.000' AS DateTime), CAST(N'2019-02-11T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (24, N'Chess Basics - Buổi 6', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 6
Tại buổi 5, sinh viên sẽ học cách chơi tàn cuộc
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của tàn cuộc
- Cách chiếu hết bằng Hậu, Xe
- Thế nào là đối Vua

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 6
Tại buổi 5, sinh viên sẽ học cách chơi tàn cuộc
Các bạn sẽ biết được:
- Các nguyên tắc cơ bản của tàn cuộc
- Cách chiếu hết bằng Hậu, Xe
- Thế nào là đối Vua

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:55:20.813' AS DateTime), 5, CAST(N'2019-01-01T12:56:31.077' AS DateTime), CAST(N'2019-02-14T19:30:00.000' AS DateTime), CAST(N'2019-02-14T21:00:00.000' AS DateTime), CAST(N'2019-02-14T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (25, N'Chess Basics - Buổi 7', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 7
Buổi 7 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 7
Buổi 7 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:55:05.110' AS DateTime), 5, CAST(N'2019-01-01T12:56:27.267' AS DateTime), CAST(N'2019-02-18T19:30:00.000' AS DateTime), CAST(N'2019-02-18T21:00:00.000' AS DateTime), CAST(N'2019-02-18T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (26, N'Chess Basics - Buổi 8', 2, N'/Images/Events/SPRING2019_ChessBasics.jpg', 1, 0, N'Closed', N'[CHESS BASICS] Buổi 8
Buổi 8 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'[CHESS BASICS] Buổi 8
Buổi 8 chính là buổi học để sinh viên có thể áp dụng các kiến thức đã học thông qua giải đấu MINI nội bộ dành cho các kì thủ đã đăng kí khóa học.

▶ Thắc mắc xin liên hệ: 0837218243 (Mr. Tuấn)
0915961857 ( Hồng Hà)
#FUHLChessClub
#ChessBasicsClass', N'205HB', 50000.0000, 5, 34, CAST(N'2019-01-01T12:57:00.687' AS DateTime), 5, CAST(N'2019-01-01T12:57:24.640' AS DateTime), CAST(N'2019-02-21T19:30:00.000' AS DateTime), CAST(N'2019-02-21T21:00:00.000' AS DateTime), CAST(N'2019-02-21T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (27, N'Tuyển phóng viên Cóc Đọc', 13, N'/Images/Events/SPRING2019_TuyenCocDoc.jpg', 1, 0, N'Closed', N'A đây rồi! Công việc hot nhất trong nămCÓC ĐỌC VÀ NHỮNG NGƯỜI BẠN TUYỂN PHÓNG VIÊN 
Yêu cầu: Biết đọc, biết viết, yêu cái đẹp, biết quay phim chụp ảnh, hài hước, dễ thương, thích nhận nhuận bút
Ưu tiên: Có nhan sắc và sức khỏe là một lợi thế
Cách thức ứng tuyển: Post status 300- 400 chữ để chế độ công khai viết về lí do Cóc Đọc và Những người bạn nên tuyển bạn. Tag Cóc Đọc và Những người bạn và để hashtag: #Coc_Doc_Tuyen_minh_diHạn cuối: Ngày Cá thế giới – 1/4/2019Truy cập website: http://cocdoc.fpt.edu.vn để biết thêm chi tiết.', N'<p>A đây rồi! Công việc hot nhất trong năm</p><p><b>CÓC ĐỌC VÀ NHỮNG NGƯỜI BẠN TUYỂN PHÓNG VIÊN </b></p><p><b>
Yêu cầu:</b> Biết đọc, biết viết, yêu cái đẹp, biết quay phim chụp ảnh, hài hước, dễ thương, thích nhận nhuận bút
</p><p><b>Ưu tiên:</b> Có nhan sắc và sức khỏe là một lợi thế
</p><p><b>Cách thức ứng tuyển: </b>Post status 300- 400 chữ để chế độ công khai viết về lí do Cóc Đọc và Những người bạn nên tuyển bạn. Tag Cóc Đọc và Những người bạn và để hashtag: </p><p>#Coc_Doc_Tuyen_minh_di</p><p>Hạn cuối: Ngày Cá thế giới – 1/4/2019</p><p>Truy cập website: <a href="http://cocdoc.fpt.edu.vn">http://cocdoc.fpt.edu.vn</a> để biết thêm chi tiết.</p>', N'Hòa Lạc', 0.0000, 5, 39, CAST(N'2019-02-26T02:01:10.017' AS DateTime), 5, CAST(N'2019-02-26T03:07:26.047' AS DateTime), CAST(N'2019-03-12T14:00:00.000' AS DateTime), CAST(N'2019-04-01T23:59:59.000' AS DateTime), CAST(N'2019-03-11T23:59:59.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (28, N'Xuân yêu thương', NULL, N'/Images/Events/SPRING2019_XuanYeuThuong.jpg', 1, 0, N'Closed', N'[XUÂN YÊU THƯƠNG 2019]“Gió xuân phơ phất thổi trong cành
Lớp lớp bên đường bóng lá xanh…”
Mùa Xuân, khi đất trời giao thoa, cây cối đua nhau đâm trồi nảy lộc, là mùa tượng trưng cho sức sống mãnh liệt, cho hi vọng và ước mơ Tết Nguyên Đán đã qua đi, thế nhưng không khí vui tươi của mùa xuân ấy vẫn chưa dịu lại, hãy để FTIC một lần nữa thổi lên ngọn gió mùa xuân ấy trong lòng các bạn nhé!Xuân Yêu Thương – một sự kiện được ấp ủ và tổ chức bởi cộng tác viên CLB FTIC, đồng hành cùng sự kiện lần này còn có sự hợp tác của Fire Team lớp WIG BA1402. Là một món quà đầu xuân mà chúng mình muốn gửi gắm đến tất cả mọi người. Đây là lúc chúng ta ngồi lại bên nhau, tâm sự, kể về những điều tuyệt vời ta đã làm trong năm cũ, những chặng đường mà FTIC đã cùng nhau đi qua. Từ đó cùng nỗ lực, sát cánh bên nhau trên chặng đường dài phía trước.Hòa cùng không khí sắc xuân này, các bạn không chỉ được thưởng thức tiếng nhạc cụ hết sức nhịp nhàng và ấm áp mà còn có cơ hội rinh ngay những phần quà vô cùng giá trị về nhà nữa đấy!!!Còn chần chừ gì nữa khi mà chúng tớ với chiếc hẹn chan chứa yêu thương đang chờ đợi các cậu! Cùng chúng tớ lắng nghe lời thì thầm của mùa xuân nhé---------------------------------------------------
Thời gian: 19H30- Thứ năm, ngày 7/3/2019
Địa điểm: Sảnh chính tòa Beta
MỌI CHI TIẾT XIN LIÊN HỆTrưởng BTC: Nguyễn Thành Đạt - 0936704211
Phó BTC: Nguyễn Văn Bách - 0974156308 ', N'<p style="text-align: justify; "><b>[XUÂN YÊU THƯƠNG 2019]</b></p><p style="text-align: justify; "><span style="font-size: 1rem;">“Gió xuân phơ phất thổi trong cành
Lớp lớp bên đường bóng lá xanh…”
</span></p><p style="text-align: justify; "><span style="font-size: 1rem;">Mùa Xuân, khi đất trời giao thoa, cây cối đua nhau đâm trồi nảy lộc, là mùa tượng trưng cho sức sống mãnh liệt, cho hi vọng và ước mơ </span></p><p style="text-align: justify; "><span style="font-size: 1rem;">Tết Nguyên Đán đã qua đi, thế nhưng không khí vui tươi của mùa xuân ấy vẫn chưa dịu lại, hãy để FTIC một lần nữa thổi lên ngọn gió mùa xuân ấy trong lòng các bạn nhé!</span></p><p style="text-align: justify; "><span style="font-size: 1rem;">Xuân Yêu Thương – một sự kiện được ấp ủ và tổ chức bởi cộng tác viên CLB FTIC, đồng hành cùng sự kiện lần này còn có sự hợp tác của Fire Team lớp WIG BA1402. Là một món quà đầu xuân mà chúng mình muốn gửi gắm đến tất cả mọi người. Đây là lúc chúng ta ngồi lại bên nhau, tâm sự, kể về những điều tuyệt vời ta đã làm trong năm cũ, những chặng đường mà FTIC đã cùng nhau đi qua. Từ đó cùng nỗ lực, sát cánh bên nhau trên chặng đường dài phía trước.</span></p><p style="text-align: justify; "><span style="font-size: 1rem;">Hòa cùng không khí sắc xuân này, các bạn không chỉ được thưởng thức tiếng nhạc cụ hết sức nhịp nhàng và ấm áp mà còn có cơ hội rinh ngay những phần quà vô cùng giá trị về nhà nữa đấy!!!Còn chần chừ gì nữa khi mà chúng tớ với chiếc hẹn chan chứa yêu thương đang chờ đợi các cậu! Cùng chúng tớ lắng nghe lời thì thầm của mùa xuân nhé</span></p><p style="text-align: justify; "><span style="font-size: 1rem;">---------------------------------------------------
</span></p><p style="text-align: justify; "><span style="font-size: 1rem;"><u>Thời gian:</u> 19H30- Thứ năm, ngày 7/3/2019
</span></p><p style="text-align: justify; "><span style="font-size: 1rem;"><u>Địa điểm: </u>Sảnh chính tòa Beta
</span></p><p style="text-align: justify; "><span style="font-size: 1rem;"><b>MỌI CHI TIẾT XIN LIÊN HỆ</b></span></p><p style="text-align: justify; "><span style="font-size: 1rem;"><b>Trưởng BTC: </b>Nguyễn Thành Đạt - 0936704211
</span></p><p style="text-align: justify; "><span style="font-size: 1rem;"><b>Phó BTC:</b> Nguyễn Văn Bách - 0974156308&nbsp;</span><br></p>', N'Sảnh chính tòa Beta', 0.0000, 5, 5, CAST(N'2019-02-26T02:02:29.093' AS DateTime), 5, CAST(N'2019-02-26T03:07:32.453' AS DateTime), CAST(N'2019-03-07T19:30:00.000' AS DateTime), CAST(N'2019-03-07T21:00:00.000' AS DateTime), CAST(N'2019-03-07T19:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (29, N'Save Your Soul', 15, N'/Images/Events/SPRING2019_SaveYourSoul.png', 0, 0, N'Closed', N'[SPRING TEAM BUILDING : SAVE YOUR SOUL]
---------------------------------------------------------------------
Sau những cuộc nhậu xuân xuyên đêm cùng với bạn bè .
💁🏼‍♀️Những lần nhảy dù cùng với đồng đội
Những trận combat ác liệt trên đấu trường công lý.
💁🏼‍♀️Hay những kèo bài khiến ví của bạn trở nên ốm yếu.........

⏰⏰DỪNG LẠI TRƯỚC KHI QUÁ MUỘN.
HÃY CỨU LẤY LINH HỒN CỦA BẠN !
----------------------------------------------------------------

✒️Linh hồn của bạn đang bị vấy bẩn bởi những cuộc vui chơi quá đà trong nhưng ngày tết, hãy đến với TEAM BUILDING lần này để gột rửa linh hồn, trở về thánh đường. 
🤼‍♀️Team Building lần này mang đến cho các bạn những trải nghiệm thú vị với những game mang đậm dấu ấn của một game show nổi tiếng trên truyền hình "AMAZING RACE" với nhiều thử thách, mật thư hóc búa đầy khó khăn,...Vậy tại sao bạn lại không trở thành một trong những nhà thám hiểm? Hãy tham gia ngay nàooooooo !
🌋Ngoài ra Team Building còn mang đến đêm lửa trại hoành tráng, ấm áp bên ánh lửa bập bùng kỳ ảo 💃💃, và sẽ có một bí mật bất ngờ cho những nhân vật đặc biệt. Không chỉ vậy, bạn còn có cơ hội:
🍻Ăn uống thỏa thích, với nguồn thức ăn vô tận và địa điểm đầy lí thú bạn có thể ăn mãi không hết .
💣Tăng Exp sau Team building
🤝 Hâm nóng tình cảm anh em.
💑Cải thiện các mối quan hệ vẫn còn mập mờ.
🕵🏼‍♀️Khám phá những điều mới lạ, bổ ích.
👼🏻👼🏻👼🏻👼🏻Và nhất là để linh hồn bạn là thánh đường.

Vậy còn chần chờ gì nữa mà không nhanh tay đặt ngay 1 slot cho chuyến đi lần này nào !!!!!!!!!
--••----••----••----••----••----••----••-
🕰 Thời gian : 8/3/2019 đến 10/3/2019
⛰ địa điểm : đồi 79 Mùa Xuân - Mê Linh - Hà Nội.', N'<p>[SPRING TEAM BUILDING : SAVE YOUR SOUL]
---------------------------------------------------------------------
</p><p>Sau những cuộc nhậu xuân xuyên đêm cùng với bạn bè .
</p><p>💁🏼‍♀️Những lần nhảy dù cùng với đồng đội
Những trận combat ác liệt trên đấu trường công lý.
</p><p>💁🏼‍♀️Hay những kèo bài khiến ví của bạn trở nên ốm yếu.........

</p><p>⏰⏰DỪNG LẠI TRƯỚC KHI QUÁ MUỘN.
HÃY CỨU LẤY LINH HỒN CỦA BẠN !
</p><p>----------------------------------------------------------------
</p><p>
✒️Linh hồn của bạn đang bị vấy bẩn bởi những cuộc vui chơi quá đà trong nhưng ngày tết, hãy đến với TEAM BUILDING lần này để gột rửa linh hồn, trở về thánh đường. </p><p>
🤼‍♀️Team Building lần này mang đến cho các bạn những trải nghiệm thú vị với những game mang đậm dấu ấn của một game show nổi tiếng trên truyền hình "AMAZING RACE" với nhiều thử thách, mật thư hóc búa đầy khó khăn,...Vậy tại sao bạn lại không trở thành một trong những nhà thám hiểm? Hãy tham gia ngay nàooooooo !
</p><p>🌋Ngoài ra Team Building còn mang đến đêm lửa trại hoành tráng, ấm áp bên ánh lửa bập bùng kỳ ảo </p><p>💃💃, và sẽ có một bí mật bất ngờ cho những nhân vật đặc biệt. Không chỉ vậy, bạn còn có cơ hội:
</p><p>🍻Ăn uống thỏa thích, với nguồn thức ăn vô tận và địa điểm đầy lí thú bạn có thể ăn mãi không hết .
</p><p>💣Tăng Exp sau Team building
</p><p>🤝 Hâm nóng tình cảm anh em.
</p><p>💑Cải thiện các mối quan hệ vẫn còn mập mờ.
</p><p>🕵🏼‍♀️Khám phá những điều mới lạ, bổ ích.
</p><p>👼🏻👼🏻👼🏻👼🏻Và nhất là để linh hồn bạn là thánh đường.

Vậy còn chần chờ gì nữa mà không nhanh tay đặt ngay 1 slot cho chuyến đi lần này nào !!!!!!!!!
--••----••----••----••----••----••----••-</p><p>
🕰 Thời gian : 8/3/2019 đến 10/3/2019
</p><p>⛰ địa điểm : đồi 79 Mùa Xuân - Mê Linh - Hà Nội.</p>', N'Đồi 79 Mùa Xuân - Mê Linh - Hà Nội', 300000.0000, 5, 7, CAST(N'2019-02-21T12:18:09.297' AS DateTime), 5, CAST(N'2019-02-21T12:31:18.750' AS DateTime), CAST(N'2019-03-08T08:00:00.000' AS DateTime), CAST(N'2019-03-10T21:00:00.000' AS DateTime), CAST(N'2019-03-07T23:59:00.000' AS DateTime), 0, NULL, NULL, N'$7^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (30, N'JS Rose Day', 16, N'/Images/Events/SPRING2019_JSRoseDay.jpg', 1, 0, N'Closed', N'JS Roses’s Day “Roses” là “Hoa hồng”! Một loại hoa tượng trưng cho tình yêu của cánh mày râu dành tặng cho chị em phụ nữ. Nhất là trong các ngày lễ, loài hoa này được sử dụng rất nhiều cùng với những lời chúc tốt đẹp mọi người muốn dành cho nhau. Và trong ngày “Quốc tế Phụ Nữ” sắp tới, những chàng trai trong đại gia đình JS sẽ dành tặng cho các bạn nữ trong CLB hoa hồng đỏ, nhưng đó không phải là những bó hoa hồng đỏ đơn thuần mà sẽ là một buổi lễ “Hoa Hồng Đỏ” rất đặc biệt mà mọi cô gái khác phải ghen tị vì không có được!!!
Sẽ là một buổi lễ không quá cầu kì, phức tạp nhưng nó chứa đựng những tình cảm, những lời cảm ơn mà các chàng trai trong ngôi nhà JS muốn dành tặng đến các cô gái xinh đẹp của chúng ta. Và trong buổi lễ không thể thiếu được những mini game, những phần quà hấp dẫn, một bữa tiệc nhỏ xinh và đặc biệt là những điều rất rất bất ngờ mà các “soái ca” của CLB chúng ta dành đến các bạn nữ!!
Đây là sự kiện đầu tiên sau khi những CTV chính thức trở thành thành viên của gia đình JS thân thương, vì vậy rất mong mọi người sẽ bỏ chút thời gian quý báu của mình để đến tham gia, đặc biệt là những cô gái xinh đẹp, dễ thương và không kém phần tài giỏi của chúng ta hãy có mặt đầy đủ để không bỏ lỡ một đêm tuyệt vời mà các chàng trai đem lại!!!! Thời gian: 19:00 ngày 7/3/2019
Địa điểm: Phòng 401R, Tòa nhà Alpha ', N'<p style="text-align: justify; "><b><span style="font-size: 24px;">JS Roses’s Day</span></b></p><p style="text-align: justify; "> “Roses” là “Hoa hồng”! Một loại hoa tượng trưng cho tình yêu của cánh mày râu dành tặng cho chị em phụ nữ. Nhất là trong các ngày lễ, loài hoa này được sử dụng rất nhiều cùng với những lời chúc tốt đẹp mọi người muốn dành cho nhau. Và trong ngày “Quốc tế Phụ Nữ” sắp tới, những chàng trai trong đại gia đình JS sẽ dành tặng cho các bạn nữ trong CLB hoa hồng đỏ, nhưng đó không phải là những bó hoa hồng đỏ đơn thuần mà sẽ là một buổi lễ “Hoa Hồng Đỏ” rất đặc biệt mà mọi cô gái khác phải ghen tị vì không có được!!!
</p><p style="text-align: justify; ">Sẽ là một buổi lễ không quá cầu kì, phức tạp nhưng nó chứa đựng những tình cảm, những lời cảm ơn mà các chàng trai trong ngôi nhà JS muốn dành tặng đến các cô gái xinh đẹp của chúng ta. Và trong buổi lễ không thể thiếu được những mini game, những phần quà hấp dẫn, một bữa tiệc nhỏ xinh và đặc biệt là những điều rất rất bất ngờ mà các “soái ca” của CLB chúng ta dành đến các bạn nữ!!
</p><p style="text-align: justify; ">Đây là sự kiện đầu tiên sau khi những CTV chính thức trở thành thành viên của gia đình JS thân thương, vì vậy rất mong mọi người sẽ bỏ chút thời gian quý báu của mình để đến tham gia, đặc biệt là những cô gái xinh đẹp, dễ thương và không kém phần tài giỏi của chúng ta hãy có mặt đầy đủ để không bỏ lỡ một đêm tuyệt vời mà các chàng trai đem lại!!!! </p><p style="text-align: justify; "><b>Thời gian:</b> 19:00 ngày 7/3/2019
</p><p style="text-align: justify; "><b>Địa điểm: </b>Phòng 401R, Tòa nhà Alpha&nbsp;</p>', N'Phòng 401R, Tòa nhà Alpha', 0.0000, 5, 8, CAST(N'2019-02-20T02:08:41.797' AS DateTime), 5, CAST(N'2019-02-20T03:08:09.750' AS DateTime), CAST(N'2019-03-07T19:00:00.000' AS DateTime), CAST(N'2019-03-07T21:30:00.000' AS DateTime), CAST(N'2019-03-07T19:00:00.000' AS DateTime), 0, NULL, NULL, N'$8^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (31, N'Happy Women Day of Chess Club', 17, N'/Images/Events/SPRING2019_FCCWomenDay.jpg', 1, 0, N'Closed', N'Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhenNhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ. Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan. Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em.', N'<p><i>Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhen</i></p><p>Nhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. </p><p>Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé </p><p>Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này </p><p>Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ. Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan. Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em.</p>', N'Phòng 205R, Tòa nhà Alpha', 0.0000, 5, 34, CAST(N'2019-02-26T02:05:06.953' AS DateTime), 5, CAST(N'2019-02-26T03:07:46.360' AS DateTime), CAST(N'2019-03-07T19:00:00.000' AS DateTime), CAST(N'2019-03-07T21:30:00.000' AS DateTime), CAST(N'2019-03-07T19:00:00.000' AS DateTime), 0, NULL, NULL, N'$34^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (32, N'Happy Man Day of Chess Club', 17, N'/Images/Events/SPRING2019_FCCWomenDay.jpg', 1, 0, N'Draft', N'
😍Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhen 😍
Nhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. 🌸🍰
Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé 💞
Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này 🌏
⚡Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ.
⚡Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan 😚 Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em 🎉', N'
😍Trai FU Chess Club không chỉ biết chơi cờ mà còn lãng mạn và tâm lý lắm nhen 😍
Nhân ngày Quốc tế Phụ Nữ, tập thể trai xinh nhà FCC đã gửi tặng hội chị em những bông hoa đỏ thắm cùng những chiếc bánh đáng yêu. 🌸🍰
Cảm ơn các cậu vì những lời yêu thương ý nghĩa nhé 💞
Qua đây, FCC cũng xin gửi lời chúc đến một nửa dân cư của hành tinh này 🌏
⚡Chúc các cô, các bác, các chị em phụ nữ có một ngày 08/03 thật vui và đáng nhớ.
⚡Chúng em cũng xin gửi một ngàn tình yêu đến cô Nguyễn Thị Thu Hoài. Cảm ơn cô vì đã luôn ở cạnh, giúp đỡ và bảo ban chúng em. Chúng em hứa sẽ trưởng thành hơn nữa, phát triển hơn nữa và luôn ngoan 😚 Chúc cô ngày 08/03 luôn trẻ, đẹp, khoẻ, và yêu chúng em 🎉', N'Phòng 205R, Tòa nhà Alpha', 0.0000, 5, NULL, CAST(N'2019-03-01T10:00:00.000' AS DateTime), 2, NULL, CAST(N'2019-03-07T19:00:00.000' AS DateTime), CAST(N'2019-03-07T21:30:00.000' AS DateTime), NULL, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (33, N'Guitar Girls Plaza', 18, N'/Images/Events/SPRING2019_GIRLPLAZA.jpg', 1, 0, N'Closed', N'Bọn anh vào đây đơn giản chỉ vì mê tiếng đàn, nhưng sau cùng thứ bọn anh say nhất lại là “em” [G-Girls Plaza 2019] GUITAR GIRLS PLAZAFROM: Các soái ca FGC cùng tình yêu bất diệt TO: Tất cả các NỮ THÀNH VIÊN xinh đẹp của chúng tôi

Thực sự mà nói, bí ẩn về ngày 8/3 của FGC Girls luôn là một trong những câu hỏi lớn và khó trả lời nhất trong lòng chúng tớ. Nối tiếp những event đi trước, năm nay với sự đổi mới hơn cả về kinh nghiệm lẫn chất lượng, hơn hết là quy mô "khủng" nhất từi trước đến nay, hứa hẹn sẽ đem tới các đóa hồng một đêm thật lãng mạng và hạnh phúc hơn bao giờ hết

Chương trình năm nay được mang tên Guitar Girls Plaza. Tại đây chị em có thể thỏa sức hoàn toàn "free" vui chơi hết mình trong khu trung tâm cực kì hoành tráng này. Có thể kể đến như: Kinh đô thời trang, check in nhận quà, Guitar và những người bạn, phim mà hội trai đẹp diễn, nhảy Kpop và đặc biệt hơn hết là đắm chìm trong tiệc rượu

Ngày Quốc tế Phụ nữ ở xứ sở Guitar đã sẵn sàng, các nàng hãy cùng chúng tớ khai phá nhé

Thời gian: 19h00- Thứ 4, ngày 06/03/2019
Địa điểm: Phòng 404L, Tòa nhà Alpha 
Đối tượng tham gia: Nữ thành viên câu lạc bộ Guitar', N'<p>Bọn anh vào đây đơn giản chỉ vì mê tiếng đàn, nhưng sau cùng thứ bọn anh say nhất lại là “em” </p><p><b><span style="font-size: 24px;">[G-Girls Plaza 2019] GUITAR GIRLS PLAZA</span></b></p><p>FROM: Các soái ca FGC cùng tình yêu bất diệt </p><p>TO: Tất cả các NỮ THÀNH VIÊN xinh đẹp của chúng tôi

</p><p>Thực sự mà nói, bí ẩn về ngày 8/3 của FGC Girls luôn là một trong những câu hỏi lớn và khó trả lời nhất trong lòng chúng tớ. Nối tiếp những event đi trước, năm nay với sự đổi mới hơn cả về kinh nghiệm lẫn chất lượng, hơn hết là quy mô "khủng" nhất từi trước đến nay, hứa hẹn sẽ đem tới các đóa hồng một đêm thật lãng mạng và hạnh phúc hơn bao giờ hết

</p><p>Chương trình năm nay được mang tên Guitar Girls Plaza. Tại đây chị em có thể thỏa sức hoàn toàn "free" vui chơi hết mình trong khu trung tâm cực kì hoành tráng này. Có thể kể đến như: Kinh đô thời trang, check in nhận quà, Guitar và những người bạn, phim mà hội trai đẹp diễn, nhảy Kpop và đặc biệt hơn hết là đắm chìm trong tiệc rượu

</p><p>Ngày Quốc tế Phụ nữ ở xứ sở Guitar đã sẵn sàng, các nàng hãy cùng chúng tớ khai phá nhé

</p><p>Thời gian: 19h00- Thứ 4, ngày 06/03/2019
</p><p>Địa điểm: Phòng 404L, Tòa nhà Alpha 
</p><p>Đối tượng tham gia: Nữ thành viên câu lạc bộ Guitar</p>', N'Phòng 404L, Tòa nhà Alpha', 0.0000, 5, 42, CAST(N'2019-02-25T03:10:39.437' AS DateTime), 5, CAST(N'2019-02-25T04:49:02.423' AS DateTime), CAST(N'2019-03-06T19:00:00.000' AS DateTime), CAST(N'2019-03-06T21:30:00.000' AS DateTime), CAST(N'2019-03-06T19:00:00.000' AS DateTime), 0, N'''MKT'', ''IS1'', ''Khoa học máy tính'', ''JPL''', NULL, N'$42^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (34, N'FGC X-Factor', 19, N'/Images/Events/SPRING2019_XFACTOR.jpg', 1, 0, N'Closed', N'[FGC X-FACTOR SEASON 3 2019] 

 Tiếp nối sự thành công của show Nhạc Kịch đầu tiên được FU Guitar Club tổ chức thì FGC X-FACTOR là sự kiện tiếp theo được câu lạc bộ đầu tư cả về chất lượng sự kiện lẫn kĩ thuật chuyên môn. Không chỉ vậy, FGC X Factor cũng chính là sân chơi lớn tạo cơ hội cho tất cả thành viên cũng như cộng tác viên được sát lại gần nhau hơn. 
Bạn có niềm đam mê với âm nhạc? Bạn có tài năng ca hát, chơi guitar, cajon,..? Bạn có mong muốn được thể hiện bản thân trên sân khấu? Hãy đến với FXF để được hòa mình vào những bản nhạc du dương đầy tâm trạng, được quẩy thật sung cùng những bài hát sôi động mang đầy nhiệt huyết tuổi trẻ. Sau 2 năm vắng bóng sự kiện FGC X FACTOR Season 3 (FXF 2019) đã chính thức quay trở lại. Hứa hẹn đây sẽ là 1 cuộc thi đầy cam go và hấp dẫn. 
Mọi người hãy cùng tham gia và theo dõi những điều đặc biệt mà FXF sẽ mang đến cho chúng ta nhé Thời gian: 19h00 ngày 04 tháng 03 năm 2019
Địa điểm: Sảnh giảng đường Beta – Đại học FPT khu công nghệ cao Hoà Lạc
Thông tin liên hệ: 
- Trưởng BTC: Nguyễn Thị Thùy Linh - 0387050787
- Phó BTC: Nguyễn Việt Hoàng - 0363908833 ', N'<p style="text-align: justify; "><b>[FGC X-FACTOR SEASON 3 2019] 

</b></p><p style="text-align: justify; "> Tiếp nối sự thành công của show Nhạc Kịch đầu tiên được FU Guitar Club tổ chức thì FGC X-FACTOR là sự kiện tiếp theo được câu lạc bộ đầu tư cả về chất lượng sự kiện lẫn kĩ thuật chuyên môn. Không chỉ vậy, FGC X Factor cũng chính là sân chơi lớn tạo cơ hội cho tất cả thành viên cũng như cộng tác viên được sát lại gần nhau hơn. 
</p><p style="text-align: justify; ">Bạn có niềm đam mê với âm nhạc? Bạn có tài năng ca hát, chơi guitar, cajon,..? </p><p style="text-align: justify; ">Bạn có mong muốn được thể hiện bản thân trên sân khấu? </p><p style="text-align: justify; ">Hãy đến với FXF để được hòa mình vào những bản nhạc du dương đầy tâm trạng, được quẩy thật sung cùng những bài hát sôi động mang đầy nhiệt huyết tuổi trẻ. Sau 2 năm vắng bóng sự kiện FGC X FACTOR Season 3 (FXF 2019) đã chính thức quay trở lại. Hứa hẹn đây sẽ là 1 cuộc thi đầy cam go và hấp dẫn. 
</p><p style="text-align: justify; ">Mọi người hãy cùng tham gia và theo dõi những điều đặc biệt mà FXF sẽ mang đến cho chúng ta nhé </p><p style="text-align: justify; "><b>Thời gian:</b> 19h00 ngày 04 tháng 03 năm 2019
</p><p style="text-align: justify; "><b>Địa điểm: </b>Sảnh giảng đường Beta – Đại học FPT khu công nghệ cao Hoà Lạc
</p><p style="text-align: justify; "><b>Thông tin liên hệ: </b></p><p style="text-align: justify; ">
- Trưởng BTC: Nguyễn Thị Thùy Linh - 0387050787
</p><p style="text-align: justify; ">- Phó BTC: Nguyễn Việt Hoàng - 0363908833&nbsp;</p>', N'Sảnh giảng đường Beta', 0.0000, 5, 42, CAST(N'2019-02-26T02:12:40.983' AS DateTime), 5, CAST(N'2019-02-26T03:07:53.453' AS DateTime), CAST(N'2019-03-04T19:00:00.000' AS DateTime), CAST(N'2019-03-04T21:30:00.000' AS DateTime), CAST(N'2019-03-04T19:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (38, N'Khóa học Public Speaking 15 ngày tại Malaysia và Singapore', NULL, N'/Images/Events/448b0575-d19d-4c90-92ed-22e8012fae40.jpg', 1, 0, N'Happening', N'[SPEAK TRIP] Khóa học Public Speaking 15 ngày tại Malaysia và SingaporeSpeak Trip là một format chương trình hoàn toàn mới do IC-PDP thiết kế, nhằm nâng cao khả năng giao tiếp bằng tiếng Anh.Hành trình Speak Trip đầu tiên với chủ đề Public Speaking sẽ tập trung vào trau dồi kỹ năng nói trước đám đông, khả năng vận dụng tiếng Anh trong tư duy và biểu đạt suy nghĩ, quan điểm bằng ngôn từ một cách hiệu quả nhất – đây là một trong những kỹ năng không thể thiếu cho những công dân trong thời đại toàn cầu hóa. Trong thời gian 2 tuần, các bạn sẽ không những được trau dồi kiến thức và kỹ năng về Public Speaking tại Đại học KDU, Malaysia mà còn được thực hành và áp dụng những kiến thức đã học vào cuộc sống hàng ngày qua những hoạt động trải nghiệm văn hóa đặc sắc tại đảo quốc Sư Tử - Singapore. ------------------THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:- Thời gian: 27/04-12/05/2019 ( 15 ngày bao gồm 3 ngày ở Singapore) - Chi phí: 7.850.000 VNĐ ( Đã bao gồm học phí, chỗ ở, phí di chuyển tới Singapore)- Đối tượng: sinh viên Đại học FPTMọi thắc mắc vui lòng liên hệ phòng Hợp tác Quốc tế và Phát triển Cá nhân qua fanpage hoặc SĐT: 024 6680 5910/0903.217.995 (A.Phú)', N'<p><b><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;">[SPEAK TRIP] Khóa học Public Speaking 15 ngày tại Malaysia và Singapore</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"></b><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Speak Trip là một format chương trình hoàn toàn mới do IC-PDP thiết kế, nhằm nâng cao khả năng giao tiếp bằng tiếng Anh.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Hành trình Speak Trip đầu tiên với chủ đề Public Speaking sẽ tập trung vào trau dồi kỹ năng nói trước đám đông, khả năng vận dụng tiếng Anh trong tư duy và biểu đạt suy nghĩ, quan điểm bằng ngôn từ một các</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">h hiệu quả nhất – đây là một trong những kỹ năng không thể thiếu cho những công dân trong thời đại toàn cầu hóa.<span style="font-weight: 400;">&nbsp;</span><br><br>Trong thời gian 2 tuần, các bạn sẽ không những được trau dồi kiến thức và kỹ năng về Public Speaking tại Đại học KDU, Malaysia mà còn được thực hành và áp dụng những kiến thức đã học vào cuộc sống hàng ngày qua những hoạt động trải nghiệm văn hóa đặc sắc tại đảo quốc Sư Tử - Singapore.<span style="font-weight: 400;">&nbsp;</span><br>------------------<br><b>THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:<br></b>- Thời gian: 27/04-12/05/2019 ( 15 ngày bao gồm 3 ngày ở Singapore)<span style="font-weight: 400;">&nbsp;</span><br>- Chi phí: 7.850.000 VNĐ ( Đã bao gồm học phí, chỗ ở, phí di chuyển tới Singapore)<br>- Đối tượng: sinh viên Đại học FPT<br><br>Mọi thắc mắc vui lòng liên hệ phòng Hợp tác Quốc tế và Phát triển Cá nhân qua fanpage hoặc SĐT: 024 6680 5910/0903.217.995 (A.Phú)</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Malaysia, Singapore', 7850000.0000, 5, 4, CAST(N'2019-02-20T01:43:45.093' AS DateTime), 5, CAST(N'2019-02-20T01:48:49.470' AS DateTime), CAST(N'2019-04-27T08:00:00.000' AS DateTime), CAST(N'2019-05-12T21:30:00.000' AS DateTime), CAST(N'2019-04-27T08:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (39, N'[HÀNH TRÌNH THỦ LĨNH] Outsmart Outlast Survival Camp 2019', NULL, N'/Images/Events/506b0790-9765-48fb-9d05-41ef00d2d64e.jpg', 0, 1, N'Closed', N'💥💥[HÀNH TRÌNH THỦ LĨNH] 💥💥 Outsmart Outlast Survival Camp 2019Học kỳ SPRING 2019 này, phòng Hợp tác Quốc tế và Phát triển Cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn BAN CHỦ NHIỆM đến với những trải nghiệm mới, cùng khám phá và mang đến những thử thách mang tính chất sinh tồn để thách thức kỹ năng của mỗi leader. Ngoài ra, đây cũng là một chương trình tập huấn kỹ năng đặc biệt của phòng IC-PDP đến tất cả các bạn BCN cho sự cố gắng, cống hiến và nỗ lực hết mình trong suốt thời gian qua <3Tạm xa Hà Nội náo nhiệt, khoác ba-lô lên và chúng ta sẽ có một chuyến trải nghiệm khó quên nhé các leader <3 <3--------------------------------------------------------------------------⏰Thời gian: Từ ngày 20/04 đến 21/04/2019.📌 Đối tượng: dành riêng cho Ban chủ nhiệm của các CLB, hội nhóm tại trường ĐH FPT.💵 Phí tham gia: 150.000 vnđ/Sinh viên 🧨 Thời hạn đăng ký và đóng tiền: 12h00 ngày 13/04/2019.☎️ Mọi thắc mắc xin vui lòng liên hệ: Anh Nguyễn Bá Khiêm - 0967 002 027', N'<p><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t40="" 1="" 16="" 1f4a5.png");"="">💥</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t40="" 1="" 16="" 1f4a5.png");"="">💥</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">[HÀNH TRÌNH THỦ LĨNH]<span> </span></span><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t40="" 1="" 16="" 1f4a5.png");"="">💥</span></span><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t40="" 1="" 16="" 1f4a5.png");"="">💥</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span> </span>Outsmart Outlast Survival Camp 2019</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Học kỳ SPRING 2019 này, phòng Hợp tác Quốc tế và Phát triển Cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn BAN CHỦ NHIỆM đến với những trải nghiệm mới, cùng khám phá và mang đến những thử thách mang tính chất sinh tồn để thách thức kỹ năng của mỗi leader. Ngoài ra, đây cũng là một chương trình tập huấn kỹ năng đặc biệt của phòn</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">g IC-PDP đến tất cả các bạn BCN cho sự cố gắng, cống hiến và nỗ lực hết mình trong suốt thời gian qua<span> </span><span title="Biểu tượng cảm xúc heart" class="_47e3 _5mfr" style="line-height: 0; vertical-align: middle; margin: 0px 1px; font-family: inherit;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 0px; width: 0px; font-family: inherit;"><3</span></span><br>Tạm xa Hà Nội náo nhiệt, khoác ba-lô lên và chúng ta sẽ có một chuyến trải nghiệm khó quên nhé các leader<span> </span><span title="Biểu tượng cảm xúc heart" class="_47e3 _5mfr" style="line-height: 0; vertical-align: middle; margin: 0px 1px; font-family: inherit;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 0px; width: 0px; font-family: inherit;"><3</span></span><span> </span><span title="Biểu tượng cảm xúc heart" class="_47e3 _5mfr" style="line-height: 0; vertical-align: middle; margin: 0px 1px; font-family: inherit;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 0px; width: 0px; font-family: inherit;"><3</span></span><br><br><span style="font-family: inherit;">--------------------------</span><wbr><span class="word_break" style="display: inline-block; font-family: inherit;"></span><span style="font-family: inherit;">--------------------------</span><wbr><span class="word_break" style="display: inline-block; font-family: inherit;"></span>----------------------<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t34="" 1="" 16="" 23f0.png");"="">⏰</span></span>Thời gian: Từ ngày 20/04 đến 21/04/2019.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" tac="" 1="" 16="" 1f4cc.png");"="">📌</span></span><span> </span>Đối tượng: dành riêng cho Ban chủ nhiệm của các CLB, hội nhóm tại trường ĐH FPT.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" tdf="" 1="" 16="" 1f4b5.png");"="">💵</span></span><span> </span>Phí tham gia: 150.000 vnđ/Sinh viên<span> </span><br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t84="" 1="" 16="" 1f9e8.png");"="">🧨</span></span><span> </span>Thời hạn đăng ký và đóng tiền: 12h00 ngày 13/04/2019.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t22="" 1="" 16="" 260e.png");"="">☎️</span></span><span> </span>Mọi thắc mắc xin vui lòng liên hệ: Anh Nguyễn Bá Khiêm - 0967 002 027</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Sơn Tinh Camp', 150000.0000, 5, 29, CAST(N'2019-04-21T20:10:17.280' AS DateTime), 5, CAST(N'2019-04-21T20:10:24.047' AS DateTime), CAST(N'2019-04-21T07:30:00.000' AS DateTime), CAST(N'2019-04-23T19:30:00.000' AS DateTime), CAST(N'2019-04-20T23:59:59.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (40, N'CHƯƠNG TRÌNH TRẢI NGHIỆM ĐẶC BIỆT KẾT HỢP GIỮA ĐẠI HỌC FPT VÀ ĐẠI HỌC INJE, HÀN QUỐC', NULL, N'/Images/Events/8b187cbb-d19f-4ad5-9892-29228a80d295.jpg', 0, 0, N'Opening', N'CHƯƠNG TRÌNH TRẢI NGHIỆM ĐẶC BIỆT KẾT HỢP GIỮA ĐẠI HỌC FPT VÀ ĐẠI HỌC INJE, HÀN QUỐC.Mùa hè rồi sẽ nóng nhưng chương trình này còn nóng hơn cả mùa hè. STUDY TOUR IN KOREA chương trình được rất nhiều sinh viên chờ đợi đã chính thức ra lò.STUDY TOUR IN KOREA sẽ mang đến cho các bạn: TRẢI NGHIỆM VĂN HOÁ, TÌM HIỂU CON NGƯỜI & CHIÊM NGƯỠNG CẢNH VẬT, ẨM THỰC TẠI ĐẤT NƯỚC HÀN QUỐC!CAMPUS TOUR và chương trình học tập, giao lưu TẠI ĐẠI HỌC INJE.VẬN DỤNG HẾT “CÔNG SUẤT” & SỰ KHÉO LÉO ĐỂ XỬ LÝ TÌNH HUỐNG ĐỜI SỐNG THỰC TẾ KHI TỚI HÀN QUỐC & CÙNG LÚC THI ĐUA VỚI CÁC ĐỘI NHÓM KHÁC! Hành trình kéo dài 7 ngày 6 đêm tại Busan và Seoul, Hàn Quốc lần này sẽ đưa các bạn tới thăm rất nhiều những địa điểm hấp dẫn đó nhé.Đối tượng tham gia: Sinh viên trường Đại học FPTSố lượng tham dự: tối đa 20 sinh viên.Yêu cầu:+ Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các quy định của chuyến đi.+ Bắt buộc sinh viên đăng ký tham gia phải có hộ chiếu. BTC sẽ không nhận đăng ký những bạn chưa có hộ chiếu.Thời gian: 01/07/2019 - 07/ 07/2019 (7 ngày 6 đêm)Phí tham dự: 9.500.000 VNĐ (Đã bao gồm vé máy bay, visa, tiền ở và hỗ trợ ăn uống, đi lại tại điểm)Hạn chót đăng ký và đặt cọc: trước 17h00 thứ Ba ngày 09/04/2019.LỊCH PHỎNG VẤN: sẽ thông báo cụ thể tới những bạn đăng kí qua emailSINH VIÊN ĐÓNG PHÍ ĐẶT CỌC 5.500.000 VNĐ trực tiếp hoặc chuyển khoản qua thông tin tài khoản:Chuyển khoản tới số TK: 01427436001Chủ TK: Trịnh Phương AnhNgân hàng: Tiên Phong BankNội dung CK: Họ tên_MSV_trai nghiem Han QuocLưu ý: Đơn đăng ký chỉ được tính là thành công khi các bạn hoàn thành tiền đặt cọc.THÔNG TIN LIÊN HỆ: Ms. Phương Anh/ Email: anhtp7@fe.edu.vn / Phone: 0978882396', N'<p><div style="text-align: justify;"><b><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">CHƯƠNG TRÌNH TRẢI NGHIỆM ĐẶC BIỆT KẾT HỢP GIỮA ĐẠI HỌC FPT VÀ ĐẠI HỌC INJE, HÀN QUỐC</span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">.</span></b></div><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><div style="text-align: justify;">Mùa hè rồi sẽ nóng nhưng chương trình này còn nóng hơn cả mùa hè. STUDY TOUR IN KOREA chương trình được rất nhiều sinh viên chờ đợi đã chính thức ra lò.</div></span><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><div style="text-align: justify;"><b>STUDY TOUR IN KOREA sẽ mang đến cho các bạn:&nbsp;</b></div></span><span class="text_exposed_show" style="display: inline; orphans: 2; text-align: left; text-indent: 0px; widows: 2; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;"><b>TRẢI NGHIỆM VĂN HOÁ, TÌM HIỂU CON NGƯỜI &amp; CHIÊM NGƯỠNG CẢNH VẬT, ẨM THỰC TẠI ĐẤT NƯỚC HÀN QUỐC!</b></span></div></font><div style="text-align: justify;"><br></div><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;"><b>CAMPUS TOUR</b> và chương trình học tập, giao lưu TẠI ĐẠI HỌC INJE.</span></div></font><div style="text-align: justify;"><b><br></b></div><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><b><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;">VẬN DỤNG HẾT “CÔNG SUẤT” &amp; SỰ KHÉO LÉO ĐỂ XỬ LÝ TÌNH HUỐNG ĐỜI SỐNG THỰC TẾ KHI TỚI HÀN QUỐC &amp; CÙNG LÚC THI ĐUA VỚI CÁC ĐỘI NHÓM KHÁC!</span></font><span style="font-size: 14px;">&nbsp;</span></b></div></font><div style="text-align: justify;"><br></div><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Hành trình kéo dài 7 ngày 6 đêm tại Busan và Seoul, Hàn Quốc lần này sẽ đưa các bạn tới thăm rất nhiều những địa điểm hấp dẫn đó nhé.</span></div></font><div style="text-align: justify;"><br></div><span class="_5mfr" style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px 1px;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;"><u>Đối tượng tham gia: </u>Sinh viên trường Đại học FPT</span></font></div></span><span class="_5mfr" style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px 1px;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;"><u>Số lượng tham dự:</u> tối đa 20 sinh viên.</span></font></div></span><span class="_5mfr" style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px 1px;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;"><u>Yêu cầu:</u></span></font></div></span><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">+ Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các quy định của chuyến đi.</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">+ Bắt buộc sinh viên đăng ký tham gia phải có hộ chiếu. BTC sẽ không nhận đăng ký những bạn chưa có hộ chiếu.</span></div></font><span class="_5mfr" style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px 1px;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;">Thời gian: 01/07/2019 - 07/ 07/2019 (7 ngày 6 đêm)</span></font></div></span><span class="_5mfr" style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px 1px;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;">Phí tham dự: 9.500.000 VNĐ (Đã bao gồm vé máy bay, visa, tiền ở và hỗ trợ ăn uống, đi lại tại điểm)</span></font></div></span><span class="_5mfr" style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px 1px;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;">Hạn chót đăng ký và đặt cọc: trước 17h00 thứ Ba ngày 09/04/2019.</span></font></div></span><span class="_5mfr" style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px 1px;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;"><b>LỊCH PHỎNG VẤN: </b>sẽ thông báo cụ thể tới những bạn đăng kí qua email</span></font></div></span><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;"><b>SINH VIÊN ĐÓNG PHÍ ĐẶT CỌC</b> 5.500.000 VNĐ trực tiếp hoặc chuyển khoản qua thông tin tài khoản:</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Chuyển khoản tới số TK: 01427436001</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Chủ TK: Trịnh Phương Anh</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Ngân hàng: Tiên Phong Bank</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Nội dung CK: Họ tên_MSV_trai nghiem Han Quoc</span></div></font><div style="text-align: justify;"><br></div><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Lưu ý: Đơn đăng ký chỉ được tính là thành công khi các bạn hoàn thành tiền đặt cọc.</span></div><div style="text-align: justify;"><span style="font-size: 14px;">THÔNG TIN LIÊN HỆ: Ms. Phương Anh/ Email: anhtp7@fe.edu.vn / Phone: 0978882396</span></div></font></span></p>', N'Busan, Hàn Quốc', 9500000.0000, 5, 29, CAST(N'2019-02-20T01:47:20.657' AS DateTime), 5, CAST(N'2019-02-20T01:48:37.563' AS DateTime), CAST(N'2019-07-01T07:30:00.000' AS DateTime), CAST(N'2019-07-07T19:30:00.000' AS DateTime), CAST(N'2019-04-09T23:59:59.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (41, N'Asean Cutural trip tại Bangkok, Thái Lan', NULL, N'/Images/Events/820d3f75-35ab-482d-a17d-afa5dc33921e.jpg', 1, 0, N'Opening', N'Dạo này IC-PDP liên tục nhận inbox của các bạn sinh viên đang học Little UK than thở rằng sao bao nhiêu chương trình mà toàn nhằm trúng lịch học. Để xoa dịu nỗi lòng của các bạn, hôm nay IC-PDP quay trở lại với một chương trình rất thú vị nhằm trúng thời gian các bạn được nghỉ tại Little UK đó nhé. THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:- ĐỐI TƯỢNG: Sinh viên Đại học FPT.- SỐ LƯỢNG THAM DỰ: Tối đa 30 suất.- YÊU CẦU: Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các yêu cầu và quy định của chuyến đi.- THỜI GIAN: 07/05/2019 – 11/05/2019- PHÍ THAM DỰ: 3.900.000 VNĐ (Chi phí tham dự bao gồm vé máy bay, đi lại, bảo hiểm du lịch và ở tại Thái Lan, sinh viên tự túc các chi phí chi tiêu cá nhân khác).- LỊCH PHỎNG VẤN: sẽ thông báo cụ thể tới những bạn đăng ký qua email.- SINH VIÊN ĐÓNG PHÍ ĐẶT CỌC 2.500.000VNĐ trực tiếp tại 104L hoặc chuyển khoản qua thông tin tài khoản: Chuyển khoản tới số TK: 01427436001Chủ TK: Trịnh Phương AnhNgân hàng: Tiên Phong BankNội dung CK: Trai nghiem Thai Lan_Ho va Ten_MSSVTHÔNG TIN LIÊN HỆ: MS. Linh SĐT: 0973.153.740 hoặc 024 6680 5910 Để biết Bangkok có gì hot, hãy nhanh tay đăng ký và tham gia cùng IC-PDP cùng chúng mình nhé. ', N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Dạo này IC-PDP liên tục nhận inbox của các bạn sinh viên đang học Little UK than thở rằng sao bao nhiêu chương trình mà toàn nhằm trúng lịch học. Để xoa dịu nỗi lòng của các bạn, hôm nay IC-PDP quay trở lại với một chương trình rất thú vị nhằm trúng thời gian các bạn được nghỉ tại Little UK đó nhé.<span>&nbsp;</span></span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b>THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:</b></span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><b><br></b>- ĐỐI TƯỢNG: Sinh viên Đại học FPT.<br>- SỐ LƯỢNG THAM DỰ: Tối đa 30 suất.<br>- YÊU CẦU: Sức khỏe tốt trong thời gian tham gia chuyến đi, tuân thủ các yêu cầu và quy định của chuyến đi.<br>- THỜI GIAN: 07/05/2019 – 11/05/2019<br>- PHÍ THAM DỰ: 3.900.000 VNĐ (Chi phí tham dự bao gồm vé máy bay, đi lại, bảo hiểm du lịch và ở tại Thái Lan, sinh viên tự túc các chi phí chi tiêu cá nhân khác).<br>- LỊCH PHỎNG VẤN: sẽ thông báo cụ thể tới những bạn đăng ký qua email.<br>- SINH VIÊN ĐÓNG PHÍ ĐẶT CỌC 2.500.000VNĐ trực tiếp tại 104L hoặc chuyển khoản qua thông tin tài khoản:<span style="font-weight: 400;">&nbsp;</span><br>Chuyển khoản tới số TK: 01427436001<br>Chủ TK: Trịnh Phương Anh<br>Ngân hàng: Tiên Phong Bank<br>Nội dung CK: Trai nghiem Thai Lan_Ho va Ten_MSSV<br>THÔNG TIN LIÊN HỆ: MS. Linh SĐT: 0973.153.740 hoặc 024 6680 5910<span style="font-weight: 400;">&nbsp;</span><br><br>Để biết Bangkok có gì hot, hãy nhanh tay đăng ký và tham gia cùng IC-PDP cùng chúng mình nhé.<span style="font-weight: 400;">&nbsp;</span></span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Bangkok, Thailand', 3900000.0000, 5, 29, CAST(N'2019-02-20T01:43:33.140' AS DateTime), 5, CAST(N'2019-02-20T01:48:53.110' AS DateTime), CAST(N'2019-05-07T07:30:00.000' AS DateTime), CAST(N'2019-05-11T19:30:00.000' AS DateTime), CAST(N'2019-05-07T07:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (42, N'Cổ vũ bán kết cuộc đua số', NULL, N'/Images/Events/5f9afcc4-4ca9-4e15-8455-41b52464a883.jpg', 0, 0, N'Closed', N'[BÁN KẾT CUỘC ĐUA SỐ 2019]🚌Thời gian: 18h30-21h30, 11/04/2019Địa điểm: Nhà thi đấu Cầu Giấy, 35 Trần Quý Kiên, Dịch Vọng, Cầu Giấy, Hà Nội.Phương tiện đi lại: Có xe đưa đón của Ban tổ chức hoàn toàn miễn phí tại sảnh Penrose tòa nha Alpha, 11/04/2019.Địa điểm nhận vé mời tham dự Cổ vũ Bán kết Cuộc đua số: Quầy 8, 102L tòa nhà Alpha trong giờ hành chính (8h30-12h, 13h30-17h00 các ngày trong tuần từ 01/04/2019 - 12h, 09/04/2019)Liên hệ: 02466805915', N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b>[BÁN KẾT CUỘC ĐUA SỐ 2019]</b></span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_5mfr" style="margin: 0px 1px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t79="" 1="" 16="" 1f68c.png");"="">🚌</span></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"></span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b>Thời gian: </b></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">18h30-21h30, 11/04/2019</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b>Địa điểm: </b></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Nhà thi đấu Cầu Giấy, 35 Trần Quý Kiên, Dịch Vọng, Cầu Giấy, Hà Nội.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b>Phương tiện đi lại: </b></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Có xe đưa đón của Ban tổ chức hoàn toàn miễn phí tại sảnh Penrose tòa nha Alpha, 11/04/2019.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Địa điểm nhận vé mời tham dự Cổ vũ Bán kết Cuộc đua số: Quầy 8, 102L tòa nhà Alpha trong giờ hành chính (8h30-12h, 13h30-17h00 các ngày trong tuần từ 01/04/2019 - 12h, 09/04/2019)</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b>Liên hệ: </b></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">02466805915</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Nhà thi đấu Cầu Giấy, 35 Trần Quý Kiên, Dịch Vọng, Cầu Giấy, Hà Nội', 0.0000, 5, 4, CAST(N'2019-02-20T01:59:16.937' AS DateTime), 5, CAST(N'2019-02-20T03:07:19.593' AS DateTime), CAST(N'2019-04-11T18:30:00.000' AS DateTime), CAST(N'2019-04-11T21:30:00.000' AS DateTime), CAST(N'2019-04-10T23:59:59.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (43, N'Talk show impossible is nothing - finnish ecosystem & innovation', NULL, N'/Images/Events/7929b4ce-281c-42dd-a83e-923a366735f8.jpg', 1, 0, N'Closed', N'Bạn có muốn gặp gỡ người đã sáng tạo ra game Angry Bird?Bạn muốn khởi nghiệp nhưng chưa biết bắt đầu từ đâu?Bạn muốn nghe các chuyên gia dẫn dắt và bổ sung thêm kinh nghiệm cho mình?Hay đơn giản bạn muốn có môi trường được giao tiếp và phản biện tiếng anh?Hãy đến với:TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM & INNOVATIONTalkshow với sự phối hợp của Phòng Công tác sinh viên cùng Văn phòng đại diện tư vấn Phần Lan (WCF) tổ chức với diễn giả Peter Vesterbacka – Nhà phát triển game Angry Bird.Tại #Talkshow bạn sẽ có cơ hội:- Tìm hiểu về SLUSH, sự kiện công nghệ lớn nhất hành tinh.- Làm thế nào để Angry Birds trở thành thương hiệu toàn cầu.- Cùng trò chuyện với nhà phát triển game Angry Bird: Peter Vesterbacka về chủ đề khởi nghiệp.Còn ngại ngần gì mà không đăng ký ngay 1 chiếc ghế xinh xắn trong hội trường vào ngày hôm đó. ;)- Tên chương trình: TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM & INNOVATION- Diễn giả: Peter Vesterbacka – Nhà phát triển game Angry Bird.- MC: Lucy Hoàng- Thời gian: 13:00 – 14:30 ngày thứ Ba, 09/4/2019- Địa điểm: Phòng 102R – 104R nhà Alpha, Đại học FPT.- Liên hệ: 02466805915', N'<p><div style="text-align: justify;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px;">Bạn có muốn gặp gỡ người đã sáng tạo ra game Angry Bird?</span></div><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><div style="text-align: justify;">Bạn muốn khởi nghiệp nhưng chưa biết bắt đầu từ đâu?</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><div style="text-align: justify;">Bạn muốn nghe các chuyên gia dẫn dắt và bổ sung thêm kinh nghiệm cho mình?</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><div style="text-align: justify;">Hay đơn giản bạn muốn có môi trường được giao tiếp và phản biện tiếng anh?</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><div style="text-align: justify;">Hãy đến với:</div></span><span class="text_exposed_show" style="display: inline; orphans: 2; text-align: left; text-indent: 0px; widows: 2; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><div style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;"><b><span style="font-size: 18px;">TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM &amp; INNOVATION</span></b></div><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">Talkshow với sự phối hợp của Phòng Công tác sinh viên cùng Văn phòng đại diện tư vấn Phần Lan (WCF) tổ chức với diễn giả Peter Vesterbacka – Nhà phát triển game Angry Bird.</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">Tại&nbsp;<a class="_58cn" href="https://www.facebook.com/hashtag/talkshow?epa=HASHTAG" data-ft="{" type":104,"tn":"*n"}"="" style="font-family: inherit; font-size: 14px; color: rgb(54, 88, 153); cursor: pointer;"><span style="font-size: 18px;">#Talkshow</span></a>&nbsp;bạn sẽ có cơ hội:</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Tìm hiểu về SLUSH, sự kiện công nghệ lớn nhất hành tinh.</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Làm thế nào để Angry Birds trở thành thương hiệu toàn cầu.</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Cùng trò chuyện với nhà phát triển game Angry Bird: Peter Vesterbacka về chủ đề khởi nghiệp.</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">Còn ngại ngần gì mà không đăng ký ngay 1 chiếc ghế xinh xắn trong hội trường vào ngày hôm đó.&nbsp;<span title="Biểu tượng cảm xúc wink" class="_47e3 _5mfr" style="font-family: inherit; font-size: 14px; line-height: 0; vertical-align: middle; margin: 0px 1px;"><img width="16" height="16" class="img" role="presentation" style="border: 0px; vertical-align: -3px;" alt="" src="https://static.xx.fbcdn.net/images/emoji.php/v9/t57/1/16/1f609.png"><span class="_7oe" aria-hidden="true" style="display: inline; font-size: 18px; width: 0px; font-family: inherit;">;)</span></span></div></span><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Tên chương trình: TALK SHOW IMPOSSIBLE IS NOTHING - FINNISH ECOSYSTEM &amp; INNOVATION</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Diễn giả: Peter Vesterbacka – Nhà phát triển game Angry Bird.</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- MC: Lucy Hoàng</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Thời gian: 13:00 – 14:30 ngày thứ Ba, 09/4/2019</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Địa điểm: Phòng 102R – 104R nhà Alpha, Đại học FPT.</div></span><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;">- Liên hệ: 02466805915</div></span></span></p>', N'Phòng 102R – 104R nhà Alpha, Đại học FPT', 0.0000, 5, 4, CAST(N'2019-02-20T04:48:44.767' AS DateTime), 5, CAST(N'2019-02-20T04:49:23.093' AS DateTime), CAST(N'2019-04-09T13:00:00.000' AS DateTime), CAST(N'2019-04-09T14:30:00.000' AS DateTime), CAST(N'2019-04-08T23:59:59.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (44, N'The dialogue: "Transforming industry to Blockchain: Utop & Akachain story " ', NULL, N'/Images/Events/9e86086a-45a7-42a7-b937-50ff2b831ba5.jpg', 0, 0, N'Closed', N'Sinh viên ĐH FPT có cơ hội đối thoại về công nghệ BlockchainChủ Nhật, ngày 24 tháng 03 năm 2019Ngày 27/3 những bạn trẻ yêu công nghệ Đại học FPT có cơ hội đối thoại cùng diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain xoay quanh chủ đề “Transforming industry to Blockchain: Utop & Akachain story “.Trong làn sóng cách mạng công nghiệp 4.0, Blockchain được xem là một công nghệ “chìa khóa” cho chuyển đổi số và xây dựng nền tảng công nghệ thông tin tương lai.Với khả năng chia sẻ thông tin dữ liệu minh bạch theo thời gian thực, tiết kiệm không gian lưu trữ và bảo mật cao, công nghệ blockchain (chuỗi khối) là một trong những xu hướng công nghệ đột phá, có khả năng ứng dụng rộng rãi ở nhiều ngành nghề, lĩnh vực.Với mong muốn mang tới cho sinh viên những kiên thức bổ ích về công nghệ tiên tiến này, phòng Công tác sinh viên tổ chức sự kiện “The dialogue: “Transforming industry to Blockchain: Utop & Akachain story’ vào ngày 27/3 tại phòng 102R – 104R Alpha. Diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain sẽ giúp các bạn có cái nhìn sâu sắc hơn về làn sóng công nghệ này.Anh Trần Hoàng Giang đã có 10 năm kinh nghiệm trong lĩnh vực lập trình, robot và các thiết bị điều khiển tự động hoá; 6 năm kinh nghiệm trong việc quản lý dự án; 2 năm kinh nghiệm làm quản lý trong bộ phận liên quan đến sản phẩm về Blockchain. Ngoài ra, anh Trần Hoàng Giang còn tham gia vào rất nhiều hoạt động phát triển cộng đồng khác như: Dự án Akachain, Blockchain Empire- Cộng đồng kết nối các kĩ sư phát triển và những người muốn tìm hiểu về công nghệ blockchain, …Đặc biệt, anh Giang chính là cựu sinh viên Đại học FPT và ra nhập FPT Software, đảm nhiệm nhiều vị trí quan trọng tại FPT Software như lập trình viên, quản trị dự án, kiến trúc sư giải pháp, quản trị chương trình Blockchain của FPT Nhật Bản. Trong năm 2019 vừa qua, anh Trần Hoàng Giang vinh dự nằm trong danh sách FPT Under 35 và hiện anh là thành viên tích cực của Ban điều hành Mạng lưới khởi nghiệp Blockchain Việt Nam.Với kinh nghiệm phong phú và đa dạng trong cả lĩnh vực Blockchain lẫn quản lý dự án, buổi đối thoại hứa hẹn sẽ mang tới cho các bạn nhiều câu chuyện thú vị về chuyển đổi số cũng như về cơ hội nghề nghiệp trong ngành này dành cho các bạn sinh viên Đại học FPT.', N'<header class="entry-header" style="box-sizing: inherit; display: block; color: rgb(64, 64, 64); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif; font-size: 14.4px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><h1 class="entry-title" style="box-sizing: inherit; font-size: 2em; margin: 0px; clear: both; line-height: 1.2;">Sinh viên ĐH FPT có cơ hội đối thoại về công nghệ Blockchain</h1><span class="posted-on" style="box-sizing: inherit;"><time class="entry-date published" style="box-sizing: inherit; color: rgb(136, 136, 136); line-height: 1; display: block; margin-top: 1em; font-style: italic;" datetime="2019-03-24T08:53:26+00:00">Chủ Nhật, ngày 24 tháng 03 năm 2019</time></span></header><div class="entry-content" style="box-sizing: inherit; margin: 1.5em 0px 0px; color: rgb(64, 64, 64); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif; font-size: 14.4px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="box-sizing: inherit; margin: 0px 0px 1.5em;"><strong style="box-sizing: inherit; font-weight: bold;">Ngày 27/3 những bạn trẻ yêu công nghệ Đại học FPT có cơ hội đối thoại cùng diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain xoay quanh chủ đề “Transforming industry to Blockchain: Utop & Akachain story “.</strong></p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;"><img width="550" height="733" class="aligncenter wp-image-19952" style="box-sizing: inherit; border: 0px; height: auto; max-width: 100%; clear: both; display: block; margin-left: auto; margin-right: auto;" alt="" src="https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-244x325.jpg" srcset="https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-244x325.jpg 244w, https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-768x1024.jpg 768w, https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o-910x1213.jpg 910w, https://daihoc.fpt.edu.vn/media/2019/03/54800085_2127311287355706_6976716829485957120_o.jpg 1296w" sizes="(max-width: 550px) 100vw, 550px"></p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Trong làn sóng cách mạng công nghiệp 4.0, Blockchain được xem là một công nghệ “chìa khóa” cho chuyển đổi số và xây dựng nền tảng công nghệ thông tin tương lai.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Với khả năng chia sẻ thông tin dữ liệu minh bạch theo thời gian thực, tiết kiệm không gian lưu trữ và bảo mật cao, công nghệ blockchain (chuỗi khối) là một trong những xu hướng công nghệ đột phá, có khả năng ứng dụng rộng rãi ở nhiều ngành nghề, lĩnh vực.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Với mong muốn mang tới cho sinh viên những kiên thức bổ ích về công nghệ tiên tiến này, phòng Công tác sinh viên tổ chức sự kiện “The dialogue: “Transforming industry to Blockchain: Utop & Akachain story’ vào ngày 27/3 tại phòng 102R – 104R Alpha. Diễn giả Trần Hoàng Giang – Đồng sáng lập- Giám đốc công nghệ Akachain sẽ giúp các bạn có cái nhìn sâu sắc hơn về làn sóng công nghệ này.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Anh Trần Hoàng Giang đã có 10 năm kinh nghiệm trong lĩnh vực lập trình, robot và các thiết bị điều khiển tự động hoá; 6 năm kinh nghiệm trong việc quản lý dự án; 2 năm kinh nghiệm làm quản lý trong bộ phận liên quan đến sản phẩm về Blockchain. Ngoài ra, anh Trần Hoàng Giang còn tham gia vào rất nhiều hoạt động phát triển cộng đồng khác như: Dự án Akachain, Blockchain Empire- Cộng đồng kết nối các kĩ sư phát triển và những người muốn tìm hiểu về công nghệ blockchain, …</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;"><img width="550" height="309" class="aligncenter wp-image-19953" style="box-sizing: inherit; border: 0px; height: auto; max-width: 100%; clear: both; display: block; margin-left: auto; margin-right: auto;" alt="" src="https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-578x325.jpg" srcset="https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-578x325.jpg 578w, https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-768x432.jpg 768w, https://daihoc.fpt.edu.vn/media/2019/03/55576313_2127601127326722_336673823384403968_o-910x512.jpg 910w" sizes="(max-width: 550px) 100vw, 550px"></p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Đặc biệt, anh Giang chính là cựu sinh viên Đại học FPT và ra nhập FPT Software, đảm nhiệm nhiều vị trí quan trọng tại FPT Software như lập trình viên, quản trị dự án, kiến trúc sư giải pháp, quản trị chương trình Blockchain của FPT Nhật Bản. Trong năm 2019 vừa qua, anh Trần Hoàng Giang vinh dự nằm trong danh sách FPT Under 35 và hiện anh là thành viên tích cực của Ban điều hành Mạng lưới khởi nghiệp Blockchain Việt Nam.</p><p style="box-sizing: inherit; margin: 0px 0px 1.5em;">Với kinh nghiệm phong phú và đa dạng trong cả lĩnh vực Blockchain lẫn quản lý dự án, buổi đối thoại hứa hẹn sẽ mang tới cho các bạn nhiều câu chuyện thú vị về chuyển đổi số cũng như về cơ hội nghề nghiệp trong ngành này dành cho các bạn sinh viên Đại học FPT.</p></div><p><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Phòng 102R – 104R Alpha', 0.0000, 5, 4, CAST(N'2019-04-21T12:09:19.890' AS DateTime), 5, CAST(N'2019-04-21T12:09:32.407' AS DateTime), CAST(N'2019-03-27T14:00:00.000' AS DateTime), CAST(N'2019-03-27T16:00:00.000' AS DateTime), CAST(N'2019-03-26T23:59:59.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (45, N'Học Kỳ Trao Đổi Tại Đại Học Kanto Gakuin - Nhật Bản', NULL, N'/Images/Events/74e98206-fec3-4eb1-989d-b9c1f463b890.jpg', 0, 1, N'Opening', N'HỌC KỲ TRAO ĐỔI TẠI ĐẠI HỌC KANTO GAKUIN - NHẬT BẢNTọa lạc tại thành phố cảng Yokohama, thủ phủ tỉnh Kanagawa, trường Đại học Kanto Gakuin được biết đến như một trong những trường đại học tư thục tốt nhất của tỉnh Kanagawa nói riêng và của đất nước Nhật Bản nói chung trong suốt hơn 100 năm hình thành và phát triển. Ngôi trường chất lượng này cũng chính là nơi sẽ diễn ra học kỳ trao đổi dành cho các bạn sinh viên FPTU HCM trong kỳ Fall 2019 sắp tới. Với chương trình Japanese Language and Culture Program, Đại học Kanto Gakuin dành ra 3 suất học trao đổi dành cho sinh viên tham gia học Ngôn ngữ và Văn hoá Nhật Bản. Đối tượng: Sinh viên ngành Ngôn ngữ Nhật và khối ngành Kỹ thuật phần mềm. Chương trình sẽ được diễn ra từ 27/08/2019 đến 24/12/2019.Thông tin chi tiết về chương trình các bạn có thể xem tại đường link: https://bitly.vn/frfĐể đăng ký, các bạn vui lòng hoàn thành các bước sau đây:Phòng IC sẽ xem xét và chọn ra các bạn sinh viên đủ tiêu chuẩn, những bạn được chọn sẽ tiếp tục hoàn tất các mẫu form bắt buộc từ phía Đại học Kanto Gakuin. Sau khi hoàn thành các bước trên, sinh viên sẽ được hỗ trợ để hoàn thành tất cả các thủ tục cuối cùng.Hạn chót đăng ký: 10/04/2019.Mọi ý kiến thắc mắc về chương trình, các bạn vui lòng liên hệ fanpage Study Overseas hoặc đến phòng IC (203) nhé!', N'<p style="text-align: justify; margin: 1em 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><b><span style="font-size: 14px;">HỌC KỲ TRAO ĐỔI TẠI ĐẠI HỌC KANTO GAKUIN - NHẬT BẢN</span></b></p><p style="text-align: justify; margin: 1em 0px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Tọa lạc tại thành phố cảng Yokohama, thủ phủ tỉnh Kanagawa, trường Đại học Kanto Gakuin được biết đế<span class="text_exposed_show" style="display: inline; font-family: inherit;">n như một trong những trường đại học tư thục tốt nhất của tỉnh Kanagawa nói riêng và của đất nước Nhật Bản nói chung trong suốt hơn 100 năm hình thành và phát triển. Ngôi trường chất lượng này cũng chính là nơi sẽ diễn ra học kỳ trao đổi dành cho các bạn sinh viên FPTU HCM trong kỳ Fall 2019 sắp tới.<span>&nbsp;</span></span></p><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="font-weight: 400; text-align: justify; margin: 1em 0px; font-family: inherit;">Với chương trình Japanese Language and Culture Program, Đại học Kanto Gakuin dành ra 3 suất học trao đổi dành cho sinh viên tham gia học Ngôn ngữ và Văn hoá Nhật Bản.<span>&nbsp;</span></p><p style="font-weight: 400; margin: 1em 0px; font-family: inherit;"></p><div style="text-align: justify;"><b><span style="font-family: inherit;">Đối tượng:</span><span style="font-family: inherit;">&nbsp;</span></b></div><div style="font-weight: 400; text-align: justify;"><span style="font-family: inherit;">Sinh viên ngành Ngôn ngữ Nhật và khối ngành Kỹ thuật phần mềm.</span><span style="font-family: inherit;">&nbsp;</span></div><p style="font-weight: 400;"></p><p style="font-weight: 400; text-align: justify; margin: 1em 0px; font-family: inherit;">Chương trình sẽ được diễn ra từ 27/08/2019 đến 24/12/2019.</p><p style="font-weight: 400; text-align: justify; margin: 1em 0px; font-family: inherit;">Thông tin chi tiết về chương trình các bạn có thể xem tại đường link:<span>&nbsp;</span><a style="color: rgb(54, 88, 153); cursor: pointer; text-decoration: none; font-family: inherit;" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fbitly.vn%2Ffrf%3Ffbclid%3DIwAR2-vEvLUkiSme_6goj2R3mVjxyQfJRWBRJge5Ozu53UJXlPyor2Fee_EiU&amp;h=AT11UrW4tlYlfDtGdtmdqRFht_FTLLMMhlhCcjOPYvFrjzTvrU9pGK4P7IvMeHgzx3jyWXI1OsychH9XThi9lOiqS3nmbcoKkGZmrdOW02uoTy1RTzWcBhFfTGP7dQlzytnpiW4QsJwAwKOIv65zSDvYJvvmmwqpqrXMtmFJZX2VMv9G7I1KRz6OJYFsRsmk4jlQChGs4NWk0GHRResk2P9A8Pdzu9rgRh768SveRT9acqxv0UbfTHnJ5H0CyiSTk1jyiaWE9w5oFAfKKDzFJZMvUO0fxuZ3WoQAekouHZdePu9lwfUF2_dwQ1wyP_hxettHMFN_YZu6Dmb0IYHWNhl01B5v_M-M7siZmjwP6JaZ7eNOsfcaRG3BtOQEXhLI2db1aJ-HeXb_ZAtQMujdB9MdtWtHKmrLAS_OiMfJ4EmXgdJtsOI_COT9ISg6MoZ1EnLkLISYy2lGgBNKSJPcVG4gShRgABFVdWBlxAaUh3xhCXZ8PqDYC6YpXz-tVz0iKKsX3D-iWhlZ8q1xAcASBakWJYAfnThJftKVaRXhVLUwnXNNvopWhEgaqa9Q-zWQ76ZaU_DvnskUb55CVKBmV0_t5ueODDbjWVX8WzE9MDtw9rZmnMWjGp3Xql6RtHTj4QoNYZgKssb8Qk8SeXccRfhy4PnWhMe-nS_4ASFFOsURXejIaKxT6TnQMbtxBhOqDLtAW6YIhi_QJPtrRph6ttUJFI5212NliepwmETYH_sX20DK2CG9paEUFsiKBHfs7uC5msaHXEoOif8M0-kPjjS4t1D_WiBXg9bLxjWdaVY40vz8wstMAG012DTNn8UihxNWecZavHZEmVv9UcQaLubVP9PJtEzmV7u3tBmQqChEVFPcGw" target="_blank" rel="noopener nofollow" data-ft="{" tn":"-u"}"="" data-lynx-mode="async">https://bitly.vn/frf</a></p><p style="font-weight: 400; text-align: justify; margin: 1em 0px; font-family: inherit;">Để đăng ký, các bạn vui lòng hoàn thành các bước sau đây:</p><p style="font-weight: 400; margin: 1em 0px; font-family: inherit;"></p><div style="font-weight: 400; text-align: justify;"><span style="font-family: inherit;">Phòng IC sẽ xem xét và chọn ra các bạn sinh viên đủ tiêu chuẩn, những bạn được chọn sẽ tiếp tục hoàn tất các mẫu form bắt buộc từ phía Đại học Kanto Gakuin.</span><span style="font-family: inherit;">&nbsp;</span><br></div><span class="_5mfr" style="font-weight: 400; margin: 0px 1px; font-family: inherit;"><div style="text-align: justify;"><span style="font-family: inherit;">Sau khi hoàn thành các bước trên, sinh viên sẽ được hỗ trợ để hoàn thành tất cả các thủ tục cuối cùng.</span></div></span><p style="font-weight: 400;"></p><p style="text-align: justify; margin: 1em 0px; font-family: inherit;"><b>Hạn chót đăng ký: </b>10/04/2019.</p><p style="font-weight: 400; text-align: justify; margin: 1em 0px 0px; font-family: inherit;">Mọi ý kiến thắc mắc về chương trình, các bạn vui lòng liên hệ fanpage Study Overseas hoặc đến phòng IC (203) nhé!</p></div><p style="margin: 1em 0px 0px; font-family: inherit;"></p><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(102, 102, 102); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><div style="text-align: justify;"><img alt="Trong hÃ¬nh áº£nh cÃ³ thá» cÃ³: báº§u trá»i, ÄÃ¡m mÃ¢y, ngoÃ&nbsp;i trá»i vÃ&nbsp; nÆ°á»c" src="https://scontent.fhan3-3.fna.fbcdn.net/v/t1.0-9/51623680_2251740125041355_1552057698801942528_n.jpg?_nc_cat=100&amp;_nc_oc=AQkuql7qzGFpasNSJli5i4gtV3KqadqAVsVkppeL7nTvAWuaJpygVCDxyOJx5q7r7E6FiOSQqL-5BR5N4ZcPu8n7&amp;_nc_ht=scontent.fhan3-3.fna&amp;oh=c89c8d9e5934c8ea809f1970aa2b7a11&amp;oe=5D44BD9E"></div></div><div class="text_exposed_show" style="display: inline; orphans: 2; text-align: start; text-indent: 0px; widows: 2; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><div style="text-align: justify;"><font color="#666666" face="Helvetica, Arial, sans-serif"><span style="font-size: 12px;"><br></span></font></div></div><p></p><div class="text_exposed_show" style="color: rgb(102, 102, 102); text-transform: none; text-indent: 0px; letter-spacing: normal; font-family: Helvetica,Arial,sans-serif; font-size: 12px; word-spacing: 0px; display: inline; white-space: normal; orphans: 2; widows: 2; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);"><div style="text-align: justify;"><img alt="Trong hÃ¬nh áº£nh cÃ³ thá» cÃ³: báº§u trá»i, cÃ¢y vÃ&nbsp; ngoÃ&nbsp;i trá»i" src="https://scontent.fhan3-3.fna.fbcdn.net/v/t1.0-9/51899496_2251740145041353_173572397369131008_n.jpg?_nc_cat=106&amp;_nc_oc=AQns65QLvWwdBTvtWqstZb39-9bm8JK-EZy4krd_9bRwE4uR4zHMrG9jceC_nvhprJAwFCE8wGnZkX27rpFjKUil&amp;_nc_ht=scontent.fhan3-3.fna&amp;oh=d4b8cc877ece3eed27beeb87fde35888&amp;oe=5D4DD43B" style="width: 599.12px; height: 449.34px;"></div></div>', N'Kanto, Japan', 73600000.0000, 5, 29, CAST(N'2019-02-20T01:46:08.127' AS DateTime), 5, CAST(N'2019-02-20T01:48:45.313' AS DateTime), CAST(N'2019-08-27T05:59:00.000' AS DateTime), CAST(N'2019-12-24T05:59:00.000' AS DateTime), CAST(N'2019-04-20T23:59:59.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (46, N'JS1102 Teambuilding', NULL, N'/Images/Events/3e19ea8a-a8ff-4058-bb2d-069a00cf3084.jpg', 0, 0, N'Closed', N'Đây là một sự kiện teambuilding của lớp JS1102', N'<p>Đây là một sự kiện teambuilding của lớp JS1102</p>', N'Sân bóng, ĐH FPT, Hòa Lạc', 150000.0000, 5, NULL, CAST(N'2019-04-20T22:26:36.110' AS DateTime), 5, CAST(N'2019-02-20T04:48:54.157' AS DateTime), CAST(N'2019-04-27T13:00:00.000' AS DateTime), CAST(N'2019-04-27T19:00:00.000' AS DateTime), CAST(N'2019-04-27T13:00:00.000' AS DateTime), 0, N'Lớp JS1102', NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (47, N'Guitar Hoa Lac Teambuilding', NULL, N'/Images/Events/9d1946dd-8793-4e9c-9b0a-2d5ce9cefac4.jpg', 1, 0, N'Pending', N'Đây là chuyến đi chơi của CLB Guitar và những người bạn.', N'<p>Đây là chuyến đi chơi của CLB Guitar và những người bạn.</p>', N'Sân bóng, ĐH FPT, Hòa Lạc', 0.0000, 5, NULL, CAST(N'2019-04-20T22:27:07.703' AS DateTime), 5, CAST(N'2019-04-15T23:08:37.250' AS DateTime), CAST(N'2019-04-15T01:00:00.000' AS DateTime), CAST(N'2019-04-15T23:59:00.000' AS DateTime), CAST(N'2019-04-15T01:00:00.000' AS DateTime), 0, N'Thành viên CLB Guitar và những người bạn', NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (48, N'JS1102 Teambuilding', NULL, N'/Images/Events/9315bed8-bb93-4f15-b4d2-022acf00d0e0.jpg', 1, 0, N'Draft', N'Đây là chuyến đi chơi vui vẻ của JS1102 nhé.', N'<p>Đây là chuyến đi chơi vui vẻ của JS1102 nhé.</p>', N'Sân bóng, ĐH FPT, Hòa Lạc', 0.0000, 5, NULL, CAST(N'2019-04-21T00:26:04.360' AS DateTime), 5, CAST(N'2019-04-21T00:10:09.767' AS DateTime), CAST(N'2019-04-15T23:18:00.000' AS DateTime), CAST(N'2019-04-15T23:20:00.000' AS DateTime), CAST(N'2019-04-15T23:18:00.000' AS DateTime), 0, N'Thành viên lớp JS1102', NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (49, N'[HÀNH TRÌNH ĐÁNG SỐNG ] Trải nghiệm lao động Bắc Giang ', NULL, N'/Images/Events/00f5b7bc-6e2c-4168-a6b9-b4746b43cfb9.jpg', 1, 0, N'Rejected', N'[HÀNH TRÌNH ĐÁNG SỐNG ] Trải nghiệm lao động Bắc Giang Tiếp nối những thành công của các chương trình trải nghiệm đã qua, học kỳ Summer 2018 này, phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn đến với những miền đất mới, cùng khám phá và cảm nhận về cuộc sống dưới nhiều góc độ khác nhau.Chương trình trải nghiệm lần này sẽ đưa chúng ta đến với làng nghề truyền thống nổi tiếng Bắc Giang. Tại đây, các bạn sẽ được chia ra thành từng nhóm để phụ giúp công việc tại các gia đình trong làng nghề, được tìm hiểu và trực tiếp tham gia vào các công đoạn khác nhau trong quy trình sản xuất bánh đa. Từ vo gạo, làm bột, phơi bánh đến nướng ra được những sản phẩm đặt yêu cầu. Ngoài ra, các bạn sẽ được trở thành một "người làm vườn thực thụ" thông qua những hoạt động tại đồi vải thiều Lục Ngạn nổi tiếng để được trực tiếp trải nghiệm quá trình chăm sóc công phu của người dân nơi đây trước khi đưa đến tay người tiêu dùng những sản phẩm tốt nhất.Hứa hẹn sẽ mang đến thật nhiều những điều vô cùng mới mẻ và lý thú cho các bạn sinh viên, Phổ tin rằng đây sẽ là một " Hành trình đáng sống" mà phòng IC-PDP muốn dành tặng cho tất cả các bạn.-----------------------------------------------------Thông tin chương trình:💡 Thời gian: Từ ngày 07/05 đến 10/05/2019.📌 Địa điểm: Bắc Giang💵 Phí tham gia: 150.000 vnđ/Sinh viên (bao gồm chi phí ăn, ở, đi lại cho 4 ngày 3 đêm).⏰Thời hạn đăng ký và đóng tiền: 17h00 ngày 22/04/2019☎️Thông tin chi tiết liên hệ: Ms. Phạm Thu Hiền - 0825.291.295Các bạn có thể nộp tiền trực tiếp tại phòng IC-PDP HB104L, tòa nhà Alpha hoặc chuyển khoản theo thông tin sau:Số tài khoản: 00450945001.Chủ tài khoản: Phạm Thu HiềnChi nhánh: TPBank chi nhánh Thăng Long- Hà Nội.Nội dung chuyển khoản:Họ và tên_MSSV_TNLD', N'<div class="q_ccwac3y2s i_ccwab-b4t clearfix" style="zoom: 1; margin-bottom: -1px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><div class="clearfix b_ccwac3x0_" style="zoom: 1; margin-bottom: -6px; font-family: inherit;"><div class="clearfix _42ef" style="zoom: 1; overflow: hidden; font-family: inherit;"><div class="e_ccwac3x14" style="padding-bottom: 6px; font-family: inherit;"><div style="font-family: inherit;"><div class="_6a _5u5j" style="display: inline-block; width: 402px; font-family: inherit;"><div class="_6a _5u5j _6b" style="display: inline-block; vertical-align: middle; width: 402px; font-family: inherit;"><h6 class="_7tae _14f3 _14f5 _5pbw _5vra" id="js_48" style="color: rgb(29, 33, 41); font-size: 14px; font-weight: normal; margin: 0px 0px 2px; padding: 0px 22px 0px 0px; line-height: 1.38; font-family: inherit;" data-ft="{"tn":"C"}"><br></h6></div></div></div></div></div></div></div><div class="_5pbx userContent _3576" id="js_4a" style="font-size: 14px; font-weight: 400; line-height: 1.38; margin-top: 6px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;" data-ft="{"tn":"K"}"><div class="text_exposed_root text_exposed" id="id_5cb585121c4e61306473396" style="display: inline; font-family: inherit;"><p style="margin: 0px 0px 6px; display: block; font-family: inherit;">[HÀNH TRÌNH ĐÁNG SỐNG ] Trải nghiệm lao động Bắc Giang<span> </span><br>Tiếp nối những thành công của các chương trình trải nghiệm đã qua, học kỳ Summer 2018 này, phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn đến với những miền đất mới, cùng khám phá và cảm nhận về cuộc sống dưới nhiều góc độ khác nhau.</p><p style="margin: 6px 0px; display: block; font-family: inherit;">Chương trình trải nghiệm lần này sẽ đưa chúng ta đến với làng nghề truyền thống nổi tiếng Bắc Giang. Tại đây, các bạn sẽ được chia ra thành từng n<span class="text_exposed_show" style="display: inline; font-family: inherit;">hóm để phụ giúp công việc tại các gia đình trong làng nghề, được tìm hiểu và trực tiếp tham gia vào các công đoạn khác nhau trong quy trình sản xuất bánh đa. Từ vo gạo, làm bột, phơi bánh đến nướng ra được những sản phẩm đặt yêu cầu. Ngoài ra, các bạn sẽ được trở thành một "người làm vườn thực thụ" thông qua những hoạt động tại đồi vải thiều Lục Ngạn nổi tiếng để được trực tiếp trải nghiệm quá trình chăm sóc công phu của người dân nơi đây trước khi đưa đến tay người tiêu dùng những sản phẩm tốt nhất.</span></p><div class="text_exposed_show" style="display: inline; font-family: inherit;"><p style="margin: 0px 0px 6px; font-family: inherit;">Hứa hẹn sẽ mang đến thật nhiều những điều vô cùng mới mẻ và lý thú cho các bạn sinh viên, Phổ tin rằng đây sẽ là một " Hành trình đáng sống" mà phòng IC-PDP muốn dành tặng cho tất cả các bạn.<br>-----------------------------------------------------<br>Thông tin chương trình:<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t3c/1/16/1f4a1.png");">💡</span></span><span> </span>Thời gian: Từ ngày 07/05 đến 10/05/2019.<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tac/1/16/1f4cc.png");">📌</span></span><span> </span>Địa điểm: Bắc Giang<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/tdf/1/16/1f4b5.png");">💵</span></span><span> </span>Phí tham gia: 150.000 vnđ/Sinh viên (bao gồm chi phí ăn, ở, đi lại cho 4 ngày 3 đêm).<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t34/1/16/23f0.png");">⏰</span></span>Thời hạn đăng ký và đóng tiền: 17h00 ngày 22/04/2019<br><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url("https://static.xx.fbcdn.net/images/emoji.php/v9/t22/1/16/260e.png");">☎️</span></span>Thông tin chi tiết liên hệ: Ms. Phạm Thu Hiền - 0825.291.295</p><p style="margin: 6px 0px; font-family: inherit;">Các bạn có thể nộp tiền trực tiếp tại phòng IC-PDP HB104L, tòa nhà Alpha hoặc chuyển khoản theo thông tin sau:<br>Số tài khoản: 00450945001.<br>Chủ tài khoản: Phạm Thu Hiền<br>Chi nhánh: TPBank chi nhánh Thăng Long- Hà Nội.<br>Nội dung chuyển khoản:<br>Họ và tên_MSSV_TNLD</p></div></div></div><p><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Bắc Giang', 150000.0000, 5, 4, CAST(N'2019-04-20T23:07:48.563' AS DateTime), 5, CAST(N'2019-04-21T00:10:12.220' AS DateTime), CAST(N'2019-05-07T07:30:00.000' AS DateTime), CAST(N'2019-05-10T19:30:00.000' AS DateTime), CAST(N'2019-05-07T07:30:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (50, N'Cơ hội học bổng toàn phần tại trường ĐH top 25 thế giới', NULL, N'/Images/Events/94692a8a-7100-4ec5-b311-43e87d1d6eca.jpg', 1, 0, N'Closed', N'[CƠ HỘI HỌC BỔNG TOÀN PHẦN TẠI TRƯỜNG ĐẠI HỌC TOP 25 THẾ GIỚI]Lượn một vòng facebook thấy nhiều bạn đang kêu than vì sao nghỉ lễ qua nhanh thếeeee. Để xua tan bầu không khí ảm đạm ấy, IC-PDP mang đến cho các bạn tin 1 tin rất vui: chính là cơ hội học bổng tại 1 trường Đại học vô cùng xịn sò đó nhaaa.Ngày 17/04 tới, đại diện trường University of Hongkong (HKU), một trong những Đại học hàng đầu châu Á và nằm top 25 thế giới sẽ có một buổi chia sẻ về các học bổng toàn phần, cũng như cơ hội theo học các chương trình sau đại học tại HKU, dành riêng cho sinh viên trường F chúng mình. Tại buổi chia sẻ này, đại diện trường HKU sẽ mang tới cho các bạn rất nhiều những thông tin hữu ích về các học bổng "khủng" cũng như các thông tin về cuộc sống, hoạt động của sinh viên tại trường.Bạn mong muốn tiếp tục sự nghiệp học tập của mình tại ngôi trường danh giá này sau khi tốt nghiệp FPTU? Còn chần chừ gì nữa hãy nhanh tay đăng ký bằng cách comment đầy đủ họ và tên để tham dự cùng chúng mình nhé.-----------------------THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:- Thời gian:14:00 - 15:30 thứ Tư ngày 17/04/2019- Địa điểm: phòng 402R, tòa nhà Alpha, Đại học FPT, campus Hòa Lạc.', N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">[CƠ HỘI HỌC BỔNG TOÀN PHẦN TẠI TRƯỜNG ĐẠI HỌC TOP 25 THẾ GIỚI]</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Lượn một vòng facebook thấy nhiều bạn đang kêu than vì sao nghỉ lễ qua nhanh thếeeee. Để xua tan bầu không khí ảm đạm ấy, IC-PDP mang đến cho các bạn tin 1 tin rất vui: chính là cơ hội học bổng tại 1 trường Đại học vô cùng xịn sò đó nhaaa.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Ngày 17/04 tới, đại diện trường University of Hongkong (HKU), một trong những Đại học hàng đầu</span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><span> </span>châu Á và nằm top 25 thế giới sẽ có một buổi chia sẻ về các học bổng toàn phần, cũng như cơ hội theo học các chương trình sau đại học tại HKU, dành riêng cho sinh viên trường F chúng mình. Tại buổi chia sẻ này, đại diện trường HKU sẽ mang tới cho các bạn rất nhiều những thông tin hữu ích về các học bổng "khủng" cũng như các thông tin về cuộc sống, hoạt động của sinh viên tại trường.<br><br>Bạn mong muốn tiếp tục sự nghiệp học tập của mình tại ngôi trường danh giá này sau khi tốt nghiệp FPTU? Còn chần chừ gì nữa hãy nhanh tay đăng ký bằng cách comment đầy đủ họ và tên để tham dự cùng chúng mình nhé.<br><img alt="Trong hÃ¬nh áº£nh cÃ³ thá» cÃ³: 2 ngÆ°á»i, má»i ngÆ°á»i Äang cÆ°á»i" src="https://scontent.fhan3-2.fna.fbcdn.net/v/t1.0-9/56927250_2319281385008022_2127766458166411264_n.jpg?_nc_cat=107&_nc_oc=AQkFKI6fDWpWutFxNVTxOyzzomk8c7iFU6Y0_ROaFumw6NjCJvDVb--xoUfjHSSEY7E015sluCf-bqb-eLWu-pO5&_nc_ht=scontent.fhan3-2.fna&oh=9f3713ab4d2edf5a33748836dad634bf&oe=5D4A314D"></span><span class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><br>-----------------------<br>THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:<br>- Thời gian:14:00 - 15:30 thứ Tư ngày 17/04/2019<br>- Địa điểm: phòng 402R, tòa nhà Alpha, Đại học FPT, campus Hòa Lạc.</span><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Phòng 402R, tòa nhà Alpha, Đại học FPT, campus Hòa Lạc.', 0.0000, 5, 4, CAST(N'2019-04-22T02:28:48.267' AS DateTime), 5, CAST(N'2019-04-22T02:29:11.093' AS DateTime), CAST(N'2019-04-17T14:00:00.000' AS DateTime), CAST(N'2019-04-17T15:30:00.000' AS DateTime), CAST(N'2019-04-17T14:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (51, N'[HÀNH TRÌNH ĐÁNG SỐNG ] Trải nghiệm lao động Bắc Giang', NULL, N'/Images/Events/d547bbb0-7d38-49aa-8fcd-9bb80155f8bb.jpg', 1, 0, N'Opening', N'[HÀNH TRÌNH ĐÁNG SỐNG ] Trải nghiệm lao động Bắc Giang Tiếp nối những thành công của các chương trình trải nghiệm đã qua, học kỳ Summer 2018 này, phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn đến với những miền đất mới, cùng khám phá và cảm nhận về cuộc sống dưới nhiều góc độ khác nhau. Chương trình trải nghiệm lần này sẽ đưa chúng ta đến với làng nghề truyền thống nổi tiếng Bắc Giang. Tại đây, các bạn sẽ được chia ra thành từng nhóm để phụ giúp công việc tại các gia đình trong làng nghề, được tìm hiểu và trực tiếp tham gia vào các công đoạn khác nhau trong quy trình sản xuất bánh đa. Từ vo gạo, làm bột, phơi bánh đến nướng ra được những sản phẩm đặt yêu cầu. Ngoài ra, các bạn sẽ được trở thành một "người làm vườn thực thụ" thông qua những hoạt động tại đồi vải thiều Lục Ngạn nổi tiếng để được trực tiếp trải nghiệm quá trình chăm sóc công phu của người dân nơi đây trước khi đưa đến tay người tiêu dùng những sản phẩm tốt nhất.Hứa hẹn sẽ mang đến thật nhiều những điều vô cùng mới mẻ và lý thú cho các bạn sinh viên, Phổ tin rằng đây sẽ là một " Hành trình đáng sống" mà phòng IC-PDP muốn dành tặng cho tất cả các bạn.-----------------------------------------------------Thông tin chương trình:Thời gian: Từ ngày 07/05 đến 10/05/2019.Địa điểm: Bắc GiangPhí tham gia: 150.000 vnđ/Sinh viên (bao gồm chi phí ăn, ở, đi lại cho 4 ngày 3 đêm).Thời hạn đăng ký và đóng tiền: 17h00 ngày 22/04/2019Thông tin chi tiết liên hệ: Ms. Phạm Thu Hiền - 0825.291.295Các bạn có thể nộp tiền trực tiếp tại phòng IC-PDP HB104L, tòa nhà Alpha hoặc chuyển khoản theo thông tin sau:Số tài khoản: 00450945001.Chủ tài khoản: Phạm Thu HiềnChi nhánh: TPBank chi nhánh Thăng Long- Hà Nội.Nội dung chuyển khoản:Họ và tên_MSSV_TNLD', N'<p style="text-align: justify; "><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b><br></b></span></p><p style="text-align: justify; "><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;"><b>[HÀNH TRÌNH ĐÁNG SỐNG ] Trải nghiệm lao động Bắc Giang&nbsp;</b></span></p><p><div style="text-align: justify;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Tiếp nối những thành công của các chương trình trải nghiệm đã qua, học kỳ Summer 2018 này, phòng Hợp tác quốc tế và phát triển cá nhân IC-PDP tiếp tục đồng hành cùng tất cả các bạn đến với những miền đất mới, cùng khám phá và cảm nhận về cuộc sống dưới nhiều góc độ khác nhau.&nbsp;</span><br></div><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><div style="text-align: justify;">Chương trình trải nghiệm lần này sẽ đưa chúng ta đến với làng ngh<span class="text_exposed_show" style="display: inline;">ề truyền thống nổi tiếng Bắc Giang. Tại đây, các bạn sẽ được chia ra thành từng nhóm để phụ giúp công việc tại các gia đình trong làng nghề, được tìm hiểu và trực tiếp tham gia vào các công đoạn khác nhau trong quy trình sản xuất bánh đa. Từ vo gạo, làm bột, phơi bánh đến nướng ra được những sản phẩm đặt yêu cầu. Ngoài ra, các bạn sẽ được trở thành một "người làm vườn thực thụ" thông qua những hoạt động tại đồi vải thiều Lục Ngạn nổi tiếng để được trực tiếp trải nghiệm quá trình chăm sóc công phu của người dân nơi đây trước khi đưa đến tay người tiêu dùng những sản phẩm tốt nhất.</span></div></span><span class="text_exposed_show" style="display: inline; orphans: 2; text-align: left; text-indent: 0px; widows: 2; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Hứa hẹn sẽ mang đến thật nhiều những điều vô cùng mới mẻ và lý thú cho các bạn sinh viên, Phổ tin rằng đây sẽ là một " Hành trình đáng sống" mà phòng IC-PDP muốn dành tặng cho tất cả các bạn.</span></div></font><span style="color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-weight: 400;"><div style="text-align: justify;"><span style="font-family: inherit;">--------------------------</span><wbr style="color: rgb(0, 0, 0); font-size: 1rem;"><span class="word_break" style="font-family: inherit; display: inline-block;"></span><span style="font-family: inherit;">--------------------------</span><wbr style="color: rgb(0, 0, 0); font-size: 1rem;"><span class="word_break" style="font-family: inherit; display: inline-block;"></span><font color="#1c1e21" face="Helvetica, Arial, sans-serif" style="font-size: 1rem;"><span style="font-size: 14px;">-</span></font></div></span><b style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><div style="text-align: justify;"><b>Thông tin chương trình:</b></div></b><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Thời gian: Từ ngày 07/05 đến 10/05/2019.</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Địa điểm: Bắc Giang</span></div></font></span></p><p style="text-align: justify;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Phí tham gia: 150.000 vnđ/Sinh viên (bao gồm chi phí ăn, ở, đi lại cho 4 ngày 3 đêm).</span></p><p style="text-align: justify;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Thời hạn đăng ký và đóng tiền: 17h00 ngày 22/04/2019</span></p><p><div style="text-align: justify;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Thông tin chi tiết liên hệ: Ms. Phạm Thu Hiền - 0825.291.295</span></div><span class="text_exposed_show" style="display: inline; orphans: 2; text-align: left; text-indent: 0px; widows: 2; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><div style="text-align: justify;"><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Các bạn có thể nộp tiền trực tiếp tại phòng IC-PDP HB104L, tòa nhà Alpha hoặc chuyển khoản theo thông tin sau:</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Số tài khoản: 00450945001.</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Chủ tài khoản: Phạm Thu Hiền</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Chi nhánh: TPBank chi nhánh Thăng Long- Hà Nội.</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Nội dung chuyển khoản:</span></div></font><font color="#1c1e21" face="Helvetica, Arial, sans-serif"><div style="text-align: justify;"><span style="font-size: 14px;">Họ và tên_MSSV_TNLD</span></div></font></span></p>', N'Bắc Giang', 150000.0000, 5, 29, CAST(N'2019-02-20T01:44:29.157' AS DateTime), 5, CAST(N'2019-02-20T01:48:33.877' AS DateTime), CAST(N'2019-05-07T07:30:00.000' AS DateTime), CAST(N'2019-05-10T19:30:00.000' AS DateTime), CAST(N'2019-04-22T23:59:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (52, N'Cơ hội học bổng toàn phần tại trường ĐH top 25 thế giới', NULL, N'/Images/Events/7bfeb945-a0ab-407d-a78c-8bfbd643b5d4.jpg', 1, 0, N'Rejected', N'[CƠ HỘI HỌC BỔNG TOÀN PHẦN TẠI TRƯỜNG ĐẠI HỌC TOP 25 THẾ GIỚI]Lượn một vòng facebook thấy nhiều bạn đang kêu than vì sao nghỉ lễ qua nhanh thếeeee. Để xua tan bầu không khí ảm đạm ấy, IC-PDP mang đến cho các bạn tin 1 tin rất vui: chính là cơ hội học bổng tại 1 trường Đại học vô cùng xịn sò đó nhaaa.Ngày 17/04 tới, đại diện trường University of Hongkong (HKU), một trong những Đại học hàng đầu châu Á và nằm top 25 thế giới sẽ có một buổi chia sẻ về các học bổng toàn phần, cũng như cơ hội theo học các chương trình sau đại học tại HKU, dành riêng cho sinh viên trường F chúng mình. Tại buổi chia sẻ này, đại diện trường HKU sẽ mang tới cho các bạn rất nhiều những thông tin hữu ích về các học bổng "khủng" cũng như các thông tin về cuộc sống, hoạt động của sinh viên tại trường.Bạn mong muốn tiếp tục sự nghiệp học tập của mình tại ngôi trường danh giá này sau khi tốt nghiệp FPTU? Còn chần chừ gì nữa hãy nhanh tay đăng ký bằng cách comment đầy đủ họ và tên để tham dự cùng chúng mình nhé.-----------------------THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:- Thời gian:14:00 - 15:30 thứ Tư ngày 17/04/2019- Địa điểm: phòng 402R, tòa nhà Alpha, Đại học FPT, campus Hòa Lạc.', N'<p style="margin: 0px 0px 6px; display: block; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">[CƠ HỘI HỌC BỔNG TOÀN PHẦN TẠI TRƯỜNG ĐẠI HỌC TOP 25 THẾ GIỚI]</p><p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Lượn một vòng facebook thấy nhiều bạn đang kêu than vì sao nghỉ lễ qua nhanh thếeeee. Để xua tan bầu không khí ảm đạm ấy, IC-PDP mang đến cho các bạn tin 1 tin rất vui: chính là cơ hội học bổng tại 1 trường Đại học vô cùng xịn sò đó nhaaa.</p><p style="margin: 6px 0px; display: block; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Ngày 17/04 tới, đại diện trường University of Hongkong (HKU), một trong những Đại học hàng đầu châu Á và nằm top 25 thế giới sẽ có một buổi chia sẻ về các học bổng toàn phần, c<span class="text_exposed_show" style="display: inline; font-family: inherit;">ũng như cơ hội theo học các chương trình sau đại học tại HKU, dành riêng cho sinh viên trường F chúng mình. Tại buổi chia sẻ này, đại diện trường HKU sẽ mang tới cho các bạn rất nhiều những thông tin hữu ích về các học bổng "khủng" cũng như các thông tin về cuộc sống, hoạt động của sinh viên tại trường.</span></p><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;"><p style="margin: 0px 0px 6px; font-family: inherit;">Bạn mong muốn tiếp tục sự nghiệp học tập của mình tại ngôi trường danh giá này sau khi tốt nghiệp FPTU? Còn chần chừ gì nữa hãy nhanh tay đăng ký bằng cách comment đầy đủ họ và tên để tham dự cùng chúng mình nhé.</p><p style="margin: 6px 0px; font-family: inherit;">-----------------------<br>THÔNG TIN CHI TIẾT VỀ CHƯƠNG TRÌNH:<br>- Thời gian:14:00 - 15:30 thứ Tư ngày 17/04/2019<br>- Địa điểm: phòng 402R, tòa nhà Alpha, Đại học FPT, campus Hòa Lạc.</p></div><p><b></b><i></i><u></u><sub></sub><sup></sup><strike></strike><br></p>', N'Phòng 402R, tòa nhà Alpha, Đại học FPT, campus Hòa Lạc.', 0.0000, 5, 4, CAST(N'2019-04-20T18:19:08.327' AS DateTime), 5, CAST(N'2019-04-20T18:19:19.923' AS DateTime), CAST(N'2019-04-17T14:00:00.000' AS DateTime), CAST(N'2019-04-17T16:00:00.000' AS DateTime), CAST(N'2019-04-16T23:59:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (53, N'The Event', 21, N'/Images/Events/2157926d-dc08-480f-9895-141b9eff1d7a.jpg', 0, 0, N'Closed', N'hello', N'<p>hello</p>', N'Hanoi', 0.0000, 2, NULL, CAST(N'2019-04-16T15:47:12.453' AS DateTime), 2, CAST(N'2019-04-16T15:47:34.377' AS DateTime), CAST(N'2018-06-06T12:00:00.000' AS DateTime), CAST(N'2018-06-06T21:00:00.000' AS DateTime), CAST(N'2018-06-05T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (54, N'FPT Educamp 2018 - Trường học 4.0', NULL, N'/Images/Events/18e86998-3531-4969-91bd-d68bc12da8d4.jpg', 0, 0, N'Closed', N'Lần thứ 5 tổ chức Educamp, nhà Giáo dục lựa chọn chủ đề “Trường học 4.0” để bật mí câu chuyện áp dụng công nghệ vào công tác giáo dục đào tạo và tuyển sinh với nội dung cùng hình thức mở hoàn toàn cho người tham dự.Educamp 2018 là sự kiện tổ chức thường niên của Tổ chức Giáo dục FPT với sứ mệnh tạo điều kiện cho cán bộ, nhân viên, giảng viên trong nhà trường có cơ hội học hỏi, trao đổi kinh nghiệm. Sự kiện lần này không chỉ dành cho người FPT Education, người FPT mà còn là sân chơi cho cộng đồng quan tâm đến giáo dục đào tạo.Từ đây, sự kiện kết nối các giảng viên, chuyên gia, các nhà hoạt động trong lĩnh vực giáo dục để chia sẻ các kinh nghiệm, tri thức và ý tưởng thúc đẩy sự phát triển của giáo dục Việt Nam nói chung và FPT Education nói riêng.Là tổ chức giáo dục sinh ra trong lòng doanh nghiệp với nền tảng công nghệ thông tin, FPT Education không nằm ngoài vòng tác động mạnh của cuộc cách mạng công nghiệp lần thứ 4. Bắt nhịp với xu hướng này, Educamp 2018 lựa chọn chủ đề “Trường học 4.0” trên hình thức hội thảo mở, diễn ra ngày 25/11 tại tòa nhà Beta, campus Hòa Lạc, Hà Nội.Chương trình với chủ đề "Trường học 4.0" sẽ diễn ra ngày 25/11. Sự kiện sẽ tập trung chia sẻ và thảo luận vào những nội dung quan trọng: Hoạt động dạy và học; Nghiên cứu khoa học; Hợp tác quốc tế; Đảm bảo chất lượng giáo dục; Thiết kế chương trình; Tuyển sinh; Dịch vụ trong trường học; Công tác sinh viên; Quan hệ doanh nghiệp; Tư vấn tâm lý; Phát triển cá nhân (PDP); Tổ chức và quản lý đào tạo; Vận hành trường học, kinh nghiệm triển khai Giáo dục trong thời đại 4.0…Đặc biệt, để đánh dấu tròn nửa thập kỷ đồng hành cùng FPT Edu, bênh cạnh việc trình bày tại các phòng chủ đề, các diễn giả tham gia hội thảo năm nay còn có cơ hội trưng bày các poster bài trình bày trong triển lãm của chương trình, và giao lưu trao đổi với người tham dự ngay tại không gian mở này. Người tham dự có thể tự di chuyển đến bất cứ phòng hội thảo nào có nội dung quan tâm, bổ ích và đặc sắc.Người FPT Education và cả các cá nhân, tổ chức bên ngoài quan tâm đến lĩnh vực giáo dục có thể đăng ký tham dự và trình bày tại đây. Thời gian đăng ký kéo dài từ ngày 17/9 -18/11.Đối với các cá nhân, tổ chức có bài trình bày tại Educamp 2018, thời gian nộp tóm tắt tham luận từ ngày 17/9 - 7/10. Từ ngày 7/10-7/11 là thời gian để nộp toàn văn tham luận và phần thuyết trình kèm poster. Từ đây, BTC sẽ chọn lựa 10 bài tham luận có ý nghĩa, thực tiến và sáng tạo để trình bày trong ngày 25/11 cùng với các chuyên gia có kinh nghiệm trong lĩnh vực giáo dục.Sự kiện Educamp đã tiến hành được 4 số từ năm 2014. Mỗi năm FPT Education đều mang đến những chủ đề theo đúng xu hướng và thiết thực với nền giáo dục hiện tại. Năm 2014 - “Đổi mới giáo dục và đào tạo trong bối cảnh toàn cầu hóa”. Năm 2015 - “Vận hành tổ chức giáo dục”. Năm 2016 - “Hướng tới chuẩn quốc tế trong tổ chức giáo dục”. Năm 2017 - “Tự học và trải nghiệm”.Educamp 2018 với chủ đề “Trường học 4.0”, diễn ra trong ngày 25/11 tại tòa nhà Beta, campus hòa Lạc, Hà Nội. Hội thảo miễn phí phí tham dự và hỗ trợ chi phí đi lại và buffet trưa tại hội thảo cho cán bộ, giảng viên và sinh viên thuộc các cơ sở đào tạo của FPT Education tại khu vực Hà Nội. 10 diễn giả có bài trình bày tại hội thảo đến từ các vùng, miền không thuộc Hà Nội sẽ nhận được vé hỗ trợ máy bay, kinh phí ăn, ở, đi lại để tham gia hội thảo.Thông tin chi tiết chương trình, đăng ký chia sẻ và liên hệ với BTC xin vui lòng truy cập website chính thức của Educamp2018 tại đây.', N'<div class="short_intro" style="margin: 0px; padding: 0px 0px 10px; font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: 700; font-stretch: normal; line-height: 23px; font-family: arial; width: 595.141px; float: left; color: rgb(51, 51, 51); text-rendering: geometricprecision;">Lần thứ 5 tổ chức Educamp, nhà Giáo dục lựa chọn chủ đề “Trường học 4.0” để bật mí câu chuyện áp dụng công nghệ vào công tác giáo dục đào tạo và tuyển sinh với nội dung cùng hình thức mở hoàn toàn cho người tham dự.</div><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Educamp 2018 là sự kiện tổ chức thường niên của Tổ chức Giáo dục FPT với sứ mệnh tạo điều kiện cho cán bộ, nhân viên, giảng viên trong nhà trường có cơ hội học hỏi, trao đổi kinh nghiệm. Sự kiện lần này không chỉ dành cho người FPT Education, người FPT mà còn là sân chơi cho cộng đồng quan tâm đến giáo dục đào tạo.</p><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Từ đây, sự kiện kết nối các giảng viên, chuyên gia, các nhà hoạt động trong lĩnh vực giáo dục để chia sẻ các kinh nghiệm, tri thức và ý tưởng thúc đẩy sự phát triển của giáo dục Việt Nam nói chung và FPT Education nói riêng.</p><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Là tổ chức giáo dục sinh ra trong lòng doanh nghiệp với nền tảng công nghệ thông tin, FPT Education không nằm ngoài vòng tác động mạnh của cuộc cách mạng công nghiệp lần thứ 4. Bắt nhịp với xu hướng này, Educamp 2018 lựa chọn chủ đề “Trường học 4.0” trên hình thức hội thảo mở, diễn ra ngày 25/11 tại tòa nhà Beta, campus Hòa Lạc, Hà Nội.</p><table align="center" border="0" cellpadding="3" cellspacing="0" class="tplCaption" style="margin: 0px auto 10px; padding: 0px; max-width: 100%; color: rgb(51, 51, 51); font-family: arial; font-size: 14px; width: 595.609px;"><tbody style="margin: 0px; padding: 0px;"><tr style="margin: 0px; padding: 0px;"><td style="margin: 0px; padding: 0px; line-height: 0;"><img alt="42059238-547050255717506-93273-9421-2682" data-natural-width="620" src="https://i.chungta.vn/2018/09/19/42059238-547050255717506-93273-9421-2682-1537329172.jpg" style="margin: 0px; padding: 0px; border: 0px; font-size: 0px; line-height: 0; max-width: 100%;"></td></tr><tr style="margin: 0px; padding: 0px;"><td style="margin: 0px; padding: 0px; line-height: 0;"><p class="Image" style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 15px 10px 10px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 15px; line-height: 22px; text-rendering: geometricprecision; background: rgb(244, 244, 244);">Chương trình với chủ đề "Trường học 4.0" sẽ diễn ra ngày 25/11.&nbsp;</p></td></tr></tbody></table><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Sự kiện sẽ tập trung chia sẻ và thảo luận vào những nội dung quan trọng: Hoạt động dạy và học; Nghiên cứu khoa học; Hợp tác quốc tế; Đảm bảo chất lượng giáo dục; Thiết kế chương trình; Tuyển sinh; Dịch vụ trong trường học; Công tác sinh viên; Quan hệ doanh nghiệp; Tư vấn tâm lý; Phát triển cá nhân (PDP); Tổ chức và quản lý đào tạo; Vận hành trường học, kinh nghiệm triển khai Giáo dục trong thời đại 4.0…</p><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Đặc biệt, để đánh dấu tròn nửa thập kỷ đồng hành cùng FPT Edu, bênh cạnh việc trình bày tại các phòng chủ đề, các diễn giả tham gia hội thảo năm nay còn có cơ hội trưng bày các poster bài trình bày trong triển lãm của chương trình, và giao lưu trao đổi với người tham dự ngay tại không gian mở này. Người tham dự có thể tự di chuyển đến bất cứ phòng hội thảo nào có nội dung quan tâm, bổ ích và đặc sắc.</p><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Người FPT Education và cả các cá nhân, tổ chức bên ngoài quan tâm đến lĩnh vực giáo dục có thể đăng ký tham dự và trình bày&nbsp;<strong style="margin: 0px; padding: 0px;"><a href="https://docs.google.com/forms/d/1cwFoIyFxPSEVoZ9TU2ykbcvXvPu2_gdvf7EKx_DprgI/viewform?edit_requested=true" style="margin: 0px; padding: 0px; color: rgb(0, 79, 139); outline: 1px; font-size: 14px;"><span style="margin: 0px; padding: 0px; color: rgb(0, 0, 255);">tại đây</span></a></strong>. Thời gian đăng ký kéo dài từ ngày 17/9 -18/11.</p><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Đối với các cá nhân, tổ chức có bài trình bày tại Educamp 2018, thời gian nộp tóm tắt tham luận từ ngày 17/9 - 7/10. Từ ngày 7/10-7/11 là thời gian để nộp toàn văn tham luận và phần thuyết trình kèm poster. Từ đây, BTC sẽ chọn lựa 10 bài tham luận có ý nghĩa, thực tiến và sáng tạo để trình bày trong ngày 25/11 cùng với các chuyên gia có kinh nghiệm trong lĩnh vực giáo dục.</p><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; font-family: arial; text-rendering: geometricprecision; color: rgb(51, 51, 51); text-align: justify;">Sự kiện Educamp đã tiến hành được 4 số từ năm 2014. Mỗi năm FPT Education đều mang đến những chủ đề theo đúng xu hướng và thiết thực với nền giáo dục hiện tại. Năm 2014 - “Đổi mới giáo dục và đào tạo trong bối cảnh toàn cầu hóa”. Năm 2015 - “Vận hành tổ chức giáo dục”. Năm 2016 - “Hướng tới chuẩn quốc tế trong tổ chức giáo dục”. Năm 2017 - “Tự học và trải nghiệm”.</p><table align="center" border="1" cellpadding="1" cellspacing="0" class="tbl_insert" style="margin: 0px auto 10px; padding: 0px; max-width: 100%; color: rgb(51, 51, 51); font-family: arial; font-size: 14px; width: 595px;"><tbody style="margin: 0px; padding: 0px;"><tr style="margin: 0px; padding: 0px;"><td style="margin: 0px; padding: 2px;"><p class="Normal" style="margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 18px; text-rendering: geometricprecision; background: none; text-align: justify;">Educamp 2018 với chủ đề “Trường học 4.0”, diễn ra trong ngày 25/11 tại tòa nhà Beta, campus hòa Lạc, Hà Nội. Hội thảo miễn phí phí tham dự và hỗ trợ chi phí đi lại và buffet trưa tại hội thảo cho cán bộ, giảng viên&nbsp;và sinh viên thuộc các cơ sở đào tạo của FPT Education tại khu vực Hà Nội. 10 diễn giả có bài trình bày tại hội thảo đến từ các vùng, miền không thuộc Hà Nội sẽ nhận được vé hỗ trợ máy bay, kinh phí ăn, ở, đi lại để tham gia hội thảo.</p><p class="Normal" style="margin: 10px 0px 20px; padding: 0px; clear: both; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 17px; line-height: 21px; text-rendering: geometricprecision; background: none; text-align: justify;">Thông tin chi tiết chương trình, đăng ký chia sẻ và liên hệ với BTC xin vui lòng truy cập website chính thức của Educamp2018&nbsp;<strong style="margin: 0px; padding: 0px;"><a href="http://educamp.fpt.edu.vn/" style="margin: 0px; padding: 0px; color: rgb(0, 79, 139); outline: 1px; font-size: 14px;"><span style="margin: 0px; padding: 0px; color: rgb(0, 0, 255);">tại đây.</span></a></strong></p></td></tr></tbody></table>', N'Hòa Lạc', 0.0000, 5, 18, CAST(N'2019-04-17T16:15:33.297' AS DateTime), 5, CAST(N'2019-04-17T16:15:48.297' AS DateTime), CAST(N'2018-11-25T08:30:00.000' AS DateTime), CAST(N'2018-11-25T16:30:00.000' AS DateTime), CAST(N'2018-11-24T12:59:00.000' AS DateTime), 0, N'Cán bộ, giảng viên và sinh viên thuộc các cơ sở đào tạo của FPT Education tại khu vực Hà Nội', NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (55, N'VIETNAM SALES AND MARKETING CAMP 2018', NULL, N'/Images/Events/9572e908-1240-4019-b644-04b3a40fcffb.png', 1, 0, N'Closed', N'🎉ĐẠI HỘI SALES & MARKETING TOÀN QUỐC LỚN NHẤT TRONG NĂM :VIETNAM SALES AND MARKETING CAMP 2018: “CREATECH - Cuộc hôn phối của sáng tạo & công nghệ”VSMCamp & CSMOSummit - hội nghị thượng đỉnh dành cho các lãnh đạo cấp cao với sự góp mặt của các chuyên gia trong lĩnh vực Sales & Marketing hàng đầu trong nước & quốc tế đã trở lại trong năm 2018.Tiếp nối thành công của hai mùa VSMCAMP và CSMO Summit trước, Le Group - tập đoàn Truyền thông Lê tiếp tục đồng hành cùng VSMCAMP và CSMO Summit 2018 trên con đường xây dựng và phát triển cộng đồng sales, marketing và truyền thông Việt Nam.Bạn sẽ sẵn sàng tham gia cùng chúng tôi chứ ?ĐĂNG KÍ NGAY!Mọi chi tiết xin vui lòng liên hệ:Email: vsmcamp.csmo@gmail.com — tại FPT University Vietnam.', N'<div id="id_5cb6f08e668882393047635" class="text_exposed_root text_exposed" style="display: inline; font-family: inherit;"><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-decoration: none; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(&quot;https://static.xx.fbcdn.net/images/emoji.php/v9/t8c/1/16/1f389.png&quot;);">🎉</span></span>ĐẠI HỘI SALES &amp; MARKETING TOÀN QUỐC LỚN NHẤT TRONG NĂM :<br>VIETNAM SALES AND MARKETING CAMP 2018: “CREATECH - Cuộc hôn phối của sáng tạo &amp; công nghệ”<br><br>VSMCamp &amp; CSMOSummit - hội nghị thượng đỉnh dành cho các lãnh đạo cấp cao với sự góp mặt của các chuyên gia trong lĩnh vực Sales &amp; Marketing hàng đầu trong nước &amp; quốc tế đã trở lại trong năm 2018.<br>Tiếp nối thành công của hai m<span class="text_exposed_show" style="display: inline; font-family: inherit;">ùa VSMCAMP và CSMO Summit trước, Le Group - tập đoàn Truyền thông Lê tiếp tục đồng hành cùng VSMCAMP và CSMO Summit 2018 trên con đường xây dựng và phát triển cộng đồng sales, marketing và truyền thông Việt Nam.<br><br><br>Bạn sẽ sẵn sàng tham gia cùng chúng tôi chứ ?<br>ĐĂNG KÍ NGAY!<br><br>Mọi chi tiết xin vui lòng liên hệ:<br>Email: vsmcamp.csmo@gmail.com</span></div><p><span class="fbPhotosPhotoCaption" tabindex="0" aria-live="polite" data-ft="{&quot;tn&quot;:&quot;K&quot;}" id="fbPhotoSnowliftCaption" style="outline: none; display: inline; width: auto; font-size: 14px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33);"><span class="hasCaption" style="font-family: inherit;"></span></span><span class="fbPhotoTagList" id="fbPhotoSnowliftTagList" style="display: inline; font-size: 14px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33);"><span class="fcg" style="color: rgb(144, 148, 156); font-family: inherit;">&nbsp;— tại&nbsp;<span class="fbPhotoTagListTag withTagItem tagItem" style="font-family: inherit;"><a class="taggee" href="https://www.facebook.com/FPTUniversity.VN/" data-hovercard="/ajax/hovercard/page.php?id=372823802772145" data-hovercard-prefer-more-content-show="1" style="color: rgb(54, 88, 153); cursor: pointer; font-family: inherit;">FPT University Vietnam</a></span>.</span></span><br></p><div><span class="fbPhotoTagList" style="display: inline; font-size: 14px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color: rgb(28, 30, 33);"><span class="fcg" style="color: rgb(144, 148, 156); font-family: inherit;"><br></span></span></div>', N'Long Biên Golf Course Hà Nội', 0.0000, 5, NULL, CAST(N'2019-04-17T16:26:54.970' AS DateTime), 5, CAST(N'2019-04-17T16:27:05.547' AS DateTime), CAST(N'2018-11-23T08:30:00.000' AS DateTime), CAST(N'2018-11-24T18:30:00.000' AS DateTime), CAST(N'2018-11-20T12:59:00.000' AS DateTime), 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (56, N'FPT EDU NihongoEng 2019', 22, N'/Images/Events/9e610451-079b-4979-87f0-b3db358c0a76.jpg', 1, 1, N'Opening', N'CUỘC THI FPT EDU NIHONGOENGMột sân chơi lớn dành cho các bạn sinh viên yêu tiếng Anh và tiếng Nhật.Nơi thể hiện khả năng và bản lĩnh của bạn.Một sân chơi nhiều trải nghiệm và giải thưởng hấp dẫn.Hạn đăng ký: 15/05/2019Cuộc thi với chủ đề: “The change – Be a game changer”1. Về thể lệ cuộc thiCuộc thi được chia làm 2 bảng thi độc lập và có giải thưởng riêng: Bảng Tiếng Anh & Bảng Tiếng Nhật.Thi theo đội, mỗi đội có 2-3 thành viên là học sinh, sinh viên đang theo học tại các hệ đào tạo thuộc tổ chức giáo dục FPT.Cuộc thi gồm 3 vòng: Vòng 1: Sơ loại; Vòng 2: Thử thách; Vòng 3: Chung kết.Tại vòng đầu tiên, vòng sơ loại, các đội thực hiện một đoạn clip dài 3-5 phút với độ phân giải tối thiểu là FullHD (1080p). Nội dung trong clip cần trình bày được: - Tên đội, các thành viên trong đội- Các thành viên trong đội thi lần lượt giới thiệu về bản thân; Ngoại ngữ đã làm thay đổi bản thân như thế nào; Lý do tham dự cuộc thi.- Clip gửi về địa chỉ email của chương trình: nihongoeng@fe.edu.vnHội đồng chuyên môn sẽ lựa chọn ra 15 đội xuất sắc nhất mỗi bảng để vào vòng 2.2. Cơ cấu giải thưởngVòng sơ loại: Top 15/ mỗi bảng: 500.000VNĐVòng bán kết: Top 6/ mỗi bảng: 1.000.000VNĐVòng chung kết: - Giải nhất mỗi bảng: 30.000.000VNĐ- Giải nhì mỗi bảng: 20.000.000VNĐ- Giải ba mỗi bảng: 10.000.000VNĐ- Giải khuyến khích mỗi bảng: 5.000.000VNĐCùng nhiều hiện vật có giá trị khác.Cuộc thi ngoại ngữ lớn nhất năm của FPT Edu đã chính thức khởi động. Có gì thú vị ở cuộc thi này và tại sao học sinh, sinh viên chúng ta không thể bỏ lỡ. Hãy đón xem 5 lý do “chuẩn không cần chỉnh” sau đây nhé!1. Giải thưởng cực “khủng”:Cùng “nghía” qua giải thưởng của cuộc thi một chút nào: Giải Nhất: 30 triệu đồng, giải Nhì: 20 triệu đồng,… thậm chí cả các đội thi lọt vào top 30 và top 12 cũng được nhận quà từ BTC. Có mấy cuộc thi dành cho học sinh, sinh viên mà giải thưởng lại “khủng” đến vậy? Cứ nghĩ đến việc số tiền kia có thể giúp mình làm biết bao nhiêu việc, các bạn đã có thêm động lực để gửi bài dự thi chưa nào?2. Được giao lưu với các FPT-ers khắp bốn phương trời:Là cuộc thi dành cho học sinh, sinh viên FPT Edu ở mọi miền đất nước, chắc chắn sân chơi Nihongo Eng 2019 sẽ trở thành điểm hội ngộ lý tưởng của FPT-ers mọi miền. Đặc biệt, với việc đêm chung kết cuộc thi diễn ra ở Hòa Lạc Campus (Hà Nội), các thí sinh đến từ miền Trung hay miền Nam còn có thêm một chuyến du lịch miễn phí ra miền Bắc nữa đấy! Thật là cơ hội tuyệt vời để giao lưu, kết bạn cũng như tìm hiểu những điều đặc biệt của FPT Edu ở mỗi miền đất nước đúng không nào?3. Nâng “level” ngoại ngữ:Ai cũng biết, muốn giỏi ngoại ngữ thì phải liên tục thực hành. Thực hành trên lớp chưa đủ, còn phải tập luyện trong thực tế và với nhiều lĩnh vực. Vậy tại sao bạn không tìm kiếm cơ hội nâng cao khả năng ngoại ngữ của mình tại NihongoEng 2019? Trong cuộc thi, bạn sẽ sử dụng ngoại ngữ để giới thiệu bản thân, thể hiện cá tính, thuyết trình, hùng biện và cả phỏng vấn chéo… Tất tần tật những tình huống “cân não” như vậy sẽ giúp bạn trui rèn khả năng của mình. Rất có thể, sau NihongoEng 2019, “level” tiếng Anh, tiếng Nhật của bạn sẽ được nâng lên một tầm cao mới.4. Sân chơi quy mô và chuyên nghiệp:Đừng quên, đây là cuộc thi ngoại ngữ đầu tiên được tổ chức với quy mô toàn FPT Edu. Không chỉ được giao lưu với các “anh tài” bốn phương, các bạn còn được hỗ trợ bởi đội ngũ giảng viên giàu kinh nghiệm chuyên môn khi lọt sâu vào vòng trong. Hình thức thi ở mỗi vòng không giống nhau, nhằm giúp bạn thể hiện được hết mọi khả năng của bản thân, đồng thời giúp BGK có thể lựa chọn ra những cái tên tỏa sáng nhất để bước vào đêm chung kết.5. Giúp CV của bạn thêm “tỏa sáng”:Có thành tích tốt ở những cuộc thi như FPT Edu Nihongo Eng 2019, chắc chắn CV của bạn sẽ thêm tỏa sáng trước mắt nhà tuyển dụng (với sinh viên) hay Hội đồng xét duyệt học bổng tại các trường Đại học (với học sinh). Đồng thời, với việc khả năng của bạn đã được kiểm chứng qua một cuộc thi, bạn cũng có thêm tự tin để tham gia các cuộc thi ở cấp cao hơn.Trên đây là 5 lợi ích tuyệt vời khi tham gia FPT Edu Nihongo Eng 2019. Vậy còn chần chừ gì nữa mà bạn chưa rủ đồng đội gửi clip tham gia vòng loại cuộc thi? Hãy bắt đầu hành trình chinh phục “ngôi vương” của FPT Edu Nihongo Eng 2019 mùa đầu tiên nào!FPT Edu NihongoEng là cuộc thi thể hiện khả năng sử dụng ngôn ngữ Tiếng Anh và Tiếng Nhật dành cho học sinh, sinh viên trong toàn Tổ chức giáo dục FPT (FPT Edu). Cuộc thi được tổ chức với mong muốn tạo sân chơi giúp các bạn rèn luyện, trau dồi kiến thức, kỹ năng qua các vòng thi; đồng thời khuyến khích tinh thần học tập ngoại ngữ, cũng như tìm kiếm những học sinh, sinh viên FPT Edu tài năng cho các cuộc thi cấp cao hơn.•    Thời gian gửi clip dự thi vòng sơ khảo: 17/04 – 15/05/2019•    Vòng bán kết diễn ra vào ngày 25/5/2019•    Vòng chung kết diễn ra vào 11/6/2019•    Link đăng ký dự thi: http://bit.ly/NihongoEngreg•    Fanpage cuộc thi: http://bit.ly/nihongoengfanpage', N'<p style="text-align: center; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><b><span style="font-size: 18px;">CUỘC THI FPT EDU NIHONGOENG</span></b></p><p style="margin: 6px 0px;"><div style="color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">Một sân chơi lớn dành cho các bạn sinh viên yêu tiếng Anh và tiếng Nhật.</div><div style="color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">Nơi thể hiện khả năng và bản lĩnh của bạn.</div><div style="color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">Một sân chơi nhiều trải nghiệm và giải thưởng hấp dẫn.</div><span class="text_exposed_show" style="display: inline;"><div style="text-align: justify;"><font color="#1d2129" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><font color="#1d2129" face="inherit"><div style="text-align: justify;"><span style="font-size: 14px; font-family: inherit;">Hạn đăng ký: 15/05/2019</span></div></font><font color="#1d2129" face="inherit"><div style="text-align: justify;"><span style="font-size: 14px; font-family: inherit;">Cuộc thi với chủ đề: “The change – Be a game changer”</span></div></font></span></p><p style="margin: 6px 0px;"><div style="text-align: justify;"><font color="#1d2129" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><span class="text_exposed_show" style="color: rgb(29, 33, 41); font-family: inherit; font-size: 14px; display: inline;"><div style="text-align: justify;"><b style="font-family: inherit;">1. Về thể lệ cuộc thi</b></div><div style="text-align: justify;"><span style="font-family: inherit;">Cuộc thi được chia làm 2 bảng thi độc lập và có giải thưởng riêng: Bảng Tiếng Anh &amp; Bảng Tiếng Nhật.</span></div><div style="text-align: justify;"><span style="font-family: inherit;">Thi theo đội, mỗi đội có 2-3 thành viên là học sinh, sinh viên đang theo học tại các hệ đào tạo thuộc tổ chức giáo dục FPT.</span></div><div style="text-align: justify;"><span style="font-family: inherit;">Cuộc thi gồm 3 vòng: Vòng 1: Sơ loại; Vòng 2: Thử thách; Vòng 3: Chung kết.</span></div><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><div style="text-align: justify;"><span style="font-family: inherit;">Tại vòng đầu tiên, vòng sơ loại, các đội thực hiện một đoạn clip dài 3-5 phút với độ phân giải tối thiểu là FullHD (1080p). Nội dung trong clip cần trình bày được:&nbsp;</span></div></span><div style="text-align: justify;"><span style="font-family: inherit;">- Tên đội, các thành viên trong đội</span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Các thành viên trong đội thi lần lượt giới thiệu về bản thân; Ngoại ngữ đã làm thay đổi bản thân như thế nào; Lý do tham dự cuộc thi.</span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Clip gửi về địa chỉ email của chương trình: nihongoeng@fe.edu.vn</span></div><div style="text-align: justify;"><span style="font-family: inherit;">Hội đồng chuyên môn sẽ lựa chọn ra 15 đội xuất sắc nhất mỗi bảng để vào vòng 2.</span></div></span></p><p style="margin: 6px 0px;"><div style="text-align: justify;"><font color="#1d2129" face="Helvetica, Arial, sans-serif"><span style="font-size: 14px;"><br></span></font></div><span class="text_exposed_show" style="color: rgb(29, 33, 41); font-family: inherit; font-size: 14px; display: inline;"><div style="text-align: justify;"><b style="font-family: inherit;">2. Cơ cấu giải thưởng</b></div><div style="text-align: justify;"><span style="font-family: inherit;">Vòng sơ loại: Top 15/ mỗi bảng: 500.000VNĐ</span></div><div style="text-align: justify;"><span style="font-family: inherit;">Vòng bán kết: Top 6/ mỗi bảng: 1.000.000VNĐ</span></div><div style="text-align: justify;"><span style="font-family: inherit;">Vòng chung kết:&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Giải nhất mỗi bảng: 30.000.000VNĐ</span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Giải nhì mỗi bảng: 20.000.000VNĐ</span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Giải ba mỗi bảng: 10.000.000VNĐ</span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Giải khuyến khích mỗi bảng: 5.000.000VNĐ</span></div><div style="text-align: justify;"><span style="font-family: inherit;">Cùng nhiều hiện vật có giá trị khác.</span></div></span></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"=""><span style="font-weight: 700;">Cuộc thi ngoại ngữ lớn nhất năm của FPT Edu đã chính thức khởi động. Có gì thú vị ở cuộc thi này và tại sao học sinh, sinh viên chúng ta không thể bỏ lỡ. Hãy đón xem 5 lý do “chuẩn không cần chỉnh” sau đây nhé!</span></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" center;"=""><img alt="" height="213" src="http://fpt.edu.vn/resources/article/uploads/20190417020240352_COVER%20them%20fanpage-01.jpg" width="560" class="fadeIn" style="border: 0px; height: auto; animation: fadeIn 0.3s ease 0s 1 normal none running; width: 730px; visibility: visible !important;"></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"=""><span style="font-weight: 700;">1. Giải thưởng cực “khủng”:</span></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"="">Cùng “nghía” qua giải thưởng của cuộc thi một chút nào: Giải Nhất: 30 triệu đồng, giải Nhì: 20 triệu đồng,… thậm chí cả các đội thi lọt vào top 30 và top 12 cũng được nhận quà từ BTC. Có mấy cuộc thi dành cho học sinh, sinh viên mà giải thưởng lại “khủng” đến vậy? Cứ nghĩ đến việc số tiền kia có thể giúp mình làm biết bao nhiêu việc, các bạn đã có thêm động lực để gửi bài dự thi chưa nào?</p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"=""><span style="font-weight: 700;">2. Được giao lưu với các FPT-ers khắp bốn phương trời:</span></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"="">Là cuộc thi dành cho học sinh, sinh viên FPT Edu ở mọi miền đất nước, chắc chắn sân chơi Nihongo Eng 2019 sẽ trở thành điểm hội ngộ lý tưởng của FPT-ers mọi miền. Đặc biệt, với việc đêm chung kết cuộc thi diễn ra ở Hòa Lạc Campus (Hà Nội), các thí sinh đến từ miền Trung hay miền Nam còn có thêm một chuyến du lịch miễn phí ra miền Bắc nữa đấy! Thật là cơ hội tuyệt vời để giao lưu, kết bạn cũng như tìm hiểu những điều đặc biệt của FPT Edu ở mỗi miền đất nước đúng không nào?</p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" center;"=""><img alt="" src="http://fpt.edu.vn/resources/article/uploads/20190416111042331_trongmo3.png" class="fadeIn" style="border: 0px; height: auto; animation: fadeIn 0.3s ease 0s 1 normal none running; width: 730px; visibility: visible !important;"></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"=""><span style="font-weight: 700;">3. Nâng “level” ngoại ngữ:</span></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"="">Ai cũng biết, muốn giỏi ngoại ngữ thì phải liên tục thực hành. Thực hành trên lớp chưa đủ, còn phải tập luyện trong thực tế và với nhiều lĩnh vực. Vậy tại sao bạn không tìm kiếm cơ hội nâng cao khả năng ngoại ngữ của mình tại NihongoEng 2019? Trong cuộc thi, bạn sẽ sử dụng ngoại ngữ để giới thiệu bản thân, thể hiện cá tính, thuyết trình, hùng biện và cả phỏng vấn chéo… Tất tần tật những tình huống “cân não” như vậy sẽ giúp bạn trui rèn khả năng của mình. Rất có thể, sau NihongoEng 2019, “level” tiếng Anh, tiếng Nhật của bạn sẽ được nâng lên một tầm cao mới.</p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"=""><span style="font-weight: 700;">4. Sân chơi quy mô và chuyên nghiệp:</span></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"="">Đừng quên, đây là cuộc thi ngoại ngữ đầu tiên được tổ chức với quy mô toàn FPT Edu. Không chỉ được giao lưu với các “anh tài” bốn phương, các bạn còn được hỗ trợ bởi đội ngũ giảng viên giàu kinh nghiệm chuyên môn khi lọt sâu vào vòng trong. Hình thức thi ở mỗi vòng không giống nhau, nhằm giúp bạn thể hiện được hết mọi khả năng của bản thân, đồng thời giúp BGK có thể lựa chọn ra những cái tên tỏa sáng nhất để bước vào đêm chung kết.</p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"=""><span style="font-weight: 700;">5. Giúp CV của bạn thêm “tỏa sáng”:</span></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"="">Có thành tích tốt ở những cuộc thi như FPT Edu Nihongo Eng 2019, chắc chắn CV của bạn sẽ thêm tỏa sáng trước mắt nhà tuyển dụng (với sinh viên) hay Hội đồng xét duyệt học bổng tại các trường Đại học (với học sinh). Đồng thời, với việc khả năng của bạn đã được kiểm chứng qua một cuộc thi, bạn cũng có thêm tự tin để tham gia các cuộc thi ở cấp cao hơn.</p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" center;"=""><img alt="" height="426" src="http://fpt.edu.vn/resources/article/uploads/dantrituvan1.jpg" width="640" class="fadeIn" style="border: 0px; height: auto; animation: fadeIn 0.3s ease 0s 1 normal none running; width: 730px; visibility: visible !important;"></p><p style="text-align: justify; margin: 10px 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" text-align:="" justify;"="">Trên đây là 5 lợi ích tuyệt vời khi tham gia FPT Edu Nihongo Eng 2019. Vậy còn chần chừ gì nữa mà bạn chưa rủ đồng đội gửi clip tham gia vòng loại cuộc thi? Hãy bắt đầu hành trình chinh phục “ngôi vương” của FPT Edu Nihongo Eng 2019 mùa đầu tiên nào!</p><p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span class="text_exposed_show" style="display: inline; font-family: inherit;"></span></p><table align="center" border="1" cellpadding="1" cellspacing="1" style="text-align: justify; border-spacing: 0px; color: rgb(51, 51, 51);" helvetica="" neue",="" arial,="" "lucida="" grande",="" sans-serif;="" font-size:="" 14px;="" width:="" 500px;"=""><tbody><tr><td style="padding: 0px;"><p style="margin: 10px 0px;"><span style="font-weight: 700;">FPT Edu NihongoEng</span>&nbsp;là cuộc thi thể hiện khả năng sử dụng ngôn ngữ Tiếng Anh và Tiếng Nhật dành cho học sinh, sinh viên trong toàn Tổ chức giáo dục FPT (FPT Edu). Cuộc thi được tổ chức với mong muốn tạo sân chơi giúp các bạn rèn luyện, trau dồi kiến thức, kỹ năng qua các vòng thi; đồng thời khuyến khích tinh thần học tập ngoại ngữ, cũng như tìm kiếm những học sinh, sinh viên FPT Edu tài năng cho các cuộc thi cấp cao hơn.<br>•&nbsp;&nbsp; &nbsp;Thời gian gửi clip dự thi vòng sơ khảo:&nbsp;<span style="font-weight: 700;">17/04 – 15/05/2019</span><br>•&nbsp;&nbsp; &nbsp;Vòng bán kết diễn ra vào ngày&nbsp;<span style="font-weight: 700;">25/5/2019</span><br>•&nbsp;&nbsp; &nbsp;Vòng chung kết diễn ra vào&nbsp;<span style="font-weight: 700;">11/6/2019</span><br>•&nbsp;&nbsp; &nbsp;Link đăng ký dự thi:&nbsp;<a href="http://bit.ly/NihongoEngreg" style="background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(220, 87, 28);">http://bit.ly/NihongoEngreg</a><br>•&nbsp;&nbsp; &nbsp;Fanpage cuộc thi:&nbsp;<a href="http://bit.ly/nihongoengfanpage" style="background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(220, 87, 28);">http://bit.ly/nihongoengfanpage</a></p><div><span style="background-color: rgb(255, 255, 0);"><br></span></div></td></tr></tbody></table>', N'Hòa Lạc', 0.0000, 5, 4, CAST(N'2019-02-20T01:41:29.377' AS DateTime), 5, CAST(N'2019-02-20T01:48:30.110' AS DateTime), CAST(N'2019-05-18T07:30:00.000' AS DateTime), CAST(N'2019-06-11T10:30:00.000' AS DateTime), CAST(N'2019-05-15T23:59:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (57, N'FPT EDU NihongoEng 2019 - Vòng sơ loại', 22, N'/Images/Events/8960901f-e236-414e-8a50-d896a3a2f7eb.PNG', 1, 0, N'Opening', N'Tại vòng đầu tiên, vòng sơ loại, các đội thực hiện một đoạn clip dài 3-5 phút với độ phân giải tối thiểu là FullHD (1080p). Nội dung trong clip cần trình bày được: - Tên đội, các thành viên trong đội- Các thành viên trong đội thi lần lượt giới thiệu về bản thân; Ngoại ngữ đã làm thay đổi bản thân như thế nào; Lý do tham dự cuộc thi.- Clip gửi về địa chỉ email của chương trình: nihongoeng@fe.edu.vnHội đồng chuyên môn sẽ lựa chọn ra 15 đội xuất sắc nhất mỗi bảng để vào vòng 2.Cơ cấu giải thưởngVòng sơ loại: Top 15/ mỗi bảng: 500.000VNĐVòng bán kết: Top 6/ mỗi bảng: 1.000.000VNĐVòng chung kết: - Giải nhất mỗi bảng: 30.000.000VNĐ- Giải nhì mỗi bảng: 20.000.000VNĐ- Giải ba mỗi bảng: 10.000.000VNĐ- Giải khuyến khích mỗi bảng: 5.000.000VNĐCùng nhiều hiện vật có giá trị khác.Cơ hội dành cho tất cả các bạn sinh viên trong khối Tổ chức giáo dục FPT.', N'<p><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Tại vòng đầu tiên, vòng sơ loại, các đội thực hiện một đoạn clip dài 3-5 phút với độ phân giải tối thiểu là FullHD (1080p). Nội dung trong clip cần trình bày được: </span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">- Tên đội, các thành viên trong đội</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">- Các thành viên trong đội thi lần lượt giới thiệu về bản thân; Ngoại ngữ đã làm thay đổi bản thân như thế nào; Lý do tham dự cuộc thi.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">- Clip gửi về địa chỉ email của chương trình: nihongoeng@fe.edu.vn</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Hội đồng chuyên môn sẽ lựa chọn ra 15 đội xuất sắc nhất mỗi bảng để vào vòng 2.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Cơ cấu giải thưởng</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Vòng sơ loại: Top 15/ mỗi bảng: 500.000VNĐ</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Vòng bán kết: Top 6/ mỗi bảng: 1.000.000VNĐ</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Vòng chung kết: </span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">- Giải nhất mỗi bảng: 30.000.000VNĐ</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">- Giải nhì mỗi bảng: 20.000.000VNĐ</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">- Giải ba mỗi bảng: 10.000.000VNĐ</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">- Giải khuyến khích mỗi bảng: 5.000.000VNĐ</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Cùng nhiều hiện vật có giá trị khác.</span><br style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;"><span style="color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Cơ hội dành cho tất cả các bạn sinh viên trong khối Tổ chức giáo dục FPT.</span><br></p>', N'Hòa Lạc', 0.0000, 5, NULL, CAST(N'2019-05-01T06:15:07.627' AS DateTime), 5, CAST(N'2019-05-01T06:15:19.533' AS DateTime), CAST(N'2019-05-18T19:30:00.000' AS DateTime), CAST(N'2019-05-19T19:30:00.000' AS DateTime), CAST(N'2019-05-15T23:59:00.000' AS DateTime), 0, N'; , ', NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (58, N'Chụp kỷ yếu js - updated', NULL, N'/Images/Events/08da3f40-d1d5-4fbe-9777-cdb8a9648101.png', 0, 0, N'Deleted', N'Các bạn vào vote nào', N'<p><b>Các bạn vào vote nào</b></p>', N'Hòa Lạc', 300000.0000, 1, 25, CAST(N'2019-04-21T02:51:34.797' AS DateTime), 1, CAST(N'2019-04-18T00:07:26.517' AS DateTime), CAST(N'2019-04-25T03:23:00.000' AS DateTime), CAST(N'2019-02-20T14:02:00.000' AS DateTime), CAST(N'2019-04-25T03:23:00.000' AS DateTime), 0, NULL, 20, N'$10^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (59, N'JS1102 Teambuilding', NULL, N'/Images/Events/0c260793-aeb8-4038-89df-a85cd1b80100.jpg', 0, 0, N'Rejected', N'aaaaaaaaaaaaaaaaaaaaaaaaaaa', N'<p>aaaaaaaaaaaaaaaaaaaaaaaaaaa</p>', N'Sân bóng, ĐH FPT, Hòa Lạc', 11000.0000, 5, NULL, CAST(N'2019-04-20T23:09:09.780' AS DateTime), 5, CAST(N'2019-04-21T00:10:14.547' AS DateTime), CAST(N'2020-01-01T01:01:00.000' AS DateTime), CAST(N'2020-01-01T02:03:00.000' AS DateTime), CAST(N'2020-01-01T01:01:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (60, N'Dinh Lan Huong', NULL, N'/Images/Events/7e6f2f45-edd3-4a2e-ba99-c040e532ecda.jpg', 0, 0, N'Rejected', N'2. Hãy cho các lớp skincare chút thời gian"Nếu bạn cho mỗi sản phẩm 1 phút để thẩm thấu, chúng sẽ không để lại lớp skincare dính dớp trên da", theo bác sĩ da liễu Fredric Brandt tại bang Florida. "Tôi vẫn thường tiến hành quy trình skincare buổi sáng như vậy; ví dụ, sau khi thoa xong một lớp sản phẩm, tôi sẽ thưởng thức chút café rồi đến lượt lớp sản phẩm tiếp theo". Điều này đòi hỏi chút thời gian và tính kiên nhẫn nhưng sau đó, bạn sẽ cảm nhận được làn da ráo mịn nhưng vẫn đủ căng mọng và hơn hết, các lớp skincare cũng được thẩm thấu tốt hơn.', N'<p style="margin-right: 0px; margin-bottom: 25px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 17px; line-height: 25px; font-family: " times="" new="" roman",="" georgia,="" serif;="" vertical-align:="" baseline;="" color:="" rgb(34,="" 34,="" 34);="" -webkit-font-smoothing:="" subpixel-antialiased;"=""><b style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: bold; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">2. Hãy cho các lớp skincare chút thời gian</b></p><p style="margin-right: 0px; margin-bottom: 25px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 17px; line-height: 25px; font-family: " times="" new="" roman",="" georgia,="" serif;="" vertical-align:="" baseline;="" color:="" rgb(34,="" 34,="" 34);="" -webkit-font-smoothing:="" subpixel-antialiased;"=""><i style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">"Nếu bạn cho mỗi sản phẩm 1 phút để thẩm thấu, chúng sẽ không để lại lớp skincare dính dớp trên da"</i>, theo bác sĩ da liễu Fredric Brandt tại bang Florida. <i style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">"Tôi vẫn thường tiến hành quy trình skincare buổi sáng như vậy; ví dụ, sau khi thoa xong một lớp sản phẩm, tôi sẽ thưởng thức chút café rồi đến lượt lớp sản phẩm tiếp theo"</i>. Điều này đòi hỏi chút thời gian và tính kiên nhẫn nhưng sau đó, bạn sẽ cảm nhận được làn da ráo mịn nhưng vẫn đủ căng mọng và hơn hết, các lớp skincare cũng được thẩm thấu tốt hơn.</p><div class="VCSortableInPreviewMode noCaption" type="Photo" style="margin: 0px auto 22px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: " times="" new="" roman",="" georgia,="" serif;="" vertical-align:="" baseline;="" text-align:="" center;="" width:="" 580px;="" color:="" rgb(34,="" 34,="" 34);"=""><div style="margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;"><font face="inherit"><span style="border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; cursor: zoom-in;"><img src="https://kenh14cdn.com/2019/4/17/a2-15555158954881764424967.jpg" id="img_c1266ad0-6127-11e9-b063-cdd32798dfd2" w="1080" h="1346" alt="Riêng thứ tự bôi các sản phẩm skincare cũng cần tuân theo 5 điều sau, bằng không làn da đẹp hoàn hảo mãi là chuyện xa vời - Ảnh 2." title="Riêng thứ tự bôi các sản phẩm skincare cũng cần tuân theo 5 điều sau, bằng không làn da đẹp hoàn hảo mãi là chuyện xa vời - Ảnh 2." rel="lightbox" photoid="c1266ad0-6127-11e9-b063-cdd32798dfd2" type="photo" data-original="https://kenh14cdn.com/2019/4/17/a2-15555158954881764424967.jpg" width="" height="" class="lightbox-content" style="margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: bottom; color: transparent; display: block; width: 580px; max-width: 100%;"></span></font></div></div>', N'HA NOI', 100000.0000, 4, NULL, CAST(N'2019-04-18T01:04:09.047' AS DateTime), 5, CAST(N'2019-04-21T00:10:25.453' AS DateTime), CAST(N'2019-02-17T12:00:00.000' AS DateTime), CAST(N'2019-02-20T12:00:00.000' AS DateTime), CAST(N'2019-02-17T12:00:00.000' AS DateTime), 0, NULL, 1, N'$18^, $17^, $19^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (63, N'Tổng công ty điện lực Hà Nội', NULL, N'/Images/Events/55b5cbca-51aa-4779-a32d-46e563150d77.jpg', 0, 0, N'Pending', N'Sau khi kết thúc khoá học tại Mỹ, Hoàng Thị Minh Hồng ấp ủ kế hoạch "tham vọng" mang về Việt Nam những ý tưởng mới và đột phá. Sẽ có hai dự án, hướng tới hai đối tượng khác nhau: giới trẻ và cộng đồng doanh nghiệp, để cùng hướng tới mục tiêu phát triển bền vững và giải quyết các vấn đề môi trường trong nước. Chị sẽ bàn với trường Columbia và Quỹ Obama để nhận được sự hỗ trợ cho dự án này và sẽ cố gắng kết nối các nguồn lực ở đây để hỗ trợ cho việc thực hiện dự án tại Việt Nam. "Tuy nhiên, thử thách lớn nhất trong việc bảo vệ môi trường ở Việt Nam vẫn chính là nhận thức. Mọi người vẫn thường có suy nghĩ, rằng các vấn đề môi trường không có tính cấp bách, hoặc biến đổi khí hậu là chuyện của 10-20 năm nữa, chứ giờ làm gì đã phải cuống lên! Mình hoàn toàn tin là các phong trào môi trường xã hội ở Việt Nam cần phải hoàn toàn dựa vào giới trẻ. Giáo dục các bạn ấy là dễ nhất, chứ thay đổi nhận thức và thói quen của thế hệ lớn tuổi khó hơn gấp vạn lần".', N'<div class="VCSortableInPreviewMode alignCenterOverflow noCaption" type="Photo" data-style="align-center-overflow" style="margin: 30px 0px 30px -160px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: NotoSerif-Regular; vertical-align: baseline; text-align: center; width: 980px; color: rgb(34, 34, 34);"><div style="margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;"><a data-lightbox="img_0d9d4bc0-62f6-11e9-9109-7ff2eb6418e1" data-thumbnail="https://kenh14cdn.com/2019/4/20/e-web30-1555714446267921272309.jpg" rel="nofollow" style="margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; cursor: default;"><img src="https://kenh14cdn.com/2019/4/20/e-web30-1555714446267921272309.jpg" id="img_0d9d4bc0-62f6-11e9-9109-7ff2eb6418e1" w="980" h="401" alt="Những thủ lĩnh môi trường: Mọi người vẫn thường nghĩ, biến đổi khí hậu là chuyện của 10-20 năm nữa, chứ giờ làm gì đã phải cuống lên! - Ảnh 3." title="Những thủ lĩnh môi trường: Mọi người vẫn thường nghĩ, biến đổi khí hậu là chuyện của 10-20 năm nữa, chứ giờ làm gì đã phải cuống lên! - Ảnh 3." rel="lightbox" photoid="0d9d4bc0-62f6-11e9-9109-7ff2eb6418e1" type="photo" data-original="https://kenh14cdn.com/2019/4/20/e-web30-1555714446267921272309.jpg" data-mobile-url="https://kenh14cdn.com/2019/4/20/e-mob20-15557144462411402407312.jpg" data-mobile-width="750" data-mobile-height="728" width="" height="" style="margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: top; color: transparent; max-width: 100%; display: inline-block; width: 980px;"></a></div></div><p style="margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: NotoSerif-Regular; vertical-align: baseline; color: rgb(51, 51, 51); margin-bottom: 20px !important;"><span id="docs-internal-guid-4e447324-7fff-6593-c4e1-8dff50c6c389" style="margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;">Sau khi kết thúc khoá học tại Mỹ, Hoàng Thị Minh Hồng ấp ủ kế hoạch "tham vọng" mang về Việt Nam những ý tưởng mới và đột phá. Sẽ có hai dự án, hướng tới hai đối tượng khác nhau: giới trẻ và cộng đồng doanh nghiệp, để cùng hướng tới mục tiêu phát triển bền vững và giải quyết các vấn đề môi trường trong nước. Chị sẽ bàn với trường Columbia và Quỹ Obama để nhận được sự hỗ trợ cho dự án này và sẽ cố gắng kết nối các nguồn lực ở đây để hỗ trợ cho việc thực hiện dự án tại Việt Nam. </span></p><p style="margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: NotoSerif-Regular; vertical-align: baseline; color: rgb(51, 51, 51); margin-bottom: 20px !important;"><span id="docs-internal-guid-4e447324-7fff-6593-c4e1-8dff50c6c389" style="margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;">"Tuy nhiên, thử thách lớn nhất trong việc bảo vệ môi trường ở Việt Nam vẫn chính là nhận thức. Mọi người vẫn thường có suy nghĩ, rằng các vấn đề môi trường không có tính cấp bách, hoặc biến đổi khí hậu là chuyện của 10-20 năm nữa, chứ giờ làm gì đã phải cuống lên! Mình hoàn toàn tin là các phong trào môi trường xã hội ở Việt Nam cần phải hoàn toàn dựa vào giới trẻ. Giáo dục các bạn ấy là dễ nhất, chứ thay đổi nhận thức và thói quen của thế hệ lớn tuổi khó hơn gấp vạn lần".</span></p>', N'Phố đi bộ Hồ Gươm', 0.0000, 4, NULL, CAST(N'2019-04-25T14:23:56.563' AS DateTime), 4, CAST(N'2019-04-20T15:03:38.483' AS DateTime), CAST(N'2019-04-20T15:09:00.000' AS DateTime), CAST(N'2019-04-20T17:00:00.000' AS DateTime), CAST(N'2019-04-20T15:09:00.000' AS DateTime), 0, NULL, 10, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (64, N'Event to draf', NULL, N'/Images/Events/3b6ba164-0ea5-459e-b354-4b06d72044f6.png', 0, 0, N'Deleted', N'aa', N'<p>aa</p>', N'Hoa Lac', 30000.0000, 1, NULL, CAST(N'2019-04-25T02:17:30.923' AS DateTime), NULL, NULL, CAST(N'2019-04-20T02:22:00.000' AS DateTime), CAST(N'2019-04-27T14:02:00.000' AS DateTime), CAST(N'2019-04-12T14:02:00.000' AS DateTime), 0, NULL, 199, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (65, N'Demo final test', NULL, N'/Images/Events/73ec86f0-4e7b-4ce5-b611-43da541663fd.png', 0, 0, N'Deleted', N'demo ', N'<p>demo </p><p><br></p><p><iframe frameborder="0" src="//www.youtube.com/embed/j5jkzfg6YtA" width="640" height="360" class="note-video-clip"></iframe><br></p>', N'Hoa Lac', 200.0000, 1, NULL, CAST(N'2019-04-21T02:14:16.203' AS DateTime), NULL, NULL, CAST(N'2019-04-23T02:02:00.000' AS DateTime), CAST(N'2019-02-20T15:03:00.000' AS DateTime), CAST(N'2019-04-20T14:03:00.000' AS DateTime), 0, NULL, 49999, N'$2^, $4^, $3^, $8^, $9^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (66, N'to verify del, date', NULL, N'/Images/Events/ec2edc19-94a7-4022-b47b-39f10c1762ad.jpg', 0, 0, N'Deleted', N'abc', N'<p>abc</p><p><iframe frameborder="0" src="//www.youtube.com/embed/nCEPP_uS7kI" width="640" height="360" class="note-video-clip"></iframe><br></p>', N'Hòa Lạc', 200.0000, 1, 31, CAST(N'2019-04-21T02:53:15.063' AS DateTime), NULL, NULL, CAST(N'9999-04-27T03:33:00.000' AS DateTime), CAST(N'9999-04-28T14:02:00.000' AS DateTime), CAST(N'9999-04-23T15:03:00.000' AS DateTime), 0, NULL, 20, N'$2^, $3^, $5^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (67, N'Ngày hội VH Nhật Bản', NULL, N'/Images/Events/2e892a4d-7afc-4965-a8bc-3268e9a1d9bc.jpg', 1, 0, N'Draft', N'abc xyzMột số hình ảnh đẹp', N'<p>abc xyz</p><p><iframe frameborder="0" src="//www.youtube.com/embed/DNrNBLI3d68" width="640" height="360" class="note-video-clip"></iframe></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p>Một số hình ảnh đẹp</p><p><img src="data:image/jpeg;base64,/9j/4gIcSUNDX1BST0ZJTEUAAQEAAAIMbGNtcwIQAABtbnRyUkdCIFhZWiAH3AABABkAAwApADlhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApkZXNjAAAA/AAAAF5jcHJ0AAABXAAAAAt3dHB0AAABaAAAABRia3B0AAABfAAAABRyWFlaAAABkAAAABRnWFlaAAABpAAAABRiWFlaAAABuAAAABRyVFJDAAABzAAAAEBnVFJDAAABzAAAAEBiVFJDAAABzAAAAEBkZXNjAAAAAAAAAANjMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB0ZXh0AAAAAEZCAABYWVogAAAAAAAA9tYAAQAAAADTLVhZWiAAAAAAAAADFgAAAzMAAAKkWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPY3VydgAAAAAAAAAaAAAAywHJA2MFkghrC/YQPxVRGzQh8SmQMhg7kkYFUXdd7WtwegWJsZp8rGm/fdPD6TD////gABBKRklGAAEBAABIAEgAAP/tADZQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAAGRwCZwAULUUxSEJVR0lFV0trTVI4Q2tILVYA/9sAQwAHBwcHBwcMBwcMEQwMDBEXERERERceFxcXFxceJB4eHh4eHiQkJCQkJCQkKysrKysrMjIyMjI4ODg4ODg4ODg4/9sAQwEJCQkODQ4ZDQ0ZOyghKDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7/8IAEQgC0ALQAwEiAAIRAQMRAf/EABsAAAIDAQEBAAAAAAAAAAAAAAIDAAEEBQYH/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/9oADAMBAAIQAxAAAAHn5mHj6S6E0x6+o54cuhfNToYuVk3Ol1fKem9fHqcvYHfh51fTzeXvhmhPPQgdTQXJk3TjkdPVwJnXp2+VfOnppx2c70Oby0deWhYHvMllqAt+aWSraMwPIpLynqPL+omrqTIoMaIhmoUGYXY3VwYFVSWS6SSRLlRJRqaEbomzH0pvVw9uDGm3J051LgfoPMROv59ZJkkLUi2It3U3NLk5HV5Ws+pZc5fQQ4Suugnn83r4dnPBWuUqVZPSed73fl0gsO/HIjUnh0zo11z1hDYE1ijQzTgWBGqKuqJLokhFMBpckSs2vLNLupNGxbyiI82vS+c9GEJ1kEZWaEkasl2HBlhWNxVEIUGBSQkqJayGhlSj7XJ7XDrx8rR3m7qdMXKo3djj9STj8no8mkEMqDIvU5vS5suXl7cO8+w14Jx99qJbdczscvt5swPHXmVJLJ3vPeg9HLcJV04KU4OW0U0caqHaYcnWxTWEjDO2LkkASi3UsowMu6tIVWTPoXKpbBmmOxlnUYrapek5fQydFkFYGXJM6ATgBXEglKqiqKkilKpJJSSoK0scXSeg6nOf5+nGLO7rhlxVjRw1Xc0JmZy8jj0widirHRK/MwTz67m8+qEh4/Ri3RVyMZwZ/Ry+by07yunHh9xU9Hn6Q2HfmAXXLVWBZXKixLhjmo6mKbRdSWwKKJSRdSJcoiyEiLpUrRJ2dgTt3Lrk7Gl/PU8d7LwHTHqn+d7+sGQwKS5blWVJCrotBhVKJiUkphNJrWUmFPSRXG6rOgmaG3O1mESczrZq8/zvQ8bphl499juvqTz3ylv5+4vXyz1NUzEca3AnpXBfn+kQ3IQwa1drFPnlBOy2coLZ6vHjdoydcMXIg1clKSJKuShm1UnKDZkz1Eqi3UqS4NBWuDBWajNDJsNejqcOqOkTeO4FZ4ry/pfPdscjrc8OvL2JYu1m472XWKaxazTTMzNNrF5x6IJZFxqDNUumZhNMzGapihrmZa6bxuTTeWw87c1iOpmaXmrGLzPydM4VuX1i9ePZcOxdblY36CCzy/ShDpjJCHVf0uV1M8bAlzkODeO5y9OpfbkkRDrwKS7KhLuSklgwpKjF0c8c+NHOwC40NHUDDYomzpY6YOxr08OqNtlz3FM50hVWnUX5X2/N3PJH6m+vLz3pgZLZCwWVBKQqQnYvIQwVIlblBBpPHK1UmDH4mQ4UhWxEVGmZmjl2IaLRXRDOsgxNis2tW84gOdInpI0p3/H+v81y117TfP6eqgvGSQ9aj0Obpc9q5rcc2nReQCQiwdVzgz9mXPDzehzbzxp1w3OVOiVcudWtOOHYvLhL9HJrzjPRMxeDq67c6wbmHz0DDqS0pSpyMCcDAse/B1zBGaxYRVNiljwSI4Fw1FlsckALVaQ7C6IqpCGVK0lpTUjO41MzMU4AyDVjoEGwiAwMXQXqYUbw3mnq6mNrz7Jly3uXn6RrVMnaMwD9WN0z39GVmPMaBqrhEgmSkZnAKMKgQwgSGVBhQtlmLj2CrJeaRTZCTPnmzHUpbLsspIJ6mh8rqcnpkbCdMRLgAsRKjMyNFdKylFYYxQShTWxV2Q1wZcGAsbWiXEceRhoLKRpU2SpOSyXC0BZjM1m1ZdXXrtXLel2fpHn8myZ9qWNhnKOuhNDbdevmjh37zMz5m0oGCCxslXRJJBVJZUKWi8nSIfpHUzo1jKl7WYoiWJVgVUMu7ER1wtlyVrkipYW8zrjWAq1htZxsYKCG5houFSUVWkiytUDU01mYhw5yhzXqmmJpqYBYNjGrkaGhTTHI1GcdmaQoMJCbSk6kj86kTW/o8jXIotQY9cW9FKz6C0yXuztMitGPQHSxFc64JZ8UlnrC61rRE0MXJe+9ZxlqT0xo08rfZrBQFgFYrgDLy2axm1wjxpbhwXGuI0rHWGOkzGsrm9TgdMvDEXTmawpDgXYRqtWKlQd5W2FIYC2CCJKh2rDqNR89jXQVejNxYOpi1kSS0cea13zNph6GLgKNVjGZk1vHM6W6fJcj1idWkac+lg6G645C16LnGjoIs5a+sOe+ZfRubwtdj153JBV5btnHh6F/ntNvaRiRqakYxs0DlHneozkut2AkMtCs0t1ig02UrJmkqQfu5uzGmwLxsVnFPznpPM9YoLHpxqqpLIRGDcBYsgY6wzXJbMikTh6qqxG7HWmU8M1uzYNMOaTGWJaeYaeZR0148ZvzZR0NufUdHocvbi9Usz+e8iN66j1Te2tTeuOkueqzpZsgZuwucS77xkmhCUppWul1CokN+aprpBiRGqYZrO7MBrobmbmlUz3JsQyrYoCxq5XwDmnPzFjTapubbBOW/J+x8T0hpMOvKquIZSEG6BOnBWUlA4SqetSaZltH4tLjk7lFRUuFvyMHL0YxkyNpEEwVNzWbKzhRGpq9Dq+f38729GLRzpiUdBos/bma8imXooBhoIY3MQ4qosJWQFkNdTUtQxCNMsWZmKmpstq5D9OErehklBxdIsqi6TyHLqtJDnJmNaLXJrSxejFLwfvvn3ZdSuvAiWwsjuBZCFtLRKsniqlsiJRsBMdnjpjsKa6+NIhksw9GbUa89b44pehGOGztsOHyPUc+uLPRp04OrpHGVOtEb+lx9uddK1txrCgR781LoBhLKDIGgwiBYqDqWOVZ9oKnfzno1QBWpuRmbpSxCOTV0mKqtL8bozK3ZtBSkDZs4uiXts53QzdEZM1dt2Zt7Bby6j88+hfPu+RE29eC3GAZCUS6OCcslfVpVdjmTWnPh02411cmab0IbrNfqz9iOS4tMbm5BiI0YDT3fNvO/wAjrjLkhIUeb2Rs4c9KBw9erNm7H4dMcWKnXJSMIZUFcMCGEChi6kxga3Z4bE3UDC0iG1WWrOOoxM10qNJdAzc3t8w5ebc3TmbNpmKb6EN1vxQcpsrmp0Y1rEq5dA8B9C+d+jBGrT04w2Eqa1jIhkGiZnSa1Jyq7DVbldHnsR8SFBJZTlb4DQ3Jiumdpr2c9hu5OJNalyHW2r25FpzFmjk6UrCeITot5u1RjZm+au76ZJ63SQ6bba2pQKHPTMykU6tEpejKpeovD282aceHOeqtRGlKCLzpDTcWZUevz55kUzjkY1KW1i6NmLbK1+YprQayjZpwaeWz8B7/AMX2gPZq3hBvULRpTYrMzMkUZ0gN9HMnVquXXUQY60LsXD1E6SBxWgo1RqB0kW9hxZ3215i+nz66PT4/fzUHozZrXYyG8PtpueFt5paegZztmNclt7NMzHqRQChHLzhoWbpal8+3uCcYu30U8np9Hzl5xdnn5BmN6TTVFKZDPl6TK5j2qrZENwPLpA583hpmmwDXFOzWR551l2KrDY/BoXoeP9RwtRWwi3F5mLliDx3Ii1liNKNFW/MyDVrRS35M0drnJzGoVqpor1iRO1swYlMWyI5LK6GFuuXjaRJraeYs6PK8bkGjnuXcbcxMHU5GpejpcrNXiedc8ugRi2ixNKGJmsU6CkRpzLt9D59K9ZYALlMVVclIuoVgN08zbG/K5cZ6fmoWZpWukWNfjOG3lZGq+Za+k28Pr87dNk0WLTm1lqougRIg25VklkJtqAXZ2Uw1kIzbRjm3uVpfPMq0WyoBZANhgCIhW5me46zEszrDrLM7EtAmksRXGqsa2OjfP0szPvVb0lkWNVUgmmURgqRlY2gkOYICXUqSylMXZJZUu9mIoQaCxajXSjL2cvQAnRnNDcrY1lm0Rqbz4dKuPmO2XB1r3G81+NbM+3FZEMBEmESDWirqZoagiBDRVGqSJl1CXnfm0FosWs7lgAxFnQU/PRHRxl1Z9JscA56XyGZLp8WOsMpNI4ktSxI5Gas2uXdLLNqiVAiVUCtAmdi4MRYKKKDWYS3JBi6c1nODy7WVmG1inFBhDUKVqz2iVprQ7MaaTzTJyXHWMdFCCkHuHVis18TqK1BLmVupdR+VtQ4qGqEYZM7AigrGqRbtzsyJGkqyLGqCA81g6hawYXqFQ8AxtIBR6GIBYy0mjGp0ZDGrhrsrztkS8bkqhwZ1I2EKiEKy11ngMmk9ZRGFWbctkI1ikLCU0RpcBWfWBGiUuQJWqIlFW5UmNB5dAVxg5+SQlVytGrJszdhpfmqU6rFC3KjgSyoljasSTGlT8EbEiQKmlqivcnBaorUMNHMot+XcRRoCcDhIDRMbkVLFlJW/NRNA4JizjSS3ZAQul67nTOlBoAAazDJCFLNVmISHQ7Wm5dM2utOV2vNRzma7M5jiN/PqtC0IaGkFFwatuUIY1EM1Wajx6I2My6sW8XXVHJZoVYfWw9HNme8yuTpBkXtKsydKBKd6KENFZMlNaz22VeDXgsmfWFIUGux7B1xlByQJULVSaiDWPguE3oghXQWZmVBx5jh3Rw9nGtsWebS9A1kYwzMFgMyjksINCtTFm259QeqPQzaya5m4c7Of0zVhqEy4UtmXS5VVclF3UKqQOpIM1MGacFx3GcR+bsUqQ/ocTrEplZWxGfTXWVyNAZBlKigaCr0xdjENVKOPdj1LxMz6O1r0GmpmyBYyiVABTYVDBo4wYVoVcbRjY5qtebQG26Wd3N0OOwbY6lDULRICt2Qy6KKlL0LucIsvUmlmMLmaM9DZXRmooFdylLZWglLKFlQEuqkugrqJZAcpw5lGiUOWMouzyOhm2nKwJDdKE3StrIzQtGqYWQ2p6oQ/JZpwrGtOaZrFsXssbtW/NyLoqyQH1mNTDODVBaUah1iUA66JpynKWbcoR1Q2423VRc9ZhGdMEFxMc2SXmN6QnLHoo1MbNHPsejNdkKlgKYGgDIQqXpWjLrWJ1yXHWsGUjrU1lpqrmjq6Km0LjKgrBkHdTIxNpm34exGUtoQGsNE0CiTa+UYpgBDMjcdy7HKpUsdQasCdTmazShSI6XMoKa3MYolwZLYBszaYG8+kkiyzU2a0bm6Oel7c2zGgaUmslMz9OVFmE1hkzV0sWRWmpkYgZRNKhFqrqtBz1tz2ANDqEu6qtmPZNsMDygtpF3caz5Ojksz6AfcLMWAWRZKjIC1dG4+dpzXdnz3oZc+jMxGwamrQ9geHWhKSxSllZGVKEdSgJVBCug3pcYQGiwuizGFSyGNTsgLSYtlQICEZvV1ue9L9FcuiWUSzMQBILD05kOiWYZr52pa8aNR2QrsSTA0hpKOpnzKDEaopR1BugNKNE21gNxTjCobfqxrMPSyzpl5PS5m+C2rbcFDsqMXmhGBYxqNcqeylEotCWaZm15rWJeq85qTJCFINXqYRsKkC6MB6gzj1RRwSjAixKEKrKeo4Fg0HFDRsV05el2cGzh12TPJW0yRjDqhXl9YK3zfj5yNR2bae5gJ6bGihoEaAkG5qg3KqSEIYWLKAas2tD8unPTTtDoY0A5MM6bMeOa5dzm+r8o5CGw7nmloESralUXYo3Rla1o14+pm58m1cyS6Sa9fK0VS3iEoc1hoqC0uyaFYaxiX4C6KFSCFYQYFQOBYcGDIBQJxqs669/PW01s57OR0DVhbuoTjyHN6OXvyy9BhwKGZykxeo6Zl6MylEFlhQwpUkkQDkAQzSQwH9jgbM9uxm4nYzec3WVg9TlyZ18oQ1z6ojeVsxJmn5aVqaGZKrdePTms6fN0w8ohDzNXYwCXTc68poo8xH5XVmSW8DQvAGYNIpwiIayxuAkNhSQOCQZVcW6nrs7XP6PHZ3DzuPQyLooMdm1y+Ogu9HIsUwIalL3Dq6S6o6qqUFVkSQyqKoqqskOaBJYmaE0GnNcu7XyzzezzstIxtdaMw6VQnn7MWoNHALIwWxit2ZTzWJCWQlWjFrsEBaNxvz1bXuByrqAIpQsVY+CUTPpTSJdElwllCXVhGL4Zvz9TGtWnPo5dGQDUHJbFmsi9mLRL4qlD6OK0a8uoi4dgwgDtZirtgBvZLmDQmhXYWHZSJZQox0mYdYLgvUaZGbLMjyYBrU3NtB5gMVTUM5YNwlNwszTHQukQhQYQWhcuRV6mIklJpqHDAQxCJbQFbAMpiRBJQI3Kq6sYQGBZSC3ZN8uzo1o5dIYlnUOmQjQrQJNdguzsl8joFffkjJpVrIMZViValULbOXNqz7IY2hzrJj6GTcz3c1GEk5DJZDjU2GEElq5KIhGSAUtvRmZDuZrxKmGWpZUUgHbZY9GmV6tOdQRqVqIB60MlARTlli0C7BgIMipl0zoPMwWD0xSjDQKuFFRkO4WcfAbFasa77s7ufRZSZR6CaLRkbYspAWAEeYU4fRzQGgWbty5chhqsUTpbiZEmskFm3Rak4yuhz9yDB1Ddmhqfz25u6spQ9+M6Y3MUr1jkk0lhPS1yU2CcRqnSWT251l0sOGZd/P2IbYIAxFgy0VTBlg3QMqrSqqZqDSCEGmxREAoBchZWZZg6KbGZu5y+lnYbKPnoZJKV0Qty7oxMAKgnnqt3bnmeTISjbijLqQ/Rxq2y4cfbxM4CodUyE4Lmbce8oE63INyLlXBDVaaYqsN+dbhVSiWBhyyWEdy5tiGSbNeTTjTHgEuni93zus6GJDYqVB1qiVSDhiLWGIBqNBUGiIBQLo5UiGFl3GAMHRA6l7M0NZ6M6vTZY20WVKohIKAcSwluhZgkWQnnNCy7YZnZkjWglos0tsZryCvUmLpZcTL0slZ2oZqOxPqzGDg1AklQSgMKHT5kkSWUUUhUZFIxKU2ZWjm1qxQ0ASsao8zT5v0nm92BF6zJIkgjVyloxZgQLoklEq4CcmkKyyGGRZEeat9NU2RmWjRjKa6TMT86eQHmySEYo5RkFNWfSmgMCOAppdua178kKC02xlym42IZLqcVh1ue/DKtta9TABqo13ETdzUGHRBaMRblUV0cVIyWmrOUyExQaQQ35t+ao3omnPQ/LT57u8fbOLR1FRlIINGkCyJQNFEw4BDoGWRREYBmUCZXNCyMlpkbFHVwwYCtbjbHWZgdnWk8zZWQGwI3RoSxdQhhwtOUe/I0gmUGIcEEVSyXVjHqctZ2Z0ezKwA1FUOOjDbF2BZVZRBcMRpRVvy61GrqWyGDjA81oPGEbFMNIGWS3Vcpc3oc7SKbWosTgsHDZmJtCaYQgdICo2xJPGFsIyrIgYclFoMg7kgrAh2d+cuSK8hXGtmFpufztGda7Uc1qzNQWSzTzq1j6OVispRNRoarpRFqtRixIgGAOnO8WJQt2aI5DASXChJVKaohF6FMUquILYxVsoc3ZoyPVoMqGWN5rIFD+Z0fO6dBamazLGgoMUoIhSIkfBOqsqlkq4l1YcKSDUi21T4kICFUVqGJsOQF1DUijWQx+Z01sdl04rlsgEsF8wGlPo4rE10EukFqTU8rQoSCIcklK6kGJKSiG7CC6LKWoCdJYFSkVGowpIwwOUltKVGvNskZTRmoQiEwXwXk/Z+M3CYk941WEWQQVyYhGrGkZqwaV1UK5TZmYNZnZD4B5q7qw3J0QIksOxtTS5FhUSx1ja3YyC0ZXy6tWHTm7IFywDUczD2+X354wcIsXRMsO9Fo1Z6GFGZdHLVURYWtDuoSroYQGog1UlgVUwwNbkZm2VyI5T4E23NaEtQDAKtHRxdLGnfPPpPzXpmzBu+ZA3PoYQAxCoODcRyr0eqhg2KuHOzapWSqxqyG1ewSkESgJQiLYqrkinUuSmLK2nKKVunG06OjJo56CrJQ5/ST0nLX0V2c1e3NrGQilABSzPR0XckzJCulqeq5KRsqrKiGByQDjQ02WCYuVbh0ZVdzFhw7WsSWa3Npz2JYDa27+P0eeuj85+g+B6gKXvjoyODUWDlFXJLKuRZAzSEejJTHSaQZTKS5LRXFYQkCV3JKNQaWqCkq04JlwSgWCRDBqu2c3dnTrQ7IxMZpI7X2cTJ2+dvPMXvTrOMNiNTOGkNEmZyJs4qweEim2YqHYB2wTTqWowCiNksO6igtY612ONEGqCxhVYfQw6ca6Xh/Y+U0zQq7cqag6tWhdiJJlVw4o5YelL5ThDjVSQtZCEVHLbAOmQSkgxasS5SWJDTZQqRLMsguCNZF9DFqzq3Z3y6WD0ue4GomcfN9JxNzj5uzl3OSLE6yK3Skx1CqfDPH0JtsRJNMC2CowygVarEGUhQrHUKCQwhKWQKQikUmqarGJkuvmbWR5/P1Ob05pKXrMqwqAdRZWQLbdnUMiiCYyjLEqigRA1SIpAxgWAMJYDVpBYEHJLROUWVEXJeab1PUXofl2ekl3PV7SxXOLKyNDj6WGuNyezyO2Lgr1l0QJtLM8kgykS3QMbURZDUWzPWs8L4cAXWA6PUjFPkYs15oELAiFkt3LCkKaLp8zrS87heo4msckdSemViV3ICwQnEctsB2NS7uAq6qQgUSuA6EvLkkWDBBWyqgMUHBNLG6iEJWwhKLuiD05X50T0Nzr1e1G3OR4jMjVvC2qxa8VnK5XV53TnlG66ZGSaM1ZtNyqqXKWzBtxTQeWXWuAPzQLlVovZ7ENkOUcouEs6gwA2LZqEYlnUZJlCktLrcbu5qMO9JycHe5+sccda+mc5kYTLPNsoyBKEueMCSLNVp2FjGqODlQYF0DVxDS9FsKSSCQqV0VQwbAlVjXJ0TUOTN//EACgQAAICAQQCAgIDAQEBAAAAAAABAhEQAxIgITAxE0EiMgRAQhQjJP/aAAgBAQABBQL7aYiRUmQ0COnFYc0h6x8rNMlVOA4+BSkha0hfyD54ilHg5JEtYnNy8i/t6fuUuv8AXDTkoyjrada2rAk+DNI1vX+ROiUlj2RisOVEtUeoOQjR/VvEkbSvFbQtWSNP+QamsOcnzeV/f04k2R98HHcPRnShJOXHT9axqdQKbNuIoT6lqpE9YcuGl6GPNFcF40LLwv769w9awuOj+zXWrFEuMfT7lr4vNjkSly0vXPb/AEnlCiUbRR7/ALOn79Gp+3HRHLrVkS4I+oftrP8AI0/2lFDwx8tL1yoocf6MswZKRYpkdT+zuNAn6cvytcdIkSeHnT7c/UP1n+2NzLwicXdcPvR9eFor+hXCiMLIfx0X/XlKi+9Hom+vjkQ0XVY+UWqafqbJe0qWJGiap+un98qslpnxs2m3Gj68NEvPeKNhtRDSNPSEiOq90Zbv6r6XbelopIVsWGicGakajL9ofyJIl/IZoRczYSVyYzS9T7lrfpnaz0PCxtHA2IegQ/HxtDXjWGLCQkQ0myGnRWGaWpTi789FGw2m0np3GGlzkiUScGKJD+P2orTjOfX6kmOQtahTTc/yNmE6e4cisq6xtZF2Tji/FJD8u3EYENNEY8NWGNHV7Xjo25sUiyzcWbizcXwY4kIdxSQ5WWTdsl28QkM+8KPWY42lEk4uOoSimbZLxteNZ09IjEjEQ2NiLGjs/jS3LbE2o2YoooUDabM7jdiy8Wbiyy8Jm43FkpdQGyUh4lIl6rGmhwltynh4gVwnpinKAtSMjaV4X2NeOGm2R0iKNuGy8bSUJDTI6TZpxlEjlZTLxZZY2WXm83mxPh/pDZ7OiUrNrt+8R6Gv/J+08fe3pp4XtcfjHoI+OcTdIWoboioeK4OJtZtK40RgQ0CEEhIWGxsRGONqNiFGJtXC0N5jLpSLHIbPvcbjdhG4vHZP3wbI4vFNm2iUqxI0440X+GvGp0IcelI3Z0/V4SIx4bIktFH/ADnwyNsj8i5G6RuZuZukdlSNkimbWbD4yOkyGlQkJYQ5YojEXgTxZZZD0WMfg+kbxe1hFjxYnljiJCVs2s/j/t/LgOQpdRmI2lViBRGAvA3ikbUbEfGbEbYm1FFIUIi04mxC0ylhLO7FYXC+V4vHosvKxYuCLI+xcPvgh59LTuUqNJVP+XH8WRLo3EZO5PESMfC5eJLFCWe82O8VlC5fd8Fi+DkReLP9WRPRF4XDcXi/AzRLImsrgOY2REsRI+4s3ctxfiWFHgkIZu5rF5vheb7svP2Jn3WVherIl92LC7KK4tZZpx/H7R7iNdxQukyOIm5ohJMrhfjSEjbhiTFHL74qzvCZZZfCy8X08euaL/J/s3STYjokiXRH2RIx3HovFPjtxSN1LcKRpS6KRtKNohvtYT2kJ2i/GkUKJtKKFErFjlxS4RiSwrNw2bjcWWbuH0WM+uFWRqse4zRHMWexbSLzeFw7FjSf5bSMe6z97bxEoj0KXjrEUKIkuNjlwopI+yhIvhuN2LL5KWL4feIpHR01Q005FH1FYs25lWLRuIuVZ9H3D9q47BwNh6FhZQiijbYtM+IUM7qIyNxZu8DdDmQwi+F43cryhCFHG024iejd1GVOMu93bJ+0WbiNm4j1hvG2NtpG5CmK2bWOLypY2dbDYKBtNhLSIxNhsZQsbiMyMolojJG4sbRY2biMxsss3FlieHKhys2kBF8Fh1wvK4IXqz7PRWO8xjI9SJoawo9YvuMiepE+UeqyxEEQIokOJREQmdcqWWXw3EZikbzcxyLxuxuxedxE3DeLI+ZPgkbcxoo2s2kl39xe4gzezcbu3iNZ3G43F3wj7RFkZZoXvCLNxvHIUu95uNxuNxZuzZGRuNxKRFkpFkXiyy82bnhYjjdlYvkh+sL3hG3O4slIlEifW0TPtMkWRseXjSh8ktWHxTxpzI7RZorhuN5uLLLLw34ZWWXhHQh8OsVhYjeK4R8ceHvN3h0dG2nuGXixfqbjd1iWPoWI+tOdCkXxslLKxZZfCxPk+8oR0dYePWEdZQsdZXFeHauDx0MU6NyLyhJt/qnGzZIWlI+OSIqnOH40+Ht1196UyxZlI3G7heFxYiNFll5fDosTzvWbEzvKxZEXJcEiu0jvDrjdOUsKTLwsbqI7pNROjojTHpI27Taf88L/AOWJ8EIxjtiTmpEUtyX5RkReL4LNG3k1iPvKzebExSN3U5s7FI3MjMUjcKQpm43MiRK4rh94TIm7PY5Ub6G+8ViJGPbNKG4jE9EmzcbjR1FKM471slEjY47iejIelrW9HUIxZ2RI4tFofBcGXi8OIkyzeXixD9qsP2inhbU5yNx3iLIkSkbSMEKMSK4orC4JcOhnyjk3z0/aj+K7fSLo7k6slGV6cVXzURla/GQ9FxPyv5JI3TbGoSPhiOMokGxG4vxbqN0Tcq3ikXxWGdn5HxSZHSoqJq9DKYj4j4T4oo2IiRrCIoXJZ74KizcORueNOGjKEdPdPV0ZaGpmPvuJGTRF/lulcO5xjbnqR0yetKRbGyOptjC2bnhxZ2Lcdkd2GhZoWKFHk8Rh1sI6eqyUZRdGw2m8qcxQo3I+WhfyCMozHAnpah8Uz4z8U95dG7voVG1HoUiPC3hCQkVw3G43EpcLxefuMSWlujWrBYjKmpbSb7sRVmlCCFGIqEyyUCW7Tl8qIu2pxxWVjvNs3F4cizchTFJG7ETocRRgfIi5McZY/UhquMlPrczcWj7pYtFnYnReFIUsPFISF2dYSzeaKfg0Y/k6IzZvIoo2m7aTtt6OoljS9/ljoRuH+ROGw3EJMUjs2iFhYt5chyxB6VPbdFMjJmlPTJz3G/a1O8dG4eobiinuhajuHnY2JUOKFubiy4nZE2oQhIpiRsK6x3w2mw2FHxqRD8SWmKCujayMEJbVZuaZE6LR0KhVU/47s03UozTGjbhTPxJx6a7jIhM3FCWKxeGxyLxWNrxGLPis+EpHdwi2bEfGfCP+OfDLTE2KSE5SKOx7jfIUjeyMpEZKRtkLflSxGyNIoURRxfZLG0SEdFdbD8kRX5Q2qU9OCXxMVHQ2exx6g+twsoi3WnKzWh3tIG43Y7RuIzsnpxmOLi4So6kRiKNYkN4s7ZH+PKR/yQP+eCPi7j/Hju+D+Ehfx5G6n82pKLxE3I3G4s3ClY9NFIjLafIbkfkbTabUKB8RHTmLej8JHwnZYtxEUSMcNiHlRx6Ed3KWxbHU/wATset+PyzkbHtlV0ei0VSj7rgj0SW6JESy1jabpRlqLckQdEdN4eNp8TFowRCEShots+ORHTiKOnFv+aSab7O82WOZZuIn0Viz5Eb5HySN8mdny0fOf9DI6srhMuJ+J2RlwlSN2FpsjElWHVRL2itFse0cWfGRjsevrfNIhjaN3huxS6i86X5Rl0LDZuNxuLNw0sShHO4so6xcSJuo3YuJuHPHWPt53Z9kHRbLESWaZ0bTsrvajaxWjS1KFOEjbA24tFjY7EsMs+M/Es7OollSarDsaRRGIvVl5ommLFmkTIRpTl3fC8KZGeHHFG1Z2nSw5RN2HQ68DsjEedwpYUj2VhSF2bT4yMIm0W0ctI/8iM4I+WAuy5ZcsX0L2X1Z9m0olN47bj+0vbiVxhEkIcej2Q/EZOVRvN8KERb8HQ28JUPcbe+sXn3hFIlMXeLHZHDIyzRE3NkZnyyPmY5xZ+JtiaYoWadxLvNZ9kfR742Q/aX7elT24+8SI/teF6TxC1hSNaWEXwsvC9qSws2jvhcVmsrCNp+g5sUGxRHiNnQzbwRuYi0fjjdlCNxGRaxKYrKPu8RrG9CkfSFjaSx6Prd0Mh7fqPQyHuu4l0bhvssvKzZZfOh1j0b2djkkXmyPv9V+U3txvLNp6GLiuC6PxZtNuY0bDbJOK4OR6ERJSRY6SQl1X4/X65l7SW09j9EOEfYpEmSl1h4vhWFnsrCR0OR3j2Vh0Wds7xFJH7S6SkxyYoizDs29cLxHhFM2E1TI4j3is0Ms3CEOnJKxvsY/S9xsrt+r6Xpn3FViyii6bfdjfeH4Vws3SK1BQ2jsvDOi4iSPtbSKty6IxpTkWxI2odCxHpylwWbyuxWOz2ehMTRGuL4UWfqb/wAU8KBP36XR/qfs/wA33p9j9C94Yu8WdiiyuUcxRTZtWKxdG42m0uiVyxGJJo6Iw3OTjFRi5OXURQ6N2I5b8CYpC99iVjiiUWUxMjuLvEmfXsihRw/SGfSXe38T7fbXR1uFZKQjT9VeZCzJ4j0sRiOFcFnTiKPDefkdrD/IlHO43IUiBsOtNSbnLaok5G7KxZfisjIVHRIeIC6LFdlCLGfs5G3ETdRHtRF0y/zXuVURiff+ZH2L1eHiNs246ESjhYRE0o8GjbYusLDksOz1nSj0ltJWRVE5G4isUUX5UbyOrZcR0ULHbNv5MQ54ZH37f2f6l79RiTPr3L0brEiJt/8ASxu3EvpMfvMcxRRIrFEURTNKsLK7OojkIkzttemSEacLblGJH8x7TV1RvEV19kn/AFe2eleJzYsrEVmh4kSfXpSkQPrTIftL2RY/ZLMSsdCZBDUkPMSMEyMSMTrCRuxTGbT8cyxGW0jCUjU15G58Po+vNRQsdZVFpuUu7FEjEo9v0VhERvv7tYkyTwho04/juxL3E+0S9rES1WPpCciWdKKkKG3hebw6KNpWOiEJTK0tElOWpKWax9f0L4WLEpdbmRR+xGPe38T9SBYxF7Y/V9bsWPGmvyoTpbCTrEfYiXvCx9dUbnUZVjbRGJGNcLw8WezbQ7PR+Uj4ZFaOmT1pSEi0h5vEvaEUUVivEuHs9Fn7OMcRiRX5Nkej6Kdn1Y5IvloxJT7jIjP8b7I+hYooirI8ViDW2Kp4WO87UfGzYkbbJbYlxHrEpTZRtGVl4QxcKKxQ8ofOsWNO4xGQj0o9rF4bUcbi81yhLqTV7ur6x/ngj9T7jwVGnBbdiFFojlJZ3o3I32b5Ep4R+p28dY7Y1yXgeYj4VlEUbShJVL31wjER/p8H1xWNOOzS4vKy83hEImivxxHFlvNj6HqscpCsjElKk5RE0fifgK3hvu+Mfea4UbXexlc6Oxbjeyx4vFn+fUf8xHjcVReL4aOk9bU19SMpeBe0ltjJ225cdNEYGkunHvDyxvGwpG03JD1C3jczdI3MjLpz4rCys0KPew+E2KMffDaVxULHCib2qLbxuVHs9jkRROTlLtEehydcp/8Az6WPrwL3mxGgk0qItD90UbShyL3NRKoaJ6xuY2UbTaVm+KyiIhCwoiiKkSmSl+IuPWLxF9RpkrZXe5J4gPDdKKH7kNUvt+86SWlG3KXnSNOBpRrlQ0bXJxjGI9QlqbR6jeVEqKN8Ts2lFeJCwiMRRHJIlMlM/j6fyKSW7adYpm1lHeIlFuJJY/0+jd+MWN9L3L2/xEyElE3OXHR0t5q6j1JLyXwjE0hco+petyQ5pKeruKI6Y4ooeEjcWPVNzfK+KEaemWoqetZuJahpwlqS/HR0rtxTOjYU+KExDGJEhm4vqB9ylbT6EPrOjpPUerq/0UaS6jBZiPMH0zcSIxsUcvNIcsbSvHE04ktVacW5Sj2bZVp6UTTjGJ/I1fxjITx2OUSUyyxNFCjjd394+4xx6UtT8cMj6ct2NLSeoaurmvEuCEaVkecPZtKIo6WHPDmbi8V5VKn87HJyek7jsVydFsuVSZEvDbLzeNxGRYvY/bkRL6Gz7RLHrTIaJPV40PyIiiMSPNZSw5DkWXfK/PCVG5ml8ZN6Ruwo90Msb5IQhzxJkGN9Jj9o/wAv2RjKT2Q0ic5SeVwa4LnE0xIXD6WV6uzcojkSkXm83wrxtZWLxprtJDVYkuKzEvguCytE+RJXivIuNEEKIuK9Zi+kzcPK5oriuNH2VlRNokL2i8TnhcEuCys/Z8UjdGBufGjbxvwpEVZpIgsrP1mDGUND8CEjbi+aQsbSjafGVRWOszlyQsR50z4qN8Yjk2+aHHC8SEaZGIllZj+v2feZPCXGsRiKOJZXK8VhFsvKEz2SyuCwsvK2m9n0VhZWE8vD8SIGn+p9izD0/fBjxXCsx9LDGuV4XGu+LkX4IkfB9cPrhdG7L5LMTaQNN9ZjmJ984oawiij7QsyiVxVG5iovCIyojK3ncbjdhZWFiJHjXleN3gWUQ7NhCxZR9kR+8yFisPEYlFDPvFkezVjzsUjeXiOLN3FYWImw2iiRWI+/7VFFEEREuf2+DEiiERksRxXTiNcWx8VwjLrcafY5Hb51hCiI9NUxYifXBvw35EhRFEjEQvH9kShYmIQiKNvUon2nhDiPwoohFylqRjGXBcaIis/Y7RHFm7jeL4X54iQhclwfD7WHIsl6zGRY4jQ8LD8PzRhp8kLCxRtIxI5isPF4TL/oLgsIjheRY+y8I+8fSkQliaHhFj/oULKkRFKJ0XhC5LyrguaL7TE/D/lkcfRT4rFidEJkvU+kRH7svwV5I5QhESv6iKKwsLimRZHFi4/RHMFY+lQ2XhEpZj7sm7IiPY/GiuCFwQjaULC4orzVlZWVj7wmKQmWXwX68LUEOWI4uuCQxkCUuG3ksrwLMcoX9GuS4LKx/rCLFIsTLzH9c2WOQ8X0PheY8YjXfBeGLzRREXjoorjXjXBCP9cUxPP1H1wsteBcXj7Qvc+K4LihEc+uK81cVyjxR94XrF5R9Q9Zs+srgxcWRGIRL+hHC5I3ClyssTNxH+n/AK5RwsaY/wCkyI+C5risNEX2LC4LKfG8bsRZfjXP7wuCxE+tMlnb3/T+heWsQ7K4ISFHMfeb5LF8FwQvfiWVhY0vcveJR4UV/RXJcFhCNpDjEjEUcxxeb4xfKOLyvF9ckaXuXvE0NcfvxrxLnHCw8IiR4R9vzLkua4rghGmT94kjabRooeF51yWELwPCIkS8ryUKJsNpQvGvHpvvg0USKHHxLK8VeCJeLyiLFwXhQheb7vpclxh75bRxGbcV5lzjhcFiy8LMRcF4UIjj78q4IWPvH1H3xhAcTaOJSw0UVis1iiuEUVhDWKF5okWNd5T8KF4F4F41mOIxFHrafGOPcoDiVisVmucc0bcVyj4UxMlDD8SwuaI4XH64LmsxxCOKJbYRZVkokhvisVwrgsUUL0MWY81lEokoj50ISEsLC5RwuS5rguGnDCRGO1SlukbepImh+4l5XJct2ZH3wfhWJRJrvmkLxrn9LxrhBCNOBqyGRxImSPpF4XBC52buX0Pxz/WUDaVxXJ8F4n4FxjhY04Was9o+EiYx8IYvKGX28tkjdhSGfS8qJDiOJJFFYjlLxrzrgvSzGNttacZS798JEh5WIcERJG7MWSwhcFxRfNEvY0SRWYxxXiXgWF40Rz//xAAhEQABAwUAAwEBAAAAAAAAAAABAAJAEBESIDADE1AxIf/aAAgBAwEBPwEOT2oivkQQNTTNDyLJZVGg6tTuIbZFPFjV+l+IgtTuP6gvKKv1BkjcI0/E1PWKcE6oq3g1WVk+PahRRTm23GzUG1fCPB1LI+JObbjZNYg1CrpThpZYhesL1L1L1L1LBYLHZ0UDUjsdXRBX9pemF05ttRXLi7sON6ZJrkxeRqP8WSurrJZrLa6bV2ghu/iyWSyTfNZe9OdfgNRV8d3I0G4Qo6IJL9R1tGFQgn6iddX3agn/ACRVqfBE898VaUYDUP4r/GOrUTqPgHcRAhuIZ0EcRm/NEcTD8W3YQT1A6ijatWKfzM9rV+Jz5I1NCjxa6yzvyOx5iUITdRCMUbCh7j4AhmUIA+Eex7joYRjj7o2M89nQghtbQchqE/iOQ0srRXQhQfAEAxBEFBQamEEYIqEE4prfjjX/xAAgEQABAwQDAQEAAAAAAAAAAAABAAIRECAwQAMSUBMh/9oACAECAQE/AU016rhTkRZCLEeJdU1mo9NwNRKaorwop2gcj03A2ooFxUO3CI/cvZMcmOocvZFy4zkJulBdV1ua5NdKDqDG56mnHptqbDQIcqD1OEvRfYwoO0m0KbdNJXcr6L6r6r6r6r6L6LvczCEMIo52wzxOijAysUGzC6rqui6LrWFChQjVmgcMUhdV1rK7KaTSL2aUUNW6Zrx+XCK4sppKlSuynCanIacXhCopNhpx+OLSuLMFOIUmk4IvhMsHjmjdoWDLChDXNhuGqNMeC30zqx4I8E65158w1GCMZtOuBUoYYUbLsMYCotjWOE5hYMYuNp0JtNBonK3CULDpHDNozN0jeKSpqMzd0aDdQYxmbtDLFTVvmtGcbBtKZnGGdXiwHEbZU1Glx7EoY5wsyDNNxzDSNTY1NvGRt4wHG2g0DUaZX//EADMQAAAEBAUDAgUDBQEAAAAAAAABESEQIDBAAhIiMWBQUXEyQQMTI3CBkaGxFEJhgpDR/9oACAEBAAY/AmkYauEvXe5cN1N+RJxx+LrwhpHmSK8UQMEgpwQurPbPRWKBMMX6mvQc1FKz3b0nvfMyFKtVuDoUzWL2aVdrhJ1OdIpZsG4C8qBYpxZQvA0uU4QnEUkSgtVOB+L5ejqGs3ggyhqTxQqK8CWgsFlYOfClD+8ycN80PMrcMKfxVyg8C5k9y2i/AUmQJRThC0dUz1kuGqMFW+SklDcbvFzC9De5eVwwWbxBw1B3DBnHpCoHINM149ltM4avlg0UMLiig0nH1Svd7DakweCW2I/iYkxewykPl49594IEOVIJY7UGGwTFLsHkYNSaitquE/8A0LiJjgxuPAUnkQd5ni509pHoqN4OdJ4tfNDU8Ggg0hTKikdq74V/I0kkimNMdp9Uu4evtSYeksS9xsNTTrBoPBA0Fvd4vIxkHhtIweLhxpMPBpHG9N4qTDCWMKZjNh/QbDUwai0HDUHg9N5ExYiIJ8w8R/4JhpwhyUIxUXGkPDYNLvBg8u42pvBAm5mNW4SGUIozmNO0Egs601ovFh3DtBT/AEIfTwkQUb0Wnag47BjoqGlQgveHcw5wLF2GbKWHxTSq1NyHaDnSe0aZYpQUNYLVadiDyOdJ6u8HnXCNpXC0UlQJO92phmG9u4eOw9x6huQ0vI4WgsmY4rBqSRcJa97pw1DeVw0vgNMkiTpB7ZoMQ2DnHaVYtaPIkyBCrNKkfHQkkSohy7weqkXlQJBZ1gt5tB4rRSowem8VlUNTeCW7ReLhophCyJK9t5keigSwTvI1o0yQa7eZBlKY4r0tQoXpLUcsPFN4IYai4alqhmP0/wAhTCFfpZqEtXDRSn8z4zYf3MN+nboXiKQaVJU7hIrWeoweLyaSD68X7BcV+k5nFKPiTLQW2cMQ7B9RhNi6EkFgkq0UBpAziZwSplxFWcJhIOfR1P3gkPNNC3MJ2gli8+wYONoOcG6ElBax/GP3bDMRW2kKcHiw3g5hr55/MUp5S/I0+nCxUswWwaCrJvM9koeshQWHiCjKEChJkIfJ/vxeq6dg0dRhMIe4UNVQIElSKwyh5v6jH/qQzYrxp2K3zmErrKgQKPmHvNmxNhIL22u0whw1Fw1Z4NFAhBYOGoPFYJAoKFn7EW5j5eH0leNeJDUFIZStUkzdo9sPuY+X8NsJdBcMNRhrNT6Dn+I2H9zGXCxF1v6g0zPa5cJKNWrF+wXi6/Fb+Rlw6f54vq0j6ZfngzWDBcZoPpl+Q/W0u+4Zvs6v208cayl7hMJrxfJ8IvzwxftgnFFPjKnST/r2/wBt14w4XjCmMpBOMZSoJxLMdsnAv//EACcQAAICAQQBBAIDAQAAAAAAAAABESHwEDFBUWFxgcHhofGRsdEg/9oACAEBAAE/Id4BRkc75HUOMRJDHDwjhStNQeTH8jtokz0LzpEWfHZfOMsUCghiL4eeo33I+4rOhG1sxR3/AKJIho3EXsn7edGUcCTLWlyIQkLPURRDOzbRNciL0rSv+I1RQtVq4OdEYci22KLRWbaJA3R/4Ninf60WeoiWbivnEMsVJan4Lmcga85/JxMYm83Ek4o1+uChWbPPjNyGMecEJHeOOBkZA45ESNEN4yBJknr9laJECgUC2L/w5MYozY9bzldLFObC/khEIlObFiICWiTzsWeotKKFOikT0QhCFrWigoo2K/4ZHoPWohZ1o1jnv1oNCM9tESOCBZOFp6EdLzjFpK0Sx5wxpfJLFM5uTSap8CTzoafRLN4J5sQxCJaXqiOi87FIvItEi14LJ/wIYTEKRRz9ij3+xIghll6rStFH/CnVf82IUKkWsiZ4FpQqCQgxy5zYo2FvQihmw7OMIWKFLGSpuhRwJUPXOcjic4KLKFE6jGuvoaZWbFELgbMhrLNhTwKSFzpWi1XkSQpFAp4KEciRWhhASMbBIorVpENcF/8AC1knStVpX/CN0Ia2O3QVCnWxxYlYyM8F6SkbYlJiXnjSENOVLYnsVwKkVnZZtnGiiSGeOC9WkJCRA2CpGkUKSUPvS9aKFnYhCjSQpHIjNoGCccs4Hpij/hFFepAjgsorSxSScf8AcCzGScVuxiJQVrQZQKkcyQFvkFCI3go0zYh5cQ11iO1kufoUCMkKEySJWnqQ9n0SxRntrnghZsQhTBRDF6IZekC2osrTPGlCFPOkMSlEDkTDDIWWJeGSxPVaUXopFH/aFP8AwgJn6vuKl0jkbfOIT7fkmNxbOT4FN2RWhR2Drxp7l+2JMRJZJAallls9P7KCkvSyGi4IFsZ30Wtfq/JJcfsbRsJdkMXBnsTnBJWlLRUKBrnYjNtEWRBAks60vXPcooiUQJHchjewhCVlC9sgWk8inRNlikX/ABf/ACtUiGWO7sY8XDEuhStsZyRFMijOyVUObY2Jie47SdV+RaHZ6Hz2yPOwviPkVInOZJBHZrGSUFt8i8liTewmbiTex0IVCSICei6xAjexNPOdPQvOhToiiz0+xrEEdCjO9K/5UHroQ2eSE2KdJrEuTJh2E42s9CSZFFC1jL5Foo0rWP8Ai57EmJhPnQkohziIGxevsIoJIjsVFCbE0NslWbD0+XvCH7ZAxr18sRf08isCK3b/AAXDVBetIkS+x9skkyBpJJzb1FPYhBnsJlQUckIaMoX6zpFKID0sQp0RDIzyIzsR6WKeRMtFC0RnklkNicSHOKEBCEkSlnI3Yh0FI6mQOmLREMh6UULRBJKEkKCEZIga8YxlJyJUJCA00oTglbCoQIk3t6ISjnzYajcthzziGfj+CS+BjciMX9GOxQRnkRjkvZmwpH7FL2E3P0Qav7JQsfoLfn8mLwSfAm82LJgU6KSlviKzsRCM/BEFFr20lleuikpbfeiMUCTbGBPX6EQIIQ6cgR53A12yCJ1iE02F4bR0fWjDkSXBBiTNiDFQRIZsQkUs5GJEoS6E2LHA24xk4JE9hNic7JCQiMBFOcDOBGd+gzM9hWOwiUOSMXpyL6002s3IZYxbkpsQQyEkZGkopnJvGl5BuhDgnnZD5X96KWUSWKM6FGdivPPJG82L5K0TKRZebCRWbQJIY8ikJFDRaJMQmLE+/rwT9jL5GKa259WSqSy4G7JJfImRI1ZaIDfg4ixIiBQmJRTBQlkmiVnYlECZJLJbpnYySF52NtrxHkMf8vkTJEcVI0xJsSotv4irHA0UHgRi1nenTRFY9xlFYixwWXnRF7oaO9nBBt/4NjQkUzgSeblkPjENh5yP+QlnZISRQkyBNnQ1jwfrnRUiJCEjGS4MPIkibg8Ik8M8ekrkUcalI5W1iYRjLLxkVsYcjozcTeOfQVhMLHgl50Ndk+CTJsSMFBjEhFl8i4O3JKFzJfImbIFS19CkZ7PsQrZ6CWCHkBGNsNgiyOhOBsdESWMe3InkhLLPUhM7jhIgXLehpf69/cXiYf2dmIXX+tCTZHp/Zh4GCO4TRt+hN0dDOBBQkMIZAVBby9CFlkhFayyxtac6ZLLG46EpaGHYpuMRdliksU8CkTgMpIjJ53JTmxKgZkh2xfgnoKBSQ4xFxSMuEnKSI1m5OWJEJhUCA5qX2OuCA2fwUMks2E3A5ZQiRROlaJLn7IQoaWnh6iBIZ+plxppULoGCRseIQJWWR0fw/JRSGxLbEwifAoEkSQJQ4zcl6TnA0zYvRYmzMmyhLPA3+e0l/wA6IZ+Ci0hZ6ehAbRmxN43F5GedFaXnRCDTzbSOsZQi52+CHHBC5PTSShiICciJNAr5IKWemJaFHBLgVnejZN6XyJMSglDiyyiEVrD4FIzkhCYgLJYseBErkiMIYglojIFEsl8FkuRzuxiSiOiGKeSD3JQkgU8CaJa3Ob9SLsed2JiYlwJoJNvFlPkDBxnfJIm0JtZwSozcTEM3LzolzkaQ4vJFK30qC3sQMuSO42myciX5wJJC5Ds6IMQhz7fiVoZJBcZt6j6Es9BJigTWdDEjbVSbFGeNE0hBTwP5IRRQ9iJbDJb4hs84JebCmMkUZ0KBRojzoW1ENIfAkZUZJCY/ImzrgcRY24yfAoeHyRErYoJRXo+SofImVOTsS9mLks6xEIvPA0IcuRHjkgMiAb75BbQrM+SOdPYUcFckMSWbjM7EXwI5sQheODaJO/odHExCYzYRE6EnLKREUWdtFQhCZKJGxYq0hFaIoSYmBokSJGY1lixJIpDt6ZCEoLJbOzIF2E2bZGCPqJGSJp5ySzwI5vZQnQdu0JvnEWKFJLHC3205GTjKEiSMTHShyTK7ImbEc7E0n9pgWthmZMVI6ixhcLAlXZ/iCE8uSJ1kCkhCgsTSQkdglKHI8a8KHOdiCSVD2jYVJCRHNymhzDylkCxaKNM9hC0sRIseNLyJIb6URQQWcjmLWBRCKzchiCWdlkL8EDDgmi2ciWSCnr8ySsTFGdcFLOBQJrNhQHUEoOM7JzwUoHM5JOOUIieH7K6L4xifrfA8is4KzoUCNs8DnaImcic8lNeCYJvgUzZCck9yy+R9BHIk+fsj8BM1kDEGoo5+yhzuxEmFyRGw72kmck9/8WWXwKSBTwJ2JChXOgaRC4FQoIjnpsWJ2cDcoLwJm82EoaLOCXyQJrYyU9CEXnsSuNCQsSZCHKJYjOiVwJC86JadZA2jHoSuSpzYUxsycfgUtN+OiUxVZIndvgSc5GyZLdBsWSltjEnGISZ5F4zoXkRHLfvgTUS8RdbIp7jzkatSJfBZDbvYrbjLDQmUJCLgOOBJkORI85HwJuSXngaSGhkVQpEYiI8koE+z6kYIoQ6KE+ngNSBK4JSVjaeikUCQsvkaWKCA3G2UhZ6Hl9EuScQ/qCUWVwSiWN/4+SU9yHm4zTsjxjJPOEMzzsXm5LPPwSzycjKbLVN4iT939jpmP/RKad8/IkaLPBF7Z7CuAyWUKBC4OQ7+EJoIryfyxNc/Yr3/AANeT3Rn/wCzm9Il2WnXNp7LoRHob5YkEiI5bCchwMnNxqYmImJufsgVmxTYZolPoYZsRzsWIiBMlpKgoRGudiWb+yOgtK7b9CbN4JTRDNyWblnqWLQ5a7/ffRhzP+FrKKzcUcEllCaLGfBOcE15+GKHnRvIhNznYoePY3fn8SNN79hsJE0yiEoxCTlYlCjnJDTX+fQU338CGk3kkGcI32zoRFu/wemIihPBCBDORPwXCeiLcghSFUDIGyEYrFlhJcELOyhtFaShFESKGyhTAg0eSRNuOJktHIUrQnRIbYmtHkbBooGwrkliklCkUmfgseehcZJYiyFm86EcspOV9ikSiE5sRUfqC+Mgw9PByxJRCzshTyJH+j8QbeH4gVKRKSv9nU/1x7iS53CuyyDWbkPkdd8bIV8fkqW/b3RBEnnBTYbXZKJ0gJQPSvQhwQuRqVxyKDYYRz6kM7HZ2NEiZMfn+hsT5FjyJ9krgTyKzshy/wBkFlDZE9iIySciSuCRMSyKRSssmMpT3xk9J8iaIe4pERITIyyxxOSJ2KCHyIhFjoTcZJHWhGf2Vn4FU5Mjpm4m0w5lLLN0LOxZ8EyzjYjj+fckJN0/R7kJGN5wSnLJYklHj5KRA0lOdEJyJexLnILIG2UIS7ial7TwLSrs8gUiZsBWXnOBUo0UECAmyVm5KJpPHBAhnZPOyfIn5J5+CWs5Gsl50JkkiaZQk82FI0hBMiZIMSTLJk4LFnYmie5ROdxJkhJ6CkSiXQk86FPBBTnB3GWSQihNPKFKQm830shq0Q3WTuLwJj8eomyvj2+SWk4E3P8A0lzmw/N18EMn+R876CVV/HuO+/2RVL7kT4IIn6vwOSc0Qy2EJMRkslaJSOUIwtob/RVQlFBAkURWx0kslDOc2JzgTrOBISzYTc2S0SjHqI9RRmwke/8AmiaFRhlckCXIrWJ47EmRmsQoG2gmyWbi5MTZAi4Ic/QoEFGdC8YhaO4JeepJLzokKOhSX/AhTJDQkuv8FKMXUFUXnRDzsgxecZtzcu118iKySY5GvG3BPRYzKEQ1ZgQbaWl6f7EXYyXyGgvRSXlsJ0LUhlNjFjkgIlZ0K4+ieiUSxSJhNoiKM6KzfRpwRzsZFmbk0SQlFBhxeMiWyHm4s9NCXOx8bkMpFebCbZhK2GJFnIliYTbyxNjMRTc22KyFnoUiM50TzkRZ0LziEQ+BTYiRnfAu0fvcrP8ABsdFMuBxn+jTga1fUQ3SoUrbTIXnRIiyCpoQSlGJI5IrlyyTBxhtKV+osTOs/kT4S5JBfMo/E5yZHAyJahL8jRobFkosgWITkksktCeikliM7KQ4TRfJLWcDsUyPYSDh2iygnj7MuSAYYmDBzjpsVmx6hW7+hHRLNoMPBN7/AHCFYumkuWWKUKxRnYlngUCncLcTHVGMlDh+iEbjIXGMSkKmjBc2TSFCIFk3nAhtiSamhrdPzIhePz6kpaxlK/UktxKkWjf4Q/fIEMZVtDocxMuYfQnYq5UIXb8f6V3E0ZcVpQzzopzcUiMSebksUQMlliclIT50IWxJiZbaEz2E2QzokyI8hFVnsTkK2JBD0Kpvx8EuNEtDR2RECLTvY4iEEo7gSSyilshUSCTW+MXPAvBfH0W85FLO5MhQkOG89tDIktsRvJLLLQm1uJ6Iwler+2Kusvg2c3E7/wBc5JYl/wA5Yuuf7KgzkldnrzwTdQq+BtedFgeSChuZL392f5xfwJxuefet/K2NQnIQP+/6FHP0JPNhJcFaQhJTb/wcdkGEXc/0XmLZJZz/AIWT3iLF5EYoSyIHl0LwEuz6FibjuIiRVWwzR3YhVObiYT6Vcr+RWnQ8k/6JOcUIhjEXGLSGs8FikSbE7FGf6Q8/wUNhTx9EiljJnI3HDLFjYWNIr2pSmPVz99v4HSpPdWb6KRE1nGRl0RM+oiX5Co8/BLqR+ePY4IfHrwM/PybQTbvQanc/ga9r9CkpXyQoFRetF3DJ5UN2UFRYh8Ek8glkslyI0Q83JzQ0SjHIkiy87KFQk82EeBJFbKp8kCL+BM8oVb+yhBCEWfSv6IFVUbwH8LHc5PAJOfoS2RJ+glxyIlsrGYeSVxuEnz/YwZoWnGdGXuJsRtSGRmwghn+FFIlImbq1EvRCUT3yiT30gvYL5xf6NYtXD4EAzxfIreSRs/oc2Pc+P7Mejoc+o9zJjOBI6l+hPTkTtyDc84lYh7fR4wIkzJs84LbG70RIUiCjNi+h9H6E2ONFb5IngS8zqF9aJS2LnsV8oU6kULWf+EWwm19HQBb7/wBiaFxJJkmckDuK4DgFPoErOBP0J58odnkT9Fjs7G8igenGUJJiLoEbZt6nRZA1Axjj3+CUtzxJgv1JaSnSHm4pLEJMQ5PYWOKLV1Ge4nuEPzEUNbzklfLYStefkgvl/QlJJsrCktbDtoNdc9SHnkTXKIcMTIVI2GN4E/BO5DWSJcsgQEhGJJcD4PsvljgUKLYik7reYIexSdL/AEbMQIZyIpNvx2Oazj5E9i/ZDvEJrnHobqJoeJWJmiaHJXH1DveUKM/0qa0JC3/2BVbj+J8DJ2/RdHP5gTEWsjQgTGMU2bCZQk5I45GkIcWPcVmw0xITiKbeP0RWxcz5Gc6VIT+PcV4DiSg+Ykby4td+BdZenGJ9Y8xEuZw4ESI9TkUnn5J378/I8K3GeRY9ApNPOBCm01hLj6NrgQKTp/skg1+xXg/genYl8CWNxd2JElwSgl8k4K5Eju/8GUhPnRazkRpsYiiF8kSYoSlTHCmIxe2lHv8AYle7Hv8Asdj2T+M2FBZ3b50SPuL8iPbA8nQ1d/klJ0zegUMyfMl7tibnuO5EryNFpZ/uBPyHN7iVuVnZHYVOSOvf4HnqT5F52KuiyuJPR8CTNyJfZeoqD/inL59DZ0tu5b39iJVc9EyTlD29eF6i8kvnkmefz0OrKL2itw8JpIc/ZaebCmLxk155I0zWdidoSpbPkpl6U3ZCMglcm7BNKkJXuWC3+SIBhTV/g55cC1zch52MxvH0S+fuRlcB1zV96R7lEXuJKPuFEB/0QJv48+x1xfhRX+CRuKc3gdEGciV7Yyr3/Yn7JcijXCAin7Ewm+HkTfaEJ27giQrb+hb2CeRH76XHFZPdbCa7ibhHd+nqMk7CJrLEIbXAgRzt+ho2LmxJiFuKRJqWczkiq/DyIi4vwiCyRDKYIaKhd+hXpK58f4Rro69NhWdRxd+T/QfkTOCtI9U+BkxDbIFK3FL3+p4LJrYUuk914YvHfgvNhGsvQhilZwYckLknnYgF4rEc6ipPn7Hzyb8jQTjz86JDnGKWf7Hy2JOafwOcT6/4KjX/AAbEf8o7dD1O3123Zhx96ZJSICZOvsZyhMQ5os6ELHY1A2JYlQm+Po7hDvHuJnj27E02Jb4J9C60bJCGvY36v401vi/wrz+hAo9xQl6/0Q+RliZlsYtiMvRx8/kUcDpjcV51z7ke6O3tjfrSXz/FJD4iD2GbjJZ6C17RDWyixIRCITm4qOehCrv87C/WcjEZ/wBCamXsQ3bfjoh5Fv6ciReVz7ittZQkksiCjJKZQnE54Y+RIMZ/IoPVcgUcEqRoSYmEsfgz2ENiCk3MgnHh8eg+K/8AfQl8X0IPV/oXVlja5DPgU8kKMmRMJJmkQ3oZ6/ckIxuQiBqyhQKJk9SBJqEyxipuhBIq3+dJuMQ6lfsXbC9f6lM+RT7fkU2wnGKyaeXwNYkltjJrd+xNwJJ3ISeuyL2LCXqfkSC6oRNTm5NuKbbkxm+ShVivBCbf7RJrNhIU8E00SqxCNEr+d/kkub+OBm4/l68jOMmRGW4lYjSmJrglcE4tksatxDef2JprIFbmWxKIWmOeBSfgXlBThCWx4Mhsg0tQUWuP3pSElnQomtBC8xusQl2JQJUJzomNhiY4LdClPIG5sMnEnP7Cbg4WGQW0nfHyct/gntoaTz+COX8CUFHuTAwW3f8AZyZgUtMTYSY3dMQkdcfIkmL4Ey33/MvwJ5h+/ocdXBSV4xUliI0fH9kfULlCocCLW+LRkDPsg49vszN0KFnIlFZQj4VkF3m4mrGTeUJs2EJ6FJGiQlolBQs6PUsU8fRT1+R4SYgsvV7bCfcHakQhIt/wVz9lFuUIpOoUnWMjvISFFkloZMhpaGwSmhJECHdm1MK/v5IHuV2c+nx/h6/sL9SiCcMkcQZuwk3rb1LQk80JJZUS5LiXiJbiI3ziG3UU8iTCjPQTU0Jr7Fb/AE6ItoEnnMkAqZvf4IRTbF3QtkQedEBJZwypoRK+/wBCGhReJJubHGTDfwMsteIExiHH2SSxMTZuIKCIiCQmkKCEluM9CHm5UF8YhN3b5LashSSXJfP1JYqzYh50SEzdlJ5/0iVoKEOyXNCchQIRRLgvkTYmiQtvfOyDuCLZnoQ20tCYoUCfgmmR5pm2IBNcv/IFvgkeSbg4YQhEtvPcRVnBTebFnj+Quaf7I5XmRh4pJehKe2MSPd/5AypNikyW8oq36gTYe5KHfAmNhFsFe/ERvr6JeL5J4449yE9vjj3JPmxLOidhNklol8EslpLkTiUTCEiUUXxiI7+xLPqVZslErjEyr9luR1ssc1ZPkl+j5EhotyWwqduxLrPwK9kJDSc2JhK9txSxsIcYxxmxBCIQ6KFD9c4E3yT9OcEp20+A4W6jOxTNfUDsWjRhcMQtxwIWdCiRApS9/wDSAinoJC2FlDNWsYzdiFDGxYUJLySX3+iE/wCPyRVDfdyWi88EULmTxKe2NFRDFMY2JZvyU3nBA63/AKck+XAxBCmiJLEwhQKCEECMUzpTYTcshCB9ODr/AAKW4o4L37fBBKs7KSzbTSZ2Kcg3IlzchkoyfnpCZHthjFYooRpv+ibFiHJYmQmmiWxYrYTeji9xcWEJMhIU+xHfZa2+yRWfnRJTe3yQ4xECcEs3JZ1oRXpR9QSW5jx9yPSPuhE+Ra7xk7l+iCGFOSaCE2Xs+hEfP9ibU56jWVlsTSSTWbG6uvlFRPo2DnASRnmx3xjELPUSYoE2L8CKJihIlCILgbeBPe36G+TKrJ8Yn4FC9TzxE9xlAr9iWEhcdUMljdiFhK2ETWJG5AyGdCSJZwOjp+SikNohyMUiaSW0W4a+hF6yLKG539+hee5LK2RM/og5EkkKRQlBKzsZ1LEm9iGs7liRbYhKpxImvV/okVff0Es3afAymJeMuvfIEmWYyk0juOMpryUrjbHDE4kViDdUKxTE00+HynROEUQWhoLHrsYP8GhYo0sSeitGZ2IKQQtCTzoacyXETPcmNZl/QEkttxG52WEJwyTcPqDNE/5pA6KGx3ASSoNFsNtslxiLTJRT/wAJtEilMQpCNiohbWxLlbnWENBi2Ywg8fohJRkCFnIp4X/QfehiihjmdiFjhEEzf3Ar9iQp9fxpEixvwQ0t/wBkNlUtMY29nRMvqO5xiSQ97iMo2bCZSGvH2Qh4WbDM4YnpIZu4f2WxJYhj1m4pFImQJFIx7iM6KzaNYLZFxCp5EfoE2OUS/b4GueSAT1+hm84FJ7EKiNwkW8xoWxLZCnkodFcnmRJEyXpJYtFInzcYd79vsVLJdCrhCS4Ic4iC5JPcwOBQhO34ITZCITKbbkQCUZwKDXYkrEcvLG0VkECWxBzJScyQykhjgiZrct7jqyy5ISjHuRrz8kouBvz94GkiZGM2KEw4EmbCSIlQjiBNuGv9EmQ0iI5JM4FJRbHRm4m37CLaWSZK0ySFhmw0t+/4IbZD4GooSRN5cegja/aHvn/JGgkudFdyXBLKK9StFoo1QkMkJehB8xMKAyncbjfctvYTPwGREjpZIp5xDRnAzWNy7hR6iFZDj18EmFGexFnYzr1LsxJE2lySUjPySog4MfI4bD5hdvKHh0UMdbCVIpkSRAoUcISwkzcZxhB/GJMkhhKESBMR8lkIIx7txzJci+PuDhCJeXyi4i9f7JyL6nWcHmPAj66l5Ijt+3x7CGLYr7EmIQlCUJ/kklFr/pCEIUaJibJebiaJXBYpSouRNGbFcm2OvkRVO5ZdZRMh8YhlDmaxkNepDPIyisYoQyGL2CbKZUIrIjG7I1llG/eWx224DpWIHcoFsTnRQhGxMhJxRPBWNJOTedvj1FatXopEaZk2KQnkbDbEE4h+D1E4dLYk3v8A6JIr3zkbebFHiJNZwIxwhAm9a5BjpFJNkJd3+xTokiTUaJei1paKP+UIVBMIKBM0ITd+j0Il8BpAxwXeA7yJPLYUeYx2KnG8jeWIm5eMZtX6FOagkxEjecQLVZAiKx6Tw2QyEwjPA04IQfj9ks8j3GxkhIsVtC3O/wAFZsQ5r7IXAkjN4H8foid/shTWwuHZ4EqISz4KjIIljiRdZL5DtsJSU2J7ocJDeNhv+oSkx/CZPK/jj2EaFOigKSzMkMhlClCPQgrVCLFIqEgoYoJepA0PafcDZzgSlW3whgUKblvbI3K2Y/Uk23GSgg1C+yXgRz3JeOCCYTc/ZJEdliHPj+x5kt6f2JmxT+j8ocyImmbEuL7GWdkliCxJkl4CjgUBT8vjglq/0S+Hz/o70kp7/RJ82Yl5yNef0JsslIgKR0uRXvWdDVCTz2Jj1CfiFykE4zRSV30F4G8/cC9f+ZHbd/QoJXJIU5tJHHKGeiLJokQyRBBYhEMgj/gpF5IZh55FZYt9DkL2CU1OMuYsett/gpw5Kfh8iSgcAuSYwvgbvcjJEhSnZL0nsU8YxFe5+gqTbt58jGIXQJuWQJoZeAvyxyOhuoyRPIlGqxolZ+yGJOCW1kCfY6zshuMs5befwNv2+RJCTJlCx9aQuDusXpf56i37/ZHlnqYn8Cl2/XqPX9o/r4kJhWkJ86HHAks7JEEOceici8CgiRIQRAwuqoWhJEIhiYpFJIU5uLgxag/r9iNVuO3K/jkdipIJCSveDePJDhD1Al+/9EJift8Dm8otieLEnBetqBsumXq9haPanutxNKBPlwSpE2S/e/wihTJY1GlRsxCgSc9lpE9izsuswOSTuefQhCKEivJKn5Msup8EO7/RNt/keih0ROKM6Hb2Fbt/98FokiU2EkU1i+BaZY4LJLITQp0RBCKKEUZI5FIhuEmJEIiJMUjtbiHz+yG5fsTkR5/mESa9FM5Ys9TPc7xSPmWdkdzHRHg2zkdlTB3iaG23RQxHBDEbh/ld6UNoUibIUY3pYriCtv3AoknF4uCkJuRNDWSIRi2rGiHz9iJDagkdZLGlyS4+7zchzXnA+9JVmP8A32JdT75JV49RjbYSZf1dEoq/6SfAoSzgVihoS+SehTqIrgTckCEQKRhkUgmQokRSKFBCJ6GQUVTfTJcbf58EpMhWXyKIz2GqzhF5bIvP2FO7GIpf7ELklMlJJWNc7JUefgght8iKEB8LdukuTqe9tcksUtjgUiXY99EiQQZ3fHyYD2Hl/U61xi0bP/C5AlCsdyKdERwC86JO0CIhnRDHY5G9ifZ5f0JXInMQrf36jWSyVxopEY/QlIp4FJMUiWvJJzck7G6lt/ZbN6VAkiQnKLFAoFGap2+RISr5+CbxlbjRiYQseIFDwsRHt/siqiKWxcEYaLhbjTp7l8FxYoLKJc8vzwNV7X68DaEhVoQv+EWQF6USFZiuBRxiDECUEhMKVvAnloeNfX8ClcXgTs1+f0Thr2ExPkkMQheSSRLLL50khMkhqT5+4IiTHPbIHi2hYc2zYhxRISFAm82JcWSa4IITEhlY9fcfFFCaC4uRoo2/MluM5sccYycMRjPIvPkcrE6Lc69zC7lsOK0lkH5e3m7JBlu36vRFkC0Qo0UCgSJL4LHSWZuQYFAvAmydEgrQlNv9jmoWEL2uyfkSbsh5uPewl83HhbiluSJkkNCTFGliIeljjpCPBjnnIlKWIckc9hCFuXS29RJvL8EYGhD0IM40wnJedEhJwLNwJWWKKj6HY3/Z7cia2WPmcn0EILNhW1XERVchSb6r88EhbwGzL0kU8ksVJ/L9TpaidJbEs6LPjRJCgol6IsTYmLEJDM6ITDE0UyQxT2SyWb0EuF2j6FM99/xZKKb6HfFkNiVvjEnG5Dl+xWyHJ0rWkQKIRCFGiZsfj505dcs3tsb7/AttsejUWsWNLP6Gtq/oacq/IuAh8jgcFDIahgqojssRcxNk4xsS9CdhCWxTPz/AvJbGSknoVjxJsocP8QScdPv/AKJaUkUUhSStbJ1S0SEEvQEmiSREUDngksR/ggsyM25yBnpEopKV9noI/wCBJev+kJHY7+fQRwNsTECyxJaXRAvAiShhCUvc8wJl+icLJ4ZBgclmEwncYtKuVnoLoq/At1I9kBKdH9kM3MPHoKUUx0XDsTRCzot42GWKRNvHQT39GKTYZxm5NvgxzPRg3MAIaXgHn/RC4KIc/wDEsQp0nQiEJCIQgaEQixNj0siIIZ2jfmw0jH4Km/7FJf3+hS7w9BO31JomVIpblIkUiTF/yoIRDGElbLSgmI3CEV3zoQdwbnZFMTcF6GRKJRAT6E0jUKQzzyUssdTyhVkJl/R3YiVwNlkyZQM6yic4OLsUC2vTAc1HwFwvsR/QlpIQsrVTpApFQp5EIMzcTBIUCNmPYUFEZvSZYscrL+hfA4gvUkUuyhSIlExTyKOBF6UWKBoooh6UNM3JbLOSPX3/AK9kMqWxwTkyEBrLLEhSRZs0wTyBkrsTazk49BeOBg62CCjFk3+ApJmm6Ffh08PslR/rj2EtFGhaQhBtpWhEMXkQhSI49BGK4IlTo4LNCijuEz9f4JCsY6bYwgXOQKETkvkohooIZIScEMUZuSxC0hyKBJEAhEiaJFitxl6HmEx/VaJFotCdiSEnokKCVJbZLgsU8FyQJZc0PSnj1yQ4lh44eo2ZBIiCxMU6SPStEtHrohQJhmdC2pRNZUCgQmNvknnjRSbaR8ZA3zsdj20RliSzcUZuXxokxBMJCG1yWIUiyGJCUhoUS4xCTI6GtiUWrgZBSRKOD9aS0sWgkhEOHlEoFA0+NFF50QFKzgW5V56F27vvI2b2+3uJORpIQoEGIaZIiyBXGsIUkaJPS4wmLySH0IEmSGiBFQTBYwdrOBM85ELQlJDL0UiRAnWjNEJD6GeBTwKhI9BQMERE7fZAkJhINzLfJFEeSFS7z8ingtb5AkS5EmIuMmRRojxjEhHngsghEoW5+Syt+CFV+Tj0F/YlsmTyy5Ioub0QhQQ0ockiGc0UKDkUiZQoEIE01lC4zcYhTqYxAp2FiCiSG3I4Sz+ho5GEIh8CgUkkqIsY8uBWkBybCPkTRL4F5EJvOiQogJLkhnQoaf8AsIaNZJZXuQELYaWbEjYixSQKjJEkQeQkI9GmK5yCGUQ7j07x16jb9RaFofRQI9GIXgUkReRkNdaWLRIVaJCCrQrQpjTRA9immEtE43GbzghwQxMQxoSEhfBDQzgOoJYqzrVosUCaYk4EmSE3m42oyBOcoW9CZY2NsoTZCW4oFKHRooVFiIFEChbiCLOjPJA0+fsghkNFjECRebCkU6FGkWSKQpiWeCRJWiKELFAiYnRATiiSC40OSdLgbDLHJZCLEDnojExIaD0MlnJK4EhMDgWJoligZNxO2+4IhLwXnYzzYRN5IxzYiiGs4G08sgRGwmpGRApM9EOiPOMSfAkbkBQLeiLIUWQF5IfJD1g2FwVpYhxNiEsnRTwKBI2IaLTAKDINFiNxCHJGckgKKEyQg0uBJOTJDNglvpS4E0V9wK5EKExY5IFu5X/FibzYmMJCYclCVxjG63+9FiiNFHGikiApINEOBUyaGaCeeCB5G3OiIcEFFIkspMTRIoKL020sZAqEJCoTCCfNhi2+xE0KKLLFqugjLEmepApJycmSIkVQpzYhmwpZDgPzf+BxJbFwJtMW84F6/wCyZIwTI3br+QyXGiq0UkIQkiwk0R2HRWxNollB8/Il0SIt8goWUKNOgRtomOCaJZNkiRQUKeCpExSJMUiQkzPgZIvnSRQQgSZdzomJ6UR2bMUDRhzZDnES9iObFSTzsks5JUeAiKMORaebEMJ4xlDKc3HcEy9VrRQrzokIQkbyHrzpDELfRAvJDjJEmJluT7/zyMhCTyJNIaJEs30yJuc2E1nZNCYTWbDTSeiSRZcDmRCkUm2iFJDEhLPIkyEQ/oRwKiEaKIFBU6oZtsXyNR6ibLH0+hUtyGNmP2Kar7FSxIoLzsUd/scJc6FND86EXAmbihRIZCEJFrRZ3Bzte77ikvgUiWkPR4FZubxQKboTL0EZsSgUCIpx7CGcaS40QNixMllk6ozyULRCQhBISEhUJEx0JqR0ITYm9GmKStWnCNtSCakb8Dkgs5KfqE2KSEvL5GZtIh77fJKgdI2FjPRAagoWl6QkVohSLRQKNEi83LmtBuxhCewSZuSs6ISOhFl6yLEKSB+BCI1RQgkJECMSYkxISYkKCaE6CxQo9dZESOZ0WyNO7IIZCUELfNxA0pIcaQllwTLjf/BpZP3FxvfwOmIzfsUZ0IyYkoLIspFiOaEhEhSLRCTFAhQJFZ5LJijgtKsZIYkMs7JLPI0yDYQo12FJWiLLIYiyGQJoSZ0JPUgoWiHMic2GyMiULQWlaJ1HJBMkczmxJGWXInd/uBPBu0hrrJIJYsfnkVCpImYjFMWX/OiTJElooLNifJASEQiZIVblEaVUEBMT5zsUEs6IvsaIc0JdaJFSeuizvSHIi5LRZAkxJiCgSEFAoEYoKPAUksqc7Ex53CRf8DknotOixJuIxFBuyW7M9D+IlsQljfPZLEWdH0PwhkV2xkijRiTJQI20WxL0JLSJCFBQhNCFJCNiFyhJkt/2W9iBHmxAkRBAkiCBpiIXJEEEEQQUJuBJiQkQhITRCgkXqWdHIQp0JBwnzYaJRakrRMitxM5FT22+SyJEgUDudE2KXt9EV5fJOcFjOc3HekikgxSfVJaREkWIUQiihZ6ioQLcUxkiYhnZPk2aKUULyRrX/CckuSGJEECYSEhJiWciSEihFC0LVmSOWqmCbE2MHEpZQnQgz0oRKedEnnApITG9ExUTpedDuKLL5KFuoIikvcMx0kHkUCTFIzzF40RIU8lvSKEY7kdzmwkhJc/Yk33OKxCFvkFcESjYnorgWehQtKIFIkiBIiEJLjRRoRBZfAoICgcaKBikxWb6MKFAkzYTsToauhVoLkkRLNiWEuxlkM7PUSQkh+ERPQvRIikSkjIyzslI6gWkkdik2GJCOBCSFHA0KZzcl6CbIFMTcWXwJnoSE6Y4TWikkTQ0EYCU0NIjPOiicjSxRpSKEKRaosR5FEsTcFi8/ejCk8RScsRn0IzyWehej1qSuChxIpE+y9IJkCPBaFnYzyFBntpnvqiFBYkxcEChA5BS9iGdkIYUC3r7EkJnmNJY7OhOEKByJ6JRIbY/QmSSiar7FIoFJQ5EbqFpyWWIWPJngokKChQbhTooRJSKUMlohkd6IhikvRaKdVJRZekvHIpJaemIUFkiILEKIFIpJZuJKSIScjSzvSXzoaOzoz86MwmhNEogSiy+DgW47KGhImkSkMhaOM30RFcaXyIUiFpYiHqWXGihmJjLQIbP4vgisgeiYgNMScDkcigUlinkQpJKPXSiNCT5M9xeSs3L10UU3K0IzH67EazkSRKLELayxArPQsWhwQRRIkGyVopIiaJJExQMxSIWKRTbKLjRSWIRtsUQkEKBOhJp6JsasnQ2tFKForzYh5sJVmwokiCgpRQ0uCBCKLEOSb0WWIQp5+xECSFBAQpnNxJCEnwJIgs5EhXP73FEZIy5JJCpHmLasoorSURQxD0WiHokxSSELPQWqqCitPQrkUrOdFlFepD/AOUBnm49oZChQOkaCD0TJQJZwSWjS1UFlCnSBRraIFaWRnJYhDJCISRWbkMVZyKZFCEhVnZZsyTf0MtECSJSbl6WKdUKBCQnFDQgeWhIhn5ElmxQp0SGKSZZVCL0oW9YyxSWNzwNifZebDjOiQimJO31I8+BkMSc0WWQxwLcoWiKBpWRohcEMsRQk4LGixNyJMSQkuSixCggJJr7JQ0aLFCzkdiTni2IWeurPnGNWNMgvShC1Iig2HAoK0UCgXnRa0JB7LFopIzghBRfAkVGspJIZCGkJ8/wcs4Fdp52K2QNMnpF+pAiGJLkvSGIsQozoSyEcWJMohFCCTERedlSJqBkJkYIxZXGhSKZH4IxnQ4lwQ9JSKOSGNf8KRCCM/kKCgxFFi20UoQkKVooKzoRSXpshlGixNitUSoqCWKSyySyrNxv6fRlzBFlkNZxoRQ2nIggSGxDzY8iM4JENIEhgrWhrPAj4+5OzROIj1+yWtvolFSKWQ1sLYvksWdCjkUCS0Iz0HYrcjS9CVI4IssSYkJMUiCCksYhzoo0dCjSiRMlQQKzsvRTwWLwQiHyWWbyWJjECORQkyTXWmcthH8fzJBZReb/AMk+M9iHImEl0QiUUUJEFsSciTEaKIa2I7/slm405yIFTII6xCaKKzs3IZ0KRUJosUF5sLyUs7GIvHT4IKLJLEbPWxJCEIxQJ0VnYkxFjWqRCEaIetlhf8QzYWiGIvSjYiJJiUpyBJiZuEesCtyIOPFedf4K2/QikKCFm4kFGOxJcEISYkzshCRWhQQZAlj+Sh1cDOBSKCGlCkhigUENL1zuhbNihzotEggyRmkODjQpFp7EP/gzwPSh7EtIKFC3KL40OClvqRApUYxreRAo4GCJMId1OPQcbCRBssVngjIZxEECUhvPAyaLKIRQxGXnZxkwVm2lrYSRmxUWSoNnkp2ElziKEhRBLOhTm4khEFnItJEIeKEaoo5IegiEnolohZ6FCgSFBGNEUOBSbFCPOli0WiJEKSCFtmxYoqSBCdlINbez8j3AT3IoTsz6G/JFCxIy5E+dEsZimc2KHnYyTItCEkOZzEJmf4XJNPNhJnZKjPYUxEktOyozcrkWPAvzosSgUyKIyBaVzj9BCnOyhIDyWhISRQhGJCgUkMUks3FBXJzqQo0U53r6imbEUCnOis6M8inRFZ3ogWiTIRRZIS6GW4EpyDLN5Em3kiSRL0TFc3iHWz6LzshrOWWS3ZsQ5+htksdJUVzkkHXf6JaJWkzpyJiejuIFwE7FOnn9i8FRkfQkhZ3Iis38CmRZ6laI0IF8IQbq+tGwgkxIQKBQQQWIcCgUCej1xmfwPRwLehigUEMvgsRUZBIcGedSeioZFp5j3JM6xCgssktt/wAQRJTZd5RS2JbEKCGdme/JVZQ5GU5FEBkJpJQ2zhYh0TOUMjHYts3LKiCBWinNyEK+dCmckUCgWl/n5FGkQZ8ieBmCEI5ElwJhQIov+CGIobLIiEJ5yIUijNhsimPwSFGinRb5BsKdLRRE2ZsKSz//2gAMAwEAAgADAAAAEGv266wQ+iw82yUNsS0BVKw0+/rDmjjtPTdLjXPvvnwh+v4wvB/t/bUTYxgLfRIBk8+Q3d1+vvrvOPLdw6nv97zQCQE9fqc7aFlrjRYi/Pagkw3782+aTvPPvtuL6o3WP702wQw8/wDjew2JF7/yhAMMIOJGdN5PlL1//wDd+3y+6fo88NhoRMNvpjNdiB0f8889KCCFSsyLZbe/v9876uZw0692sgxIXripx/P9EL/sR+/98LDcHbAo+XteNqWyJ02M8waAm6yEkiH6WZ9LT78+419nOKMXOeCe/wDwn9DHOsAuJGsvOo8ZDvgti56o450rq+fdwj2rvst6moApvv08GBuutmigjBojimiJnA1R1krxRXI2kFvU/r06QxiCcyve8hskn6zaY16ihb0upsmtls/wah+YCkIxu1TVNe4dn/1IIjg8988xRw33urfTljK9do5Fr+vRjpW2cS/A1f2Hzp+wUIpwxQkx5Y86Ay46ZYxw0i/zz74E9sb21ZOS12i7Z7yseYHwwnI18eJT62s86gMI14XV/OI7i87ITx9sx4Y/+wxjsvV7mwAf8zAQay8fpVl1Z5o2zvp3tqAiA1+zWyy86lGn2hi+2G7VfQUA08wSqi41wvxlSai8zMAgy21Shp6r2xz54UkhE4CUc+734QS0Uf8Alm55t/8ADYKHDFDRiw1gTjL0jBnubjC9r9i2hJQnT/vBvzbSv9rKybLYuNLTPD5pTXRB3VzrzB1OWKvuWv7rRHVL2HBVqjBjtPZjV7OCH8z+7uzSgGjmJ5BzrW6LoHPbDB3TZx3x9QfCDz75pZIY4UgqO/rJBKkTiWSFzDf+iX7n6ibCTVbJOUuijuBVBO3UqAx8kZnxj0Wjiz3PTNjVbuPDTLCLDWHlnGC0in+ZB6DTIAK8Q2tFLXgsa3X+TqCpMBq3HJZBjDRbffwbQTByACfLvOvGEuqTERa6+hfxFIRkjLrTYBTrRT2i5PSe2CGAZzjGPJ+uMgczclipyOBCzho8QOZbxHxbDvl/rFkwWymaQqUDNl/i+qecLmvnuueUAjCwtI2rBT/vEVTIr9PLqdMsaaRdvReHsc++pXjfP7wNinWLgMef8S7/AKlgjiwUNMVqKsI4wEUFv/5sotnq4iDZQPEaXNgbOsSD+uwksPE2IvFliviCgIAJ9zKiJsz13e70iCcY+E5s2wC0U600lFgwRrupFBf/AO8Qxk49MnLqE2EFewgAhBhaTH4M8uVJI9cvcXNeJZb7BPfO/N8O9Tkm+MPlbrB8YyECLm+UMr9UrzuEN/FjJo+SuCoI5eWGT3AZ4MX/AL6u++68MNJWPJRxuehObTzbZivs9DFDin8hDqMF5Wf7XrJyn13fLnASsUpB/wBYTsLdAaWQ8wS966orLtMdbGb9oI58wxwOryww77b/AGtp2MMKpBtGEskLVPf8IPaMrR2y3z+B3IPML7tuOmeflbccXm/KuEQLsygCUe+v87qheydf3n20ZWIrRNb5FBIKRIus6/KVY58Zo+td3OorP2rtN8b3jWj2964D9u1Ib+8AEGIpdEBKxROgpOHduoYKAd3NiV4ZBRSfm0aXaxlHLbT9nQ8h/wD+WDCIr8sygp5DzL5DGagWsmWLfT91Pn++OWiKYi3vdLLUoyyGrrjQuPKbSyyif8oAQqC+PPkIBO2CiACEq+PVvf8A20x0ip/j0Vv/ALKY57765wIAIkKySCq3jcAO5r/65e/3lN8gMq4G5btP6MIsucoJRp4oEKRQAJdfkWutsFb8u1Of8s988J+tpLHDXpQFYIXc2/p4KFIQAIAW1+IOisYDrMgdeevrElVMC9dV4jU20IK2E67z4AMIIYIIA/8ALL/WaFHLOKiCXvO+AWAI6Ce+bZzaD/X2u+6IBCCQAABv6RfSP2JCGSyz7CBJOL+ABHBsmT9fPfqbY+doCJASiMAA/gVBTjPVvpLoiEoG7A8pFpGx4CTCX3j+AoiisQsCEACAC7rOH6PdS6MrszkCfcRsiGDV/wBB1u28mwBItrKAggiAgAEb7ff/xAAdEQEBAQEBAQEBAQEAAAAAAAABABEQICExMEBB/9oACAEDAQE/EGHD8tLFycLclpNLf9hj5wKQzmlNJayrb/Yzwddl8zv5P3mX5Nsms8fegjIciH2zq2y+GbZHz+ow2X32syK0jhlt9kknwht5/vXuR8h2IFkRx/oswJVdZ+WzHX6n7afB+2tgJjIJQ0vogyPBc/OZ0NhBYJD6Nv8AHLIOpsmQLBk/Lct4ndYQcI6RxyOmTqTbM4cGemNvkK2f1gzg2TIOZ3LPGWdyDh+dBJVkCHSIMyExtnSWY2S8PkNkGwmRYC/UvDZZlnM/qycDWSQDCBfjCHwt7O4f2VtGRAOAFmMwCAOBw/r230myWZsl8/W0sF2dJEbJ6fzJB7753o+v1BnAQNuRFGzEeS+ihYhE23bbZYd89t/wM5tuw2Pn7IfhD+TMyBIayQQWSSSdymFWHLchhnbKG2/Zbb/EP8N4C8Qpe5P3DHZ5r8bdpfvE2xs+wEn/AE4222y+Y8N/iW292W2GGGzZk/eBbzI+cDjPQP8ACdh8Lw4HIlW3v3u7Fsv8RhIN/mbbw5kEluDOHyH2Wc3xvfvgVf5Dcl8BnNh8ZEPpht84sX7ZsfPC+/wdl8Bfls9PA+vnT7fOrw9iV+iPJ9sssgjmcyzy37DzObzJGPnj5ftnkfC7/E8Ob0+wbampnBs5nf8AnMy33ttsKwxb/NnFt5sIg1ky7L9/hnl5t++P2/LYf5Z/Adbdltl9M6u83wHA2S/I5kc3uWf0PLdZe54H1mfwH/CXjwl8Dwzwz43I+923h94fbM6bPJzZfO8JLOjXzvQtJ4FvNt5vByHY+9XmeW3f5hHH+Gy+Qk9Hllnle/nl6EGwZ53jL7xvkGyZzJM/jnlennNs8G4Ms9b1+Wc/MlkEjZ3P8zeDPPy2+t+X5Lz9dptkFsEQflncgkz+jLLLOhsfLbe7ZZxvh0eDjH2Tdfy+T/kSq637BZn8n+Z2yzwDpueM7sX021h+Qylu83JeCP8AECzmej7HPB95tu8f4MKWuywWT/E8zznHpjucbwlv3gWTZZFp6b4fnMss5n8v1BkfwMgg42dyCzyz+OWWeHhzIIg9ftmcZf8AeZsmW2w28zm+Mgf6HwEGwZHrOfridX0LPA8kz1tvdt3mQQQZH8Qtv3weZZ4Dm9PTfIWQQQcD/QPXme84cC/XnLO5wPIw28zw3zsO+n2RPcsssssss97DDb53u+N/kOHO292Pv8Xow+M8bbfv8hwLLI+3xwbbbYbbbfGewepZYsn+oEHP2EObb0ts8t/gHjqc2ec5m9yyyDb86feHzt+eDmQZ4z+AO8SScsksszmQcyzp8nPAfePE2zmR7Z6YiHrJJMeZZZZZsEYSbZb0s6TSTO5zIO53f4B1ouFkl+rbZYfIy8DbMjh2Z6G/4gv4ftkWSSO9WPkNsuW7bbEfJdYNgOESbJkkH92ZwOf/xAAcEQEBAQEBAQEBAQAAAAAAAAABABEQISAxQTD/2gAIAQIBAT8QQ/X9hl/hAsBfSAMBs3yfJxMkkMbYW9yAgywjy8vOP1n2sMj7s8TYLLL+wt3bA6wX2MLb9bK0ekm2Mx78hbbLLxd/0ZPXIeWWX9j4MGGMTAy2euXmWe2cQgnuRZx8v2UId5n+R51nYYGR7JZ7Zx8gjzin4QobDLRxsGXTT5flntlnPzj5LObdtzJM+s5uWwkqYdFIdlA20sdlsMsyUYOKDYOkEzMl3jeJvN7tsoGsE7Y9sl+z49222Xuc3opKsHIwSFwsyXIfMb2wM0dIgxjbDbZxJneKERM27Z7KFnYS+222y28yyyyzbOZBsZyTAkrOtjB7JtoXvgiChkK9cM3O53Krw84nwyyyyyOP1ZZZl/eBBZPIZFyF23mDIv5OxwNsI4S+w7LbrbHsDZxjZ1sssss5llm2Wc/ODbkuux4whDrEaOSa5JkHwcBXIUslBg2zOJksescsstWfLIOLBz+/CpCyZZIMC+EGSO6zv6Q6Y3sJcJZCWGwT9gDJk5kbYN5hxsghtkFlkFnNj2RsmHmdHslmSnM2BLF5JYxHvbMlgjYncqR5x4EgyTiSe9fvwW9HONkHc2DYJJwnsKQ28skssSFt2TJ9bNhyXZ8h2G2WHby9e8zq53Nshyy/bL9sjyCyZPZLIJCyyyzgS2We9TTSDLc4ix4ZJtq8ucZZ1d+A2TYGzhc5XGbE7lk2SCS/OLw+w9IsLf3iBAQEkl5MJdb1blvwEG2QZ1clZ9gknyGGVZbLbbZeftllmX51jsqT1tCdzrLZFv7fju7ZZBBnM4p8BZku2Q8Rt4OdCCCckksggkvxm/ds2z7Hhbe/loQkPtoy262y8bIRk5kOSDJlsPBBljdsOy3sQo2jaHYPYw45shxpy34bzOA8ehZfjL8bkm27DbkrbCwK2QSWEBJke2MGyBIQFgxxt9yH3vBty3uTL7bvhAtljZnNSG3jIN+Cw7fliwcGTgMiyZz/AJS3mWcbIMsskss4Cyzg9gYM5nGW8OJskOWjOSg4wH6dyz4fes5nNg1syw48e/lsjee7kOx5GrPAz4b+yybB78h8hBvxkPeHGV4tlnBgg2fJYMv2DL+wWWcZGfIZR+F5vyeuX5MRB3YdmZ5Pls68DIYZdjdyB/WzgbZZ3LNkLy2WZwNs+H2Igz1tj2zgcXg+27LkuzZfkSx5bBsQ+h+M42ebJZDzct6GQSy7zevwXJZ16e25JsG3hDscJMttth3q9akstvFsg5nEWCCziy78NXb9tj2CBP2SBs4a2cebbCS7bbbZsliSbZb8BBDMEfayXXbOCIFgjCMZIodebhKEKsO27wLxYdeHgcIgs+Ml7kuS62WcDYz1tDwh3p/Y5ttsmS22CerB7aFscbO5tkEEHDjzLLOvZl5kfkr+QMCWoyPZ8t3mWQXwvPMWDDLLVlmyJA7wss4F+cXLbe5NsuTF+UggyDeAlt3u28/Z2DJch43bZeMs6eW82/bODi9WS/Q+NyWPYM5ttsMvtqSCDLeFv1ze78D0s4ss9fjOnkrDLLerLuT5bvP78G7xd4Heb3ebzI2OrLLfr5yMJbfbVt/L+wzGySTbEsyyNbBsPwsly3Jd6yPnILIOLxZcl36JLk9ZbUO/eZOZ3LyDOLbLsy3qyyySCCyz422WWXfots2D2ySxHnI0hssss428xtRn2zvLLIPjbbZZZdsk+yRsGS6YQZJsHtvBbbbDxm22fbTLe9tthtttlttlt6kkllnUszn7skYLMbLUh3oy81Z1k+Q4Nttttsttsrb3LLNsk+H2CzYMj2zOLsSQcSOflsu3hLbftpZY2WQbZZ/izrfnR2yGzYOZZzO7Hvw/J9hk4w8yyCCSxsk5nc6yySTOjyMMO228CYngzLLtspE+SvWWWcyDh7nx58E2yyMmEbY9t+GwkuNvwpLMeyQSSb3+2d2XLbeb8sZPMkODkVtsOMOzbDkuwy22pXePluQK8b28sMPxs28Cf8W8SXIhyNw7CltuQ8DjbrKw5DrBJf3mS8HGe/C5LLw96N+8/flk8PrBhrbL3jNgkkRyev5ZYXmSh8/smShh22W3o4vR+S2dAfWXWyPGcPAKbJJZkYsEBODxdgZMlA23Y/cjjDkMpSXgcO7b87zZ4//EACcQAQACAgICAQQDAQEBAAAAAAEAESExQVFhcYEQkaGxwdHw4fEg/9oACAEBAAE/EEvtfm5U81bfBBak2ZvNwNsNV/aFHq5zr4jemhRTUB6c33MCtATILncSKZc1wS4cLU9luIt7zBTw8RQyF11mFlu0XGMwTEys+faUm+SXNrxDDd5m+IIbovqMJSh7h83RlIxLM3fqGltqtBB7DXEZ2KrEPK3X3m4+pgFbVvEE4jXRIc5q7YVnZJe7xKDPpiWVu4g4XxN8w/ExtZm5jjMHtxxqdDcFjymI0D1LmtD/AKg9tfS0T1Ps+n5z9vqCvobnQ+03vdQP9r6awM5id4h/tPyxBqODxxfEwJruO7yXcu2Jghw4iuIyZl54CHFYtXAtFN6rMyMzgOnmJS5rnEzerb7jNzl/UTaHRBhfYYFXOR/6QeFpMVPuxYfp4IQBhm/6Q71UV3E2m4EVLNQtH3gDeeYvcUNNZvlLdjOuIrZjOOoSz3uOF9VBAOfyjRWWE8xFHnfMJxauRjhfi4NvZdwrh3zxcq1OmO04fPhly+zojVHXUO3nPmFPiYigG/8AoltdhbvcYHehNQJdwfiLuluYZy5YFO4aZg/0gz2kq8cMAKnOE5gtD5zFauzMxfOI7qstRxu24xwasv5TMK6P1Nzb+Zf5R+36lzFifhJcfceuI+u5jqP5g/HX0GnV4lyqaXD4fRfCXma8Zg1G25FkxO+JkHW1hPDmO/tFQWHd/wDZa1kHn5VFwr87iGdmvmEpz1Dw4zMfWo7JyOIAA2FRXfm/M0FpSpa3xCMvHqv7jClaEa54cwHbWccwBLtr7y0B76zC0OfxBAhrmWk5XiN3cux7j3ShjivuTRmR2/iO8EOXatcR6FOIKONiEJWLpK8zc61M/VRD+I5quJpcq8b4jP5zGmLXywfbUx0qPP0Dh0zB2ii6+Irdw6+Z2/Mzl8Mf7+8e0dK7QajmyV1Gv0fSl5lRh+pcuP8AP4ly6fRr8/Xn8xxf0ee+5cv9wYtS7Ry0c0wW1mplmzjzDRTnErPUYy6/25cOSmMBF0/7LlWb3qVtYrUbW31/CDyw4qX8JmUXn7wXFi+mAo4DJGG7u5/GZlyLiLmad3OIWX7uGp1AegY3cLwajfHGBhUXqFoC8Qg+7uOimArHEu47JXnluL3xMq4a+UQD963Oe1LfUNeQtOKjjuvoVQ1FsfCJW5l611OfHEePMMf64YxsmbUXquME+ap95+JQ8WXFNfRPfOzuCwrLNt3V+pQ4cb6vxG87SvEUFmxuCj4IKIsq58aEivo4hj6eT/X9GX6Qjj9y8e4+5cYX7/Wn+EqfDMVHJxDQ/wBcqhl+lxWt8feJWcLvmWJxdTlasI67zF4C25i0QLzshsdBR/cKs3SjyRlujD16TWWfjHXzLV6de4+OALgjdvD8xS65i9HeJlkxe4pa0Mqn6h/GhlxMZc8Eat5o8ae0X5R89YifFsOa/wCysxrPiX8WEpwFfmOjlx4+JcNXxDXwjT58SvSMN9QqHn7VD9uJr531AR1P/Jv9QFtxCmKhvUa/GfpfpE4S+YLPGY+18Md4RoMwCWUFd2sY3vqDc53sgjdXdeyfNgxArHM5v6PrJPuiKGsP01SpVL0yrRifVGKhj19NYfpV/aI3zxBn6RfpkvQZYzM63H7bYZVzLqLR+JiXXJDK2mMwsH7QqlXePxKMdNVxDfwuFE6zQ09TbuzExo83ti2azk3Md+pW3hTFpVhWIhta6h/D7kF7qXDRL7YzYpa3cq1isQz4zUSs5DqDAweenmJfHMdHzC4x94u+G79zrsi53uAs56nE1MLK/mvMWu9LArV3z/Uyep/cPEFY8YnFfFR4ziwqZfzDDqVAr3AfeaR1SzFXbEdRb1DOcRL4435mLf2mD5qMrjrxDAc3gNwl2c5uGtZDBKns+ld41A55m8uoxXCV9/pHDviDf1vEL+mfi44ZdR3/ABAE4MsJNkSPbxOsNaljOc/EeQo7P6R83Ea6hNWqAom/ZW5ohQ1C5hf5isjupjBlftEaK1VefiXDarpl9qGvcDF6uZfl/wAzDa6zVy8Nuq5Iq24yzmHJnnMxeL+0sHcu1JSxywaiVx4JY95YxiWCWFXW1ggzqv8Aoiv09wVtkOt/MrSEsPR2gMnj8w1MEP31F5Y49Tx5gfQ5t4hGEuMuiirYPeKRuEPXzDZKR5hu5mPi5WbLziMwp/tSoNfNSvmKBX4lCdhiNv7imi2XBWwVNS6j5OSG3S6L/dQ8b2jxjxBV20p8JaeDRedtQOeGT+Y4H7z7+Cm4/omfX0Wpa+m2GNzj5htgc/uV1md8S18RNvhgh0fvACUg7YKjlmoRTB/cULthpg+ZNXr3LijnPiLM02QXhivXJN2qS0VBQQ/ylt5LBc3ExycZySzqri8DFCxhAGMvGbg1N01iXscu149QAviv+Q1pRi+IWn0rMZnK5QhtDEYTCnPP2nO2HyxZgjvEvHOYc8aZYCViZDQVq6glbZK9zIrOROCGC13fiH2cVcLRemVxCcMw+HdDm4OL9x06p1NN6mQdXEteUK+/i4Be+I3A/Lcqb41F29/RVB2MMal5/Eq/BHwhq/SYY4/MebctVBDdsJsW4VXiVC/aaXK7l52W3C5XiDGcBo7hhPDX8Jg3CL+5WFSOOpZOX4itPaGUfWy+iQ8fMCaiSqHtmGc+u5V1mMarO4rZZv7y5Hh1C1mj9zHYI+3hDnKErFYqN2orh8RRL0drcoIzeZgkQcsYFxaDNOVa7TtULsgLXl6II6tKP2Qe1ZX/ABHasjB12lmHTbwSjcqWVnAq9y0VP4SkKxx9o1fi/Eav4Z2goI48ZtIBhsXfiKX5q4NtbdnEVa2RpLvBfEO3wi1Y9S5TBuDqcUyS8vJ44jXOE31BKvbHHF83+IW8YqPF78SpvXE35WO95J3Y4mZaXm4RbrGWBN+LPESz1p1NvxOKMcuGUx8zUeKHF35l1g1FVl4Kjq62+IV3m5W2HT2TCsF3DDsKePiAhRjqXvMS7p8kXo4z6jqVNyp6R1CWaqm/wht7Ib1nE3mGvX0qmcZlDxD4Q5gcecS6GTWxhrhBrKSnbWGLQccxyHpGevtCY01De5Vt3cEtlT7zLHEducYxL2fiXX90WV41OmatwR5GVgMJmsviKV0YhivHHU35AwwQ3pWIUu7UQ3a0cQRiC+Z3AH2PhBpeYd6aW43p4/UIwxz1Am81Ymn5l2pWBt9ob6WaZUuDT4fpNqMwDSgGqdsZQaTONPlHKmlrOd3Vwg82wqpxeCFPjT3DT1DTvjpl2Avli9v8w7riLDjznTOXvJ/CbqzTctbgp7Rd8VHigjRGjqO2cipLB+yBrxfM5Xx4hHGazxDYZWvmU1I5zDAAMFdQZOmsSkidc38RN1ahX2WFdXaN/hMzo3Dt7Dc1J58QrBwWC/qGvbnqGK9Jb/Uu2mvodfZNffESc7cxV8XFQ7ViqzxmWlbSVFvPzCa0FxyPc43xMRcMQUCeE4NDqKPNh9SlfiKMMI+453YTFcqiGBoAqAFYG/cAX8BDlOePESzVDuCcy4XjwhsYTKi4VuXX3l3dx/CW2pWY57RqKnvMFnn7kRrtTHHZ/DKcmmYueo5R8f8AsJ8I0VxH3fqIKMzuJF+IHMq35izwG4r7FeIl5jFWj4QxfFbuPR8eIpMzP3eJknDt6nV7YqO91xAn4hwFJvzDTGRl7+IKLMruLRjJTOPuLkOMS158cQp6i8yUm4BdXiDqTBde4Qs8HR8wc5TL5hCjDzPxIeDioD5qyKa2Q9eowvwkG0NjQ65IgihTPMBsq6wc+3UL5AgOeyJFnEwj3UpmU2dI1HxGqtOIKvmExQPRVxTJnuExyjXLtIhM5Wupir1BP3FDW5jzuMqnn7wod7lh9sJU4+0eJt4hf+/MOIjXErM7j13qY8dVKsWsH8pTu3MEi42xe/hChFerhjEb8oubjcsd5ziDgHHMdreZTpjIsLMLtfxBMyl/+pZL9evcfCJY2zREgqQ/xlPGTnio7HN5jG3redwo9ghUdw7dy6FYY24t1Fsdhi+IgJhCiKcWxb/CPHK52EFMgYK3bLDzTV69QIHhxAGlIebPcv5/mZPi4drOMxWXPcd9HNMJq41zflPnKmJ+HrbAHssepWrcpB395rbeIX1lhDSZzDP3l6l2WitNwJ+9QAGtT9PD/uoAq6Yj4lpWw4lmaxBzHUst+DPiag9AOrinWmOoNax1C6tuY2CuIg7rOJYWMbm7xGNwQ/SLL5CQgVXcs+8AHdiQ69aov+EXIcJioV3qXl6cwsf6opPlmKNrRqKcxrWsqXHvXEIt2B8xWHeLhopYttRtbpAM6irLbcuTVVEv3dmjiJF4Jk4mIKJYpb/H3lQpaUf0hTq5e5nL0ge5v5gWiqljuQRdsBAoViF0Z4lI86iKMIZ8+oElqVjmUESsZ/qF1eT6+Jc7ZfW8gAdQgYywd/PcTAbQWe1t8xJjTA0VDi17ionBs/uKVoN4wsd1hMe4aiZEz4vFXgHaorKLCrer9REwabfPh3AcZI70R/cQsIYcDDDoXx+KlJp00wo6v4zFa6aXGMTqUjnlg3UXPcfe16K1BqMrDPLGbXijHtM85oruUb7RGftKjuJGWsa4gcN1TLW7vEZa2N+5Qe8whiOz1Hppl3LzvUbsfMX/AIjsjTL7NMa+WZaxDQyxrcuJhXd3KieY0K83LSnqUk8Riysu/UDyZdy+2iNHxUEGd6jVmFCkaPHDK026IIqUB94kb5orctWdrmEsnMPqQRrLmWHzMqZKjpxpjq5Yp1Uo1q5c25XMa/5/ECCl3zFjdho5+0AW8FxAiwYbFN0RzYNN3CFnDxxHCqpfMAAd4YC4488xSVIQZT5cfaCOHC+5me0tN/ErPJhhiGjzzHM7qoHsxLFXG5R9K0Mx6+IYuCepRxsjm6uJlFbv3FCqo168J6wBV5I048XLNkA6KKItb82cQ8xzi4zFKcxdvGzGEir5PHiVfCiBCgCE0aMMGNDFzDDCKJfr3PjOJzc+0mPEYOh8kKYbUxG8nHj6DdXMYz3vmDuMZl35xMdYYxGFCVvc231FTnTqesqOx5zMk1MjPqX48oRSY0WuYLDAsxnESHvqGz8SwvDYx0VxxUEnz9o1mm4wbo3GJWH7Ytu7cECOcjLvXW4Co1dfMJ0/efiZz8sGq2rbDjpAgrn9RTfo89ImXwaIvb05i2VyczZeCobBYOevCIFfaViUseK4MagvOVMxYZUtTK8zfGoLO2qqWLF4l1OhEsZXVyiVjEBWu5WXzG8X1LWckNx7epWS7eYlENGLu5YPWoq/cpeTmZviUP5g1rUX57i2ciGEFwkRpp8Sje2OWMwP+YVVZx8w6c8nqJdIgOcxS5wa8S7Hycy4qsEr11DjrmUs6x8waeZp1F/XELvDDLtnUWY4Rwyqnt66Rhb48xuvBKe4MVyZziXF4N3MjbuVFbqMpvNMdK0KhmZjyxKKGbpYAb8tx2WU1vjyQico0+JjFulMuHeY+G4/6iJbvCEAFu1cdw0XlgG4xmNfY32x77gQE3rf8I2h0ZYrRg1UAHODnuFL0zg5jTj2v8SoO32goPyhky0H3+It7wRVOHE52W8QeBp1DZGoSvLNxAvN+7lgF4s5qK24AnjCTAe/iAO0Hfco33GkDQz+RUVHaUsEvJKHwHqGetotUO+TOd3iEkNbkqKnGOYpyv4i2xmXnt1DGP8As/q+ilzXEOt5gqCS23n9wqGY0lXk48xQ/Mye+Je/iC0M6YPe1Qzh5xA231H4tMVHVfiG83LswONRR+4NdlxA8Rc9AxJvtGk6azEW91Dm9H+I6RFVhlXd/wCWl18sb5hhfGH5RVN5CePCNA7TLK3rkjSOarP9QcM1FAXNkrghdrIYKhi0YGJwL8MHL2q3lYCN+YFjy4i1sxQed8xmDnBxXkigaDDijlGiutxWddR8qMUaWg37TKa0K+ItNccxQ3Ri5UV6HMoyl3rxAVZkDHIct0Qb+ZW5YTRxVsGv57ItPhn+oquV8y57W2Nqcwa/OO+Dl9o7SxOzk5qFBbuq/tKRBG8Ep9Fxf2fvAluxrHmVhWH7QyhtuXGIGDVSv5iQ51mKXzF04gjDHhzmMN3KSgVP6huAtx7UCqMxdYq2uialbGt3xuKYjbi9xzNceYbVRTV313FGio1VVdS8P+I0zxpjfrNZZkrl+I5vSHJnysdMTtqKD4MJuvfUfj+SPvZh4hJw+Zpe3PmVWOc0wHMMLk/qXrdYfMVsodcRwGxgi8pk5myBniMzidLiVUVIzBzn3HD85PEtLiguNXqbOSD7VV3oij5VzzDCGbLhJzrUa3RYDDk46itGLwdSn65iG/wcwScl1EL6fRGQlVjuFjzUELvOW3pC0jG1gpv56jVX2lRmWldvEuHHmapS3j+42e1pK6BVVjERXZ36hH9RHzDPUXJjjf6RmKtdxApRLxFtNYjGjBzM6gAGXEYL+ITC3LkYpuUhoX8+o6ls3GjH9Et4Y++I5tO4aZxuBVXtn+P5T8Z1/wASsdSv8xEW4lxk+0Ua1i+YUMMRB44jhnP1mFHvMIK0xsvGvcI4x3DfhqVi9ty5mVsSAO3T2QLXMwOyBtdwR5wnUYKwlYYEHcVs0VXiXbM9/wA+ZRnSsI7OqzkY1j6fUOq7lkbVO8IWEaSniVu/UuzVI08nZFXsdxcDhuk/mNTm0QvrpLl73Z/JFX8IsxtYjoJuyDGrpHcLaM9RovjH8JSGQc/2go6oMZfKLSsBUKxEQuSYjGVZL8wgL2N1Eda2QiDPOYYPIVCFFcWvMNKWBC4OGWm2AuNMO1VsxztVTb3Bt4eOY1TJ74IGbcxziEW8dxE3hxN1dGFxcWdrhhyGEvAr3Kn9oQG1xKtR0pQi4zcYRya8NLu4kvRUUEzWmWHozLnjNx05XmUTvmMVIunU/wASr8NRLdkq2dSqeEI4o3CTlTRmav2yt9RBe++4TP8AQgCVzqLXpHruMRAVwR2jGZvbm5053AXm5meIB5DJxMF5IbxhCXLA/nMJeeDxDdeV/iNx7hqxLRNRB70wb+SI4rFadxxLDkjRz6IzOk+8tD3zHKc1B6PnMdPbqJF/ciMarHiL9aR1GDw1XqCzdg0+fKNbpx/SOrq8/wBwaR3iFmuDiUg77gi7gJ88BLlNC0f2j00t7IpUIC3vF2fcoPzuuItilIZgv1sl64J8xl8s3ULpyw8xcoU+PylW3Owg21hWDz4R0EZSua7gfxMEBVoHjn5jy3FvEJkBugpfNxbHb1EDmv3EcWDExCm4Xc1xiZ7q8wRrivvGvO5jQnKXvzWo16MU54m9zgdYjAjbXq4anAN+Zcr/ABFPWeofaQTjZuWhcUS49RR8zCgpWVkcDcz2XcENOue5z1j7M0ufz8Q1rnmotcHHEaKcdczT1RFOfiVvWp8kWWFth1pwz8zi08QrfuX41AKN16lrm1MeJXrox3cKcSgz8xhRisHmOi7OdR4vdDcPBVa18oE+EeZQ+z/oSpaatir4lClTE+YEnCEKBn9CKtcMEHMZYUtBGk4IkKh7OfKBWt1bAAo6LT0ilg2ahsU7p8dos9lQtV326jiZFNx3dQh/aPk3DsikWQD9L8wNZUB79MY9Cltt8YSyuea/MCDnmK1ziGKXtqIcla99wB3BxqBL88EXJxV8eCOL04gBgtOIdAAXl/qGI2ngYIYWY9wpvpQDU0aA7zGy1YYHRGNbXg4gDNNYYA4qBanDcozkVN7Mw7DLArHOk4ggumGhyftGHUarwxENuhzZmAYjUdni79R0Ng08MKVhWP6TAd1TFHvUNlLLmG3q4xzjNxmFUYgG8NuY04y8Q35yv4j9IxG+ZZOf4l7utS1zrNxG8eDMoxqVqXn7wy3Jh4see5k3KoZ1KF9RKlRz/aEVu++IMEWLzADf9o6NENottQlq3mXqzBdR1kqLvcGAMrx08owPUNi9Vhh0wpqu5Q8ZYc1xz5mimKEO/mJxs4hdwVmM78CnzA23vKeYxqnnplRbpc9u9QpeRafyIRpi2x/SVhcWRIOTLB2XGrhFOfuys85uXUXGiGsqmvlD8W68dkT5VN9+U6zH+SYlzbPHSVNKG+uks2Zotg4jwM9+EB5IsLSG1Nxnhk2zigDK7gxssH7Q8TFlQc0rdsU0vJTUv3mZPmUByrL5jCJXWUisN3URbU3G7qa/B+IJz1A4ZYnsrxDNcvxAyvuw4Xj+p9v+YLKrGZYj1+Y0ol1zX6uGvxcudy/7zKMdj9Sr2feAa1oitPOYnzFtwFO2KalTyTKm0xGtwsU9VOc5iLndx1m9V4hnGY2Ki4VhmNDT/DEOe6lzjLrwyrXNZgzxVS48y4/1xqq85l2HiFb4al0PpIW+MJaw23zVYi3V0ZItzf8AmUMJfXUx4yYYYP8AZmIFpp7iIsS8wW3vqCC49ZiFzRiMeBMkChVCzMC2qd3w+ENlXSrH9ooNRl/kS5UxdXoOR6gGVWAQv1l8wRWFCnN+DqP1g5vjwiHQ5rqBwsAB5vMqbYVf8IBuG23ohuQePcHcBSk35IkpWKD+UW1aqh5gFazF+e8cQV8msxji4OItjjr6TOPMY+ECGM4xAemXLzLkM3FrVOaHceZytceIaMal9pp6uVHk15jgF7hbBxghDmME1FlziUPZvphgy6lwm1ahmtaiRXlXmXNfH9JWbdxJr4eYajLNW3CguiL9fMKrjsrcsXygcPcUebVQTnBFQbwDxlY87yw3gwoxjiNc5OYCKOpqb5l2wkEG9/MW2nq2K9ZQzV6x6iBV0iFtvqGsaqGn+YNreH9RYr5SCnGbz6iQzF/lcdvHM1lEapuD3nhi1QWU+YNThwxjTa3fUwwNzHim8kFDOSsVhhxH1Cr1tv8ASVgCXmNH0OIaVqzh/aXvY0PnbMpzwWdaZS4AvFq14RWKtF+0NlBVvcCDq0EDxzmI0wjb46S6PdvUAeVo6g4bFNw4FNZgi0q+3wmTqm2+hhFq8ED/AEipVy6+fwh2b6IHBZdEIxqy2lZphUrbvJKQOjhYLXPbgjGt3cuzvhl3j6CjcP8AfMuPMVC6jyTMFzOks5uZbHJ5hI3cZLw/mGbyxdQZPOHmMpuyqIBTmquBtbpi9F7h/wAwxvUNy3zK/KGVHRDXN4gmZ1bKnVXufEuJjPLApwXXqZ3a48IwFar1LMd6ls869w56g3PiJO10+fvEVV1XUXCcvUMagL4rEdByxiFS95/UrK05jv5jNWDxAHamFpXJYCeIv4TUaa1HqWodO7F1uO9wl6xX5lNwe4vss/XuWnNo/YQ3jaVrzWHi4crsNOEhnN30xLAKpPbpHMBkS9mZuNCAy3gx3CkAE73Dmkqh+F4qPQm+VbbhIbbxhmF5LGn+URmqm+3lKyvLf/JRALXLLhgbNcj3GFPOZoqhYgCWuW+IxxvMdnXXmD2a3FnxKkfRAnbmFC/mLy4cQHU9xcXIjjPqXVCe/iIS6pNRZ0llnpFjMSd7iuhUFSGM5hhfW+WUNneIf5n1LmrrmLFhTpj8rI7HRjTjcVo+cxMYgtJen4hnGTUOL4uJNbpbZYohJq4hj5Ri2i+mGMONQ4vZSxGorNQaFu2mOnbx/fiOc3WfURTNG/LzCjZHY03MU756gi8u4qPC7ir5IwfjuO2MHcZ24XgjecVmBjwP6iS8ZYZujBY1CjNm/U6tXAsVsR2nHcPiVqX808Yzcxr4RvpZuKO+Mo7eDGIiVxWBgcesQ3dV8sUKyI13HS5XV/8AUFWzZkhXjFC39oo1Ima17gmlg099IZvAKeGZnbgRR8wScGpgRe/6jpBcmoyxbjFRxV8XF/a/MbiqroePSEAXZVcxcJgGZ6lYKrHqaa5lye4VQyULGZYxfUGs3dzDRTiuY4iLA8TrAt7gBexXpxFcOdxtFjj4ly3GPtcJzzmecKL8RX6x5h9vxF0+QITGr8y+HFY9zJm8fH0JUN3Z7gsUWG4CcY3G1d++Jc8v4lXRxADh3liOp1CrzcHLw6fMFOSzUMh8QTBC9RZx/ZLGnAGW44K7LGHTGjbFm5dta8IWm++4o6sTLDc2OY/V1n/xK+KoS4pvqOw4jhBlwePLBZcv5jvFalR1hv7oqLfL45mOdcHafEvKrmF3e7jNBVmrg0xfNKDEyPqr/mV5OMRFliVdagQAkFobTyRCZEvXMCxvybjL6K6iBcMNLwCZurh2NK4OmXnSkIblFscx5dBuGHOM1Lt6ZWP3gLxC9eOZbisGL3BzVXrmEGCCt8+EWLsdZ3HHkOCChuNAPNQRYB2JxD7DdluiDOqpfDDdgBVA11F4jBurIIMb0FYqHGFld5rywrjeBYLYAz3c3uDUoIncWy7f4jBC+li2pb7jmp5i6bvBEPLf2g4VTwx4HvDGt46eGUGRt+ErP5zOt+dwa1gvMaMO5x7LrMRkPdw29ZlojhRxrz6Yot0vHiO6XHrZ9R5LvGbmOBZqEarN5hGorVm/mNLdtRovXUorwKiwt9f8REVjFe5Wrt1O9CsQGtkPbvEFwcvUZzWYKFmpG8yMU6wf3G3xv+yXeNwlW1uSBQBriYq9I6b1T+pd2W/uKsj7jZ5tgjVQpyV08RenNb6j5kcSqmlQ0Us7lb1cLRFpeYReppt2Qpni4Hkjz5MMKr0aTuHc85HiZADo0keJK2G67Esty54JS8s1iDNuoR3kcQqVgW9eEtIwG3qD298naBzTbEFymQxDHQC04+IjNuB5l/wAnzAV75fgruMKVhgNR5svFZlL0OMPzNuWx4IfYtpzn4IKoryEDrMFvNRNMgXnEOl5VvqafLWckK9C2OdSoTdFeXhFsGCore0LeeSEYriKinjUVVa5hzrFxd5hdv8AsVrmvhGYq9RbZhjQsyZ8RnvAwPzKXVeuoSZHfmYnzEUyzxDGc5+0dn3ijks1AMG93xF7Nsb2035hYHcVxY6f6jJeRg5kq+9S0oNc8TC8aFahVTQ+WZcPmG7cof5imn56YElvuMyHzL9YN4xAbkH7+IQCg4hA8YPpLZ4H9Rx5pb+UrB+yP3h6cDBNADzUMsUXRFfk4Y1tYcPMoZycRa0XqGbzrJX4TOefqPpN3W7jyw3dcQo4zUEXdVT79xLapuwy+kUXZyzFjEVbl/QxnviaeqjssVn3LENVt/RLywlv8GWgtobKgii+Xg+bi6U1xgxuWvn9Kgbai64CLhUSzaCTAADirtAbogj/ACi3rvb16nS23ebhM0V0bmFRkVDXgqBndpEF9Alcsvdifcg9JdjFgemVjVjVBxABpTioQsCj4Zbll4jMYs+/vzFP8uP8oI8qh2tQbG0MCkMfeXXzuDuGjncPdCjMEmLt08xVS1v1HBa2ldSjC0XOPKB75s6gpwVV/wBoVuTNzdBdH2izrGcR0BhiK31ZD0oW7ShqNOOU10GzvMH3elq4RFpzHGQz8RzIU4bg1DdxRv5eYrwHfGYY3l6xHYaTfki4G1XjEOGN5P0g8bZk1AtGnEOJ+eoEERK1cFjLEY6iT4YBKOMv7hG461b62SnAv9kKKwv2hJuqa+8Fa6Z6mrYgqY+YXzg7cEpacg2seJbqVYUZ1pjtXjiCntlfSPaq2U2DUtAGAt0WhQFO6qgWI8lM8vpVqG+AtLwQs0DaGfuhAwRoZR83pgHoW3w+I3lDhxflG0q6mrfZFBWxSMjyHUrZVxa4jB0Lq6gngJYdTM6sQIOKi5w1UMJfbNwNSuJkUs3jMubBaM3THisavN36tmbSpRnUW3Zo5z6qW6Wd1xURXfWqlHZTiO7gAhu5r1BA+yUnNXDEhvEW0wc549QF28XUv1lRdw7j+KiDijjklWRp4P7REVizPUU+bl7A1CsqsYzVy9lZizZGKENDYX0gKsi6sY5vr/iVlEZc9xLQVWzmM4VccEML0UNauJ4DcXDFoWnEMhoPcHxLyEpOamMxe6sVePhO2sImRRGOUr8+43wotjxC+X9xcFszZ3LrUa/y4KXq4dHlymK9wwMnvTFkGuooFYvMdmZuJXxYUae2oPfLKXUHBiAz50bfK4gvQ87lNc981GYVolrVc/eP82DCcGSNr4VGmX6KzjUqGtQoBd3K7GGocNWmM2sS4SCBtLw8sPXpCpxrcLwxTEPjUkw9Bmda9kFbbw2HZAgKDOeDdc5jdKU+HaNntcw1xWcxFClvXaV20MXuBxopXhgptkunmVvLEbr1DW1LGspoOafMrUlvm1gLA5TRENf1pYwbHPcJx1Kvx1HTk8RWzD/LMqS6/cvc/wDHmWiixrBCOfI8xdKCVn+EtdVXqqigVkvPflLF6OczdCAHI5lizb4iAFyuepXRUfeL5dYAWVrQyGSo7amnMsJnP4jea3ZpjAAoacVXqNrS7gJZczVTOtF1yIeGFckQaDg1uUx3WGwIsdeOY7EK45gwm1RXbI1GXN7zqG13fCVtBlrOPSMwW/McVmc0V6JgNLu9w4rDuJazN6gd7vHcISmXEafmpaHAYdxsW/vDf1zMmVBu8wfQgALoYaGAv7xXxurICHjLCjrKEnLqOIfJ4jr5c+oWhXz+IhsQhX/ZrxBh5rcPSOV/S5rjmKsQ8QYcU1xflBVKbCsPjxDjCCq0OBB7ofvFptGBUq94OWrXpUUy1e9XcGxrnB+Jg0w5FlQGsQsNGGmkyeYZbtWb/uKqeNeoczh4hJqh4mIf7JT0jiO/tePVzUsc9TSDvq4mVjwRuzxslDQN9ZgHzBT+tSujKBXqKM44OIfkOvK+owrDmFwyfZjKcYzA3WoBx88xV+ZzNkTZwEWknWSvlPM9QnxqI9BA/XdoqRe2hXLR6HEq6Z83FOWGsQ69B5CFF6VAnAhbLDeQwbSN49u5czUqibWREq19o2wDOoJtZWcZjfKoZzX2jgVJV5v5m5QMeWBKHuFzzbsupXWzpq4QboVfNwpVmlwNR9rKiy2m4yXlrUXmMdOj9yxDgf1GYYy31lFFi7zyw52fNxOnHUOMeCkSuq6IaAwOVMfDEBW8Q7HmZ2sxr/MqPzHiLiXFEBdueYhQRC0C4dwJa0nYpsgTYsbKVqCDopsp5oywGQLRRB7UxrhF9ygLLWEgXXHW33OQpUsI4RwOIR1oH8EUstOYlTz3tUJjfd17QZxAKGsPuMrRtbRTMutldj2l461Z16lbS6cwTWKgpWMUkRjeLiFuUJe9vUE5Glr15hvUNXUXZNadMEVFS44ijq6joYCe2NqjHxcuKo4Dv3BPfDNb+kpwxw9xLNn+YhXVY8y1QnxAuWk+0Q1bzBa5l7fcqWcv2gwfYjM5YdCreKgNkDfiXL4KykE0gyvSVqzt7hUC1aKgjbGu4GMtCYUGKqnCscw+gX7q4LiYwJtjNEnLkHvqESn3zLngO9wGcEQMWemI1hLpzRDlclOIwA06rT/aJaUOqsCcS28wRpVlDZZHl2GaOYvWSZKDXuL2lAyYojq3eG5VAXw8S8uagDGyWgDDvTC45LVOeYLyTxuI3Za5iAz6IWpk3iIi3rMDw1YsOWHjGDb3FmVQlnmWjwwYbAVjzEGUF5DbCYUAoOukeKYtz/AlYYKTKnyeWIC0jSuQLeXDDiWbFcAE5hIZKrudBFiEQFRYTlZClCDV5vgZW+A5dnxACs0ljx6RM2Q5YmmSZXfEQy7DR16hEJvTqDe1cdIl3dw1XC/z0lW1XD+DMqSNh10gLuAsdMwpu30nIjgQtKYVI3Yl1uvcYgcMdS3PCYe0XEyKjYFjgcjEdoTF7hsrwl3xKs15REdZ3xGK/wAysGwq8JodNHl0lRKvduYOLHeKgR33n9QbfsQ5nPMKj8wrVllbeu3zKHlx3TcTi2X8wxqBaFAe5stM3E8BLduFrUQd24sUKuKGMUGWWIeU3SalKOi8xTwbrjUbXZeoDy9bmBrOf6VGUVHMN4K4oxDKSzxxcTOWjzDkAGm+YHb5uoxopUFq4IvFhPF2wCuLwssG3QXW47aW+sxyDQn29TEUT7vW47dWdcxxyp7CPoxr0grxbxjMeeNGsSowI1nMxtPjMYCh7wRPFLZghb7Gm7yxVeysaPfjiVqHliC3NNdfMymFEU104ja+THaMwFcHg5hvgVnOfIRWLGa79I6lvII0eUK8yAykKrL7OvENp21bvwXxBqFXs/h3KTWOuS9HuHRyjwOWEcQE0KK7Vdkdo2hRycbSrFtY7CURe8ZrUoclt+z0xwIWQSKc4xT1EOqauL98C3qMa0cn5EFPnmFbw7e5xC9h2E6OVjuCIC4GqsFpGBvYY7/7AsXcO7ffEWf78pioWUa3EVTI5/pCS7sPEp9rChAq6bqVUS7rPMCDrHeYIDSKTiEXXhGrsTUdVxjHEWtWjnMOFUOOGKYVF11XSAugO6c/LEaQQOgWvsRz7Itul8W3F8o6uz+UbNpu48Fso02gNSIjlA6uhsKNeo9E1ggp21m8zNzHG4v1LErHXmNnBgSGj4mI7rvZAK0OP+R1rOG8WSo3jHkDpPjfeEyZuNVajvdPWZr7dd58IOZjHNyplhkYOjSm8CDRtblvUWOZvzfzUu0MGxSVqUKxlBi70a4jOJ4sr7JhDC5rBEzuI3a37jxl8kKS7E3ySrN5qwrecnzCymi8bi82m5mXgrqYWtDVQU1bihTtl3CLo2+EZQDeM9R07WBuKFyO6gAb3T2/Mc8elf44RZqlT0n9zKMrdYHk8xN6DmjLKt2K3eCYQX6OR5pOYP6QB0mFbjOaJn1yb6PEWBCokr0F+oAVVWK4jjcCAo6ID/ctRsaYuA2DRfEGO40+IF0ZMHSxpDClcJZ54IFuLsjKgMQox1dR5MRmaax2/EY05ojTOlX5hiKU9zIubcOPulxVWEo3hZs4gmzDOYn2OIUa2mIp4OWMf35hkSs4eY4ynTxBOlH2kXdD7TQ8GWKUw4vN+pXuhO+YDXbdW5CDZSUcNHwmkVQIefcW6ACi63FWxR55ljmvmmP0azm4pP8AgjLWXPcqOg2wa0aMQ24Q8ZOIXNgupiaQeYQ5wmubgrNi7qWKNPkhkSG0VnHVRRtps1qGZ2+cx8dfhLqUF6bhbZeKnNCmqshBxlu+qixaTDqWkAmCKerzgqMLw2hopXs0/hKntX7Q2FntjsdbiTxuDKNkoMkbSyjH9Q8KC5zuFht0pdX5RpYoMHEfkrXLMcKDLw9RyQVx1K+7TxK2m0C9PEZWFc/EeXoNHbAy8Lw8Ex8Xl0xu6a6GVOTQGPA4qIp2F4gYXlcsEL0Zt/ZBO7tVemMI6yJLkPhFFq3P/Yy2GmBB7/EGroWOvCW604HZ2ecQUatyDB0S8NrdQvDVuViqGTmMyu9Tjcuoa/siX+YitaGomwA2IP4U11Gt5/KKeobP5i2rou2Ou8k3cMN4jRWTxxUq3XLEu7uzmNZuHBgRcMm6eWOGQrjTLubW6VyRCGR6xEjkLrOL80R2Qpp9S+HzRVeLjhuzdDxDnFjyVEJx/EcZK6ginwlhRy1BFTylJQwxftX4l+z5jeRLxLQVjvUObYfiDm6FzCVXxduo7W48ZgBnDpxGU7dYjMUO6Rv0uOcrBsEtiz2FRp2BvdX5iytgXATAGhUVF6oIaBqDcnJuG0AveSxgZqzuhjHuWRySuiltl4jW3jg8zQNu5jTbiCnO0HMPXKayUQ3trgviGxwFxgKh3ccSqX78pnXeAwMSwba8eoyOSaIDsRDNNvhMQ00HLLPoBrlg8stcRauKp17RthSsvjzFBrAKiNwprjFpEca3c3xtaOblmmi4hbce05hrujUVS4Nj/cHSDb9koJyLXflHldOnqL0cFPMV7gg1+UVX3eoZOwUQ5/lY+NBtZ3KitzPaqjTiF9xwx1cF6NypLqt/1Ev3YniXstZ+8bO/fMOG+4XFKCGqo70R6ZXkgRLo5XuMCABm8FxzjLpdXADM6CIUrRd7EHtZyuj45ZVULNLmLOtl9QtVyADMR5SdRemJomA3KCV36g979ZlBeGVbjsQ1CTKJfEz+4l3BxqC1rG43uxydRU7iiLj7MXq1HucXEHOTjiCl2CiXqICxrV5+05McHCDGBXGKqONRrJxEFQW7Y+6JmUHmn0jaSjxaIF8Ba3T7gw4hm2pUVjeKzCbteWEU2vjxPOX8w12AcQpqFXjmHSGAN32i7Wqjym6v3LZewahyhbllRWF74+8rfaf3GgDY24o8rl/S2sePMILfAi8cDfY6Rq5F0cfNR1WHiAHS1hdtmWK0NtvUN3oKqIx7F27CGl2oxjWmi2GhxYDKPgKYIr6A1OVLf+J0NMLCldWlw/R2ddoNOvj2FfKIyUDZEeoxfKStbipcMdS7N+PMz9dS5l9xmb1G39QntMz95emb/BKcwL4sleVD+40Clbc1MWqnl3FwveoVV/zBFUrOV1BVySgM1HhByhv1Fq3UQ1k8HJLG18VqCt6Kbi02sLaKIQccvuEzK+o1z4MWphfyxS23MFzLTeQ6i1MG+YIujmHb+JaMkM+402Pd6jRrBIZyytjc4Gu4bMCbOYS0B2KSjVZec2MJbpz6YivgjN7+zHxS9zmH1LeLt9or4pW6wwuoVNc3M1tmjOPVRBmr1W7hi8FQFo0ycL7gy8MZWzAqPXgIYxDKaMAcS+BxeXXpL/vn3FrHfHwuLVDBjtEbVqQKuHutniVZrAz+ggJ5bqNXgLGJ4MweaX8wbx9o8bLREVOFyQJdpbHk6RfWjEUUrKxL/thVdESCNPP9oBYbQJ/CEW5RYxQx7p/ZAVcRhrnkhTOKwwgYpLzFuW7Wa0pU5zNt3M1wXZigfjqVeUM+4xz3MS9GTzLub6l+9RMwfxLHN3l5ipraM29LhtVaVnj3AGDX28pd2+OZcQGjdbmCseeDsRcsT0big2em8TBeGfvL8YaW6jHbQ+OYXSq43YPRL6Y6wxxVLRjAz+5umMX0w/J71DPHcLVVayYtbrDBdws5rUso7Q6V1uCrjmPVKp2qbljeM0xnjFKa0wZqkRdreP2pjcHUcsZzcMRom6hWk99RVfSFaCs44YerPkw/MpxKRxxBBdqouNGNU1XcSafcOtNXnxBYLXiERgz7xQVrBcVxnOcksSt0zE7bt59QVgvJ1BWwL3/CWam1W3CxUU5/y2XZfLXuHaLKIuoEDv8AhF+HBliDSwUAjng5U8kC1sSyAFdNwBGzs5jFfFTS8M5indUTJAE+YajR86IQNcZfwEenaV7iics1cvBKIjjZ0hFXVEd+GPZEMwqxue5HcxFcS4v7xS/Oo1YJvi5djLyVfmKFf+ITKLxUr/VXAeO3cqVFvbXuUUZW08CVawDZ3GLAnXNykNLZzmKYQF7cEXc3Vg9eEBVC2vvCI07wbhnsoqJgXQbiLf5/iC6M1b3DVXQNsO4jF8OQgWmXNxFLV+5oeWoYv7YBMYxiJ0ztlZwd/wBI3BS9OPtFGOsMfXEuPxEhjepvWbjtjnjiO24yTHBv9xzumHFQTisRouHeFIwNnpEg2e9kupjzGQd6iSgYY4xKoFYbja4HHzFW81z+kenrEIgw08QmIrXN/pFMVg2/xFm+KKljexYCbcVrxAyuK4jCUDZ6JZbYOBgfMMOJchEkMP0jC5F5ha8ARxBj74RhI5n6hLvV2h/MV4KVCa9XxuKnbC+ojDlnEXC8FINtk3cGLosgwvbZEN7p6hw24UYoRGG1eD8QtwM+/M5zbVwaDg5haOkPvGJihxBrjEM+DcdmGDZ4YgagTjM1TQbje5VoqO9aff2nBT0RmjCrKtZVxAvpIBFavNhAhgN7iVOTpeoCa7XLxFa2zx4jdeQaNfMPRWdHUyKNmb48InjXioNoXd7lzog74+JXeQrds5C1bLvZS/B4I4oObeYrkx9pidNsC3QNHUssKTd8kP0217jC6YG8VXBbcuFb9yw8Iaf9cZMambvUquyFGE1HhkZjWspFOwVbWagqOQhiD1IxlqMy01fEMaMmf7eYWq44OiNeGN+YMYNLX8os0I7W17iWu2IrSF2rS6gnlCg7i/rKGHYMgqd+0FtwLbF6wUefct2qcow0Ggv1FDSm5e4NFUhbwwO6Vg5Y5tANRZzo1/Uq1g1dahO4H2htcN9SiPki73QaOYqmwUSunVIR25QB2WIsbpRYjxskUfDCPmFDVobeCCC3QK5/xjB74eIU3jiGkNr0Y4gozDbMxUVsl8psiAfOP5RjGufMJNrVSvxuo2TFd+Ybx3/CFCpuyj9QRpfPiZtWjHJxzCghKwxXGBhdHuY/LmxendhuoK79V0EGo7S/84lIYC5gUraK6fK4wPaOCU4K/JFrzzDDk5bxDsSDlxiFILTF8e5W7wyprQbfMOWYsKmILdjuCXmsHmLfiX+Jdk8bG7H4DO83LEabLJjnDK91upT4MFO4Kq7XASr3iATB8EW4tbHEARkG2Gp5vN9QHTnLRthkjShOKg6YRgNMHJLMMHEWVzXRDRDg/gjsomCdMocnPEud+Cd+UudF8VNYy8wD7tRHnyquKVoz4i1urI8+ESF24TPybagWakMdzE9IrPOnzDqXZXQRdeC6ienCrYCaru5jTsYGD6SLZXuVo87lTQVeo7ZaKgFOFLtGvlS/EVa1Vs2ZT9pppDNSq/VQpM2czFnUR8TPB6hSG/EzODVQBbQuYa6ywcHzuLfo3HFRv4qKtABdefmHDd+YxbgVcyurV1AA2Bk0eo1rFGxg14qB6MDnzCFu68czKoGdpcG14tjf0Kz/AFDuSYmyPBoYQOB28wwe9UsdNnSxnbAVFtRq4dzGxWGMRqVsainGT69aczzjTUePiGa/EYf9ialYqEHKGU4iSYFrR/8AEKBR5GBN5Bp3XiWroayajmsuYyVtf+EDXSENCjNY6Ilbk2RVXvXn1D7sNnFwK3ZoimIptire3XjzG8lm4tYairbGY59s9QTOLaO/SYxtq3xAA8Vf/Ja3FVW3cHMBovcTxQ28Sh6iwBfyr4geVAcy3vbEdl3VQIqVipjtJl8cwg3dT8ri1bujMuFc9dTKvgECWM9T99kSvyQWNYioGuYJuDY45ixOGXhYNVs2weA17+UM06xPkh4jT0m4o68zd1ghrO8Sh8kTUb51EWbc48QZaq10YgwCsR2a89Tk8RJqgxitfMQpnJTwR7nPXMFXGdVuO+cISy/+SsAfxHT3SsTtrPZKo7GKHjtCdzxeiCa8N/wRohFNvxETCOBxGq4uE4aj7oK8S+0DofM/EOP5+l0NfR0zHd13F7O5WLD4ZuKA1VEuowfOoF+eMws78wZSlIlTfFpwbqVZwFARktg0dxTjNY9Q0HvJOCUFXz6RrLoW2OFm8EKR0GGFKiwAJcTaw+PEuRoG/wDkv6xb1AtBQUR0v/X8oIeEAUx2l5VSq6iOhjHuXji69R27xj+UUjZxOXggaWsn7qPTQD1hlKroLXuCOAKi8zJFQOcrHkBMePMNRYoSJXNRqfjSgRxX5g2+My42P6nX61CFDmBM2qLAp8o0VV004SOmBcRZqYR/EFrho8SsxFArzNPWCJdXm2K8oq8qV4iLYlcall2rnqAKwYUhwA558EVc8oLjjlYMOT9kuq63LfmGzv5eoxIUKAGPiGZhaLxwCWUl3aYvQDqMLAG61EV0uHPWZkepnJgCiZh8xhuwV1N5fMkJcuX9PGYzab7xGsfP0rjjDUvKvF/Ca2osVlimIH7RrW7QA4iD32eYVrgdQtgXFsUhAUHEdoVIbjgXiVk54i/BrbEihnnxFmmdvf3jPMXXfyi2nKiBXI2xhsAaIIrvN+4+nrHqYFqjXEcJrbM2vcrJ1xGs8tP8EsbW0qQ+faSAa3BKjx8mAvnDUUSugPWIaD2sqL2VLvDmVPMFv4l33x0RxNlzEqKabNMC5U4mLYBMLo9wKsg0cw4Sc8xc5xCnwy08agn2X+EB16lEbqz1MK+keTtTzORk8xFVB8pdl9o7qsbYLTtrZkP7jk8dsHyyBo18sPDziEsYbv4fHmDjTFyt4xBK5b27eiKUta8dIJwMgwnACIaRyvnyuMFQzDHXM3Z5A7iBxayix7gfMMqB39DmPb6jxL+ZvX03X5hPq1eudy04huCzKKA6RsXHCAQYO/MJfbSERTy2+JdfQXKqM8vcrpu9/wCYhQ32eoUMm9wb6rNvj1C2nFDG3lfMIZg58eUbp5shR4oZhflzmoVHNAIIDDuFvznECK5VXUA8MWwjvYWwezC3EqL5bt8S0mgQLZxbRG/u1ILDpslsC2fUymNGAzGUwKVCDXuBhao7C9+SI7LHhrD48yuWsQoyhF7SDKn8JnocxXW3S8MfXhzfExjlApiu0q4w8rlixXm4TQ5z5lsvNbepXm63UqOTi8zEtVZf6CC0MXd1TCZpWyKDE37+IguI0dsaRTvzDGMnHSV1EHK4jHJ0gLEgevHg4A0QLbiG2ePmVOJmFFxQDWV9zPhs1FjES8SlzDA3A4hnafiXHD9uZXL/ANh/COkJ7jy1XdQpx3AjDNArxcyca+0Lu+bgDequu4wMC6g7LVWvcY/C2XqHG5XBxMdg24Cu1NL3CgUVizqB20kaGlw1v4gEWa+Uoxy5ixi2+YWeQvK6uFMOXB3CiZW5hq9kVc7qL+Ja45jLQAOmM0V3cpGMsOof9AAygktCkLimw+8tfi1+ES9z9wRe7BgWKw2xSr8eYZgr10YVnJcRro6ZXqiHTj+yGKbzT2iELYP/AFCna2Rvgo2Q5gz+4L48RUOyrhwXiAvcvP4lgUX6hBquoLeXGOZdjhXF5WCcW1jEcHJbk/lDM4rUowFP1K99tefFRu3BgolcuhebyyjaR4xAfU4MRXZ5iW+W4Yqrt8PKK3LMEX+pu6TBjHUdqOWFVOiv2S1fEFlfMeDmopnBULniFMa42xqxIfzDZAi43zDOc+foC9SrxM87cy5jDyfzHeyRUhey4LeuAMTLuBmI0xXz/EDMlbYoToK79IKm0oN+0dr0P8ktWqbgzNCQgHGXv5lDuBd8zP7uea5JdlsEQXQF9RoQ5jsbVUavLBrH3jina4aXw1ATnXo+Uchuj9kRy0BR18zh3gHflYtty2H8JWvqD2Sr5KjWPNscPjic/wCIIHrUY4zzC4zRdd+ZbtG/X3huK24hmuzUbvF/5UrUGzXcE/EFceD3DRSTh4dCDjJcw4xDKfMIJfW4Vjj7QF7zHUAlmc1V4jAcC9a+5GLBec5YdyhXBf5QDsHyD8I29jvGfuTC3scpt4HekV883Km5abxg6jmi85WCGsqcx2XRqoFrh36uUN4BmFfPMxTiGqtCGNerYQvf0BUni3uUyqxxKjn1KSHz9Nc1XmVOOdyvGo4uqHBoYy4IuN2KO4KRhqozfSBcLIsSnAaLg3LAMAqsA2+ZWz2+3iEZw3K99cviI1fSJhwj8QuJwYe4AOKyKh5NZhp6NdSxeGpnOiKv5lYxEznW4vlVZCgWwRlMEOsw087F+4UMFw+IV7jUBVjrvRKlXmgGEqv1KRZglc7oWGgva1Ct4YbrHpLrcXZFtaY5l+AufEcQyAsqZvlMwVS14SKtVt3cBa9H9wKubUt8VuNfqWC7gul6xGjw/ECw9nBBgJpkFsS248BQo5aNO01HNVOODHuOaL7zX2CMt0Du3L6R2meF4Yg8Ka5gqcUG4Y7KzHEVv3l9Uui4lBIXmuYKzZxLpWoVFhlViLDDoQ35hv544jVeMyolKVTzFaQRfhhXErBN4YrIv8Qx1WZ35GcWrlh6MQ38Qh9TGZDMWAeBz8pQNRvL/CVDGD+7hHW62QLwkA5i2m0Kv8EeGuLuAsowXbC0YWApBnGW3qCB5GSOVzm1ho7cO5Vb7be4x1ou75iY7bdQU0xxzHLijqcbEaHUdNeIoLzmLMu1ER75Q4c7MMseYas2EacytXX5lRwi37n0rhFHkzY6GO0FG3BkOWEEtd3nmFQMfOZcmW1IzXMVgLWWGwhBMrF0RJqBoqK0Nf7yxzitRopUc1j4iy8tOIfsaDdQDBDzd+DmEdy+S+f4QbWM0HD4i7VQHHMrZldwFLTR6EDjFYXAeAi6qtXjmMiKG7wfMveBSjbyxK/AWo7XuaGwfB8xjeUx5hltQQTm8xZuMcQfyiZ6mVI9VnuAE05mMih4mCce+IRvcxXWf3GaSsrIsZpKrfEu/WGA2cz2h9ov2SrWSUP4i+Osx1U96YYRDPO4kBhSnhekZvnNHMaEuDPvkl2bWbfEGnK68QSkrNF4SAhrRg/hPmCBbK5eYoEaDUC4yfcigDOaIULe14JdayjCfdP2g2stR2YNt7ig6mF5K8GYplPxpO62KLONXD6c4oYvSHb8wLzK4qPcsAFocdpeIA1umy9QjclbYbxzLt1WKibdPtAU4jjjicjg9SilVYMoXvMeJuZ/lmKk8MyvnEGuhS5Y8d0uo54bS6DJGf6naxu1lrPNxelArLcOAcmqi3Ch+O/15lecncV3bR/Spiv6TEmhgNRc4CiFiXQYKizbwzNWXcuNNc8TS+82xCF+Y0XtDQ/MxOMGoUfuX/eY6oeZd4N3DV5SyN+UVcxBJXxA5KXMGpxPuOwb/MEcZhjVG2UBa5lbmY8LxiFhbh1DcY5HctWSos4tDTV0DvMvDLuv6RucwVf4EJI5UvtAraytf8I08ALzUF2dY9EKKRRyuX9EBaNsoKVaU3BRYS3fBEaxtdxQ1p+YoLs6jEtPRwQA43zF95IBy6AhrpDljia1cpnxL5rc3lm2GvNQyrYGa8RIc2cQaBeKx1AB4ZkPuyXe+5YH/XBCuIpOr3BLVI5gx8XNLT9QYWqqNkUp3GjvuEaSA1OzmNDdt24PKC0Wu4xW+HUcCIJwhnEF55euoWq7vZCeBsjd4vnmIIbblCfSS1xDuHsnUXy45lreZlnDP04PJM9Oo31KG4rM/EWMSrOkAal7EqQVZa99CALrJn/kF5obF70dQ8cXlhK8BV/qmfwaYTvLrHEPAd6GM9y5OeoLbwTmOgXK0buHHFOKnClnu4KZ1dBAgXQv5IIYBTyiltta/gjAnAP+JY1ZUCDOOM1FrOGETt4OYQebc7Mio6Km7ScyjcMZ/pLVjbUtRwUE/wA8S6It4mZRtG3LIigC1m1hVt8XMc7irHPMJxiYhhB3DMNI/wAMStuNXEVFQq3GfOWk2KEdWuNVHXSGa8wvzPEaDzXuAbr3Hkmnbk+IvyNWwhVqvR7uAWDXjK+WZofbzHzY1HVqAQHGb5mY5zGeWdvMG3AxjmUb4xmKNBjn6ndCPX2nHiVaZ+IlrzExAtZqY/mCY27mdH3mWOiIlmMC1uMDDglJui4XTSbVLvEBe3geYqXgXiLepuG1Z+IcXWYpnWpQtt5huVrmXL2/caecQPbmMo2DPPzGSyyykwxtc3b/AGhFtqBg6tD9oq8unRTKbDqHW4DR3BI9mH+ZXGWc8XOVBqo5FRXqJ8wleYOlvsHHuamY0cS6fwxrvLlnNF3juObYtxdLolq4KngwKEAPLHA/xNeYq95hsaj5S4C1Bs6jsgi/MMxnvmXynjLzGyLp8WvEVHFbhlJXR9oYzzGq/ESjBUVMStxGW3E3DSzEFOQzW34lPeF0cV4qMu1rhsvGiWpSgwd+YUyY6vPzDcu0X9XUBSBNApRpfcbF+Pb/AEgDCxSuWJLwhiX9AeJXsgDBUuYl8xiLN7iWDKtHMrtvttmEy8ZirqFCC2RwxOsr8QuCWy5qDVgMUchytviALgD5WH2QUv8ApL14hmCVw12x1Ye5U01cNJQ76OYLTrBFr5lpWYxSHJVMGGR5iirfFuJq1RqIGlFDBTjLUUJbM/1G2larxCLukq5hpvRjqCA0T7ZjuaEMVy3LU5S2N6S0fGZkza4jgHbKpQegc8lebS8lZ+JhdstY6gwf0Vy8WS4JGptlZUq/5nfxKZ6+2myC1sbOiGdwkW77gB5uGkqayJiuGWBH4RDW2b/lH7Vq7/pNp6TBuAM+fEXTBLIWy8lbIu1aBWeI5xYMXo9JqBaFsoFsRggGzJj3P/sZ4IgEy9alBr4js/8AZXUSDLorCO8xu7g2ltHkJhZ08TMsvAblg5KuG6pTdMVVaHnvlHC3oMaDyJx+W/HoldnGleKiMDki5pwesRxWzNlbYubogrRbaIoVrfMU5tnMyRJVF3AmkPEW55gaRecfeOC5oojIDKy/1DFPFrFga2XAUtmoRbVFlxrea1HO8hcNglPJYNgbszLrfOoL1bYS7ALf4RC6MQaY7MCtkPB2zrLQ28leYNQ0RsecQ/8AwZP4j+ENeofSOep1S/8AmYRLdfMY2aFIrincPeGAGGfMzDxD+5VKiH4dRqPKRFxbu2DfwvHy1KBoOI7mV+a9RdrYOAdEKjJcvD4g14Rq8EZeQK8RRobeYp5jZPc/iIoe43PPEuK+YaGVbKzKrG5X/kqerl2txuli4vJxDa9PUsfm8dB4gDrj8RbW4e5RcWu2CGMEXpYKyEQN3R9/KKzzZ8SpWValNvXXiAeaz3UUzV99x54yaHozOeCD/E8BvyTq7irJhRi1HSsUrDk+PKUKa0S7gBBZmi/eAi9IguyvEZ0PCoY7LcZviFuOVZ8Spz1Cat2BjwhDZjIGh3nmDWoEc8cmOZZKfTMIK3K/+Ik6cQ8pcVBlueIx6HcMKFNZhrTqobQ5+ZdDwMdlJcfuVFBO25VmPUb6YL8xDkxCIaosgDGXKvPiJU3io7OeEUWujBxMdx9nMKH7Q0xLG4t71GsaQgDluZ+e5Uba8xozDXUKD5+mIdV3G2o+4gcagJhf66l3Yf3Hmi2Cm/d8Q4UEFVavQHQS7WDiOntjrFWZb1Kh3i4xV/3EL60sXawXAUoblY+pmrmZ94KpuHB7zEvwQqow7/UcvbarFuGFq5UVwrf2giBVYOIQZcblp2JS26IxTm4XhhFVnxL8DwLry8HtmZBN8va8w+dbhdeAOPAmOpUqPHEVv68R56mmLGIBuHTM/Er6BuF0PEdZn4l39x2G4M04NQAOlVD8uIHgYFkqb1mIj95g9sFfEW2OJmHZywheGK8RUwu1dkNTwW9+pYr/ANlHllq+4J5beDiX+AQFUYGfmIE2/mFfMxQQhseZ9qVnzCz0TNhicT4lSpqOUqfAvcNM7xDWIVte8RWve4lswFtOJQQq1ybimrJ9o4u8+viFMQd+9y+/vKwblQtLwbl6u4ZSCsbzGyvV+JdjzuLY6jl+MSgXbiKvvLhNDqNn0RB8Rbxx7lKotvFZfiH2bTfwCAc5pFa7Oz6IzWjYcEL47gDXUJNNbqX13Gin4nGMQxrn9y0S2VwlYnXBjMCunM1H3Lt4jHGC5c8FVBfBpYXCuMQar1Pvle45tahWfsgxr1KiXwl3kMEzbFRV89sKMfjcNLh5l8QWePtKp6nGhguFRWBE6js8c+Z4cxtrhlZbj+Z31+oYuFScDLfWIcGYHCEs4XtjdZ8x67MVFVx94pnS8QZLhnXKZNn9Qj2SvVjzFCsdSsvXUUK61Ehbp+8v2sr/ANQy1qUYfUqzrMSDjGJY29y9mG6oq8RxGwt+UVlbhCefn4hr0rMGG8cEW2IUK+Y+LEeghhg23s+BtjmqlNHsDRGVS+0tQ1vOY57hp8SzJLty7WK+lF7ve4bJp6hQ+MMeUHjH0P6IRHGJX02Y8Zld4HEQAa3iL9GdI3cZhoFOPqP9546xCvvP0QV5/iEa1mK4wOjmOJrqAS8Pej1BWsqS+clWyqjYblUxzFrG+YV4l/gJi6xc0uky6xuIKNR3c0S7HCLl5Yuqllw0qZ8TRBWftLOdZgj/AGI32OYQfNV7ljk4qa9eSXDl01HKNaYrXfcNMfAmbatocY5go5XUbcjl7huMUziLZ7l64etRQPzBrcF7L5jv00xenV5in5+0JRNWVGuN5iTXmLN+KfMe3H7iX5XFCIqsCKAjho/AamNMaqBao21zDly8QvY9ShXtEs6/idZjx5jRiWf7EsL5iVviKEk85i1xXB5Q2lcNfSrlrXMOm5To3AnO4hnlp8Qq5jGYBp6nzT1Fn6XLQ7qyMI7YuLBr2ZUfcC+qF9eoX8defJCDV5juv2lG9ce4VUxIvSqhDe4UlaqFhljLG/UMTY0l4gZvda8xLs8SrG6u/E5uJXKKTynv04g2O03uMd9R1X1AlEx3AFXjgigcYzxDt7EpF8wRiGLgJBrFw031iCqqEn8+4jWBDM7vMX1yLRje2oRVcQYP2lJnKw5vmGN1jMFJpyS/O+4rUrruC+IqtVUVH4YFq1A92XJoi7RAwmF+XKO5MX6iXAmY6/EvIlOIUef3BK7jlw2fuWNmJmXwz5jRe+GC2Pd8R5vCKa+JfnaXLUdw38y9h0lW45lctPmKc51CNqrqVSJSPtCDUVvzFp7mZbgxPWagt9/S5fmNH7Jv5VnxFVeWFStNRYqvMqhtq+okVx+pe195n7slTqIB2x9pc8y0cVBf5cWdejMVb8RQvX7iZef4lCZRibzGcRmNDfmOb465YhgxnENmrYzvMVqcaWHYO2IyOYT4rnpHHXBHdUvEqfLojTqBocjfEcgcsQNEOyfEFY6huFcVrmFX4jwZZzOG/LxNrV6iqaO4zl6oNT8QZzH4moAXl3DQqNUqVhXUReM3BEu9IYcw0ahtmZ6gi9i1CNnpjlzhmfr+YyY1Cp4iJvUqoYFp25gBhpdZgtc8T43czBzYsxPFw44I3hxzatx2q/MxKjwnzHb4m0zr8y1tlQri8REeSJAzLw3KB4PUBO73qbdYuppcwj5+5KMGf1K1eFJdXVQXPdkLD4YZehtX7xCrZh8S6rw4lpvuGJdHyzyQW+EZbUaYMeXuMs04jbJgjS4u40KoURk6VvzCvG/Eql7cA4CkNznFQjjbcwZyhZxHKu3UdhqKP1HP44n2tzf4thbA4tjAcDXjtHNfJ69zSgLO856I0zOvtM1RaRLibJk8x3iOaLjuVWK9Sv1G8CHFLeYtFRyxnmO94luuPzDBnMqKjLMxHziPaCv6mepUcEr7w051DnrNkY5M48zX0F+4Qzkgs8wRxqa55g1iwiVNFfFEC2JSzcWnfMzwDBxmcvAxwbOowHP5QkU+IMj4ZerjjiWB1RLMD3CLbMFN28R0OMysPNeJWetSpxnYkIG3xUEodZqICpc3iH8y+UMwEVdwB8y7F5jdc/MbxuoKfmJJoGWDFV8Q21mKkjV7tXX8xL9QosVzFTfxNnrEQ2ZYQD3mU59onXZL8+pfp/jcB45JVl6WV3znUJ96L6js6zUdXi/vGH9Sq/HMK+eYin6PvGr/AKoPpFo/bqa0fnGmvmKMRt5nHWoNoZIIYb+ISbZhrEzw8Mxhcualyt9RmMniBdma1AEED7ghxNOoc4g033liUvmNB5S3fmKiOPjE5rCpY+ty3bnL3ENMcczMvNVUKLtNTPvdUdy6qLzUMFPBX/EI4xBcL9xRaY1AOuOYZ3cAL4mfRl76q4PTvcU40uJn6hf0GPpt24HD4ljnLEqA2QgMsoefMUM8a6lRncrqYqO5cazDmuJevqYPbjmffKjdPcLfmwcwC4dTOVjmILVcuI2ukfjl+IYzy48QqWyX/aW2oU1sNxyVxf8AiZEWOfMd/qL6VFuX8xxnjvuVb4huM8yze5vUY3LUemXcdmpfLvMdfuHSXs425QF7q4wHMVRg1KQrfMFbUwX5jt5hR6GXfj6d8G/vME5uo8Ydf5maV99MqOiZ6lxW14hsOnxMS67hpbgMefcbQ4CmEGG044IsF9/E2XhS+Ii9FmVHTghVdrshJPzBNYQO/W4rsOPUEtt/XqAFuLifpKrOoeZnBqD1LoRKYi8Aq9W+0Q3GApVZwTCEGZo4CHUascbhmdPpM46xhLlWe4M9Qi8M4/uWA9IUvV48yrTFs3ifrECDnEu18L+5c3QYhsPaQIPKx2WGLzWTZDW/PEI1M5+0MfcvGZ5sR7wBlpvhl4IxjmYxw4COJn8Q6QKXy/H2itjcaOayiLN5dy9XiAEIa6YQWP7JlNYr6M0cghRTlNrsxFzcGYibhumS16Q2sGgINvOcylGox/8AjzLxpXEMBbVZZa/2/KENYXf/ABBFYEaeo0zlSPQ9QDnGVPMCL7ZmcTOKZ8BUVHeeZwnxzO//AMh/8+kZeZgQrP5m9aOYOGwpUV2Ea3L2wftDpd8yt7ZuzUqx9AvOkoe9Rn2SFb1WpivSV5uNo+YSZxpjDkaHHD7jvzyzdaywdr7a+8FWX+LhRXKX8QGL9RjV4irPOvpslgly/OpUxvzqXXmFUP0NmJ88NTp+nvjjUMzHKOPMEnGPprjEdkrPP8T5zOepW8O4bXHK4tPzFpPDcd0OY4F9zIZV741DYrQ2xxB1l8w2MH7wgexqIN4SG07hyaAhrjqBt5Rd8EB05tH9kZ0FqtwMc63CupVzKrS3MHGKuvE4TdkaJeoNFzuXhOYlOmG4FeZSzKsnudwBnZKxN8QiH6XY0w6Rs9ZqAeVlfKLF7DOB3qWhSkz6nBzx/MvV51C63dMFR7hDf14hvNie8Q5rq/3FX8RUyqm4FriVbuOhv4jtUJxSHUN+IfSNsPySsK0blupXqX7md/iC98zWnG6zPzwx5q2XlVzqEcZiyaYQ8a4jiAajWsJdfE94jjXBNz3mPGOn7Ra1M0XBMjpGG/gwkx7oxMa9bf4RgPGfc463mJV8Y6i4eLlReAg01wkQVo0Wwwpjglt8HsjatsqjHs+IKja4/lGi/FMbXFV3KA+xj2TzNrmJFfcn2Q/Eu6cpf9szNo5iB8QhzM/cMN9w2URzuVe7qO9zml5UQNEUGrhdG6H/AMhWYaTlX4hI1zOndj6iABzBVeMP/UsJ7foTNcO4fvDwlRyV1KtLj4iIv6HvPUqKgP4+kA+5VC+eJ9nbOkqXzBqOoL93K+iU/wAQb8zfxUY31FOo0PuUqsbyx3S4riZB9P4IqR6iUYal/qmb3En8RvuFO8pnp9JmQsGtT8IYJvg3BGFQMyxzxAs+IgGLvfiW0coPxLmWfjqO1dHEqVWlXGM4OOYcB7Xm4rXNFdRuu7WCfmVV05iU+EcviOaMRqUfM3ieOIQ3Gx6jlX3mavhEP9qVvXMNfaDDW4qfYgUNjSJwPc5myYZHGB5irG6L6ihR17WFdc/aG/7QdIhrm3/iN27W3/U99QJcpeIOYOpQ/qJ7JykJk98TWoDFeYlaHqB6JUK9pj4zccZxDYmL9wjGpfKd7zDy5h/hFbx33Dp8wwszMs8pc+NRu10V5jt+4rTOnMqXd3Kisuoglx6hkV7lxzk6mD1+YPjubl0fhBErVA74/tGilmjn2xa423j4IuL0Cgi9X4Ofcpy6+0W/24RgymPH8pc9rmPGMVL4bvPNzAO0U/cZSz4JlbY2MeLn5hjvdw3VUiZjs9NVFDWU+Khqz4mmt4lfLDVI3ZwQcPUJHa18cxh6eJcHqEfJGkdA9wz4hs98Q2OeIGzaq6+ZdZkMkM3ou2MYdVBTeEa53vwwWarbGD8wrfRuZuctv7jcJm+SVmuYXvvnzGrkhob9QYrSf1X9LnP4p3gIyZly8o7crn28Qq+ZfnmW8SsfiG/Vwj8XB6StDRNOOpkw0eIW7Cuofi5i9TA/cJqF1mb2oQbw/iMriO/ccl8GZyPAvuP5pef9UvySrdLrBWIzhF4iIZiBmePiMkYaY1rFS0NxPxDeoVDJYS8wF1o6RuGYVPphq+LIa711NtaWFcX4hGfmoVTjdQENErLUq8OiOa4JVbgRHGM4iBziby5txbRFvdUQ3jvUOl7iCq7Jdw0Nw0u4avYJKC/+w8jLqYW3buVZ6hIHiWMaZ83E/mxMYx9Ks+5sqOdws+IBJo+ESZnviPzn4xhdTn2S/XEdjk3M+INPxylD1KyjmTW+487z+5/mX+paJZh4+ZdoYxRBuDsjQ+8Ko11Lm9wBW5YriYlfEXjvcd5fEUjrdwaei2Zz/SLVug3/AEjghi2v7RnAfmXnWYua+0vqOqG2P7yv1KdwKqzq5YXu4oxDs7I7KBSudQo8xFTa9SlWdR233LY/EaH6Y0vgjvPFzHEOmoWjOXU3xmIBxVxr5Ro7g1mskzw1uDPIYjq8bg61R/lzP/HHzHlcsHSc8A5lYPKmPE5s5juvKRdqV7qZviWr9xwxl/UeO3PiWsRiW5i9o5vwTI8zPcKcStWpe63GPiDOcdS9cV+4QJPDnc3zud4Gj3O0PpAm+oQ7mguN4+5GOMyk/XMvks4nTcXb+paDrEC2JcwmGMatT53Et8XEdGfMNu+5QZ24hLpiq5xWISvzmpW390eN6zctbzioiB5Vi7t5R2eKuG3+NylEorP/AGMY5jvyzKo/aJxK0pp13EMu3nuBC84s5IB5QQvMWH88fMpKy8w4uVCpXeItU2IdIPDMyxM/mHhgU+PEdx3shpXSpev5jA/xDbeFp5jtZa1FTxUtw0mI6sWCrHmV5uXecmUZ5pjS+F9bhm+NR0uMFfS5r88QmDH8zWSgxCuEp95jphrEOD9xv9ovGYI9Sre4HeZQS/SO8TMVww0fufmLmupvCnmPr5hlPavEAe9Spv8AH0ywWNZxzcdNi8ZhamWtceIxXPUyDwjoXFZ4ECl92QVirdyueDmUnUcPcdJXzLvPmGG/MsZnuCowGNfrHY8StqLSyr/24Y7piSZ1KF4q/M4P6S2Dj9Q8azcZnjUwvTjxDb3HHuKh1un5gfwSrZhfeH/NMrJ+WGcfEMaGolMmaiDE9CSyb+CtswZzvxGJttqyUmDX+sjHrczppF7PidPSsLy8Zf3C47lyrjnM3jxH8pzStjYvlIl5ZgrZHHmCQm25WX8McaPmVYmeSGiNFeZV4jY5WP7od/eCybczWo98y6/FnCD6VM/Pc5ulYrL3s+ggRg1iNHeKzLjVoTIEfpj7RUVzXshw5xNh4YrTzMS8TV73HwriUv4i7qxI1UGs/wDY5fEPc+1eY9IqK+8St8x9dTc+aVYcfQqFNRtXmIG7gVFvRhwk1US3/Yi/DUXeOo03M3wRwmqdzHiHbmZk8QoLwajSm4rbNN9QhTjNXCeRr+4YC6u2Kohq04gtK3dxDY1ywBkzT+DmO8Zy/QLL6I2XMWycTJd3Fsy7S7XC6XHXzjk/MEepcohXxDMw1MxVR0d4l9epUbhWN4zAulQwv4RK8RbQ8UYjG5njw5mavNy7i6HMOKd1mIFOky8wQzb3Fbe8y4E+Jj+JaDnc3nHnmfbL3wSi+AYhuy6dn8JjV4xcq/jcZs18yw6YZviPsFQ6fiZIfhqZuniHCXFRjvqdWJT8RbH4hn9J/cyYSK1uXqUSj4uDUJTzsgflNb6gXmFr70yl4g00g5c1F3jFxV+fiNl7r3Bw90cQ7Oq3F78GO4oKqO29juW8xywQ959QguymNn2/zPGC2VEAU6l0xMd/Scf0t4hDiWK7jWnc++NzGOZju7jXiGc6uow9yp/cVcepV6qc+5em7hp+nU95nzEAY3iyaqcw21zNqgLdzzGLPMdl9uY1/MvoY4lZ82y/prb6xCy4bnoYi36hgBVZ5lK+7e4XDHOYrv194Olju+Yq7cRpXiog8xzfa5vDWeeYbfEAalLfcPZUxTm8wWZxmUr8MHhuZz85cwMxa3P70Nrd8wU+Y6hFTX3hz8XDZXMMULefMdXF4gpOLi9TTWI67xAjD2xaXDNEzarmzuKwduJSA2rbBo4Q0uFP7Qy9r+4EDI3epSVc+2FCo81HBnzK/ccCYsf2g4jipVjXMQzohr5mZ+TiL3Atx7lUf5hvCWiv8Rs+iV/EC05fqUMeSeJU801At+LZtWqiWruYQVY4g/7DozDdfeLEArfMUHSxS18hHiuCCpcZiKuqxKgawhzUzVV4f/ZS4Td+JkzicGZ/9SBGsYiaxufOlYzjqXyahB8ZgrxMKe5ceBxFrMJdE4lZ96lfEpIhuCOJeCUn9RuazcIAT0ykqVbzUC8U2SkX1ccRNOoNfF3vMNh8xcHtxCnx6l5/qlKqxuClfMFlXIpCsXpScMBl3cMN8I/aOb7f2z3ohhuLWz/2VbrOInmbaht9CHiGDcvjsbhqv6ZD4hgafmCvFStlzKqDHmLy47g06lVOffBHxHRNbRjv1uPK+ZlD1DBNECpkXDVMuYVPzGMdfaAvHJKMs36qK2+tS+tMRQY5jMXuqIsmKQz5h5pKJ/DdozZ9MAbg+8xuVVdRB4ifh7iZcxfMPxnljiffKs/Uwnm5e8NcwrvG4KTpajqylXxAWswyqDdVhmdgJmPVNSgd4/zGFxWx1cct2YjgMNXzEwOsEuMdez+o1nzTEqHdXXMJXLUJH6/qMrwLiFN+TGYwPHxDk93EAbOIMeQ1AJ+X6D7S1XLUV37jo1gCXMvpM2rdwQrZiEH1RioGjUfDljWI+MP19MftDL55lVF4F4eog+Mf5lPfEr4XXmb+LiUF5IUeIV1zHyJVuu4mDlMd7jbDguKuJzXcW33GNzXqLZ3jHE7jwz1u4y356WBKPjdRljFUPMXdaIdrPEQPio5I7tRjDlmLu5nRQyaiXT3v6Rz3KqXggziFtBnmOF6eoE3ztjmukc9RMOKjKkVzcpH79x3mCP8AVGnyQqa06vgjvLiCPmVqmPzFyapu+fUSZp9oKA02fQ054xGD3LXrzEsc0MEFwX9xlxEPMhWBEnuY4mX6VmoduYfxKXEU9TBOn2mr/wBJsrHUOoc79+JYOzZG35mMhk8kf8HUd4+mdmeprXxC8155IlK7IFn6J2cLGq+Zflgqsyc+YvbFQ2XVe4gf9UMUdy7ylAX+41aKpcSh4Fi0rcv6KcQBXNNf2jLNgjGvA3Cq9GGDE5yuosNJv5gy/vCE21Bzfswjj0SuUu8Q0oWHMb+QgRzqCAO7iVjio31L29oUxN89TEf4JjQ7xAlPbyiwPC+4GKKSrmIXXndwT1UHC4QMv5QulmeJa/NlzBXcczKvhjvLZiInsMbOeMnMxqviCCukqmNWe59ofLAaeGAqzNsGrEogjMydcRanMNv9mNcNSuWI7xK/ojFL+u5lvmofS5temHPlm+ZVVa+8HFbZUV94P6lVFT8weu4KjUleuSXX3i75mS9PEMHzFprv5g2QovhBD+SZ8ssbvF4nFw+YYeoAb33DZd9TFiX39HWMbyP35gPYjR5qWzQu+5mFWjRMOHLbBbxayGRnRcNsneO0KN6UPmG71eIznWe4bds8y7EaJpdErDOtytKjnZ942huivMN0PVwRg9moLfN2ytmNZWGNniZ7RQOqz08TOmKaYVfCoatcCKzWUc/3FW+IAfj7wGn/ACWDyhDpXcPJm8Z64hZsxwwr/V8S1T21BorDvzNP4jA1zLK1is+YZ1nTuNV/8gwc8RWX4hqX6Jd+biWdykw1HP6h7hubXMNLlJfpAFvi4cXDbf8AiAnzM51EzBR+oE/EZaSZpr2Rb7q4mm8ygudTfXEIuLnFaxidnz4Q954IaqZoPcdtdE1OSvUr5TH1cH3ai26zGkzV2HUdC/LCelmIc1ohMKVxMQGjt5im21Xj4j4E1ClGHPtgwzgwm/aAnfCw7O8Sk/1wrNeI7viKRfuUsbKjas0EHKTnEeIuHM9+oxt1cJFcZZeGOPtMBPFxVWccRpxzkgFvAPz8xS9X+4ACsmTmErnTiCRteeIqnjEaH/XMWPfcGzZiBTuotsb74jX2h/Mw/wBIbDFt5zDMulO4uf8AMvMdS5nTdZlL9Z8fQGD95khmH5iW9fSW+Ig/64Bt9yqMEFGEsfnr6Dp89R1uVb1HKMJHmDLz1Kv1B0QK8Qb+C5dencKb4Ya/OZNyqm/RNK2MKJlWSWnojAZ8wp8zAGkOIOE3iG/4jmGxaZinQEXNPLArAoIhoxxE3BavUOm4KcGHSs8w3O0L+IRvXcUUXWLmE4XEL5/iYvvMrM4r7xlDTxKTEWX+4kNcfMp07wBOf2YreaxKT2sVVYBTHzCWDrOYq1tcZblfUXSilxUM3jPl9QXc0MHEQWTeHj7w2MtMLbqVmOX7R09xBSHatVqHPdZjziH75gD8Mq/PL+JiVo5uHm/3UVt84ixGDeDfmIVZVzMdyqKl/wBQ3mJ+Eq5Lqnyfqe0fTBYDyfTetQ/7uGjq4Tl7hEvYbQgvN28fH02lWmLxthpniV6RRlmivMVl6WS7mWt3MbucnK/EN56jk9GIf36qf+4/t1H8tsWjcd3/AOw6cxo9Hkl2PJ6qcPwldfLoi8YtPnpKd7ucuzGt+epfb6JBde0KMeDMHqozfMKbF7rmMPN4YGfOnzOFZwwvFheANc/3BZvT8II6PxLK9GZrXyR/uo/sFdRXg7zFwt+PcFDfQsQqMjbDQQo5/lLiNfph38V1y+5dgy/nzCmOOIFYjEcVuHzCz1jMw8j95W959x4PiEZmajevFnmOT1Mi0uKYYmeobazf+Mqh6JeAWuA/mbG8+IkuMXb49RoPiXuZ/chQhuCM9an74QQih1xBv1CIE+PoqGdIt33cy2cYg0f6odPmGlTRiFOcfQ58/i4vC8wqukFhdR8Nm+aiw6qNF96horglWHevUdHzDBbvviBbbg0nMa6cQ09R4jj9QCu1ykeaxEgvKStH4eI7bAgKmw/BBHaj/ZHS5z8pc/6otrBVH/ZnbVHDEEThVeYq6y0vmBPUl/0gjfGuISX3XqFEs6z/AHCurUv7SIA0M5hxl5+/uBB3iplrbHXfNMW13bTFF7Yv+DLg+LggJ1mUcZKB4+ZbDDjiFANhTMS1mWFq+Lg8HqUUNuGZb6VW4XrmKjxqvMTEjTeBAGHqFa3k9szsgdHASvgMCPBKJn9osJe1qZV2hCAb2zIwQJdCNwH3mDYdPott1UvSuehLnzuVQxx1G7kprMqHitYhtKipdqfb1NMeY6DYgMniH+Z45mK85jsnT3Dh5rqXa2Fw8pcF+4UL7TTwPuvpyF9xY72y5rqXm/mA63VnUwA/EKL1dSrqOkqH/wA9zYtGPL3EWQ2nXqGZhmoqIqqJSuaqIaypDI3y08QF0otD+o2bFF2QgPbETbZoiWXe/wCIUBpzXUJeuDXLyRAUqhV5Iab7z2RVdPEGc5pMbhgpxepmdUnmXhWWl8T1k11Lnu3AKrkj8/EUvMV2OcQatgLhyyZqCrdYhGDd88+JY4rVS7t5zcu9c/wlRfl8/aGzgbzNtc1Cq7xDF3kWj+41WKvLM3HLvqVQXq7qXc6pXUducGJhqwYjfXMw3sgDhfNsqzmrv/kauDzEsRheBcONKX5Yz/VDYYefzKScce4cuREvPcMvlPjzBB6mSH7VHV3rxsmfVl47R8S5K7hD2Rnoec+fEVntzzHx2xAGvOuZ1HuO9Y47lxnXETGPEd1jQlSf+xKr3RPuc9xcDvMuDt3Bfzg2mE//2Q==" data-filename="akodcmange.jpg" style="width: 720px;"><br></p>', N'Hoa Lac', 0.0000, 1, 6, CAST(N'2019-05-02T21:45:13.530' AS DateTime), 1, CAST(N'2019-04-27T22:25:00.860' AS DateTime), CAST(N'2019-02-20T03:03:00.000' AS DateTime), CAST(N'2019-04-27T14:22:00.000' AS DateTime), CAST(N'2019-04-24T14:02:00.000' AS DateTime), 0, NULL, 18, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (68, N'Codebattle 2019', NULL, N'/Images/Events/705f0f07-adf5-43d7-8fe6-b8d8091c1932.jpg', 1, 1, N'Opening', N'Code Battle là một chương trình dựa theo mô hình CTF (Capture the Flag - kiến thức chuyên sâu về bảo mật máy tính) và ACM/ICPC (ACM - International Collegiate Programming Contest - cuộc thi lập trình sinh viên quốc tế). Cuộc thi Code Battle sẽ quay trở lại vào tháng 5/5 sắp tới.Một sự kiện đặc biệt dành cho các clb Về Toán Tin – Công nghệ thông tin do 4 trường đại học tổ chức..------[Thể lệ cuộc thi CODEBATTLE2019]------Hello everyone.Hôm nay Codebattle2019 sẽ khái quát qua về thể lệ cuộc thi nhé!!!💞💞CodeBattle2019 là sự kết hợp sáng tạo từ hai cuộc thi nổi tiếng là ACM/ICPC và CTF (Capture The Flags), đồng thời kết hơp thử nghiệm một nội dung thi mới trong thời kỳ 4.0 là AI (Artificial Intelligence). Đòi hỏi những hiểu biết sâu về thuật toán, các kỹ năng về an toàn thông tin và khả năng làm việc nhóm, kỹ năng lập trình,...Đối tượng tham gia:Các CLB trong địa bàn Hà Nội có thể đăng ký tham gia cuộc thi. Mỗi CLB có thể chọn và thành lập đội tham dự phù hợp với các tiêu chí sau: - Thành viên là sinh viên năm 1, năm 2 tại các trường đại học, cao đẳng.☝️✌️- Mỗi đội tối đa 3 thành viên.Hình thức và nội dung thi:Bao gồm 3 nội dung: Algorithm, Hacking và AI. 1. Đối với các dạng bài Algorithm: Các đội chơi cần viết một đoạn mã chương trình bằng 1 trong 3 ngôn ngữ lập trình C, C++, Java để giải quyết các bài toán do BTC đưa ra.2. Đối với các dạng bài Hacking: đây là phần thi về kiến thức bảo mật, giải quyết bài toán qua bất kỳ ngôn ngữ lập trình nào, kỹ năng google, …. Mỗi bài toán đều có 1 xâu ký tự đặc biệt là đáp án. Nhiệm vụ đội chơi là dựa vào những tài liệu hay thông tin từ đề bài để có thể tìm được đáp án.3. Đối với phần thi AI: đây là phần thi được thử nghiệm lần đầu có hệ thống giải riêng và không tính vào kết quả chung của 2 phần thi trên. Hạng mục cho phép các đội thi sử dụng thuật toán cổ điển để lập trình AI sơ cấp.Cách thức và lịch trình tham gia Cuộc thi được tổ chức thi theo đội nhóm. Mỗi đội gồm 3 thành viên của CLB. Vòng 1: Đăng ký đội thi (30/03 – 20/04) - Các đội thi hoàn thiện hồ sơ đăng ký bao gồm tên đội thi, thông tin của từng thành viên- Họ tên, email và số điện thoại - Mỗi đội gồm 3 người thuộc cùng một CLB – giới hạn mỗi CLB tối đa 15 đội thi. Vòng 2: KHỞI TRANH CODE BATTLE 2019----------------------------------------------------------------------Đây là thể lệ khái quát của cuộc thi chi tiết CodeBattle2019 sẽ giới thiệu ở phần nội dung sau nhé 💌💌___________________________________________Thông tin liên hệ: - Fanpage: facebook.com/codebattlevn - Email: codebattlevietnam@gmail.com - Đại diện BTC: Lê Bình Dương (SĐT: 0983236058) #CodeBattle2019#ProPTIT #ITPTIT #JSClub #HAMIC #TLIT', N'<p style="text-align: justify; "><span style="color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;">Code Battle là một chương trình dựa theo mô hình CTF (Capture the Flag - kiến thức chuyên sâu về bảo mật máy tính) và ACM/ICPC (ACM - International Collegiate Programming Contest - cuộc thi lập trình sinh viên quốc tế). Cuộc thi Code Battle sẽ quay trở lại vào tháng 5/5 sắp tới.Một sự kiện đặc biệt dành cho các clb Về Toán Tin – Công nghệ thông tin do 4 trường đại học tổ chức..</span></p><p style="margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"></p><div style="text-align: justify;"><b><span style="font-size: 18px;">------[Thể lệ cuộc thi CODEBATTLE2019]------</span></b></div><div style="text-align: justify;">Hello everyone.</div><div style="text-align: justify;">Hôm nay Codebattle2019 sẽ khái quát qua về thể lệ cuộc thi nhé!!!<span class="_5mfr" style="font-family: inherit; margin: 0px 1px;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t98="" 1="" 16="" 1f49e.png");"="">💞</span></span><span class="_5mfr" style="font-family: inherit; margin: 0px 1px;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t98="" 1="" 16="" 1f49e.png");"="">💞</span></span></div><div style="text-align: justify;">CodeBattle2019 là sự kết hợp sáng tạo từ hai cuộc thi nổi tiếng là ACM/ICPC và CTF (Capture The Flags), đồng thời kết hơp thử nghiệm một nội dung thi mới trong thời kỳ 4.0 là AI (Artificial Intelligence). Đòi hỏi những hiểu biết sâu về thuật toán, các kỹ năng về an toàn thông tin và khả năng làm việc nhóm, kỹ năng lập trình,...</div><p></p><p style="text-align: justify; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><br></p><div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><p style="margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-family: inherit;"></p><div style="text-align: justify;"><b style="font-family: inherit;">Đối tượng tham gia:</b></div><div style="text-align: justify;"><span style="font-family: inherit;">Các CLB trong địa bàn Hà Nội có thể đăng ký tham gia cuộc thi. Mỗi CLB có thể chọn và thành lập đội tham dự phù hợp với các tiêu chí sau: </span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Thành viên là sinh viên năm 1, năm 2 tại các trường đại học, cao đẳng.</span><span class="_5mfr" style="font-family: inherit; margin: 0px 1px;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t40="" 1="" 16="" 261d.png");"="">☝️</span></span><span class="_5mfr" style="font-family: inherit; margin: 0px 1px;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t61="" 1="" 16="" 270c.png");"="">✌️</span></span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Mỗi đội tối đa 3 thành viên.</span></div><p></p><p style="text-align: justify; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-family: inherit;"><b style="font-family: inherit;">Hình thức và nội dung thi:</b></p><p style="margin: 6px 0px; font-family: inherit;"></p><div style="text-align: justify;"><span style="font-family: inherit;">Bao gồm 3 nội dung: Algorithm, Hacking và AI. </span></div><div style="text-align: justify;"><span style="font-family: inherit;">1. Đối với các dạng bài Algorithm: Các đội chơi cần viết một đoạn mã chương trình bằng 1 trong 3 ngôn ngữ lập trình C, C++, Java để giải quyết các bài toán do BTC đưa ra.</span></div><p></p><p style="text-align: justify; margin: 6px 0px; font-family: inherit;">2. Đối với các dạng bài Hacking: đây là phần thi về kiến thức bảo mật, giải quyết bài toán qua bất kỳ ngôn ngữ lập trình nào, kỹ năng google, …. Mỗi bài toán đều có 1 xâu ký tự đặc biệt là đáp án. Nhiệm vụ đội chơi là dựa vào những tài liệu hay thông tin từ đề bài để có thể tìm được đáp án.</p><p style="text-align: justify; margin: 6px 0px; font-family: inherit;">3. Đối với phần thi AI: đây là phần thi được thử nghiệm lần đầu có hệ thống giải riêng và không tính vào kết quả chung của 2 phần thi trên. Hạng mục cho phép các đội thi sử dụng thuật toán cổ điển để lập trình AI sơ cấp.</p><p style="margin: 6px 0px; font-family: inherit;"></p><div style="text-align: justify;"><b style="font-family: inherit;">Cách thức và lịch trình tham gia </b></div><div style="text-align: justify;"><span style="font-family: inherit;">Cuộc thi được tổ chức thi theo đội nhóm. Mỗi đội gồm 3 thành viên của CLB. </span></div><u><div style="text-align: justify;"><u style="font-family: inherit;">Vòng 1:</u><span style="font-family: inherit;"> Đăng ký đội thi (30/03 – 20/04) </span></div></u><div style="text-align: justify;"><span style="font-family: inherit;">- Các đội thi hoàn thiện hồ sơ đăng ký bao gồm tên đội thi, thông tin của từng thành viên</span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Họ tên, email và số điện thoại </span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Mỗi đội gồm 3 người thuộc cùng một CLB – giới hạn mỗi CLB tối đa 15 đội thi. </span></div><u><div style="text-align: justify;"><u style="font-family: inherit;">Vòng 2: </u><span style="font-family: inherit;">KHỞI TRANH CODE BATTLE 2019</span></div></u><div style="text-align: justify;"><span style="font-family: inherit;">----------------------------------------------------------------------</span></div><span class="_5mfr" style="margin: 0px 1px; font-family: inherit;"><div style="text-align: justify;"><span style="font-family: inherit;">Đây là thể lệ khái quát của cuộc thi chi tiết CodeBattle2019 sẽ giới thiệu ở phần nội dung sau nhé </span><span class="_5mfr" style="font-family: inherit; margin: 0px 1px;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" tf7="" 1="" 16="" 1f48c.png");"="">💌</span></span><span class="_5mfr" style="font-family: inherit; margin: 0px 1px;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; font-size: 16px; background-image: url(" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" tf7="" 1="" 16="" 1f48c.png");"="">💌</span></span></div></span><div style="text-align: justify;"><span style="font-family: inherit;">___________________________________________</span></div><div style="text-align: justify;"><span style="font-family: inherit;">Thông tin liên hệ: </span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Fanpage: </span><a class="profileLink" href="https://www.facebook.com/CodeBattleVN/?__tn__=K-R&eid=ARCM-Wl6Q7ROXbfJfCpfZjVgaZassJOzJBtRqmhAyw9h9JP3EIveJTQFtBFPZri1dmw3LioCZtkPNmn2&fref=mentions&__xts__%5B0%5D=68.ARCLsSKCZ4i8awQbuOaEPC7JSN-oDkgCpBuaLo25CqnKSUzKOJSe-bK9RHPjLMDWISmU2kUUuK8HAMIu8qw6VfGQKCQ3GghyguYV33oAbP-3ZEe46DpxxptdyOchZkaweFjFn-JlMUv-PXEM8ZM-qjFcb5W8WLVf9di4PQkb7t13cDlhSLUhSadhqHWiXZfwegEk42twRq5jK9LrrSz52nN5ATXf81SYYbES3ceK0wUSUw706vvOpJMiWqkUy_fP0QG81ezuMf3ARNCDkGGtXjbfJmbMihk8YgIui9HcdiXuy18_VM2ML-eYM6jObVcJRqSdPc3uQB8GPmgLbJ5qzlrrmveyqQ" data-hovercard="/ajax/hovercard/page.php?id=2044643672473572&extragetparams=%7B%22__tn__%22%3A%22%2CdK-R-R%22%2C%22eid%22%3A%22ARCM-Wl6Q7ROXbfJfCpfZjVgaZassJOzJBtRqmhAyw9h9JP3EIveJTQFtBFPZri1dmw3LioCZtkPNmn2%22%2C%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1" style="font-family: inherit; background-color: rgb(255, 255, 255); color: rgb(54, 88, 153); cursor: pointer;">facebook.com/codebattlevn</a><span style="font-family: inherit;"> </span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Email: codebattlevietnam@gmail.com </span></div><div style="text-align: justify;"><span style="font-family: inherit;">- Đại diện BTC: Lê Bình Dương (SĐT: 0983236058) </span><a class="_58cn" href="https://www.facebook.com/hashtag/codebattle2019?source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARCLsSKCZ4i8awQbuOaEPC7JSN-oDkgCpBuaLo25CqnKSUzKOJSe-bK9RHPjLMDWISmU2kUUuK8HAMIu8qw6VfGQKCQ3GghyguYV33oAbP-3ZEe46DpxxptdyOchZkaweFjFn-JlMUv-PXEM8ZM-qjFcb5W8WLVf9di4PQkb7t13cDlhSLUhSadhqHWiXZfwegEk42twRq5jK9LrrSz52nN5ATXf81SYYbES3ceK0wUSUw706vvOpJMiWqkUy_fP0QG81ezuMf3ARNCDkGGtXjbfJmbMihk8YgIui9HcdiXuy18_VM2ML-eYM6jObVcJRqSdPc3uQB8GPmgLbJ5qzlrrmveyqQ&__tn__=%2ANK-R" data-ft="{" type":104,"tn":"*n"}"="" style="font-family: inherit; background-color: rgb(255, 255, 255); color: rgb(54, 88, 153); cursor: pointer;"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span aria-label="hashtag" class="_58cl _5afz" style="unicode-bidi: isolate; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">CodeBattle2019</span></span></a><a class="_58cn" href="https://www.facebook.com/hashtag/proptit?source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARCLsSKCZ4i8awQbuOaEPC7JSN-oDkgCpBuaLo25CqnKSUzKOJSe-bK9RHPjLMDWISmU2kUUuK8HAMIu8qw6VfGQKCQ3GghyguYV33oAbP-3ZEe46DpxxptdyOchZkaweFjFn-JlMUv-PXEM8ZM-qjFcb5W8WLVf9di4PQkb7t13cDlhSLUhSadhqHWiXZfwegEk42twRq5jK9LrrSz52nN5ATXf81SYYbES3ceK0wUSUw706vvOpJMiWqkUy_fP0QG81ezuMf3ARNCDkGGtXjbfJmbMihk8YgIui9HcdiXuy18_VM2ML-eYM6jObVcJRqSdPc3uQB8GPmgLbJ5qzlrrmveyqQ&__tn__=%2ANK-R" data-ft="{" type":104,"tn":"*n"}"="" style="font-family: inherit; background-color: rgb(255, 255, 255); color: rgb(54, 88, 153); cursor: pointer;"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span aria-label="hashtag" class="_58cl _5afz" style="unicode-bidi: isolate; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">ProPTIT</span></span></a><span style="font-family: inherit;"> </span><a class="_58cn" href="https://www.facebook.com/hashtag/itptit?source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARCLsSKCZ4i8awQbuOaEPC7JSN-oDkgCpBuaLo25CqnKSUzKOJSe-bK9RHPjLMDWISmU2kUUuK8HAMIu8qw6VfGQKCQ3GghyguYV33oAbP-3ZEe46DpxxptdyOchZkaweFjFn-JlMUv-PXEM8ZM-qjFcb5W8WLVf9di4PQkb7t13cDlhSLUhSadhqHWiXZfwegEk42twRq5jK9LrrSz52nN5ATXf81SYYbES3ceK0wUSUw706vvOpJMiWqkUy_fP0QG81ezuMf3ARNCDkGGtXjbfJmbMihk8YgIui9HcdiXuy18_VM2ML-eYM6jObVcJRqSdPc3uQB8GPmgLbJ5qzlrrmveyqQ&__tn__=%2ANK-R" data-ft="{" type":104,"tn":"*n"}"="" style="font-family: inherit; background-color: rgb(255, 255, 255); color: rgb(54, 88, 153); cursor: pointer;"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span aria-label="hashtag" class="_58cl _5afz" style="unicode-bidi: isolate; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">ITPTIT</span></span></a><span style="font-family: inherit;"> </span><a class="_58cn" href="https://www.facebook.com/hashtag/jsclub?source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARCLsSKCZ4i8awQbuOaEPC7JSN-oDkgCpBuaLo25CqnKSUzKOJSe-bK9RHPjLMDWISmU2kUUuK8HAMIu8qw6VfGQKCQ3GghyguYV33oAbP-3ZEe46DpxxptdyOchZkaweFjFn-JlMUv-PXEM8ZM-qjFcb5W8WLVf9di4PQkb7t13cDlhSLUhSadhqHWiXZfwegEk42twRq5jK9LrrSz52nN5ATXf81SYYbES3ceK0wUSUw706vvOpJMiWqkUy_fP0QG81ezuMf3ARNCDkGGtXjbfJmbMihk8YgIui9HcdiXuy18_VM2ML-eYM6jObVcJRqSdPc3uQB8GPmgLbJ5qzlrrmveyqQ&__tn__=%2ANK-R" data-ft="{" type":104,"tn":"*n"}"="" style="font-family: inherit; background-color: rgb(255, 255, 255); color: rgb(54, 88, 153); cursor: pointer;"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span aria-label="hashtag" class="_58cl _5afz" style="unicode-bidi: isolate; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">JSClub</span></span></a><span style="font-family: inherit;"> </span><a class="_58cn" href="https://www.facebook.com/hashtag/hamic?source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARCLsSKCZ4i8awQbuOaEPC7JSN-oDkgCpBuaLo25CqnKSUzKOJSe-bK9RHPjLMDWISmU2kUUuK8HAMIu8qw6VfGQKCQ3GghyguYV33oAbP-3ZEe46DpxxptdyOchZkaweFjFn-JlMUv-PXEM8ZM-qjFcb5W8WLVf9di4PQkb7t13cDlhSLUhSadhqHWiXZfwegEk42twRq5jK9LrrSz52nN5ATXf81SYYbES3ceK0wUSUw706vvOpJMiWqkUy_fP0QG81ezuMf3ARNCDkGGtXjbfJmbMihk8YgIui9HcdiXuy18_VM2ML-eYM6jObVcJRqSdPc3uQB8GPmgLbJ5qzlrrmveyqQ&__tn__=%2ANK-R" data-ft="{" type":104,"tn":"*n"}"="" style="font-family: inherit; background-color: rgb(255, 255, 255); color: rgb(54, 88, 153); cursor: pointer;"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span aria-label="hashtag" class="_58cl _5afz" style="unicode-bidi: isolate; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">HAMIC</span></span></a><span style="font-family: inherit;"> </span><a class="_58cn" href="https://www.facebook.com/hashtag/tlit?source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARCLsSKCZ4i8awQbuOaEPC7JSN-oDkgCpBuaLo25CqnKSUzKOJSe-bK9RHPjLMDWISmU2kUUuK8HAMIu8qw6VfGQKCQ3GghyguYV33oAbP-3ZEe46DpxxptdyOchZkaweFjFn-JlMUv-PXEM8ZM-qjFcb5W8WLVf9di4PQkb7t13cDlhSLUhSadhqHWiXZfwegEk42twRq5jK9LrrSz52nN5ATXf81SYYbES3ceK0wUSUw706vvOpJMiWqkUy_fP0QG81ezuMf3ARNCDkGGtXjbfJmbMihk8YgIui9HcdiXuy18_VM2ML-eYM6jObVcJRqSdPc3uQB8GPmgLbJ5qzlrrmveyqQ&__tn__=%2ANK-R" data-ft="{" type":104,"tn":"*n"}"="" style="font-family: inherit; background-color: rgb(255, 255, 255); color: rgb(54, 88, 153); cursor: pointer;"><span class="_5afx" style="direction: ltr; unicode-bidi: isolate; font-family: inherit;"><span aria-label="hashtag" class="_58cl _5afz" style="unicode-bidi: isolate; font-family: inherit;">#</span><span class="_58cm" style="font-family: inherit;">TLIT</span></span></a></div><p></p></div>', N'Hòa Lạc', 0.0000, 5, 8, CAST(N'2019-05-01T21:02:49.733' AS DateTime), 5, CAST(N'2019-05-01T21:02:57.733' AS DateTime), CAST(N'2019-05-05T07:30:00.000' AS DateTime), CAST(N'2019-05-05T17:30:00.000' AS DateTime), CAST(N'2019-05-04T19:30:00.000' AS DateTime), 0, N'; Thành viên là sinh viên năm 1, năm 2 tại các trường đại học, cao đẳng, mỗi đội tối đa 3 thành viên., ', 100, N'$8^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (69, N'FU Debate Camp 2019 - Vòng đời của rác', NULL, N'/Images/Events/fce94061-96ef-4059-83a7-fa2193f7c6ea.png', 1, 1, N'Closed', N'[DEBATE CAMP 2019 – VÒNG ĐỜI CỦA RÁC]BẠN CÓ BIẾT rằng Trái Đất của chúng ta có khoảng 2,12 tỉ tấn rác thải mỗi năm? Rằng 99% các sản phẩm mà chúng ta mua về sẽ trở thành rác trong vòng 6 tháng? ( Theo The World Counts)BẠN CÓ BIẾT chỉ có một phần nhỏ rác thải được xử lí, tái chế, còn hầu hết sẽ được đốt, chôn xuống đất hay vứt xuống đại dương?BẠN CÓ BIẾT mỗi năm có hơn 200 triệu tấn rác thải nhựa được vứt xuống đại dương và người ta ước tính rằng đến năm 2050, rác thải nhựa trên đại dương còn nhiều hơn cả cá?Bạn sẽ đứng yên hay hành động???Với sức NÓNG BỎNG và CẤP THIẾT của vấn đề môi trường hiện nay, “VÒNG ĐỜI CỦA RÁC” đã được lấy làm chủ đề chính cho Debate Camp lần này. Thông qua đó, chúng mình muốn đem đến cho các bạn trại sinh những góc nhìn rõ hơn, sâu hơn về vấn đề rác thải, tăng nhận thức về thực trạng môi trường hiện nay, những tác động của chính bản thân chúng ta đối với môi trường và đồng thời biết cách xây dựng cách sống xanh, thân thiện với môi trường cho bản thân và tạo ra sự lan tỏa. 💙️Đến với Debate Camp 2019, bằng việc trò chuyện cùng các trainers là những người có kinh nghiệm làm trong tổ chức phi chính phủ về môi trường, và các bạn debaters khác, chúng mình mong muốn rằng mỗi cá nhân sẽ tìm ra cho chính mình câu trả lời cho câu hỏi “ Vấn đề ở đây là gì? Chúng ta có thể làm được gì??? ️Chuỗi hoạt động trong camp gồm có:1. Đấu Debate2. Xem phim tài liệu3. Training “Vòng đời của rác”4. Training Giả lập tình huống5. Thảo luận Cùng đếm ngược tới Debate Camp 2019 nào. Nhanh tay đăng kí để trở thành trại sinh của Camp năm nay nhé---------------------------------------------------------------------------------Thông tin DEBATE CAMP 2019:Đơn vị tham gia: 3 CLB Tranh biện trên địa bàn Hà Nội và các Debaters tự doThời gian: 28/04/2019-29/04/2019Địa điểm: Campus Đại học FPT Hòa Lạc---------------------------------------------------------Mọi thắc mắc xin liên hệ: Trưởng BTC: Bế Khánh Duy SĐT 0829 190 997Phó BTC: Lê Đình Duy SĐT 0946 491 831', N'<p style="text-align: justify; "><b><span style="font-family: inherit; font-size: 24px;">[DEBATE CAMP 2019 – VÒNG ĐỜI CỦA RÁC]</span></b><br></p><p style="display: inline; font-family: inherit;"><div style="text-align: justify;"><b style="font-family: inherit; font-size: 1rem;">BẠN CÓ BIẾT </b><span style="font-family: inherit; font-size: 1rem;">rằng Trái Đất của chúng ta có khoảng 2,12 tỉ tấn rác thải mỗi năm? Rằng 99% các sản phẩm mà chúng ta mua về sẽ trở thành rác trong vòng 6 tháng? ( Theo The World Counts)</span></div></p><p style="display: inline;"><div style="text-align: justify;"><br></div><b style="font-family: inherit;"><div style="text-align: justify;"><b style="font-family: inherit; font-size: 1rem;">BẠN CÓ BIẾT</b><font style="font-size: 1rem; font-weight: 400;"> chỉ có một phần nhỏ rác thải được xử lí, tái chế, còn hầu hết sẽ được đốt, chôn xuống đất hay vứt xuống đại dương?</font></div></b></p><p style="display: inline;"><div style="text-align: justify;"><br></div><span class="text_exposed_show" style="display: inline;"><div style="font-family: inherit; text-align: justify;"><b style="font-family: inherit; font-size: 1rem;">BẠN CÓ BIẾT </b><span style="font-family: inherit; font-size: 1rem;">mỗi năm có hơn 200 triệu tấn rác thải nhựa được vứt xuống đại dương và người ta ước tính rằng đến năm 2050, rác thải nhựa trên đại dương còn nhiều hơn cả cá?</span></div><div style="text-align: justify;"><br></div><font face="inherit"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">Bạn sẽ đứng yên hay hành động???</span></div></font><div style="text-align: justify;"><br></div><font face="inherit"><div style="text-align: justify;"><font style="font-size: 1rem;">Với sức NÓNG BỎNG và CẤP THIẾT của vấn đề môi trường hiện nay, “VÒNG ĐỜI CỦA RÁC” đã được lấy làm chủ đề chính cho Debate Camp lần này.</font><span style="font-family: inherit; font-size: 1rem;">&nbsp;</span></div></font><div style="text-align: justify;"><br></div><font face="inherit"><div style="text-align: justify;"><font style="font-size: 1rem;">Thông qua đó, chúng mình muốn đem đến cho các bạn trại sinh những góc nhìn rõ hơn, sâu hơn về vấn đề rác thải, tăng nhận thức về thực trạng môi trường hiện nay, những tác động của chính bản thân chúng ta đối với môi trường và đồng thời biết cách xây dựng cách sống xanh, thân thiện với môi trường cho bản thân và tạo ra sự lan tỏa.</font><span style="font-family: inherit; font-size: 1rem;">&nbsp;</span><span class="_5mfr" style="font-family: inherit; font-size: 1rem; margin: 0px 1px;"><span class="_6qdm" style="background-repeat: no-repeat; background-size: contain; color: transparent; display: inline-block; text-shadow: none; vertical-align: text-bottom; font-family: inherit; height: 16px; width: 16px; background-image: url(&quot;&quot;);" https:="" static.xx.fbcdn.net="" images="" emoji.php="" v9="" t6c="" 1="" 16="" 1f499.png");"="">💙</span></span></div></font><div style="text-align: justify;"><br></div><font face="inherit"><div style="text-align: justify;"><font style="font-size: 1rem;">️Đến với Debate Camp 2019, bằng việc trò chuyện cùng các trainers là những người có kinh nghiệm làm trong tổ chức phi chính phủ về môi trường, và các bạn debaters khác, chúng mình mong muốn rằng mỗi cá nhân sẽ tìm ra cho chính mình câu trả lời cho câu hỏi “ Vấn đề ở đây là gì? Chúng ta có thể làm được gì???</font><span style="font-family: inherit; font-size: 1rem;">&nbsp;</span></div></font><div style="text-align: justify;"><br></div><font face="inherit"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">️Chuỗi hoạt động trong camp gồm có:</span></div></font><i style="font-family: inherit;"><div style="text-align: justify;"><i style="font-family: inherit; font-size: 1rem;">1. Đấu Debate</i></div><div style="text-align: justify;"><i style="font-family: inherit; font-size: 1rem;">2. Xem phim tài liệu</i></div><div style="text-align: justify;"><i style="font-family: inherit; font-size: 1rem;">3. Training “Vòng đời của rác”</i></div><div style="text-align: justify;"><i style="font-family: inherit; font-size: 1rem;">4. Training Giả lập tình huống</i></div><div style="text-align: justify;"><i style="font-family: inherit; font-size: 1rem;">5. Thảo luận&nbsp;</i></div></i><div style="text-align: justify;"><br></div><b style="font-family: inherit;"><div style="text-align: justify;"><b style="font-family: inherit; font-size: 1rem;">Cùng đếm ngược tới Debate Camp 2019 nào. Nhanh tay đăng kí để trở thành trại sinh của Camp năm nay nhé</b></div></b><span style="font-family: inherit;"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">--------------------------</span><wbr style="font-size: 1rem;"><span class="word_break" style="font-family: inherit; font-size: 1rem; display: inline-block;"></span><span style="font-family: inherit; font-size: 1rem;">--------------------------</span><wbr style="font-size: 1rem;"><span class="word_break" style="font-family: inherit; font-size: 1rem; display: inline-block;"></span><span style="font-family: inherit; font-size: 1rem;">--------------------------</span><wbr style="font-size: 1rem;"><span class="word_break" style="font-family: inherit; font-size: 1rem; display: inline-block;"></span><font style="font-size: 1rem;">---</font></div></span><b style="font-family: inherit;"><div style="text-align: justify;"><b style="font-family: inherit; font-size: 1rem;">Thông tin DEBATE CAMP 2019:</b></div></b><font face="inherit"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">Đơn vị tham gia: 3 CLB Tranh biện trên địa bàn Hà Nội và các Debaters tự do</span></div></font><font face="inherit"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">Thời gian: 28/04/2019-29/04/2019</span></div></font><font face="inherit"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">Địa điểm: Campus Đại học FPT Hòa Lạc</span></div></font><div style="text-align: justify;"><br></div><span style="font-family: inherit;"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">--------------------------</span><wbr style="font-size: 1rem;"><span class="word_break" style="font-family: inherit; font-size: 1rem; display: inline-block;"></span><span style="font-family: inherit; font-size: 1rem;">--------------------------</span><wbr style="font-size: 1rem;"><span class="word_break" style="font-family: inherit; font-size: 1rem; display: inline-block;"></span><font style="font-size: 1rem;">-----</font></div></span><b style="font-family: inherit;"><div style="text-align: justify;"><b style="font-family: inherit; font-size: 1rem;">Mọi thắc mắc xin liên hệ:&nbsp;</b></div></b><font face="inherit"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">Trưởng BTC: Bế Khánh Duy SĐT 0829 190 997</span></div></font><font face="inherit"><div style="text-align: justify;"><span style="font-family: inherit; font-size: 1rem;">Phó BTC: Lê Đình Duy SĐT 0946 491 831</span></div></font></span></p>', N'Hòa Lạc', 0.0000, 73, 38, CAST(N'2019-02-20T01:54:53.657' AS DateTime), 5, CAST(N'2019-02-20T01:55:06.767' AS DateTime), CAST(N'2019-04-28T07:30:00.000' AS DateTime), CAST(N'2019-04-29T19:30:00.000' AS DateTime), CAST(N'2019-04-27T19:30:00.000' AS DateTime), 0, NULL, 50, N'$38^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (70, N'The Event 2', 21, N'/Images/Events/1952ff83-f06d-4411-aba0-95264ea3bcf9.jpg', 0, 0, N'Draft', N'Hello', N'<p>Hello</p>', N'Hanoi', 0.0000, 2, NULL, CAST(N'2019-04-24T22:04:47.220' AS DateTime), 2, CAST(N'2019-04-24T21:56:41.187' AS DateTime), CAST(N'2019-04-24T23:00:00.000' AS DateTime), CAST(N'2019-04-24T23:59:00.000' AS DateTime), CAST(N'2019-04-24T23:00:00.000' AS DateTime), 0, NULL, NULL, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (71, N'Yen Luyen', NULL, N'/Images/Events/6ea53ee2-9ed6-48c3-9a35-e8bb797bad69.jpg', 1, 0, N'Draft', N'abcc', N'<p>abcc</p>', N'Hoa Lac', 0.0000, 1, NULL, CAST(N'2019-04-29T21:04:46.093' AS DateTime), 1, CAST(N'2019-04-29T21:04:34.173' AS DateTime), CAST(N'2019-04-22T14:02:00.000' AS DateTime), CAST(N'2019-04-22T14:02:00.000' AS DateTime), CAST(N'2019-04-21T14:02:00.000' AS DateTime), 0, NULL, NULL, N'$2^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (72, N'Test', NULL, N'/Images/Events/309b45f8-cf6d-4ea7-9539-6d90499e3b5c.jpg', 1, 0, N'Draft', N'Test', N'<p>Test</p>', N'Test', 11000.0000, 5, 39, CAST(N'2019-05-02T03:05:51.377' AS DateTime), NULL, NULL, CAST(N'2020-01-01T01:01:00.000' AS DateTime), CAST(N'2020-01-01T15:03:00.000' AS DateTime), CAST(N'2020-01-01T01:00:00.000' AS DateTime), 0, N'''Kinh doanh quốc tế'', ''Khoa học máy tính'', ''COF'', ''Ngôn ngữ Anh'', ''An toàn thông tin'', ''Thiết kế đồ họa'', ''Ngôn ngữ Nhật'', ''Tài chính ngân hàng'', ''Truyền thông đa phương tiện''', NULL, N'$4^, $5^, $8^, $32^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (73, N'sss', NULL, N'/Images/Events/aed58c7b-1914-4fd9-a491-c432b3a98595.jpg', 1, 0, N'Deleted', N'aaa', N'<p>aaa</p>', N'Other', 2333000.0000, 1, 5, CAST(N'2019-02-20T05:26:27.827' AS DateTime), NULL, NULL, CAST(N'2019-02-20T02:02:00.000' AS DateTime), CAST(N'2019-04-27T14:03:00.000' AS DateTime), CAST(N'2019-04-25T14:02:00.000' AS DateTime), 0, NULL, 200, N'$4^, $5^, $29^, $2^, $34^, ')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (74, N'Test event', 24, N'/Images/Events/30bc3e0e-b686-4ab7-b6f8-0061be1908f7.jpg', 0, 0, N'Draft', N'https://www.youtube.com/watch?v=yIEkYc2s8Q0Day la video nay', N'<p><a href="https://www.youtube.com/watch?v=yIEkYc2s8Q0" target="_blank">https://www.youtube.com/watch?v=yIEkYc2s8Q0</a><a href="https://www.youtube.com/watch?v=yIEkYc2s8Q0"></a></p><p><br></p><p><br></p><p>Day la video nay</p><p><iframe frameborder="0" src="//www.youtube.com/embed/yIEkYc2s8Q0" width="640" height="360" class="note-video-clip"></iframe></p><hr><p><br><img src="https://media.wpri.com/nxs-wpritv-media-us-east-1/photo/2018/11/21/pic%20of%20the%20day%20nov%2021_1542802532936.jpg_62805245_ver1.0_640_360.jpg" style="width: 640px;"><br></p>', N'Phòng f211R', 0.0000, 1, 5, CAST(N'2019-05-02T21:44:54.860' AS DateTime), 1, CAST(N'2019-02-20T02:02:28.063' AS DateTime), CAST(N'2019-04-25T15:33:00.000' AS DateTime), CAST(N'2019-04-25T16:22:00.000' AS DateTime), CAST(N'2019-04-24T03:22:00.000' AS DateTime), 0, NULL, 1, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (75, N'tesst', NULL, N'/Images/Events/d5ebfe3f-709a-4dbd-b869-04e1f949e5ec.jpg', 1, 0, N'Pending', N'aaaa', N'<p>aaaa</p>', N'Other', 0.0000, 1, 41, CAST(N'2019-05-02T20:15:57.733' AS DateTime), NULL, NULL, CAST(N'2019-05-11T15:33:00.000' AS DateTime), CAST(N'2019-05-17T14:02:00.000' AS DateTime), CAST(N'2019-05-08T03:03:00.000' AS DateTime), 1, N'; , ', 20, N'')
GO
INSERT [dbo].[Event] ([EventId], [EventName], [FamilyId], [CoverImage], [IsPublic], [IsFeatured], [EventStatus], [EventDescription], [EventDescriptionHtml], [EventPlace], [EventFee], [OrganizerId], [GroupId], [CreatedDate], [ManagerId], [ApprovedDate], [OpenDate], [CloseDate], [RegisterEndDate], [IsOrganizerOnly], [Participants], [RegisterMax], [TargetGroup]) VALUES (76, N'tesst', NULL, N'/Images/Events/271c411d-a487-4c49-8ab4-e506893b9f51.png', 1, 0, N'Draft', N'aaaa', N'<p>aaaa</p>', N'Hoa Lac', 0.0000, 1, 5, CAST(N'2019-05-02T20:19:50.610' AS DateTime), NULL, NULL, CAST(N'2019-05-17T14:02:00.000' AS DateTime), CAST(N'2019-05-19T14:02:00.000' AS DateTime), CAST(N'2019-05-16T14:02:00.000' AS DateTime), 0, N'MC, JPL, An toàn thông tin, ; aaaa, ', 198, N'')
GO
SET IDENTITY_INSERT [dbo].[Event] OFF
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (1, 6)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (1, 8)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (2, 6)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (3, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (3, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (4, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (4, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (5, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (6, 10)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (7, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (8, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (9, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (10, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (11, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (12, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (13, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (14, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (15, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (16, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (17, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (18, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (18, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (18, 10)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (19, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (20, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (21, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (22, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (23, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (24, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (25, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (26, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (27, 10)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (28, 6)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (28, 8)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (28, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (29, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (29, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (29, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (30, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (31, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (32, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (33, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (34, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (34, 6)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (34, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (38, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (38, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (38, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (39, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (39, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (40, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (40, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (41, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (41, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (41, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (42, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (43, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (43, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (44, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (45, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (46, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (46, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (47, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (47, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (47, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (48, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (48, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (48, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (49, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (49, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (49, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (50, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (50, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (50, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (50, 10)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (51, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (51, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (51, 12)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (52, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (52, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (52, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (53, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (53, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (54, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (54, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (54, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (55, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (56, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (57, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (58, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (59, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (59, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (59, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (60, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (63, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (64, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (65, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (66, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (66, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (66, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (67, 11)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (68, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (69, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (69, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (70, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (71, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (71, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (71, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (71, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (72, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (72, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (73, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (73, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (73, 6)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (73, 7)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (73, 9)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (73, 10)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (74, 1)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (74, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (74, 6)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (75, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (75, 4)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (76, 2)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (76, 3)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (76, 5)
GO
INSERT [dbo].[EventCategory] ([EventId], [CategoryId]) VALUES (76, 6)
GO
SET IDENTITY_INSERT [dbo].[EventFamily] ON 
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (1, N'Miss FPTU')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (2, N'Chess Workshop- Basics Chess')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (3, N'Tết dân gian')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (4, N'Night Concert')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (5, N'Học kỳ Nhật bản')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (6, N'Newbie trip')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (7, N'Amazing race')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (8, N'Tuyển thành viên No Shy')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (9, N'F-Talent Code 2019')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (10, N'Khai Xuân Kì Hội 2019')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (11, N'Estrella')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (12, N'Orientation & Coaching 2019')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (13, N'Company Tour')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (14, N'Tuyển phóng viên cóc đọc')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (15, N'Xuân yêu thương')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (16, N'Save your soul')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (17, N'JS Rose Day')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (18, N'Day of Chess Club')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (19, N'Guitar Girls Plaza')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (20, N'FGC X-Factor')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (21, N'Big Open Day')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (22, N'FPT EDU NihongoEng 2019')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (23, N'Fucking Big Open Day')
GO
INSERT [dbo].[EventFamily] ([FamilyId], [FamilyName]) VALUES (24, N'Test')
GO
SET IDENTITY_INSERT [dbo].[EventFamily] OFF
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 8, 2.5, CAST(N'2019-02-28T00:00:00.000' AS DateTime), N'Ban tổ chức không chọn nhóm mình :<
cuộc thi thất bại!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 38, 5, CAST(N'2019-05-02T20:28:47.593' AS DateTime), N'楽しみ')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 39, 4, CAST(N'2019-04-21T20:08:16.747' AS DateTime), N'Hay')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 41, 2.5, CAST(N'2019-05-01T08:56:01.500' AS DateTime), N'Hay lắm nè')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 51, 4, CAST(N'2019-04-18T00:23:14.640' AS DateTime), N'Giá mà được tài trợ chi phí thì tốt')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 52, 4, CAST(N'2019-04-20T20:31:39.007' AS DateTime), N'123')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 56, 5, CAST(N'2019-05-02T20:26:19.490' AS DateTime), N'Một sân chơi đầy bổ ích, Cảm ơn bố mẹ đã sinh ra em để em được tham gia cuộc thi này.')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 57, 5, CAST(N'2019-04-29T20:10:48.987' AS DateTime), N'Yay đi thôi')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (1, 71, 0.5, CAST(N'2019-04-25T01:26:55.547' AS DateTime), N'abc')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (2, 8, 5, CAST(N'2019-01-16T00:00:00.000' AS DateTime), N'Nhóm mình giành được giải nhất nè, hihi')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (2, 51, 4, CAST(N'2019-04-18T00:36:53.410' AS DateTime), N'Sự kiện đã thay đổi hoàn toàn góc nhìn của mình!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (3, 51, 4.5, CAST(N'2019-04-18T00:27:23.593' AS DateTime), N'Khá thú vị, mong muốn đi!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (3, 57, 4.5, CAST(N'2019-02-20T16:38:50.250' AS DateTime), N'Mình sẽ đăng kí sớm thôi :))')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (4, 8, 4, CAST(N'2019-01-16T00:00:00.000' AS DateTime), N'Chúc mừng nhóm Batah team nha!!!
Ban tổ chức cũng xuất sắc lắm!!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (4, 51, 5, CAST(N'2019-04-18T00:30:30.673' AS DateTime), N'Mình đã đi kì trước, chuyến đi có nhiều trải nghiệm thú vị!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (4, 68, 4.5, CAST(N'2019-05-01T22:22:12.057' AS DateTime), N'test')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 6, 5, CAST(N'2019-02-20T01:04:20.480' AS DateTime), N'Hay thế')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 8, 4.5, CAST(N'2019-01-18T00:00:00.000' AS DateTime), N'Giải thưởng lớn quá, tiếc là mình không tham dự được :(')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 9, 5, CAST(N'2019-04-20T18:23:25.420' AS DateTime), N'Rất hay và bổ ích')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 10, 5, CAST(N'2019-04-22T06:08:44.640' AS DateTime), N'Hay quá, cho 10 điểm! Mình còn được giải 1 cơ mà =))')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 38, 5, CAST(N'2019-04-16T19:46:52.200' AS DateTime), N'Nghe đã biết xịn rồi, vừa rẻ vừa được học nhiều thứ hay, lại còn được đi 2 nước cơ aaaaaaaaaaaaaaaaaaaaa')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 39, 5, CAST(N'2019-04-16T20:09:56.577' AS DateTime), N'Mình là thủ lĩnh, và mình nghĩ trường nên tạo nhiều điều kiện tổ chức thêm vài chương trình như thế này nữa. Rất hay ạ :D')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 41, 3.5, CAST(N'2019-04-20T18:34:42.940' AS DateTime), N'Đã đăng kí!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 45, 4, CAST(N'2019-04-15T23:57:42.843' AS DateTime), N'Em rất mong chờ sự kiện này xảy ra ạ.')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 51, 4, CAST(N'2019-02-20T01:03:58.750' AS DateTime), N'Nghe có vẻ thích, nhưng mình rất lười -_-')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 56, 5, CAST(N'2019-04-20T18:48:09.883' AS DateTime), N'Đã đăng kí và sẽ cố gắng hết sức :D')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (5, 68, 4.5, CAST(N'2019-04-22T05:57:30.747' AS DateTime), N'Mong chờ')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (8, 38, 0.5, CAST(N'2019-04-23T21:48:33.673' AS DateTime), N'Trúng lúc bảo vệ đồ án nên không thể tham gia rồi :( Vẫn vote 5 sao vì chương trình thú vị ạ !')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (8, 39, 5, CAST(N'2019-04-15T22:27:41.927' AS DateTime), N'This program is very good. Let''s join it.')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (26, 56, 0.5, CAST(N'2019-04-20T16:04:50.670' AS DateTime), N'Sao trường chờ em ra trường mới tổ chức cái cuộc thi này ạ :(( Ko công bằng =((((')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (26, 57, 4.5, CAST(N'2019-04-20T16:04:15.607' AS DateTime), N'Không biết ra trường rồi thì có tham gia chương trình được không nhỉ?')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (29, 38, 3.5, CAST(N'2019-04-20T17:48:45.253' AS DateTime), N'Mình có thắc mắc là lúc đấy đang ở Singapore luôn thì chi phí có giảm không hay sao?')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (31, 38, 4.5, CAST(N'2019-04-20T17:57:20.133' AS DateTime), N'Éc éc, híc híc, ựa ựa... nhìn chung là rất chất!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (32, 38, 5, CAST(N'2019-04-20T17:55:51.237' AS DateTime), N'Vừa mới ra trường đã thấy chương trình hay :( BTC cố lên nhé!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (35, 38, 3.5, CAST(N'2019-04-21T21:13:55.310' AS DateTime), N'kawaiii')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (35, 41, 0.5, CAST(N'2019-04-23T22:36:22.817' AS DateTime), N'Giờ còn đăng ký đk hok ta')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (35, 51, 5, CAST(N'2019-04-18T00:31:52.767' AS DateTime), N'Tưởng không vui mà vui không tưởng')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (35, 56, 5, CAST(N'2019-04-29T20:33:20.883' AS DateTime), N'Thích sự kiện này quá <3')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (35, 57, 4, CAST(N'2019-04-20T15:09:22.720' AS DateTime), N'So excited to join!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (36, 38, 4.5, CAST(N'2019-04-20T17:40:41.053' AS DateTime), N'Khá thú vị')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (36, 51, 4.5, CAST(N'2019-04-18T00:34:08.497' AS DateTime), N'Ổn đấy!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (37, 51, 4, CAST(N'2019-04-18T00:35:29.793' AS DateTime), N'Khá bảnh!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (37, 57, 5, CAST(N'2019-04-20T15:56:50.327' AS DateTime), N'Sẽ dành giải quán quân!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (40, 38, 4.5, CAST(N'2019-04-20T17:41:49.850' AS DateTime), N'Rất mong chờ về sự kiện này :) ')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (42, 38, 0.5, CAST(N'2019-04-20T17:50:25.253' AS DateTime), N'Thấy chương trình chỉ có chơi chứ học cái gì đâu?')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (43, 38, 4, CAST(N'2019-04-20T17:52:14.277' AS DateTime), N'Không thích mấy cái khóa này, nhưng feedback ủng hộ BTC là bạn thân lâu năm :v')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (44, 38, 5, CAST(N'2019-04-20T17:53:31.227' AS DateTime), N'Mong chờ chương trình, vote tạm 5 sao, đi rồi ko thấy hay thì down vote nhá!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (45, 38, 4.5, CAST(N'2019-04-20T17:59:32.220' AS DateTime), N'Rất hay!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (46, 38, 4.5, CAST(N'2019-04-20T18:04:34.763' AS DateTime), N'Chương trình có hỗ trợ thêm về tài chính không ạ?')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (47, 38, 3, CAST(N'2019-04-20T18:07:28.237' AS DateTime), N'Lần này không được đi vì trúng cuộc đua số rồi ạ :(')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (48, 38, 3.5, CAST(N'2019-04-20T18:11:46.650' AS DateTime), N'Thú vị ')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (49, 38, 1.5, CAST(N'2019-04-20T18:13:12.693' AS DateTime), N'Không biết nói tiếng Anh nên không đi được.')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (51, 38, 5, CAST(N'2019-04-20T18:40:49.363' AS DateTime), N'Ủng hộ các em')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (57, 38, 5, CAST(N'2019-04-21T01:06:44.253' AS DateTime), N'Sư kiện rất đáng mong chờ')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (58, 38, 5, CAST(N'2019-04-21T11:47:31.257' AS DateTime), N'So excited to join this event!')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (60, 38, 4.5, CAST(N'2019-04-21T11:49:43.910' AS DateTime), N'Hay')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (71, 57, 5, CAST(N'2019-04-22T00:07:12.927' AS DateTime), N'Tuyệt vời ông mặt trời')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (93, 69, 4.5, CAST(N'2019-04-25T22:36:38.273' AS DateTime), N'Ủng hộ')
GO
INSERT [dbo].[Feedback] ([UserId], [EventId], [Value], [CreatedDate], [FeedbackContent]) VALUES (100, 41, 5, CAST(N'2019-02-20T00:16:25.813' AS DateTime), N'Tuyệt vời hihi')
GO
INSERT [dbo].[FeedbackOuter] ([Key], [EventId], [Value], [CreatedDate], [FeedbackContent], [UserName], [UserImage]) VALUES (N'1758724840893851', 10, 4.5, CAST(N'2019-04-12T00:54:31.587' AS DateTime), N'Test nè', N'Phương Nguyễn', N'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1758724840893851&height=200&width=200&ext=1557597263&hash=AeScQIK59ZU9CQgb')
GO
INSERT [dbo].[FeedbackOuter] ([Key], [EventId], [Value], [CreatedDate], [FeedbackContent], [UserName], [UserImage]) VALUES (N'1781536618613168', 57, 5, CAST(N'2019-04-21T13:58:05.510' AS DateTime), N'Hay quá đi, kiểu gì mình cũng có giải', N'Bo Cong Anh', N'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1781536618613168&height=200&width=200&ext=1558421816&hash=AeQ7y1sRTvYikQCO')
GO
INSERT [dbo].[FeedbackOuter] ([Key], [EventId], [Value], [CreatedDate], [FeedbackContent], [UserName], [UserImage]) VALUES (N'2138011532980792', 10, 0.5, CAST(N'2019-04-12T00:30:18.657' AS DateTime), N'aaa', N'Nguyễn Hùng Tiến', N'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2138011532980792&height=200&width=200&ext=1557595789&hash=AeT3Z8Abh-r2gbg2')
GO
INSERT [dbo].[FeedbackOuter] ([Key], [EventId], [Value], [CreatedDate], [FeedbackContent], [UserName], [UserImage]) VALUES (N'793384021016953', 38, 5, CAST(N'2019-04-24T22:26:26.017' AS DateTime), N'Ổn đấy :)) đã đăng kí, hi vọng còn slot', N'Mỹ Duyên', N'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=793384021016953&height=200&width=200&ext=1558711567&hash=AeQ1c1vm-ssU8zBT')
GO
INSERT [dbo].[FeedbackOuter] ([Key], [EventId], [Value], [CreatedDate], [FeedbackContent], [UserName], [UserImage]) VALUES (N'793384021016953', 69, 4.5, CAST(N'2019-04-23T21:43:19.217' AS DateTime), N'Ủng hộ sự kiện :))', N'Mỹ Duyên', N'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=793384021016953&height=200&width=200&ext=1558622584&hash=AeQVM9aOPQ9R-k_P')
GO
SET IDENTITY_INSERT [dbo].[Form] ON 
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (2, 4, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2018-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (3, 5, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2018-05-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (5, 7, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (10, 12, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (44, 9, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-04-20T18:23:11.453' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (46, 47, N'https://docs.google.com/forms/d/1zX_2YS8uQNGSHh0KSUAg_12lHYvRsCF0ptAYUI_TA-w/edit', N'https://docs.google.com/forms/d/e/1FAIpQLScPxeDvT9ZIHL0yhRPQHDkAAg_qjMmULIgyxv8EvhKzHMyj2A/viewform?usp=pp_url&entry.239484714=ThangLVSE04854&entry.1711799306=1997-01-13&entry.162377397=thanglvse04854@fpt.edu.vn&entry.2117412344=0332554026&embedded=true', N'https://docs.google.com/forms/d/1zX_2YS8uQNGSHh0KSUAg_12lHYvRsCF0ptAYUI_TA-w/viewanalytics?embedded=true', CAST(N'2019-04-20T22:27:07.703' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (47, 49, N'https://docs.google.com/forms/d/1mzGUucvG7m31a_-tJqPz9NW4zXqdvDiFwvCnL6q6XCY/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSeBfcZmSchkr_sQMOurnxIiJCG3Bim9hbRCtSaE1NqhMPfHSw/viewform?usp=pp_url&entry.1085362805=ThangLVSE04854&entry.1689651981=1997-01-13&entry.1775387557=thanglvse04854@fpt.edu.vn&entry.1655586440=0332554026&embedded=true', N'https://docs.google.com/forms/d/1mzGUucvG7m31a_-tJqPz9NW4zXqdvDiFwvCnL6q6XCY/viewanalytics?embedded=true', CAST(N'2019-04-20T23:07:48.563' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (51, 44, N'https://docs.google.com/forms/d/1eiEqmFaAPdEEO191aUOya-woCBzCCnB6RHaYWsjCpv0/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSde5r6nnZ7jmulaLbYqQ_UDsAueUeuuhitqPzL8neKvVHYryA/viewform?usp=pp_url&entry.1862883321=ThangLVSE04854&entry.1529378116=1997-01-13&entry.495271899=thanglvse04854@fpt.edu.vn&entry.2118770351=0332554026&embedded=true', N'https://docs.google.com/forms/d/1eiEqmFaAPdEEO191aUOya-woCBzCCnB6RHaYWsjCpv0/viewanalytics?embedded=true', CAST(N'2019-04-21T12:09:19.890' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (56, 18, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-04-21T12:47:01.657' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (61, 67, N'https://docs.google.com/forms/d/1XZzVLmlrgdQjxISOYPkKRnGy_m5SFBZbbp2pnXgto8g/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSdL4-5VWG0cRDzzzEGqmWvGRWVZ_gGhs7INUOt4h9tmktM7Rg/viewform?usp=pp_url&entry.264958970=ThangLVSE04854&entry.2005575450=1997-01-13&entry.1167356760=thanglvse04854@fpt.edu.vn&entry.1910955801=0332554026&embedded=true', N'https://docs.google.com/forms/d/1XZzVLmlrgdQjxISOYPkKRnGy_m5SFBZbbp2pnXgto8g/viewanalytics?embedded=true', CAST(N'2019-04-21T23:04:52.327' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (68, 3, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-04-23T23:16:24.657' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (73, 70, N'https://docs.google.com/forms/d/1YyN334DRelukoncjL7aTmSxP3T14979EcWuUTOacciA/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSeS3NcVkn23qlh6FVz8dFee2uFej__eOB1aqnmvoa6W9sFj0A/viewform?usp=pp_url&entry.795064808=ThangLVSE04854&entry.1868284781=1997-01-13&entry.768006182=thanglvse04854@fpt.edu.vn&entry.1037440894=0332554026&embedded=true', N'https://docs.google.com/forms/d/1YyN334DRelukoncjL7aTmSxP3T14979EcWuUTOacciA/viewanalytics?embedded=true', CAST(N'2019-04-24T22:04:47.220' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (74, 71, N'https://docs.google.com/forms/d/1lO-_OGyoxJvXSBEvGDXw3NoOoS_yjAWD_qb_uWGRIN4/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSeG1imW4kEqpFuYe4633JwjfO_saq3AxByuMAodGgdI5XPg_A/viewform?usp=pp_url&entry.1959654379=ThangLVSE04854&entry.343138618=1997-01-13&entry.1241337354=thanglvse04854@fpt.edu.vn&entry.1344559335=0332554026&embedded=true', N'https://docs.google.com/forms/d/1lO-_OGyoxJvXSBEvGDXw3NoOoS_yjAWD_qb_uWGRIN4/viewanalytics?embedded=true', CAST(N'2019-04-25T01:24:44.483' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (76, 56, N'https://docs.google.com/forms/d/1z3pMDinjRgGZX4oNBF7Yh-tSUMR5Gr6sF_ICST82IZo/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSeQdkevn_-FK19Fgn5Itgm4xot4I4hK-eCpKm5lICoB04L_vQ/viewform?usp=pp_url&entry.1715769660=ThangLVSE04854&entry.702610186=1997-01-13&entry.1212152801=thanglvse04854@fpt.edu.vn&entry.116965394=0332554026&embedded=true', N'https://docs.google.com/forms/d/1z3pMDinjRgGZX4oNBF7Yh-tSUMR5Gr6sF_ICST82IZo/viewanalytics?embedded=true', CAST(N'2019-02-20T01:41:29.377' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (78, 41, N'https://docs.google.com/forms/d/1wjuuSI1_HcSyWgg4XnlvnxnttVPAjoaQbbC_0H5Tze4/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSc0BKdET99hf4OrqmLJy4g3ff4JMvT7g6EeM5wMqssg6qhkYw/viewform?usp=pp_url&entry.547838405=ThangLVSE04854&entry.1591795084=1997-01-13&entry.2130651168=thanglvse04854@fpt.edu.vn&entry.60036003=0332554026&embedded=true', N'https://docs.google.com/forms/d/1wjuuSI1_HcSyWgg4XnlvnxnttVPAjoaQbbC_0H5Tze4/viewanalytics?embedded=true', CAST(N'2019-02-20T01:43:33.140' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (79, 38, N'https://docs.google.com/forms/d/1xXaWR404LOse-YWeHQbBEgA8xffH0Ztws1GlAbkRZBA/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSehQSfcwuQvyMNMu7FNE5ad8GrF9tpHdDCMuSui4GGrfzg5FA/viewform?usp=pp_url&entry.2136228084=ThangLVSE04854&entry.1256107218=1997-01-13&entry.108773365=thanglvse04854@fpt.edu.vn&entry.235571913=0332554026&embedded=true', N'https://docs.google.com/forms/d/1xXaWR404LOse-YWeHQbBEgA8xffH0Ztws1GlAbkRZBA/viewanalytics?embedded=true', CAST(N'2019-02-20T01:43:45.093' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (81, 51, N'https://docs.google.com/forms/d/10Z1BsAMsOQVeAL3DQHb8ykcUh41rqPFXu3UJxf1EM9U/edit', N'https://docs.google.com/forms/d/e/1FAIpQLScShllO8602B0Ph4e0xhRL80BwXRmrwz_mFU-2ydWSMVpu6wg/viewform?usp=pp_url&entry.1079965859=ThangLVSE04854&entry.837225902=1997-01-13&entry.1731595408=thanglvse04854@fpt.edu.vn&entry.1800208370=0332554026&embedded=true', N'https://docs.google.com/forms/d/10Z1BsAMsOQVeAL3DQHb8ykcUh41rqPFXu3UJxf1EM9U/viewanalytics?embedded=true', CAST(N'2019-02-20T01:44:29.157' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (82, 45, N'https://docs.google.com/forms/d/1-NSuZtqii3haN6YMIObxUM48ov6dXQgyzUM_4vb-74M/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSd8kIik2FkS5zbkNetV-TtmlwzsymzJ5ARNDsX4_F_12_riZw/viewform?usp=pp_url&entry.1501009126=ThangLVSE04854&entry.1644490591=1997-01-13&entry.714784524=thanglvse04854@fpt.edu.vn&entry.1047446744=0332554026&embedded=true', N'https://docs.google.com/forms/d/1-NSuZtqii3haN6YMIObxUM48ov6dXQgyzUM_4vb-74M/viewanalytics?embedded=true', CAST(N'2019-02-20T01:46:08.127' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (83, 40, N'https://docs.google.com/forms/d/1i-FuOSmJHNXmw0dWWsdDVf-9-X4WmdVWiY6Q_CJA9Gc/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSedeHrr4Ce8hxTSZaphqFwqLwtF9etfqbZ0rsDdEKrAxfiL-g/viewform?usp=pp_url&entry.1460515575=ThangLVSE04854&entry.1218073800=1997-01-13&entry.585447260=thanglvse04854@fpt.edu.vn&entry.172889523=0332554026&embedded=true', N'https://docs.google.com/forms/d/1i-FuOSmJHNXmw0dWWsdDVf-9-X4WmdVWiY6Q_CJA9Gc/viewanalytics?embedded=true', CAST(N'2019-02-20T01:47:20.657' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (85, 69, N'https://docs.google.com/forms/d/1ls-JFVdD0dQD1qx6djMmpE1rH_mNyavcKOXrlnS0FSs/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSeShTqXKBq8frUIyDUAUtxFXw2y3JL2oW3wH4-Ut0-MMNi6rg/viewform?usp=pp_url&entry.1881704890=ThangLVSE04854&entry.1132863953=1997-01-13&entry.914980099=thanglvse04854@fpt.edu.vn&entry.1338699300=0332554026&embedded=true', N'https://docs.google.com/forms/d/1ls-JFVdD0dQD1qx6djMmpE1rH_mNyavcKOXrlnS0FSs/viewanalytics?embedded=true', CAST(N'2019-02-20T01:54:53.657' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (87, 42, N'https://docs.google.com/forms/d/1hFyTrD16U69PIv3C33FZefiIEr2pCewq0CxY4-Cd4OA/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSfNOJolCVpDxuiBUbN_mluspLbERB0Oc8l50EZlC-uJVkkfNA/viewform?usp=pp_url&entry.84598528=ThangLVSE04854&entry.1784985863=1997-01-13&entry.1719326975=thanglvse04854@fpt.edu.vn&entry.858957936=0332554026&embedded=true', N'https://docs.google.com/forms/d/1hFyTrD16U69PIv3C33FZefiIEr2pCewq0CxY4-Cd4OA/viewanalytics?embedded=true', CAST(N'2019-02-20T01:59:16.937' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (90, 27, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-02-20T02:01:10.017' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (91, 74, N'https://docs.google.com/forms/d/10UpVMeu5hGW8REPLNwLDYMCsZWC-BGHRzPeMOuA6Tto/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSfpsAJXfsal6Ewkteh9B7F1xJcNxPMy8HGyRoPa-Uq5RrImYQ/viewform?usp=pp_url&entry.77484277=ThangLVSE04854&entry.70835196=1997-01-13&entry.1929823824=thanglvse04854@fpt.edu.vn&entry.1519536382=0332554026&embedded=true', N'https://docs.google.com/forms/d/10UpVMeu5hGW8REPLNwLDYMCsZWC-BGHRzPeMOuA6Tto/viewanalytics?embedded=true', CAST(N'2019-02-20T02:02:15.437' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (93, 34, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-02-20T02:12:40.983' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (94, 6, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-02-20T02:19:02.577' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (97, 8, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-02-20T02:22:12.470' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (98, 11, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-02-20T04:47:26.140' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (99, 10, N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', N'https://goo.gl/forms/dFzbOC2L6g0u1ynf2', CAST(N'2019-02-20T04:47:41.470' AS DateTime))
GO
INSERT [dbo].[Form] ([FormId], [EventId], [FormLink], [FormRegister], [FormResult], [CreatedDate]) VALUES (100, 43, N'https://docs.google.com/forms/d/1M2hcA0MQ1ubrjo18rSqBk2MNmQyRPRGdbOPCreYM2sc/edit', N'https://docs.google.com/forms/d/e/1FAIpQLSe4WgGhTeOF2NO2x8jB3Hw-eimqjJTRDE0zGFPdNbYVjUpJbw/viewform?usp=pp_url&entry.1429928612=ThangLVSE04854&entry.1805681664=1997-01-13&entry.1832696572=thanglvse04854@fpt.edu.vn&entry.458959013=0332554026&embedded=true', N'https://docs.google.com/forms/d/1M2hcA0MQ1ubrjo18rSqBk2MNmQyRPRGdbOPCreYM2sc/viewanalytics?embedded=true', CAST(N'2019-02-20T04:48:44.767' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Form] OFF
GO
SET IDENTITY_INSERT [dbo].[Group] ON 
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (1, N'Câu lạc bộ Cờ', N'/Images/Groups/FCC.jpg', N'FU Chess Club - CLB Cờ
Chính thức thành lập vào ngày 30/07/2012 bởi chủ nhiệm đời đầu là anh Trần Hoàng Vy (cựu sinh viên K7), CLB Cờ (FCC) đã bắt đầu sinh hoạt với 30 thành viên đầu tiên. Từ lúc bắt đầu đi vào hoạt động chính thức, CLB đã trải qua rất nhiều khó khăn khi những buổi sinh hoạt đầu tiên mọi người phải sinh hoạt ở thư viện và thiếu nhiều cơ sở vật chất để các thành viên có thể giao lưu, sinh hoạt cùng nhau. Nhưng với ý tưởng tìm những người có chung sở thích và để nâng cao khả năng của chính mình, mọi người đã đến với nhau và đã xây dựng nên một cộng đồng người yêu cờ ở FU.
Thế rồi trải qua gần 4 năm phát triển, CLB Cờ đã từng bước được chuyên môn hóa và phát triển hơn, đã có thời gian số thành viên của CLB lên đến hơn 100 người, sinh hoạt nhiều nhất gần 50 người (buổi họp), quỹ CLB có lúc đạt ngưỡng 6 triệu và có nhiều học kỳ CLB đạt được danh hiệu Xuất sắc của trường trong đó có 2 kỳ Spring và Summer 2013, kỳ Spring 2015, và Spring 2016 . FCC đạt được thành tích là 1 trong 5 CLB Xuất sắc của trường đánh dấu những bước tiến vững chắc của CLB.
Ngoài ra, CLB Cờ là nơi tập trung và nung đúc những tài năng trẻ, đưa những kỳ thủ trẻ đi cọ sát và giành những giải thưởng lớn khắp miền Bắc. Trong đó, nổi bật là kiện tướng quốc gia Đồng Bảo Nghĩa (cựu sinh viên) , anh đã giành rất nhiều giải thưởng lớn có quy mô toàn quốc và châu lục trong màu áo của đội tuyển Bắc Giang hay đội tuyển Việt Nam. Đối với trường ĐH FPT, gần đây nhất, anh đã giành huy chương Vàng tại Đại hội Thể dục thể thao sinh viên toàn quốc 2015 được tổ chức tại Bắc Ninh. Ngoài ra, ta không thể không kể đến những cái tên như Phạm Minh Đức (cựu sinh viên K7), Nguyễn Huy Hoàng (sinh viên K8), Lê Hà Phan (sinh viên K10), Mỹ Duyên (K12)… Đấy là những cái tên có tiếng nổi lên ở các giải đấu cờ không chuyên có quy mô lớn ở Hà Nội. 
Ở trường ĐH FPT, hằng năm CLB Cờ chính là đơn vị tổ chức 2 giải Cờ chính, trong đó một giải cờ thường tổ chức vào kỳ Fall là giải Chess Tournament và một giải được tổ chức vào kỳ Spring, được gọi là giải Khai Xuân Kỳ Hội. Đây là môi trường mà các kỳ thủ của trường ĐH FPT và Fschool có thể tranh tài với nhau và tạo điều kiện cho chính FCC tìm kiếm ra những tài năng trẻ. Nội dung của các giải đấu đều rất đa dạng khi có các loại cờ khác nhau từ những môn Cờ rất cổ điển, cần suy nghĩ cho đến những môn Cờ mang tính giải trí cao như: cờ Vua, cờ Tướng, cờ Caro, cờ Domino, cờ Cá Ngựa,… Tất cả tạo nên những điều đặc biệt trong các giải đấu mở rộng này và tạo sự thu hút lớn cho cộng đồng trường ĐH FPT, Hòa Lạc với bình quân có từ 100-150 kỳ thủ tham gia các giải đấu. Trong tương lai, CLB Cờ sẽ nâng cao chất lượng giải đấu hơn với hi vọng thu hút được nhiều kỳ thủ tham gia hơn cũng như tạo ra một sân chơi trí tuệ và vô cùng bổ ích cho sinh viên, học sinh sau những giờ học căng thẳng.', NULL, NULL, 0, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 3, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (2, N'Câu lạc bộ No Shy', N'/Images/Groups/NSC.jpg', NULL, N'noshy@gmail.com', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 30, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (3, N'Phòng phát triển cá nhân', N'/Images/Groups/PDP.jpg', NULL, NULL, NULL, 0, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 3, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (4, N'Phòng công tác sinh viên SRO', N'/Images/Groups/a9f3a163-1fef-4835-a526-d02e89030467.jpg', N'Phòng công tác sinh viên là phòng ban gồm các cán bộ trẻ, giúp sinh viên kiếm việc làm, giúp định hướng sinh viên,... thông qua những workshop định hướng kì thực tập, workshop kĩ năng, workshop trò chuyện với những diễn giả nổi tiến, các Company Tour hay là những buổi giao lưu với các đối tác công ty trong và ngoài nước,...', N'fpgfu@gmail.com', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 50, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (5, N'Câu lạc bộ FTIC', N'/Images/Groups/FTIC.jpg', NULL, NULL, NULL, 1, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 3, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (6, N'Câu lạc bộ Cóc Đọc và Những người bạn', N'/Images/Groups/CocDoc.jpg', NULL, N'aaaaaasss@gmail.com', NULL, 0, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 5, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (7, N'Câu lạc bộ Vì Cộng Đồng iGo', N'/Images/Groups/iGo.jpg', N'Có người nói chúng tôi là 1 nhóm, 1 đội chưa đủ tầm để thành 1 tổ chức tình nguyện vì cộng đồng. Nhưng với lòng nhiệt huyết của các tình nguyện viên, sinh viên FPT, chúng tôi sẵn sàng góp sức trẻ, giúp cuộc sống của những bất hạnh trên thế giới này có thêm giây phút ấm áp từ chính những người Việt trẻ.
Là 1 sân chơi
Là 1 tổ chức  
Là 1 gia đình 
iGo club luôn chào đón các tấm lòng thiện nguyện.
Vì 1 thế giới tốt đẹp hơn ! ', NULL, CAST(N'2010-02-20' AS Date), 1, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 3, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (8, N'Câu lạc bộ Kỹ sư Cầu nối Nhật Bản - JS', N'/Images/Groups/JS.jpg', NULL, N'js@gmail.com', NULL, 1, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 4, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (9, N'Câu lạc bộ Guitar - FGC', N'/Images/Groups/FGC.jpg', NULL, NULL, NULL, 0, 1, CAST(N'2019-04-15T21:50:49.450' AS DateTime), 3, CAST(N'2019-04-15T21:50:49.450' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (10, N'JS1102 Class', N'/Images/Groups/DSC04839.jpg', N'Đây là lớp JS1102', NULL, NULL, 0, 5, CAST(N'2019-04-11T21:35:01.220' AS DateTime), 5, CAST(N'2019-04-11T21:35:01.220' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (11, N'Test nè', N'/Images/Groups/IMG_7915.jpg', N'Thử xem create group có bị lỗi không', NULL, NULL, 0, 3, CAST(N'2019-04-15T22:52:13.407' AS DateTime), 3, CAST(N'2019-04-15T22:52:13.407' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (12, N'Câu lạc bộ Guitar', N'/Images/Groups/44.jpg', N'a', NULL, NULL, 0, 5, CAST(N'2019-04-15T22:53:16.267' AS DateTime), 5, CAST(N'2019-04-15T22:53:16.267' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (13, N'Guitar club', NULL, NULL, NULL, NULL, 0, 5, CAST(N'2019-04-15T22:58:42.093' AS DateTime), 5, CAST(N'2019-04-15T22:58:42.093' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (14, N'Câu lạc bộ Guitar', NULL, NULL, NULL, NULL, 0, 5, CAST(N'2019-04-15T23:04:05.453' AS DateTime), 5, CAST(N'2019-04-15T23:04:05.453' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (15, N'JS1102 Class', N'/Images/Groups/7cc14acc-d22f-49f7-8afb-a8e60a4ecc3f.jpg', NULL, NULL, NULL, 0, 5, CAST(N'2019-04-15T23:04:32.267' AS DateTime), 5, CAST(N'2019-04-15T23:04:32.267' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (16, N'Test cái create', N'/Images/Groups/IMG_7915.jpg', N'Thử chức năng create', NULL, NULL, 0, 3, CAST(N'2019-04-15T23:05:58.407' AS DateTime), 3, CAST(N'2019-04-15T23:05:58.407' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (17, N'Câu lạc bộ demo', N'/Images/Groups/Screenshot (40).png', N'ahihi mình test thôi', NULL, NULL, 0, 1, CAST(N'2019-04-15T23:09:34.767' AS DateTime), 1, CAST(N'2019-04-15T23:09:34.767' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (18, N'Phòng phát triển cá nhân - ICPDP', N'/Images/Groups/e45c17b3-04dc-4203-83f2-7fdf7811fa98.jpg', N'Là phòng giúp cho sự phát triển của sinh viên thông qua việc giúp sinh viên tổ chức các hoạt động ngoài khoa và tổ chức các ho động nằm năng cao kĩ năng sống,...', NULL, NULL, 0, 5, CAST(N'2019-04-16T14:37:54.047' AS DateTime), 5, CAST(N'2019-04-16T14:37:54.047' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (19, N'CLB có chút test', N'/Images/Groups/Screenshot (57).png', N'AAA', N'donknow@gmail.com', NULL, 0, 1, CAST(N'2019-04-17T22:06:45.890' AS DateTime), 1, CAST(N'2019-04-17T22:06:45.890' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (20, N'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', N'/Images/Groups/Screenshot (47).png', NULL, N'ơ sai', NULL, 0, 1, CAST(N'2019-04-17T22:47:11.377' AS DateTime), 1, CAST(N'2019-04-17T22:47:11.377' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (21, N'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', NULL, N'aaa', NULL, NULL, 0, 1, CAST(N'2019-04-17T22:47:59.907' AS DateTime), 1, CAST(N'2019-04-17T22:47:59.907' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (22, N'Tooi muon check xem neu dai thi no co xuong dong h', NULL, NULL, NULL, NULL, 0, 1, CAST(N'2019-04-17T22:48:52.470' AS DateTime), 1, CAST(N'2019-04-17T22:48:52.470' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (23, N'Tooi muon check xem neu dai thi no co xuong dong h', N'/Images/Groups/Screenshot (57).png', NULL, N'aaa', NULL, 0, 1, CAST(N'2019-04-17T22:54:19.657' AS DateTime), 1, CAST(N'2019-04-17T22:54:19.657' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (24, N'Tooi muon check xem neu dai thi no co xuong dong h', N'/Images/Groups/New Text Document.txt', NULL, N'aaa', NULL, 0, 1, CAST(N'2019-04-17T22:56:46.470' AS DateTime), 1, CAST(N'2019-04-17T22:56:46.470' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (25, N'có chút clb', N'/Images/Groups/Screenshot (57).png', NULL, NULL, NULL, 0, 1, CAST(N'2019-04-18T00:09:25.720' AS DateTime), 1, CAST(N'2019-04-18T00:09:25.720' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (26, N'có chút clb', NULL, NULL, NULL, NULL, 0, 1, CAST(N'2019-04-18T01:06:35.530' AS DateTime), 1, CAST(N'2019-04-18T01:06:35.530' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (27, N'Yen Luyen', N'/Images/Groups/d4571582-9109-41c0-8fd4-422150226c6a.jpg', NULL, N'dd@gmail.com', NULL, 0, 1, CAST(N'2019-04-20T22:41:23.187' AS DateTime), 1, CAST(N'2019-04-20T22:41:23.187' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (28, N'IC - PDP', N'/Images/Groups/22b3e5f6-4629-470f-add0-0e3d6141f9ad.jpg', N'Cập nhật thông tin, hình ảnh và các câu chuyện về các hoạt động, chương trình dành cho sinh viên ĐH FPT.', N'pdp.hn@fpt.edu.vn', NULL, 0, 5, CAST(N'2019-04-20T22:43:09.423' AS DateTime), 5, CAST(N'2019-04-20T22:43:09.423' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (29, N'Phòng Phát triển Cá nhân (IC - PDP) ', N'/Images/Groups/2c38a460-810a-426a-a347-1cf2be63eb54.jpg', NULL, N'pdp.hn@fpt.edu.vn', NULL, 1, 5, CAST(N'2019-04-21T00:09:30.453' AS DateTime), 53, CAST(N'2019-04-21T00:09:30.453' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (30, N'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', N'/Images/Groups/3445ee1f-5994-4dcb-9cc9-4ae4b405766b.jpg', NULL, N'dd@gmail.com', NULL, 0, 1, CAST(N'2019-04-21T02:16:06.640' AS DateTime), 1, CAST(N'2019-04-21T02:16:06.640' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (31, N'test ', N'/Images/Groups/0af105b4-1efd-48ea-b3d9-13a0742f5838.png', N'test successfully mess', N'dd@gmail.com', NULL, 0, 1, CAST(N'2019-04-21T02:17:02.530' AS DateTime), 1, CAST(N'2019-04-21T02:17:02.530' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (32, N'FPT Vovinam Club - FVC', N'/Images/Groups/a172d3ad-fcbe-4791-843e-7120bc6ba184.jpg', N'Đây là CLB Việt Võ Đạo', N'fptvovinamclub@gmail.com', NULL, 1, 5, CAST(N'2019-04-21T12:01:26.923' AS DateTime), 62, CAST(N'2019-04-21T12:01:26.923' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (33, N'FPTU Business Club', N'/Images/Groups/ea6dfe1a-bc8d-48f6-8f99-6a5cafb84ca8.jpg', NULL, NULL, NULL, 1, 5, CAST(N'2019-04-21T12:04:33.640' AS DateTime), 5, CAST(N'2019-04-21T12:04:33.640' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (34, N'FCC - FPTU Chess Club', N'/Images/Groups/e8473346-516a-4ef5-a688-b4ca5ed5f6b4.jpg', N'Đây là nơi mà các tài năng trẻ Cờ Vua có thể hội ngộ!', N'fuchessclub@gmail.com', NULL, 1, 5, CAST(N'2019-04-21T12:48:24.453' AS DateTime), 5, CAST(N'2019-04-21T12:48:24.453' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (35, N'ss', NULL, NULL, N'aaaaaa@gmail.com', NULL, 0, 1, CAST(N'2019-04-21T13:35:49.827' AS DateTime), 5, CAST(N'2019-04-21T13:35:49.827' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (36, N'aaaaaaaaaaaaaa', NULL, NULL, NULL, NULL, 0, 5, CAST(N'2019-04-22T00:00:30.203' AS DateTime), 50, CAST(N'2019-04-22T00:00:30.203' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (37, N'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', NULL, NULL, NULL, NULL, 0, 1, CAST(N'2019-04-22T21:30:22.593' AS DateTime), 1, CAST(N'2019-04-22T21:30:22.593' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (38, N'FU Debate Club', N'/Images/Groups/5525fd74-4dd4-4411-91da-00d0334c07f8.jpg', NULL, NULL, NULL, 1, 5, CAST(N'2019-04-23T21:25:23.030' AS DateTime), 74, CAST(N'2019-04-23T21:25:23.030' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (39, N'Câu lạc bộ Cóc Đọc và Những người bạn', NULL, NULL, NULL, NULL, 1, 5, CAST(N'2019-04-24T22:14:35.563' AS DateTime), 5, CAST(N'2019-04-24T22:14:35.563' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (40, N'Test group', N'/Images/Groups/d1fba290-ac35-487f-8f98-a40e30460cd9.jpg', N'abc xyz
', N'dd@gmail.com', CAST(N'2019-03-31' AS Date), 0, 1, CAST(N'2019-04-25T01:34:53.733' AS DateTime), 1, CAST(N'2019-04-25T01:34:53.733' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (41, N'Aaa', NULL, NULL, NULL, NULL, 0, 1, CAST(N'2019-02-20T00:41:26.000' AS DateTime), 1, CAST(N'2019-02-20T00:41:26.000' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (42, N'FPTU Guitar Club - FGC', NULL, N'Đây là CLB Guitar.', N'fgc@gmail.com', CAST(N'2001-01-01' AS Date), 1, 5, CAST(N'2019-02-20T02:11:12.923' AS DateTime), 5, CAST(N'2019-02-20T02:11:12.923' AS DateTime))
GO
INSERT [dbo].[Group] ([GroupId], [GroupName], [GroupImage], [GroupDescription], [GroupMail], [FoundedYear], [IsEnabled], [ManagerId], [CreatedDate], [LeaderId], [AssignedDate]) VALUES (43, N'test', NULL, NULL, NULL, CAST(N'2019-04-30' AS Date), 1, 3, CAST(N'2019-04-27T23:06:02.187' AS DateTime), 3, CAST(N'2019-04-27T23:06:02.187' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Group] OFF
GO
SET IDENTITY_INSERT [dbo].[Notification] ON 
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (1, 57, 57, N'Event', N'Organizer Event Feedback', CAST(N'2019-04-29T20:09:58.360' AS DateTime), 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (2, 5, 57, N'Event', N'Organizer Event Register', CAST(N'2019-04-29T20:17:21.673' AS DateTime), 5)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (3, 5, 56, N'Event', N'Organizer Event Register', CAST(N'2019-04-29T20:32:59.343' AS DateTime), 35)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (4, 5, 68, N'Event', N'Organizer Event Report', CAST(N'2019-04-29T20:34:07.970' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (7, 1, 71, N'Event', N'Organizer Event Reject', CAST(N'2019-04-29T21:04:34.187' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (9, 21, 5, N'Group', N'User Group Remove', CAST(N'2019-04-29T21:16:10.077' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (10, 103, NULL, NULL, N'User Role Change', CAST(N'2019-04-29T21:34:29.860' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (11, 104, NULL, NULL, N'User Role Change', CAST(N'2019-04-29T21:34:34.470' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (17, 1, NULL, NULL, N'User Role Change', CAST(N'2019-04-30T02:54:47.687' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (18, 1, NULL, NULL, N'User Role Change', CAST(N'2019-04-30T02:54:51.627' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (5, 5, 68, N'Event', N'Student Event Draft', CAST(N'2019-04-29T20:34:07.970' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (6, 5, 68, N'Event', N'Organizer Event Accept', CAST(N'2019-04-29T20:36:13.140' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (12, 105, NULL, NULL, N'User Role Change', CAST(N'2019-04-29T21:35:06.423' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (13, 105, NULL, NULL, N'User Role Change', CAST(N'2019-04-29T21:35:10.563' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (8, 100, 5, N'Group', N'User Group Add', CAST(N'2019-04-29T21:15:56.827' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (14, 103, NULL, NULL, N'User Role Change', CAST(N'2019-04-29T21:35:15.267' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (15, 104, NULL, NULL, N'User Role Change', CAST(N'2019-04-29T21:35:19.377' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (16, 5, 57, N'Event', N'Organizer Event Register', CAST(N'2019-04-30T00:48:55.750' AS DateTime), 1)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (19, 3, 7, N'Group', N'Organizer Group Leader', CAST(N'2019-05-01T06:14:32.877' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (20, 5, 57, N'Event', N'Organizer Event Report', CAST(N'2019-05-01T06:14:47.423' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (21, 1, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.423' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (22, 2, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.423' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (23, 4, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.423' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (24, 5, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.423' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (25, 26, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.440' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (26, 35, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.440' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (27, 37, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.440' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (28, 71, 57, N'Event', N'Student Event Draft', CAST(N'2019-05-01T06:14:47.440' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (29, 5, 57, N'Event', N'Organizer Event Accept', CAST(N'2019-05-01T06:15:19.547' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (31, 5, 68, N'Event', N'Organizer Event Register', CAST(N'2019-05-01T19:19:14.813' AS DateTime), 35)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (33, 3, 8, N'Group', N'User Group Add', CAST(N'2019-05-01T21:04:55.047' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (34, 3, 8, N'Group', N'Organizer Group Leader', CAST(N'2019-05-01T21:05:07.030' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (35, 5, 68, N'Event', N'Organizer Event Register', CAST(N'2019-05-01T21:08:24.360' AS DateTime), 2)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (36, 4, 8, N'Group', N'Organizer Group Leader', CAST(N'2019-05-01T21:10:44.233' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (38, 68, 68, N'Event', N'Organizer Event Feedback', CAST(N'2019-05-01T22:22:11.140' AS DateTime), 4)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (41, 5, 68, N'Event', N'Organizer Event Register', CAST(N'2019-05-02T20:12:30.970' AS DateTime), 111)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (43, 5, 68, N'Event', N'Organizer Event Register', CAST(N'2019-05-02T20:22:44.827' AS DateTime), 100)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (30, 5, 68, N'Event', N'Organizer Event Register', CAST(N'2019-05-01T16:52:32.970' AS DateTime), 2)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (32, 5, 68, N'Event', N'Organizer Event Accept', CAST(N'2019-05-01T21:02:57.750' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (37, 3, 8, N'Group', N'User Group Remove', CAST(N'2019-05-01T21:11:08.907' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (39, 68, NULL, NULL, N'User Role Change', CAST(N'2019-05-01T22:45:50.843' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (40, 3, 5, N'Group', N'Organizer Group Leader', CAST(N'2019-05-02T07:01:23.343' AS DateTime), NULL)
GO
INSERT [dbo].[Notification] ([NotificationId], [UserId], [ObjectId], [ObjectType], [NotificationContent], [NotifiedDate], [SubjectId]) VALUES (42, 100, 41, N'Group', N'User Group Add', CAST(N'2019-05-02T20:17:54.610' AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[Notification] OFF
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (1, 38, CAST(N'2019-04-20T21:37:21.453' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (1, 39, CAST(N'2019-04-21T20:08:07.203' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (1, 56, CAST(N'2019-04-17T17:12:52.830' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (1, 57, CAST(N'2019-04-30T00:48:55.733' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 4, CAST(N'2019-01-12T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 5, CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 6, CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 7, CAST(N'2019-03-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 8, CAST(N'2019-03-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 9, CAST(N'2019-01-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 10, CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 11, CAST(N'2019-03-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 12, CAST(N'2019-03-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 13, CAST(N'2019-03-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 39, CAST(N'2019-04-21T20:08:33.157' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 56, CAST(N'2019-04-24T21:24:58.750' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 57, CAST(N'2019-04-20T16:21:16.377' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (2, 70, CAST(N'2019-04-24T21:57:01.907' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (3, 1, CAST(N'2018-11-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (3, 2, CAST(N'2018-11-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (3, 38, CAST(N'2019-04-16T16:21:43.077' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (3, 39, CAST(N'2019-04-21T20:07:15.797' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (3, 63, CAST(N'2019-04-20T15:05:29.280' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 3, CAST(N'2018-05-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 4, CAST(N'2019-01-12T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 5, CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 6, CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 7, CAST(N'2019-03-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 8, CAST(N'2019-03-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 9, CAST(N'2019-01-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 10, CAST(N'2018-10-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 11, CAST(N'2019-03-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 12, CAST(N'2019-03-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 13, CAST(N'2019-03-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 39, CAST(N'2019-04-21T20:07:42.343' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (4, 57, CAST(N'2019-04-20T15:10:24.577' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 1, CAST(N'2018-11-22T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 2, CAST(N'2018-11-22T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 3, CAST(N'2018-05-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 16, CAST(N'2019-04-24T03:41:30.733' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 38, CAST(N'2019-04-20T17:39:28.890' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 39, CAST(N'2019-04-21T20:05:46.673' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 41, CAST(N'2019-04-20T18:34:32.110' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 48, CAST(N'2019-04-15T23:16:40.593' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 57, CAST(N'2019-04-29T20:17:21.640' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 68, CAST(N'2019-04-22T05:37:19.047' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (5, 69, CAST(N'2019-04-25T01:37:08.780' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (23, 38, CAST(N'2019-04-16T16:28:13.267' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (24, 38, CAST(N'2019-04-16T18:13:31.737' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (25, 38, CAST(N'2019-04-16T20:01:15.360' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (26, 38, CAST(N'2019-04-20T17:26:55.157' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (26, 57, CAST(N'2019-04-20T16:03:24.983' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (28, 38, CAST(N'2019-04-16T21:05:21.157' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (29, 38, CAST(N'2019-04-20T17:48:15.280' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (30, 38, CAST(N'2019-04-17T10:12:40.517' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (31, 38, CAST(N'2019-04-20T17:56:39.923' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (31, 39, CAST(N'2019-04-21T20:09:26.970' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (32, 38, CAST(N'2019-04-20T17:55:33.407' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (32, 39, CAST(N'2019-04-21T20:08:49.827' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (35, 41, CAST(N'2019-04-23T22:35:17.483' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (35, 56, CAST(N'2019-04-29T20:32:59.343' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (35, 57, CAST(N'2019-04-20T15:08:49.297' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (35, 68, CAST(N'2019-05-01T19:19:14.780' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (36, 38, CAST(N'2019-04-20T17:40:26.860' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (37, 38, CAST(N'2019-04-20T17:39:53.437' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (37, 57, CAST(N'2019-04-20T15:56:37.093' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (39, 38, CAST(N'2019-04-20T17:28:31.673' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (40, 38, CAST(N'2019-04-20T17:41:32.063' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (42, 38, CAST(N'2019-04-20T17:50:09.077' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (43, 38, CAST(N'2019-04-20T17:51:50.970' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (44, 38, CAST(N'2019-04-20T17:53:07.767' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (45, 38, CAST(N'2019-04-20T17:59:24.140' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (46, 38, CAST(N'2019-04-20T18:04:19.470' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (47, 38, CAST(N'2019-04-20T18:06:49.890' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (48, 38, CAST(N'2019-04-20T18:11:27.797' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (49, 38, CAST(N'2019-04-20T18:12:58.577' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (57, 38, CAST(N'2019-04-21T01:06:32.047' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (58, 38, CAST(N'2019-04-21T11:47:11.937' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (59, 38, CAST(N'2019-04-21T11:48:37.407' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (60, 38, CAST(N'2019-04-21T11:49:22.530' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (61, 38, CAST(N'2019-04-21T11:50:28.157' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (71, 57, CAST(N'2019-04-22T00:12:17.687' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (93, 69, CAST(N'2019-04-25T22:36:26.907' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (100, 68, CAST(N'2019-05-02T20:22:44.813' AS DateTime))
GO
INSERT [dbo].[Register] ([UserId], [EventId], [RegisteredDate]) VALUES (101, 38, CAST(N'2019-02-20T02:14:38.483' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Report] ON 
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (1, 5, 41, CAST(N'2019-04-16T04:04:24.187' AS DateTime), N'', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (2, 5, 41, CAST(N'2019-04-16T04:04:29.077' AS DateTime), N'2', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (3, 5, 41, CAST(N'2019-04-16T04:04:53.970' AS DateTime), N'23', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (4, 2, 9, CAST(N'2019-04-16T16:03:15.313' AS DateTime), N'su kien lua dao', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (5, 5, 3, CAST(N'2019-04-16T19:54:13.063' AS DateTime), N'Sự kiện nhiều tiền, report!', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (6, 5, 30, CAST(N'2019-04-16T19:55:20.483' AS DateTime), N'Report vì không được đi', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (7, 5, 39, CAST(N'2019-04-16T19:55:52.483' AS DateTime), N'Mở đơn quá gấp!', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (8, 5, 59, CAST(N'2019-04-17T23:55:21.720' AS DateTime), N'a', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (9, 1, 58, CAST(N'2019-04-17T23:55:32.907' AS DateTime), N'test', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (10, 1, 58, CAST(N'2019-04-17T23:56:18.127' AS DateTime), N'', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (11, 1, 58, CAST(N'2019-04-18T00:00:12.000' AS DateTime), N'', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (12, 3, 51, CAST(N'2019-04-18T00:21:59.360' AS DateTime), N'Test thử ', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (13, 1, 51, CAST(N'2019-04-18T00:22:03.437' AS DateTime), N'đắt quá', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (14, 3, 30, CAST(N'2019-04-18T00:22:38.233' AS DateTime), N'Như dở hơi
', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (15, 1, 58, CAST(N'2019-04-18T00:23:36.780' AS DateTime), N'không đủ kinh phí', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (16, 5, 54, CAST(N'2019-04-18T00:41:07.987' AS DateTime), N'Sự kiện này không hiện lên ở lịch university', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (17, 5, 48, CAST(N'2019-04-18T10:47:57.923' AS DateTime), N'', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (18, 5, 45, CAST(N'2019-04-20T15:54:45.407' AS DateTime), N'Thông tin đưa ra gấp và thời gian học không trùng với kì học trên trường khiến khá là vật vã trong việc xếp lịch ạ', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (19, 5, 38, CAST(N'2019-04-20T15:55:11.877' AS DateTime), N'Lúc đang đi trên phố đi bộ ở Sing thì bị trộm mất điện thoại, thế mà không ai giúp', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (20, 37, 63, CAST(N'2019-04-20T15:55:55.267' AS DateTime), N'Đây là sự kiện không có thật!', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (21, 37, 56, CAST(N'2019-04-20T15:56:20.377' AS DateTime), N'Sự kiện tổ chức ngay dịp nghỉ lễ mà về quê mất rồi!', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (22, 5, 51, CAST(N'2019-04-20T16:02:39.483' AS DateTime), N'Đã đăng kí lâu thế mà không thấy mail thông báo ở đâu là sao?', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (23, 5, 56, CAST(N'2019-04-20T17:05:24.647' AS DateTime), N'd', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (24, 39, 38, CAST(N'2019-04-20T17:28:53.500' AS DateTime), N'Không regis lại được form', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (25, 61, 41, CAST(N'2019-04-21T11:53:00.203' AS DateTime), N'Sai người tổ chức', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (26, 1, 41, CAST(N'2019-04-21T18:48:33.470' AS DateTime), N'Thái có gì ấy sợ lắm mà quên mất tên', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (27, 5, 56, CAST(N'2019-04-22T02:31:07.577' AS DateTime), N'Test block', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (28, 5, 6, CAST(N'2019-04-22T05:38:42.470' AS DateTime), N'test target group', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (29, 3, 56, CAST(N'2019-04-22T07:35:55.173' AS DateTime), N'', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (30, 3, 56, CAST(N'2019-04-22T07:35:57.627' AS DateTime), N'', 1)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (31, 76, 69, CAST(N'2019-04-23T21:48:27.407' AS DateTime), N'Không cho register thì report luôn', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (32, 1, 73, CAST(N'2019-04-25T02:21:47.000' AS DateTime), N'aaaaaaaaaaaaa', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (33, 1, 73, CAST(N'2019-04-25T02:22:02.877' AS DateTime), N'Ơ sự kiện còn chưa được duyệt mà đã report được', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (34, 35, 56, CAST(N'2019-02-20T00:32:27.093' AS DateTime), N'cái này có public hok', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (35, 35, 56, CAST(N'2019-02-20T00:32:34.750' AS DateTime), N'cái này có public hok?', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (36, 35, 56, CAST(N'2019-02-20T00:32:42.017' AS DateTime), N'cái này có public hok?
ơ ko xóa à', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (37, 35, 56, CAST(N'2019-02-20T00:32:54.390' AS DateTime), N'cái này có public hok?
ơ ko xóa à
ô có nhận được nhiều lần ko?', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (38, 35, 56, CAST(N'2019-02-20T00:35:41.063' AS DateTime), N'Ơ sao ko đký được???', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (39, 5, 56, CAST(N'2019-02-20T00:41:03.703' AS DateTime), N'Chỉnh lại các icon', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (40, 5, 51, CAST(N'2019-02-20T00:41:19.313' AS DateTime), N'Chỉnh lại các icon', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (41, 5, 45, CAST(N'2019-02-20T00:41:29.953' AS DateTime), N'Chỉnh icon', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (42, 5, 40, CAST(N'2019-02-20T00:41:38.953' AS DateTime), N'Chỉnh icon', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (43, 1, 50, CAST(N'2019-02-20T01:23:56.250' AS DateTime), N'mấy ảnh tiếng anh đọc chả hiểu gì', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (44, 1, 38, CAST(N'2019-04-28T12:40:40.333' AS DateTime), N'Hic hic', 0)
GO
INSERT [dbo].[Report] ([Id], [UserId], [EventId], [CreatedDate], [Description], [IsDismissed]) VALUES (45, 35, 69, CAST(N'2019-04-29T20:30:04.703' AS DateTime), N'Hay thế', 0)
GO
SET IDENTITY_INSERT [dbo].[Report] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (1, 1, N'yenltse04823@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-05-01T08:51:52.627' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (2, 2, N'thanglvse04854@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-05-02T20:37:23.780' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (3, 3, N'phuongnmse05113@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-05-02T01:04:49.157' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (4, 4, N'huongdlse05123@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-04-28T10:27:06.137' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (5, 5, N'duyenntmse05164@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-05-02T01:57:50.640' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (8, 10, N'baopnse04414@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (21, 9, N'tiennhse04976@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-04-29T15:28:44.297' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (22, NULL, N'yenlt@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (23, 11, N'hiephvse04523@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (24, 12, N'tuanmase04862@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (25, 13, N'minhndse04560@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (26, 14, N'uyenhtsb01954@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (27, 15, N'datndsb01880@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (28, 16, N'duynbse04748@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (29, 17, N'phanlhse04195@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (30, 18, N'hunghxse04721@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-02-20T01:20:09.077' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (31, 19, N'thangnmse04322@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (32, 20, N'thanglkdse04320@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (33, NULL, N'phuongntmse05113@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (34, NULL, N'aaaaaaaaaaaaaaaaaaa@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (35, 21, N'nambcse04832@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-05-01T19:19:19.267' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (36, 22, N'huytqse04731@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (37, 23, N'datncse04714@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (38, 24, N'anhnmse04500@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (39, 25, N'lampdse04797@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (40, 26, N'bachvhse04778@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (41, 89, N'yenltse04832@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-30T02:55:09.267' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (42, 27, N'namdtse05875@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (43, 28, N'huyvqse04608@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (44, 29, N'quanpmse04686@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (45, 30, N'uyenlpse04634@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (46, 31, N'yennthse04366@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (47, 32, N'dungdmse05228@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (48, 33, N'anhnthe141442@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (49, 34, N'tuannase62864@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (50, 37, N'hapth@fpt.edu.vn', 0, 1, 1, 1, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (51, 35, N'lampt@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (52, 36, N'hapth@fpt.edu.v', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (53, 50, N'thanhnh3@fpt.edu.vn', 0, 1, 1, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (54, 48, N'khiemhnb@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (55, 52, N'anhtp@fpt.edu.vn', 1, 1, 1, 1, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (56, NULL, N'test@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (57, 38, N'nhatvmse04828@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T02:00:48.113' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (58, 39, N'khanhnxse04280@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T11:46:58.233' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (59, 40, N'huynlqse04182@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T11:48:22.780' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (60, 41, N'bachnvse04221@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T11:49:08.530' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (61, 42, N'congndse04261@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T11:50:13.470' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (62, 51, N'sontvse04903@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T12:02:13.843' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (63, 43, N'anhgqse04763@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T21:54:11.843' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (64, 44, N'Anhpnse04804@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-29T14:07:43.170' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (65, 45, N'dunglvse04734@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T21:57:13.673' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (66, 46, N'canhkdse04533@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T21:58:09.813' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (67, 47, N'quanghdse03459@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T22:01:15.343' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (68, 91, N'quantmse04590@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-05-01T23:54:55.673' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (69, 49, N'thanhhn3@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T23:11:39.407' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (70, 53, N' hapth@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-21T23:20:49.077' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (71, 54, N'haltthe131024@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-02-20T00:26:50.813' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (72, 55, N'oanhptse04853@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-04-22T06:02:35.983' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (73, 56, N'duybkse04836@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-04-25T01:37:24.267' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (74, 58, N'nghiatdse04563@fpt.edu.vn', 0, 1, 0, 0, 1, CAST(N'2019-04-23T21:46:17.250' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (75, NULL, N' nghiatdse04563@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-23T21:46:35.547' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (76, 57, N'vinhtqse04881@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-23T21:47:29.733' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (77, NULL, N'duchtse61924@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-24T01:00:38.890' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (78, 59, N'hungpvsb01857@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-04-24T04:01:14.173' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (79, 60, N'kienntse04792@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-24T17:07:08.110' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (80, 61, N'ducttse04759@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-24T17:18:19.453' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (81, 62, N'datbqse04576@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-24T17:21:23.813' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (82, 79, N'thuyntxhs130223@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T21:27:34.017' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (83, 77, N'thuyntha130070@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:23:01.220' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (84, 63, N'anhtlmsb01816@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:25:39.720' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (85, 64, N'thuytt@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:26:38.377' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (86, 65, N'halthhs140308@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:27:40.627' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (87, 66, N'tungnvhe130151@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:30:37.923' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (88, 67, N'nhihvhs140009@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:31:28.877' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (89, 68, N'minhnnha130039@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:32:24.360' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (90, 69, N'trangntkhs140289@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:32:31.720' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (91, 70, N'abc@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:34:05.530' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (92, 71, N'linhntths140227@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:35:00.547' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (93, 72, N'thuynths130143@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:36:01.907' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (94, 73, N'thuyntha140117@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:37:19.877' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (95, 78, N'chithhs140240@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:39:25.593' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (96, 74, N'hoangnvse04703@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:40:37.047' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (97, 90, N'lannhhs140680@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:42:20.750' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (98, NULL, N'hoangbvhs140682@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T22:44:28.517' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (99, 75, N'phuongnhse04218@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-25T23:29:44.813' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (100, 76, N'trinhltsb01828@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-05-02T20:20:41.517' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (101, 80, N'linhnttsb02066@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-02-20T02:14:58.453' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (102, NULL, N' linhnttsb02066@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-02-20T02:13:51.437' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (103, 81, N'anhbn@fpt.edu.vn', 1, 1, 1, 0, 1, CAST(N'2019-04-29T21:10:06.970' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (104, 83, N'duongtb@fpt.edu.vn', 1, 1, 1, 0, 1, CAST(N'2019-04-27T01:50:23.720' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (105, 84, N'sangnv@fpt.edu.vn', 1, 1, 1, 0, 1, CAST(N'2019-04-27T01:52:44.767' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (106, NULL, N'sangnv@fpt.edu.vn	', 1, 0, 0, 0, 1, CAST(N'2019-04-27T01:53:22.327' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (107, NULL, N'chienbd@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-27T01:56:12.047' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (108, 85, N'kienlt@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-27T02:01:22.970' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (109, 82, N'hieuld@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-27T10:18:35.157' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (110, 86, N'CuongN@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-27T10:28:21.767' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (111, 87, N'datbqhs140147@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-04-27T10:31:56.250' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (112, 88, N'thanglv9@fpt.edu.vn', 1, 1, 0, 0, 1, CAST(N'2019-04-28T02:00:53.063' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (113, 92, N'travmhse05081@fpt.edu.vn', 1, 0, 0, 0, 1, CAST(N'2019-05-02T01:57:34.517' AS DateTime))
GO
INSERT [dbo].[User] ([UserId], [ProfileId], [Email], [IsStudent], [IsOrganizer], [IsManager], [IsAdmin], [IsEnabled], [NotificationSeenDate]) VALUES (114, 1, N'datvt@fpt.edu.vn', 0, 0, 0, 1, 1, CAST(N'2019-05-02T01:57:34.517' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (4, 2, CAST(N'2019-04-24T03:51:08.313' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 2, CAST(N'2019-04-18T00:42:42.813' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (21, 2, CAST(N'2019-04-24T03:53:18.187' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (30, 2, CAST(N'2019-04-21T12:39:19.657' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (35, 2, CAST(N'2019-04-24T03:50:39.000' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (40, 2, CAST(N'2019-04-25T02:08:30.907' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (55, 2, CAST(N'2019-04-21T23:51:46.843' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (72, 2, CAST(N'2019-04-24T03:51:01.297' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (73, 2, CAST(N'2019-04-24T03:53:25.203' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (82, 2, CAST(N'2019-04-25T23:18:48.483' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 4, CAST(N'2019-04-18T00:43:18.640' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (50, 4, CAST(N'2019-04-20T18:25:10.970' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 5, CAST(N'2019-04-24T03:54:39.687' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (2, 5, CAST(N'2019-04-24T03:54:43.343' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 5, CAST(N'2019-04-25T02:10:15.030' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (4, 5, CAST(N'2019-04-28T01:10:04.207' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 5, CAST(N'2019-04-18T00:43:29.907' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (30, 5, CAST(N'2019-04-24T03:54:52.923' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (50, 5, CAST(N'2019-04-24T03:54:56.423' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (51, 5, CAST(N'2019-04-24T03:55:00.093' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (53, 5, CAST(N'2019-04-25T02:10:36.733' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (55, 5, CAST(N'2019-04-24T03:55:07.627' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (73, 5, CAST(N'2019-04-24T03:55:11.343' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (74, 5, CAST(N'2019-04-24T03:55:14.843' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (100, 5, CAST(N'2019-04-29T21:15:56.827' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 6, CAST(N'2019-04-19T17:59:29.797' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 6, CAST(N'2019-04-19T17:59:20.970' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 6, CAST(N'2019-04-18T00:05:03.987' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (53, 6, CAST(N'2019-04-21T23:53:00.187' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (55, 6, CAST(N'2019-04-21T11:05:37.407' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 7, CAST(N'2019-04-24T03:56:21.907' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (2, 7, CAST(N'2019-04-24T03:56:28.250' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 7, CAST(N'2019-04-24T03:56:25.063' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 7, CAST(N'2019-04-18T00:42:10.140' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (21, 7, CAST(N'2019-04-24T03:56:34.563' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (30, 7, CAST(N'2019-04-24T03:56:31.407' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (35, 7, CAST(N'2019-04-24T03:56:37.750' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (50, 7, CAST(N'2019-04-24T03:56:11.233' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (51, 7, CAST(N'2019-04-24T03:56:40.843' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (53, 7, CAST(N'2019-04-24T03:56:51.640' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (55, 7, CAST(N'2019-04-24T03:56:48.360' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (72, 7, CAST(N'2019-04-24T03:56:03.673' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (73, 7, CAST(N'2019-04-24T03:56:55.547' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (74, 7, CAST(N'2019-04-24T03:56:45.110' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (78, 7, CAST(N'2019-04-24T04:01:46.250' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (4, 8, CAST(N'2019-04-22T03:39:48.720' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 8, CAST(N'2019-04-18T00:42:24.877' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (72, 8, CAST(N'2019-04-22T06:03:06.233' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 10, CAST(N'2019-04-15T01:12:42.720' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (4, 10, CAST(N'2019-04-11T21:35:37.390' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 10, CAST(N'2019-04-15T22:47:18.983' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 14, CAST(N'2019-04-18T00:04:05.610' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (54, 18, CAST(N'2019-04-20T18:56:15.733' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 25, CAST(N'2019-04-18T00:09:25.720' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 26, CAST(N'2019-04-18T01:06:35.530' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 27, CAST(N'2019-04-20T22:41:23.203' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 28, CAST(N'2019-04-20T22:43:09.437' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 29, CAST(N'2019-04-21T00:09:30.483' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (53, 29, CAST(N'2019-04-21T11:56:38.657' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (54, 29, CAST(N'2019-04-21T11:56:24.297' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (55, 29, CAST(N'2019-04-21T23:24:54.017' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 30, CAST(N'2019-04-21T02:16:06.657' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 31, CAST(N'2019-04-21T02:17:02.547' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 32, CAST(N'2019-04-21T12:01:26.937' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (35, 32, CAST(N'2019-04-23T22:28:39.030' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (62, 32, CAST(N'2019-04-21T12:02:38.390' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 33, CAST(N'2019-04-21T12:04:33.657' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 34, CAST(N'2019-04-21T12:48:24.470' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (40, 34, CAST(N'2019-04-25T01:52:23.267' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (50, 34, CAST(N'2019-04-25T01:52:33.767' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (74, 34, CAST(N'2019-04-25T01:52:31.127' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 35, CAST(N'2019-04-21T13:35:49.860' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (50, 36, CAST(N'2019-04-22T00:00:30.220' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 37, CAST(N'2019-04-22T21:30:22.877' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (73, 38, CAST(N'2019-04-23T21:25:23.047' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (74, 38, CAST(N'2019-04-23T21:51:08.220' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 39, CAST(N'2019-04-24T22:14:35.593' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 40, CAST(N'2019-04-25T01:34:53.780' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 41, CAST(N'2019-02-20T00:41:26.017' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (100, 41, CAST(N'2019-05-02T20:17:54.593' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (5, 42, CAST(N'2019-02-20T02:11:12.937' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (1, 43, CAST(N'2019-04-27T23:06:16.827' AS DateTime))
GO
INSERT [dbo].[UserGroup] ([OrganizerId], [GroupId], [ParticipatedDate]) VALUES (3, 43, CAST(N'2019-04-27T23:06:02.267' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[UserProfile] ON 
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (1, N'SE04823', N'Yên', N'Luyện', CAST(N'1997-07-20' AS Date), 0, N'0388696963', N'Nghệ AN', N'Xóm Xuân Lai, Đô Thành, Yên Thành, Nghệ AN', N'/Images/ProfileImage/ccf22963-afaa-43ba-9c54-e8abb863f690.jpg', N'FU-HL', N'BSE', N'JS', N'Luyện Ngọc Kiểu', N'01698032152')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (2, N'SE04854', N'Thắng', N'Lê Việt', CAST(N'1997-01-13' AS Date), 1, N'0332554026', N'Hà Nội', N'65 Ngõ 58 Đào Tấn, Cống Vị, Ba Đình, Hà Nội', N'/Images/ProfileImage/5bc73e82-2ebb-4dc0-97ad-956deb9da149.jpg', N'FU-HL', N'BSE', N'JS', N'Lê Việt Cường', N'01677652616')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (3, N'SE05113', N'Momo', N'Beria', CAST(N'1997-08-31' AS Date), 1, N'0113114115', N'Hà Nội', N'Tổ dân phố số 1, thị trấn Đông Anh, huyện Đông Anh, Hà Nội', N'/Images/ProfileImage/c6bb9c2b-0033-4d1f-b3ef-3b68840b9591.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Tiến Cảnh', N'0979417153-0904792683')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (4, N'SE05123', N'Hương', N'Đinh Lan', CAST(N'1997-01-17' AS Date), 0, N'0962925214', N'Hà Nội', N'203 xóm Lễ Nghĩa, Cự Đà, Cự Khê, Thanh Oai, Hà Nội', N'/Images/ProfileImage/be1f2bdc-f8cb-461b-9260-d08f37599de2.jpg', N'FU-HL', N'BSE', N'JS', N'Đinh Thành Sơn', N'0906234394-0982396394')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (5, N'SE05164', N'Duyên', N'Trừng', CAST(N'1997-02-20' AS Date), 0, N'0835713075', N'tỉnh Thừa Thiên Huế', N'68 Nguyễn Phúc Nguyên, Tp Huế, tỉnh Thừa Thiên Huế', N'/Images/ProfileImage/53b8a5e5-c404-432d-b32c-792a10535240.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Thị Ngọc Lan', N'0906523717')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (9, N'SE04976', N'Tiến', N'Nguyễn Hùng', CAST(N'1997-01-10' AS Date), 1, N'0344216717', N'Hà Nội', N'Số 63 ngách 33 văn chương 2 Đống Đa Hà Nội', N'/Images/ProfileImage/9f619adf-b3ae-497e-a2e6-edc72a77d453.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Hùng Khanh', N'0904121911-0979695554')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (10, N'SE04414', N'Bảo', N'Phan Nguyên', CAST(N'1996-10-07' AS Date), 1, N'0349689410', N'114 Bà Huyện Thanh Quan, quận Ngũ Hành Sơn, Tp Đà Nẵng', N'114 Bà Huyện Thanh Quan, quận Ngũ Hành Sơn, Tp Đà Nẵng', N'/Images/ProfileImage/be54253a-f161-4229-8bd9-63209e1cc7ec.png', N'Hòa Lạc', N'BSE', N'JS', N'Phan Thanh Sanh', N'0905133119')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (11, N'SE04523', N'Hiệp', N'Hoàng Văn', CAST(N'1997-01-14' AS Date), 1, N'0828266561', N'Số 416 phố Phúc Trì, phường Nam Thành, TP Ninh Bình', N'Số 416 phố Phúc Trì, phường Nam Thành, TP Ninh Bình', N'/Images/ProfileImage/ee0c94ba-3699-4e9b-8add-a1df3b221950.jpg', N'Hòa Lạc', N'BSE', N'JS', N'Hoàng Văn Hùng', N'0914806561-0944499171')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (12, N'SE04862', N'Tuấn', N'Mai Anh', CAST(N'1997-06-05' AS Date), 1, N'0966263305', N'Thôn Đoàn Kết, xã Cổ Đông, Sơn Tây, Hà Nội', N'Thôn Đoàn Kết, xã Cổ Đông, Sơn Tây, Hà Nội', N'/Images/ProfileImage/fa66f969-f380-41a6-8e73-83e4f85e390c.jpg', N'Hòa Lạc', N'BSE', N'JS', N'Mai Đức Khoa', N'0987920288')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (13, N'SE04560', N'Minh', N'Nguyễn Đức', CAST(N'1997-10-06' AS Date), 1, N'0977509797', N'4TK - 210 - Chi Lăng - Lạng Sơn', N'4TK - 210 - Chi Lăng - Lạng Sơn', N'/Images/ProfileImage/7df2a3c3-dc32-4939-93fc-45fef62d8f77.jpg', N'Hòa Lạc', N'BSE', N'IS', N'Nguyễn Văn Tùng', N'01659286888; 0948918998-')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (14, N'SB01954', N'Uyên', N'Hồ Thảo', CAST(N'1997-10-22' AS Date), 0, N'0934938615', N'Đà Nẵng', N'237 Hàn Thuyên, P. Hòa Cường Bắc, Q. Hải Châu, Tp Đà Nẵng', N'/Images/ProfileImage/78cb7380-5ac7-4d0a-a388-75a3ccb2fd99.jpg', N'FU-HL', N'JPL', N'JPL', N'Hồ Thảo Uyên', N'0934938615')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (15, N'SB01880', N'Đạt', N'Nguyễn Đình', CAST(N'1997-08-03' AS Date), 1, N'', N'Số 5 ngõ 81/35 Cống Vị, Ba Đình, Hà Nội', N'Số 5 ngõ 81/35 Cống Vị, Ba Đình, Hà Nội', N'/Images/ProfileImage/1fad858e-7fa9-4d56-a21f-2669636cd5bf.jpg', N'Hòa Lạc', N'JPL', N'JPL', N'Nguyễn Đình Thuận', N'0982686467-0934370489')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (16, N'SE04748', N'Duy', N'Nguyễn Bá', CAST(N'1997-01-29' AS Date), 1, N'0948932829', N'Cụm 2 Điềm Niêm, xã Tân Hưng, huyện Vĩnh Bảo, Tp. Hải Dương', N'Cụm 2 Điềm Niêm, xã Tân Hưng, huyện Vĩnh Bảo, Tp. Hải Dương', N'/Images/ProfileImage/563de277-9674-4b26-bb3f-ee49a7611c0f.jpg', N'Hòa Lạc', N'BSE', N'JS', N'Nguyễn Bá Duy', N'0914575509-0904223869')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (17, N'SE04195', N'Phan', N'Lê Hà', CAST(N'1996-07-02' AS Date), 1, N'0819020796', N'Quảng Trị', N'Khu phố 10, P.5, đường Lý Thường Kiệt, Tp Đông Hà, tỉnh Quảng Trị', N'/Images/ProfileImage/0f60f10b-0884-40e4-9ee8-1c3289ea4c3c.jpg', N'FU-HL', N'BSE', N'IS', N'Nguyễn Thị Hà', N'0915112796')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (18, N'SE04721', N'Hưng', N'Hồ Xuân', CAST(N'1997-02-01' AS Date), 1, N'0981344650', N'Hà Nội', N'Phòng 705, Nhà 24T2, KĐT Trung Hòa, Nhân Chính, Cầu Giấy, Hà Nội', N'/Images/ProfileImage/57b3af96-678d-4595-a127-7d226df4af2b.jpg', N'FU-HL', N'BSE', N'IS', N'Trần Thị Minh', N'0916635849')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (19, N'SE04322', N'Thắng', N'Nguyễn Minh', CAST(N'1996-03-22' AS Date), 1, N'0909941996', N'Hà Đông', N'Số 133 tổ 4 và 5 dãy D khu giãn dân Đa Sỹ, phường Kiến Hưng, Hà Đông', N'/Images/ProfileImage/16475364-1efc-4ede-88be-0e6e238ca159.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Cung Sang', N'0902463469')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (20, N'SE04320', N'Thắng', N'Lê Khắc Đức', CAST(N'1996-07-22' AS Date), 1, N'0396121612', N'Hà Nam', N'Xóm 7, Đại Cương, Kim Bảng, Hà Nam', N'/Images/ProfileImage/b19c4339-ab16-4615-8427-2bad335e3eea.jpg', N'FU-HL', N'BSE', N'JS', N'Lê Khắc Dũng', N'0979402904')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (21, N'SE04832', N'Nam', N'Bùi Công', CAST(N'1997-07-14' AS Date), 1, N'09010101010', N'Thái Nguyên', N'Số nhà 25 tổ 1 phường Túc Duyên, tp Thái Nguyên, Thái Nguyên', N'/Images/ProfileImage/70e16b7e-8d50-485c-8130-21895c9eb209.jpg', N'FU-HL', N'BSE', N'IS', N'Bùi Công Bắc', N'01683326016-01657101269')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (22, N'SE04731', N'Huy', N'Trần Quang', CAST(N'1997-12-05' AS Date), 1, N'0944096033', N'tỉnh Thái Nguyên', N'Trường Cao đẳng Sư phạm Thái Nguyên, phường Thịnh Đán, Tp. Thái Nguyên, tỉnh Thái Nguyên', N'/Images/ProfileImage/fa3d146f-d6ac-48e8-b51d-5fa14a09b84f.jpg', N'FU-HL', N'BSE', N'IS', N'Lê Thị Hồng Minh', N'0914656225-01687649300')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (23, N'SE04714', N'Đạt', N'Nguyễn Công', CAST(N'1997-05-27' AS Date), 1, N'0945037474', N'tỉnh Tuyên Quang', N'thôn Hồ Tiêu, thị trấn Sơn Dương, huyện Sơn Dương, tỉnh Tuyên Quang', N'/Images/ProfileImage/122962dc-27c1-4c97-9539-3885ef08adeb.jpg', N'FU-HL', N'BSE', N'IS', N'Nguyễn Văn Thắng', N'0945037474-0904502937')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (24, N'SE04500', N'Anh', N'Nguyễn Minh', CAST(N'1996-09-18' AS Date), 1, N'0963392888', N'tỉnh Lai Châu', N'số nhà 180 đường Nguyễn Trãi, tổ 9, phường Quyết Thắng, thành phố Lai Châu, tỉnh Lai Châu', N'/Images/ProfileImage/ebcb212f-cf2f-4914-98d9-ca7456f2bfe6.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Văn Chiến', N'0988256666-0968568888')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (25, N'SE04797', N'Lâm', N'Phan Đăng', CAST(N'1997-05-10' AS Date), 1, N'0836631188', N'Hà Nội', N'Số 14 ngõ 189/15/16 An Dương. Tây Hồ. Hà Nội', N'/Images/ProfileImage/a9f7a2f4-45b3-4d44-b1e8-935d63b64185.jpg', N'FU-HL', N'BSE', N'IS', N'Nguyễn Thị Tuyết Mai', N'0913212774-0912226366')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (26, N'SE04778', N'Bách', N'Vũ Hồ', CAST(N'1995-03-24' AS Date), 1, N'0965362504', N'Hà Nội', N'Nhà B17 ngõ 109 Trường Chinh, phường Phương Liêt, quận Thanh Xuân, Hà Nội', N'/Images/ProfileImage/493ce8e5-9988-47d2-bcbf-5a348cdcff1f.jpg', N'FU-HL', N'BIA', N'IA', N'Vũ Hải Nam', N'0913534223')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (27, N'SE05875', N'Nam', N'Đặng Tuấn', CAST(N'1998-03-30' AS Date), 1, N'0329115656', N'Yên Bái', N'Km12, chợ Yên Bình, tổ 11, Thị trấn Yên Bình, Huyện Yên Bình, Yên Bái', N'/Images/ProfileImage/00eff58c-3518-4013-aa92-117be2ce71d5.jpg', N'FU-HL', N'BSE', N'SE', N'Đặng Tuấn Nam', N'01663176176-01673335912')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (28, N'SE04608', N'Huy', N'Vũ Quang', CAST(N'1994-05-10' AS Date), 1, N'0904607052', N'Hà Nội', N'36 Hàm Long, Hàng Bài, Hoàn Kiếm, Hà Nội', N'/Images/ProfileImage/d6bcc313-1ba6-4934-9440-3f5904f64924.jpg', N'FU-HL', N'BSE', N'IS', N'Vũ Quang Vinh', N'0936331519')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (29, N'SE04686', N'Quân', N'Phùng Minh', CAST(N'1997-08-27' AS Date), 1, N'0383888298', N'Hà Nội', N'Số B5 ngõ 2 phố Linh Lang, Cống Vị, Ba Đình, Hà Nội', N'/Images/ProfileImage/9a8f9ca3-afeb-45ee-9a74-f300615d2b8e.jpg', N'FU-HL', N'BSE', N'IS', N'Nguyễn Thị Hà', N'0904469398')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (30, N'SE04634', N'Uyên', N'Lê Phương', CAST(N'1997-07-11' AS Date), 0, N'0976059264', N'Hà nội', N'Số 13 ngõ 143 phố Kim Mã,Ba Đình,Hà nội', N'/Images/ProfileImage/00087087-4934-4f99-8f6d-09a57f8a6ba9.jpg', N'FU-HL', N'BSE', N'JS', N'Trần Thu Phương', N'0983042872-0942394222')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (31, N'SE04366', N'Yến', N'Nguyễn Thị Hải', CAST(N'1995-09-28' AS Date), 0, N'0383363338', N'tỉnh Ninh Bình', N'phố Mía, phường Ninh Khánh, Tp. Ninh Bình, tỉnh Ninh Bình', N'/Images/ProfileImage/9abf7ad0-8b98-4cf3-9d10-348068ab9383.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Trọng Hưng', N'0904219751-256402869')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (32, N'SE05228', N'Dũng', N'Đỗ Mạnh', CAST(N'1998-04-07' AS Date), 1, N'0981065638', N'tỉnh Hải Dương', N'số 255, đường Yết Kiêu, thị trấn Gia Lộc, huyện Gia Lộc, tỉnh Hải Dương', N'/Images/ProfileImage/71f03114-589b-46a8-bad0-f6c85b1c87ec.jpg', N'FU-HL', N'BSE', N'SE', N'Đỗ Mạnh Dũng', N'0977685868')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (33, N'HE141442', N'Anh', N'Ngô  Tiến ', CAST(N'2000-04-06' AS Date), 1, N'', N'', N'', N'/Images/ProfileImage/1a65ff6c-97e2-4156-8793-9cac4b1bd2ef.jpg', N'FU-HL', N'CS', N'CS', N'', N'')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (34, N'SE62864', N'Tuấn', N'Nguyễn Anh', CAST(N'1998-05-19' AS Date), 1, N'0386785732', N'Tỉnh Quảng Bình', N'thôn Trung Tiến, phù Hóa, Quảng Trạch, Tỉnh Quảng Bình', N'/Images/ProfileImage/8a7d8f4f-52dd-4efd-9a2d-390fd48f0017.jpg', N'FU-HL', N'BIA', N'IA', N'Nguyễn Trọng Kiểm', N'0356820064')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (35, NULL, N'Lâm', N'Phan Trường', CAST(N'2019-04-21' AS Date), 1, N'0123456789', NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (36, NULL, N'hapth@fpt.edu.v', N'FPT', NULL, 1, NULL, NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (37, NULL, N'Hà', N'Phạm Tuyết Hạnh', CAST(N'2019-04-21' AS Date), 1, N'0123456789', NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (38, N'SE04828', N'Nhật', N'Võ Minh', CAST(N'1997-02-19' AS Date), 1, N'0344145250', N'Tp. Hà Nội', N'số 57D, ngõ 34, đường Xuân La, quận Tây Hồ, Tp. Hà Nội', N'/Images/ProfileImage/89933b7b-06c8-4433-a645-d2f61101fdcf.jpg', N'FU-HL', N'BSE', N'IS', N'Võ Trọng Hùng', N'0904055698')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (39, N'SE04280', N'Khánh', N'Nguyễn Xuân', CAST(N'1996-09-27' AS Date), 1, N'0912825239', N'tỉnh Nam Định', N'40/269 đường Giải Phóng phường Trường Thi thành phố Nam Định, tỉnh Nam Định', N'/Images/ProfileImage/ed1d9eec-df35-4908-a889-f077c4499c3f.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Xuân Khanh', N'0912091358')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (40, N'SE04182', N'Huy', N'Nguyễn Lê Quang', CAST(N'1996-09-24' AS Date), 1, N'0945494127', N'Đà Nẵng', N'k27/9 Nguyễn Thành Hãn, Phường Hòa Thuận Tây, Hải Châu, Đà Nẵng', N'/Images/ProfileImage/98254f03-9580-44ce-9a69-9da295e3e11a.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Lê Quang Huy', N'05112214576')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (41, N'SE04221', N'Bách', N'Nguyễn Viết', CAST(N'1996-07-28' AS Date), 1, N'0777262009', N'Hà Nội', N'Fb20 khu đô thị Thiên Đường Bảo Sơn - An Khánh - Hoài Đức - Hà Nội', N'/Images/ProfileImage/d7d99776-4397-438c-9b62-360457ef2445.jpg', N'FU-HL', N'BSE', N'JS', N'Tạ Thị Nga', N'0902234603')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (42, N'SE04261', N'Công', N'Nguyễn Đình', CAST(N'1995-09-20' AS Date), 1, N'0976887733', N'Hà Nội', N'Cụm 3, thôn Hạ, Liên Trung, Đan Phượng, Hà Nội', N'/Images/ProfileImage/65eea604-9b09-431e-8b8e-e380149f40bc.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Đình Chiến', N'0989144690')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (43, N'SE04763', N'Anh', N'Giản Quốc', CAST(N'1997-01-25' AS Date), 1, N'01256402477', N'tỉnh Nghệ An', N'số 36, ngõ 2, Ngô Thì Nhậm, Khối 1, phường Trung Độ, Tp. Vinh, tỉnh Nghệ An', N'/Images/ProfileImage/2e7e0485-fc50-4a93-8b8b-5436b24835b1.jpg', N'FU-HL', N'BSE', N'IS', N'Giản Tư Lâm', N'0983989610')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (44, N'SE04804', N'Anh', N'Phạm Ngọc', CAST(N'1997-12-07' AS Date), 0, N'0836756752', N'Hà Nội', N'Phố Cầu Giẽ, Đại Xuyên, Phú Xuyên, Hà Nội', N'/Images/ProfileImage/8c5c0082-80f1-443b-ab67-86eac06bc285.jpg', N'FU-HL', N'BGD', N'GD', N'Pham Ngoc Anh', N'0913501351-0942536200')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (45, N'SE04734', N'Dũng', N'Lê Văn', CAST(N'1997-01-06' AS Date), 1, N'0378398809', N'Hà nội', N'Số 35 ngách 32 ngõ 76 An Dương,Yên Phụ,Tây Hồ,Hà nội', N'/Images/ProfileImage/d6889dd2-b43f-4683-9855-e758285d588b.jpg', N'FU-HL', N'BSE', N'IS', N'Trần Thị Thanh', N'0913218135-0983821966')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (46, N'SE04533', N'Cảnh', N'Khổng Đức', CAST(N'1997-02-17' AS Date), 1, N'0989284470', N'Vĩnh Phúc', N'Xóm Lũng Hòa, huyện Vĩnh Tường, Vĩnh Phúc', N'/Images/ProfileImage/ae43218b-e986-4454-b789-43948e16c452.jpg', N'FU-HL', N'BSE', N'IS', N'Khổng Duy Thọ', N'0979806489')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (47, N'SE03459', N'Quang', N'Hoàng Đình', CAST(N'1995-03-03' AS Date), 1, N'0961912745', N'tỉnh Nghệ An', N'xóm 3, xã Thanh Liên, huyện Thanh Chương, tỉnh Nghệ An', N'/Images/ProfileImage/51d559c7-c58c-4a3e-be79-8e5525c4e94d.jpg', N'FU-HL', N'BSE', N'IS', N'Trần Thị Giang', N'0976051052-635770051')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (48, NULL, N'khiemhnb@fpt.edu.vn', N'FPT', CAST(N'2019-04-21' AS Date), 1, NULL, NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (49, NULL, N'thanhhn3@fpt.edu.vn', N'FPT', CAST(N'2019-04-21' AS Date), 1, NULL, NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (50, NULL, N'Thành', N'Nguyễn Hà', CAST(N'2019-04-21' AS Date), 1, N'0123456789', NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (51, N'SE04903', N'Sơn', N'Trần Văn', CAST(N'1997-05-24' AS Date), 1, N'0344810356', N'Gia Lai', N'Thôn 6, xã Iablang, huyện Chư Sê, Gia Lai', N'/Images/ProfileImage/83e0eaba-ef22-4031-827d-c9aecf00232f.jpg', N'FU-HL', N'BSE', N'SE', N'Trần Văn Sơn', N'01644810356')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (52, NULL, N'Anh', N'Trịnh Phương', CAST(N'2019-04-21' AS Date), 1, N'0123456789', NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (53, NULL, N'Hà', N'Phạm Tuyết Hạnh', CAST(N'2019-04-21' AS Date), 1, N'0123456789', NULL, NULL, NULL, N'Hoa Lac', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (54, N'HE131024', N'Hà', N'Lê Thị Thu', CAST(N'1997-05-01' AS Date), 0, N'0339856504', N'Lâm Đồng', N'Lâm Đồng', N'/Images/ProfileImage/54b90c8f-3a03-4461-a037-7e696903f348.jpg', N'FU-HL', N'SE', N'IS', N'Ha Le', N'0339856504')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (55, N'SE04853', N'Oanh', N'Phạm Thị', CAST(N'1997-01-04' AS Date), 0, N'0962873142', N'Hà Nội', N'Cụm 5, thôn Phượng Nghĩa, Phụng Châu, Chương Mỹ, Hà Nội', N'/Images/ProfileImage/f6b8123c-1984-427e-86cb-52cb7059f2c8.jpg', N'FU-HL', N'BSE', N'IS', N'Phạm Văn Trường', N'0902127304-01214077221')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (56, N'SE04836', N'Duy', N'Bế Khánh', CAST(N'1997-09-19' AS Date), 1, N'0829190997', N'TP Cao Bằng', N'046 tổ 9, phố Bế Văn Đàn, phường Hợp Giang, TP Cao Bằng', N'/Images/ProfileImage/281a8773-037f-481c-affa-76ae3b3cd598.jpg', N'FU-HL', N'BIA', N'IA', N'Bế Kim Giáp', N'0988984472-0985335339')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (57, N'SE04881', N'Vinh', N'Tran Quang', CAST(N'1997-02-07' AS Date), 1, N'0988070297', N'Tp. Hà Nội', N'Thôn Chiền, xã Đức Thượng, huyện Hoài Đức, Tp. Hà Nội', N'/Images/ProfileImage/084191db-93ad-40e2-8fb0-1ed608f86a92.jpg', N'FU-HL', N'BSE', N'JS', N'Trần Quang Vinh', N'0855100900')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (58, N'SE04563', N'Nghĩa', N'Trần Đại', CAST(N'1994-11-04' AS Date), 1, N'0966194068', N'Hà Tĩnh', N'Khối phố 5 Thị Trấn Đức Thọ, Đức Thọ, Hà Tĩnh', N'/Images/ProfileImage/d2a09f65-44ea-4e6c-91ac-5ebea02c5d2b.jpg', N'FU-HL', N'BSE', N'IS', N'Trần Ngọc Liêm', N'0976985140-0913641027')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (59, N'SB01857', N'Hùng', N'Phan Văn', CAST(N'1997-12-02' AS Date), 1, N'0967677522', N'Bắc Ninh', N'Đồng Nguyên, Từ Sơn, Bắc Ninh', N'/Images/ProfileImage/8377a7e3-f884-473d-95fc-703822234385.jpg', N'FU-HL', N'BBA', N'COF', N'Phan Văn Nhung', N'0913091643-0973525610')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (60, N'SE04792', N'Kiên', N'Nguyễn Trung', CAST(N'1997-09-09' AS Date), 1, N'0973118854', N'Hà Nội', N'Nhà số 1, ngách 178/30 phố Tây Sơn, Trung Liệt, Đống Đa, Hà Nội', N'/Images/ProfileImage/c33a1aa0-e7b9-4810-8241-6c18ca17595c.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Văn Báu', N'0912544086-0914725212')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (61, N'SE04759', N'Đức', N'Trương Tuấn', CAST(N'1997-11-13' AS Date), 1, N'0795159939', N'Tp. Hà Nội', N'số 15, ngõ 36 phố Láng Hạ, phường Láng Hạ, quận Đống Đa, Tp. Hà Nội', N'/Images/ProfileImage/00546df6-5362-4bee-9368-fc34002ccdfd.jpg', N'FU-HL', N'BSE', N'JS', N'Trương Cam Thịnh', N'0904282888-0904022022')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (62, N'SE04576', N'Đạt', N'Bùi Quang', CAST(N'1997-11-01' AS Date), 1, N'01233188886', N'Hà nội', N'Số 34 Hàng Cá,Hàng Đào,Hoàn Kiếm,Hà nội', N'/Images/ProfileImage/b0229c58-1519-4b4b-a5b9-03e4799d7ea4.jpg', N'FU-HL', N'BSE', N'JS', N'Bùi Quang Nghĩa', N'01238997686-01233300879')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (63, N'SB01816', N'Anh', N'Trần Lê Minh', CAST(N'1997-11-28' AS Date), 0, N'0949511897', N'tp. Hà Nội', N'P408, nhà M2, tập thể Láng Trung, phường Láng Hạ, quận Đống Đa, tp. Hà Nội', N'/Images/ProfileImage/913dfccd-34ae-4ac6-8f99-503ca50f4146.jpg', N'FU-HL', N'BBA', N'FIN', N'Lê Thị Mỹ Hạnh', N'0988821739-0983372318')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (64, NULL, N'thuytt@fpt.edu.vn', N'FPT', CAST(N'2019-04-25' AS Date), 1, NULL, NULL, NULL, NULL, N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (65, N'HS140308', N'Hà', N'Lê  Thị Hồng ', CAST(N'2000-10-15' AS Date), 0, N'0915961857', N'Hà Nội', N'96, ngõ 93 Hoàng Văn Thái, Thanh Xuân, Hà Nội', N'/Images/ProfileImage/395be1e8-5b94-4aff-ad9f-8f7af9b9c4ee.jpg', N'FU-HL', N'BA', N'BA', N'Phan Thị Hiên', N'0354414363')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (66, N'HE130151', N'Tùng', N'Nguyễn Việt', CAST(N'1999-09-22' AS Date), 1, N'0375541892', N'Hà Nội', N'Số nhà 184 ngõ 467 Lĩnh Nam Hoàng Mai Hà Nội', N'/Images/ProfileImage/9afd803d-1e12-4a89-a378-d9f7a064577d.jpg', N'FU-HL', N'CS', N'CS', N'Nguyễn Danh Trụ', N'0375541892')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (67, N'HS140009', N'Nhi', N'Hoàng  Vân ', CAST(N'2000-08-17' AS Date), 0, N'0941411699', N'Hải Phòng', N'20 Ký Con - Phạm Hồng Thái - Hồng Bàng - Hải Phòng', N'/Images/ProfileImage/d8964ada-ebc8-45b8-9458-fda87f64cf83.jpg', N'FU-HL', N'IB', N'IB', N'Nguyễn Thị Vân', N'0912520700')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (68, N'HA130039', N'Minh', N'Nguyễn Ngọc', CAST(N'1999-10-31' AS Date), 1, N'01668351356', N'Hà Nội', N'Hẻm 102/44/49Phá Đài Láng -Đống Đa-Hà Nội', N'/Images/ProfileImage/357da5ba-9fce-4116-837c-0468dd8e0a11.jpg', N'FU-HL', N'GD', N'GD', N'Vũ Lan Anh', N'0913582942')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (69, N'HS140289', N'Trang', N'Nguyễn  Thị Kiều ', CAST(N'2000-09-24' AS Date), 0, N'', N'', N'', N'/Images/ProfileImage/369b999b-42cd-4d09-b04b-bc40bdd20115.jpg', N'FU-HL', N'IB', N'IB', N'', N'')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (70, NULL, N'abc@fpt.edu.vn', N'FPT', CAST(N'2019-04-25' AS Date), 1, NULL, NULL, NULL, NULL, N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (71, N'HS140227', N'Linh', N'Nguyễn  Thị Thùy ', CAST(N'2000-10-24' AS Date), 0, N'', N'', N'', N'/Images/ProfileImage/95b15ee1-16d8-4f5b-b92e-3728e7408595.jpg', N'FU-HL', N'IB', N'IB', N'', N'')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (72, N'HS130143', N'Thùy', N'Nguyễn Thanh', CAST(N'1999-10-18' AS Date), 0, N'Nguy?n Thanh Thu?', N'Thái Bình', N'Khu Mẽ, Thị Trấn Hưng Nhân, Hưng Hà, Thái Bình', N'/Images/ProfileImage/6b1c29d9-6ae0-4083-82ec-b3535318be6c.jpg', N'FU-HL', N'BA', N'BA', N'ĐỒng Thị Nguyên', N'0352419775')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (73, N'HA140117', N'Thủy', N'Nguyễn  Thị ', CAST(N'2000-02-29' AS Date), 0, N'', N'', N'', N'/Images/ProfileImage/9e235c95-22c4-4fae-9862-604d990ce300.jpg', N'FU-HL', N'JPL', N'JPL', N'', N'')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (74, N'SE04703', N'Hoàng', N'Nguyễn Văn', CAST(N'1997-04-02' AS Date), 1, N'01634442388', N'Thái Bình', N'tổ 1, Đông Linh, An Bài, Quỳnh Phụ, Thái Bình', N'/Images/ProfileImage/6fa9027d-8e73-4d35-9725-b85ea295c4d1.jpg', N'FU-HL', N'BSE', N'IS', N'Nguyễn Thị Hương', N'0975593838')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (75, N'SE04218', N'Phương', N'Nguyễn Hồng', CAST(N'1996-08-24' AS Date), 0, N'0986721906', N'Hà Nội', N'Số nhà 43, ngõ 342 đường Khương Đình, Thanh Xuân, Hà Nội', N'/Images/ProfileImage/98771b74-2cd3-4a71-8f35-f890024cc902.jpg', N'FU-HL', N'BSE', N'JS', N'Nguyễn Hồng Quân', N'0987510763')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (76, N'SB01828', N'Trinh', N'Lại Thục', CAST(N'1997-01-26' AS Date), 0, N'0945894025', N'Nam Định', N'khu 5 thị trấn Ngô Đồng, Giao Thủy, Nam Định', N'/Images/ProfileImage/c0df555f-5547-4679-8cde-333fc9864908.jpg', N'FU-HL', N'BBA', N'BA', N'Lại Văn Nhân', N'0986170393-01248378495')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (77, N'HA130070', N'Thủy', N'Nguyễn Thanh', CAST(N'1999-02-04' AS Date), 0, N'0911040299', N'Hà Nội', N'P109, C1, phố Hoàng Ngọc Phách, phường Láng Hạ, quận Đống Đa, Hà Nội', N'/Images/ProfileImage/Default.jpg', N'FU-HL', N'JPL', N'JPL', N'Bùi Thị Chung', N'0982170467')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (78, N'HS140240', N'Chi', N'Trần  Hà ', CAST(N'2000-03-09' AS Date), 0, N'', N'', N'', N'/Images/ProfileImage/Default.jpg', N'FU-HL', N'MC', N'MC', N'', N'')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (79, N'HS130223', N'Thủy', N'Nguyễn Thị Xuân', CAST(N'1999-01-31' AS Date), 0, N'0833217457', N'phố Vinh', N'Khối Mỹ Thành - phường Đông Vĩnh - Thành phố Vinh', N'/Images/ProfileImage/Default.jpg', N'FU-HL', N'Truyền thông đa phương tiện', N'MC', N'Lê Thị Đào', N'943304531')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (80, N'SB02066', N'Linh', N'Nguyễn Thị Thùy', CAST(N'1998-05-08' AS Date), 0, N'0833585405', N'Tp. Hà Nội', N'Xã Thạch Hòa, huyện Thạch Thất, Tp. Hà Nội', N'/Images/ProfileImage/c70715c2-c158-4d12-9114-243bd77b6852.jpg', N'FU-HL', N'BBA', N'MKT', N'Nguyễn Thị Thùy Chi', N'0914625257')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (81, NULL, N'anhbn', N'FPT', CAST(N'2019-04-29' AS Date), 1, NULL, NULL, NULL, N'/Images/ProfileImage/Default.jpg', N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (82, NULL, N'hieuld@fpt.edu.vn', N'FPT', CAST(N'2019-04-27' AS Date), 1, NULL, NULL, NULL, NULL, N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (83, NULL, N'duongtb', N'FPT', CAST(N'2019-04-29' AS Date), 1, NULL, NULL, NULL, N'/Images/ProfileImage/Default.jpg', N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (84, NULL, N'sangnv@fpt.edu.vn', N'FPT', CAST(N'2019-04-27' AS Date), 1, NULL, NULL, NULL, NULL, N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (85, NULL, N'kienlt', N'FPT', CAST(N'2019-04-29' AS Date), 1, NULL, NULL, NULL, N'/Images/ProfileImage/Default.jpg', N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (86, NULL, N'CuongN@fpt.edu.vn', N'FPT', CAST(N'2019-04-27' AS Date), 1, NULL, NULL, NULL, NULL, N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (87, N'HS140147', N'Đạt', N'Bùi  Quang ', CAST(N'1997-06-03' AS Date), 1, N'0334871280', N'Hà Nội', N'Số 3a ngõ 9 đường 800A Nghĩa Đô Cầu Giấy Hà Nội', N'/Images/ProfileImage/9b8e48d1-f1f5-4cbe-b6ab-89a434f90a61.jpg', N'FU-HL', N'BA', N'BA', N'Nguyễn Thị Minh Anh', N'0983856899')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (88, NULL, N'thanglv9', N'FPT', CAST(N'2019-04-28' AS Date), 1, NULL, NULL, NULL, N'/Images/ProfileImage/Default.jpg', N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (89, NULL, N'yenltse04832', N'FPT', CAST(N'2019-04-30' AS Date), 1, NULL, NULL, NULL, N'/Images/ProfileImage/Default.jpg', N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (90, NULL, N'lannhhs140680', N'FPT', CAST(N'2019-04-30' AS Date), 1, NULL, NULL, NULL, N'/Images/ProfileImage/Default.jpg', N'FU-HL', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (91, N'SE04590', N'Quân', N'Trần Minh', CAST(N'1997-12-02' AS Date), 1, NULL, N'Hà Nội', N'số 9, hẻm 28/31, ngách 28 ngõ Văn Hương, phố Tôn Đức Thắng, phường Hàng Bột, quận Đống Đa, Tp. Hà Nội', N'/Images/ProfileImage/80b7dbfd-d0af-48ae-82aa-7c8543a6f8a6.jpg', N'FU-HL', N'BGD', N'GD', N'Trần Thanh Hải', N'0903439879-0909696886')
GO
INSERT [dbo].[UserProfile] ([ProfileId], [RollNumber], [FirstName], [LastName], [DOB], [Gender], [Phone], [City], [Address], [ProfileImage], [Campus], [Major], [Specialization], [ParentName], [ParentPhone]) VALUES (92, N'SE05081', N'Trà', N'Võ Minh Hương', CAST(N'1996-02-15' AS Date), 0, N'0917791502', N'Nghệ An', N'số 17, đường Văn Đức Giai, khối 17, phường Hưng Bình, Tp. Vinh, tỉnh Nghệ An', N'/Images/ProfileImage/e485ed24-aac4-454b-a5e2-d223375de146.jpg', N'FU-HL', N'BSE', N'IS', N'Võ Anh Tuấn', N'0902206757-0918313069')
GO
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__User__A9D105348A4D9309]    Script Date: 5/2/2019 21:45:50 ******/
ALTER TABLE [dbo].[User] ADD UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Bookmark] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Category] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[Category] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Checkin] ADD  DEFAULT (getdate()) FOR [CheckedinDate]
GO
ALTER TABLE [dbo].[Event] ADD  DEFAULT ((0)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[Event] ADD  DEFAULT ((0)) FOR [IsFeatured]
GO
ALTER TABLE [dbo].[Event] ADD  DEFAULT ('Pending') FOR [EventStatus]
GO
ALTER TABLE [dbo].[Event] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Event] ADD  DEFAULT ((0)) FOR [IsOrganizerOnly]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FeedbackOuter] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Form] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Group] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[Group] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Group] ADD  DEFAULT (getdate()) FOR [AssignedDate]
GO
ALTER TABLE [dbo].[Notification] ADD  DEFAULT (getdate()) FOR [NotifiedDate]
GO
ALTER TABLE [dbo].[Register] ADD  DEFAULT (getdate()) FOR [RegisteredDate]
GO
ALTER TABLE [dbo].[Report] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Report] ADD  DEFAULT ((0)) FOR [IsDismissed]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT ((1)) FOR [IsStudent]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT ((0)) FOR [IsOrganizer]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT ((0)) FOR [IsManager]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT (getdate()) FOR [NotificationSeenDate]
GO
ALTER TABLE [dbo].[UserGroup] ADD  DEFAULT (getdate()) FOR [ParticipatedDate]
GO
ALTER TABLE [dbo].[Bookmark]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Bookmark]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Category]  WITH CHECK ADD FOREIGN KEY([CreatorId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Checkin]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Checkin]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD FOREIGN KEY([FamilyId])
REFERENCES [dbo].[EventFamily] ([FamilyId])
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([GroupId])
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD FOREIGN KEY([ManagerId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD FOREIGN KEY([OrganizerId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[EventCategory]  WITH CHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[EventCategory]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[FeedbackOuter]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Form]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD FOREIGN KEY([LeaderId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD FOREIGN KEY([ManagerId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD FOREIGN KEY([SubjectId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Register]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Register]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Report]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Report]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD FOREIGN KEY([ProfileId])
REFERENCES [dbo].[UserProfile] ([ProfileId])
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([GroupId])
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD FOREIGN KEY([OrganizerId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD CHECK  (([EventStatus]='Rejected' OR [EventStatus]='Closed' OR [EventStatus]='Happening' OR [EventStatus]='Opening' OR [EventStatus]='Cancelled' OR [EventStatus]='Pending' OR [EventStatus]='Draft' OR [EventStatus]='Deleted'))
GO
