namespace capstone.database.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Register")]
    public partial class Register
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EventId { get; set; }

        public string Status { get; set; }

        public string QRCode { get; set; }

        public DateTime? CheckinDate { get; set; }

        public DateTime RegisteredDate { get; set; }

        public virtual Event Event { get; set; }

        public virtual User User { get; set; }
    }
}
