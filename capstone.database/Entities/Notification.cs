namespace capstone.database.Entities {
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Notification")]
    public partial class Notification {
        public int NotificationId { get; set; }

        public int UserId { get; set; }

        public int? SubjectId { get; set; }

        public int? ObjectId { get; set; }

        [StringLength(10)]
        public string ObjectType { get; set; }

        public string NotificationContent { get; set; }

        public DateTime NotifiedDate { get; set; }

        public virtual User Subject { get; set; }

        public virtual User User { get; set; }
    }
}
