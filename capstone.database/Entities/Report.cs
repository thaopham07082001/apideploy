namespace capstone.database.Entities {
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Report")]
    public partial class Report {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int EventId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }

        public bool IsDismissed { get; set; }

        public virtual Event Event { get; set; }

        public virtual User User { get; set; }
    }
}
