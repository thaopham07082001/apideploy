namespace capstone.database.Entities {
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Checkin")]
    public partial class Checkin {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EventId { get; set; }

        public DateTime CheckedinDate { get; set; }

        public virtual Event Event { get; set; }

        public virtual User User { get; set; }
    }
}
