namespace capstone.database.Entities {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Event")]
    public partial class Event {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Event() {
            Bookmarks = new HashSet<Bookmark>();
            Checkins = new HashSet<Checkin>();
            Feedbacks = new HashSet<Feedback>();
            FeedbackOuters = new HashSet<FeedbackOuter>();
            Forms = new HashSet<Form>();
            Registers = new HashSet<Register>();
            EventRoles = new HashSet<EventRole>();
            SmartTVs = new HashSet<SmartTV>();
            Reports = new HashSet<Report>();
            Categories = new HashSet<Category>();
        }

        public int EventId { get; set; }

        [Required]
        [StringLength(100)]
        public string EventName { get; set; }

        public int? FamilyId { get; set; }

        [Required]
        public string CoverImage { get; set; }

        public bool IsPublic { get; set; }

        public bool IsFeatured { get; set; }

        [Required]
        [StringLength(10)]
        public string EventStatus { get; set; }

        [Required]
        public string EventDescription { get; set; }

        [Required]
        public string EventDescriptionHtml { get; set; }

        [Required]
        [StringLength(100)]
        public string EventPlace { get; set; }

        [Column(TypeName = "money")]
        public decimal EventFee { get; set; }

        public int? OrganizerId { get; set; }

        public int? GroupId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ManagerId { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime? RegisterEndDate { get; set; }

        public DateTime OpenDate { get; set; }

        public DateTime CloseDate { get; set; }

        public int? RegisterMax { get; set; }

        public bool IsOrganizerOnly { get; set; }

        public string Participants { get; set; }

        public string TargetGroup { get; set; }

        public string UrlLink { get; set; }

        public DateTime? StartDateBanner { get; set; }

        public DateTime? EndDateBanner { get; set; }

        public DateTime? StartDateVideo { get; set; }

        public DateTime? EndDateVidep { get; set; }

        public string StandeeImage { get; set; }

        public int CampusId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bookmark> Bookmarks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Checkin> Checkins { get; set; }

        public virtual EventFamily EventFamily { get; set; }

        public virtual Campus Campus { get; set; }

        public virtual Group Group { get; set; }

        public virtual User Manager { get; set; }

        public virtual User Organizer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feedback> Feedbacks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedbackOuter> FeedbackOuters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Form> Forms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Register> Registers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventRole> EventRoles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SmartTV> SmartTVs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Report> Reports { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Category> Categories { get; set; }
    }
}
