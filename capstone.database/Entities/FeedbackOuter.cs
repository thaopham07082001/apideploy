namespace capstone.database.Entities {
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("FeedbackOuter")]
    public partial class FeedbackOuter {
        public string UserName { get; set; }

        public string UserImage { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(200)]
        public string Key { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EventId { get; set; }

        public double Value { get; set; }

        public DateTime CreatedDate { get; set; }

        public string FeedbackContent { get; set; }

        public virtual Event Event { get; set; }
    }
}
