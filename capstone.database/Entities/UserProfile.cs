namespace capstone.database.Entities {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserProfile")]
    public partial class UserProfile {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserProfile() {
            Users = new HashSet<User>();
        }

        [Key]
        public int ProfileId { get; set; }

        [StringLength(10)]
        public string RollNumber { get; set; }

        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DOB { get; set; }

        public bool Gender { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        public string Address { get; set; }

        public string ProfileImage { get; set; }

        [Required]
        [StringLength(100)]
        public string Campus { get; set; }

        [StringLength(100)]
        public string Major { get; set; }

        [StringLength(100)]
        public string Specialization { get; set; }

        [StringLength(100)]
        public string ParentName { get; set; }

        [StringLength(50)]
        public string ParentPhone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }
    }
}
