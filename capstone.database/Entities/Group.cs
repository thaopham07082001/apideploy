namespace capstone.database.Entities {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Group")]
    public partial class Group {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Group() {
            Events = new HashSet<Event>();
            UserGroups = new HashSet<UserGroup>();
        }

        public int GroupId { get; set; }

        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }

        public string GroupImage { get; set; }

        public string GroupDescription { get; set; }

        [StringLength(100)]
        public string GroupMail { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FoundedYear { get; set; }

        public bool IsEnabled { get; set; }

        public int ManagerId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int LeaderId { get; set; }

        public DateTime AssignedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }

        public virtual User Leader { get; set; }

        public virtual User Manager { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserGroup> UserGroups { get; set; }
    }
}
