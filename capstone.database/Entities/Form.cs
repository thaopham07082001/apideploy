namespace capstone.database.Entities {
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Form")]
    public partial class Form {
        public int FormId { get; set; }

        public int? EventId { get; set; }

        [Required]
        public string FormLink { get; set; }

        [Required]
        public string FormRegister { get; set; }

        [Required]
        public string FormResult { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual Event Event { get; set; }
    }
}
